function yoXXX = LinearInt(yfECG, NsegOUT, NsamOUT, TSstartOUT, FsOUT, DlXXX, TSstartXXX, FsXXX, TSfreq)
%LinearInt linear interpolation process

yoXXX = zeros(1,(NsegOUT-1)*NsamOUT);
for seg = 1:NsegOUT-1
    %% XXX block processing
    % Find segment edges for E
    XXX_first = DlXXX*FsOUT/FsXXX +               (TSstartOUT(seg) - TSstartXXX)*FsOUT/TSfreq; % Samples already decimated to output rate
    XXX_last  = DlXXX*FsOUT/FsXXX + (NsamOUT-1) + (TSstartOUT(seg) - TSstartXXX)*FsOUT/TSfreq; % Samples already decimated to output rate

    [ECG_first_sam, ECG_first_seg] = min(XXX_first.*(XXX_first>=0) + (XXX_first<0)*NsamOUT);
    [~,             ECG_last_seg ] = min(XXX_last .*(XXX_last >=0) + (XXX_last <0)*NsamOUT);
    
    % Calculate 'local' FsECG
    if (ECG_last_seg < length(TSstartXXX))
        ECGstartSam = ECG_first_sam + (ECG_first_seg-1)*NsamOUT;
        ECGsamInc   = 1;
        
        Secg = ECGstartSam + ECGsamInc*(0:NsamOUT-1); % ECG sample indexes
        Secg = max(min(Secg,length(yfECG)-2),1); % Limit from 1to ECG vector length
        SIecg = floor(Secg);  % Integer part
        SFecg = Secg - SIecg; % Fractional part
        
        y0 = yfECG(SIecg+1); % index+0
        y1 = yfECG(SIecg+2); % Index+1
        
        % Map to y = pb*x + pc, where x = 0 or +1
        pc = y0; % Solve for pb and pc
        pb = y1-y0;
        
        idx = (seg-1)*NsamOUT + 1;
        yoXXX(idx:idx+NsamOUT-1) = pb.*SFecg + pc; % Interpolation range: x = 0.0 to +1.0
    end
end

end

