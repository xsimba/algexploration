% SRC data input

% 1. DONE: Remove vector based tests
% 2. DONE: Don't use fillers in timestamp data
% 3. DONE: Use new files
% 4. DONE: Add extra PPG, BioZ and ACC inputs
% 5. Add correct text headers

clear all;
close all;
clc;

%% Make SRCs

% Build filters
HdECG      = ECGsrcDec4();        % FIR: ECG  dec by  4 input rate  512 Hz
HdBioZ     = BioZsrcDec8();       % FIR: BioZ dec by  8 input rate 1024 Hz
HdPPG      = PPGsrcInt16();       % FIR: PPG  int by 16 input rate ~100 Hz
HdACC      = ACCsrcInt16();       % FIR: ACC  int by 16 input rate ~125 Hz
Hd30HzMain = Main30HzLPF128kFs(); % IIR: Main filter LPF 30 Hz/0.1 dB pass, 49.5 Hz/125 dB stop @ 128kHz rate

% Filter responses
figure; hold on;
[h,w]=freqz(HdECG,65536);        plot(w*512   /(2*pi),max(20*log10(abs(h)   ),-140),'m');
[h,w]=freqz(HdBioZ,65536);       plot(w*1024  /(2*pi),max(20*log10(abs(h)   ),-140),'c');
[h,w]=freqz(HdPPG,65536);        plot(w*100*16/(2*pi),max(20*log10(abs(h)/16),-140),'b'); % Assuming 100 Hz PPG
[h,w]=freqz(HdACC,65536);        plot(w*125*16/(2*pi),max(20*log10(abs(h)/16),-140),'r'); % Assuming 125 Hz ACC
[h,w]=freqz(Hd30HzMain,1048576); plot(w*128000/(2*pi),max(20*log10(abs(h)   ),-140),'k'); % 128 kHz, many frequency points
grid on; title('Filter responses.'); legend('ECG','BioZ','PPG','ACC','main');
ylim([-140 0.1]); xlim([0 800]);
clear h w;

% Filter group delays
DlECG  = grpdelay(HdECG);
DlBioZ = grpdelay(HdBioZ);
DlPPG  = grpdelay(HdPPG);
DlACC  = grpdelay(HdACC);
[Dlmain,w] = grpdelay(Hd30HzMain,131072);

figure; plot(w*128000/(2*pi),Dlmain,'k'); grid on; xlim([0 50]);

% Sanity check
if (sum(abs(diff(DlECG))) >0), error('Non constant group delay for ECG SRC');  end
if (sum(abs(diff(DlBioZ)))>0), error('Non constant group delay for BioZ SRC'); end
if (sum(abs(diff(DlPPG))) >0), error('Non constant group delay for PPG SRC');  end
if (sum(abs(diff(DlACC))) >0), error('Non constant group delay for ACC SRC');  end

DlECG  = DlECG(1); % Reduce to single values, this is the group delay referred to the input sample rates
DlBioZ = DlBioZ(1);
DlPPG  = DlPPG(1);
DlACC  = DlACC(1);

disp(['Delay values (samples), ECG: ',num2str(DlECG),', BioZ: ',num2str(DlBioZ),', PPG: ',num2str(DlPPG),', ACC: ',num2str(DlACC)]);

%% Block based processing
TSfreq = 32768;

%% File input
EnableACC  = 1;
EnableBioZ = 1;
EnableECG  = 1;
EnableETI  = 1;
EnablePPG  = 1;


if (EnablePPG)
    NsamPPG = 50; %#ok<UNRCH>
    % PPG channel A
    %data = dlmread('\\winnl\simba\Databases\Rest Recordings\Raw data\Participant 2\Wristband 1 - 13\Light on\sample_data.PPG2-Grn.txt',',',1,0);
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.PPG2-Grn.txt',',',1,0);
    yPPGa = data(:,2)'; % Data
    NsegPPGa = floor(length(yPPGa)/NsamPPG); % Number of segments
    TSstartPPGa  = data(1:NsamPPG:NsamPPG*NsegPPGa,1)'; % All PPGa start time stamps
    
    % PPG channel B
    %data = dlmread('\\winnl\simba\Databases\Rest Recordings\Raw data\Participant 2\Wristband 1 - 13\Light on\sample_data.PPG2-Red.txt',',',1,0);
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.PPG2-Red.txt',',',1,0);
    yPPGb = data(:,2)'; % Data
    NsegPPGb = floor(length(yPPGb)/NsamPPG); % Number of segments
    TSstartPPGb  = data(1:NsamPPG:NsamPPG*NsegPPGb,1)'; % All PPGb start time stamps
    if(TSstartPPGa(1) == TSstartPPGb(1)) % Introduce 1/2 sample delay for PPGb channel if not done in input data stream
        TSstartPPGb = TSstartPPGb + 0.5*(TSstartPPGb(2) - TSstartPPGb(1))/NsamPPG; %#ok<NASGU> % 1/2 TicksPerBlock/SamplesPerBlock
        disp('Adding 1/2 sample delay to PPGb channel');
    end
end

if (EnableECG)
    NsamECG = 256; %#ok<UNRCH>
    % ECG
    %data = dlmread('\\winnl\simba\Databases\Rest Recordings\Raw data\Participant 2\Wristband 1 - 13\Light on\sample_data.ECG.txt',',',1,0);
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.ECG.txt',',',1,0);
    FsECG = 512;
    yECG = data(:,2)'; % Data
    NsegECG = floor(length(yECG)/NsamECG); % Number of segments
    TSstartECG  = data(1:NsamECG:NsamECG*NsegECG,1)'; % All ECG start time stamps
end

if (EnableETI)
    NsamETI = 256; %#ok<UNRCH>
    % ETI
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.IMP.txt',',',1,0);
    FsETI = 512;
    yETI = data(:,2)'; % Data
    NsegETI = floor(length(yETI)/NsamETI); % Number of segments
    TSstartETI = data(1:NsamETI:NsamETI*NsegECG,1)'; % All ETI start time stamps
end

if (EnableBioZ)
    NsamBioZ = 512; %#ok<UNRCH>
    FsBioZ = 1024;
    % BioZ: in-phase channel
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.BioZI.txt',',',1,0);
    yBioZi = data(:,2)'; % Data
    NsegBioZ = floor(length(yBioZi)/NsamBioZ); % Number of segments
    TSstartBioZ  = data(1:NsamBioZ:NsamBioZ*NsegBioZ,1)'; % All BioZ start time stamps
    
    % BioZ: quadrature channel
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.BioZQ.txt',',',1,0);
    yBioZq = data(:,2)'; % Data
end

if (EnableACC)
    NsamACC = 125; %#ok<UNRCH>
    % ACC
    data = dlmread('\\winnl\simba\Tech Documents Alpha\PPGresearch\Algorithms\SRC\TestData\22042014\sample_data.ACC.txt',',',1,0);
    yACCx = data(:,2)'; % Data
    yACCy = data(:,3)';
    yACCz = data(:,4)';
    NsegACC = floor(length(yACCx)/NsamACC); % Number of segments
    TSstartACC  = data(1:NsamACC:NsamACC*NsegACC,1)'; % All ACC start time stamps
end

clear data;


% Test data
if (0)
    TestDur = 500.0; %#ok<UNRCH> % Test duration (s)
    FreqT = 0.90; % Test frequency
    
    if (EnableECG)
        FsECG = 512;
        NsamECG = 256;
        NsegECG = ceil(TestDur*FsECG/NsamECG) + 1;
        TSstartECG = (0:NsegECG-1)*NsamECG*TSfreq/FsECG + 5432; % Timestamps with offset
        yECG = sin((0:NsamECG*NsegECG-1)*2*pi*FreqT/FsECG);
    end
    
    if (EnableETI)
        FsETI = 512;
        NsamETI = 256;
        NsegETI = ceil(TestDur*FsETI/NsamETI) + 1;
        TSstartETI = (0:NsegETI-1)*NsamETI*TSfreq/FsETI + 5432; % Timestamps with offset
        yETI = sin((0:NsamETI*NsegETI-1)*2*pi*FreqT/FsETI);
    end

    if (EnableBioZ)
        FsBioZ = 1024; % Test sample rate
        NsamBioZ = 512;
        NsegBioZ = ceil(TestDur*FsBioZ/NsamBioZ) + 1;
        TSstartBioZ = (0:NsegBioZ-1)*NsamBioZ*TSfreq/FsBioZ + 5432; % Timestamps with offset
        yBioZi = sin((0:NsamBioZ*NsegBioZ-1)*2*pi*FreqT/FsBioZ);
        yBioZq = -yBioZi/3; % Scale by -1/3 for quad channel
    end
    
    if (EnablePPG)
        FsPPGt = 52.34; % Test sample rate
        NsamPPG = 50;
        NsegPPG = ceil(TestDur*FsPPGt/NsamPPG) + 1;
        TSstartPPGa = (0:NsegPPG-1)*NsamPPG*TSfreq/FsPPGt + 5432; % Timestamps with offset
        yPPGa = sin((0:NsamPPG*NsegPPG-1)*2*pi*FreqT/FsPPGt);
        
        TSstartPPGb = ((0:NsegPPG-1)*NsamPPG+0.5)*TSfreq/FsPPGt + 5432; % Timestamps with offset with 1/2 sample offset
        yPPGb = sin(((0:NsamPPG*NsegPPG-1)+0.5)*2*pi*FreqT/FsPPGt);
    end
    
    if (EnableACC)
        FsACCt = 99.85; % Test sample rate
        NsamACC = 125;
        NsegACC = ceil(TestDur*FsACCt/NsamACC) + 1;
        TSstartACC = (0:NsegACC-1)*NsamACC*TSfreq/FsACCt + 5432; % Timestamps with offset
        yACCx = sin((0:NsamACC*NsegACC-1)*2*pi*FreqT/FsACCt);
    end
    
    leg = {}; % Empty legend (cell array)
    figure; hold on;
    if (EnableECG),  plot((0:length(yECG)-1)*TSfreq/FsECG + 5432, yECG,'.-b');          leg = [leg 'ECG'];   end
    if (EnableETI),  plot((0:length(yETI)-1)*TSfreq/FsETI + 5432, yETI,'s-c');          leg = [leg 'ETI'];   end
    if (EnableBioZ), plot((0:length(yBioZi)-1)*TSfreq/FsBioZ + 5432, yBioZi,'x-k');     leg = [leg 'BioZi']; end
    if (EnablePPG),  plot((0:length(yPPGa)-1)*TSfreq/FsPPGt + 5432, yPPGa,'o-g');       leg = [leg 'PPGa'];
                     plot(((0:length(yPPGb)-1)+0.5)*TSfreq/FsPPGt + 5432, yPPGb,'o-r'); leg = [leg 'PPGb'];  end
    if (EnableACC),  plot((0:length(yACCx)-1)*TSfreq/FsACCt + 5432, yACCx,'o-m');       leg = [leg 'ACC'];   end
    title('Test waveforms');legend(leg);
end

TSref = 0;
if (EnableECG), TSref = TSstartECG(1); end % Make reference ECG as then no illegal interpolation will be performed on it.

if (EnableECG),  TSstartECG  = TSstartECG  - TSref; end
if (EnablePPG),  TSstartPPGa = TSstartPPGa - TSref;
                 TSstartPPGb = TSstartPPGb - TSref; end
if (EnableETI),  TSstartETI  = TSstartETI  - TSref; end
if (EnableACC),  TSstartACC  = TSstartACC  - TSref; end
if (EnableBioZ), TSstartBioZ = TSstartBioZ - TSref; end

MinList = [];
if (EnableECG),                                                                                    MinList = [MinList length(yECG)/FsECG];   end
if (EnablePPG), FsPPGe = (length(TSstartPPGa)-1)*NsamPPG*TSfreq/(TSstartPPGa(end)-TSstartPPGa(1)); MinList = [MinList length(yPPGa)/FsPPGe]; end % Number of samples/time duration
if (EnableACC), FsACCe = (length(TSstartACC)-1)*NsamACC*TSfreq/(TSstartACC(end)-TSstartACC(1));    MinList = [MinList length(yACCx)/FsACCe]; end 
Tout = min(MinList); % Minimum time duration of output stream

% Define output parameters
FsOUT = 128; % Output sample rate
SegDur = 0.500; % Segment duration (s)
NsamOUT = round(FsOUT*SegDur); % Number of samples per block at output rate
NsegOUT = floor(Tout*FsOUT/NsamOUT); % Number of segments
TSstartOUT  = (0:NsegOUT-1)*NsamOUT*TSfreq/FsOUT;  % All output start time stamps

% Filtering and decimation/interpolation for SRC
if (EnableECG)
    yfECG   = filter(HdECG,yECG);    yfECG   = yfECG(1:(FsECG/FsOUT):end); % Down sample to FsOUT
    yoECG   = LinearInt( yfECG,   NsegOUT, NsamOUT, TSstartOUT, FsOUT,          DlECG,  TSstartECG,  FsECG,  TSfreq); % Synchronous  SRC for ECG
end    

if (EnableETI)
    yfETI   = filter(HdECG,yETI);    yfETI   = yfETI(1:(FsETI/FsOUT):end); % Down sample to FsOUT
    yoETI   = LinearInt( yfETI,   NsegOUT, NsamOUT, TSstartOUT, FsOUT,          DlECG,  TSstartETI,  FsETI,  TSfreq); % Synchronous  SRC for ETI at same rate as ECG
end

if (EnableBioZ)
    yfBioZi = filter(HdBioZ,yBioZi); yfBioZi = yfBioZi(1:(FsBioZ/FsOUT):end); % Down sample to FsOUT
    yfBioZq = filter(HdBioZ,yBioZq); yfBioZq = yfBioZq(1:(FsBioZ/FsOUT):end);
    yoBioZi = LinearInt( yfBioZi, NsegOUT, NsamOUT, TSstartOUT, FsOUT,          DlBioZ, TSstartBioZ, FsBioZ, TSfreq); % Synchronous  SRC for BioZ
    yoBioZq = LinearInt( yfBioZq, NsegOUT, NsamOUT, TSstartOUT, FsOUT,          DlBioZ, TSstartBioZ, FsBioZ, TSfreq);
end

if (EnablePPG)
    yfPPGa  = filter(HdPPG,yPPGa); % Interpolate to ~1600 Hz (16*FsPPG)
    yfPPGb  = filter(HdPPG,yPPGb);
    yoPPGa  = PPGquadInt(yfPPGa,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamPPG, DlPPG,  TSstartPPGa, FsPPGe, TSfreq); % Asynchronous SRC for PPG
    yoPPGb  = PPGquadInt(yfPPGb,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamPPG, DlPPG,  TSstartPPGb, FsPPGe, TSfreq);
end

if (EnableACC)
    yfACCx  = filter(HdACC,yACCx);     % Interpolate to ~2000 Hz (16*FsACC)
    yfACCy  = filter(HdACC,yACCy);
    yfACCz  = filter(HdACC,yACCz);
    yoACCx  = ACClinInt( yfACCx,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamACC, DlACC,  TSstartACC,  FsACCe, TSfreq); % Asynchronous SRC for ACC
    yoACCy  = ACClinInt( yfACCy,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamACC, DlACC,  TSstartACC,  FsACCe, TSfreq);
    yoACCz  = ACClinInt( yfACCz,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamACC, DlACC,  TSstartACC,  FsACCe, TSfreq);
end

leg = {}; % Empty legend (cell array)
figure; hold on;
if (EnableECG),  plot((0:length(yoECG)-1)/FsOUT,   yoECG-mean(yoECG),'.-b');     leg = [leg 'ECG'];   end
if (EnableETI),  plot((0:length(yoETI)-1)/FsOUT,   yoETI-mean(yoETI),'s-c');     leg = [leg 'ETI'];   end
if (EnableBioZ), plot((0:length(yoBioZi)-1)/FsOUT, yoBioZi-mean(yoBioZi),'.-k'); leg = [leg 'BioZi'];
                 plot((0:length(yoBioZq)-1)/FsOUT, yoBioZq-mean(yoBioZq),'o-k'); leg = [leg 'BioZq']; end
if (EnablePPG),  plot((0:length(yoPPGa)-1)/FsOUT,  yoPPGa-mean(yoPPGa),'.-g');   leg = [leg 'PPGa'];
                 plot((0:length(yoPPGb)-1)/FsOUT,  yoPPGb-mean(yoPPGb),'o-g');   leg = [leg 'PPGb'];  end
if (EnableACC),  plot((0:length(yoACCx)-1)/FsOUT,  yoACCx,'.-m');                leg = [leg 'ACCx'];
                 plot((0:length(yoACCy)-1)/FsOUT,  yoACCy,'o-m');                leg = [leg 'ACCy'];
                 plot((0:length(yoACCz)-1)/FsOUT,  yoACCz,'x-m');                leg = [leg 'ACCz'];  end
title('Aligned output block processing');legend(leg);



FsECG




