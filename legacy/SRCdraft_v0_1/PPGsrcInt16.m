function Hd = PPGsrcInt16
%PPGSRCINT16 Returns a discrete-time filter object.

% Generated by MATLAB(R) 7.14 and the DSP System Toolbox 8.2.
% Generated on: 03-Mar-2014 16:55:34

N     = 383;    % Order
Fpass = 30;     % Passband Frequency
Fstop = 50;     % Stopband Frequency
Astop = 122.4;  % Stopband Attenuation (dB)
Fs    = 1600;   % Sampling Frequency

h = fdesign.interpolator(16, 'Lowpass', 'n,fp,fst,ast', N, Fpass, Fstop, Astop, Fs);
Hd = design(h, 'equiripple');

% Old filter
% % Generated by MATLAB(R) 7.14 and the DSP System Toolbox 8.2.
% % Generated on: 25-Feb-2014 17:46:16
% 
% N     = 383;   % Order
% Fpass = 30;    % Passband Frequency
% Fstop = 50;    % Stopband Frequency
% Astop = 120;   % Stopband Attenuation (dB)
% Fs    = 1600;  % Sampling Frequency
% 
% h = fdesign.interpolator(16, 'Lowpass', 'n,fp,fst,ast', N, Fpass, Fstop, Astop, Fs);
% Hd = design(h, 'equiripple');