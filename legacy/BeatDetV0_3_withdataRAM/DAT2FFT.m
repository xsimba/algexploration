%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Leadonoffdet.m>
% Content   : Convert data into fft, calculate the peak freq and the height
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
% 
function [datfft,datfft_p,datfft_m,datfft_top,f,stpeak_top,dat_fft] =DAT2FFT(dat2fft,Fs) 


% for p = 1:1:size(dat2fft,1)
    dat2fft=dat2fft-mean(dat2fft);
T = 1/Fs;          % Sample time
L = length(dat2fft);   % Length of signal
NFFT = 2^nextpow2(L);
dat_fft = fft(dat2fft,NFFT)/L; %fft of the acc channels
f = Fs/2*linspace(0,1,NFFT/2+1); %corresponding frequencies

datfft = 2*abs(dat_fft(1:NFFT/2+1)); 
datfft_p= max(datfft); %height of the peak
datfft_m = find(datfft == datfft_p); % determine the max in the FFT spectrum
datfft_top = f(datfft_m); % peak of fft per channel

ind_minf = find(f==0.5);
ind_maxf = find(f==4)+1;
ind1stpeak = max(datfft(ind_minf:ind_maxf));
% freq1stpeak_m(p) = find(datfft(p,ind_minf:ind_maxf) == ind1stpeak(p)); % determine the max in the FFT spectrum
stpeak_top =1% f(p,(freq1stpeak_m(p)+ind_minf(p)-1));

end