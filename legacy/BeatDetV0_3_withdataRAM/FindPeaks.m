function [FoundPeaks, PeakIndici, State] = FindPeaks(InputSamples, BlockSize, State, Params)
% Peak detection, C code in comments
% InputSamples    Block of input samples
% BlockSize       Number of input samples
% State           Structure of state variables: input and output, initialised on first call
% (SigType         Signal type, determines wavelet and processing options. 0:ECG, 1:PPG, 2:BioZ) maybe required

PeakIndici = [];

%Debug
if (1)
    State.Debug_FindPeaks_InputSamples    = InputSamples;
    State.Debug_FindPeaks_cmp             = zeros(1,BlockSize);
    State.Debug_FindPeaks_threshold       = zeros(1,BlockSize);
    State.Debug_FindPeaks_max             = zeros(1,BlockSize);
    State.Debug_FindPeaks_edge            = zeros(1,BlockSize);
    State.Debug_FindPeaks_holdoffcounter  = zeros(1,BlockSize);
    State.Debug_FindPeaks_maxindx         = zeros(1,BlockSize);
end


for i = 0:BlockSize-1
    j = i + 1; % Matlab indexing!!

    if (InputSamples(j) > State.cmp), 
        State.v = InputSamples(j) - State.cmp; 
        State.av = Params.attack * State.v; 
        State.cmp = State.cmp + State.av;
    else
        State.v = InputSamples(j) - State.cmp; 
        State.dv = Params.decay  * State.v; 
        State.cmp = State.cmp + State.dv;
    end

    threshold = (Params.gain * State.cmp) + State.offset; % T(n) = Gain * cmp + Offset

    if ((InputSamples(j) > threshold) && (State.holdoffcounter == 0)) % If we cross the threshold and we are not counting yet -> We have a rising edge
        State.holdoffcounter = Params.holdoffSamples;       % Set the window in which we will look for the peak
        State.risingedge = true;
        State.fallingedge = false;
    else                                        % If we are lower than the threshold or we are already counting
        State.risingedge = false; % No rising edge
        State.fallingedge = (State.holdoffcounter == 1); % If we reach the end of the counting period -> Falling edge
        State.holdoffcounter = max(State.holdoffcounter - 1,0);
    end

    if (State.holdoffcounter > 0) % If we are counting
        if (InputSamples(j) > State.max), 
            State.max = InputSamples(j); 
            State.maxindx = State.holdoffcounter; 
            end
    else
        if (State.fallingedge)                  % When we reach a falling edge, we determine the index where the maximum was
            peak_idx = (i - State.maxindx);
            PeakIndici = [PeakIndici peak_idx]; %#ok<AGROW>
        end
        State.max = 0;
    end

% Debug
if (1)
    State.Debug_FindPeaks_cmp(j)            = State.cmp;
    State.Debug_FindPeaks_threshold(j)      = threshold;
    State.Debug_FindPeaks_max(j)            = State.max;
    State.Debug_FindPeaks_edge(j)           = State.risingedge - State.fallingedge; % 0:no edge, 1:rising, -1:falling
    State.Debug_FindPeaks_holdoffcounter(j) = State.holdoffcounter;
    State.Debug_FindPeaks_maxindx(j)        = State.maxindx;
    
    if ((State.risingedge == 1) && (State.fallingedge == 1)), disp('Rising edge and falling edge true at same time!'); end
end

end % for i = 0:BlockSize-1
FoundPeaks = ~isempty(PeakIndici);
end % Of primary function


 
