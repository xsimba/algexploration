%
% EstimatePAT__single
%
% This script processes time-aligned, sample-rate converted, ECG and PPG
% traces with beat detection using 'findbeats.m'.  The pulse arrival times
% between detected beats are computed over a manually (exogenously) 
% selected region and a vector of pulse arrival times is produced.
%
% created 5/7/2014 asif.khalak@samsung.com
% based on 'BeatDetTest.m' code from IMEC

%
% clean up plots and workspace
%
close all; clc
clear ecgOutput ppgOutput
debug = 1;

%
% define constants
%
Fs = 128;                        % Sample Rate in samples/sec
FreqTS = 32768;                  % Timestamp clock frequency
BlockSize = round(Fs*0.5);       % 500 ms blocks

calc=1;
if (calc),
%
% Load heart signals from ECG and PPG, and manually select region of interest.  
% Before this step, the data is presumed to be interpolated, time-aligned, 
% and sample-rate converted.  The manual selection could be replaced with the
% confidence indicator, for example.
%
load('Data/PB_3.mat')
ECG   = ECG(22000:end-100);
PPG2R = PPG2R(22000:end-100);

%
% flip and scale PPG data
%
PPG2R = (PPG2R.*-1)+10*10^6;

%
%  ECG processing
%
disp('ECG test');

% parameter definitions for ECG model
Params.CWT_FIR        = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv');  % 0: Load ECG  CWT coefficients
Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
Params.gain           = 0.48799; % Scaling for peak tracking to threshold
Params.StdScale       = 1.00000; % Scaling for standard deviation to offset
Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
Params.holdoffSamples = round(100.0*Fs/1000); % Hold-off duration 100.0 ms
Params.GroupDelay     = 13; % Symmetric filter delay, 13 = (27-1)/2

% define wavelet filter
Morph        = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv'); % Load CWT coefficients
%BeatsPerMin = 102.37 + 61.234*3*0; % Heart rate .. dont understand the 2nd term (set to zero)
BeatsPerMin  = 102.37; % Heart rate

% process ecg trace
ecgOutput = traceFiltPeak(Params, ECG, Morph, BeatsPerMin, Fs, FreqTS, BlockSize);

%
% PPG processing test
%
disp('PPG test.');

% parameter definition for ppg model
Params.CWT_FIR        = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv');  % 1: Load PPG  CWT coefficients
Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
Params.gain           = 0.48799; % Scaling for peak tracking to threshold
Params.StdScale       = 1.00000; % Scaling for standard deviation to offset
Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
Params.holdoffSamples = round(200.0*Fs/1000); % Hold-off duration 200.0 ms
Params.GroupDelay     = 16; % Asymmetric filter delay

% define wavelet filter
cwt = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv'); % Load CWT coefficients
Morph = cumsum(fliplr(cwt)); % Reverse coefficients to get positive time and integrate
BeatsPerMin = 61.234; % Heart rate

% process ppg trace
ppgOutput = traceFiltPeak(Params, PPG2R, Morph, BeatsPerMin, Fs, FreqTS, BlockSize);

%
% Select portions for Pulse Arrival Time estimates
%
ecgPInd = 116:128;
ppgPInd = (-48:-37) + length(ppgOutput.BeatAmp); %from the end

assert(length(ecgPInd) == length(ppgPInd)+1)

trace_ecg         = ecgOutput.Samp;
trace_ecg_filt    = ecgOutput.FiltSamp;
trace_ppg         = ppgOutput.Samp;
trace_ppg_filt    = ppgOutput.FiltSamp;
trace_times       = (0:length(trace_ecg)-1)/Fs;

ecg_beat_times    = ecgOutput.AllBeatTimeStamps(ecgPInd) / FreqTS;
ecg_beat_amp      = ecgOutput.BeatAmp(ecgPInd);
ecg_beat_amp_filt = ecgOutput.BeatAmpFilt(ecgPInd);
ppg_beat_times    = ppgOutput.AllBeatTimeStamps(ppgPInd) / FreqTS; 
ppg_beat_amp      = ppgOutput.BeatAmp(ppgPInd);
ppg_beat_amp_filt = ppgOutput.BeatAmpFilt(ppgPInd);

ECGPAT = ecgOutput.AllBeatTimeStamps(ecgPInd)/FreqTS;
% manually clip point 8 since there is no corresponding PPG pulse detected
ECGPAT(8)=[];
PPGPAT = ppgOutput.AllBeatTimeStamps(ppgPInd)/FreqTS;

PAT = abs(ECGPAT(:)-PPGPAT(:));

end


if (debug),

%
% plot 'raw' ECG data, Successive Peaktime Deltas, Instantaneous Heart Rate
% Raw + Peaks
%

figure(1);
time = (0:length(ecgOutput.Samp)-1)/Fs;
plot(time,ecgOutput.Samp); 
xlabel('Time (s)'); 
title('ECG test data');

figure(2);
plot(diff(ecgOutput.AllBeatTimeStamps)/FreqTS);
title('ECG R-R intervals');

figure(3);
plot(60*FreqTS ./ diff(ecgOutput.AllBeatTimeStamps));
title('ECG Heart rate');
ylim([30 240]);

figure(4);
hold on;
plot(time,ecgOutput.Samp,'b');
plot(ecgOutput.AllBeatTimeStamps/FreqTS,ecgOutput.BeatAmp,'or');

figure(5); 
FindPeaks = ecgOutput.Peaks;
hold on;
plot(ecgOutput.Samp, 'k');
plot(FindPeaks.InputSamples, 'b');
plot(FindPeaks.threshold, 'r');
plot(FindPeaks.max, 'm');
plot(FindPeaks.holdoffcounter/10, '*k');
plot(FindPeaks.maxindx/10, 'ob');
plot(FindPeaks.InputSamples./(FindPeaks.edge ==  1), '^b');
plot(FindPeaks.InputSamples./(FindPeaks.edge == -1), 'vb');
legend('Input','|cwt|','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); 
title('ECG debug'); 

figure(6);
time = (0:length(ppgOutput.Samp)-1)/Fs;
plot(time,ppgOutput.Samp); 
title('PPG test data');

figure(7); hold on;
plot(0:length(ppgOutput.Samp)-1,8*ppgOutput.Samp,'o-b');
plot(0:length(ppgOutput.Samp)-1,filter(fliplr(cwt),1,ppgOutput.Samp),'o-g');
plot((0:length(ppgOutput.Samp)-2)+0.5,50*diff(ppgOutput.Samp),'o-m');
title('PPG data after CWT');legend('Input','RevFilt','Diff');
xlabel('Samples');

figure(8);
plot(diff(ppgOutput.AllBeatTimeStamps)/FreqTS);
title('PPG R-R intervals');

figure(9);
plot(60*FreqTS./diff(ppgOutput.AllBeatTimeStamps));
title('PPG Heart rate');

figure(10); 
FindPeaks = ppgOutput.Peaks;
hold on;
plot(ppgOutput.Samp, 'k');
plot(FindPeaks.InputSamples, 'b');
plot(FindPeaks.threshold, 'r');
plot(FindPeaks.max, 'm');
plot(FindPeaks.holdoffcounter/10, '*k');
plot(FindPeaks.maxindx/10, 'ob');
plot(FindPeaks.InputSamples./(FindPeaks.edge ==  1), '^b');
plot(FindPeaks.InputSamples./(FindPeaks.edge == -1), 'vb');

legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('PPG debug');

figure(11);
hold on;
plot(time,ppgOutput.Samp,'b');
BeatAmp = interp1(time,ppgOutput.Samp,ppgOutput.AllBeatTimeStamps/FreqTS);
plot(ppgOutput.AllBeatTimeStamps/FreqTS,ppgOutput.BeatAmp,'or');

end

%
% plot ECG/PPG to show goodness of fit of peak time estimates
% raw outputs
%

figure(12);
hold on;
subplot(2,1,1);
plot(trace_times, trace_ecg, 'b');
hold on;
plot(ecg_beat_times, ecg_beat_amp, 'or');

subplot(2,1,2); 
plot(trace_times,trace_ppg,'b');
hold on;
plot(ppg_beat_times, ppg_beat_amp,'or');

%
% plot ECG/PPG to show goodness of fit of peak time estimates
% filtered outputs
%
figure(13);
hold on;
subplot(2,1,1);
plot(trace_times, trace_ecg_filt, 'b');
 hold on;
plot(ecg_beat_times, ecg_beat_amp_filt, 'or');

subplot(2,1,2); 
plot(trace_times,trace_ppg_filt,'b');
hold on;
plot(ppg_beat_times, ppg_beat_amp_filt,'or');
