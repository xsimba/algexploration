% Test for beat detector
 
try
% close (1:11);  % should get rid of all of the standard plots but leave the PAT stuff
 close all;  % should get rid of all of the standard plots but leave the PAT stuff
catch ME
end
clear all
%clc
filtVersion = 'May16'; % legacy, May13, May16
testList = {'ECG', 'PPG', 'PAT'}; % {'ECG', 'PPG', 'PAT'}
detrendFlag = 'off';
savePlotFlag = 'off';
beatThresh = 0.4;   % double beat rejection threshold in seconds
doubleBeatFiltFlag = 'on';
ppgChannelFlag = 'green2';
ppgValueFlip =  'no flip';
ppgPlotFlip = 'no flip';
patTimeRange = [5 450];
datasetLabel = 'RestStudy10.2';

% recordings root
root_dir = 'C:\Users\asif.khalak\Documents\work\projects\Simba_EDA\';

if (strcmp(datasetLabel, 'Ram5.5.3')),
    load('Data/PB_3.mat');
elseif (strcmp(datasetLabel, 'RestStudy2.1')),
    load([root_dir,'Rest Recordings - Reference Simba Data\Raw data\SRC Data\Participant_2_wb1_lon.mat']);
elseif (strcmp(datasetLabel, 'RestStudy6.1')),
    load([root_dir,'Rest Recordings - Reference Simba Data\Raw data\SRC Data\Participant_6_wb1_lon.mat']);
elseif (strcmp(datasetLabel, 'RestStudy6.2')),
    load([root_dir,'Rest Recordings - Reference Simba Data\Raw data\SRC Data\Participant_6_wb2_lon.mat']);
elseif (strcmp(datasetLabel, 'RestStudy10.1')),
    load([root_dir,'Rest Recordings - Reference Simba Data\Raw data\SRC Data\Participant_10_wb1_lon.mat']);
elseif (strcmp(datasetLabel, 'RestStudy10.2')),
    load([root_dir,'Rest Recordings - Reference Simba Data\Raw data\SRC Data\Participant_10_wb2_lon.mat']);
end

warning('off', 'MATLAB:interp1:UsePCHIP');
studyTitle = [filtVersion, 'filt', ... %, detrend ',detrendFlag, ...
    ', ', ppgValueFlip, ' Value, ', ppgPlotFlip, ' Visual, ', ppgChannelFlag, ', ', datasetLabel];

if strcmp(ppgPlotFlip, 'flip'),
    ppgPlotDir = -1;
else
    ppgPlotDir = 1;
end

Fs = 128;
FreqTS = 32768; % Timestamp clock frequency
BlockSize = round(Fs*0.5); % 500 ms blocks
TestDur = 300.0; % Test duration (s)
beatThresh= beatThresh * FreqTS;

%% ECG test
if (strmatch('ECG', testList, 'exact')),
    Nsamp = ceil(length(ECG));
    Nblocks = floor(Nsamp/BlockSize);
    disp(' ');
    disp('ECG test.');
    BeatsPerMin = 102.37 + 61.234*3*0; % Heart rate
    SigType = 0; % 0:ECG, 1:PPG, 2:BioZ
    Params.CWT_FIR        = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv');  % 0: Load ECG  CWT coefficients
    Params.attack         = 1 - exp(-1000/(1.8055*Fs)); % 1.7000 ms
    Params.decay          = 1 - exp(-1000/(466.34*Fs)); % 1000.0 ms -- 37% after 1 sec.
    Params.gain           = 0.55; % Scaling for peak tracking to threshold
    Params.StdScale       = 0.75; % Scaling for standard deviation to offset
    Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
    Params.OffUpdateFast  = 0.5; %1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
    Params.OffUpdateSlow  = 0.1; %1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
    Params.holdoffSamples = 10; %round(100.0*Fs/1000); % Hold-off duration 100.0 ms
    Params.GroupDelay     = 13; % Symmetric filter delay, 13 = (27-1)/2
    Morph = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv'); % Load CWT coefficients
    % Samp = randn(1,Nsamp)*0.01*0; % Noise amplitude
    Samp = ECG;
    Nbeats = floor((length(Samp) - length(Morph))*BeatsPerMin/(Fs*60));
    
    % Off-grid composition
    MorphUS10 = resample([0 Morph 0], 10, 1); % 10x upsample
    for n = 1:Nbeats-1
        idx = n*Fs*60/BeatsPerMin + 1;
        %disp(['Beat timestamps: ',num2str((idx-1)*FreqTS/Fs)]);
        idxI = round(idx);
        idxF = idx - idxI;
        Samp(idxI:idxI+length(Morph)-1) = Samp(idxI:idxI+length(Morph)-1) + interp1(MorphUS10,(10:10:length(Morph)*10+1)-idxF*10,'cubic');
    end
    
    figure(1);plot((0:length(Samp)-1)/Fs,Samp); xlabel('Time (s)'); title('ECG test data');
    
    % Debug
    FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
    FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
    FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
    FindPeaks_max             = zeros(1,BlockSize*Nblocks);
    FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
    FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
    FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);
    
    BeatTimestamps2=zeros(Nblocks, 3);
    State = []; % Initial (empty) state
    AllBeatTimeStamps =[];
    for Blk = 1:Nblocks-1
        Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
        InputSamples = Samp(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract samples for this block
        [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigType);
        %     BeatTimestamps2(Blk)=BeatTimestamps;
        % Concatenate debug data
        FindPeaks_InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
        FindPeaks_cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
        FindPeaks_threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
        FindPeaks_max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
        FindPeaks_edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
        FindPeaks_holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
        FindPeaks_maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
        
        %disp(['ECG BeatTimestamps: ',num2str(BeatTimestamps)]);
        AllBeatTimeStamps = [AllBeatTimeStamps BeatTimestamps]; %#ok<AGROW>
    end
    disp(' ');
    figure(2);plot(diff(AllBeatTimeStamps)/32768);title('ECG R-R intervals');
    figure(3);plot(60*32768./diff(AllBeatTimeStamps));title('ECG Heart rate');ylim([30 240]);
    
    figure(4);hold on;
    plot((0:Nsamp-1)/Fs,Samp,'b');
    BeatAmp = interp1((0:Nsamp-1)/Fs,Samp,AllBeatTimeStamps/FreqTS);
    plot(AllBeatTimeStamps/FreqTS,BeatAmp,'or');
    
    
    EAllBeatTimeStamps = AllBeatTimeStamps;
    EBeatAmp=BeatAmp;
    ESamp=Samp;
    
    % Debug plots
    figure(4); hold on;
    plot(Samp, 'k');
    plot(FindPeaks_InputSamples, 'b');
    plot(FindPeaks_threshold, 'r');
    plot(FindPeaks_max, 'm');
    plot(FindPeaks_holdoffcounter/10, '*k');
    plot(FindPeaks_maxindx/10, 'ob');
    
    plot(FindPeaks_InputSamples./(FindPeaks_edge ==  1), '^b');
    plot(FindPeaks_InputSamples./(FindPeaks_edge == -1), 'vb');
    
    legend('Input','|cwt|','Thres','max','HOcnt','MaxIn');
    xlabel('Samples'); title('ECG debug');
end


%% PPG test
if (strmatch('PPG', testList, 'exact')),
    disp(' ');
    disp('PPG test.');
    BeatsPerMin = 35.678*0 + 61.234; % Heart rate
    SigType = 1; % 0:ECG, 1:PPG, 2:BioZ
    if strcmp(filtVersion, 'legacy'),
        Params.CWT_FIR        = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv');  % 1: Load PPG  CWT coefficients
        Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
        Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
        Params.gain           = 0.48799; % Scaling for peak tracking to threshold
        Params.StdScale       = 1.00000; % Scaling for standard deviation to offset
        Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
        Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
        Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
        Params.holdoffSamples = round(200.0*Fs/1000); % Hold-off duration 200.0 ms
        Params.GroupDelay     = 16; % Asymmetric filter delay
    elseif    strcmp(filtVersion, 'May13'),
        Params.CWT_FIR        = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv');  % 1: Load PPG  CWT coefficients
        Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
        Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
        Params.gain           = 0.48799; % Scaling for peak tracking to threshold
        Params.StdScale       = 0.7; % Scaling for standard deviation to offset
        Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
        Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
        Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
        Params.holdoffSamples = round(200.0*Fs/1000); % Hold-off duration 200.0 ms
        Params.GroupDelay     = 16; % Asymmetric filter delay
    elseif strcmp(filtVersion, 'May16'),
        Params.CWT_FIR        = importdata('Wavelets/Coeffs/Wavelet_Coefficients_16May2014.csv');  % 1: Load PPG  CWT coefficients
        Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
        Params.decay          = 1 - exp(-1000/(450.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
        Params.gain           = 0.6; % Scaling for peak tracking to threshold
        Params.StdScale       = 0.7; % Scaling for standard deviation to offset
        Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
        Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
        Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
        Params.holdoffSamples = round(450.0*Fs/1000); % Hold-off duration 200.0 ms
        Params.GroupDelay     = 61; % Asymmetric filter delay
    end
    
    cwt = Params.CWT_FIR; % Load CWT coefficients
    
    Morph = cumsum(fliplr(cwt)); % Reverse coefficients to get positive time and integrate
    % Samp = 1.0 + randn(1,Nsamp)*0.03*0; % DC offset and noise amplitude
    
    if (strcmp( ppgChannelFlag, 'green2') || strcmp(ppgChannelFlag, 'green')),
        dataSeries = PPG2G;
    elseif (strcmp ( ppgChannelFlag, 'red2') || strcmp (ppgChannelFlag, 'red')),
        dataSeries = PPG2R;
    elseif (strcmp( ppgChannelFlag, 'green1')),
        dataSeries = PPG1G;
    elseif (strcmp ( ppgChannelFlag, 'red1')),
        dataSeries = PPG1R;
    else
        error('unknown PPG channel');
    end
    
    if (strcmp (ppgValueFlip, 'flip')),
        % flipped the dataSeries
        dataSeries = 1e7 - dataSeries;
    end
    
    if (strcmp(detrendFlag, 'off')),
        Samp=dataSeries;
    else
        %
        % two step detrending (coarse + fine) in a single pass
        %
        windowhlen = 128; % 2 sec
        order = 12; % 5th percentile
        runningfloor = zeros(size(dataSeries));
        
        windowhlen_2 = 38; % 0.6 sec.  needs to be smaller than 1/2 windowhlen
        order_2 = 2; % 2d percentile
        runningfloor_2 = zeros(size(dataSeries));
        
        detrend_coarse = zeros(size(dataSeries));
        detrend_fine   = zeros(size(dataSeries));
        
        for i = windowhlen:(length(dataSeries)-windowhlen),
            filtwindow1           = dataSeries(i-windowhlen+1:i+windowhlen);
            runningfloor(i)       = orderstat(filtwindow1, order);
            detrend_coarse(i)     = dataSeries(i) - runningfloor(i);
            if i > (windowhlen),
                filtwindow2       = detrend_coarse(i-2*windowhlen_2:i);
                runningfloor_2(i-windowhlen_2) = orderstat(filtwindow2, order_2);
                detrend_fine(i-windowhlen_2)   = detrend_coarse(i-windowhlen_2) - ...
                    runningfloor_2(i-windowhlen_2);
            end
        end
        
        Samp=detrend_fine;
    end
    
    
    Nbeats = floor((length(Samp) - length(Morph))*BeatsPerMin/(Fs*60));
    % for n = 1:2:Nbeats-1
    %     idx = round(n*Fs*60/BeatsPerMin) + 1;
    %     Samp(idx:idx+length(Morph)-1) = Samp(idx:idx+length(Morph)-1) + Morph;
    % end
    
    MorphUS10 = resample([0 Morph 0], 10, 1); % 10x upsample
    for n = 1:Nbeats-1
        idx = n*Fs*60/BeatsPerMin + 1;
        idxI = round(idx);
        idxF = idx - idxI;
        Samp(idxI:idxI+length(Morph)-1) = Samp(idxI:idxI+length(Morph)-1) + interp1(MorphUS10,(10:10:length(Morph)*10+1)-idxF*10,'cubic');
    end
    
    figure(5);plot((0:length(Samp)-1)/Fs,Samp); title('PPG test data');
    
    % Debugging...
    figure(6); hold on;
    plot(0:length(Samp)-1,8*Samp,'o-b');
    plot(0:length(Samp)-1,filter(fliplr(cwt),1,Samp),'o-g');
    plot((0:length(Samp)-2)+0.5,50*diff(Samp),'o-m');
    title('PPG data after CWT');legend('Input','RevFilt','Diff');
    xlabel('Samples');
    
    cmp = zeros(1,Nblocks); % Debug
    off = cmp;
    
    % Debug
    FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
    FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
    FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
    FindPeaks_max             = zeros(1,BlockSize*Nblocks);
    FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
    FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
    FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);
    
    State = []; % Initial (empty) state
    AllBeatTimeStamps =[];
    for Blk = 0:Nblocks-1%(BlockSize*8)%-1)
        Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
        InputSamples = Samp(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract samples for this block
        [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigType);
        
        % Concatenate debug data
        FindPeaks_InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
        FindPeaks_cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
        FindPeaks_threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
        FindPeaks_max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
        FindPeaks_edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
        FindPeaks_holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
        FindPeaks_maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
        
        %disp(['PPG BeatTimestamps: ',num2str(BeatTimestamps)]);
        AllBeatTimeStamps = [AllBeatTimeStamps BeatTimestamps]; %#ok<AGROW>
    end
    disp(' ');
    figure(7);plot(diff(AllBeatTimeStamps)/32768);title('PPG R-R intervals');
    figure(8);plot(60*32768./diff(AllBeatTimeStamps));title('PPG Heart rate');
    
    % Debug plots
    figure(9); hold on;
    plot(Samp, 'k');
    plot(FindPeaks_InputSamples, 'b');
    plot(FindPeaks_threshold, 'r');
    plot(FindPeaks_max, 'm');
    plot(FindPeaks_holdoffcounter/10, '*k');
    plot(FindPeaks_maxindx/10, 'ob');
    
    plot(FindPeaks_InputSamples./(FindPeaks_edge ==  1), '^b');
    plot(FindPeaks_InputSamples./(FindPeaks_edge == -1), 'vb');
    
    legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
    xlabel('Samples'); title('PPG debug');
    
    figure(10);hold on;
    Nsamp = length(Samp);
    plot((0:Nsamp-1)/Fs,Samp,'b');
    BeatAmp = interp1((0:Nsamp-1)/Fs,Samp,AllBeatTimeStamps/FreqTS);
    plot(AllBeatTimeStamps/FreqTS,BeatAmp,'or');
    
    %
    % store comparison data
    %
    SampPpg = Samp;
    BeatAmpPpg = BeatAmp;
    BeatTimePpg = AllBeatTimeStamps;
    
    if (strcmp(savePlotFlag, 'on')),
        if strcmp(filtVersion, 'legacy'),
            save nominal.mat SampPpg BeatAmpPpg BeatTimePpg
        elseif strcmp(filtVersion,'May13'),
            SampPpg2 = SampPpg; BeatAmpPpg2 = BeatAmpPpg; BeatTimePpg2 = BeatTimePpg;
            save nominalNew.mat SampPpg2 BeatAmpPpg2 BeatTimePpg2
        elseif strcmp(filtVersion,'May16'),
            SampPpg3 = SampPpg; BeatAmpPpg3 = BeatAmpPpg; BeatTimePpg3 = BeatTimePpg;
            save updateMay16.mat SampPpg3 BeatAmpPpg3 BeatTimePpg3
        end
    end
    
end

if(strmatch('PAT', testList, 'exact')),
    
    %
    % EAllBeatTimeStamps
    % EBeatAmp=BeatAmp
    % ESamp=Samp
    
    
%    patTimeRange = [250 280];
    ecgInd = find((EAllBeatTimeStamps > patTimeRange(1)*FreqTS) & (EAllBeatTimeStamps < patTimeRange(2)*FreqTS) );
    ppgInd = find((AllBeatTimeStamps > patTimeRange(1)*FreqTS) & (AllBeatTimeStamps < patTimeRange(2)*FreqTS) );

    %
    % double-beat filter
    %
    if strcmp(doubleBeatFiltFlag,'on'),
        %
        % This (19May2014) version of the double beat filter works as follows:
        % The beats are considered in time-order, and are compared with
        % other beats within the beat time threshold, T.  For each binary 
        % decision, the beat with the larger raw amplitude is accepted, and 
        % others are rejected.  The beat is accepted if there is no other
        % beat with a larger raw amplitude in time T after it.
        %    
        lastBeat=1;
        for i = 2:length(ecgInd),
            if (EAllBeatTimeStamps(ecgInd(i))-EAllBeatTimeStamps(ecgInd(lastBeat))<beatThresh),
                if (EBeatAmp(ecgInd(i)) >= EBeatAmp(ecgInd(lastBeat))),
                ecgInd(lastBeat) = -1;
                lastBeat = i;
                else
                ecgInd(i) = -1;
                end
            else
                lastBeat = i;
            end
        end
        ecgInd(ecgInd<0) = [];


        lastBeat =1;
        for i = 2:length(ppgInd),
            if (AllBeatTimeStamps(ppgInd(i))-AllBeatTimeStamps(ppgInd(lastBeat))<beatThresh),
                if (BeatAmp(ppgInd(i)) >= BeatAmp(ppgInd(lastBeat))),
                ppgInd(lastBeat) = -1;
                lastBeat = i;
                else
                ppgInd(i) = -1;
                end
            else
                lastBeat = i;
            end
        end
        ppgInd(ppgInd<0) = [];
    end
    
  
    figure(11);
    clf;
    hold on;
    subplot(2,1,1);
    plot((0:length(ESamp)-1)/Fs,ESamp,'b');
    hold on; 
    plot(EAllBeatTimeStamps(ecgInd)/FreqTS,EBeatAmp(ecgInd),'or');   
    subplot(2,1,2); 
    plot((0:length(Samp)-1)/Fs,ppgPlotDir*Samp,'b');
    hold on;
    plot(AllBeatTimeStamps(ppgInd)/FreqTS,ppgPlotDir*BeatAmp(ppgInd),'or');
    linkaxes([subplot(211) subplot(212)], 'x');
    
    
    ECGPAT = EAllBeatTimeStamps(ecgInd)/FreqTS;
    PPGPAT = AllBeatTimeStamps(ppgInd)/FreqTS;

    PAT = [];
    PATtime = [];
    for i = 1:length(ECGPAT)-1,
        patInd = find(PPGPAT > ECGPAT(i) & PPGPAT < ECGPAT(i+1));
        if (length(patInd)>0),
        PAT = [PAT(:)' PPGPAT(patInd(1))-ECGPAT(i)];
        PATtime = [PATtime(:)' ECGPAT(i)];
        end
    end
    

    %
    % not numbered to allow comparisons with multiple runs
    %
    ESamp = ESamp(1:end-200);
    Samp = Samp(1:end-200);
    figure;
    clf;
    hold on;
    subplot(3,1,1);
    plot((0:length(ESamp)-1)/Fs,ESamp,'b');
    hold on; 
    plot(EAllBeatTimeStamps(ecgInd)/FreqTS,EBeatAmp(ecgInd),'or');
    set(gca, 'XLim', patTimeRange);
    ylabel('ECG');
    title(studyTitle);
    
    subplot(3,1,2); 
    plot((0:length(Samp)-1)/Fs,ppgPlotDir*Samp,'b');
    hold on;
    plot(AllBeatTimeStamps(ppgInd)/FreqTS,ppgPlotDir*BeatAmp(ppgInd),'or');
    set(gca, 'XLim', patTimeRange);
    set(gca, 'YLim', [min(ppgPlotDir*Samp(2:end)) max(ppgPlotDir*Samp(2:end))]);
    ylabel('PPG')

    subplot(313);
    plot(PATtime, PAT, 'bx', 'MarkerSize', 10, 'LineWidth', 2);
    set(gca, 'XLim', patTimeRange);
    set(gca, 'YLim', [0 1]);
    xlabel('time [s]')
    ylabel('PAT [s]')

    linkaxes([subplot(311) subplot(312) subplot(313)], 'x');

    
    %PAT = abs(ECGPAT-PPGPAT)
    %
    %ECGPAT = EAllBeatTimeStamps(116:129)/FreqTS;
    %ECGPAT(8)=[];
    %PPGPAT = AllBeatTimeStamps(length(BeatAmp)-48:end-36)/FreqTS;
    %PAT = abs(ECGPAT-PPGPAT)

    %
    % not numbered to allow comparisons with multiple runs
    %
    figure(121);
    clf;
    hold on;
    title(studyTitle);
    plot((0:length(Samp)-1)/Fs,ppgPlotDir*Samp,ppgChannelFlag(1));
    hold on;
    plot(AllBeatTimeStamps(ppgInd)/FreqTS,ppgPlotDir*BeatAmp(ppgInd),'ko');
    set(gca, 'XLim', patTimeRange);
    set(gca, 'YLim', [min(ppgPlotDir*Samp(2:end)) max(ppgPlotDir*Samp(2:end))]);
    ylabel('PPG')

    xlabel('time [s]')

    %PAT = abs(ECGPAT-PPGPAT)
    %
    %ECGPAT = EAllBeatTimeStamps(116:129)/FreqTS;
    %ECGPAT(8)=[];
    %PPGPAT = AllBeatTimeStamps(length(BeatAmp)-48:end-36)/FreqTS;
    %PAT = abs(ECGPAT-PPGPAT)
    
    figure;
    ind = find(PAT<0.8);
    PAT = PAT(ind);
    PATtime = PATtime(ind);
    clf
    subplot(211);
    hist(PAT, 50);
    xlabel('Pulse Arrival Time [s]');
    title(studyTitle);

    subplot(212);
    plot(PATtime, PAT, '.');
    xlabel('Acquisition Time [s]'); ylabel('Pulse Arrival Time [s]');

    figure;
    hdiff_PPG = diff(AllBeatTimeStamps(ppgInd)/FreqTS);
    hdiff_ECG = diff(EAllBeatTimeStamps(ecgInd)/FreqTS);

    subplot(211);
    [nPPG, xPPG] = hist(hdiff_PPG, [0:0.05:1.5]);
    [nECG, xECG] = hist(hdiff_ECG, [0:0.05:1.5]);
    h = bar(xECG, nECG, 'hist'); 
    hold on;
    h2 = bar(xPPG, nPPG, 'hist');
    set (h, 'facecolor', 'r', 'facealpha', 0.25);
    set (h2, 'facecolor', 'b', 'facealpha', 0.25);
    l = legend({'ECG', 'PPG'});
    lchild = allchild(l);
    lpatch = find(strcmp(get(lchild, 'Type'),'patch'));
    set(lchild(lpatch), 'facealpha', 0.25);
    title(studyTitle);

    xlabel('Inter-beat Time[s]');
    
    subplot(212);
    plot(EAllBeatTimeStamps(ecgInd(1:end-1))/FreqTS, hdiff_ECG,'or');
    hold on;
    plot(AllBeatTimeStamps(ppgInd(1:end-1))/FreqTS, hdiff_PPG, 'xb'); 
    xlabel('time [s]');
    ylabel('Inter-beat time [s]');
    set(gca, 'YLim', [0 1.5]);
    
end

