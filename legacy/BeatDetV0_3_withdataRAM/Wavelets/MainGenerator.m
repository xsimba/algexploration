
% Generate wavelet coefficients for ECG, PPG and BioZ
% Refer to xls for specifications - fill in xls first!

% The function Coef5060quant may take some time so only enable the generators that are required to be updated!

close all
clear all
clc

%% ECG #1: Mexican hat, Fstd = 14.30 Hz, Fs = 512 Hz, Ncoef = 105 (odd for integer sample delay)
%          No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 512;
    wav = MexicanHatsWaveletGen(14.30, Fs, 105, 2, 0.00);
    Coef = Coef5060quant(Fs, wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/ECGwavelet1_512HzSupp.csv', Coef, 'newline','pc', 'precision',8);
end


%% ECG #2: Mexican hat, Fstd = 14.30 Hz, Fs = 128 Hz, Ncoef = 27 (odd for integer sample delay)
%          No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    wav = MexicanHatsWaveletGen(14.30, Fs, 27, 2, 0.00);
    Coef = Coef5060quant(Fs, wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/ECGwavelet2_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
end


%% PPG #1/2: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 100 Hz
%            No quantisation, StopFreq = 49.5 Hz, 50 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 100;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/PPGwavelet1_100Hz.csv', Wav, 'newline','pc', 'precision',8);
    dlmwrite('Coeffs/PPGwavelet2_100HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwaveletX_100Hz: delay = ',num2str(dl),' samples.']);
end


%% PPG #3/4: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 128 Hz
%            No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/PPGwavelet3_128Hz.csv', Wav, 'newline','pc', 'precision',8);
    dlmwrite('Coeffs/PPGwavelet4_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwaveletX_128Hz: delay = ',num2str(dl),' samples.']);
end


%% PPG #5: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 50 Hz
%          No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (1)
    Fs = 50;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
%     Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/PPGwavelet5_50Hz.csv', Wav, 'newline','pc', 'precision',8);
    disp(['PPGwaveletX_50Hz: delay = ',num2str(dl),' samples.']);
end


%% BioZ #1: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 2.500, Fs = 1024 Hz
%           No quantisation, StopFreq = 49.5 Hz, 50 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 1024;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 2.500, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/BioZwavelet1_1024HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['BioZwavelet1_1024HzSupp: delay = ',num2str(dl),' samples.']);
end


%% BioZ #2: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 2.500, Fs = 128 Hz
%           No quantisation, StopFreq = 49.5 Hz, 50 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 2.500, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/BioZwavelet2_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['BioZwavelet2_128HzSupp: delay = ',num2str(dl),' samples.']);
end