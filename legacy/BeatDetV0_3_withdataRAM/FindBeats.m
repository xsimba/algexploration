function [FoundBeats, BeatTimestamps, State, FiltSamples] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigType)
% Beat detection, C code in comments
% InputSamples    Block of input samples
% BlockSize       Number of input samples
% Timestamp       Timestamp of first sample in block (32768 Hz ticks)
% FoundBeats      Boolean flag to indicate if any beats were found in current block
% BeatTimestamps  List of timestamps of beats
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters
% SigType         Signal type, determines wavelet and processing options. 0:ECG, 1:PPG, 2:BioZ

% Assumptions: sample rate, fs = 128 Hz

if (isempty(State)) % Populate state: CWT state and from PeakDetect.cpp
    State.cwt = ones(length(Params.CWT_FIR)-1,1)*InputSamples(1); % Initial state of CWT, constant level of first sample
    State.cmp = 0;
    State.v = 0;
    State.av = 0;
    State.dv = 0;
    State.max = 0;
    State.maxindx = 0;
    State.fallingedge = 0; % false
    State.risingedge = 0; % false
    State.holdoffcounter = 0;
    State.PrevInputSamples = ones(1,BlockSize)*InputSamples(1);
    State.PrevBlockSize = BlockSize;
    State.Count = 0;
    State.offset = 0;
end


BeatTimestamps = [];
[CWTsamples,State.cwt] = filter(fliplr(Params.CWT_FIR),1,InputSamples,State.cwt); % Perform filter (reverse order for Matlab to match C impl.)

if     (SigType == 0), 
    aCWTsamples = abs(CWTsamples);             % 0: ECG use abs function
elseif (SigType == 1), 
    aCWTsamples = CWTsamples.*(CWTsamples>0);  % 1: PPG use clamp function
else                   
    aCWTsamples = abs(CWTsamples);             % 2: BioZ use abs function
end

% figure;plot(aCWTsamples);
% Use standard deviation to update offset

ss = std(aCWTsamples)*Params.StdScale;
if (State.Count < Params.OffUpdateFblk), 
    State.offset = State.offset*(1-Params.OffUpdateFast) + ss*Params.OffUpdateFast; % Fast update for first OffUpdateFblk sample blocks
else                                     
    State.offset = State.offset*(1-Params.OffUpdateSlow) + ss*Params.OffUpdateSlow; % Slow update for rest
end

[FoundPeaks, PeakIndexList, State] = FindPeaks(aCWTsamples, BlockSize, State, Params);
if (FoundPeaks)
    InterpolatedIndexList = QuadInterpolatePeaks(aCWTsamples, State.PrevInputSamples, State.PrevBlockSize, PeakIndexList);
    for n = 1:length(PeakIndexList)
        PeakTimestamp = Timestamp + (InterpolatedIndexList(n) - Params.GroupDelay) * 32768.0 / 128.0;
        BeatTimestamps = [BeatTimestamps PeakTimestamp]; %#ok<AGROW>
    end
end

%State.PrevInputSamples = InputSamples;
State.PrevInputSamples = aCWTsamples;
State.PrevBlockSize = BlockSize;

State.Count = State.Count + 1;
FiltSamples = aCWTsamples; 
FoundBeats  = FoundPeaks;
