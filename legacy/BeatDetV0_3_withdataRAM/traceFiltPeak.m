function traceOutput = traceFiltPeak(Params, Samp, Morph, BeatsPerMin, Fs, FreqTS, BlockSize, verbose);
%
% traceFiltPeak processes the traces, filters them for denoising and detrending,
% and performs peak identification.
%

if ~exist('verbose'),
	verbose = 0;
end

MorphUS10    = resample([0 Morph 0], 10, 1); % 10x upsample
Nbeats = floor((length(Samp) - length(Morph))*BeatsPerMin/(Fs*60));

%
% Off-grid composition.  What is this?
%
for n = 1:Nbeats-1
    idx = n*Fs*60/BeatsPerMin + 1;
	if (verbose),
    	disp(['Beat timestamps: ',num2str((idx-1)*FreqTS/Fs)]);
	end
    idxI = round(idx);
    idxF = idx - idxI;
    Samp(idxI:idxI+length(Morph)-1) = Samp(idxI:idxI+length(Morph)-1) + interp1(MorphUS10,(10:10:length(Morph)*10+1)-idxF*10,'pchip');
end

%
% Pack into Ecg structure
% 
EcgParams = Params;
EcgBeatsPerMin = BeatsPerMin;

%
% Initialize debug output 
%
Nsamp = ceil(length(Samp));
Nblocks = floor(ceil(length(Samp))/BlockSize);
FindPeaks.InputSamples    = zeros(1,BlockSize*Nblocks);
FindPeaks.cmp             = zeros(1,BlockSize*Nblocks);
FindPeaks.threshold       = zeros(1,BlockSize*Nblocks);
FindPeaks.max             = zeros(1,BlockSize*Nblocks);
FindPeaks.edge            = zeros(1,BlockSize*Nblocks);
FindPeaks.holdoffcounter  = zeros(1,BlockSize*Nblocks);
FindPeaks.maxindx         = zeros(1,BlockSize*Nblocks);

%
% Run FindBeats algorithm over blocks
%
BeatTimestamps2 = zeros(Nblocks, 3);

State = []; % Initial (empty) state
AllBeatTimeStamps =[];
FiltSamp = [];
SigType = 0; % 0:ECG, 1:PPG, 2:BioZ
for Blk = 1:Nblocks-1
    Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
    InputSamples = Samp(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract samples for this block
    [FoundBeats, BeatTimestamps, State, OutputSamples] = ...
        FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigType);

    % Concatenate debug data
    FindPeaks.InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
    FindPeaks.cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
    FindPeaks.threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
    FindPeaks.max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
    FindPeaks.edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
    FindPeaks.holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
    FindPeaks.maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
    
	if (verbose),
    disp(['BeatTimestamps: ',num2str(BeatTimestamps)]);
	end
    AllBeatTimeStamps = [AllBeatTimeStamps BeatTimestamps]; %#ok<AGROW>
    FiltSamp = [FiltSamp OutputSamples];
end

%
%  Align the filtered and raw traces on the same timeline
%
timeOffset = Fs/2;                 % filtering introduces a 0.5 sample delay
timeDelay  = round(0.07*Fs);       % the filter seems to introduce a 7% delay
timeShift  = timeOffset-timeDelay; 
lengthDiff = length(Samp) - length(FiltSamp);
Samp       = Samp(timeShift:end-lengthDiff+timeShift-1);

assert(length(Samp) == length(FiltSamp))

timeGrid    = (0:length(Samp)-1)/Fs + timeShift/Fs;
BeatAmp     = interp1(timeGrid,Samp,AllBeatTimeStamps/FreqTS);
BeatAmpFilt = interp1(timeGrid,FiltSamp,AllBeatTimeStamps/FreqTS);

traceOutput.AllBeatTimeStamps = AllBeatTimeStamps - timeShift/Fs;
traceOutput.BeatAmp           = BeatAmp;
traceOutput.BeatAmpFilt       = BeatAmpFilt;
traceOutput.Samp              = Samp;
traceOutput.FiltSamp          = FiltSamp;
traceOutput.Peaks             = FindPeaks;

