% Test for beat detector

close all
clear all
clc
load('PB_3.mat')
% PPG2R = PPG2R(22000:end-100)
PPG2R=PPG2R;
% ECG = ECG(22000:end-100)

%take inverse
PPG2R = (PPG2R.*-1)+10*10^6;
Fs = 128;

FreqTS = 32768; % Timestamp clock frequency
BlockSize = round(Fs*0.5); % 500 ms blocks
TestDur = 300.0; % Test duration (s)
Nsamp = ceil(length(PPG2R));
Nblocks = floor(Nsamp/BlockSize);
threshold = 0.49;
trial = 6;

%% PPG test
disp(' ');
disp('PPG test.');
BeatsPerMin = 35.678*0 + 61.234; % Heart rate
SigType = 1; % 0:ECG, 1:PPG, 2:BioZ
Params.CWT_FIR        = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv');  % 1: Load PPG  CWT coefficients
Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
Params.gain           = threshold;%0.48%8799; % Scaling for peak tracking to threshold
Params.StdScale       = 1.00000; % Scaling for standard deviation to offset
Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
Params.holdoffSamples = round(200.0*Fs/1000); % Hold-off duration 200.0 ms
Params.GroupDelay     = 16; % Asymmetric filter delay
cwt = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv'); % Load CWT coefficients
Morph = cumsum(fliplr(cwt)); % Reverse coefficients to get positive time and integrate
% Samp = 1.0 + randn(1,Nsamp)*0.03*0; % DC offset and noise amplitude
Samp=PPG2R;
Nbeats = floor((length(Samp) - length(Morph))*BeatsPerMin/(Fs*60));
% for n = 1:2:Nbeats-1
%     idx = round(n*Fs*60/BeatsPerMin) + 1;
%     Samp(idx:idx+length(Morph)-1) = Samp(idx:idx+length(Morph)-1) + Morph;
% end

MorphUS10 = resample([0 Morph 0], 10, 1); % 10x upsample
for n = 1:Nbeats-1
    idx = n*Fs*60/BeatsPerMin + 1;
    idxI = round(idx);
    idxF = idx - idxI;
    Samp(idxI:idxI+length(Morph)-1) = Samp(idxI:idxI+length(Morph)-1) + interp1(MorphUS10,(10:10:length(Morph)*10+1)-idxF*10,'cubic');
end

% Samp = filter(fliplr(cwt),1,Samp);
figure;plot(Samp)

figure;plot((0:length(Samp)-1)/Fs,Samp); title('PPG test data');

% Debugging...
figure; hold on;
plot(0:length(Samp)-1,8*Samp,'o-b');
plot(0:length(Samp)-1,filter(fliplr(cwt),1,Samp),'o-g');
plot((0:length(Samp)-2)+0.5,50*diff(Samp),'o-m');
title('PPG data after CWT');legend('Input','RevFilt','Diff');
xlabel('Samples');

cmp = zeros(1,Nblocks); % Debug
off = cmp;

% Debug
FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
FindPeaks_max             = zeros(1,BlockSize*Nblocks);
FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);

State = []; % Initial (empty) state
AllBeatTimeStamps =[];


for Blk = 0:Nblocks-1%(BlockSize*8)%-1)
    Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
    InputSamples = Samp(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract samples for this block
    [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigType);
    
    % Concatenate debug data
    FindPeaks_InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
    FindPeaks_cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
    FindPeaks_threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
    FindPeaks_max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
    FindPeaks_edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
    FindPeaks_holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
    FindPeaks_maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
    
    disp(['PPG BeatTimestamps: ',num2str(BeatTimestamps)]);
    AllBeatTimeStamps = [AllBeatTimeStamps BeatTimestamps]; %#ok<AGROW>
end
disp(' ');
figure;plot(diff(AllBeatTimeStamps)/32768);title('PPG R-R intervals');
figure;plot(60*32768./diff(AllBeatTimeStamps));title('PPG Heart rate');

PPGRR = diff(AllBeatTimeStamps)/32768;
PPGHR= 60*32768./diff(AllBeatTimeStamps);
MPPGHR = mean(PPGHR)
SDPPGHR = std(PPGHR)
MPPGRR = mean(PPGRR)
SDPPGRR = std(PPGRR)

for rr = 2:1:length(PPGHR)
    if PPGHR(rr) > 2*PPGHR(rr-1)
        PPGHRcorr(rr)= 0;
    else
        PPGHRcorr(rr)= PPGHR(rr);
    end
end

PPGHRcorr2=PPGHRcorr;
PPGHRcorr2(PPGHRcorr2==0)=[];
figure;plot(PPGHRcorr);hold on;plot(PPGHR,'r');plot(PPGHRcorr2,'m')


% Samp = filter(fliplr(cwt),1,Samp);
[datfft,datfft_p,datfft_m,datfft_top,f,stpeak_top,dat_fft] =DAT2FFT(Samp,Fs);
figure;plot(f,datfft)

% Debug plots
figure; hold on;
plot(Samp, 'k');
plot(FindPeaks_InputSamples, 'b');
plot(FindPeaks_threshold, 'r');
plot(FindPeaks_max, 'm');
plot(FindPeaks_holdoffcounter/10, '*k');
plot(FindPeaks_maxindx/10, 'ob');

plot(FindPeaks_InputSamples./(FindPeaks_edge ==  1), '^b');
plot(FindPeaks_InputSamples./(FindPeaks_edge == -1), 'vb');

legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('PPG debug');

figure;hold on;
plot((0:Nsamp-1)/Fs,Samp,'b');
BeatAmp = interp1((0:Nsamp-1)/Fs,Samp,AllBeatTimeStamps/FreqTS);
plot(AllBeatTimeStamps/FreqTS,BeatAmp,'or');
% plot(AllBeatTimeStamps/FreqTS,PPGHR+mean(Samp),'g');


% % 
% % EAllBeatTimeStamps
% % EBeatAmp=BeatAmp
% % ESamp=Samp
% 
% figure;hold on;
% subplot(2,1,1);plot((0:Nsamp-1)/Fs,ESamp,'b');hold on; plot(EAllBeatTimeStamps(116:128)/FreqTS,EBeatAmp(116:128),'or');
% subplot(2,1,2); plot((0:Nsamp-1)/Fs,Samp,'b');hold on;plot(AllBeatTimeStamps(length(BeatAmp)-48:end-36)/FreqTS,BeatAmp(length(BeatAmp)-48:end-36),'or');
% 
% ECGPAT = EAllBeatTimeStamps(116:128)/FreqTS;
% PPGPAT = AllBeatTimeStamps(length(BeatAmp)-48:end-36)/FreqTS;
% PAT = abs(ECGPAT-PPGPAT)
% 
% ECGPAT = EAllBeatTimeStamps(116:129)/FreqTS;
% ECGPAT(8)=[];
% PPGPAT = AllBeatTimeStamps(length(BeatAmp)-48:end-36)/FreqTS;
% PAT = abs(ECGPAT-PPGPAT)
DatThVar = [trial, threshold,MPPGHR,SDPPGHR];
xlswrite('threshold',DatThVar,'sddata',['A' num2str(trial)])



Fs
