function [InterpolatedIndexList] = QuadInterpolatePeaks(InputSamples, PrevSamples, PrevBlockSize, PeakIndexList)
% Peak detection, C code in comments
% InputSamples            Block of input samples
% PrevSamples             Block of input samples from previous block
% PrevBlockSize           Number of input samples in previous block
% PeakIndexList           List of detected peaks
% InterpolatedIndexList   List of corrected peaks

AllSamples = [PrevSamples InputSamples]; % Concatenate previous and current input samples so that no boundary processing is required

PrevY = AllSamples(PeakIndexList+PrevBlockSize+1-1);
PeakY = AllSamples(PeakIndexList+PrevBlockSize+1+0);
NextY = AllSamples(PeakIndexList+PrevBlockSize+1+1);

a = (NextY + PrevY - 2*PeakY)*0.5;
b = (NextY - PrevY)*0.5;
f = ((abs(a) < abs(b)) | (a == 0)); % Flag bad values

a(f) = 1;
b(f) = 0;

Correction = -b./(2*a);

InterpolatedIndexList = PeakIndexList + Correction;

end

