function [multi_corrected_signals_arr, baseline_signals_arr, params_arr, graphic_data] = ...
                        fit_each_PPG_signal_to_sum_gaussians(timestamp_arr, signal_arr, ...
                        general_params, multichannel_params)

    %#codegen
    params_arr = [];
    
    num_channels = size(signal_arr,1);
    signal_length = size(signal_arr,2);
    
    relevant_channel_indexes_arr = find(multichannel_params.processed_channels_arr == 1);
    num_relevant_channels = length(relevant_channel_indexes_arr);
    
    %detect beats
    [~, systolic_foot_index_arr2, ~, ~] = get_PPG_beats_segmentation(timestamp_arr, signal_arr(general_params.segmentation_ch_index,:), general_params.fs,...
        general_params.min_heart_rate, general_params.max_heart_rate, 2, ...
        general_params.chunk_size_min, general_params.filter_lower_than_mean_values, ...
        general_params.correct_baseline_wandering, ...
        false, general_params.show_corrected_wandering_baseline, '');
    
   
   [~, systolic_foot_index_arr1, ~, ~] = get_PPG_beats_segmentation(timestamp_arr, signal_arr(general_params.segmentation_ch_index,:), general_params.fs,...
        general_params.min_heart_rate, general_params.max_heart_rate, 1, ...
        general_params.chunk_size_min, general_params.filter_lower_than_mean_values, ...
        general_params.correct_baseline_wandering, ...
        general_params.show_beat_segmentation, general_params.show_corrected_wandering_baseline, '');
   

    
    %presentation and intermediate variables
    
    num_beats = length(systolic_foot_index_arr2) - 1;
    
    all_mu_arr = nan*ones(num_channels, num_beats, multichannel_params.num_peaks);
    all_sigma_arr = nan*ones(num_channels, num_beats, multichannel_params.num_peaks);
    all_amp_arr = nan*ones(num_channels, num_beats, multichannel_params.num_peaks);
   
    x_offset_arr = nan*ones(num_channels, num_beats);
    beat_length_arr = nan*ones(1, num_beats);
    y_offset_arr = nan*ones(num_channels, num_beats);
    amp_coeff_correction_arr = nan*ones(num_channels, num_beats);
    

%     coder.varsize('x_offset_arr',[1 100],[0 1]);
    
    
    all_norm_beat_arr = nan*ones(num_channels, signal_length);
    all_comp_arr = nan*ones(num_channels, signal_length, multichannel_params.num_peaks);
    all_modeled_beat_arr = nan*ones(num_channels, signal_length); %fitted beats
   
    %the corrected signals for all channels based on merging ('ac' correction)
    
    all_waves_arr = nan*ones(num_channels, signal_length,multichannel_params.num_peaks);
    multi_corrected_signals_arr = nan*ones(size(signal_arr));
   
    
    channels_selected_for_merge_arr = zeros(num_channels, num_beats);
    
    baseline_corrected_signal_arr = nan*ones(num_channels, signal_length);
    baseline_signals_arr = nan*ones(num_channels, signal_length);
    
    fprintf('Checkpoint 1\n');

    for ch_index = 1:num_channels

        if (multichannel_params.processed_channels_arr(ch_index) == 0)
            continue;
        end
        
        %get baseline for each channel
        
        [~, ~, baseline_corrected_signal, baseline_signal] = get_PPG_beats_segmentation(timestamp_arr, signal_arr(ch_index, :), general_params.fs,...
                general_params.min_heart_rate, general_params.max_heart_rate, 1, ...
                general_params.chunk_size_min, general_params.filter_lower_than_mean_values, ...
                general_params.correct_baseline_wandering, ...
                false, general_params.show_corrected_wandering_baseline, '');

         baseline_corrected_signal_arr(ch_index, :) = baseline_corrected_signal;
         baseline_signals_arr(ch_index, :) = baseline_signal;
        
    end
    
    set(0,'DefaultFigureWindowStyle','docked'); 
    

    beats_begin_index_arr = zeros(1,num_beats);
    beats_end_index_arr = zeros(1,num_beats);
    
    for beat_index = 1:num_beats

        begin_index = systolic_foot_index_arr2(beat_index);
        end_index = systolic_foot_index_arr1(beat_index+1);
        
        beats_begin_index_arr(beat_index) = begin_index;
        beats_end_index_arr(beat_index) = end_index;
   
    end
    
    %---------------------------------------------
    fprintf('Checkpoint 2\n');
    show_beats_debug = false;
    if (show_beats_debug)
        debug_figure_handler = figure;
    end
          
    for beat_index = 1:num_beats
        if(mod(beat_index,100)==0)
            fprintf('Processing beat %d/%d..\n', beat_index, num_beats);
        end
        
        begin_index = beats_begin_index_arr(beat_index);
        end_index = beats_end_index_arr(beat_index);
        beat_length = end_index - begin_index + 1;

       
        x_offset_arr(ch_index, beat_index) = begin_index;
            
        if (beat_length <=1)
            continue;
        end

        for ch_index = 1:num_channels
            
            if (multichannel_params.processed_channels_arr(ch_index) == 0)
                continue;
            end

            beat = baseline_corrected_signal_arr(ch_index, begin_index:end_index);
            [x_arr, norm_beat, amp_coeff_correction, y_offset] = normalize_beat(beat, general_params.fs);
            
            
            amp_coeff_correction_arr(ch_index, beat_index) = amp_coeff_correction;
            y_offset_arr(ch_index, beat_index) = y_offset;
            
            
            [modeled_signal, mu_arr, amp_arr, sigma_arr, comp_arr] =  fit_PPG_beat_to_gaussian_model(x_arr, norm_beat, multichannel_params.num_peaks);

            channels_selected_for_merge_arr(ch_index,beat_index) = 1;
            
            all_mu_arr(ch_index, beat_index,:) = mu_arr;
            all_sigma_arr(ch_index, beat_index,:) = sigma_arr;
            all_amp_arr(ch_index, beat_index,:) = amp_arr;

            all_modeled_beat_arr(ch_index, begin_index:end_index) = modeled_signal;
            all_norm_beat_arr(ch_index,  begin_index:end_index) = norm_beat;
        
   
            
            for peak_index = 1:multichannel_params.num_peaks
                all_comp_arr(ch_index, begin_index:end_index, peak_index) = comp_arr(peak_index, :);
            end 
            
           
           
            multi_corrected_signals_arr(ch_index, begin_index:end_index) =  ...
                amp_coeff_correction_arr(ch_index, beat_index)*modeled_signal + y_offset_arr(ch_index, beat_index);
            
            for peak_index = 1:multichannel_params.num_peaks
                all_waves_arr(ch_index, begin_index:end_index, peak_index) = ...
                    amp_coeff_correction_arr(ch_index, beat_index)*all_comp_arr(ch_index, begin_index:end_index, peak_index) + y_offset_arr(ch_index, beat_index);
            end
         end
        
         
         %------------------FIGURES---------------------------------------------------------------------------
%         show_beats_debug = true;

        
         if (show_beats_debug)
            figure;
%              clf(debug_figure_handler);
             plot_index = 0;
             
             for ch_index = 1:num_channels

                if ( multichannel_params.processed_channels_arr(ch_index) == 0)
                    continue;
                end
                
                plot_index = plot_index + 1;
                
                subplot(length(relevant_channel_indexes_arr), 1, plot_index);
                hold on

                
                dc_arr = baseline_signals_arr(ch_index, begin_index:end_index); 
                
                plot(x_arr, signal_arr(ch_index,begin_index:end_index), 'color', [0 1 0]); 

                if (channels_selected_for_merge_arr(ch_index,beat_index))
                    
                    multi_signal = dc_arr + multi_corrected_signals_arr(ch_index, begin_index:end_index);
                    plot(x_arr, multi_signal, 'linewidth', 2, 'color', [255 215 0]/255);
%                     plot(aligned_x_arr, dc_arr, '--w');
                    for peak_index = 1:multichannel_params.num_peaks
                        
                        comp = dc_arr + all_waves_arr(ch_index,begin_index:end_index,peak_index);
                        plot(aligned_x_arr, comp, '--c');
                    end
                end
                
                axis tight
                
                title(sprintf('Channel %d', ch_index));
            
             end
             close all;
         end
        
         %-----------------------------------------------------------------------------------------------
    end
    
    fprintf('Checkpoint 3\n');
    
    for ch_index = 1:num_channels
        %after correcting the AC component, add the previously removed wandering baseline (dynamic DC component)
        ind = find(~isnan(multi_corrected_signals_arr(ch_index,:)));
        multi_corrected_signals_arr(ch_index,ind) =  multi_corrected_signals_arr(ch_index,ind) + baseline_signals_arr(ch_index,ind);

        ind = find(isnan(multi_corrected_signals_arr(ch_index,:)));
        multi_corrected_signals_arr(ch_index,ind) = signal_arr(ch_index, ind);
        
        for peak_index = 1:multichannel_params.num_peaks
            all_waves_arr(ch_index, :, peak_index) = all_waves_arr(ch_index, : , peak_index) +  baseline_signals_arr(ch_index,:);
        end
        
        
    end
    
    %general data
    graphic_data.timestamp_arr = timestamp_arr;
    graphic_data.signal_arr = signal_arr;
    graphic_data.relevant_channel_indexes_arr = relevant_channel_indexes_arr;
    
    %beat timestamps
    params_arr.beats_begin_index_arr = beats_begin_index_arr;
    params_arr.beats_end_index_arr = beats_end_index_arr;
   
    %beat segmentation data
    params_arr.x_offset_arr = x_offset_arr;
    params_arr.beat_length_arr = beat_length_arr;
    %normalization data
    params_arr.y_offset_arr = y_offset_arr;
    params_arr.amp_coeff_correction_arr = amp_coeff_correction_arr;
    %model results data
    params_arr.all_mu_arr = all_mu_arr;
    params_arr.all_sigma_arr = all_sigma_arr;
    params_arr.all_amp_arr = all_amp_arr;
    params_arr.all_fitted_beat_arr = all_modeled_beat_arr;
    
    
    %continous data representation 
    graphic_data.all_norm_beat_arr = all_norm_beat_arr;
    graphic_data.all_comp_arr = all_comp_arr;
    graphic_data.all_fitted_beat_arr = all_modeled_beat_arr;
    
    graphic_data.all_waves_arr = all_waves_arr;
    graphic_data.multi_corrected_signals_arr = multi_corrected_signals_arr;

    fprintf('Checkpoint 4\n');
end

