function [max_peaks_index_arr] = detect_local_maxima_points(signal, fs, max_rate, filter_lower_than_mean_values, ...
                                 chunk_size_min, show_results)

    %#codegen 
    
    frame_size = 5; %sec
    visible_frame_sec = 20;
    show_debug_figure = false;
    
    if (show_debug_figure) 
        debug_figure = figure;
    end
       
    min_dist_between_max_peaks_samples = ceil(60/max_rate*fs); 
    overlap_len = 0; 
    win_len = floor(min_dist_between_max_peaks_samples/2);
    timestamp_arr = 0:(1/fs):ceil(length(signal)/fs);
    timestamp_arr = timestamp_arr(1:length(signal));
    %----------------------------------------------------------------------------------
    %for each window, calculate the max values
    %according to these values, remove extreme values
    %-----------------------------------------------------------------------------------
    

    %for each window find the maximum value
    data_length = length(signal);
   
    if (isnan(chunk_size_min)) || (chunk_size_min == 0)
        
        chunk_size_samples = data_length;
    else
        chunk_size_samples = chunk_size_min*60*fs;
    end
    
    all_max_peaks_index_arr = [];
        
    for chunk_index = 1:ceil(data_length/chunk_size_samples)
        
        max_peaks_index_arr = Inf*ones(1, data_length); %pre allocating the array
        max_peak_index_counter = 1;
        
        chunk_begin_index = (chunk_index - 1)*chunk_size_samples + 1;
        chunk_end_index = min(data_length, chunk_index*chunk_size_samples);
        
        timestamp_chunk_arr = timestamp_arr(chunk_begin_index:chunk_end_index);
        signal_chunk_arr = signal(chunk_begin_index:chunk_end_index);
        
        
        begin_index = 1;
        while ((begin_index + win_len - 1) < length(signal_chunk_arr))
            end_index = begin_index + win_len - 1;
            [~, max_index] = max(signal_chunk_arr(begin_index:end_index));
%             [min_value, min_index] = min(data);
            max_index = begin_index + max_index - 1;
%             min_index = begin_index + min_index - 1;

            if (~isempty(max_index))
                max_peaks_index_arr(max_peak_index_counter) =  max_index(1);
                max_peak_index_counter = max_peak_index_counter + 1;
            end

%            if (~isempty(min_index))
%                 min_peaks_index_arr = [min_peaks_index_arr , min_index(1)];
%             end

            begin_index = begin_index + win_len - 1 - overlap_len;
        end
        
        max_peaks_index_arr(max_peak_index_counter:end) = [];

        if (show_debug_figure)
            
            clf(debug_figure);
            hold on
            plot(timestamp_chunk_arr, signal_chunk_arr);
            plot(timestamp_chunk_arr(max_peaks_index_arr), chunk_data(max_peaks_index_arr), '*r');
            axis tight
        end

        %remove peaks too small----------------------------------------------
        if (filter_lower_than_mean_values)
            mean_data = mean(signal_chunk_arr);
            ind = find(signal_chunk_arr(max_peaks_index_arr)<mean_data);
            max_peaks_index_arr(ind) = [];
        end
        
        if (show_debug_figure)
            
            clf(debug_figure);
            hold on
            plot(timestamp_chunk_arr, signal_chunk_arr);
            plot(timestamp_chunk_arr(max_peaks_index_arr), signal_chunk_arr(max_peaks_index_arr), '*r');
            axis tight
%             show_debug_figure(h, data, max_peaks_index_arr, 0);
        end
        
        
        max_peaks_index_arr = max_peaks_index_arr + chunk_begin_index - 1;
        all_max_peaks_index_arr = [all_max_peaks_index_arr  max_peaks_index_arr];
        
    end
    
    max_peaks_index_arr = all_max_peaks_index_arr;
    
    if (show_debug_figure)
        clf(debug_figure);
        hold on
        plot(timestamp_arr, signal);
        plot(timestamp_arr(max_peaks_index_arr), signal(max_peaks_index_arr), '*r');
        axis tight
%         if (frame_size > 0)
% %             add_slidebar(frame_size, max(timestamp_arr));
%         end
%         show_debug_figure(h, signal, max_peaks_index_arr, frame_size);
    end
    
    %remove peaks too close-------------------------------------------
    index_diff = diff(max_peaks_index_arr) ;
    indexes_too_close =  find(index_diff < min_dist_between_max_peaks_samples);

    flag = true;

    while (flag)

        if (~isempty(indexes_too_close))
            for i = 1:length(indexes_too_close) 
                if (signal(max_peaks_index_arr(indexes_too_close(i))) > (signal(max_peaks_index_arr(indexes_too_close(i) + 1))))
                    indexes_too_close(i) = indexes_too_close(i) + 1;
                end 
            end
            max_peaks_index_arr(indexes_too_close) =[]; 
        end

        index_diff = diff(max_peaks_index_arr) ;
        indexes_too_close =  find(index_diff < min_dist_between_max_peaks_samples);
        if (isempty(indexes_too_close))
            flag = false;
        end
    end

    if (show_debug_figure)
        clf(debug_figure);
        hold on
        plot(timestamp_arr, signal);
        plot(timestamp_arr(max_peaks_index_arr), timestamp_arr(signal(max_peaks_index_arr)), '*r');
        axis tight
        if (frame_size > 0)
            add_slidebar(frame_size, max(timestamp_arr));
        end
%         show_debug_figure(h, signal, max_peaks_index_arr, frame_size);
    end


%         index_diff = diff(min_peaks_index_arr) ;
%         indexes_too_close =  find(index_diff < min_dist_between_max_peaks_samples);

%         flag = true;
% 
%         while (flag)
% 
%             if (~isempty(indexes_too_close))
%                 for i = 1:length(indexes_too_close) 
%                     if (signal(min_peaks_index_arr(indexes_too_close(i))) < (signal(min_peaks_index_arr(indexes_too_close(i) + 1))))
%                         indexes_too_close(i) = indexes_too_close(i) + 1;
%                     end 
%                 end
%                 min_peaks_index_arr(indexes_too_close) =[]; 
%             end
% 
%             index_diff = diff(min_peaks_index_arr) ;
%             indexes_too_close =  find(index_diff < min_dist_between_max_peaks_samples);
%             if (isempty(indexes_too_close))
%                 flag = false;
%             end
%         end
% 

    %remove nan values
    ind = isnan(signal(max_peaks_index_arr));
    max_peaks_index_arr(ind) = [];

%         ind = isnan(signal(min_peaks_index_arr));
%         min_peaks_index_arr(ind) = [];

    
    if (show_debug_figure)
        close(debug_figure);
    end
      
    if (show_results)
        figure;
        hold on
        plot(timestamp_arr, signal);
        plot(timestamp_arr(max_peaks_index_arr), signal(max_peaks_index_arr), '*r');
        axis tight
        
       
        add_slidebar(frame_size, max(timestamp_arr));
        
    end
        
end

