function [zero_crossing_locs_arr, type_arr] = findZeroCrossing(signal)
    

    %smooth signal
    signal = smooth(signal,5);
    %calculate first derivative
    diff1 = diff(signal);

    %find zero crossings
    t1 = diff1(1:end-1);
    t2 = diff1(2:end);
    tt = t1.*t2;
    zero_crossing_locs_arr = find(tt < 0) + 1;
    
    points_arr = t1(zero_crossing_locs_arr - 1);
    ind_max = find(points_arr > 0);
    ind_min = find(points_arr < 0);
    
    type_arr(ind_max) = 1;
    type_arr(ind_min) = -1;
end
