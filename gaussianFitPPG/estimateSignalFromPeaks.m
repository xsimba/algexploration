function [fitted_signal, comp_arr] = estimateSignalFromPeaks(x_arr, mu_arr, sigma_arr, amp_arr)

    comp_arr = zeros(length(mu_arr), length(x_arr));
    for peaks_ind = 1:length(mu_arr)
        comp_arr(peaks_ind, :) = amp_arr(peaks_ind)*1/(sigma_arr(peaks_ind)*sqrt(2*pi))*exp(-((x_arr-mu_arr(peaks_ind))./(sqrt(2).*sigma_arr(peaks_ind))).^2);
    end
            
    fitted_signal = sum(comp_arr,1);
end