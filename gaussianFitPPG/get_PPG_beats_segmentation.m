function [systolic_peak_index_arr, systolic_foot_index_arr, corrected_signal, baseline_signal] = get_PPG_beats_segmentation(timestamp_arr, signal, fs, ...
                                min_heart_rate,  max_heart_rate, foot_detection_type, ...
                                chunk_size_min, filter_lower_than_mean_values, ...
                                correct_baseline_wandering, ...
                                show_beat_segmentation, show_corrected_wandering_baseline, title_str)
                            
      
                            
    [systolic_peak_index_arr] = detect_local_maxima_points(signal, fs, max_heart_rate, filter_lower_than_mean_values, ...
                                 chunk_size_min, false);

                             
    [systolic_foot_index_arr] = detect_local_minima_points(timestamp_arr, signal, systolic_peak_index_arr, fs, foot_detection_type, show_beat_segmentation, title_str);
                                

    %remove wandering baseline
   
    baseline_signal = zeros(size(signal));
  
    
    if (correct_baseline_wandering)
        
        [correct_signal, baseline_signal] = remove_baseline(signal, systolic_foot_index_arr);

       else
        corrected_signal = signal;
    end
   
    
    
    corrected_signal = signal - baseline_signal;

    if (show_corrected_wandering_baseline)
        figure; 
        h1 = subplot(2,1,1);
        hold on;
        plot(timestamp_arr, signal, 'linewidth', 2);
        plot(timestamp_arr, corrected_signal, 'linewidth', 2);
        plot(timestamp_arr, baseline_signal,'g', 'linewidth', 2);
        axis tight;
        legend('original', 'fitted');
        add_slidebar(10, max(timestamp_arr));

    end
    
   
    ppg_index = 1;
    max_cycle_length = ceil(60/min_heart_rate*fs); 
    
    for cycle_index = 1:(length(systolic_foot_index_arr) - 1)
        ppg_cycle = corrected_signal(systolic_foot_index_arr(cycle_index):systolic_foot_index_arr(cycle_index+1));
        if (length(ppg_cycle) <= max_cycle_length)
            PPG_beat_arr{ppg_index} = ppg_cycle;
            ppg_index = ppg_index + 1;
        end
    end
end
