
function [x_arr, norm_beat, amp, y_offset] = normalize_beat(beat, fs)
    if (size(beat,2) == 1) 
        beat = beat'; %work with column vectors
    end

    %normalize signal
    y_offset = min(beat);
    norm_beat_ = beat - y_offset;

    amp = max(norm_beat_(1:ceil(length(norm_beat_)/2)));
    norm_beat = norm_beat_/amp;


    max_t  = length(beat)/fs;
    dt = 1/fs;
    x_arr = 0:dt:max_t;
    x_arr = x_arr(1:length(beat)); %work with column vectors

   
end