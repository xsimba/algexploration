function [PPG_beats_arr, corrected_signal,  systolic_peak_index_arr, systolic_foot_index_arr] = show_beats_multichannel(timestamp_arr, signals_arr, fs, ...
                        segmentation_channel, ...
                        min_heart_rate, max_heart_rate, foot_detection_type, ...
                        chunk_size_min, filter_lower_than_mean_values, correct_baseline_wandering, ...
                        show_beat_segmentation, show_corrected_wandering_baseline)

   
    
    channels_arr = {'Blue-B2', 'Green-A1', 'Red-D2', 'Ired-E2', 'Green-C3', 'Green-C2', 'Red-F5', 'Green-G5'};
    color_arr = {'b', 'g', 'r', 'k', 'g', 'g', 'm', 'c'};
    symbol_arr = {'-', '-', '-', '-', '--', '-.', '-', '-'};

    max_hr_sec = 60/max_heart_rate*fs;
    left_offset = max_hr_sec*0.0;
    right_offset = left_offset;
    
    %detect beats
    [PPG_beats_arr, corrected_signal, baseline_signal, systolic_peak_index_arr, systolic_foot_index_arr] = get_PPG_beats(timestamp_arr, signals_arr{segmentation_channel}, fs,...
        min_heart_rate, max_heart_rate, foot_detection_type, ...
        chunk_size_min, filter_lower_than_mean_values, ...
        correct_baseline_wandering, ...
        show_beat_segmentation, show_corrected_wandering_baseline, '');
    
                              
    num_channels = length(signals_arr);
 
    figure_handler = figure;
    
    
    for component_index = 1:(length(systolic_foot_index_arr)-1)
       
        begin_index = systolic_foot_index_arr(component_index);
        
        end_index = systolic_peak_index_arr(component_index+2) + ceil(0.05*fs);
        
       
        norm_beats_arr = [];
        for ch_index = 1:num_channels
            
            beat = signals_arr{ch_index}(begin_index:end_index);


             %normalize signal
            norm_beat_ = beat - min(beat);

            amp_coeff_correction = max(norm_beat_(1:ceil(length(norm_beat_)/2)));
            norm_beat = norm_beat_/amp_coeff_correction;


            max_t  = length(beat)/fs;
            dt = 1/fs;
            x_corr_arr = 0:dt:max_t;
            x_corr_arr = x_corr_arr(1:length(beat))'; %work with column vectors

            y_offset = min(beat);
            
            norm_beats_arr(:, ch_index) = norm_beat;
        end
        
        clf(figure_handler);
        hold on
        for ch_index = 1:num_channels
            plot(x_corr_arr, norm_beats_arr(:, ch_index), symbol_arr{ch_index}, 'color', color_arr{ch_index});
        end
        legend(channels_arr, 'Location', 'EastOutside');
        axis tight
    end
end
        