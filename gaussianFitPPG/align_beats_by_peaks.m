%align all beats according to the location of the mean slope
function [aligned_timestamp_arr, aligned_beats_arr, corrected_offsets_samples_arr] = align_beats_by_peaks(timestamp_arr, beats_arr, relevant_channel_indexes_arr, fs)

    
    max_distance_from_center_sec = 0.1;
    max_distance_from_center = ceil(0.1*fs);
    
    ppg_referecene_arr = nan*ones(1, size(beats_arr, 1));
    
    for beat_index = 1:size(beats_arr, 1)
        if (relevant_channel_indexes_arr(beat_index))
            beat = beats_arr(beat_index, :);
    %         [max_val max_ind] = max(beat);

            mid_upstroke_index = get_middle_PPG_upstroke(beat);

            if (~isnan(mid_upstroke_index))
                ppg_referecene_arr(beat_index) = timestamp_arr(mid_upstroke_index);
            end
       
        end
    end
    
    %align beats according to the maximal peak
    %ignore beats that are too distant from the center by 0.1

    valid_values_arr = find(~isnan(ppg_referecene_arr));
    aligned_beats_arr = zeros(size(beats_arr));
    
    median_pivot_value = median(ppg_referecene_arr(valid_values_arr));
    
    dist_from_center_arr = abs(ppg_referecene_arr - median_pivot_value);
    
    relevant_beats_ind = find(dist_from_center_arr < max_distance_from_center_sec);
    
    pivot_value = median(ppg_referecene_arr(relevant_beats_ind));
    
    corrected_offsets_samples_arr = nan*ones(1,size(beats_arr, 1));
   
    for beat_index = 1:size(beats_arr, 1)
        offset_index = 0;
        if (relevant_channel_indexes_arr(beat_index))
            beat = beats_arr(beat_index, :);
            if (dist_from_center_arr(beat_index) < max_distance_from_center_sec)
                offset_sec =  pivot_value - ppg_referecene_arr(beat_index);
                offset_index = round(offset_sec*fs);
                
            end

            if (offset_index > 0)
                corrected_beat = [zeros(1, offset_index) beat(1:end-offset_index)];
            else
                corrected_beat = [beat((abs(offset_index) +1):end) zeros(1, abs(offset_index))];
            end

            aligned_beats_arr(beat_index,:) = corrected_beat;
            corrected_offsets_samples_arr(beat_index) = offset_index;
        end
    end
   
    max_left_padding = max(corrected_offsets_samples_arr(corrected_offsets_samples_arr>=0));
    max_right_padding = abs(min(corrected_offsets_samples_arr(corrected_offsets_samples_arr<=0)));
    aligned_beats_arr = aligned_beats_arr(:, (max_left_padding+1):(end - max_right_padding));
    aligned_timestamp_arr = timestamp_arr(1:end - max_left_padding - max_right_padding);
    
end