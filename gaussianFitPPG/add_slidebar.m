function [] = add_slidebar(frame_size, signal_size)

    a = gca;
    % Set appropriate axis limits and settings
    set(gcf,'doublebuffer','on');
    % This avoids flickering when updating the axis

    set(a,'xlim',[0 frame_size]);


    % Generate constants for use in uicontrol initialization
    pos=get(a,'position');
    Newpos=[pos(1) pos(2)-0.1 pos(3) 0.02]; %set the position of the scroller on the figure
    % This will create a slider which is just underneath the axis
    % but still leaves room for the axis labels above the slider
    xmax = signal_size;
    S=['set(gca,''xlim'',get(gcbo,''value'')+[0 ' num2str(frame_size) '])'];
    % Setting up callback string to modify XLim of axis (gca)
    % based on the position of the slider (gcbo)

    % Creating Uicontrol
    if (frame_size < xmax)
         h = uicontrol('style','slider',...
             'units','normalized','position',Newpos,...
             'callback',S,'min',0,'max',xmax-frame_size);
    end
end