function [b_coefs_arr, a_coefs_arr] = create_butter_bp_filter(fs, lowFreq, hiFreq, filter_order)

%     A_stop1 = 20;		% Attenuation in the first stopband = 60 dB
%     F_stop1 = 0.1;		% Edge of the stopband = 8400 Hz
%     F_pass1 = 0.2;	% Edge of the passband = 10800 Hz
%     F_pass2 = 30;	% Closing edge of the passband = 15600 Hz
%     F_stop2 = 35;	% Edge of the second stopband = 18000 Hz
%     A_stop2 = 60;		% Attenuation in the second stopband = 60 dB
%     A_pass = 1;		% Amount of ripple allowed in the passband = 1 dB
%     
%     d  = fdesign.bandpass('Fst1,Fp1,Fp2,Fst2,Ast1,Ap,Ast2', ...
% 		F_stop1, F_pass1, F_pass2, F_stop2, A_stop1, A_pass, ...
% 		A_stop2, fs);
%     
%   
%     Hd = design(d,'equiripple');

    
    [b_coefs_arr, a_coefs_arr] = butter(filter_order, [lowFreq hiFreq]/(fs/2), 'bandpass');


    % freqz(b_coefs_arr,a_coefs_arr, 100, fs)

end