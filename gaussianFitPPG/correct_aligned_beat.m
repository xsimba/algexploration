function [corrected_signal] = correct_aligned_beat(signal, offset)

    corrected_signal = nan*size(signal);
    if (offset > 0)
        corrected_signal(offset:end) = signal(1:end-offset);
    else
        corrected_signal(1:end-offset) = signal((offset+1):end);
    end
    
end