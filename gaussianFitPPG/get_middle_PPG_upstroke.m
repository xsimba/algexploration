function [mid_upstroke_index] = get_middle_PPG_upstroke(beat)
    
    [val_max ind_max] = max(beat);
    
    mid_val = beat(1) + (val_max  - beat(1))/2;
    %get median slope 
    ind = find(beat > mid_val);
    
    if (~isempty(ind))
    
        mid_upstroke_index = ind(1);
    else
        mid_upstroke_index = nan;
    end
    
end