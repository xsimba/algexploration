function [fitResults,GOF,BestStart,xi,yi,BootResults]=peakfit_multichannel(x_arr, y_arr, center, ...
                 window,numPeaks,numTrials,start,plots,bipolar,minwidth,DELTA)

    %#codegen
    
    global temp_PEAKHEIGHTS  delta BIPOLAR
    format short g
    format compact
    warning off all
    
    NumArgOut = nargout;
    
    BIPOLAR = bipolar;
   
    start=calcstart(x_arr,numPeaks);
  
    MINWIDTH=x_arr(2)-x_arr(1);
    delta=1;

  
    % Default values for placeholder zeros1
    if numTrials == 0;numTrials=1; end
    if numPeaks == 0;numPeaks=1; end
    if start==0; start=calcstart(x_arr, numPeaks);end
    
    

   
    n = length(x_arr);
    newstart = start;
    % Assign ShapStrings
 
    % Perform peak fitting for selected peak shape using fminsearch function
    % options = optimset('TolX',.001,'Display','off','MaxFunEvals',1000 );
    %put here constaints
    options = optimset('TolX',.001,'Display','off','MaxFunEvals',1000 );


    LowestError = Inf; % or any big number greater than largest error expected
    FitParameters=zeros(1,numPeaks.*2); 
    BestStart = zeros(1,numPeaks.*2); 
    model_height = zeros(1,numPeaks); 
    temp_PEAKHEIGHTS = zeros(1,numPeaks);
   
    num_channels = size(y_arr, 1);
    
     bestmodel=zeros(1, size(y_arr,2));
    for k=1:numTrials, 
        % StartMatrix(k,:)=newstart;
        % disp(['Trial number ' num2str(k) ] ) % optionally prints the current trial number as progress indicator
%         TrialParameters=fminsearch(@fitgaussian(lambda,x_arr,y_arr), newstart, options);
%      
        TrialParameters=fminsearch(@fitgaussian, newstart, options, x_arr,y_arr);
%             [x,fval,exitflag,output] = fminsearchbnd(fun,x0,LB,UB,options,varargin)
        for Peak=1:numPeaks;
            if TrialParameters(2*Peak)<MINWIDTH,
                TrialParameters(2*Peak)=MINWIDTH;
            end
        end
            

        % Construct model from Trial parameters
        A=zeros(numPeaks,n);
        for m=1:numPeaks,

             A(m,:)=gaussian(x_arr,TrialParameters(2*m-1),TrialParameters(2*m));

            for parameter=1:2:2*numPeaks,
                newstart(parameter)=newstart(parameter)*(1+delta*(rand-.5)/500);
                newstart(parameter+1)=newstart(parameter+1)*(1+delta*(rand-.5)/100);
            end
        end % for NumPeaks

        % Multiplies each row by the corresponding amplitude and adds them up

       

        model=temp_PEAKHEIGHTS'*A;
        baseline=0;

       

        % Compare trial model to data segment and compute the fit error
        
        MeanFitError = 0;
        for ch_index = 1:num_channels
            MeanFitError = MeanFitError + 100*norm(y_arr(ch_index, :)-model)./(sqrt(n)*max(y_arr(ch_index, :)));
        end
        MeanFitError = MeanFitError/num_channels;

      % Take only the single fit that has the lowest MeanFitError
      if MeanFitError<LowestError, 
          if min(temp_PEAKHEIGHTS)>=-BIPOLAR*10^100,  % Consider only fits with positive peak heights
            LowestError=MeanFitError;  % Assign LowestError to the lowest MeanFitError
            FitParameters=TrialParameters;  % Assign FitParameters to the fit with the lowest MeanFitError
            BestStart=newstart; % Assign BestStart to the start with the lowest MeanFitError
            model_height = temp_PEAKHEIGHTS; % Assign height to the temp_PEAKHEIGHTS with the lowest MeanFitError
            bestmodel=model; % Assign bestmodel to the model with the lowest MeanFitError
          end % if min(temp_PEAKHEIGHTS)>0
      end % if MeanFitError<LowestError
        %  ErrorVector(k)=MeanFitError;
    end % for k (NumTrials)

    Rsquared = 0;
    num_channels = size(y_arr, 1);
    SStot  = zeros(1, num_channels);
    SSres = zeros(1, num_channels);
    for ch_index = 1:num_channels
       SStot(ch_index)=sum((y_arr(ch_index, :)-mean(y_arr(ch_index, :))).^2);
       SSres(ch_index)=sum((y_arr(ch_index, :)-bestmodel).^2);

    end
     Rsquared=1-mean(SSres./SStot);

    GOF=[LowestError Rsquared];
 
    AA=zeros(numPeaks,600);
    xxx=linspace(min(x_arr),max(x_arr),600);
    % xxx=linspace(min(xx)-length(xx),max(xx)+length(xx),200);
    for m=1:numPeaks,
       
        AA(m,:)=gaussian(xxx,FitParameters(2*m-1),FitParameters(2*m));
        
    end % for NumPeaks

    % Multiplies each row by the corresponding amplitude and adds them up
   
    heightsize=size(model_height');
    AAsize=size(AA);
    if heightsize(2)==AAsize(1),
        mmodel=model_height'*AA+baseline;
    else
        mmodel=model_height*AA+baseline;
    end

    yi = zeros(size(AA));
    area = zeros(1, numPeaks);
    for m=1:numPeaks,
        if plots, plot(xxx,model_height(m)*AA(m,:)+baseline,'g'),end  % Plot the individual component peaks in green lines
        area(m)=trapz(xxx,model_height(m)*AA(m,:)); % Compute the area of each component peak using trapezoidal method
        yi(m,:)=model_height(m)*AA(m,:); % Place y values of individual model peaks into matrix yi
    end
   
    xi=xxx; % Place the x-values of the individual model peaks into xi

   
    % Put results into a matrix FitResults, one row for each peak, showing peak index number,
    % position, amplitude, and width.
    fitResults=zeros(numPeaks,6);
    
       
    for m=1:numPeaks,
        if m==1,
            fitResults = [round(m) FitParameters(2*m-1) model_height(m) abs(FitParameters(2*m)) area(m)];
        else
            fitResults = [fitResults ; [round(m) FitParameters(2*m-1) model_height(m) abs(FitParameters(2*m)) area(m)]];
        end % if m==1
    end % for m=1:NumPeaks,
   
    
    if NumArgOut==8,
        if plots,disp('Computing bootstrap sampling statistics.....'),end
        BootstrapResultsMatrix=zeros(6,100,numPeaks);
        BootstrapErrorMatrix=zeros(1,100,numPeaks);
        clear bx by
        tic;
        for trial=1:100,
            n=1;
            bx=x_arr;
            by=y_arr;
            while n<length(x_arr)-1,
                if rand>.5,
                    bx(n)=x_arr(n+1);
                    by(n)=y_arr(n+1);
                end
                n=n+1;
            end
            bx=bx+xoffset;
            [fitResults,BootFitError]=fitpeaks(bx,by,numPeaks,temp_PEAKHEIGHTS, numTrials,start);
            for peak=1:numPeaks,
                
                BootstrapResultsMatrix(1:5,trial,peak)=fitResults(peak,1:5);
              
                BootstrapErrorMatrix(:,trial,peak)=BootFitError;
            end
        end
        if plots,toc;end
        
        BootResults = zeros(numPeaks, 5);
        for peak=1:numPeaks,
            if plots,
                disp(' ')
                disp(['Peak #',num2str(peak) '         Position    Height       Width       Area      Shape Factor']);
            end % if plots
            BootstrapMean=mean(real(BootstrapResultsMatrix(:,:,peak)'));
            BootstrapSTD=std(BootstrapResultsMatrix(:,:,peak)');
            BootstrapIQR=iqr(BootstrapResultsMatrix(:,:,peak)');
            PercentRSD=100.*BootstrapSTD./BootstrapMean;
            PercentIQR=100.*BootstrapIQR./BootstrapMean;
            BootstrapMean=BootstrapMean(2:6);
            BootstrapSTD=BootstrapSTD(2:6);
            BootstrapIQR=BootstrapIQR(2:6);
            PercentRSD=PercentRSD(2:6);
            PercentIQR=PercentIQR(2:6);
            if plots,
                disp(['Bootstrap Mean: ', num2str(BootstrapMean)])
                disp(['Bootstrap STD:  ', num2str(BootstrapSTD)])
                disp(['Bootstrap IQR:  ', num2str(BootstrapIQR)])
                disp(['Percent RSD:    ', num2str(PercentRSD)])
                disp(['Percent IQR:    ', num2str(PercentIQR)])
            end % if plots
            BootResults(peak,:)=[BootstrapMean BootstrapSTD PercentRSD BootstrapIQR PercentIQR];
        end % peak=1:NumPeaks,
    end % if NumArgOut==8,
  
    
% ----------------------------------------------------------------------
function [FitResults,LowestError]=fitpeaks(xx,yy,NumPeaks, NumTrials,start)
    format short g
    format compact
    warning off all
    global temp_PEAKHEIGHTS;
    if start==0;start=calcstart(xx,NumPeaks);end
    temp_PEAKHEIGHTS = zeros(1,NumPeaks);
    n=length(xx);
    newstart=start;

    % Perform peak fitting for selected peak shape using fminsearch function
    options = optimset('TolX',.001,'Display','off','MaxFunEvals',1000 );
    LowestError = 1000; % or any big number greater than largest error expected
    FitParameters = zeros(1,NumPeaks.*2); 
    BestStart = zeros(1,NumPeaks.*2); 
    model_height = zeros(1,NumPeaks); 
    bestmodel = zeros(size(yy));

    for k=1:NumTrials,
 
        TrialParameters=fminsearch(@fitgaussian, newstart,options, xx,yy);
        for Peak=1:NumPeaks;
            if TrialParameters(2*Peak)<MINWIDTH,
                TrialParameters(2*Peak)=MINWIDTH;
            end
        end
   
        for peaks=1:NumPeaks,
             peakindex=2*peaks-1;
             newstart(peakindex)=start(peakindex)-xoffset;
        end

        % Construct model from Trial parameters
        A=zeros(NumPeaks,n);
        for m=1:NumPeaks,
            A(m,:)=gaussian(xx,TrialParameters(2*m-1),TrialParameters(2*m));  
        end % for

        % Multiplies each row by the corresponding amplitude and adds them up
       

        model = temp_PEAKHEIGHTS'*A;
   
        

        % Compare trial model to data segment and compute the fit error
        MeanFitError=100*norm(yy-model)./(sqrt(n)*max(yy));
        
        % Take only the single fit that has the lowest MeanFitError
        if (MeanFitError < LowestError)
            if min(temp_PEAKHEIGHTS)>=-BIPOLAR*10^100,  % Consider only fits with positive peak heights
                LowestError = MeanFitError;  % Assign LowestError to the lowest MeanFitError
                FitParameters=TrialParameters;  % Assign FitParameters to the fit with the lowest MeanFitError
                model_height = temp_PEAKHEIGHTS; % Assign height to the temp_PEAKHEIGHTS with the lowest MeanFitError
            end % if min(temp_PEAKHEIGHTS)>0
        end % if MeanFitError<LowestError
    end % for k (NumTrials)
    
    Rsquared = 1-(norm(yy-bestmodel)./norm(yy-mean(yy)));
    SStot = sum((yy-mean(yy)).^2);
    SSres = sum((yy-bestmodel).^2);
    Rsquared = 1-(SSres./SStot);
    GOF = [LowestError Rsquared];
    area_arr = zeros(1, NumPeaks);
    
    for m=1:NumPeaks,
        area_arr(m)=trapz(xx+xoffset,model_height(m)*A(m,:)); % Compute the area of each component peak using trapezoidal method
    end

    % Put results into a matrix FitResults, one row for each peak, showing peak index number,
    % position, amplitude, and width.
    FitResults=zeros(NumPeaks,6);
    for m=1:NumPeaks,
        if m==1,
            FitResults=[round(m) FitParameters(2*m-1)+xoffset model_height(m) abs(FitParameters(2*m)) area_arr(m)];
        else
            FitResults=[FitResults ; [round(m) FitParameters(2*m-1)+xoffset model_height(m) abs(FitParameters(2*m)) area_arr(m)]];
        end % if m==1
    end % for m=1:NumPeaks,

% ----------------------------------------------------------------------
function start=calcstart(xx,NumPeaks)
    n=max(xx)-min(xx);
    start=zeros(1, NumPeaks*2);
    startpos=[n/(NumPeaks+1):n/(NumPeaks+1):n-(n/(NumPeaks+1))]+min(xx);
    for marker=1:NumPeaks,
      markx=startpos(marker);
      start((marker-1)*2 +1) = markx;
      start(marker*2) = n/(3.*NumPeaks);
    end % for marker
% ----------------------------------------------------------------------
function err = fitgaussian(lambda,t,y)
    % Fitting function for a Gaussian band signal.
    global temp_PEAKHEIGHTS BIPOLAR
    numpeaks = round(length(lambda)/2);
    A = zeros(length(t),numpeaks);
    for j = 1:numpeaks,
    %    if lambda(2*j)<MINWIDTH,lambda(2*j)=MINWIDTH;end
        A(:,j) = gaussian(t,lambda(2*j-1),lambda(2*j))';
    end 

    num_channels = size(y, 1);
    err = 0;
    for ch_index = 1:num_channels
        
        if BIPOLAR,temp_PEAKHEIGHTS=A\y(ch_index, :)';else temp_PEAKHEIGHTS=abs(A\y(ch_index, :)');end
        z = A*temp_PEAKHEIGHTS;
        
        err = err + norm(z-y(ch_index, :)');
       
    end

    err = err/num_channels;
% ----------------------------------------------------------------------
function g = gaussian(x,pos,wid)
    %  gaussian(X,pos,wid) = gaussian peak centered on pos, half-width=wid
    %  X may be scalar, vector, or matrix, pos and wid both scalar
    % Examples: gaussian([0 1 2],1,2) gives result [0.5000    1.0000    0.5000]
    % plot(gaussian([1:100],50,20)) displays gaussian band centered at 50 with width 20.

    % g = exp(-((x-pos)./(0.6005615.*wid)).^2);
    g = 1/(wid*sqrt(2*pi))*exp(-((x-pos)./(sqrt(2).*wid)).^2);
% ----------------------------------------------------------------------
function b=iqr(a)
    % b = IQR(a)  returns the interquartile range of the values in a.  For
    %  vector input, b is the difference between the 75th and 25th percentiles
    %  of a.  For matrix input, b is a row vector containing the interquartile
    %  range of each column of a.
    %  T. C. O'Haver, 2012
    mina=min(a);
    sizea=size(a);
    NumCols=sizea(2);
    b = zeros(size(a));
    for n=1:NumCols
        b(:,n)=a(:,n)-mina(n);
    end
    Sorteda=sort(b);
    lx=length(Sorteda);
    SecondQuartile=round(lx/4);
    FourthQuartile=3*round(lx/4);
    b=abs(Sorteda(FourthQuartile,:)-Sorteda(SecondQuartile,:));
