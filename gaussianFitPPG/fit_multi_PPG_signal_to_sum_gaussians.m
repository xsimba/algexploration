%fit_multi_PPG_signal_to_sum_gaussians models each detected AC componenet into a sum of Gaussians

function [multi_corrected_signals_arr, baseline_signals_arr, params_arr, graphic_data] = ...
                        fit_multi_PPG_signal_to_sum_gaussians(timestamp_arr, signal_arr, ...
                        general_params, multichannel_params)
                    
                    

    %#codegen
    params_arr = [];
    
    num_channels = size(signal_arr,1);
    signal_length = size(signal_arr,2);
    
    relevant_channel_indexes_arr = find(multichannel_params.processed_channels_arr == 1);
    num_relevant_channels = length(relevant_channel_indexes_arr);
    
    %detect beats
    [~, systolic_foot_index_arr2, ~, ~] = get_PPG_beats_segmentation(timestamp_arr, signal_arr(general_params.segmentation_ch_index,:), general_params.fs,...
        general_params.min_heart_rate, general_params.max_heart_rate, 2, ...
        general_params.chunk_size_min, general_params.filter_lower_than_mean_values, ...
        general_params.correct_baseline_wandering, ...
        false, general_params.show_corrected_wandering_baseline, '');
    
   
   [~, systolic_foot_index_arr1, ~, ~] = get_PPG_beats_segmentation(timestamp_arr, signal_arr(general_params.segmentation_ch_index,:), general_params.fs,...
        general_params.min_heart_rate, general_params.max_heart_rate, 1, ...
        general_params.chunk_size_min, general_params.filter_lower_than_mean_values, ...
        general_params.correct_baseline_wandering, ...
        general_params.show_beat_segmentation, general_params.show_corrected_wandering_baseline, '');
   

    
    %presentation and intermediate variables
    
    num_beats = length(systolic_foot_index_arr2) - 1;
    
    all_mu_arr = nan*ones(num_beats, multichannel_params.num_peaks);
    all_sigma_arr = nan*ones(num_beats, multichannel_params.num_peaks);
    all_amp_arr = nan*ones(num_beats, multichannel_params.num_peaks);
    all_corrected_alignment_offset = nan*ones(num_channels, num_beats);
    
    x_offset_arr = nan*ones(num_channels, num_beats);
    beat_length_arr = nan*ones(1, num_beats);
    y_offset_arr = nan*ones(num_channels, num_beats);
    amp_coeff_correction_arr = nan*ones(num_channels, num_beats);
    

%     coder.varsize('x_offset_arr',[1 100],[0 1]);
    
    
    all_norm_beat_arr = nan*ones(num_channels, signal_length);
    all_comp_arr = nan*ones(num_channels, signal_length, multichannel_params.num_peaks);
    all_modeled_beat_arr = nan*ones(1, signal_length); %fitted beats
   
    %the corrected signals for all channels based on merging ('ac' correction)
    
    all_waves_arr = nan*ones(num_channels, signal_length,multichannel_params.num_peaks);
    multi_corrected_signals_arr = nan*ones(size(signal_arr));
   
    
    channels_selected_for_merge_arr = zeros(num_channels, num_beats);
    
    baseline_corrected_signal_arr = nan*ones(num_channels, signal_length);
    baseline_signals_arr = nan*ones(num_channels, signal_length);
    
    

    for ch_index = 1:num_channels

        if (multichannel_params.processed_channels_arr(ch_index) == 0)
            continue;
        end
        
        %get baseline for each channel
        [~, ~, baseline_corrected_signal, baseline_signal] = get_PPG_beats_segmentation(timestamp_arr, signal_arr(ch_index, :), general_params.fs,...
                general_params.min_heart_rate, general_params.max_heart_rate, 1, ...
                general_params.chunk_size_min, general_params.filter_lower_than_mean_values, ...
                general_params.correct_baseline_wandering, ...
                false, general_params.show_corrected_wandering_baseline, '');

         %get the AC components after baseline removal
         baseline_corrected_signal_arr(ch_index, :) = baseline_corrected_signal;
         baseline_signals_arr(ch_index, :) = baseline_signal;
        
    end
    
    set(0,'DefaultFigureWindowStyle','docked'); 
    

    beats_begin_index_arr = zeros(1,num_beats);
    beats_end_index_arr = zeros(1,num_beats);
    
    for beat_index = 1:num_beats

        begin_index = systolic_foot_index_arr2(beat_index);
        end_index = systolic_foot_index_arr1(beat_index+1);
        
        beats_begin_index_arr(beat_index) = begin_index;
        beats_end_index_arr(beat_index) = end_index;
   
    end
    
    %---------------------------------------------
    
    show_beats_debug = false;
    if (show_beats_debug)
        debug_figure_handler = figure;
    end
          
    for beat_index = 1:num_beats
        
        if(mod(num_beats,100) == 0)
            fprintf('Processing beat %d/%d..\n', beat_index, num_beats);
        end
        
        %get beat (from systolic foot on both ends)
        begin_index = beats_begin_index_arr(beat_index);
        end_index = beats_end_index_arr(beat_index);
        beat_length = end_index - begin_index + 1;
        
        if (beat_length <=1)
            continue;
        end
            
        
        beat_arr = zeros(num_channels,beat_length);
        norm_beats_vector_arr = zeros(num_channels,beat_length);
        
        for ch_index = 1:num_channels
            
            if (multichannel_params.processed_channels_arr(ch_index) == 0)
                continue;
            end
            
            %get beat data
            beat = baseline_corrected_signal_arr(ch_index, begin_index:end_index);
            beat_arr(ch_index,:) = beat;
            
            %normalize beat by its amplitude
            [x_arr, norm_beat, amp_coeff_correction, y_offset] = normalize_beat(beat, general_params.fs);
            
            %save normalization coefficient
            amp_coeff_correction_arr(ch_index, beat_index) = amp_coeff_correction;
            y_offset_arr(ch_index, beat_index) = y_offset;
            
            norm_beats_vector_arr(ch_index,:) =  norm_beat;
            
         end
  
         
         %now align beats to cancel time delay between channels
         [aligned_x_arr, aligned_norm_beats_arr, corrected_offsets_samples_arr] = align_beats_by_peaks(x_arr, norm_beats_vector_arr, multichannel_params.processed_channels_arr, general_params.fs);

         
         %-----------------------get single model for all candidate channels ---------------------------------------------
%          [modeled_signal, mu_arr, amp_arr, sigma_arr, comp_arr, is_merged_channels_arr] =  ...
%              merge_all_PPG_signals_gaussian_multichannel(x_arr, norm_beats_vector_arr(relevant_channel_indexes_arr,:), multichannel_params.num_peaks);
%          

         %calculate the model parameters that opmially fit the beat from all channels
         [modeled_signal_aligned, mu_arr, amp_arr, sigma_arr, comp_arr, is_merged_channels_arr] = ...
             merge_all_PPG_signals_gaussian_multichannel(aligned_x_arr, aligned_norm_beats_arr(relevant_channel_indexes_arr,:), multichannel_params.num_peaks);
         
        
         
  
%          clf(debug_figure_handler);
%          figure;
%          subplot(2,1,1);
%          hold on; plot(norm_beats_vector_arr'); plot(modeled_signal, 'linewidth', 2); axis tight
%          subplot(2,1, 2);
%          hold on; plot(aligned_norm_beats_arr'); plot(modeled_signal_aligned, 'linewidth', 2); axis tight
%          
%          close all;
         %--------------------------------------------------------------------
         channels_selected_for_merge_arr(relevant_channel_indexes_arr(find(is_merged_channels_arr==1)),beat_index) = 1;
         
         %save model parameters for current beat, for all processed channels
         all_mu_arr(beat_index,:) = mu_arr;
         all_sigma_arr(beat_index,:) = sigma_arr;
         all_amp_arr(beat_index,:) = amp_arr;
         
         all_corrected_alignment_offset(:, beat_index) = corrected_offsets_samples_arr;
         beat_length_arr(beat_index) = length(modeled_signal_aligned);
        
         
         corrected_end_index = begin_index + length(modeled_signal_aligned) - 1;
         all_modeled_beat_arr(begin_index:corrected_end_index) = modeled_signal_aligned;
         all_norm_beat_arr(:, begin_index:corrected_end_index) = aligned_norm_beats_arr;
            
         
         max_left_padding = max(corrected_offsets_samples_arr(corrected_offsets_samples_arr>=0));
         max_right_padding = abs(min(corrected_offsets_samples_arr(corrected_offsets_samples_arr<=0)));
   
         
         for ch_index = 1:num_channels
            
            if (multichannel_params.processed_channels_arr(ch_index) == 0)
                continue;
            end
            
            offset = max_left_padding - corrected_offsets_samples_arr(ch_index);
            corrected_begin_index =  begin_index + offset;
            corrected_end_index = corrected_begin_index + length(modeled_signal_aligned) - 1;
   
            x_offset_arr(ch_index, beat_index) = corrected_begin_index;
                
            
            for peak_index = 1:multichannel_params.num_peaks
                all_comp_arr(ch_index, corrected_begin_index:corrected_end_index, peak_index) = comp_arr(peak_index, :);
            end 
            
           
           
            multi_corrected_signals_arr(ch_index, corrected_begin_index:corrected_end_index) =  ...
                amp_coeff_correction_arr(ch_index, beat_index)*modeled_signal_aligned + y_offset_arr(ch_index, beat_index);
            
            for peak_index = 1:multichannel_params.num_peaks
                all_waves_arr(ch_index, corrected_begin_index:corrected_end_index, peak_index) = ...
                    amp_coeff_correction_arr(ch_index, beat_index)*all_comp_arr(ch_index, corrected_begin_index:corrected_end_index, peak_index) + y_offset_arr(ch_index, beat_index);
            end
         end
        
         
         %------------------DEBUGGING FIGURES---------------------------------------------------------------------------
         if (show_beats_debug)
             figure;
%              clf(debug_figure_handler);
             plot_index = 0;
             
             for ch_index = 1:num_channels

                if ( multichannel_params.processed_channels_arr(ch_index) == 0)
                    continue;
                end
                
                plot_index = plot_index + 1;
                
                subplot(length(relevant_channel_indexes_arr), 1, plot_index);
                hold on

                
                offset = max_left_padding - corrected_offsets_samples_arr(ch_index);
                corrected_begin_index =  begin_index + offset;
     
                corrected_end_index = corrected_begin_index + length(modeled_signal_aligned) - 1;
                
                dc_arr = baseline_signals_arr(ch_index, corrected_begin_index:corrected_end_index); 
                
                plot(aligned_x_arr, signal_arr(ch_index,corrected_begin_index:corrected_end_index), 'color', [0 1 0]); 

                if (channels_selected_for_merge_arr(ch_index,beat_index))
                    
                    multi_signal = dc_arr + multi_corrected_signals_arr(ch_index, corrected_begin_index:corrected_end_index);
                    plot(aligned_x_arr, multi_signal, 'linewidth', 2, 'color', [255 215 0]/255);
%                     plot(aligned_x_arr, dc_arr, '--w');
                    for peak_index = 1:multichannel_params.num_peaks
                        
                        comp = dc_arr + all_waves_arr(ch_index,corrected_begin_index:corrected_end_index,peak_index);
                        plot(aligned_x_arr, comp, '--c');
                    end
                end
                
                axis tight
                
                title(sprintf('Channel %d', ch_index));
            
             end
             close all;
         end
        
         %-----------------------------------------------------------------------------------------------
    end
    
    for ch_index = 1:num_channels
        %after correcting the AC component, add the previously removed wandering baseline (dynamic DC component)
        ind = find(~isnan(multi_corrected_signals_arr(ch_index,:)));
        multi_corrected_signals_arr(ch_index,ind) =  multi_corrected_signals_arr(ch_index,ind) + baseline_signals_arr(ch_index,ind);

        ind = find(isnan(multi_corrected_signals_arr(ch_index,:)));
        multi_corrected_signals_arr(ch_index,ind) = signal_arr(ch_index, ind);
        
        for peak_index = 1:multichannel_params.num_peaks
            all_waves_arr(ch_index, :, peak_index) = all_waves_arr(ch_index, : , peak_index) +  baseline_signals_arr(ch_index,:);
        end
        
        
    end
    
    %general data
    graphic_data.timestamp_arr = timestamp_arr;
    graphic_data.signal_arr = signal_arr;
    graphic_data.relevant_channel_indexes_arr = relevant_channel_indexes_arr;
    
    %beat timestamps
    params_arr.beats_begin_index_arr = beats_begin_index_arr;
    params_arr.beats_end_index_arr = beats_end_index_arr;
    
    %beat segmentation data
    params_arr.x_offset_arr = x_offset_arr;
    params_arr.beat_length_arr = beat_length_arr;
    %normalization data
    params_arr.y_offset_arr = y_offset_arr;
    params_arr.amp_coeff_correction_arr = amp_coeff_correction_arr;
    params_arr.all_corrected_alignment_offset = all_corrected_alignment_offset;
    %model results data
    params_arr.all_mu_arr = all_mu_arr;
    params_arr.all_sigma_arr = all_sigma_arr;
    params_arr.all_amp_arr = all_amp_arr;
    params_arr.all_fitted_beat_arr = all_modeled_beat_arr;
    
    
    %continous data representation 
    graphic_data.all_norm_beat_arr = all_norm_beat_arr;
    graphic_data.all_comp_arr = all_comp_arr;
    graphic_data.all_fitted_beat_arr = all_modeled_beat_arr;
    
    graphic_data.all_waves_arr = all_waves_arr;
    graphic_data.multi_corrected_signals_arr = multi_corrected_signals_arr;

    
end

