function [filtered_signals_arr] = filter_PPGs(signals_arr, fs, lowFreq, hiFreq, avg_win_sec)
   
   [filtered_signals_arr] = bandpass_filter(signals_arr, fs, lowFreq, hiFreq, avg_win_sec);
   
end
    