%this function creates a model for the given channel given gaussian decomposition
%selects the channels to be merged
%if not suitable decomposition exits, no signals are merged together
function [fitted_signal, mu_arr, amp_arr, sigma_arr, comp_arr, is_merged_channel_arr] =  merge_PPG_signals_gaussian_multichannel(x_arr, data_arr, num_peaks)


    center = 0;
    window = 0; %set center and window to fit the entire signal
%         num_peaks = 2;
    peakshape = 1; %gaussian model
    extra = []; %sed in the Pearson, exponentially-broadened Gaussian, Gaussian/Lorentzian blend, bifurcated Gaussian, and Breit-Wigner-Fano shapes to fine-tune the peak shape. In version 5, 'extra' can be a vector of different extra values for each peak).
    NumTrials = 1;%if the result is not consistent, increase the numTrials until the result is stable
    start = 0; %specification of the first guess for he peak positions and width
    autozero = 0; %sets the baseline correction mode (
                %0: do not substact baseline from segment
                %1: interpolates a linear baseline from the edges of the data segment;
                %2: qaudratic curve; 
                %3: compensates for a flat baseline without reference to the signal itself

    fixedparameters = []; %  specifies fixed values for widths (shapes 10, 11) or positions (shapes 16, 17)
    plot_res = 0; %do not show results
    bipolar = 0;%'bipolar' = 0 constrain peaks heights to be positive; 1 = alows positive and negative peak heights
    minwidth = 0; %sets the minimum allowed peak width. The default if not specified is equal to the x-axis interval. Must be a vector of minimum  widths, one value for each peak, if the multiple peak shape is chosen.

    delta = 1; %'DELTA' (14th input argument) controls the restart variance when  NumTrials>1
    clipheight = [];

    
    num_channels = size(data_arr, 2);
    is_merged_channel_arr = ones(1, num_channels);
    flag = true;
    
    while (flag)
        channel_indexes_to_merge_arr = find(is_merged_channel_arr == 1);
  
            
        data_vector = [x_arr, data_arr(:,channel_indexes_to_merge_arr)];
        [FitResults, goodness_fit_arr, baseline, coeff, BestStart, xi, yi] = peakfit_multichannel(data_vector,center,window,num_peaks,peakshape, extra, NumTrials, start, autozero, fixedparameters, plot_res, bipolar);

        mu_arr = FitResults(:,2);
        amp_arr = FitResults(:,3);
        sigma_arr = FitResults(:,4);


        [fitted_signal, comp_arr] = estimateSignalFromPeaks(x_arr, mu_arr, sigma_arr, amp_arr);

         %if a channel is too distant from the merge signal, exclude it

        error_arr = zeros(1, num_channels);

        for ch_ind = 1:length(channel_indexes_to_merge_arr)
             ch_index = channel_indexes_to_merge_arr(ch_ind);
             err = abs(data_arr(:,ch_index) - fitted_signal);
             percentage_err = median(err./fitted_signal); %error from model
             error_arr(ch_index) = percentage_err;
        end

        ind = find(error_arr > 0.15);

        if (isempty(ind))
                flag = false;
        else
             is_merged_channel_arr(ind) = 0;
        end
         
        channel_indexes_to_merge_arr = find(is_merged_channel_arr == 1);
  
        if (length(channel_indexes_to_merge_arr) < 2)
            flag = false;
            fitted_signal = nan*ones(size(x_arr));
            mu_arr = nan;
            amp_arr = nan;
            sigma_arr = nan; 
            comp_arr = nan*ones(length(x_arr), num_peaks);
            is_merged_channel_arr = []; 
        end
        
        
    end
%     
%     if (~isempty(is_merged_channel_arr))
%         is_merged_channel_arr
%     end
    
end

%--------------USE CONSTRAINTS-------------------------------------

%         min_values_arr = [-Inf, -Inf, -Inf, -Inf];
%         max_values_arr = [Inf, Inf, Inf, Inf];
%    
        
%         [FitResults2, b, baseline, coeff, BestStart, xi, yi] = peakfit_constrained_multichannel(data_vector,center,window,num_peaks,peakshape, extra, NumTrials, start, autozero, fixedparameters, plot_res, bipolar, minwidth, delta, clipheight, ...                                                              
%                                                                     min_values_arr, max_values_arr);
% 
%         
%           mu_arr2 = FitResults2(:,2);
%          amp_arr2 = FitResults2(:,3);
%          sigma_arr2 = FitResults2(:,4);
% 
% 
%        
%         [fitted_signal2, comp_arr2] = estimateSignalFromPeaks(x_arr, mu_arr2, sigma_arr2, amp_arr2);