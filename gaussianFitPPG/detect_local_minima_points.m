function [minima_peaks_arr] = detect_local_minima_points(timestamp_arr, signal, maxima_peaks_arr, ...
                                fs, min_detection_type, show_segmentation_results, title_str)


    %min_detection_type: 1 - define systolic foot as the minima between two local maxima
    %                2  - define systolic foot as the closest saddle point to the second maximum
    minima_peaks_arr = [];
    
    if (min_detection_type ~= 1) && (min_detection_type ~= 2)
        min_detection_type = 1;
    end
    
    for k = 2:length(maxima_peaks_arr)
        %detection option 2------------
        %get the last minimum point
      
        values = signal(maxima_peaks_arr(k-1):maxima_peaks_arr(k));


        [global_min, global_min_index] = min(values);

        [zero_crossing_arr, extrema_type_arr] = findZeroCrossing(values);

        %get possible maxima
        ind_max = find(extrema_type_arr == 1);

        max_locs_arr = zero_crossing_arr(ind_max);
        max_val_arr = values(max_locs_arr);

        %ignore maxima points left to the global minimum
        bad_ind = find(max_locs_arr < global_min_index);
        max_locs_arr(bad_ind) = [];
        max_val_arr(bad_ind) = [];

        %now find the lowest maxima right to the min point that is 50% of the global maxima

        max_halp_peak_val = global_min + (values(end) - global_min)*0.5;
        ind = find(max_val_arr > max_halp_peak_val);
        if (~isempty(ind))
            new_end_ind = max_locs_arr(ind(1));
            values = values(1:new_end_ind);

             ind = find(zero_crossing_arr > new_end_ind);
             zero_crossing_arr(ind) = [];
             extrema_type_arr(ind) = [];
        end

        %get minimum point
        ind = find(extrema_type_arr == -1);
        if (~isempty(ind))
            min_index = zero_crossing_arr(ind(end));
        else
            
            continue;
        end

            
            
            
        if (min_detection_type == 1)
        %detection option 1:------------
        %define minima as the minimum point
        
            min_index = global_min_index;
        
        end
        
        if (min_detection_type == 2)
          
            %now find if there is a saddle point in between the minima and the maximum

            ext2 = diff(diff(values(min_index:end)));
            [zero_crossing_arr, extrema_type_arr] = findZeroCrossing(ext2);
            ind  = find(extrema_type_arr == 1);
            if (~isempty(ind))
                min_index = min_index + zero_crossing_arr(ind(1)) - 1;
            end
        end

        %  option 3:
        %find the longest segment with the constant slope and define it as the systolic phase
        

        %-----------------------------
          
        systolic_foot = maxima_peaks_arr(k-1) + min_index - 1;
        %now check if there are any saddle points, define the foot as the saddle point
        minima_peaks_arr(length(minima_peaks_arr)+1) = systolic_foot;
    end
    
    if (show_segmentation_results)
        
        visible_frame = 30;
        frame_size = visible_frame; 
        figure_handler = figure;
%         jFrame = get(handle(gcf),'JavaFrame');
%         jFrame = get(figure_handler,'JavaFrame');
%         drawnow; pause(0.01); 
%         jFrame.setMaximized(true);   % to maximize the figure
        clf(figure_handler);
        hold on
        plot(timestamp_arr, signal);
        plot(timestamp_arr(maxima_peaks_arr), signal(maxima_peaks_arr), '*r');
        plot(timestamp_arr(minima_peaks_arr), signal(minima_peaks_arr), '*g');
%         
%         plot(signal);
%         plot((systolic_peaks_arr), signal(systolic_peaks_arr), '*r');
%         plot((systolic_foot_arr), signal(systolic_foot_arr), '*g');
%         axis tight
        xlabel(' time [s]');
        legend('signal', 'peaks', 'foot');
        title(title_str);
        
        if (frame_size > 0)
            add_slidebar(frame_size, max(timestamp_arr));
%             add_slidebar(fs*frame_size, length(timestamp_arr));
        end
    end
end