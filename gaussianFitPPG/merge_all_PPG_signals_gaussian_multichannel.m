%%this function models a beat from multiple channels
%using a mixture of n gausians

function [fitted_signal, mu_arr, amp_arr, sigma_arr, comp_arr, is_merged_channel_arr] =  merge_all_PPG_signals_gaussian_multichannel(x_arr, data_arr, num_peaks)

    %#codegen
    center = 0;
    window = 0; %set center and window to fit the entire signal
%         num_peaks = 2;
    NumTrials = 1;%if the result is not consistent, increase the numTrials until the result is stable
    start = 0; %specification of the first guess for he peak positions and width
   
    plot_res = 0; %do not show results
    bipolar = 0;%'bipolar' = 0 constrain peaks heights to be positive; 1 = alows positive and negative peak heights
  
    
    num_channels = size(data_arr, 1);
    is_merged_channel_arr = ones(1, num_channels);
    
    channel_indexes_to_merge_arr = (is_merged_channel_arr == 1);

    [FitResults, goodness_fit_arr1, BestStart, xi, yi] = peakfit_multichannel(x_arr,data_arr(channel_indexes_to_merge_arr,:) ,center,window, num_peaks, NumTrials, start, plot_res, bipolar);
%     [FitResults, goodness_fit_arr, baseline, coeff, BestStart, xi, yi] = peakfit_weighted_multichannel(data_vector,center,window,num_peaks,peakshape, extra, NumTrials, start, autozero, fixedparameters, plot_res, bipolar);

          
    mu_arr = FitResults(:,2)';
    amp_arr = FitResults(:,3)';
    sigma_arr = FitResults(:,4)';

    [fitted_signal, comp_arr] = estimateSignalFromPeaks(x_arr, mu_arr, sigma_arr, amp_arr);

end

%--------------USE CONSTRAINTS-------------------------------------

%         min_values_arr = [-Inf, -Inf, -Inf, -Inf];
%         max_values_arr = [Inf, Inf, Inf, Inf];
%    
        
%         [FitResults2, b, baseline, coeff, BestStart, xi, yi] = peakfit_constrained_multichannel(data_vector,center,window,num_peaks,peakshape, extra, NumTrials, start, autozero, fixedparameters, plot_res, bipolar, minwidth, delta, clipheight, ...                                                              
%                                                                     min_values_arr, max_values_arr);
% 
%         
%           mu_arr2 = FitResults2(:,2);
%          amp_arr2 = FitResults2(:,3);
%          sigma_arr2 = FitResults2(:,4);
% 
% 
%        
%         [fitted_signal2, comp_arr2] = estimateSignalFromPeaks(x_arr, mu_arr2, sigma_arr2, amp_arr2);