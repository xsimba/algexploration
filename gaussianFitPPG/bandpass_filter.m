function [filtered_signals_arr] = bandpass_filter(signals_arr, fs, lowFreq, hiFreq, avg_win_sec)
   
    %each row represents a data vector
    if (size(signals_arr, 1) > size(signals_arr, 2))
        signals_arr = signals_arr';
    end
    
    num_channels = size(signals_arr, 1);
    filter_delay = 0;
    
    b_coeffs_arr = nan;
    a_coeffs_arr = nan;
    filter_type = 'kaiserwin'; %equiripple
    
    global base_codepath;
    
    prepared_filters_dir = sprintf('%s/utils/filters/perpared_fir_filters/', base_codepath);
    try
        filter_data = load(sprintf('%sbp_%s_%2.2f_%2.2f_%d.mat', prepared_filters_dir, filter_type, lowFreq, hiFreq, fs));
        filter_delay = round(median(grpdelay(filter_data.Hd)));
    catch      
        filter_order = 2; %default for 


        fprintf('Predefined FIR filter [%2.2f %2.2f] Hz (fs = %d) not found. Using Butterworth...\n', lowFreq, hiFreq, fs);
             [b_coeffs_arr, a_coeffs_arr] = create_butter_bp_filter(fs, lowFreq, hiFreq, filter_order);
    end
   
   
    
    %replace nan with mean signal value
    corrected_signals_arr = replace_missing_values(signals_arr,  fs, avg_win_sec);
   
    %pad signals to remove filtering artifacts
    
    filtered_data_length = size(corrected_signals_arr, 2);
    if (filtered_data_length > filter_delay)
        filtered_data_length = filtered_data_length - filter_delay;
    end
    filtered_signals_arr = nan*ones(size(corrected_signals_arr, 1), filtered_data_length);

    
    %replace nan with mean signal value
    for ch_index = 1:num_channels
     
         %now filter data
         if (~isnan(b_coeffs_arr))
             
            %padd to remove some cheby filter artifacts
            padding_arr = corrected_signals_arr(1:min(length(corrected_signals_arr), 30000));
            padded_signal = [padding_arr corrected_signals_arr(ch_index, :)];
            
            filtered_signal = filter(b_coeffs_arr, a_coeffs_arr, padded_signal);
              
            filtered_signal = filtered_signal((length(padding_arr)+1):end);
            filtered_signals_arr(ch_index,:) = filtered_signal; 
         else

            
            filtered_signal = filter(filter_data.Hd, corrected_signals_arr(ch_index,:)); 
         
            %remove FIR constant group delay
            if (length(filtered_signal) > filter_delay)
                filtered_signals_arr(ch_index,:) = filtered_signal((filter_delay+1):end);
            else
                filtered_signals_arr(ch_index,:) = filtered_signal;
            end
         end
    end
    
  
end
    