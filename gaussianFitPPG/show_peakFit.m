%this method shows the multichannel PPG modeling
function [] = show_peakFit(data, channels_str_arr)

    timestamp_arr = data.timestamp_arr;
    signal_arr = data.signal_arr;
    relevant_channel_indexes_arr = data.relevant_channel_indexes_arr;
      
    all_norm_beat_arr = data.all_norm_beat_arr;
    all_comp_arr = data.all_comp_arr;
    all_fitted_beat_arr = data.all_fitted_beat_arr;
    
    all_waves_arr = data.all_waves_arr;
    multi_corrected_signals_arr = data.multi_corrected_signals_arr;
    num_peaks = size(all_waves_arr, 3);
    
    %---------------Show AC modeling only-----------------------------------------------------------------------------
    figure;
    hold on

    for ch_ind = 1:length(relevant_channel_indexes_arr)
        ch_index = relevant_channel_indexes_arr(ch_ind);
        plot(timestamp_arr, all_norm_beat_arr(ch_index, :), 'linewidth', 1, 'color', [0 1 0]);
        
    end


     
     for  peak_index = 1:num_peaks      
        plot(timestamp_arr, all_comp_arr(ch_index, :, peak_index), '--', 'color', [255 204 102]/255);
     end 

     plot(timestamp_arr, all_fitted_beat_arr, 'y', 'linewidth', 2);

     xlabel('time [s]');
     ylabel('Normalized beats');
     axis tight

     legend_str_arr = {};

      for ch_ind = 1:length(relevant_channel_indexes_arr)
        ch_index = relevant_channel_indexes_arr(ch_ind);
    
        legend_str_arr{length(legend_str_arr)+1} = channels_str_arr{ch_index};
     end

     
     for  peak_index = 1:num_peaks  
         legend_str_arr{length(legend_str_arr)+1} = sprintf('Component %d', peak_index);
     end

     legend_str_arr{length(legend_str_arr)+1} = 'Multichannel PPG';

     legend(legend_str_arr);
 
     add_slidebar(3, max(timestamp_arr));


    %------------------Show AC modeling on baseline -----------------------------------------------------------------


     figure;
     num_relevant_channels = length(relevant_channel_indexes_arr);
     
     h_arr = zeros(1,num_relevant_channels);

     for ch_ind = 1:length(relevant_channel_indexes_arr)
        ch_index = relevant_channel_indexes_arr(ch_ind);
   
        h_arr(ch_ind) = subplot(num_relevant_channels, 1, ch_ind);
        hold on
        
       
        plot(timestamp_arr, signal_arr(ch_index, :), 'g');
        plot(timestamp_arr, multi_corrected_signals_arr(ch_index,:),'y', 'linewidth', 1);


        for peak_index = 1:num_peaks
            plot(timestamp_arr, all_waves_arr(ch_index, :, peak_index), '--', 'color', [255 204 102]/255);
        end


        axis tight

        legend_str_arr = {};
        legend_str_arr{length(legend_str_arr) + 1} =  channels_str_arr{ch_index};
        legend_str_arr{length(legend_str_arr) + 1} = 'DC';
        legend_str_arr{length(legend_str_arr) + 1} = 'Multichannel PPG';

        for peak_index = 1:num_peaks
            legend_str_arr{length(legend_str_arr) + 1} = sprintf('Component %d', peak_index);
        end

        title(sprintf('Channel %s', channels_str_arr{ch_index}));

     end

    xlabel('time [s]');
    linkaxes(h_arr, 'x');
    add_slidebar(3, max(timestamp_arr));
             
end
   