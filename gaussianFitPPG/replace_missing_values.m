function signals_arr = replace_missing_values( signals_arr,  fs, avg_win_sec)


    num_channels = size(signals_arr, 1);
    signal_length = size(signals_arr, 2);
    
   
    for ch_index = 1:num_channels
        
         ind_not_nan = ~isnan(signals_arr(ch_index,:));
         mean_PPG = mean(signals_arr(ch_index,ind_not_nan));
         
         
         ind_nan_arr = find(isnan(signals_arr(ch_index,:)));
         while (~isempty(ind_nan_arr))
             
            for nan_ind = 1:length(ind_nan_arr)

                 %for each found nan index, replace it with mean value data
                 nan_index = ind_nan_arr(nan_ind);
                 win_begin_index = max(1, ceil(nan_index - fs*avg_win_sec));
                 win_end_index   = min(signal_length, ceil(nan_index + fs*avg_win_sec));
                 neighbour_data = signals_arr(ch_index,win_begin_index:win_end_index);

                 neighbour_data(isnan(neighbour_data))= [];
                 if (isempty(neighbour_data))
                     neighbour_data = mean_PPG;
                 end

                 signals_arr(ch_index,nan_index) = mean(neighbour_data);

            end
            
            ind_nan_arr = find(isnan(signals_arr(ch_index,:)));
             
         end
    end
end

