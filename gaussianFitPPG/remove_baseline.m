function [correct_signal, baseline_signal] = remove_baseline(signal, locs_arr)

    baseline_correction_range = locs_arr(1):locs_arr(end);
    baseline_signal = zeros(size(signal));
    baseline_signal(baseline_correction_range) = interp1(locs_arr, signal(locs_arr), ...
            baseline_correction_range, 'spline');
    
    correct_signal = signal - baseline_signal;
    
end