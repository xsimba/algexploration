%% Need to import timestart_unix and timeend_unix: vector array of start and end times for each dataset in alphabetic order
% datasets in format as imported from RC (v5 structure, not aligned)
% data named in format: data_(name)_(electrode type)        ex: data_Amy_Acree

% Manually enter in timestart_rel and timeend_rel --> in seconds from start
% of segment --> can use output from ECG_SNR to more specifically select
% data region for SNR calculations
% timestart and timeend must be in order as on list
% within each row - in order from Acree, Hudson, PtIr, SS, UHV, UHVclasp


% Multiply output of timetounix converter by 1000 --> should be in 10^12
if timestart_unix(1)<1E10  % if in ms unix time convert to us unix time
    timestart_unix = timestart_unix*1000;
end
if timeend_unix(1)<1E10  % if in ms unix time convert to us unix time
    timeend_unix = timeend_unix*1000;
end
% timestart_rel = [65,105,100,85,65,115,...Amy
%     45,90,100,50,120,120,...Arjun
%     45,60,55,80,45,25,...Arvind
%     60,20,55,8,85,85,...David
%     60,60,60,50,60,70,...Jasmine
%     90,80,100,120,60,50,...Mike
%     55,150,100,75,50,105,...Monque
%     58,55,80,15,80,80,...Praveen
%     11,35,70,30,55,45,...Reana
%     90,90,100,90,100,35];%Tiffany
% timeend_rel = [95,135,130,115,95,145,...Amy
%     75,120,130,80,150,150,...Arjun
%     75,90,85,110,75,55,...Arvind
%     90,50,80,28,110,115,...David
%     90,90,90,80,90,100,...Jasmine
%     120,105,130,150,90,80,...Mike
%     85,175,120,90,80,120,...Monique
%     75,75,110,30,110,105,...Praveen
%     21,50,90,60,75,75,...Reana
%     120,120,130,120,130,65];%Tiffany


% timestart_rel = [130,100,135,125,115,115, ...Asif
%     55,40,30,30,30,30,...Geoffrey
%     100,70,75,45,20,80,...Ryan
%     40, 40, 110, 85, 90, 30, ...Swati
% ];
% timeend_rel = [160,130,165,155,145,145, ...Asif
%     85,70,60,60,60,60,...Geoffrey
%     125,100,90,70,50,105,...Ryan
%     80, 80, 140, 115, 120,70, ...Swati
% ];
timestart_rel = [95,90,60,65,80,110,...Dafina
   30,20,40,40,15,45,...Lindsay
    130,120,105,160,45,70,...Narendra
    110,100,80,25,50,90, ...Will
    125,135,85,105,145,95,...Yelei
];
timeend_rel = [135,115,90,85,105,130, ...Dafina
   60,50,70,70,40,70,...Lindsay
    155,140,120,175,70,80,...Narendra
    140,125,110,40,65,120, ...Will
    155,160,100,120,165,120,...Yelei
];

format bank

datdir = 'C:\Users\amy.liao\Documents\GSRData\database\071415_ElectrodeVar_ECG\';
savefolder = [date,'-AnalysisPlots_manualtime\'];
mkdir(datdir,savefolder);
cd ([datdir, savefolder]);
list = who('data*');
offset = 0;
snrwindow = 8;
c=1;
filename = 'CI-SNR_avg+std';
filename2 = 'CI-SNR_avg+std_2';

e = 1;
while exist([datdir, savefolder,filename,'.xslx'])
    e=e+1;
    filename = [filename,'(',num2str(e),')'];
    filename2 = [filename2,'(',num2str(e),')'];
end
CI_snr_summary = {date,'','','','','';'offset',offset,'','','','';'snrwindow',snrwindow,'','','','';'','','CI-avg','CI-std','SNR-avg','SNR-std'};
CI_snr_summary2 = {date,'','','',;'offset',offset,'','';'snrwindow',snrwindow,'','';'','','CI:avg(std)','SNR:avg(std)'};



% Choose 1
% subplotrow = 3; subplotcol = 3; subplotind = [1 2 3 ; 4 5 6 ]; subplotindsnr = [7 8 9 ];  plotset = 'ECG-CI-SNR';
% subplotrow = 4; subplotcol = 3; subplotind = [1 2 3 7 8 9; 4 5 6 10 11 12]; plotset = 'ECG-CI'; clear subplotindsnr % plots ECG and CI
subplotrow = 6; subplotcol = 3; subplotind = [1 2 3 10 11 12; 4 5 6 13 14 15]; subplotindsnr = [7 8 9 16 17 18]; plotset = 'ECG-CI-SNR';% plots ECG, CI, and SNR



for i = 1:length(list)
    name = strsplit(list{i},'_');
    

    % format data_structure
    eval(['temp_data = ',list{i},'.physiosignal.ecg.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.unixTimeStamps;']); 
    
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_all.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_all.time_unix_all = temp_time;']);
    
    % ECG signal cropped to time in timestart and timeend
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_time_seconds = (ECGdata.' name{2},'.',name{3},'.ECG_time_unix - ECGdata.' name{2},'.',name{3},'.ECG_time_unix(1))/1000;']);

    
    % beats
    eval(['temp_data = ',list{i},'.physiosignal.ecg.beats.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.beats.unixTimeStamps;']); 

    eval(['ECGdata.' name{2},'.',name{3},'.beats_all.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.beats_all.time_unix_all = temp_time;']);
    
    
    if ECGdata.(name{2}).(name{3}).ECG_time_seconds(end)<180
        timeend_unix(i) = ECGdata.(name{2}).(name{3}).ECG_time_unix(end);
    end
    eval(['ECGdata.' name{2},'.',name{3},'.beats_signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.beats_time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.beats_time_seconds = (ECGdata.' name{2},'.',name{3},'.beats_time_unix - ECGdata.' name{2},'.',name{3},'.ECG_time_unix(1))/1000;']);
    
    
    % CI
    eval(['temp_data = ',list{i},'.physiosignal.ecg.confidenceraw.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.confidenceraw.unixTimeStamps;']); 
    
    eval(['ECGdata.' name{2},'.',name{3},'.CI_all.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.CI_all.time_unix_all = temp_time;']);
    
    eval(['ECGdata.' name{2},'.',name{3},'.CI_signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.CI_time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.CI_time_seconds = (ECGdata.' name{2},'.',name{3},'.CI_time_unix - ECGdata.' name{2},'.',name{3},'.ECG_time_unix(1))/1000;']);        

    % Calculate CI stats
    CIind = intersect(find(ECGdata.(name{2}).(name{3}).CI_time_seconds>timestart_rel(i)),find(ECGdata.(name{2}).(name{3}).CI_time_seconds<timeend_rel(i)));

    ECGdata.(name{2}).(name{3}).CI_average = nanmean(ECGdata.(name{2}).(name{3}).CI_signal(CIind(1):CIind(end)));
    eval(['fprintf(''%s - %s: CI_average = %0.2f\n'',name{2},name{3},ECGdata.',name{2},'.',name{3},'.CI_average);']);
    
    ECGdata.(name{2}).(name{3}).CI_std = nanstd(ECGdata.(name{2}).(name{3}).CI_signal(CIind(1):CIind(end)));
    eval(['fprintf(''%s - %s: CI_std = %0.2f\n'',name{2},name{3},ECGdata.',name{2},'.',name{3},'.CI_std);']);
    
    % Calculate SNR
    SNRind = intersect(find(ECGdata.(name{2}).(name{3}).ECG_time_seconds>timestart_rel(i)),find(ECGdata.(name{2}).(name{3}).ECG_time_seconds<timeend_rel(i)));
    SNRsignal = ECGdata.(name{2}).(name{3}).ECG_signal(SNRind(1):SNRind(end));
    SNRtimestamps = ECGdata.(name{2}).(name{3}).ECG_time_seconds(SNRind(1):SNRind(end));
    beatsind = intersect(find(ECGdata.(name{2}).(name{3}).beats_time_seconds>timestart_rel(i)),find(ECGdata.(name{2}).(name{3}).beats_time_seconds<timeend_rel(i)));
    SNRbeats = ECGdata.(name{2}).(name{3}).beats_time_unix(beatsind(1):beatsind(end));
    SNRbeats = (SNRbeats-ECGdata.(name{2}).(name{3}).ECG_time_unix(1))/1000;
        

    [SNR,t_snr] = SNR_checker_v2(SNRsignal,SNRtimestamps,SNRbeats,'ecg',snrwindow);
   
    ECGdata.(name{2}).(name{3}).snr_signal = SNR;
    ECGdata.(name{2}).(name{3}).snr_time_seconds = t_snr;

    ECGdata.(name{2}).(name{3}).snr_average = nanmean(SNR);
    ECGdata.(name{2}).(name{3}).snr_std = nanstd(SNR);
    

    fprintf('%s - %s: snr_avg = %0.2f\n',name{2},name{3},ECGdata.(name{2}).(name{3}).snr_average);
    fprintf('%s - %s: snr_std = %0.2f\n\n',name{2},name{3},ECGdata.(name{2}).(name{3}).snr_std);
    
%     xlswrite(filename,{name{2},name{3},ECGdata.(name{2}).(name{3}).snr_average});
%     xlswrite(filename,{name{2},name{3},ECGdata.(name{2}).(name{3}).snr_std});


    CI_snr_summary(end+1,:) ={name{2},name{3},ECGdata.(name{2}).(name{3}).CI_average,ECGdata.(name{2}).(name{3}).CI_std,ECGdata.(name{2}).(name{3}).snr_average,ECGdata.(name{2}).(name{3}).snr_std};
    CI_snr_summary2(end+1,:) ={name{2},name{3},[num2str(round(ECGdata.(name{2}).(name{3}).CI_average,2)),' (',num2str(round(ECGdata.(name{2}).(name{3}).CI_std,2)),')'],[num2str(round(ECGdata.(name{2}).(name{3}).snr_average,2)),' (',num2str(round(ECGdata.(name{2}).(name{3}).snr_std,2)),')']};


    if ~isfield(ECGdata,'stats')|| ~isfield(ECGdata.stats,name{3})
        ECGdata.stats.(name{3}).snr_average(1) = nanmean(SNR);
        ECGdata.stats.(name{3}).snr_std(1) = nanstd(SNR);
        ECGdata.stats.(name{3}).CI_average(1) = ECGdata.(name{2}).(name{3}).CI_average;
        ECGdata.stats.(name{3}).CI_std(1) = ECGdata.(name{2}).(name{3}).CI_std;    
    else
        ECGdata.stats.(name{3}).snr_average(end+1) = nanmean(SNR);
        ECGdata.stats.(name{3}).snr_std(end+1) = nanstd(SNR);
        ECGdata.stats.(name{3}).CI_average(end+1) = ECGdata.(name{2}).(name{3}).CI_average;
        ECGdata.stats.(name{3}).CI_std(end+1) = ECGdata.(name{2}).(name{3}).CI_std;
    end
    
%     % plot data vs SNR plots
%     f= figure;
%     A(1)=subplot(2,1,1);plot(data.timestamps,data.(SigType).signal);hold on;%plot(data.(SigType).beats.signal,data.(SigType).beats.debug.usamp,'r+'); title(['original signal ' SigID]);ylabel('ECG')
%     A(2)=subplot(2,1,2);plot(t_snr,SNR);title('SNR');ylabel('SNR');hline(0.5)
% %         A(3)=subplot(3,1,3);plot(t_snr,mean(STDqrs,2)); title('SD of the avarage beat');ylabel('SD');xlabel('Time (s)')
%         linkaxes(A,'x');
%     title(['SNR = ',num2str(nanmean(SNR))]);
%         saveas(f,[datdir, 'AnalysisPlots\','ECG-snr-',name{2},'-',name{3}]);
%         saveas(f,[datdir, 'AnalysisPlots\','ECG-snr-',name{2},'-',name{3},'.jpg']);   



    % Plot figure
    figure(floor((i-1)/6)+1)
    
    eval(['h(',num2str(i),',1) = subplot(',num2str(subplotrow),',',num2str(subplotcol),',',num2str(subplotind(1,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.ECG_time_seconds, ECGdata.' name{2},'.',name{3},'.ECG_signal);']);
    xlabel('time (sec)');
    ylabel('ECG');
    title({['ECG-raw: ',name{2},' - ',name{3},],['Avg SNR = ',num2str(ECGdata.(name{2}).(name{3}).snr_average)]});
    
    eval(['h(',num2str(i),',2) = subplot(',num2str(subplotrow),',',num2str(subplotcol),',',num2str(subplotind(2,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.CI_time_seconds, ECGdata.' name{2},'.',name{3},'.CI_signal,''.'',''markersize'',20);']);
    eval(['line(xlim,[ECGdata.' name{2},'.',name{3},'.CI_average, ECGdata.' name{2},'.',name{3},'.CI_average],''Color'',''r'');']);
    xlabel('time (sec)');
    ylabel('CI');
    title(['ECG-CI: ',name{2},' - ',name{3}]);

    if exist('subplotindsnr') 
    eval(['h(',num2str(i),',3) = subplot(',num2str(subplotrow),',',num2str(subplotcol),',',num2str(subplotindsnr(1,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.snr_time_seconds, ECGdata.' name{2},'.',name{3},'.snr_signal);']);
    eval(['line(xlim,[ECGdata.' name{2},'.',name{3},'.snr_average, ECGdata.' name{2},'.',name{3},'.snr_average],''Color'',''r'');']);
    xlabel('time (min)');
    ylabel('SNR');
    title(['ECG-SNR: ',name{2},' - ',name{3},': Avg = ',num2str(ECGdata.(name{2}).(name{3}).snr_average)]);    
    end
    
    
    if c<6
        c=c+1;
    else
        c=1;
        set(gcf, 'Position', get(0,'Screensize')); 
        saveas(figure(floor((i-1)/6)+1),[datdir, savefolder,plotset,'-',name{2}]);
        saveas(figure(floor((i-1)/6)+1),[datdir, savefolder,plotset,'-',name{2},'.jpg']);
        linkaxes(h(i-5:i,:),'x'); % links all axes
        
    end
    
    
    eval(['linkaxes(h(',num2str(i),',:), ''x'');']); % links axis for each electrode
    axis([0 180 -inf inf]);

    
end



save([datdir, savefolder,plotset],'ECGdata');
xlswrite([filename,'.xlsx'],CI_snr_summary);
xlswrite([filename2,'.xlsx'],CI_snr_summary2);