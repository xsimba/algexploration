cat = {'Acree_rest', 'Acree_bike', 'SS_rest', 'SS_bike', 'Hudson_rest','Hudson_bike',...
    'UHV_rest','UHV_bike','PtIr_rest','PtIr_bike'};
cat=strrep(cat,'_','-');
timestartall = [7, 10,    14.5, 17.5, 23.5, 26.5, 31,    33.5, 40, 43];  
timeendall   = [10,11.75, 17.5, 19.5, 26.5, 28,   33.75, 34.75, 43, 45]; %modified
% timeendall   = [10, 12, 17.5, 19.5, 26.5, 28.5, 34, 36, 43, 45]; % based on recorded timestamps

offset = 0.5;


for i = 1:10


timestart = timestartall(i)+offset;
timeend = timeendall(i)-offset;

Empatica_mod = data_Empatica.GSR.GSR_data(intersect(find(time_Empatica>timestart),find(time_Empatica<=timeend)));
Sami_mod = data_Sami.physiosignal.gsr.phasic.signal(intersect(find(time_Sami>timestart),find(time_Sami<=timeend)));
timenew = time_Empatica(intersect(find(time_Empatica>timestart),find(time_Empatica<=timeend)));


% downsample Hermes data
Sami_mod = downsample(Sami_mod, 8); %1920 Hz Sami --> 4 Hz

% upsample Empatica data
% Empatica_mod = interp(Empatica_mod,8);
% % timenew2 = min(timenew):(0.25/60/8):max(timenew);
% timenew = 1:length(Empatica_mod);

% Adjust length
minlen = min(length(Empatica_mod),length(Sami_mod));
Empatica_mod = Empatica_mod(1:minlen);
Sami_mod = Sami_mod(1:minlen)';

% [fig sstruct] = Correlation(Empatica_mod, Sami_mod)


% Normalize data
Empatica_mod_norm = (Empatica_mod - min(Empatica_mod))/(max(Empatica_mod)-min(Empatica_mod));
Sami_mod_norm = (Sami_mod - min(Sami_mod))/(max(Sami_mod)-min(Sami_mod));

% Align data using cross correlation
[crosscorr,lag]= xcorr(Empatica_mod_norm, Sami_mod_norm);
lagzero = find(lag==0);
% [~,I] = max(abs(crosscorr));  % allows any adjustment of lag
maxcorr = max((crosscorr(lagzero-10:lagzero+10)));
I = find(crosscorr==maxcorr); % limits lag adjustmentto within 10 s

lagDiff = lag(I);
lagDiff = 6;
timenew_align = timenew(1:end-abs(lagDiff));

fprintf('%s: lag = %d\n',cat{i},lagDiff);

if lagDiff>= 0
    Empatica_mod_norm_align = Empatica_mod_norm(1+lagDiff:end);
    Sami_mod_norm_align = Sami_mod_norm(1:end-lagDiff);
    
    Empatica_mod_align = Empatica_mod(1+lagDiff:end);
    Sami_mod_align = Sami_mod(1:end-lagDiff);
else
    Empatica_mod_norm_align = Empatica_mod_norm(1:end-abs(lagDiff));
    Sami_mod_norm_align = Sami_mod_norm(1+abs(lagDiff):end);
    
    Empatica_mod_align = Empatica_mod(1:end-abs(lagDiff));
    Sami_mod_align = Sami_mod(1+abs(lagDiff):end);
end


    


% Pearson's Correlation Coefficient
R = corrcoef(Empatica_mod, Sami_mod);
% fprintf('%s: r = %d\n',cat{i},R(2,1));

R2 = corrcoef(Empatica_mod_norm, Sami_mod_norm);
% fprintf('%s: r_norm = %d\n',cat{i},R2(2,1));

R3 = corrcoef(Empatica_mod_norm_align, Sami_mod_norm_align);
fprintf('%s: r_align = %d\n',cat{i},R3(2,1));

%     subplot(4,2,[2,4]),scatter(Empatica_mod_norm_align,Sami_mod_norm_align);
%     line([0,1],[0,1],'Color','r');
%     xlabel('Empatica-aligned');
%     ylabel('Sami-aligned');
%     title(['r^2-aligned = ',num2str(R3(2,1)^2)]);

    


% ANSLABeda(Empatica_mod, cat{i},data_dir);
% ANSLABeda(Sami_mod, cat{i},data_dir);



% Plot
figure(i); 

subplot(4,2,1),plot(timenew,Empatica_mod);
title([cat{i} ' - Empatica']);

subplot(4,2,3),plot(timenew,Sami_mod);
title([cat{i} ' - Sami']);
subplot(4,2,5), plot(timenew,Empatica_mod_norm, timenew,Sami_mod_norm);
title('Normalized data');

subplot(4,2,7),plot(timenew_align,Empatica_mod_norm_align,timenew_align,Sami_mod_norm_align);
title('Normalized data-aligned');

subplot(4,2,[2,4]),plot(lag,crosscorr);
line([lag(I),lag(I)],ylim,'Color','r');
xlabel('lag');
ylabel('crosscorrelation');


delete(subplot(4,2,[6,8]))
fig = subplot(4,2,[6,8]);
[fig sstruct,r,SSE,rho] = Correlation_test(fig, Empatica_mod_norm_align, Sami_mod_norm_align);
hold off;
fprintf('%s: rho_align = %d\n',cat{i},rho);
fprintf('\n\n');
end