%% Non-paired data (not rest+activity segments --> use v2 for paired datasets)
% Need to run plot_Empatica_Sami first to get data in correct structure
% format

% cat, timestartall,and timeendall are arrays contining info for each
% sement to analyze
cat = {'UHV','Acree','SS','PtIr','Hudson','Athena4'}; % category name
cat=strrep(cat,'_','-');
timestartall = [28.75,35,50.5,61.5,48.5,56.5];  % start time in minutes (from plot_Empatica_Sami plot)
timeendall   = [31,38,55,64.5,51.5,57.5]; % end time in minutes(from plot_Empatica_Sami plot)
% timeendall   = [10, 12, 17.5, 19.5, 26.5, 28.5, 34, 36, 43, 45]; % Amy_exercise dataset
offset = 0; % offset from start and end time to analyze, default = 0;


filt = 1;
maxlag = 10; % max lag to search for alignment purposes
% If you want to manually set the lag time, change lagDiff on line 77, else comment out line 77
% set lagDiff to 0 in line 77 if you don't want to align data

data_dir = ['C:\Users\amy.liao\Documents\GSRData\database\072415_ElectrodeVar_GSR_analysis\',currset];



% close all;
for i = 5:6%length(cat)


timestart = timestartall(i)+offset;
timeend = timeendall(i)-offset;

Empatica_mod = data_Empatica.GSR.GSR_data(intersect(find(time_Empatica>timestart),find(time_Empatica<=timeend)));
Sami_mod_raw = data_Sami.physiosignal.gsr.phasic.signal(intersect(find(time_Sami>timestart),find(time_Sami<=timeend)));
timenew = time_Empatica(intersect(find(time_Empatica>timestart),find(time_Empatica<=timeend)));

%% Low-pass filter
if filt
%     [b,a]=butter(5,([0.0159,5]./(32/2)),'bandpass'); % butterworth filter: order = 5, sr = 32 Hz for GSR
    [b,a]=butter(5,([1]./(32/2)),'low'); % butterworth filter: order = 5, sr = 32 Hz for GSR
    
    Sami_mod_filt = filtfilt(b,a,Sami_mod_raw); % filtered GSR data
    % data filtered with butterworth filter and zero-phase filter
    % lowpass filter that preserves timestamps of features
    % cutoff = freq/(samplerate/2)      sr = 32Hz
    clear b a
else
    Sami_mod_filt = Sami_mod_raw;
end
figure(12),plot(Sami_mod_raw,'b'),hold on, plot(Sami_mod_filt,'r')

%% downsample Hermes data
Sami_mod = downsample(Sami_mod_filt, 8); %32 Hz Sami --> 4 Hz

% upsample Empatica data
% Empatica_mod = interp(Empatica_mod,8);
% % timenew2 = min(timenew):(0.25/60/8):max(timenew);
% timenew = 1:length(Empatica_mod);

%% Adjust length
minlen = min(length(Empatica_mod),length(Sami_mod));
Empatica_mod = Empatica_mod(1:minlen);
Sami_mod = Sami_mod(1:minlen)';
timenew = timenew(1:minlen);
% [fig sstruct] = Correlation(Empatica_mod, Sami_mod)


%% Normalize data
Empatica_mod_norm = (Empatica_mod - min(Empatica_mod))/(max(Empatica_mod)-min(Empatica_mod));
Sami_mod_norm = (Sami_mod - min(Sami_mod))/(max(Sami_mod)-min(Sami_mod));
Sami_mod_raw_norm = (Sami_mod_raw - min(Sami_mod_raw))/(max(Sami_mod_raw)-min(Sami_mod_raw));
%% Align data using cross correlation
[crosscorr,lag]= xcorr(Empatica_mod_norm, Sami_mod_norm);
lagzero = find(lag==0);
% [~,I] = max(abs(crosscorr));  % allows any adjustment of lag
maxcorr = max((crosscorr(lagzero-maxlag:lagzero+maxlag)));
I = find(crosscorr==maxcorr); % limits lag adjustmentto within 10 s

lagDiff = lag(I);
lagDiff =-6; % override auto ladDiff with a preset value
timenew_align = timenew(1:end-abs(lagDiff));

fprintf('%s: lag = %d\n',cat{i},lagDiff);

if lagDiff>= 0
    Empatica_mod_norm_align = Empatica_mod_norm(1+lagDiff:end);
    Sami_mod_norm_align = Sami_mod_norm(1:end-lagDiff);
    
    Empatica_mod_align = Empatica_mod(1+lagDiff:end);
    Sami_mod_align = Sami_mod(1:end-lagDiff);
else
    Empatica_mod_norm_align = Empatica_mod_norm(1:end-abs(lagDiff));
    Sami_mod_norm_align = Sami_mod_norm(1+abs(lagDiff):end);
    
    Empatica_mod_align = Empatica_mod(1:end-abs(lagDiff));
    Sami_mod_align = Sami_mod(1+abs(lagDiff):end);
end


    

%% Calculate correlation
% Pearson's Correlation Coefficient
R = corrcoef(Empatica_mod, Sami_mod);
% fprintf('%s: r = %d\n',cat{i},R(2,1));

R2 = corrcoef(Empatica_mod_norm, Sami_mod_norm);
% fprintf('%s: r_norm = %d\n',cat{i},R2(2,1));

R3 = corrcoef(Empatica_mod_norm_align, Sami_mod_norm_align);
fprintf('%s: r_align = %d\n',cat{i},R3(2,1));

% Plot correlation
%     subplot(4,2,[2,4]),scatter(Empatica_mod_norm_align,Sami_mod_norm_align);
%     line([0,1],[0,1],'Color','r');
%     xlabel('Empatica-aligned');
%     ylabel('Sami-aligned');
%     title(['r^2-aligned = ',num2str(R3(2,1)^2)]);


%% Plot Anslab event detection  ([events_ind, nsf_amp, risetime, half_time]=GSR_analysis(curr_dataset,varname,data_dir,rise1,rise2,filtyes))
GSR_Analysis(Empatica_mod_norm_align, cat{i},data_dir,0.01,0.02,0);
saveas(figure(3),[cat{i},'-Empatica.jpg']);
saveas(figure(5),[cat{i},'-Empatica-stats.jpg']);
GSR_Analysis(Sami_mod_norm_align, cat{i},data_dir,0,0.001,0);
saveas(figure(3),[cat{i},'-Sami.jpg']);
saveas(figure(5),[cat{i},'-Sami-stats.jpg']);


%% Frequency Analysis
% figure(2) 
% clf
% % Plot single-sided amplitude spectrum.
% NFFT = 2^nextpow2(length(Sami_mod_raw_norm)); % Next power of 2 from length of y
% GSR_raw_freq = fft(Sami_mod_raw_norm,NFFT)/length(Sami_mod_raw_norm);
% f = 32/2*linspace(0,1,NFFT/2+1);
% 
% NFFT_filt = 2^nextpow2(length(Sami_mod_norm_align)); % Next power of 2 from length of y
% GSR_filt_freq = fft(Sami_mod_norm_align,NFFT_filt)/length(Sami_mod_norm_align);
% f_filt = 32/2*linspace(0,1,NFFT_filt/2+1);
% 
% NFFT_Emp = 2^nextpow2(length(Empatica_mod_norm_align)); % Next power of 2 from length of y
% GSR_raw_Emp_freq = fft(Empatica_mod_norm_align,NFFT_Emp)/length(Empatica_mod_norm_align);
% f_Emp = 32/2*linspace(0,1,NFFT_Emp/2+1);
% 
% 
% 
% plot(f,2*abs(GSR_raw_freq(1:NFFT/2+1))); hold on;
% plot(f_filt,2*abs(GSR_filt_freq(1:NFFT_filt/2+1))); hold on;
% plot(f_Emp,2*abs(GSR_raw_Emp_freq(1:NFFT_Emp/2+1))); hold on; 
% title('Single-Sided Amplitude Spectrum of y(t)')
% xlabel('Frequency (Hz)')
% ylabel('|Y(f)|')
% 
% 
% legend('Sami-raw','Sami-filtered','Empatica')
% saveas(figure(2),[strrep(cat{i},'_','-'),'- Freq Response']);
% saveas(figure(2),[strrep(cat{i},'_','-'),'- Freq Response.jpg']);


%% Plot Correlation
% Plot
% if mod(i,2)==1
    figure(i+5); 

    subplot(3,2,1),plot(Sami_mod_raw,'b'),hold on, plot(Sami_mod_filt,'r'), hold off;
    title('Sami: filtering');
    legend('Raw','Filtered');
    
    
    subplot(3,2,3), plot(timenew,Empatica_mod_norm, timenew,Sami_mod_norm);
    title([cat{i} ' - normalized']);
    legend('Empatica','Hermes');
    subplot(3,2,5),plot(timenew_align,Empatica_mod_norm_align,timenew_align,Sami_mod_norm_align);
    title([cat{i} ' - normalized+aligned']);
    xlabel('time (min)');

    
    delete(subplot(3,2,[2,4,6]))
    fig = subplot(2,2,[2,4]);
    [fig sstruct,r,SSE,rho] = Correlation_test(fig, Empatica_mod_norm_align, Sami_mod_norm_align);
    title([cat{i} ' - correlation']);
    hold on;
% else
%     figure(i-1+5); hold on;
% 
%     subplot(4,2,5), plot(timenew,Empatica_mod_norm, timenew,Sami_mod_norm);
%     title([cat{i} ' - normalized']);
%     legend('Empatica','Hermes');
%     subplot(4,2,7),plot(timenew_align,Empatica_mod_norm_align,timenew_align,Sami_mod_norm_align);
%     title([cat{i} ' - normalized+aligned']);
%     xlabel('time (min)');
% 
% 
%     delete(subplot(4,2,[6,8]))
%     fig = subplot(4,2,[6,8]);
%     [fig sstruct,r,SSE,rho] = Correlation_test(fig, Empatica_mod_norm_align, Sami_mod_norm_align);
%     title([cat{i} ' - correlation']);
    
    set(gcf, 'Position', get(0,'Screensize')); 
    saveas(figure(i+5),[strrep(cat{i},'_rest',''),'-corr']);
    saveas(figure(i+5),[strrep(cat{i},'_rest',''),'-corr.jpg']);
% end

hold off;fprintf('%s: rho_align = %d\n',cat{i},rho);
fprintf('\n\n');
end

% save('GSRanalysis');