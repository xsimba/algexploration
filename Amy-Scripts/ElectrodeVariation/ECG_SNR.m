%% Need to import timestart_unix and timeend_unix: vector array of start and end times for each dataset in alphabetic order
% Multiply output of timetounix converter by 1000 --> should be in 10^12
if timestart_unix(1)<1E10  % if in ms unix time convert to us unix time
    timestart_unix = timestart_unix*1000;
end
if timeend_unix(1)<1E10  % if in ms unix time convert to us unix time
    timeend_unix = timeend_unix*1000;
end


format bank

datdir = 'C:\Users\amy.liao\Documents\GSRData\database\071415_ElectrodeVar_ECG\';
savefolder = [date,'-AnalysisPlots\'];
mkdir(datdir,savefolder);
cd ([datdir, savefolder]);
list = who('data*');
offset = 45;
snrwindow = 8;
c=1;
filename = 'CI-SNR_avg+std';
filename2 = 'CI-SNR_avg+std_2';

e = 1;
while exist([datdir, savefolder,filename,'.xslx'])
    e=e+1;
    filename = [filename,'(',num2str(e),')'];
    filename2 = [filename2,'(',num2str(e),')'];
end
CI_snr_summary = {date,'','','','','';'offset',offset,'','','','';'snrwindow',snrwindow,'','','','';'','','CI-avg','CI-std','SNR-avg','SNR-std'};
CI_snr_summary2 = {date,'','','',;'offset',offset,'','';'snrwindow',snrwindow,'','';'','','CI:avg(std)','SNR:avg(std)'};



% Choose 1 plotting format type
% subplotrow = 3; subplotcol = 3; subplotind = [1 2 3 ; 4 5 6 ]; subplotindsnr = [7 8 9 ];  plotset = 'ECG-CI-SNR';
% subplotrow = 4; subplotcol = 3; subplotind = [1 2 3 7 8 9; 4 5 6 10 11 12]; plotset = 'ECG-CI'; clear subplotindsnr % plots ECG and CI
subplotrow = 6; subplotcol = 3; subplotind = [1 2 3 10 11 12; 4 5 6 13 14 15]; subplotindsnr = [7 8 9 16 17 18]; plotset = 'ECG-CI-SNR';% plots ECG, CI, and SNR



for i = 1:length(list)
    name = strsplit(list{i},'_');
    fprintf(list{i});

    % format data_structure
    eval(['temp_data = ',list{i},'.physiosignal.ecg.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.unixTimeStamps;']); 
%         eval(['temp_time = ',list{i},'.physiosignal.ecg.timestamps;']); 
    
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_all.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_all.time_unix_all = temp_time;']);
    
    % ECG signal cropped to time in timestart and timeend
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.ECG_time_seconds = (ECGdata.' name{2},'.',name{3},'.ECG_time_unix - ECGdata.' name{2},'.',name{3},'.ECG_time_unix(1))/1000;']);

    % beats
    eval(['temp_data = ',list{i},'.physiosignal.ecg.beats.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.beats.unixTimeStamps;']); 

    eval(['ECGdata.' name{2},'.',name{3},'.beats_all.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.beats_all.time_unix_all = temp_time;']);
    
    
    if ECGdata.(name{2}).(name{3}).ECG_time_seconds(end)<180
        timeend_unix(i) = ECGdata.(name{2}).(name{3}).ECG_time_unix(end);
    end
    eval(['ECGdata.' name{2},'.',name{3},'.beats_signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.beats_time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.beats_time_seconds = (ECGdata.' name{2},'.',name{3},'.beats_time_unix - ECGdata.' name{2},'.',name{3},'.ECG_time_unix(1))/1000;']);
    
    
    % CI
    eval(['temp_data = ',list{i},'.physiosignal.ecg.confidenceraw.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.confidenceraw.unixTimeStamps;']); 
    
    eval(['ECGdata.' name{2},'.',name{3},'.CI_all.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.CI_all.time_unix_all = temp_time;']);
    
    eval(['ECGdata.' name{2},'.',name{3},'.CI_signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.CI_time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.CI_time_seconds = (ECGdata.' name{2},'.',name{3},'.CI_time_unix - ECGdata.' name{2},'.',name{3},'.ECG_time_unix(1))/1000;']);        

    % Calculate CI stats
    eval(['ECGdata.' name{2},'.',name{3},'.CI_average = nanmean(temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')+offset*1000),find(temp_time<=timeend_unix(',num2str(i),')-offset*1000))));']);
    eval(['fprintf(''%s - %s: CI_average = %0.2f\n'',name{2},name{3},ECGdata.',name{2},'.',name{3},'.CI_average);']);
        
    eval(['ECGdata.' name{2},'.',name{3},'.CI_std = nanstd(temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')+offset*1000),find(temp_time<=timeend_unix(',num2str(i),')-offset*1000))));']);
    eval(['fprintf(''%s - %s: CI_std = %0.2f\n'',name{2},name{3},ECGdata.',name{2},'.',name{3},'.CI_std);']);
    
    
    SNRsignal = ECGdata.(name{2}).(name{3}).ECG_signal(128*offset:min(23040-128*offset,end));
    SNRtimestamps = ECGdata.(name{2}).(name{3}).ECG_time_seconds(128*offset:min(23040-128*offset,end));
    tempSNRbeats = ECGdata.(name{2}).(name{3}).beats_time_unix;
    SNRbeats = tempSNRbeats(intersect(find(tempSNRbeats>=timestart_unix(i)+offset*1000),find(tempSNRbeats<=timeend_unix(i)-offset*1000)));
    SNRbeats = (SNRbeats-ECGdata.(name{2}).(name{3}).ECG_time_unix(1))/1000;
        

    [SNR,t_snr] = SNR_checker_v2(SNRsignal,SNRtimestamps,SNRbeats,'ecg',snrwindow);
   
    ECGdata.(name{2}).(name{3}).snr_signal = SNR;
    ECGdata.(name{2}).(name{3}).snr_time_seconds = t_snr;

    ECGdata.(name{2}).(name{3}).snr_average = nanmean(SNR);
    ECGdata.(name{2}).(name{3}).snr_std = nanstd(SNR);
    

    fprintf('%s - %s: snr_avg = %0.2f\n',name{2},name{3},ECGdata.(name{2}).(name{3}).snr_average);
    fprintf('%s - %s: snr_std = %0.2f\n\n',name{2},name{3},ECGdata.(name{2}).(name{3}).snr_std);
    
%     xlswrite(filename,{name{2},name{3},ECGdata.(name{2}).(name{3}).snr_average});
%     xlswrite(filename,{name{2},name{3},ECGdata.(name{2}).(name{3}).snr_std});


    CI_snr_summary(end+1,:) ={name{2},name{3},ECGdata.(name{2}).(name{3}).CI_average,ECGdata.(name{2}).(name{3}).CI_std,ECGdata.(name{2}).(name{3}).snr_average,ECGdata.(name{2}).(name{3}).snr_std};
    CI_snr_summary2(end+1,:) ={name{2},name{3},[num2str(round(ECGdata.(name{2}).(name{3}).CI_average,2)),' (',num2str(round(ECGdata.(name{2}).(name{3}).CI_std,2)),')'],[num2str(round(ECGdata.(name{2}).(name{3}).snr_average,2)),' (',num2str(round(ECGdata.(name{2}).(name{3}).snr_std,2)),')']};


    if ~isfield(ECGdata,'stats')|| ~isfield(ECGdata.stats,name{3})
        ECGdata.stats.(name{3}).snr_average(1) = nanmean(SNR);
        ECGdata.stats.(name{3}).snr_std(1) = nanstd(SNR);
        ECGdata.stats.(name{3}).CI_average(1) = ECGdata.(name{2}).(name{3}).CI_average;
        ECGdata.stats.(name{3}).CI_std(1) = ECGdata.(name{2}).(name{3}).CI_std;    
    else
        ECGdata.stats.(name{3}).snr_average(end+1) = nanmean(SNR);
        ECGdata.stats.(name{3}).snr_std(end+1) = nanstd(SNR);
        ECGdata.stats.(name{3}).CI_average(end+1) = ECGdata.(name{2}).(name{3}).CI_average;
        ECGdata.stats.(name{3}).CI_std(end+1) = ECGdata.(name{2}).(name{3}).CI_std;
    end
    
%     % plot data vs SNR plots
%     f= figure;
%     A(1)=subplot(2,1,1);plot(data.timestamps,data.(SigType).signal);hold on;%plot(data.(SigType).beats.signal,data.(SigType).beats.debug.usamp,'r+'); title(['original signal ' SigID]);ylabel('ECG')
%     A(2)=subplot(2,1,2);plot(t_snr,SNR);title('SNR');ylabel('SNR');hline(0.5)
% %         A(3)=subplot(3,1,3);plot(t_snr,mean(STDqrs,2)); title('SD of the avarage beat');ylabel('SD');xlabel('Time (s)')
%         linkaxes(A,'x');
%     title(['SNR = ',num2str(nanmean(SNR))]);
%         saveas(f,[datdir, 'AnalysisPlots\','ECG-snr-',name{2},'-',name{3}]);
%         saveas(f,[datdir, 'AnalysisPlots\','ECG-snr-',name{2},'-',name{3},'.jpg']);   



    % Plot figure
    figure(floor((i-1)/6)+1)
    
    eval(['h(',num2str(i),',1) = subplot(',num2str(subplotrow),',',num2str(subplotcol),',',num2str(subplotind(1,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.ECG_time_seconds, ECGdata.' name{2},'.',name{3},'.ECG_signal);']);
    xlabel('time (sec)');
    ylabel('ECG');
    title({['ECG-raw: ',name{2},' - ',name{3},],['Avg SNR = ',num2str(ECGdata.(name{2}).(name{3}).snr_average)]});
    
    eval(['h(',num2str(i),',2) = subplot(',num2str(subplotrow),',',num2str(subplotcol),',',num2str(subplotind(2,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.CI_time_seconds, ECGdata.' name{2},'.',name{3},'.CI_signal,''.'',''markersize'',20);']);
    eval(['line(xlim,[ECGdata.' name{2},'.',name{3},'.CI_average, ECGdata.' name{2},'.',name{3},'.CI_average],''Color'',''r'');']);
    xlabel('time (sec)');
    ylabel('CI');
    title(['ECG-CI: ',name{2},' - ',name{3}]);

    if exist('subplotindsnr') 
    eval(['h(',num2str(i),',3) = subplot(',num2str(subplotrow),',',num2str(subplotcol),',',num2str(subplotindsnr(1,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.snr_time_seconds, ECGdata.' name{2},'.',name{3},'.snr_signal);']);
    eval(['line(xlim,[ECGdata.' name{2},'.',name{3},'.snr_average, ECGdata.' name{2},'.',name{3},'.snr_average],''Color'',''r'');']);
    xlabel('time (min)');
    ylabel('SNR');
    title(['ECG-SNR: ',name{2},' - ',name{3},': Avg = ',num2str(ECGdata.(name{2}).(name{3}).snr_average)]);    
    end
    
    
    if c<6
        c=c+1;
    else
        c=1;
        set(gcf, 'Position', get(0,'Screensize')); 
        saveas(figure(floor((i-1)/6)+1),[datdir, savefolder,plotset,'-',name{2}]);
        saveas(figure(floor((i-1)/6)+1),[datdir, savefolder,plotset,'-',name{2},'.jpg']);
        linkaxes(h(i-5:i,:),'x'); % links all axes
        
    end
    
    
    eval(['linkaxes(h(',num2str(i),',:), ''x'');']); % links axis for each electrode
    axis([0 180 -inf inf]);

    
end



save([datdir, savefolder,plotset],'ECGdata');
xlswrite([filename,'.xlsx'],CI_snr_summary);
xlswrite([filename2,'.xlsx'],CI_snr_summary2);