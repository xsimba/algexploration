cat = {'Acree_rest', 'Acree_bike', 'SS_rest', 'SS_bike', 'Hudson_rest','Hudson_bike',...
    'UHV_rest','UHV_bike','PtIr_rest','PtIr_bike'};
cat=strrep(cat,'_','-');
timestartall = [7, 10,    14.5, 17.5, 23.5, 26.5, 31,    33.5, 40, 43];  
timeendall   = [10,11.75, 17.5, 19.5, 26.5, 28,   33.75, 34.75, 43, 45]; %modified
% timeendall   = [10, 12, 17.5, 19.5, 26.5, 28.5, 34, 36, 43, 45]; % based on recorded timestamps

data_dir = ['C:\Users\amy.liao\Documents\GSRData\database\072415_ElectrodeVar_GSR_analysis\',currset];

offset = 0.5;
filt = 1;

close all;
for i = 1:10


timestart = timestartall(i)+offset;
timeend = timeendall(i)-offset;

Empatica_mod = data_Empatica.GSR.GSR_data(intersect(find(time_Empatica>timestart),find(time_Empatica<=timeend)));
Sami_mod = data_Sami.physiosignal.gsr.phasic.signal(intersect(find(time_Sami>timestart),find(time_Sami<=timeend)));
timenew = time_Empatica(intersect(find(time_Empatica>timestart),find(time_Empatica<=timeend)));

%% Low-pass filter
Sami_mod_raw = Sami_mod;
if filt
    [b,a]=butter(5,1/(32/2)); % butterworth filter: order = 5, freq = 32 Hz for GSR
    
    Sami_mod = filtfilt(b,a,Sami_mod); % filtered GSR data
    % data filtered with butterworth filter and zero-phase filter
    % lowpass filter that preserves timestamps of features
    % cutoff = freq/(samplerate/2)      sr = 32Hz
    clear b a
else
    Sami_mod = Sami_mod;
end


%% downsample Hermes data
Sami_mod = downsample(Sami_mod, 8); %32 Hz Sami --> 4 Hz

% upsample Empatica data
% Empatica_mod = interp(Empatica_mod,8);
% % timenew2 = min(timenew):(0.25/60/8):max(timenew);
% timenew = 1:length(Empatica_mod);

%% Adjust length
minlen = min(length(Empatica_mod),length(Sami_mod));
Empatica_mod = Empatica_mod(1:minlen);
Sami_mod = Sami_mod(1:minlen)';

% [fig sstruct] = Correlation(Empatica_mod, Sami_mod)


%% Normalize data
Empatica_mod_norm = (Empatica_mod - min(Empatica_mod))/(max(Empatica_mod)-min(Empatica_mod));
Sami_mod_norm = (Sami_mod - min(Sami_mod))/(max(Sami_mod)-min(Sami_mod));
Sami_mod_raw_norm = (Sami_mod_raw - min(Sami_mod_raw))/(max(Sami_mod_raw)-min(Sami_mod_raw));
%% Align data using cross correlation
[crosscorr,lag]= xcorr(Empatica_mod_norm, Sami_mod_norm);
lagzero = find(lag==0);
% [~,I] = max(abs(crosscorr));  % allows any adjustment of lag
maxcorr = max((crosscorr(lagzero-10:lagzero+10)));
I = find(crosscorr==maxcorr); % limits lag adjustmentto within 10 s

lagDiff = lag(I);
lagDiff = 6;
timenew_align = timenew(1:end-abs(lagDiff));

fprintf('%s: lag = %d\n',cat{i},lagDiff);

if lagDiff>= 0
    Empatica_mod_norm_align = Empatica_mod_norm(1+lagDiff:end);
    Sami_mod_norm_align = Sami_mod_norm(1:end-lagDiff);
    
    Empatica_mod_align = Empatica_mod(1+lagDiff:end);
    Sami_mod_align = Sami_mod(1:end-lagDiff);
else
    Empatica_mod_norm_align = Empatica_mod_norm(1:end-abs(lagDiff));
    Sami_mod_norm_align = Sami_mod_norm(1+abs(lagDiff):end);
    
    Empatica_mod_align = Empatica_mod(1:end-abs(lagDiff));
    Sami_mod_align = Sami_mod(1+abs(lagDiff):end);
end


    

%% Calculate correlation
% Pearson's Correlation Coefficient
R = corrcoef(Empatica_mod, Sami_mod);
% fprintf('%s: r = %d\n',cat{i},R(2,1));

R2 = corrcoef(Empatica_mod_norm, Sami_mod_norm);
% fprintf('%s: r_norm = %d\n',cat{i},R2(2,1));

R3 = corrcoef(Empatica_mod_norm_align, Sami_mod_norm_align);
fprintf('%s: r_align = %d\n',cat{i},R3(2,1));

% Plot correlation
%     subplot(4,2,[2,4]),scatter(Empatica_mod_norm_align,Sami_mod_norm_align);
%     line([0,1],[0,1],'Color','r');
%     xlabel('Empatica-aligned');
%     ylabel('Sami-aligned');
%     title(['r^2-aligned = ',num2str(R3(2,1)^2)]);


%% Plot Anslab event detection  ([events_ind, nsf_amp, risetime, half_time]=GSR_analysis(curr_dataset,varname,data_dir,rise1,rise2,filtyes))
GSR_Analysis(Empatica_mod_norm_align, cat{i},data_dir,0.01,0.02,0);
saveas(figure(3),[cat{i},'-Empatica.jpg']);
saveas(figure(5),[cat{i},'-Empatica-stats.jpg']);
GSR_Analysis(Sami_mod_norm_align, cat{i},data_dir,0.01,0.02,0);
saveas(figure(3),[cat{i},'-Sami.jpg']);
saveas(figure(5),[cat{i},'-Sami-stats.jpg']);


%% Frequency Analysis
figure(2) 
clf
% Plot single-sided amplitude spectrum.
NFFT = 2^nextpow2(length(Sami_mod_raw_norm)); % Next power of 2 from length of y
GSR_raw_freq = fft(Sami_mod_raw_norm,NFFT)/length(Sami_mod_raw_norm);
f = 32/2*linspace(0,1,NFFT/2+1);

NFFT_filt = 2^nextpow2(length(Sami_mod_norm_align)); % Next power of 2 from length of y
GSR_filt_freq = fft(Sami_mod_norm_align,NFFT_filt)/length(Sami_mod_norm_align);
f_filt = 32/2*linspace(0,1,NFFT_filt/2+1);

NFFT_Emp = 2^nextpow2(length(Empatica_mod_norm_align)); % Next power of 2 from length of y
GSR_raw_Emp_freq = fft(Empatica_mod_norm_align,NFFT_Emp)/length(Empatica_mod_norm_align);
f_Emp = 32/2*linspace(0,1,NFFT_Emp/2+1);



plot(f,2*abs(GSR_raw_freq(1:NFFT/2+1))); hold on;
plot(f_filt,2*abs(GSR_filt_freq(1:NFFT_filt/2+1))); hold on;
plot(f_Emp,2*abs(GSR_raw_Emp_freq(1:NFFT_Emp/2+1))); hold on; 
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')


legend('Sami-raw','Sami-filtered','Empatica')
saveas(figure(2),[strrep(cat{i},'_','-'),'- Freq Response']);
saveas(figure(2),[strrep(cat{i},'_','-'),'- Freq Response.jpg']);


%% Plot Correlation
% Plot
if mod(i,2)==1
    figure(i+5); 

    subplot(4,2,1), plot(timenew,Empatica_mod_norm, timenew,Sami_mod_norm);
    title([cat{i} ' - normalized']);
    legend('Empatica','Hermes');
    subplot(4,2,3),plot(timenew_align,Empatica_mod_norm_align,timenew_align,Sami_mod_norm_align);
    title([cat{i} ' - normalized+aligned']);
    xlabel('time (min)');

    
    delete(subplot(4,2,[2,4]))
    fig = subplot(4,2,[2,4]);
    [fig sstruct,r,SSE,rho] = Correlation_test(fig, Empatica_mod_norm_align, Sami_mod_norm_align);
    title([cat{i} ' - correlation']);
    hold on;
else
    figure(i-1+5); hold on;

    subplot(4,2,5), plot(timenew,Empatica_mod_norm, timenew,Sami_mod_norm);
    title([cat{i} ' - normalized']);
    legend('Empatica','Hermes');
    subplot(4,2,7),plot(timenew_align,Empatica_mod_norm_align,timenew_align,Sami_mod_norm_align);
    title([cat{i} ' - normalized+aligned']);
    xlabel('time (min)');


    delete(subplot(4,2,[6,8]))
    fig = subplot(4,2,[6,8]);
    [fig sstruct,r,SSE,rho] = Correlation_test(fig, Empatica_mod_norm_align, Sami_mod_norm_align);
    title([cat{i} ' - correlation']);
    
    set(gcf, 'Position', get(0,'Screensize')); 
    saveas(figure(i-1+5),[strrep(cat{i},'_rest',''),'-corr']);
    saveas(figure(i-1+5),[strrep(cat{i},'_rest',''),'-corr.jpg']);
end

hold off;fprintf('%s: rho_align = %d\n',cat{i},rho);
fprintf('\n\n');
end

save('GSRanalysis');