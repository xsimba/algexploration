cd 'C:\Users\amy.liao\Documents\GSRData\database';
clearvars -except data*
clear data_Sami*;
clear data_Empatica*;

% close all;
% data_Empatica = import_Empatica_folder; % generate Empatica data
% structure

currset = '080315Arvind'; % set name
eval(['data_Sami = data_',currset,'_Hermes;']); % use if data named in format: data_(currset)_Athena
% eval(['data_Empatica = data_',currset,'_Empatica;']);
% data_Sami = data_Amy0806015_Athena; % data in form of v5 struct, aligned (data = readSamiPortalData(p);)
data_Empatica = data_080315Arvindb_Empatica;


filt = 1;
plotacc = 1; % 1 = plot accelerometer data, 0 = don't plot accelerometer data (from Empatica)
plothr = 0; % 1 = plot hr data, 0 = don't plot hr data (from Sami)
timelag = 0; % default = 0;  % for data alignment, timelag in number of indices (can use electrodecorr_plot to estimate)

%%
data_dir = ['C:\Users\amy.liao\Documents\GSRData\database\072415_ElectrodeVar_GSR_analysis\',currset];
savedir = ['\',date,'-GSR_analysis']; % folder to save plots into
mkdir(data_dir,savedir); warning off
red_dir=[data_dir,savedir]; 
cd(red_dir)  % cd to data_dir




%%
% temp modification for Yelei's data
% data_Empatica_mod.GSR.GSR_time = data_Empatica.GSR.GSR_time(1:6721);
% data_Empatica_mod.GSR.GSR_data = data_Empatica.GSR.GSR_data(241:end-730);

%% Plot Empatica data
% figure(1);
% plot(data_Empatica.GSR.GSR_time,data_Empatica.GSR.GSR_data);
% 
% title('Empatica -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');
% 
% % Recorded tags
% for i = 1:length(data_Empatica.tags.tags_data)
%    line([data_Empatica.tags.tags_data(i),data_Empatica.tags.tags_data(i)], ylim,'Color','r'); 
% end
% 
% % Marked tags on Empatica
% % [tags_orig_data_raw] = import_Empatica_csv([data_Empatica.folderpath '\tags_orig.csv'],1);
% % [tags_orig_data] = (tags_orig_data_raw-data_Empatica.GSR.GSR_time_1)/60;
% % for m = 1:length(tags_orig_data)
% %    line([tags_orig_data(m),tags_orig_data(m)], ylim,'Color','g'); 
% % end

%% Plot Sami data

data_Sami_time_1 = data_Sami.physiosignal.gsr.phasic.unixTimeStamps(1)/1000;
time_minutes = (data_Sami.physiosignal.gsr.phasic.timestamps-data_Sami_time_1)/60;

% figure(2);
% plot(time_minutes,data_Sami.physiosignal.gsr.phasic.signal);
% 
% title('Hermes -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR signal(ADC code)');


%% Aligned data by timestamp

timepoint1 = min(data_Empatica.GSR.GSR_time_1,data_Sami_time_1);
time_Empatica = (data_Empatica.GSR.GSR_time_1-timepoint1)/60+data_Empatica.GSR.GSR_time;
time_Sami = (data_Sami.physiosignal.gsr.phasic.unixTimeStamps/1000-timepoint1)/60;
time_tags = (data_Empatica.GSR.GSR_time_1-timepoint1)/60+data_Empatica.tags.tags_data;
% time_tags_orig = (data_Empatica.GSR.GSR_time_1-timepoint1)/60+tags_orig_data;



%% Plot aligned data
% figure(3); 
% 
% ha(1) = subplot(2,1,1);plot(time_Empatica,data_Empatica.GSR.GSR_data);
% title('Empatica -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');
% 
% ylim;
% for j = 1:length(time_tags)
%    subplot(2,1,1);line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
% end
% % for n = 1:length(time_tags_orig)
% %    subplot(2,1,1),line([time_tags_orig(n),time_tags_orig(n)], ylim,'Color','g'); 
% % end
% 
% ha(2) = subplot(2,1,2);plot(time_Sami,data_Sami.physiosignal.gsr.phasic.signal);
% title('Hermes -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (ADC units)');
% 
% ylim;
% for j = 1:length(time_tags)
%    subplot(2,1,2),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
% end
% % for n = 1:length(time_tags_orig)
% %    subplot(2,1,2),line([time_tags_orig(n),time_tags_orig(n)], ylim,'Color','g'); 
% % end
% 
% axis([0 max(time_Empatica(end),time_Sami(end)) -inf inf]);
% linkaxes(ha, 'x'); 


%% Filtering
GSR_raw = data_Sami.physiosignal.gsr.phasic.signal;
GSR_raw_Emp =data_Empatica.GSR.GSR_data;
if filt
    [b,a]=butter(5,1/(32/2)); % butterworth filter: order = 5, freq = 32Hz for Sami GSR
    GSR_filt = filtfilt(b,a,GSR_raw); % filtered GSR data
    
    [b,a]=butter(5,1/(4/2));
    GSR_Emp_filt = filtfilt(b,a,GSR_raw_Emp);

    % data filtered with butterworth filter and zero-phase filter
    % lowpass filter that preserves timestamps of features
    % cutoff = freq/(samplerate/2)      sr = 32Hz
    clear b a
else
    GSR_filt = GSR_raw;
end

% Normalize datasets
Empatica_norm = (data_Empatica.GSR.GSR_data - min(data_Empatica.GSR.GSR_data))/(max(data_Empatica.GSR.GSR_data)-min(data_Empatica.GSR.GSR_data));
GSR_filt_norm= (GSR_filt - min(GSR_filt))/(max(GSR_filt)-min(GSR_filt));

%% Plotting
% Plot filtered data
figure(4); 
clf;
ha(1) = subplot(plotacc+plothr+3,1,1);plot(time_Empatica,data_Empatica.GSR.GSR_data); hold on
plot(time_Empatica,GSR_Emp_filt,'m','LineWidth',1.5);
title('Empatica -- GSR');
% xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');
legend('Raw','Smoothed');

ha(2) = subplot(plotacc+plothr+3,1,2);plot(time_Sami+timelag,data_Sami.physiosignal.gsr.phasic.signal,'.'); hold on
% plot(time_Sami+timelag,GSR_filt,'m','LineWidth',1.5);
line(xlim,[0,0]);
line(xlim, [4096,4096]);
title('Simband -- GSR');
% xlabel('Time (minutes)');
ylabel('GSR (ADC units)');
legend('Raw','Smoothed');

ha(3) = subplot(plotacc+plothr+3,1,3);plot(time_Empatica,Empatica_norm, time_Sami+timelag,GSR_filt_norm);
title('Normalized Hermes + Empatica-- GSR');
% xlabel('Time (minutes)');
ylabel('GSR (normalized)');
legend('Empatica','Hermes');

ylim;
for j = 1:length(time_tags)
   subplot(plotacc+plothr+3,1,1);line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
   subplot(plotacc+plothr+3,1,2),line([time_tags(j),time_tags(j)], ylim,'Color','r');  
   subplot(plotacc+plothr+3,1,3),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
end

% Accelerometer
if plotacc == 1
    time_Empatica_acc = (data_Empatica.accel.accel_time_1-timepoint1)/60+data_Empatica.accel.magaccel_time;

    ha(4) = subplot(plotacc+plothr+3,1,4);plot(time_Empatica_acc,data_Empatica.accel.magaccel_data);
    ylim;
    for j = 1:length(time_tags)
       subplot(plotacc+plothr+3,1,4),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
    end
    
    title('Acceleration - Empatica');
%     xlabel('Time (minutes)');
    ylabel('Acceleration (g)');
end

% HR
if plothr == 1
    time_Sami_hr = (data_Sami.heartRate.monitoring_hr.timestamps-timepoint1)/60;

    ha(5) = subplot(plotacc+plothr+3,1,4+plotacc);plot(time_Sami_hr+timelag,data_Sami.heartRate.monitoring_hr.signal);
    ylim;
    for j = 1:length(time_tags)
       subplot(plotacc+plothr+3,1,4+plotacc),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
    end
    
    title('Heart Rate - Sami');
%     xlabel('Time (minutes)');
    ylabel('Heart Rate (bpm)');
end

xlabel('Time (minutes)');
axis([0 max(time_Empatica(end),time_Sami(end)) -inf inf]);
linkaxes(ha, 'x'); 

saveas(figure(4),[currset,'- Hermes vs Empatica']);
saveas(figure(4),[currset,'- Hermes vs Empatica.jpg']);
%% Frequency response
NFFT = 2^nextpow2(length(GSR_raw)); % Next power of 2 from length of y
GSR_raw_freq = fft(GSR_raw,NFFT)/length(GSR_raw);
f = 32/2*linspace(0,1,NFFT/2+1);

NFFT_filt = 2^nextpow2(length(GSR_filt)); % Next power of 2 from length of y
GSR_filt_freq = fft(GSR_filt,NFFT_filt)/length(GSR_filt);
f_filt = 32/2*linspace(0,1,NFFT_filt/2+1);

NFFT_Emp = 2^nextpow2(length(GSR_raw_Emp)); % Next power of 2 from length of y
GSR_raw_Emp_freq = fft(GSR_raw_Emp,NFFT_Emp)/length(GSR_raw_Emp);
f_Emp = 32/2*linspace(0,1,NFFT_Emp/2+1);

% figure(5) 
% clf
% % Plot single-sided amplitude spectrum.
% plot(f,2*abs(GSR_raw_freq(1:NFFT/2+1))) 
% title('Single-Sided Amplitude Spectrum of y(t)')
% xlabel('Frequency (Hz)')
% ylabel('|Y(f)|')
% hold on;
% 
% plot(f_filt,2*abs(GSR_filt_freq(1:NFFT_filt/2+1))) 
% plot(f_Emp,2*abs(GSR_raw_Emp_freq(1:NFFT_Emp/2+1))) 
% 
% legend('Sami-raw','Sami-filtered','Empatica')
% saveas(figure(5),[currset,'- Freq Response']);
% saveas(figure(5),[currset,'- Freq Response.jpg']);