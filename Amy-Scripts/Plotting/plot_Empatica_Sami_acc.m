cd 'C:\Users\amy.liao\Documents\GSRData\database';

data_Empatica = import_Empatica_folder;

% temp modification for Yelei's data
data_Empatica_mod = data_Empatica;
% data_Empatica_mod.GSR.GSR_time = data_Empatica.GSR.GSR_time(1:6721);
% data_Empatica_mod.GSR.GSR_data = data_Empatica.GSR.GSR_data(241:end-730);
%
%%
figure(1)
plot(data_Empatica.GSR.GSR_time,data_Empatica.GSR.GSR_data);

title('Empatica -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');

% Recorded tags
for i = 1:length(data_Empatica.tags.tags_data)
   line([data_Empatica.tags.tags_data(i),data_Empatica.tags.tags_data(i)], ylim,'Color','r'); 
end

% Marked tags on Empatica
% [tags_orig_data_raw] = import_Empatica_csv([data_Empatica.folderpath '\tags_orig.csv'],1);
% [tags_orig_data] = (tags_orig_data_raw-data_Empatica.GSR.GSR_time_1)/60;
% for m = 1:length(tags_orig_data)
%    line([tags_orig_data(m),tags_orig_data(m)], ylim,'Color','g'); 
% end

%%

% data_Sami = data_Arvind;
data_Sami = data_Amy_music;

data_Sami_time_1 = data_Sami.physiosignal.gsr.phasic.timestamps(1);
time_minutes = (data_Sami.physiosignal.gsr.phasic.timestamps-data_Sami_time_1)/60;

figure(2)
plot(time_minutes,data_Sami.physiosignal.gsr.phasic.signal);

title('Hermes -- GSR');
xlabel('Time (minutes)');
ylabel('GSR signal(ADC code)');


%% Aligned data

timepoint1 = min(data_Empatica.GSR.GSR_time_1,data_Sami_time_1);
time_Empatica = (data_Empatica.GSR.GSR_time_1-timepoint1)/60+data_Empatica.GSR.GSR_time;
time_Sami = (data_Sami.physiosignal.gsr.phasic.timestamps-timepoint1)/60;
time_tags = (data_Empatica.GSR.GSR_time_1-timepoint1)/60+data_Empatica.tags.tags_data;
% time_tags_orig = (data_Empatica.GSR.GSR_time_1-timepoint1)/60+tags_orig_data;

figure(3) 
% subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6961),data_Empatica.GSR.GSR_data(241:end-490));
% subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6721),data_Empatica.GSR.GSR_data(241:end-730));
ha(1) = subplot(3,1,1);plot(time_Empatica,data_Empatica.GSR.GSR_data);
title('Empatica -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');

ylim
for j = 1:length(time_tags)
   subplot(3,1,1),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
end
% for n = 1:length(time_tags)
%    subplot(2,1,1),line([time_tags_orig(n),time_tags_orig(n)], ylim,'Color','g'); 
% end

ha(2) = subplot(3,1,2);plot(time_Sami,data_Sami.physiosignal.gsr.phasic.signal);
title('Hermes -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (ADC units)');

ylim
for j = 1:length(time_tags)
   subplot(3,1,2);line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
end
% for n = 1:length(time_tags)
%    subplot(3,1,2),line([time_tags_orig(n),time_tags_orig(n)], ylim,'Color','g'); 
% end
% data.GSR.GSR_time = 0:1/data.GSR.GSR_samplerate/60:(length(data.GSR.GSR_data)-1)/data.GSR.GSR_samplerate/60;
% data_Empatica.accel.accel_time = 
time_Empatica_acc = (data_Empatica.accel.accel_time_1-timepoint1)/60+data_Empatica.accel.magaccel_time;

ha(3) = subplot(3,1,3);plot(time_Empatica_acc,data_Empatica.accel.magaccel_data);
ylim
for j = 1:length(time_tags)
   subplot(3,1,3),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
end

axis([0 max(time_Empatica(end),time_Sami(end)) -inf inf]);
linkaxes(ha, 'x'); 