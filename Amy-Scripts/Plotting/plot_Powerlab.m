
data_Powerlab = import_Powerlabs_mat;

%% Temp code to append 2 datasets together
% data_Powerlab2 = import_Powerlabs_csv;

% GSR_time1 = data_Powerlab.GSR.GSR_time;
% GSR_time2_temp = data_Powerlab2.GSR.GSR_time;
% GSR_time2 = data_Powerlab2.GSR.GSR_time+GSR_time1(end);
% GSR_time_Powerlab = [GSR_time1;GSR_time2(2:end)];
% 
% GSR_data1 = data_Powerlab.GSR.GSR_data;
% GSR_data2 = data_Powerlab2.GSR.GSR_data;
% GSR_data_Powerlab = [GSR_data1;GSR_data2(2:end)];

figure(1)
% subplot(3,1,1),plot(GSR_time1/60,GSR_data1);
% subplot(3,1,2),plot(GSR_time2_temp/60,GSR_data2);
% subplot(3,1,3),plot(GSR_time_Powerlab/60,GSR_data_Powerlab);
%%
% plot(GSR_time/60,data.GSR.GSR_data);
plot(data_Powerlab.GSR.GSR_time,data_Powerlab.GSR.GSR_data);
title('Powerlab -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');