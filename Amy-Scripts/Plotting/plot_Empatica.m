cd 'C:\Users\amy.liao\Documents\GSRData\database';

data_Empatica = import_Empatica_folder;

% temp modification for Yelei's data
data_Empatica_mod = data_Empatica;
% data_Empatica_mod.GSR.GSR_time = data_Empatica.GSR.GSR_time(1:6721);
% data_Empatica_mod.GSR.GSR_data = data_Empatica.GSR.GSR_data(241:end-730);
%

figure(1)
plot(data_Empatica.GSR.GSR_time,data_Empatica.GSR.GSR_data);

title('Empatica -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');


for i = 1:length(data_Empatica.tags.tags_data)
   line([data_Empatica.tags.tags_data(i),data_Empatica.tags.tags_data(i)], ylim,'Color','r'); 
end


% figure(3) 
% % subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6961),data_Empatica.GSR.GSR_data(241:end-490));
% % subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6721),data_Empatica.GSR.GSR_data(241:end-730));
% subplot(2,1,1),plot(data_Empatica_mod.GSR.GSR_time,data_Empatica_mod.GSR.GSR_data);
% title('Empatica -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');
% subplot(2,1,2),plot(data_Powerlab.GSR.GSR_time,data_Powerlab.GSR.GSR_data);
% title('Powerlab -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');