% cd 'C:\Users\amy.liao\Documents\GSRData\database';

data_Simband = data_GSR_wrist;


% % temp modification for Yelei's data
% data_Empatica_mod = data_Empatica;
% data_Empatica_mod.GSR.GSR_time = data_Empatica.GSR.GSR_time(1:6721);
% data_Empatica_mod.GSR.GSR_data = data_Empatica.GSR.GSR_data(241:end-730);
%

time = (data_GSR_wrist.physiosignal.gsr.tonic.timestamps-data_GSR_wrist.physiosignal.gsr.tonic.timestamps(1))/60;
DAC1 = 56368;
DAC2 = 52973;
code = data_GSR_wrist.physiosignal.gsr.tonic.signal;

VDAC1 = floor(DAC1/16)/4096*5;
VDAC2 = floor(DAC2/16)/4096*5;

Vout2 = code/4096*1.8;
Vout1 = VDAC2*0.14784 - (Vout2-VDAC2*0.14784)/100;

Res = 100000 * (VDAC1-0.6)./(0.6-Vout1)-430000;
Cond = 1./Res/10^-6;

figure(1)
subplot(3,1,1);plot(time,Cond);
title('Simband -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');

subplot(3,1,2);plot(time(1:1920),Cond(1:1920));
title('Simband -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');

subplot(3,1,3);plot(time(1920:end),Cond(1920:end));
title('Simband -- GSR');
xlabel('Time (minutes)');
ylabel('GSR (microSiemens)');

% figure(3) 
% % subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6961),data_Empatica.GSR.GSR_data(241:end-490));
% % subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6721),data_Empatica.GSR.GSR_data(241:end-730));
% subplot(2,1,1),plot(data_Empatica_mod.GSR.GSR_time,data_Empatica_mod.GSR.GSR_data);
% title('Empatica -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');
% subplot(2,1,2),plot(data_Powerlab.GSR.GSR_time,data_Powerlab.GSR.GSR_data);
% title('Powerlab -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');