function [time_min, GSR] = plot_Sami(data_Sami)

% cd 'C:\Users\amy.liao\Documents\GSRData\database';
% RC setup
% data_Sami = getPortalSnapshot;
% data_Sami = data_Amy_exercise2;


% temp modification for Yelei's data
% data_Empatica_mod = data_Empatica;
% data_Empatica_mod.GSR.GSR_time = data_Empatica.GSR.GSR_time(1:6721);
% data_Empatica_mod.GSR.GSR_data = data_Empatica.GSR.GSR_data(241:end-730);
%

data_Sami_time_1 = data_Sami.physiosignal.gsr.phasic.timestamps(1);
time_min = (data_Sami.physiosignal.gsr.phasic.timestamps-data_Sami_time_1)/60;

figure(1)
plot(time_min,data_Sami.physiosignal.gsr.phasic.signal);

title('Sami -- GSR');
xlabel('Time (minutes)');
ylabel('GSR signal(ADC code)');

hold on
line([,], ylim,'Color','r'); 

% for i = 1:length(data_Empatica.tags.tags_data)
%    line([data_Empatica.tags.tags_data(i),data_Empatica.tags.tags_data(i)], ylim,'Color','r'); 
% end


% figure(3) 
% % subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6961),data_Empatica.GSR.GSR_data(241:end-490));
% % subplot(2,1,1),plot(data_Empatica.GSR.GSR_time(1:6721),data_Empatica.GSR.GSR_data(241:end-730));
% subplot(2,1,1),plot(data_Empatica_mod.GSR.GSR_time,data_Empatica_mod.GSR.GSR_data);
% title('Empatica -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');
% subplot(2,1,2),plot(data_Powerlab.GSR.GSR_time,data_Powerlab.GSR.GSR_data);
% title('Powerlab -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');