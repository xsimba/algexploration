clear all
close all
clc
% Adds Matlab code to path
addpath(genpath('C:\Users\amy.liao\Documents\GSRData\scripts'));

% fpathfolder = 'C:\Users\amy.liao\Documents\GSRData\database\061915_Electrode Variation';
% fpathfolder = 'C:\Users\amy.liao\Documents\GSRData\database\062515_Electrode Variation_Pressure Controlled';
fpathfolder = 'C:\Users\amy.liao\Documents\GSRData\database\062615_Electrode Variation_all data';



% Import all data in folder
data = import_Keithley_folder(fpathfolder);

electrodecat = {'Ag_Hudson';'SS_Ag_Acree';'SS_Ag_UHV';'SS_PtIr';'SS'};
electrodecat2 = strrep(electrodecat,'_','-');
data.electrodes.Ag_Hudson.normalized_steel = [];
data.electrodes.SS_Ag_Acree.normalized_steel = [];
data.electrodes.SS_Ag_UHV.normalized_steel =[];
data.electrodes.SS_PtIr.normalized_steel = [];
data.electrodes.SS.normalized_steel = [];
data.electrodes.Ag_Hudson.normalized_voltage = [];
data.electrodes.SS_Ag_Acree.normalized_voltage = [];
data.electrodes.SS_Ag_UHV.normalized_voltage =[];
data.electrodes.SS_PtIr.normalized_voltage = [];
data.electrodes.SS.normalized_voltage = [];

subjects = fieldnames(data);
% Hudson,Acree,UHV,PtIr,SS
plotcolor = {'[0 0.5 0]','''b''','[0.75 0 0.75]','[0.93 0.69 0.13]','''r''','''g''','''c''','''m''','''y'''}; % dark green,blue,purple,orange,red
plotmarker = ['d','^','v','s','o'];

runres = zeros(16,1);


mkdir(fpathfolder,[date, '_AnalysisPlots']);
cd([fpathfolder, '\', date, '_AnalysisPlots']);

for i = 1:length(subjects)-1
    eval(['electrodes = fieldnames(data.',subjects{i},');']);
    electrodes2 = strrep(electrodes,'_','-');
    for j = 1:length(electrodes)
      
        eval(['runs = fieldnames(data.',subjects{i},'.',electrodes{j},');']);
        
        for k = 1:length(runs)
            fprintf(['data.',subjects{i},'.',electrodes{j},'.',runs{k},'\n']);
            
            eval(['runres(1:end,',num2str(k),') = data.',subjects{i},'.',electrodes{j},'.',runs{k},'.resistance(2:end);']);
                
            
            [temp,electrodeind,temp2] = intersect(electrodecat,electrodes{j});
            
            figure(i)
            eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
                ',data.',subjects{i},'.',electrodes{j},'.',runs{k},'.resistance(2:end)'...
                ',''Color'',',plotcolor{electrodeind},...
                ',''LineStyle'',''-.'''...
                ',''LineWidth'',0.5'...
                ',''Marker'',''','none','''',...%plotmarker(electrodeind),...
                ',''DisplayName'',''',subjects{i},'.',electrodes2{j},'.',runs{k},''');']); 

            hold on;
            figure(length(subjects)+1)
            eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
                ',data.',subjects{i},'.',electrodes{j},'.',runs{k},'.resistance(2:end)'...
                ',''Color'',',plotcolor{i},...
                ',''LineStyle'','':'''...
                ',''Marker'',''',plotmarker(electrodeind),'''',...
                ',''DisplayName'',''',subjects{i},'.',electrodes2{j},'.',runs{k},''');']); 
            hold on;
        end
        eval(['data.',subjects{i},'.',electrodes{j},'.average = nanmean(runres,2);']);
        eval(['data.',subjects{i},'.',electrodes{j},'.std = std(runres,0,2);']);
        
        
        figure(i)
        eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
            ',data.',subjects{i},'.',electrodes{j},'.average'...
            ',''Color'',',plotcolor{electrodeind},...
            ',''LineStyle'',''-'''...
            ',''LineWidth'',1.0'...
            ',''Marker'',''',plotmarker(electrodeind),'''',...
            ',''DisplayName'',''',subjects{i},'.',electrodes2{j},'.average'');']); 
        
        
        hold on;
        
        runres = zeros(16,1);
               
    end
    figure(i)
    xlabel('Voltage (V)');
    ylabel('Resistance (ohms)');
    legend('toggle')
    eval(['saveas(figure(i),''' ,subjects{i},'.',electrodes{j},'.fig'');']);
    eval(['saveas(figure(i),''' ,subjects{i},'.',electrodes{j},'.jpg'');']);
    
    for m = 1:length(electrodes)
        eval(['data.',subjects{i},'.',electrodes{m},'.normalized_steel = ','data.',subjects{i},'.',electrodes{m},'.average./','data.',subjects{i},'.SS.average;']);
        eval(['data.',subjects{i},'.',electrodes{m},'.normalized_voltage = ','data.',subjects{i},'.',electrodes{m},'.average./','data.',subjects{i},'.',electrodes{m},'.average(end);']);
        eval(['data.electrodes.',electrodes{m},'.normalized_steel(:,end+1) = ','data.',subjects{i},'.',electrodes{m},'.normalized_steel(:,:);']); 
        eval(['data.electrodes.',electrodes{m},'.normalized_voltage(:,end+1) = ','data.',subjects{i},'.',electrodes{m},'.normalized_voltage(:,:);']); 
    
    
    figure(length(subjects)+2)
    eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
                ',data.',subjects{i},'.',electrodes{m},'.normalized_voltage'...
                ',''Color'',',plotcolor{m},...
                ',''LineStyle'','':'''...
                ',''LineWidth'',0.5'...
                ',''Marker'',''','none','''',...%plotmarker(electrodeind),...
                ',''DisplayName'',''',subjects{i},'.',electrodecat2{m},''');']); 
    hold on;
    
    
    figure(length(subjects)+3)
    eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
                ',data.',subjects{i},'.',electrodes{m},'.normalized_steel'...
                ',''Color'',',plotcolor{m},...
                ',''LineStyle'','':'''...
                ',''LineWidth'',0.5'...
                ',''Marker'',''','none','''',...%plotmarker(electrodeind),...
                ',''DisplayName'',''',subjects{i},'.',electrodecat2{m},''');']); 
    hold on;    
    
    
    
    end 
end

for n = 1:length(electrodecat)
    eval(['data.electrodes.',electrodecat{n},'.normalized_steel_average = ','nanmean(data.electrodes.',electrodecat{n},'.normalized_steel(:,:),2);']);
    eval(['data.electrodes.',electrodecat{n},'.normalized_steel_std = ','std(data.electrodes.',electrodecat{n},'.normalized_steel(:,:),0,2);']);

    eval(['data.electrodes.',electrodecat{n},'.normalized_voltage_average = ','nanmean(data.electrodes.',electrodecat{n},'.normalized_voltage(:,:),2);']);
    eval(['data.electrodes.',electrodecat{n},'.normalized_voltage_std = ','std(data.electrodes.',electrodecat{n},'.normalized_voltage(:,:),0,2);']);
    
    
    eval(['fprintf(''',electrodecat{n},'_mean = %d\n'',data.electrodes.',electrodecat{n},'.normalized_steel_average(end))']);
    eval(['fprintf(''',electrodecat{n},'_std = %d\n'',data.electrodes.',electrodecat{n},'.normalized_steel_std(end))']);
    
    figure(length(subjects)+2)
    eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
                ',data.electrodes.',electrodecat{n},'.normalized_voltage_average'...
                ',''Color'',',plotcolor{n},...
                ',''LineStyle'',''-'''...
            ',''LineWidth'',1.0'...
                ',''Marker'',''',plotmarker(n),'''',...
                ',''DisplayName'',''',electrodecat2{n},''');']); 
    hold on;
    
    
    figure(length(subjects)+3)
    eval(['plot(data.',subjects{i},'.',electrodes{j},'.',runs{k},'.level(2:end)'...
                ',data.electrodes.',electrodecat{n},'.normalized_steel_average'...
                ',''Color'',',plotcolor{n},...
                ',''LineStyle'',''-'''...
            ',''LineWidth'',1.0'...
                ',''Marker'',''',plotmarker(n),'''',...
                ',''DisplayName'',''',electrodecat2{n},''');']); 
    hold on;
end

figure(length(subjects)+1)
legend('toggle')
xlabel('Voltage (V)');
ylabel('Resistance (ohms)');
axis([0,4.5,0,2E8]);
eval(['saveas(figure(length(subjects)+1),''All Data.fig'');']);
eval(['saveas(figure(length(subjects)+1),''All Data.jpg'');']);

figure(length(subjects)+2)
legend('toggle')
xlabel('Voltage (V)');
ylabel('Normalized resistance (normalized to 4.3V)');
% axis([0,4.5,0,2E8]);
eval(['saveas(figure(length(subjects)+1),''Normalized_voltage.fig'');']);
eval(['saveas(figure(length(subjects)+1),''Normalized_voltage.jpg'');']);

figure(length(subjects)+3)
legend('toggle')
xlabel('Voltage (V)');
ylabel('Normalized resistance (normalized to steel)');
% axis([0,4.5,0,2E8]);
eval(['saveas(figure(length(subjects)+1),''Normalized_steel.fig'');']);
eval(['saveas(figure(length(subjects)+1),''Normalized_steel.jpg'');']);



save('ElectrodeVariation')