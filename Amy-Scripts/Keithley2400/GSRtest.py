## Code to test electrode-skin impedance using different voltage settings

##Import
from Tkinter import Tk, VERTICAL, ALL, TOP, LEFT, BOTH, RAISED, Canvas, StringVar, IntVar, Text, DISABLED
from ttk import Frame, Button, Style, Entry, Label, OptionMenu, Checkbutton, Combobox
from tkFileDialog import askopenfilename, asksaveasfilename

import sys
import tkMessageBox
import datetime

import math
# import visa

import time
import serial
import serial.tools.list_ports as list_ports

import csv

print "\n\n\n"
print "import complete"


## Custom settings -- modify as needed
subjectID = 'Natasha_Ag-Hudson_run3'
# subjectID = 'test'
csvname = subjectID + '.csv'	
ResRange = 1E3  # Starting point for, estimated resistance range
CurrCompl = 1E-6
# V=[4.3, 1.7, 0.7]
V = [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.2, 1.4, 1.6, 1.7, 1.8, 2.0, 2.5, 3.0, 3.5, 4.0, 4.3]


## Set comm port settings to communicate with Keithley 2400
ser = serial.Serial(
	# port='/dev/ttyUSB1',
	port = 6,
	baudrate=9600,
	parity=serial.PARITY_ODD,
	# stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS,
	timeout = 3
)
if not ser.isOpen():
# ser = serial.Serial(7)
	ser.open()
	print "port opened"
print ser.isOpen()






ser.write('*RST\r') #Reset GPIB defaults
ser.write(':TRAC:TST:FORM ABS \r')  # set absolute timestamp
ser.write('SYST:BEEP:STAT 0\r')  # turn beep off

ser.write(':SENS:FUNC "RES"\r')	# measure resistance
# ser.write(':SENS:RES:RANG '+str(ResRange)+'\r')  #set resistance measurement range
ser.write(':SENS:RES:RANG:AUTO ON\r')  #set resistance measurement range
ser.write(':SENS:RES:MODE MAN\r')  # set measurement mode to manual
ser.write(':SENS:RES:OCOM OFF\r')  # set offset compensation state
# ser.write('SENS:VOLT:PROT <n>\r') # set voltage compliance
# ser.write('SENS:VOLT:PROT '+str(CurrCompl)+'\r') #set current compliance 

ser.write(':SOUR:FUNC VOLT\r')  # source voltage

ser.write(':SYST:RSEN OFF\r')  #2 wire sense
ser.write(':FORM:ELEM RES\r')

ser.write(':OUTP ON\r')

## Print header
outfile = open(csvname, 'w')
outfile.write(csvname+'\n\n')
outfile.write("Resistance measurements using the Keithley2400 Source Meter\n")
outfile.write("Subject ID:,"+subjectID+"\n")
outfile.write("Time:,"+time.strftime("%H:%M:%S")+"\n")
# outfile.write("Current Compliance:,"+str(CurrCompl)+"\n")
# outfile.write("Measurement Range:,"+str(ResRange))
outfile.write("\n\n")

outfile.write("\n,Voltage, Meas Range, Resistance \n\n")


## Set measurement range
ser.write(':SOUR:VOLT:LEV:IMM:AMPL 4.3\r')  #set level
ser.write(':INIT\r')
ser.write(':FETC?\r')
test = ser.read(14)
print test
restest=math.ceil(float(test))*10
# restest = 140000000
print "Meas range = " + str(restest)
ser.write(':SENS:RES:RANG '+str(restest)+'\r')  #set resistance measurement range



for x in range(0, len(V)):
	print "V="+str(V[x])
	ser.write(':SOUR:VOLT:LEV:IMM:AMPL ' +str(V[x])+'\r')  #set level
	outfile.write("meas")

	for i in range(0,10):
		outfile.write(",")
		outfile.write(str(V[x]))
		outfile.write(','+str(restest))
		# ser.write(':READ?\r')
		ser.write(':INIT\r')
		ser.write(':FETC?\r')
		res=ser.read(14)
		# res2 = res.strip().split('\r');
		# print "res\n"
		print res
		# print "\nres2\n"
		# print res2
		# res = ser.read(15);
		outfile.write(",")
		outfile.write(str(res))
		# ser.write(':TRAC:CLE\r')
		# ser.flush()
		
		# outfile.write("\n")

ser.write(':OUTP OFF\r')	
outfile.close()	


