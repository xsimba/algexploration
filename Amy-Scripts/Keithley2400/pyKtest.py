# Tested with python 2.7.2+, GTK+3
# Must have python-gi-cairo package installed (Debian)

import sys
import datetime
import math		# necessary for rotation when drawing (need pi)
import re		# for regular expression searching
import random	# for random number generation - only used during debugging
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import GdkPixbuf		# for loading source meter picture
from gi.repository import PangoCairo	# for drawing text on graph
#from gi.repository import Pango	# necessary if I can ever figure out how to use pango attributes
import serial, time
from decimal import *	# so decimals can be represented exactly (not floats)
import ConfigParser		# for reading and writing configuration files
import os	# so we can get home dir with home = os.path.expanduser("~")
#from pprint import pprint	# used for debugging only

# want to see what these libraries give you access to?
#print dir(Gtk.PolicyType)
#print dir(Gdk)	# this one will show the available keysym constants
#print dir(PangoCairo)

#print Gtk.get_major_version()
#print Gtk.get_minor_version()
#print Gtk.get_micro_version()

# Definitions
CATCH_EXCEPTIONS = True
validDataBits = {
	'5': serial.FIVEBITS,
	'6': serial.SIXBITS,
	'7': serial.SEVENBITS,
	'8': serial.EIGHTBITS
}
validParities = {
	'None': serial.PARITY_NONE,
	'Odd': serial.PARITY_ODD,
	'Even': serial.PARITY_EVEN,
	'Space': serial.PARITY_SPACE,
	'Mark': serial.PARITY_MARK
}
validStopBits = {
	'1': serial.STOPBITS_ONE,
	'1.5': serial.STOPBITS_ONE_POINT_FIVE,
	'2': serial.STOPBITS_TWO
}
panelButtonKeyNums = {
	'butPanelSourceV' : '12',
	'butPanelSourceI' : '19',
	'butPanelOutput' : '24'
}
controlAndConfigDefinitions = {
	###########		TERMINAL_OPTIONS	#########
	'checkShowLocal' : {'section':'TERMINAL_OPTIONS', 'option':'SHOW_LOCAL'},
	'checkShowReplies' : {'section':'TERMINAL_OPTIONS', 'option':'SHOW_REPLIES'},
	'checkShowErrors' : {'section':'TERMINAL_OPTIONS', 'option':'SHOW_ERRORS'},
	'checkShowSystem' : {'section':'TERMINAL_OPTIONS', 'option':'SHOW_SYSTEM'},
	
	###########		FILE_OPTIONS	##########
	'comboentrySeparator' : {'section':'FILE_OPTIONS', 'option':'SEPARATOR'},
	'toggleHeader' : {'section':'FILE_OPTIONS', 'option':'INCLUDE_HEADER'},
	'toggleColHeadings' : {'section':'FILE_OPTIONS', 'option':'INCLUDE_HEADINGS'},
	'checkHeaderFilePath' : {'section':'FILE_OPTIONS', 'option':'SAVE_FILE_PATH'},
	'checkHeaderDate' : {'section':'FILE_OPTIONS', 'option':'SAVE_DATE'},
	'checkHeaderSource' : {'section':'FILE_OPTIONS', 'option':'SAVE_SOURCE_FUNCTION'},
	'checkHeaderSourceRange' : {'section':'FILE_OPTIONS', 'option':'SAVE_SOURCE_RANGE'},
	'checkHeaderSense' : {'section':'FILE_OPTIONS', 'option':'SAVE_SENSE_FUNCTIONS'},
	'checkHeaderType' : {'section':'FILE_OPTIONS', 'option':'SAVE_RUN_TYPE'},
	'checkHeaderTerminals' : {'section':'FILE_OPTIONS', 'option':'SAVE_TERMINAL_POSITION'},
	'checkHeaderAutozero' : {'section':'FILE_OPTIONS', 'option':'SAVE_AUTOZERO'},
	'checkHeaderRepetitions' : {'section':'FILE_OPTIONS', 'option':'SAVE_REPETITIONS'},
	'checkHeaderLoop' : {'section':'FILE_OPTIONS', 'option':'SAVE_LOOP'},
	'checkHeaderNotes' : {'section':'FILE_OPTIONS', 'option':'SAVE_USER_NOTES'},
	'txtHeaderNotes' : {'section':'FILE_OPTIONS', 'option':'USER_NOTES'},
	
	###########		CONNECTION_OPTIONS	###########
	'comboPort' : {'section':'CONNECTION_OPTIONS', 'option':'PORT'},
	'comboBaud' : {'section':'CONNECTION_OPTIONS', 'option':'BAUDRATE'},
	'comboDataBits' : {'section':'CONNECTION_OPTIONS', 'option':'DATA_BITS'},
	'comboParity' : {'section':'CONNECTION_OPTIONS', 'option':'PARITY'},
	'comboStopBits' : {'section':'CONNECTION_OPTIONS', 'option':'STOP_BITS'},
	'comboFlowControl' : {'section':'CONNECTION_OPTIONS', 'option':'FLOW_CONTROL'},
	'spinTimeout' : {'section':'CONNECTION_OPTIONS', 'option':'TIMEOUT'},
	'spinSilenceTimeout' : {'section':'CONNECTION_OPTIONS', 'option':'SILENCE_TIMEOUT'},
	'spinCommandSpacing' : {'section':'CONNECTION_OPTIONS', 'option':'MIN_COMMAND_TIME'},
	'comboEOLChar' : {'section':'CONNECTION_OPTIONS', 'option':'EOL'},
	
	###########		RUN_OPTIONS		##########
	'toggleDisplay' : {'section':'RUN_OPTIONS', 'option':'DISPLAY_ENABLE'},
	'comboType' : {'section':'RUN_OPTIONS', 'option':'TYPE'},
	'toggleAutozero' : {'section':'RUN_OPTIONS', 'option':'AUTOZERO'},
	'checkReadV' : {'section':'RUN_OPTIONS', 'option':'READ_FUNCTIONS', 'checkListValue':'voltage'},
	'checkReadI' : {'section':'RUN_OPTIONS', 'option':'READ_FUNCTIONS', 'checkListValue':'current'},
	'checkReadR' : {'section':'RUN_OPTIONS', 'option':'READ_FUNCTIONS', 'checkListValue':'resistance'},
	'checkReadTime' : {'section':'RUN_OPTIONS', 'option':'READ_FUNCTIONS', 'checkListValue':'time'},
	'checkReadStatus' : {'section':'RUN_OPTIONS', 'option':'READ_FUNCTIONS', 'checkListValue':'status'},
	'comboTermPosition' : {'section':'RUN_OPTIONS', 'option':'TERMINAL_POSITION'},
	'toggleLock' : {'section':'RUN_OPTIONS', 'option':'LOCK_PANEL'},
	'toggleLocal' : {'section':'RUN_OPTIONS', 'option':'LOCAL_AT_END'},
	'toggleBeep' : {'section':'RUN_OPTIONS', 'option':'BEEP'},
	'toggleResetTime' : {'section':'RUN_OPTIONS', 'option':'RESET_TIME'},
	'toggleAutoErrorQuery' : {'section':'RUN_OPTIONS', 'option':'AUTO_ERROR_QUERY'},
	## SENSE ////////////////////////////////
	'checkSenseV' : {'section':'RUN_OPTIONS', 'option':'SENSE_FUNCTIONS', 'checkListValue':'voltage'},
	'checkSenseI' : {'section':'RUN_OPTIONS', 'option':'SENSE_FUNCTIONS', 'checkListValue':'current'},
	'checkSenseR' : {'section':'RUN_OPTIONS', 'option':'SENSE_FUNCTIONS', 'checkListValue':'resistance'},
	'comboSenseVRange' : {'section':'RUN_OPTIONS', 'option':'SENSE_V_RANGE'},
	'comboSenseIRange' : {'section':'RUN_OPTIONS', 'option':'SENSE_I_RANGE'},
	'comboSenseRRange' : {'section':'RUN_OPTIONS', 'option':'SENSE_R_RANGE'},
	'spinSenseVProtection' : {'section':'RUN_OPTIONS', 'option':'SENSE_V_LIMIT'},
	'spinSenseIProtection' : {'section':'RUN_OPTIONS', 'option':'SENSE_I_LIMIT', 'numberPart':'factor'},
	'spinSenseIProtectionOrder' : {'section':'RUN_OPTIONS', 'option':'SENSE_I_LIMIT', 'numberPart':'order'},
	## SOURCE ////////////////////////////
	'comboSource' : {'section':'RUN_OPTIONS', 'option':'SOURCE'},
	'comboSourceVRange' : {'section':'RUN_OPTIONS', 'option':'SOURCE_V_RANGE'},
	'comboSourceIRange' : {'section':'RUN_OPTIONS', 'option':'SOURCE_I_RANGE'},
	'comboSourceVProtection' : {'section':'RUN_OPTIONS', 'option':'SOURCE_V_LIMIT'},
	'checkSourceDelayAuto' : {'section':'RUN_OPTIONS', 'option':'SOURCE_DELAY_AUTO'},
	'spinSourceDelay' : {'section':'RUN_OPTIONS', 'option':'SOURCE_DELAY_VALUE'},
	'toggleAutoOutput' : {'section':'RUN_OPTIONS', 'option':'SOURCE_OUTPUT_AUTO'},
	## SWEEP //////////////////////////
	'comboSweepControl' : {'section':'RUN_OPTIONS', 'option':'SWEEP_CONTROL'},
	'spinSweepRepetitions' : {'section':'RUN_OPTIONS', 'option':'SWEEP_REPETITIONS'},
	'spinSweepRepeatDelay' : {'section':'RUN_OPTIONS', 'option':'SWEEP_REPEAT_DELAY'},
	'comboSweepLoop' : {'section':'RUN_OPTIONS', 'option':'SWEEP_LOOP'},
	'spinPointAverages' : {'section':'RUN_OPTIONS', 'option':'SWEEP_POINT_AVERAGES'},
	'comboSweepSpacing' : {'section':'RUN_OPTIONS', 'option':'SWEEP_BUILTIN_SPACING'},
	'comboSweepStepMode' : {'section':'RUN_OPTIONS', 'option':'SWEEP_BUILTIN_STEP_MODE'},
	'spinSweepPoints' : {'section':'RUN_OPTIONS', 'option':'SWEEP_BUILTIN_POINTS'},
	'comboSweepRanging' : {'section':'RUN_OPTIONS', 'option':'SWEEP_BUILTIN_RANGING'},
	'comboSweepDirection' : {'section':'RUN_OPTIONS', 'option':'SWEEP_BUILTIN_DIRECTION'},
	
	#########	SOURCE_RANGE	###########
	'spinVMin' : {'section':'SOURCE_RANGE', 'option':'SOURCE_V_MIN', 'numberPart':'factor'},
	'spinVMinOrder' : {'section':'SOURCE_RANGE', 'option':'SOURCE_V_MIN', 'numberPart':'order'},
	'spinVMax' : {'section':'SOURCE_RANGE', 'option':'SOURCE_V_MAX', 'numberPart':'factor'},
	'spinVMaxOrder' : {'section':'SOURCE_RANGE', 'option':'SOURCE_V_MAX', 'numberPart':'order'},
	'spinVStep' : {'section':'SOURCE_RANGE', 'option':'SOURCE_V_STEP', 'numberPart':'factor'},
	'spinVStepOrder' : {'section':'SOURCE_RANGE', 'option':'SOURCE_V_STEP', 'numberPart':'order'},
	'spinIMin' : {'section':'SOURCE_RANGE', 'option':'SOURCE_I_MIN', 'numberPart':'factor'},
	'spinIMinOrder' : {'section':'SOURCE_RANGE', 'option':'SOURCE_I_MIN', 'numberPart':'order'},
	'spinIMax' : {'section':'SOURCE_RANGE', 'option':'SOURCE_I_MAX', 'numberPart':'factor'},
	'spinIMaxOrder' : {'section':'SOURCE_RANGE', 'option':'SOURCE_I_MAX', 'numberPart':'order'},
	'spinIStep' : {'section':'SOURCE_RANGE', 'option':'SOURCE_I_STEP', 'numberPart':'factor'},
	'spinIStepOrder' : {'section':'SOURCE_RANGE', 'option':'SOURCE_I_STEP', 'numberPart':'order'},
	
	#############	DISPLAY_OPTIONS		#################
	'comboXVar' : {'section':'DISPLAY_OPTIONS', 'option':'X_VAR'},
	'comboYVar' : {'section':'DISPLAY_OPTIONS', 'option':'Y_VAR'},
	'colorbuttonTerminalBackground' : {'section':'DISPLAY_OPTIONS', 'option':'TERMINAL_BACKGROUND_COLOR'},
	'colorbuttonLocal' : {'section':'DISPLAY_OPTIONS', 'option':'LOCAL_COLOR'},
	'colorbuttonReply' : {'section':'DISPLAY_OPTIONS', 'option':'REPLY_COLOR'},
	'colorbuttonError' : {'section':'DISPLAY_OPTIONS', 'option':'ERROR_COLOR'},
	'colorbuttonSystem' : {'section':'DISPLAY_OPTIONS', 'option':'SYSTEM_COLOR'},
	'checkAutoScroll' : {'section':'DISPLAY_OPTIONS', 'option':'TERMINAL_AUTOSCROLL'},
	'colorbuttonBackground' : {'section':'DISPLAY_OPTIONS', 'option':'FIGURE_BACKGROUND_COLOR'},
	'colorbuttonBox' : {'section':'DISPLAY_OPTIONS', 'option':'BOX_COLOR'},
	'spinBoxLineWidth' : {'section':'DISPLAY_OPTIONS', 'option':'BOX_LINE_WIDTH'},
	'colorbuttonPoints' : {'section':'DISPLAY_OPTIONS', 'option':'POINTS_COLOR'},
	'comboPointsShape' : {'section':'DISPLAY_OPTIONS', 'option':'POINTS_SHAPE'},
	'spinPointsSize' : {'section':'DISPLAY_OPTIONS', 'option':'POINTS_SIZE'},
	'colorbuttonAxis' : {'section':'DISPLAY_OPTIONS', 'option':'AXIS_COLOR'},
	'spinAxisLineWidth' : {'section':'DISPLAY_OPTIONS', 'option':'AXIS_LINE_WIDTH'},
	'colorbuttonTrace' : {'section':'DISPLAY_OPTIONS', 'option':'TRACE_COLOR'},
	'spinTraceLineWidth' : {'section':'DISPLAY_OPTIONS', 'option':'TRACE_LINE_WIDTH'},
	'colorbuttonTickMark' : {'section':'DISPLAY_OPTIONS', 'option':'TICK_MARK_COLOR'},
	'spinTickMarkLineWidth' : {'section':'DISPLAY_OPTIONS', 'option':'TICK_MARK_LINE_WIDTH'},
	'spinTickMarkLength' : {'section':'DISPLAY_OPTIONS', 'option':'TICK_MARK_LENGTH'},
	'colorbuttonGrid' : {'section':'DISPLAY_OPTIONS', 'option':'GRID_COLOR'},
	'spinGridLineWidth' : {'section':'DISPLAY_OPTIONS', 'option':'GRID_LINE_WIDTH'},
	'colorbuttonAxisLabel' : {'section':'DISPLAY_OPTIONS', 'option':'AXIS_LABEL_COLOR'},
	'colorbuttonAxisTitle' : {'section':'DISPLAY_OPTIONS', 'option':'AXIS_TITLE_COLOR'},
	'spinDivisions' : {'section':'DISPLAY_OPTIONS', 'option':'DIVISIONS'}
}
queryCommands = [
	':configure?',
	
	':calculate:math:expression?',
	':calculate:math:expression:catalog?',
	':calculate:math:expression:name?',
	':calculate:math:units?',
	':calculate:state?',
	':calculate2:feed?',
	':calculate2:null:offset?',
	':calculate2:null:state?',
	':calculate2:limit1:compliance:fail?',
	':calculate2:limit1:compliance:source2?',
	':calculate2:limit1:state?',
	':calculate2:limit1:fail?',
	':calculate2:limit2:upper:data?',
	':calculate2:limit2:upper:data? default',
	':calculate2:limit2:upper:data? minimum',
	':calculate2:limit2:upper:data? maximum',
	':calculate2:limit2:upper:source2?',
	':calculate2:limit2:lower:data?',
	':calculate2:limit2:lower:data? default',
	':calculate2:limit2:lower:data? minimum',
	':calculate2:limit2:lower:data? maximum',
	':calculate2:limit2:lower:source2?',
	':calculate2:limit2:pass:source2?',
	':calculate2:limit2:state?',
	':calculate2:limit2:fail?',
	':calculate2:limit3:upper:data?',
	':calculate2:limit3:upper:data? default',
	':calculate2:limit3:upper:data? minimum',
	':calculate2:limit3:upper:data? maximum',
	':calculate2:limit3:upper:source2?',
	':calculate2:limit3:lower:data?',
	':calculate2:limit3:lower:data? default',
	':calculate2:limit3:lower:data? minimum',
	':calculate2:limit3:lower:data? maximum',
	':calculate2:limit3:lower:source2?',
	':calculate2:limit3:pass:source2?',
	':calculate2:limit3:state?',
	':calculate2:limit3:fail?',
	':calculate2:climits:bcontrol?',
	':calculate2:climits:mode?',
	':calculate3:format?',
	
	':display:enable?',
	':display:window1:text:data?',
	':display:window1:text:state?',
	':display:window1:data?',
	':display:window1:attributes?',
	':display:window2:text:data?',
	':display:window2:text:state?',
	':display:window2:data?',
	':display:window2:attributes?',
	':display:digits?',
	':display:digits? default',
	':display:digits? minimum',
	':display:digits? maximum',
	
	':format:sregister?',
	':format:data?',
	':format:border?',
	':format:elements:sense?',
	':format:elements:calculate?',
	':format:source2?',
	
	':output:state?',
	':output:interlock:state?',
	':output:smode?',
	
	':route:terminals?',
	
	':sense:function:concurrent?',
	':sense:function:on:count?',
	':sense:function:on?',
	':sense:function:off:count?',
	':sense:function:off?',
	':sense:function:state? "voltage"',
	':sense:function:state? "current"',
	':sense:function:state? "resistance"',
	':sense:current:range:upper?',
	':sense:current:range:auto?',
	':sense:current:range:auto:ulimit?',
	':sense:current:range:auto:llimit?',
	':sense:current:nplcycles?',
	':sense:current:nplcycles? default',
	':sense:current:nplcycles? minimum',
	':sense:current:nplcycles? maximum',
	':sense:current:protection:level?',
	':sense:current:protection:level? default',
	':sense:current:protection:level? minimum',
	':sense:current:protection:level? maximum',
	':sense:current:protection:tripped?',
	':sense:voltage:range:upper?',
	':sense:voltage:range:auto?',
	':sense:voltage:range:auto:ulimit?',
	':sense:voltage:range:auto:llimit?',
	':sense:voltage:nplcycles?',
	':sense:voltage:nplcycles? default',
	':sense:voltage:nplcycles? minimum',
	':sense:voltage:nplcycles? maximum',
	':sense:voltage:protection:level?',
	':sense:voltage:protection:level? default',
	':sense:voltage:protection:level? minimum',
	':sense:voltage:protection:level? maximum',
	':sense:voltage:protection:tripped?',
	':sense:resistance:mode?',
	':sense:resistance:ocompensated?',
	':sense:resistance:range:upper?',
	':sense:resistance:range:auto?',
	':sense:resistance:range:auto:ulimit?',
	':sense:resistance:range:auto:llimit?',
	':sense:resistance:nplcycles?',
	':sense:resistance:nplcycles? default',
	':sense:resistance:nplcycles? minimum',
	':sense:resistance:nplcycles? maximum',
	':sense:average:tcontrol?',
	':sense:average:count?',
	':sense:average:count? default',
	':sense:average:count? minimum',
	':sense:average:count? maximum',
	':sense:average:state?',
	
	':source:clear:auto?',
	':source:clear:auto:mode?',
	':source:function:mode?',
	':source:delay?',
	':source:delay? default',
	':source:delay? minimum',
	':source:delay? maximum',
	':source:delay:auto?',
	':source:current:mode?',
	':source:current:range?',
	':source:current:range? default',
	':source:current:range? minimum',
	':source:current:range? maximum',
	':source:current:range:auto?',
	':source:current:level:immediate:amplitude?',
	':source:current:level:immediate:amplitude? default',
	':source:current:level:immediate:amplitude? minimum',
	':source:current:level:immediate:amplitude? maximum',
	':source:current:level:triggered:amplitude?',
	':source:current:level:triggered:amplitude? minimum',
	':source:current:level:triggered:amplitude? maximum',
	':source:current:level:triggered:amplitude? default',
	':source:current:start?',
	':source:current:start? default',
	':source:current:start? minimum',
	':source:current:start? maximum',
	':source:current:stop?',
	':source:current:stop? default',
	':source:current:stop? minimum',
	':source:current:stop? maximum',
	':source:current:step?',
	':source:current:step? default',
	':source:current:step? minimum',
	':source:current:step? maximum',
	':source:current:span?',
	':source:current:span? default',
	':source:current:span? minimum',
	':source:current:span? maximum',
	':source:current:center?',
	':source:current:center? default',
	':source:current:center? minimum',
	':source:current:center? maximum',
	':source:voltage:mode?',
	':source:voltage:range?',
	':source:voltage:range? default',
	':source:voltage:range? minimum',
	':source:voltage:range? maximum',
	':source:voltage:range:auto?',
	':source:voltage:level:immediate:amplitude?',
	':source:voltage:level:immediate:amplitude? default',
	':source:voltage:level:immediate:amplitude? minimum',
	':source:voltage:level:immediate:amplitude? maximum',
	':source:voltage:level:triggered:amplitude?',
	':source:voltage:level:triggered:amplitude? minimum',
	':source:voltage:level:triggered:amplitude? maximum',
	':source:voltage:level:triggered:amplitude? default',
	':source:voltage:protection:level?',
	':source:voltage:protection:level? default',
	':source:voltage:protection:level? minimum',
	':source:voltage:protection:level? maximum',
	':source:voltage:protection:tripped?',
	':source:voltage:start?',
	':source:voltage:start? default',
	':source:voltage:start? minimum',
	':source:voltage:start? maximum',
	':source:voltage:stop?',
	':source:voltage:stop? default',
	':source:voltage:stop? minimum',
	':source:voltage:stop? maximum',
	':source:voltage:step?',
	':source:voltage:step? default',
	':source:voltage:step? minimum',
	':source:voltage:step? maximum',
	':source:voltage:span?',
	':source:voltage:span? default',
	':source:voltage:span? minimum',
	':source:voltage:span? maximum',
	':source:voltage:center?',
	':source:voltage:center? default',
	':source:voltage:center? minimum',
	':source:voltage:center? maximum',
	':source:soak?',
	':source:sweep:spacing?',
	':source:sweep:points?',
	':source:sweep:points? default',
	':source:sweep:points? minimum',
	':source:sweep:points? maximum',
	':source:sweep:direction?',
	':source:sweep:ranging?',
	':source:list:current?',
	':source:list:current:points?',
	':source:list:voltage?',
	':source:list:voltage:points?',
	':source:memory:points?',
	':source:memory:points? default',
	':source:memory:points? minimum',
	':source:memory:points? maximum',
	':source:memory:start?',
	':source:pulse:width?',
	':source:pulse:width? default',
	':source:pulse:width? minimum',
	':source:pulse:width? maximum',
	':source:pulse:delay?',
	':source:pulse:delay? default',
	':source:pulse:delay? minimum',
	':source:pulse:delay? maximum',
	
	':source2:bsize?',
	':source2:ttl?',
	':source2:ttl:level?',
	':source2:ttl:default?',
	':source2:ttl:actual?',
	':source2:ttl4:mode?',
	':source2:ttl4:bstate?',
	':source2:clear:auto?',
	':source2:clear:auto:delay?',
	':source2:clear:auto:delay? default',
	':source2:clear:auto:delay? minimum',
	':source2:clear:auto:delay? maximum',
	
	':status:measurement:event?',
	':status:measurement:enable?',
	':status:measurement:condition?',
	':status:operation:event?',
	':status:operation:enable?',
	':status:operation:condition?',
	':status:questionable:event?',
	':status:questionable:enable?',
	':status:questionable:condition?',
	':status:queue:next?',
	':status:queue:enable?',
	':status:queue:disable?',
	
	':system:posetup?',
	':system:error:count?',
	':system:error:next?',
	':system:error:all?',
	':system:error:code:next?',
	':system:error:code:all?',
	':system:rsense?',
	':system:key?',
	':system:guard?',
	':system:beeper:state?',
	':system:azero:state?',
	':system:azero:caching:state?',
	':system:azero:caching:nplcycles?',
	':system:lfrequency?',
	':system:lfrequency:auto?',
	':system:time?',
	':system:time:reset:auto?',
	':system:rcmode?',
	':system:mep:state?',
	':system:version?',
	
	':trace:free?',
	':trace:points?',
	':trace:points? default',
	':trace:points? minimum',
	':trace:points? maximum',
	':trace:points:actual?',
	':trace:feed?',
	':trace:feed:control?',
	':trace:tstamp:format?',
	
	':arm:sequence1:layer1:count?',
	':arm:sequence1:layer1:count? default',
	':arm:sequence1:layer1:count? minimum',
	':arm:sequence1:layer1:count? maximum',
	':arm:sequence1:layer1:source?',
	':arm:sequence1:layer1:timer?',
	':arm:sequence1:layer1:tconfigure:direction?',
	':arm:sequence1:layer1:tconfigure:asynchronous:iline?',
	':arm:sequence1:layer1:tconfigure:asynchronous:oline?',
	':arm:sequence1:layer1:tconfigure:asynchronous:output?',
	
	':trigger:sequence1:count?',
	':trigger:sequence1:count? default',
	':trigger:sequence1:count? minimum',
	':trigger:sequence1:count? maximum',
	':trigger:sequence1:delay?',
	':trigger:sequence1:delay? default',
	':trigger:sequence1:delay? minimum',
	':trigger:sequence1:delay? maximum',
	':trigger:sequence1:source?',
	':trigger:sequence1:tconfigure:direction?',	
	':trigger:sequence1:tconfigure:asynchronous:iline?',
	':trigger:sequence1:tconfigure:asynchronous:input?',
	':trigger:sequence1:tconfigure:asynchronous:oline?',
	':trigger:sequence1:tconfigure:asynchronous:output?',
	':trigger:sequence2:source?',
	':trigger:sequence2:tout?'
]

FIXED_DISPLAY_OPTIONS = {
	'STATUSBAR_GENERAL_DEFAULT_MESSAGE' : "Ready.",	# not customizable

	'CONNECTED_FOREGROUND_COLOR' : Gdk.RGBA(0,1,0,1),	# not customizable
	'CONNECTED_BACKGROUND_COLOR' : Gdk.RGBA(0,0,0,1),	# not customizable
	'DISCONNECTED_FOREGROUND_COLOR' : Gdk.RGBA(1,0,0,1),	# not customizable
	'DISCONNECTED_BACKGROUND_COLOR' : Gdk.RGBA(0,0,0,1),	# not customizable

	'PANEL_NORMAL_COLOR' : Gdk.RGBA(0.5,1,1,1),	# not customizable
	'PANEL_BLINK_COLOR' : Gdk.RGBA(1,0.5,0.9,1),	# not customizable
}

# need a way to convert string to boolean
def boolstr(string):
	# look for 0, false, no, off, or '', everything else is true
	return not string.lower() in ('0','false', 'no', 'off','')
# and we frequently want to get a number as a number in the form of #.## x 10^#
def expform(number):
	# number can be int, float, string, decimal, etc.
	number = Decimal(number)
	if number != Decimal('0'):
		order = Decimal(int(math.floor(math.log(abs(number),10))))	# e.g. = Decimal('24')
	else:
		# can't take log of zero
		order = Decimal('0')
	factor = number / (Decimal(10) ** order)	# e.g. = Decimal('1.556')
	return factor, order	# Decimal('1.556'), Decimal('24') for number = 1.556e24
# sometimes we need to get a single item from a dictionary
def firstkey(dictionary):
	myKey = None
	if type(dictionary is dict):
		for key in dictionary:
			myKey = key
			# run only once
			break
	return myKey
	
# the pySerial class's .write() method doesn't append the eol character automatically
# so we'll define our own
def serialSend(self, command):
	self.write(command + self.eolChar)
serial.Serial.send = serialSend
# likewise, a new .read() method is nice too
def serialRead(self):
	# here lies the old read code, the problem was the use of inWaiting
	# The loop stopped when there were no more characters in the buffer
	# But sometimes the machine wasn't done sending data - you just read
	# the buffer to fast and drained it.
	'''
	# give the port time to receive the reply
	time.sleep(self.readDelay)
	reply = ""
	while (self.inWaiting() > 0):
		reply += self.read(1)
	reply = reply.rstrip(self.eolChar)
	return reply
	'''
	# with the new code, we only quit when the machine says it's finished
	# or when it seems to have stopped sending to the buffer 
	reply = ""
	timeProfile = []
	startTime = time.time()
	while 1:
		timeNow = time.time()
		if self.inWaiting() > 0:
			char = self.read(1)
			try:
				altChar = char	# so I can change char to something printable w/o affecting code below
				if char == self.eolChar:
					altChar = '[EOL]'
				timeProfile.append({'time':timeNow-startTime, 'char':altChar})
				#timeProfile.append("WAITED "+str(timeNow-startTime)+" seconds for character: "+altChar)
			except:
				# app may not yet be initialized (happens during __init__)
				pass
			if char == self.eolChar:
				break
			else:
				reply += char
			# reset the timer
			startTime = time.time()
		elif timeNow - startTime > self.silenceTimeout:
			timeProfile.append({'time':timeNow-startTime, 'char':None})
			#timeProfile.append("WAITED "+str(timeNow-startTime)+" seconds but got NO CHARACTER.")
			# too much time has elapsed since the last character was sent
			# we can't wait forever!
			break
		# this can go on a while, don't let ui freeze:
		while(Gtk.events_pending()):
			Gtk.main_iteration()
	return reply, timeProfile
serial.Serial.getReply = serialRead
# a command to clear the read buffer
def serialPurge(self):
	while 1:
		if self.inWaiting() > 0:
			char = self.read(1)
		else:
			break
		# this can go on a while, don't let ui freeze:
		while(Gtk.events_pending()):
			Gtk.main_iteration()
serial.Serial.purge = serialPurge
# and I think adding text to the textview is clumsy
def textviewClear(self):
	buffer = self.get_buffer()
	buffer.set_text('')
Gtk.TextView.clear = textviewClear
def textviewSetText(self, text, gdkFColor=None, gdkBColor=None):
	self.clear()
	self.add_text(text, gdkFColor, gdkBColor)
Gtk.TextView.set_text = textviewSetText
def textviewAddText(self, text, gdkFColor=None, gdkBColor=None):
	buffer = self.get_buffer()
	tag = buffer.create_tag(None)
	if gdkFColor != None:
		tag.set_property('foreground_rgba',gdkFColor)
	if gdkBColor != None:
		tag.set_property('background_rgba',gdkBColor)
	end = buffer.get_end_iter()
	buffer.insert_with_tags(end, text, tag)
Gtk.TextView.add_text = textviewAddText
# even getting text from a textview is clumsy
def textviewGetText(self, getHiddenChars=True):
	buffer = self.get_buffer()
	firstLine = buffer.get_start_iter()
	lastLine = buffer.get_end_iter()
	text = buffer.get_text(firstLine, lastLine, getHiddenChars)
	return text
Gtk.TextView.get_text = textviewGetText
# SpinButton's return floats but decimals are more convenient
def getSpinButtonValueAsDecimal(self):
	# trick is to convert the float returned by get_value() to a Decimal
	# and round to the number of places shown in the spin button
	# we have to generate a string-number with as many decimal places
	# as in the spin box so we can use the decimal.quantize method
	dDigits = Decimal(int(self.get_digits()))
	compareString = "1."
	for i in xrange(0, dDigits):
		compareString += "0"
	# now compareString should be something like "1.00"
	floatValue = self.get_value()
	dValue = Decimal(floatValue).quantize(Decimal(compareString))
	return dValue
Gtk.SpinButton.get_decimal_value = getSpinButtonValueAsDecimal
# showing text in a status bar is overly complicated as well and doesn't allow color
def statusbarGetLabel(self):
	box = self.get_message_area()
	children = box.get_children()
	label = children[0]
	return label
Gtk.Statusbar.get_label = statusbarGetLabel
def statusbarSetText(self, text):
	label = self.get_label()
	# we're using our custom set_text() method for the label which does color too
	label.set_text(text)
Gtk.Statusbar.set_text = statusbarSetText
def statusbarGetText(self):
	label = self.get_label()
	text = label.get_text()
	return text
Gtk.Statusbar.get_text = statusbarGetText
def statusbarSetFColor(self, gdkRGBA):
	label = self.get_label()
	label.fColor = gdkRGBA
	self.set_text(self.get_text())
Gtk.Statusbar.set_fColor = statusbarSetFColor
def statusbarSetBColor(self, gdkRGBA):
	label = self.get_label()
	label.fColor = gdkRGBA
	self.set_text(self.get_text())
Gtk.Statusbar.set_bColor = statusbarSetBColor
# Gdk.RGBA doesn't have a to_hex() method which we need to feed colors to Pango markup
def rgbaToHex(self):
   return "#{0:02x}{1:02x}{2:02x}{3:02x}".format(int(self.red * 255), int(self.green * 255), int(self.blue * 255), int(self.alpha * 255))  
Gdk.RGBA.to_hex = rgbaToHex
def rgbaToHexNoAlpha(self):
   return "#{0:02x}{1:02x}{2:02x}".format(int(self.red * 255), int(self.green * 255), int(self.blue * 255))  
Gdk.RGBA.to_hex_no_alpha = rgbaToHexNoAlpha
def getRGBATuple(self):
	r = self.red
	g = self.green
	b = self.blue
	a = self.alpha
	return r,g,b,a
Gdk.RGBA.get_rgba_tuple = getRGBATuple
# labels should have easy color functions too
Gtk.Label.fColor = Gdk.RGBA(0,0,0,1)	# black
Gtk.Label.bColor = Gdk.RGBA(0,0,0,0)	# transparent
def labelSetText(self, text):
	# This block gets the #000000 color for the Pango markup
	# Set the color alpha to 0 to make 100% transparent
	if self.fColor.alpha != 0:
		fColorString = " foreground='" + self.fColor.to_hex_no_alpha() + "'"
	else:
		fColorString = ''
	if self.bColor.alpha != 0:
		bColorString = " background='" + self.bColor.to_hex_no_alpha() + "'"
	else:
		bColorString = ''
	if not self.get_use_markup():
		self.set_use_markup(True)
	self.set_markup("<span" + fColorString + bColorString + ">" + text + "</span>")
Gtk.Label.set_text = labelSetText	
def labelSetFColor(self, gdkRGBA):
	self.fColor = gdkRGBA
	self.set_text(self.get_text())
Gtk.Label.set_fColor = labelSetFColor
def labelSetBColor(self, gdkRGBA):
	self.bColor = gdkRGBA
	self.set_text(self.get_text())
Gtk.Label.set_bColor = labelSetBColor
# nice to be able to set GtkComboBoxText's by value
def comboboxtextGetPosition(self, searchValue, isCaseSensitive=True):
	model = self.get_model()
	found = False
	position = 0
	row = model.get_iter_first()
	while 1:
		if not row:
			break
		valueFromCombo = model.get_value(row, 0)
		if isCaseSensitive and valueFromCombo == searchValue:
			found = True
			break
		elif (not isCaseSensitive) and (valueFromCombo.lower() == searchValue.lower()):
			found = True
			break
		row = model.iter_next(row)
		position += 1
	if found:
		result = position
	else:
		result = -1
	return result
Gtk.ComboBoxText.get_position = comboboxtextGetPosition
def comboboxtextSelect(self, value, isCaseSensitive=True):
	position = self.get_position(value, isCaseSensitive)
	if position != -1:	# not found
		self.set_active(position)
		selected = True
	else:
		selected = False
	return selected
Gtk.ComboBoxText.select = comboboxtextSelect
# color buttons don't easily import the rgba strings they export
def colorbuttonSetRGBAString(self, rgbaString):
	color = Gdk.RGBA(0,0,0,0)	# dummy instance for creating color
	color.parse(rgbaString)
	self.set_rgba(color)
Gtk.ColorButton.set_rgba_string = colorbuttonSetRGBAString

class pyKControl:

	def __init__(self):
		self.updatePanelGraphics = False
		self.terminalHistory = []
		self.terminalHistoryPosition = -1
		self.measurementDate = ''
		self.lastDataFile = ''
		self.lastCommandTime = 0
		self.panelIsBusy = False		
		self.readyForEvents = False
		
		self.guiFile = 'pyK-Control 2400.glade'
		self.builder = Gtk.Builder()
		self.builder.add_from_file(self.guiFile)
		self.builder.connect_signals(self)
		self.get("mainWindow").show()
		# once the interface is loaded, we can log exceptions on the 
		# debug tab (so we start having 'try' blocks below)
		
		# this is a work-around to right-align controls in a GtkGrid
		self.get('boxSweepControls').set_hexpand(False)
		
		self.initializeStyles()
		self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
		
		# filefilter names can't be set in Glade, pity
		self.get("filefilterAll").set_name("All files (*)")
		self.get("filefilterTXT").set_name("*.txt")
		self.get("filefilterCSV").set_name("*.csv")
		self.get("filefilterDAT").set_name("*.dat")
		self.get("filefilterPNG").set_name("PNG")
		self.get("filefilterJPG").set_name("JPEG")
		self.get("filefilterBMP").set_name("Bitmap")
		self.get("filefilterConfig").set_name("Config files (*.config)")
		
		self.loadCombos()
		try:
			self.loadDefaultConfig()
		except Exception:
			self.error()
		
		#self.fabricateData(20)		# fake data for debugging
		
		try:
			self.isConnected = False
			self.serialPort = serial.Serial()
			self.configureSerialPort()
			self.connect()
		except Exception:
			self.error()
		
		self.readyForEvents = True
		#GLib.timeout_add_seconds(1,self.getMeterCurrentDisplayState)
	def onDeleteWindow(self, *args):
		# if a serialPort has been opened, we'll try returning the 
		# source meter to local control before leaving
		try:
			self.serialPort
			self.serialPort.send(":system:local")
			Gtk.main_quit(*args)
		except:
			Gtk.main_quit(*args)
	def get(self, id):
		# this is a shortcut method for self.builder.get_object()
		widget = self.builder.get_object(id)
		return widget
	def getFrom(self, controlID):
		# is this function even useful?
		control = self.get(controlID)
		t = type(control)
		if t is Gtk.ToggleButton:
			value = control.get_active()
		elif t is Gtk.ComboBoxText:
			value = control.get_active_text()
		elif t is Gtk.CheckButton:
			value = control.get_active()
		elif t is Gtk.SpinButton:
			value = control.get_decimal_value()
		elif t is Gtk.ColorButton:
			# reading color from colorbutton seems to depend on pyGObject bindings version?
			try:
				value = control.get_rgba()
			except:
				color = Gdk.RGBA(0,0,0,0)
				control.get_rgba(color)
				value = color
		elif t is Gtk.TextView:
			value = control.get_text()
		else:
			# can't determine type, don't know how to get value
			value = None
		return value
	def option(self, option, section=None, colorsAsStrings=False, noLineBreaksInText=False):
		# this function gets the value of an option from the control(s)
		# defined in controlAndConfigDefinitions
		# you can optionally specify a section
		value = None
		matches = {}
		for controlID, definition in controlAndConfigDefinitions.iteritems():
			if definition['option'] == option and (section == None or definition['section'] == section):
				matches[controlID] = definition
				# add value to the definition
				matches[controlID]['value'] = self.getFrom(controlID)
		if len(matches) == 1:
			controlID = firstkey(matches)
			value = matches[controlID]['value']
			if colorsAsStrings and type(value) is Gdk.RGBA:
				value = value.to_string()
			if noLineBreaksInText and type(value) is str:
				value = value.replace('\r\n','<br/>').replace('\n','<br/>')
		elif len(matches) > 1:
			# two controls used to set a single option, happens for 
			# checkboxes and spinbuttons sometimes
			firstID = firstkey(matches)
			firstDef = matches[firstID]
			controlType = type(self.get(firstID))	# assumes all matches are of same type
			# are these checkboxes that belong to a group?
			if controlType is Gtk.CheckButton and 'checkListValue' in firstDef:
				values = []
				for controlID, definition in matches.iteritems():
					# value will be true/false (active/not active)
					if definition['value']:
						values.append(definition['checkListValue'])
				if len(values) > 0:
					value = ','.join(values)
				else:
					value = ''
			# maybe this is a set of spin buttons
			elif controlType is Gtk.SpinButton and 'numberPart' in firstDef:
				parts = {}
				for controlID, definition in matches.iteritems():
					parts[definition['numberPart']] = definition['value']	# sets either order or factor
				value = parts['factor'] * Decimal(10)**(parts['order'])		# combine to a single number
		return value
	def error(self,indicatorOn=True):
		# use self.error(False) to clear the indicator icon or 
		# self.error() to log an error to the unhandled exceptions log
		indicator = self.get('imageErrorIndicator')
		if indicatorOn == False:
			indicator.hide()
			indicator.set_tooltip_text('')
		else:
			if CATCH_EXCEPTIONS:
				# exception_type, exception_obj, exception_traceback = sys.exc_info()
				# the line above shows the tuple returned by sys.exc_info()
				# Apparently it's bad practice to assign this tuple to local variables so 
				# we have to always use sys.exc_info()[#] to reference them.
				fname = os.path.split(sys.exc_info()[2].tb_frame.f_code.co_filename)[1]
				errText = "Error in ["+fname+"] line ["+str(sys.exc_info()[2].tb_lineno)+"]: "+sys.exc_info()[1].message
				self.get('textviewUnhandledExceptions').add_text(str(errText)+"\n")
				indicator.show()
				indicator.set_tooltip_text(errText)
			else:
				raise
	def getFilechooserDataSave(self):
		filechooser = self.get("filechooserGenericSave")
		filters = filechooser.list_filters()
		for filter in filters:
			filechooser.remove_filter(filter)
		filechooser.add_filter(self.get("filefilterAll"))
		filechooser.add_filter(self.get("filefilterTXT"))
		filechooser.add_filter(self.get("filefilterCSV"))
		filechooser.add_filter(self.get("filefilterDAT"))
		self.get('comboImageType').hide()	# only shown when saving graph image
		return filechooser
	def getFilechooserTerminalSave(self):
		filechooser = self.get("filechooserGenericSave")
		filters = filechooser.list_filters()
		for filter in filters:
			filechooser.remove_filter(filter)
		filechooser.add_filter(self.get("filefilterAll"))
		filechooser.add_filter(self.get("filefilterTXT"))
		self.get('comboImageType').hide()	# only shown when saving graph image
		return filechooser
	def getFilechooserGraphSave(self):
		filechooser = self.get("filechooserGenericSave")
		filters = filechooser.list_filters()
		for filter in filters:
			filechooser.remove_filter(filter)
		filechooser.add_filter(self.get("filefilterAll"))
		filechooser.add_filter(self.get("filefilterPNG"))
		filechooser.add_filter(self.get("filefilterJPG"))
		filechooser.add_filter(self.get("filefilterBMP"))
		self.get('comboImageType').show()
		return filechooser
	def getFilechooserConfigSave(self):
		filechooser = self.get("filechooserGenericSave")
		filters = filechooser.list_filters()
		for filter in filters:
			filechooser.remove_filter(filter)
		filechooser.add_filter(self.get("filefilterAll"))
		filechooser.add_filter(self.get("filefilterConfig"))
		self.get('comboImageType').hide()
		return filechooser
	def getFilechooserConfigLoad(self):
		filechooser = self.get('filechooserGenericOpen')
		filters = filechooser.list_filters()
		for filter in filters:
			filechooser.remove_filter(filter)
		filechooser.add_filter(self.get('filefilterAll'))
		filechooser.add_filter(self.get('filefilterConfig'))
		return filechooser
	def initializeStyles(self):
		self.applyTerminalStyle()
		self.applyStatusBarStyles()
		self.get('imageConnected').hide()
		self.get('labelConnection').set_fColor(FIXED_DISPLAY_OPTIONS['DISCONNECTED_FOREGROUND_COLOR'])
		self.get('labelConnection').set_bColor(FIXED_DISPLAY_OPTIONS['DISCONNECTED_BACKGROUND_COLOR'])
		self.warn(False)
		self.get('imageErrorIndicator').hide()
	def loadPanelImage(self):
		# NOT USED ANYMORE
		# but left because it's good example code
		pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_size("2400.png", 800, 326)
		image = Gtk.Image.new_from_pixbuf(pixbuf)
		self.get('imgPanel').set_from_pixbuf(pixbuf)
	def loadCombos(self):
		# setting the 'active' property from the glade builder file doesn't work
		# so we have to select the default options in all combo boxes manually
		
		# connection tab
		self.get('comboPort').select('0')
		self.get('comboBaud').select('57600')
		self.get('comboDataBits').select('8')
		self.get('comboStopBits').select('1')
		self.get('comboParity').select('None')
		self.get('comboFlowControl').select('None')
		self.get('comboEOLChar').select('LF')
		# Run Setup tab
		self.get('comboSource').set_active(0)
		self.get('comboSourceVProtection').select('20')
		self.get('comboSourceVRange').set_active(0)
		self.get('comboSourceIRange').set_active(0)
		self.get('comboSenseVRange').set_active(0)
		self.get('comboSenseIRange').set_active(0)
		self.get('comboSenseRRange').set_active(0)
		self.get('comboSweepControl').set_active(0)
		self.get('comboType').set_active(0)
		self.get('comboTermPosition').set_active(0)
		self.get('comboSweepSpacing').set_active(0)
		self.get('comboSweepStepMode').set_active(0)
		self.get('comboSweepRanging').set_active(0)
		self.get('comboSweepDirection').set_active(0)
		self.get('comboSweepLoop').set_active(0)
		# File tab
		self.get('comboentrySeparator').set_active(0)
		# Graph Save As dialog
		self.get('comboImageType').set_active(0)
		# Display Options Tab
		self.get('comboXVar').set_active(0)
		self.get('comboYVar').set_active(1)
		self.get('comboPointsShape').set_active(0)
	def statusBar(self,text):
		self.get("statbarGeneral").set_text(text)
		self.get('textviewStatusBarLog').add_text(text+"\n")
	def message(self, text):
		self.get('textviewMessages').add_text(str(text)+"\n")
	def warn(self,text):
		indicator = self.get('imageWarningIndicator')
		if text == None or text == '' or text == False:
			indicator.hide()
			indicator.set_tooltip_text('')
		else:
			self.message('WARNING: '+text)
			indicator.show()
			indicator.set_tooltip_text(text)
	def loadDefaultConfig(self):
		sysFile = 'pyK-Control 2400.config'
		userDir = os.path.expanduser('~')
		userFile = os.path.join(userDir,"pyK-Control 2400.config")
		# config files found in both locations will be read
		# the latter will override the former
		self.loadConfig([sysFile, userFile])
		# we want to update any display changes
		self.onDisplayOptionsChange()
	def fabricateData(self, numberOfRows):
		# this function generates some data in the table for debugging table interaction
		store = self.get('liststoreData')
		for i in range (0, numberOfRows):
			row = ["","","","",""]
			for k in range (0, 5):
				randomNumber = random.random()
				row[k] = str(randomNumber)
			store.append(row)
	def connect(self):
		# if the user clicks disconnect before we're finished, it will cause an error
		self.get('butDisconnect').set_sensitive(False)
		self.get('menuitemConnectionDisconnect').set_sensitive(False)
		# if using Windows, trying to re-open an already open port will fail
		self.serialPort.close()
		try:
			self.serialPort.open()
		except:
			print 'couldnt open port'
		self.serialPort.purge()
		# some times the source meter doesn't respond to the first command sent
		# so we'll always issue a query as the first command and ignore it's reply
		command = ":system:clear"
		self.processSystemCommand(command)
		# Note: the above does not guarantee the connection was successfully opened
		# use isConnected to test
		self.isConnected = False
		self.processSystemCommand(":system:clear")	# clear all errors
		reply = self.processSystemCommand(":system:error?")	# ask for any errors, should be none
		if reply == "0,\"No error\"":
			self.isConnected = True
			self.showConnectionStatus(True)
			self.statusBar("Port opened, connection verified.")
		# unlock interface elements
		self.get('butDisconnect').set_sensitive(True)
		self.get('menuitemConnectionDisconnect').set_sensitive(True)
		return self.isConnected
	def disconnect(self):
		self.serialPort.close()
		self.isConnected = False
		self.statusBar("Port closed.")
		self.showConnectionStatus(False)
	def showConnectionStatus(self,status):
		if status:
			self.get('imageConnected').show()
			self.get('imageDisconnected').hide()
			self.get('labelConnection').set_fColor(FIXED_DISPLAY_OPTIONS['CONNECTED_FOREGROUND_COLOR'])
			self.get('labelConnection').set_bColor(FIXED_DISPLAY_OPTIONS['CONNECTED_BACKGROUND_COLOR'])
			self.get('labelConnection').set_text('Connected')
		else:
			self.get('imageConnected').hide()
			self.get('imageDisconnected').show()
			self.get('labelConnection').set_fColor(FIXED_DISPLAY_OPTIONS['DISCONNECTED_FOREGROUND_COLOR'])
			self.get('labelConnection').set_bColor(FIXED_DISPLAY_OPTIONS['DISCONNECTED_BACKGROUND_COLOR'])
			self.get('labelConnection').set_text('Not Connected')
	def onConnectionConfigChange(self, *args):
		try:
			if self.readyForEvents:
				self.configureSerialPort()
				self.connect()
		except Exception:
			self.error()
	def onButConnectClick(self, *args):
		try:
			self.connect()
		except Exception:
			self.error()
	def onButDisconnectClick(self, *args):
		try:
			self.disconnect()
		except Exception:
			self.error()

	def onButStartClick(self, *args):
		try:
			self.stop = False
			min, max, step = self.getMinMaxStep()
			if self.option('SOURCE') == 'Voltage':
				function = 'voltage'
			else:
				function = 'current'
			self.toggleControlsForSweep(False)
			self.onButClearClick()	# clear data, graph, graph footer
			self.statusBar("Sweep in progress...")
			self.setGraphFooterForCurrentRun()
			self.measurementDate = datetime.datetime.now()
			repetitions = self.option('SWEEP_REPETITIONS')
			delay = self.option('SWEEP_REPEAT_DELAY')
			# to measure, we can do a manual sweep, requesting each set pt. and
			# measuring the result, or we can do an auto sweep, setting the
			# range and getting all the data back at once
			if self.option('SWEEP_CONTROL') == 'Manual':
				result = self.doManualSweep(min, max, step, function, repetitions, delay)
			else:
				result = self.doBuiltInSweep(min, max, step, function, repetitions, delay)

			self.toggleControlsForSweep()

			if result == "ok":
				self.statusBar("Sweep complete.")
			elif result == "stopped":
				self.statusBar("Sweep stopped.")
			elif result == 'range error':
				self.statusBar("Invalid source range.")
			elif result == 'conenct error':
				self.statusBar('Could not connect.')
			elif result == 'delay read error':
				self.statusBar('Timeout getting stabalization delay.')
			elif result == 'functions read error':
				self.statusBar('Timeout getting read functions.')
		except Exception:
			self.error()
	def toggleControlsForSweep(self, state=True):
		# state = true means put back in the normal state, most buttons unlocked
		# false means lock most buttons, used during a sweep
		
		# These controls are normally on but locked during the sweep
		# Menu
		self.get('menuitemRunStart').set_sensitive(state)
		self.get('menuitemFileSaveData').set_sensitive(state)
		self.get('menuitemFileSaveGraph').set_sensitive(state)
		self.get('menuitemFileSaveTerminal').set_sensitive(state)
		self.get('menuitemFileImportConfig').set_sensitive(state)
		self.get('menuitemEditCopySelectedData').set_sensitive(state)
		self.get('menuitemEditCopyAllData').set_sensitive(state)
		self.get('menuitemEditCopyGraph').set_sensitive(state)
		self.get('menuitemEditClearData').set_sensitive(state)
		self.get('menuitemViewPanel').set_sensitive(state)
		self.get('menuitemConnectionConnect').set_sensitive(state)
		self.get('menuitemConnectionDisconnect').set_sensitive(state)
		# Run
		self.get('butStart').set_sensitive(state)
		self.get('frameVSource').set_sensitive(state)
		self.get('frameISource').set_sensitive(state)
		# Run Setup
		self.get('frameSourceOptions').set_sensitive(state)
		self.get('frameSenseOptions').set_sensitive(state)
		self.get('frameSweepOptions').set_sensitive(state)
		self.get('frameMiscOptions').set_sensitive(state)
		# Connection
		self.get('frameConnection').set_sensitive(state)
		# Terminal
		self.get('gridQuery').set_sensitive(state)
		self.get('entryCommand').set_sensitive(state)
		self.get('butTerminalExecute').set_sensitive(state)
		# Panel
		self.get('boxPanelPage').set_sensitive(state)
		# Config
		self.get('butImportConfigApply').set_sensitive(state)
		
		
		# These controls are normally locked but on during the sweep
		self.get('butStop').set_sensitive(not state)
		self.get('menuitemRunStop').set_sensitive(not state)
	def doManualSweep(self, min, max, step, function, repetitions, delay):
		checkErrors = self.option('AUTO_ERROR_QUERY')
		# before we send any commands, best to check that we have a sane range
		if (step != 0) and ((step > 0 and min < max) or (step < 0 and min > max)):
			result = 'ok'
		else:
			result = 'range error'
		
		if result == 'ok':
			self.statusBar("Initializing...")
			result = self.initSourceMeter(checkErrors)
		
		if result == 'ok':	
			# if the meter is waiting the stabalize, we'd better wait at least this long
			# before trying to read the result, get value directly from meter
			# MOVE THIS INTO A LOOP WITH "READ?" IF AUTO DELAY IS ON!
			timeToStabalize = self.processLocalCommand(":source:delay?", checkErrors)
			if timeToStabalize != '':
				timeToStabalize = Decimal(timeToStabalize)
			else:
				result = 'delay read error'
				
		if result == 'ok':
			# we'll need to know what values are going to be returned by the source meter
			# function list from source meter looks like: "VOLT,CURR,RES"
			reply = self.processLocalCommand(":format:elements?", checkErrors)
			if reply != '':
				readFunctions = reply.split(",")
			else:
				result = 'functions read error'
		
		if result == 'ok':
			# there will be a delay for each averaged point
			averages = self.option('SWEEP_POINT_AVERAGES')
			# main loop will not start if prior checks failed
			def mainLoop(result, setpoint, end, step, function):
				oldTimeout = self.serialPort.silenceTimeout
				newTimeout = oldTimeout + (timeToStabalize + averages * Decimal('0.06'))	# time per average determined empirically
				self.message('Silence timeout adjusted to '+str(newTimeout)+
					' seconds during sweep to accommodate '+str(averages)+' averages.')
				self.serialPort.silenceTimeout = newTimeout	# returned to original value below
				# before turning on output, make sure source is zero
				self.processLocalCommand(":source:" + function + ":level:immediate:amplitude 0", checkErrors)
				# turn on output
				self.processLocalCommand(":output:state on", checkErrors)
				while result ==  'ok':
					# move to next setpoint
					self.processLocalCommand(":source:" + function + ":level:immediate:amplitude " + str(setpoint), checkErrors)
					# read what that gives you
					reply = self.processLocalCommand(":read?", checkErrors)
					self.processReadReplyFromManualSweep(reply, readFunctions)
					self.updateGraph()
					setpoint += step
					# since all of these values should be Decimals (not floats),
					# the comparison is safe
					if (step > 0 and setpoint > end) or (step < 0 and setpoint < end):
						break
					# sweeping can take a while, we need to allow other events to process
					# while we're working in this loop, such as refreshing the treeview
					# to which we're adding rows
					while(Gtk.events_pending()):
						Gtk.main_iteration()
					if self.stop:
						result = "stopped"
						break
				# turn output back off
				self.processLocalCommand(":output:state off", checkErrors)
				self.serialPort.silenceTimeout = oldTimeout
				return result
			
			originalMin = min
			originalMax = max
			originalStep = step
			for i in range(0, repetitions):
				# reset min and max for next repetition
				min = originalMin
				max = originalMax
				step = originalStep
				self.statusBar("("+str(i+1)+") Sweeping...")
				start,end = self.getStartEnd(min, max, step)
				setpoint = start
				result = mainLoop(result, setpoint, end, step, function)
				# full loop?
				if self.option('SWEEP_LOOP') == 'Full' and result == 'ok':
					# reverse start and end
					end, start = self.getStartEnd(min, max, step)
					step = -step
					setpoint = start
					result = mainLoop(result, setpoint, end, step, function)
				if result != 'ok':
					break
				# kill time until next run
				startTime = Decimal(time.time())
				while delay > 0 and i+1 != repetitions:		#i+1 check makes sure you don't delay after last 
					timePassed = Decimal(time.time())-startTime
					if timePassed < delay:
						timeLeft = (delay - timePassed).quantize(Decimal("1"))	# formatted to one digit
						self.statusBar("Waiting "+str(timeLeft)+" seconds until next repetition.")
						while(Gtk.events_pending()):
							Gtk.main_iteration()
						if self.stop:
							result = "stopped"
							break
						# let's not kill the cpu
						time.sleep(0.1)
					else:
						break
				
		self.deinitSourceMeter(checkErrors)
		return result
	def processReadReplyFromManualSweep(self, reply, readFunctions):
		# the reply is a string of comma separated values that need to 
		# be parsed to voltage, current, resistance, etc.
		# readFunctions is a list of the things read back from the source meter
		# in the same order as they appear in the reply string
		
		view = self.get('treeviewData')
		columns = view.get_columns()
		store = self.get('liststoreData')
		
		row = ["<NO DATA>","<NO DATA>","<NO DATA>","<NO DATA>","<NO DATA>"]
		# convert string to a list
		if reply != '':
			values = str(reply).split(",")
		else:
			# this usually happens if the source meter is taking too long
			# to measure a data point, i.e. the read? query is timing out
			# before the measurement is done
			values = []
			self.warn('No data returned from :read? during manual sweep. '
				'This usually happens because the :read? query is timing out '
				'before the source meter is finished measuring a data point. '
				'Try increasing the silence timeout (Connection tab).')
		# list may have upto 5 values, we need to figure out what they are
		# so we can put them in the right columns of our liststore
		columnTitles = []
		for column in columns:
			columnTitles.append(column.get_title().lower())
		# the function in readFunctions should be in the same order as
		# the corresponding data in the reply, so we're really iterating
		# over both, i.e. readFunctions(i) corresponds to values(i)
		for i,value in enumerate(values):
			try:
				functionName = readFunctions[i]
				if functionName == "VOLT":
					index = columnTitles.index("voltage")
				elif functionName == "CURR":
					index = columnTitles.index("current")
				elif functionName == "RES":
					index = columnTitles.index("resistance")
				elif functionName == "TIME":
					index = columnTitles.index("time")
				elif functionName == "STAT":
					index = columnTitles.index("status")
				row[index] = value
			except:
				self.warn('Error parsing reply from manual sweep.')
		store.append(row)
	def doBuiltInSweep(self, min, max, step, function, repetitions, delay):
		result = 'ok'
		checkErrors = self.option('AUTO_ERROR_QUERY')
		# sanity checks
		if self.option('SWEEP_BUILTIN_STEP_MODE') == 'Step Size':
			# do we have a valid range?
			if not ((step != 0) and ((step > 0 and min < max) or (step < 0 and min > max))):
				result = 'range error'
		elif self.option('SWEEP_BUILTIN_SPACING') == 'Logarithmic':
			# can only set number of points for log run, but you can't go <= 0 b/c log(<=0) = undefined
			if min <= 0 or max <= 0:
				result = 'range error'
		
		if result == 'ok':		
			self.statusBar("Initializing...")
			result = self.initSourceMeter(checkErrors)
			
		if result == 'ok':	
			# if the meter is waiting the stabalize, we'd better wait at least this long
			# before trying to read the result, get value directly from meter
			# MOVE THIS INTO A LOOP WITH "READ?" IF AUTO DELAY IS ON!
			timeToStabalize = self.processLocalCommand(":source:delay?", checkErrors)
			if timeToStabalize != '':
				timeToStabalize = Decimal(timeToStabalize)
			else:
				result = 'delay read error'
				
		if result == 'ok':
			# we'll need to know what values are going to be returned by the source meter
			# function list from source meter looks like: "VOLT,CURR,RES"
			reply = self.processLocalCommand(":format:elements?", checkErrors)
			if reply != '':
				readFunctions = reply.split(",")
			else:
				result = 'functions read error'

		if result == 'ok':
			# there will be a delay for each averaged point
			averages = self.option('SWEEP_POINT_AVERAGES')
			def sweep(start, end, step, function, checkErrors, readFunctions):
				self.processLocalCommand(":source:"+function+":start "+str(start), checkErrors)
				self.processLocalCommand(":source:"+function+":stop "+str(end), checkErrors)
				if self.option('SWEEP_BUILTIN_STEP_MODE') == 'Step Size':
					# this will override any :source:[voltage]:points command sent earlier
					self.processLocalCommand(":source:"+function+":step "+str(step), checkErrors)
					# but the trigger count must be the same as the new number of points
					points = self.processLocalCommand(":source:sweep:points?", checkErrors)
					self.processLocalCommand(":trigger:count "+points, checkErrors)
					points = Decimal(points)	# need in Decimal form for calc below
				else:
					points = self.option('SWEEP_BUILTIN_POINTS')
				self.processLocalCommand(":output:state on", checkErrors)
				
				# we'll extend the timeout to give enough time for each measurement
				oldTimeout = self.serialPort.silenceTimeout
				newTimeout = oldTimeout + (timeToStabalize * averages + averages * Decimal('0.08'))	# time per average determined empirically
				self.message('Silence timeout adjusted to '+str(newTimeout)+
					' seconds during sweep to accommodate '+str(averages)+' averages.')
				self.serialPort.silenceTimeout = newTimeout	# returned to original value below
				
				self.statusBar("Waiting for data...")
				reply = self.processLocalCommand(":read?", checkErrors)
				self.processLocalCommand(":output:state off", checkErrors)
				self.processReadReplyFromBuiltInSweep(reply, readFunctions, points)
				self.updateGraph()
				self.serialPort.silenceTimeout = oldTimeout
				
			originalMin = min
			originalMax = max
			originalStep = step
			for i in range(0, repetitions):
				# reset min and max for next repetition
				min = originalMin
				max = originalMax
				step = originalStep
				start,end = self.getStartEnd(min, max, step)
				sweep(start, end, step, function, checkErrors, readFunctions)
				
				# first check if stop button is pressed, etc.
				while(Gtk.events_pending()):
					Gtk.main_iteration()
				if self.stop:
					result = "stopped"
					break
					
				# full loop?
				if self.option('SWEEP_LOOP') == 'Full' and result == 'ok':
					# reverse start and end
					end, start = self.getStartEnd(min, max, step)
					step = -step
					sweep(start, end, step, function, checkErrors, readFunctions)
					
				# kill time until next run
				startTime = Decimal(time.time())
				while delay > 0 and i+1 != repetitions:		#i+1 check makes sure you don't delay after last 
					timePassed = Decimal(time.time())-startTime
					if timePassed < delay:
						timeLeft = (delay - timePassed).quantize(Decimal("1"))	# formatted to one digit
						self.statusBar("Waiting "+str(timeLeft)+" seconds until next repetition.")
						while(Gtk.events_pending()):
							Gtk.main_iteration()
						if self.stop:
							result = "stopped"
							break
						# let's not kill the cpu
						time.sleep(0.1)
					else:
						break
				if result != 'ok':
					break
		self.deinitSourceMeter(checkErrors)
		return result
	def processReadReplyFromBuiltInSweep(self, reply, readFunctions, points):
		# the reply from a built in sweep is a long list of comma separated
		# values like: V1, I1, R1, V2, I2, R2, V3, I3, R3....etc.
		view = self.get('treeviewData')
		columns = view.get_columns()
		store = self.get('liststoreData')
		
		# get liststore column titles as a list
		columnTitles = []
		for column in columns:
			columnTitles.append(column.get_title().lower())
		
		# readFunctions are VOLT, CURR, RES, TIME, and STAT
		# it'd be easier if they were: voltage, current, resistance...etc.
		for i in range(0, len(readFunctions)):
			functionName = readFunctions[i]
			if functionName == "VOLT":
				readFunctions[i] = "voltage"
			elif functionName == "CURR":
				readFunctions[i] = "current"
			elif functionName == "RES":
				readFunctions[i] = "resistance"
			elif functionName == "TIME":
				readFunctions[i] = "time"
			elif functionName == "STAT":
				readFunctions[i] = "status"
		
		row = ["<NO DATA>","<NO DATA>","<NO DATA>","<NO DATA>","<NO DATA>"]
		# convert string to a list
		if reply != '':
			values = str(reply).split(",")
		else:
			# this usually happens if the source meter is taking too long
			# to measure a data point, i.e. the read? query is timing out
			# before the measurement is done
			values = []
			self.warn('No data returned from :read? during built-in sweep. '
				'This usually happens because the :read? query is timing out '
				'before the source meter is finished measuring a data point. '
				'Try increasing the silence timeout (Connection tab).')
		
		# did the source meter return the expected number of points?
		returnedValues = len(values)
		expectedValues = points * len(readFunctions)
		if returnedValues < expectedValues:
			self.warn('The number of data points returned from the built-in '
						'sweep is less than expected.')
		elif returnedValues > expectedValues:
			# why would this ever happen?
			self.warn('The number of data points returned from the built-in '
						'sweep is more than expected.')
		
		functionIndex = 0
		for value in values:
			# where we are in the readFunctions list tells us what type Value is
			try:
				function = readFunctions[functionIndex]		# voltage, current, etc.
				# we need to know what column this type is in
				colIndex = columnTitles.index(function)
				# put the data in the column of the new row
				row[colIndex] = value
			except:
				self.warn('Error parsing reply from built-in sweep.')
			functionIndex += 1
			# when we run out of readFunctions, the row must be finished
			if functionIndex >= len(readFunctions):
				functionIndex = 0
				store.append(row)
	def setGraphFooterForCurrentRun(self):
		text = ""
		text += self.option('SWEEP_CONTROL') + ' (' \
				+ self.option('SWEEP_BUILTIN_SPACING') + '), '
		text += "Source: " + self.option('SOURCE') + "\n"
		text += self.option('TYPE') + ', '
		if self.option('AUTOZERO'):
			autozeroState = 'On'
		else:
			autozeroState = 'Off'
		text += "Autozero: " + autozeroState + ', '
		text += self.option('TERMINAL_POSITION') + ' terminals'
		self.get('labelGraphFooter').set_text(text)
	def onButStopClick(self, *args):
		self.stop = True
	def initSourceMeter(self, checkErrors):
		result = 'ok'
		self.configureSerialPort()	# always reconnect after re-configure in case port changes
		if not self.connect():
			result = 'connect error'
		else:
			initList = self.buildSourceMeterInitCommands()
			for command in initList:
				self.processLocalCommand(command, checkErrors)
				# don't make gui events wait for this loop to end
				while(Gtk.events_pending()):
					Gtk.main_iteration()
				if self.stop:
					result = 'stopped'
					break
		return result
	def deinitSourceMeter(self, checkErrors):
		# turn off filter (averaging)
		self.processLocalCommand(":sense:average:state off", checkErrors)
		# make sure controls are unlocked
		if self.option('LOCK_PANEL'):
			# only way out is to go local
			self.processLocalCommand(":system:local", False)
		# return to local?
		if self.option('LOCAL_AT_END'):
			# error checking must be false with :local command else you'll return
			# to remote state and get an error
			self.processLocalCommand(":system:local", False)
	def getMinMaxStep(self):
		# notice in this section we're doing everything as decimals (not floats)
		# values depend on whether we're sourcing voltage or current
		if self.option('SOURCE') == 'Voltage':
			min = self.option('SOURCE_V_MIN')
			max = self.option('SOURCE_V_MAX')
			step = self.option('SOURCE_V_STEP')
		else:
			min = self.option('SOURCE_I_MIN')
			max = self.option('SOURCE_I_MAX')
			step = self.option('SOURCE_I_STEP')
		return min, max, step
	def getStartEnd(self, min, max, step):
		if step > 0:
			start = min
			end = max
		else:
			start = max
			end = min
		return start, end
	def buildSourceMeterInitCommands(self):
		initList = [];
		
		# MANDATORY ////////////////////////////
		# allow sensing V, I, and R simultaneously
		initList.append(":sense:function:concurrent on")
		
		# MISC //////////////////////////////
		# front display on or off?
		if self.option('DISPLAY_ENABLE'):
			initList.append(":display:enable on")
		else:
			initList.append(":display:enable off")
		# 2-wire or 4-wire measurement?
		if self.option('TYPE') == '4-wire':
			initList.append(":system:rsense on")
		else:
			initList.append(":system:rsense off")
		# autozero on every measurement?
		if self.option('AUTOZERO'):
			initList.append(":system:azero:state on")
		else:
			initList.append(":system:azero:state off")
		# what values to return when read is called?
		elementString = self.option('READ_FUNCTIONS')
		initList.append(":format:elements " + elementString)
		# use front or rear terminals?
		position = self.option('TERMINAL_POSITION')
		initList.append(":route:terminals "+position)
		# lock panel during measurement?
		if self.option('LOCK_PANEL'):
			initList.append(":system:rwlock")
		# beep when output is turned on?
		if self.option('BEEP'):
			initList.append(":system:beep:state on")
		else:
			initList.append(":system:beep:state off")
		# reset device time?
		if self.option('RESET_TIME'):
			initList.append(":system:time:reset")
		
		# SENSE ////////////////////////////////
		# how to measure the resistance? NOTE: this must be manual for sweeping
		# and it needs to come before the protection:level commands because
		# they will error if resistance mode is auto
		initList.append(":sense:resistance:mode manual")
		# which parameters to measure? (note: source parameter will return the set value)
		initList.append(":sense:function:off:all")	# start with all off
		onFunctionString = self.option('SENSE_FUNCTIONS')
		# need to add quotes around each, bah
		functions = onFunctionString.split(',')
		for i in range(0, len(functions)):
			functions[i] = "'"+functions[i]+"'"
		onFunctionString = ",".join(functions)
		initList.append(":sense:function:on " + onFunctionString)
		# how to set measurement ranges?
		if self.option('SENSE_V_RANGE') == 'Auto':
			initList.append(":sense:voltage:range:auto on")
		else:
			initList.append(':sense:voltage:range '+self.option('SENSE_V_RANGE'))
		if self.option('SENSE_I_RANGE') == 'Auto':
			initList.append(":sense:current:range:auto on")
		else:
			initList.append(':sense:current:range '+self.option('SENSE_I_RANGE'))
		if self.option('SENSE_R_RANGE') == 'Auto':
			initList.append(":sense:resistance:range:auto on")
		else:
			initList.append(":sense:resistance:range:auto off")
			initList.append(':sense:resistance:range '+self.option('SENSE_R_RANGE'))
		# what are the limits when measuring?
		VLimit = self.option('SENSE_V_LIMIT')
		ILimit = self.option('SENSE_I_LIMIT')
		initList.append(":sense:voltage:protection:level " + str(VLimit))
		initList.append(":sense:current:protection:level " + str(ILimit))
		
		# SOURCE ////////////////////////////
		# source V or I?
		function = self.option('SOURCE').lower()
		initList.append(":source:function:mode "+function)
		# how to set the range for the source meter voltage?
		if self.option('SOURCE_V_RANGE') == 'Auto':
			initList.append(":source:voltage:range:auto on")
		if self.option('SOURCE_I_RANGE') == 'Auto':
			initList.append(":source:current:range:auto on")
		# how high to allow source? (enforced for V when sourcing I)
		VLimit = self.option('SOURCE_V_LIMIT')
		initList.append(":source:voltage:protection:level " + str(VLimit))
		# how to set the source stabalization delay?
		if self.option('SOURCE_DELAY_AUTO'):
			initList.append(":source:delay:auto on")
		else:
			initList.append(":source:delay:auto off")
			delay = self.option('SOURCE_DELAY_VALUE')
			initList.append(":source:delay " + str(delay))
		# turn output on/off before/after each measured point?
		if self.option('SOURCE_OUTPUT_AUTO'):
			initList.append(":source:clear:auto on")
		else:
			initList.append(":source:clear:auto off")
		
		# SWEEP MODE ////////////////////////
		# should we do the sweeping manually or let the source meter handle it
		function = self.option('SOURCE').lower()	# current or voltage
		if self.option('SWEEP_CONTROL') == 'Manual':
			initList.append(":source:"+function+":mode fixed")
			initList.append(":trigger:count 1")
		else:	# built-in sweep
			initList.append(":source:"+function+":mode sweep")
			ranging = self.option('SWEEP_BUILTIN_RANGING').lower()
			initList.append(":source:sweep:ranging "+ranging)
			spacing = self.option('SWEEP_BUILTIN_SPACING').lower()
			initList.append(":source:sweep:spacing "+spacing)
			if self.option('SWEEP_BUILTIN_STEP_MODE') == 'Points':
				points = self.option('SWEEP_BUILTIN_POINTS')
				initList.append(":source:sweep:points "+str(points))
				initList.append(":trigger:count "+str(points))	# same as number of points
			else:
				# use step size set on run tab, set during self.doBuiltInSweep
				pass
			direction = self.option('SWEEP_BUILTIN_DIRECTION').lower()
			initList.append(":source:sweep:direction "+direction)
		# point averaging is the same for manual or built-in
		averages = self.option('SWEEP_POINT_AVERAGES')
		initList.append(":sense:average:tcontrol repeat")
		initList.append(":sense:average:count "+str(averages))
		initList.append(":sense:average:state on")
		
		return initList

	def onComboSourceChange(self, combo, *args):
		if combo.get_active_text() == 'Voltage':
			self.get('frameVSource').show()
			self.get('frameISource').hide()
			# can't set a range for source function
			self.get('comboSenseVRange').set_sensitive(False)
			self.get('comboSenseVRange').select('Auto')
			self.get('comboSenseIRange').set_sensitive(True)
		else:
			self.get('frameISource').show()
			self.get('frameVSource').hide()
			self.get('comboSenseVRange').set_sensitive(True)
			self.get('comboSenseIRange').set_sensitive(False)
			self.get('comboSenseIRange').select('Auto')
	def onSpinSenseVProtectionChange(self, *args):
		# trigger some validation
		rangeCombo = self.get('comboSenseVRange')
		self.onComboSenseVRangeChange(rangeCombo)
	def onComboSenseVRangeChange(self, combo, *args):
		try:
			limit = self.option('SENSE_V_LIMIT')
			range = self.option('SENSE_V_RANGE')
			if range != 'Auto' and range != None and limit != 'Auto' and limit != '':
				limit = Decimal(limit)
				range = Decimal(range)
				# the range can't be 10x higher (or more) than the limit
				# e.g. if limit is 2.2, range can be 21 (or 2.1 or 0.21) but not 210
				if range >= limit * Decimal('10'):
					model = combo.get_model()
					currentRange = combo.get_active_iter()
					nextLowerRange = model.iter_next(currentRange)
					if nextLowerRange:
						self.warn('Attempted to set sense voltage range 10x higher (or more) than '
									'the sense voltage protection limit. Adjusted sense range '
									'to an acceptable value.')
						# don't worry if this isn't low enough - the change event will fire
						# again when we set this and select a lower range, and so on
						combo.set_active_iter(nextLowerRange)
					else:
						# no more lower ranges, luckily, lowest range
						# will never trigger an error
						pass
				# if the source current range is at max (1.05A) the sense voltage range can't be higher than 21V
				if range == Decimal('210'):
					sourceIRange = self.option('SOURCE_I_RANGE')
					if sourceIRange != 'Auto' and Decimal(sourceIRange) >= Decimal('1.05'):
						self.warn('Sense voltage range cannot be at maximum (210 V) while source '
								'current range is at maximum (1.05 A). Adjusted sense range '
								'to an acceptable value.')
						combo.select('21')
		except Exception:
			self.error()
	def onComboSourceIRangeChange(self, combo, *args):
		# trigger some validation
		rangeCombo = self.get('comboSenseVRange')
		self.onComboSenseVRangeChange(rangeCombo)
	def onSpinSenseIProtectionChange(self, *args):
		# trigger some validation
		rangeCombo = self.get('comboSenseIRange')
		self.onComboSenseIRangeChange(rangeCombo)
	def onSpinSenseIProtectionOrderChange(self, *args):
		# trigger some validation
		rangeCombo = self.get('comboSenseIRange')
		self.onComboSenseIRangeChange(rangeCombo)
	def onComboSenseIRangeChange(self, combo, *args):
		try:
			limit = self.option('SENSE_I_LIMIT')
			range = self.option('SENSE_I_RANGE')
			if range != 'Auto' and range != None and limit != 'Auto' and limit != '':
				limit = Decimal(limit)
				range = Decimal(range)
				# the range can't be 10x higher (or more) than the limit
				# e.g. if limit is 1.1e-3, range can be 1.05e-2 (or e-3, or e-4, etc.) but not 1.05e-1
				if range >= limit * Decimal('10'):
					model = combo.get_model()
					currentRange = combo.get_active_iter()
					nextLowerRange = model.iter_next(currentRange)
					if nextLowerRange:
						self.warn('Attempted to set sense current range 10x higher (or more) than '
									'the sense current protection limit. Adjusted sense range '
									'to an acceptable value.')
						# don't worry if this isn't low enough - the change event will fire
						# again when we set this and select a lower range, and so on
						combo.set_active_iter(nextLowerRange)
					else:
						# no more lower ranges, luckily, lowest range
						# will never trigger an error
						pass
				# if the source voltage range is at max (210V) the sense current range can't be higher than 0.105 A
				if range == Decimal('1.05'):
					sourceVRange = self.option('SOURCE_V_RANGE')
					if sourceVRange != 'Auto' and Decimal(sourceVRange) >= Decimal('210'):
						self.warn('Sense current range cannot be at maximum (1.05 A) while source '
								'voltage range is at maximum (210 V). Adjusted sense range '
								'to an acceptable value.')
						combo.select('1.05e-1')
		except Exception:
			self.error()
	def onComboSourceVRangeChange(self, combo, *args):
		# trigger some validation
		rangeCombo = self.get('comboSenseIRange')
		self.onComboSenseIRangeChange(rangeCombo)
				
	def onButGraphSaveAsClick(self, *args):
		try:
			dialog = self.getFilechooserGraphSave()
			dialog.set_current_name("untitled.png")
			response = dialog.run()
			dialog.hide()
			if response == 1:
				filename = dialog.get_filename()
				layout = self.get('layoutFigure')
				rect = layout.get_allocation()
				width = rect.width
				height = rect.height
				type = self.get('comboImageType').get_active_text()
				if type == "PNG":
					type = 'png'
				elif type == "JPEG":
					type = 'jpeg'
				else:
					type = 'bmp'
				pixbuf = Gdk.pixbuf_get_from_window(layout.get_window(),0,0,width,height)
				pixbuf.savev(filename,type, [], [])
				self.statusBar("Image saved.")
		except Exception:
			self.error()
	def onButGraphCopyClick(self, *args):
		try:
			layout = self.get('layoutFigure')
			rect = layout.get_allocation()
			width = rect.width
			height = rect.height
			pixbuf = Gdk.pixbuf_get_from_window(layout.get_window(),0,0,width,height)
			self.clipboard.set_image(pixbuf)
			self.statusBar("Image copied.")			
		except Exception:
			self.error()
		
	def onButCopyAllDataClick(self, *args):
		try:
			text = self.getDataAsText(ignoreSelection=True)
			self.clipboard.set_text(text, -1)
			self.statusBar("Data copied.")	
		except Exception:
			self.error()
	def onButCopySelectedDataClick(self, *args):
		try:
			text = self.getDataAsText()
			self.clipboard.set_text(text, -1) 	# -1 tells Gtk to determine length with strlen()
			self.statusBar("Data copied.")
		except Exception:
			self.error()
	def getDataAsText(self, ignoreSelection=False):
		treeview = self.get("treeviewData")
		columns = treeview.get_columns()
		selection = treeview.get_selection()
		treemodel, treepaths = selection.get_selected_rows()
		separator = self.option('SEPARATOR')
		
		rows = []
		if ignoreSelection:
			# get all data
			iter = treemodel.get_iter_first()
			while 1:
				if not iter:
					break
				rowData = []
				for index in xrange(0, len(columns)):
					rowData.append(treemodel.get_value(iter, index))
				rowText = separator.join(rowData)
				rows.append(rowText)
				iter = treemodel.iter_next(iter)
		elif len(treepaths) > 0:
			for path in treepaths:
				iter = treemodel.get_iter(path)
				rowData = []
				for index in xrange(0, len(columns)):
					rowData.append(treemodel.get_value(iter, index))
				rowText = separator.join(rowData)
				rows.append(rowText)
		text = "\n".join(rows)
		# final text should be comma separated data in rows separated by line feeds
		return text
	def onButClearClick(self, *args):
		try:
			self.clearData()
			self.updateGraph()
			self.get('labelGraphFooter').set_text("")
		except Exception:
			self.error()
	def clearData(self):
		store = self.get("liststoreData")
		store.clear()
		self.statusBar("Data cleared.")
	def onButDataSaveAs(self, *args):
		try:
			dialog = self.getFilechooserDataSave()
			dialog.set_current_name("untitled.txt")
			response = dialog.run()
			dialog.hide()
			if response == 1:
				text = ''
				filename = dialog.get_filename()
				self.lastDataFile = filename
				file = open(filename, 'w')
				if self.option('INCLUDE_HEADER'):
					text += self.getDataHeaderText() + "\n"
				if self.option('INCLUDE_HEADINGS'):
					text += self.getDataHeadingsText() + "\n"
				text += self.getDataAsText(True)
				file.write(text)
				file.close()
				self.statusBar("Data saved.")
		except Exception:
			self.error()
	def getDataHeaderText(self):
		text = ''
		if self.option('SAVE_FILE_PATH'):
			text += "Original Filename: " + self.lastDataFile + "\n"
		if self.option('SAVE_DATE'):
			text += "Date: " + str(self.measurementDate) + "\n"
		if self.option('SAVE_SOURCE_FUNCTION'):
			text += "Source: " + self.option('SOURCE') + "\n"
		if self.option('SAVE_SOURCE_RANGE'):
			min, max, step = self.getMinMaxStep()
			start, end = self.getStartEnd(min, max, step)
			if self.option('SOURCE') == "Voltage":
				units = " (Volts)"
			else:
				units = " (Amps)"
			text += "Start: " + str(start) + ", End: " + str(end) + ", Step: " + str(step) + units + "\n"
		if self.option('SAVE_SENSE_FUNCTIONS'):
			modeString = self.option('SENSE_FUNCTIONS')
			text += "Sense Mode(s): " + modeString + "\n"
		if self.option('SAVE_TYPE'):
			text += "Experiment Type: " + self.option('TYPE') + "\n"
		if self.option('SAVE_TERMINAL_POSITION'):
			if self.option('TERMINAL_POSITION') == 'Front':
				text += "Terminal Position: Front\n"
			else:
				text += "Terminal Position: Rear\n"
		if self.option('SAVE_AUTOZERO'):
			if self.option('AUTOZERO'):
				autozeroState = 'On'
			else:
				autozeroState = 'Off'
			text += "Autozero: " + autozeroState + "\n"
		if self.option('SAVE_REPETITIONS'):
			text += 'Number of Reptitions: ' + str(self.option('SWEEP_REPETITIONS')) + "\n"
		if self.option('SAVE_LOOP'):
			text += 'Loop: ' + str(self.option('SWEEP_LOOP')) + "\n"
		if self.option('SAVE_USER_NOTES'):
			text += self.option('USER_NOTES') + "\n"
			
		return text
	def getDataHeadingsText(self):
		text = ''
		view = self.get('treeviewData')
		columns = view.get_columns()
		titles = []
		for column in columns:
			titles.append(column.get_title())
		text = ",".join(titles)
		return text
	
	def onButTerminalExecuteClick(self, *args):
		try:
			self.sendTerminalCommand()
		except Exception:
			self.error()
	def onEntryCommandActivate(self, *args):
		try:
			self.sendTerminalCommand()
		except Exception:
			self.error()
	def onEntryCommandKeypress(self, widget, event, *args):
		entryCommand = widget
		
		if event.keyval == Gdk.KEY_Up:
			if self.terminalHistoryPosition > 0:
				self.terminalHistoryPosition += -1
				entryCommand.set_text(self.terminalHistory[self.terminalHistoryPosition])
			# keep the cursor at the end of the text
			length = entryCommand.get_text_length()
			entryCommand.set_position(length)
			return True	# prevents default action (moving focus away)
		
		if event.keyval == Gdk.KEY_Down:
			if self.terminalHistoryPosition < len(self.terminalHistory):
				self.terminalHistoryPosition += 1
				if self.terminalHistoryPosition == len(self.terminalHistory):
					# exhausted history list, put blank text
					entryCommand.set_text("")
				else:
					entryCommand.set_text(self.terminalHistory[self.terminalHistoryPosition])
			# keep the cursor at the end of the text
			length = entryCommand.get_text_length()
			entryCommand.set_position(length)
			return True	# prevents default action (moving focus away)
	def sendTerminalCommand(self):
		entryCommand = self.get('entryCommand')
		command = entryCommand.get_text()
		if not self.isConnected:
			self.statusBar("Not connected.")
			return
		self.processLocalCommand(command)
		self.statusBar("Command sent.")
		self.terminalHistory.append(command)
		self.terminalHistoryPosition = len(self.terminalHistory)
		entryCommand.set_text("")
	def onTextbufferTerminalChange(self, buffer, *args):
		# both of these techniques seem to leave off last line, which contains
		# only the cursor. I think the problem is the scroll_to_iter() method then
		# it doesn't seem to want get that last blank line (because it has no iter?)
		if self.option('TERMINAL_AUTOSCROLL'):
			endLine = buffer.get_end_iter()
			txtTerminal = self.get('txtTerminal')
			txtTerminal.scroll_to_iter(endLine,0,False,0,0)
		#if self.option('TERMINAL_AUTOSCROLL'):
			#numberOfLines = buffer.get_line_count()
			#lastLineIndex = numberOfLines-1
			#endLine = buffer.get_iter_at_line(lastLineIndex)
			#txtTerminal = self.get('txtTerminal')
			#txtTerminal.scroll_to_iter(endLine,0,False,0,0)
	def onButTerminalClearClick(self, *args):
		self.get('txtTerminal').clear()
	def onButTerminalSaveClick(self, *args):
		try:
			dialog = self.getFilechooserTerminalSave()
			dialog.set_current_name("untitled.txt")
			response = dialog.run()
			dialog.hide()
			if response == 1:
				filename = dialog.get_filename()
				file = open(filename, 'w')
				txtTerminal = self.get('txtTerminal')
				text = txtTerminal.get_text()
				file.write(text)
				file.close()
				self.statusBar("Text saved.")
		except Exception:
			self.error()
	def onButQueryClick(self, button, *args):
		try:
			if self.isConnected:
				name = Gtk.Buildable.get_name(button)
				# regex gets text after underscore in "butQuery_all", "butQuery_source", etc.
				prefix = re.search('(?<=butQuery_).*$',name).group(0)
				if prefix == 'source':
					# this prevents 'source' from running :source2:... queries
					# but we keep that behavior for :calculate#:
					prefix += ':'
				elif prefix == 'all':
					# setting this to '' will run all query commands
					prefix = ''
				self.stop = False
				self.statusBar('Querying source meter...')
				self.get('butQueryStop').set_sensitive(True)
				for command in queryCommands:
					while(Gtk.events_pending()):
						Gtk.main_iteration()
					if self.stop:
						break
					# only process queries specific to this button
					if re.search('^:'+prefix, command) != None:
						self.processLocalCommand(command)
				# done, did we cancel or finish normally?
				if self.stop:
					self.statusBar('Query cancelled.')
				else:
					self.statusBar('Query finished.')
				self.get('butQueryStop').set_sensitive(False)
			else:
				self.statusBar('Not connected.')
		except Exception:
			self.error()
	def onButQueryStopClick(self, *args):
		self.stop = True
		self.get('butQueryStop').set_sensitive(True)
	def toggleQueryButtons(self):
		# set sensitivity of all Query buttons while running a query
		pass
	
	def processCommand(self, command, checkForErrors=True):
		isQuery = False
		# regex matches ":aaa:bbb?", ":aaa:bbb? ccc", etc.
		if re.search('^(:\S+)+\?', command) != None:
			isQuery = True
			# since we're going to be reading a response, we need to
			# clear the listen buffer of anything left over from last command
			self.serialPort.purge()

		# minimum time spacing between commands prevents overflowing 
		# the source meter with commands
		timeNow = time.time()
		elapsedTime = Decimal(timeNow - self.lastCommandTime)
		if elapsedTime < self.commandSpacing:
			time.sleep(self.commandSpacing - elapsedTime)
		self.lastCommandTime = time.time()
		
		self.serialPort.send(command)
		# only get reply if sending a query command
		reply = ''
		timeProfile = []
		if isQuery:
			reply, timeProfile = self.serialPort.getReply()
			self.logResponseTimeProfile(timeProfile)
		# in case calling function needs to know the result to take action
		return reply
	def processLocalCommand(self, command, checkForErrors=True):
		if not self.isConnected:
			self.statusBar("Not connected.")
			return
		reply = self.processCommand(command, checkForErrors)
		# log conversation in terminal
		txtTerminal = self.get('txtTerminal')
		buffer = txtTerminal.get_buffer()
		if (len(command) > 0) and self.option('SHOW_LOCAL'):
			txtTerminal.add_text(str(command) + "\n", self.option('LOCAL_COLOR'))
		if (len(reply) > 0) and self.option('SHOW_REPLIES'):
			txtTerminal.add_text("\t" + str(reply) + "\n", self.option('REPLY_COLOR'))
			
		error = ''
		if checkForErrors:
			error = self.processSystemCommand(":system:error:all?")
		if len(error) > 0 and error[0] == '0':
			# 0 means no error occurred (default message returned)
			error = ''
		
		if (len(error) > 0) and self.option('SHOW_ERRORS'):
			txtTerminal.add_text("\t" + str(error) + "\n", self.option('ERROR_COLOR'))
		
		self.get("statbarLocal").set_text(command)
		self.get("statbarReply").set_text(reply)
		self.get("statbarError").set_text(error)
		
		# in case calling function needs to know the result to take action
		return reply
	def processSystemCommand(self, command):
		# use this function to run source meter commands the user shouldn't
		# normally know about
		reply = self.processCommand(command)
		txtTerminal = self.get('txtTerminal')
		buffer = txtTerminal.get_buffer()
		if (len(command) > 0) and self.option('SHOW_SYSTEM'):
			txtTerminal.add_text(str(command) + "\n", self.option('SYSTEM_COLOR'))
		if (len(reply) > 0) and self.option('SHOW_SYSTEM'):
			txtTerminal.add_text("\t" + str(reply) + "\n", self.option('SYSTEM_COLOR'))
		return reply
	def configureSerialPort(self):
		port = self.option('PORT')
		if port.isdigit():
			port = int(port)
		self.serialPort.port = port
		
		self.serialPort.baudrate = int(self.option('BAUDRATE'))
		
		# note that we have to translate some of these to serial library constants
		dataBitsSelection = self.option('DATA_BITS')
		self.serialPort.bytesize = validDataBits[dataBitsSelection]
		
		paritySelection = self.option('PARITY')
		self.serialPort.parity = validParities[paritySelection]
		
		stopBitsSelection = self.option('STOP_BITS')
		self.serialPort.stopbits = validStopBits[stopBitsSelection]

		flowControl = self.option('FLOW_CONTROL')
		if flowControl == 'XON/XOFF':
			self.serialPort.xonxoff = 1
		else:
			self.serialPort.xonxoff = 0
			
		self.serialPort.timeout = self.option('TIMEOUT') # seconds
		self.serialPort.silenceTimeout = self.option('SILENCE_TIMEOUT')	# seconds
		self.commandSpacing = self.option('MIN_COMMAND_TIME')
		
		eolChar = self.option('EOL')
		if eolChar == 'LF':
			self.serialPort.eolChar = "\n"
		elif eolChar == 'CR':
			self.serialPort.eolChar = "\r"
		elif eolChar == 'LF+CR':
			self.serialPort.eolChar = "\n\r"
		elif eolChar == 'CR+LF':
			self.serialPort.eolChar = "\r\n"
		elif eolChar == 'None':
			self.serialPort.eolChar = ""
		else:
			self.serialPort.eolChar = eolChar
		#self.debugSerialPort()
	def debugSerialPort(self):
		print "Port: " + str(self.serialPort.name)
		print "Buad Rate: " + str(self.serialPort.baudrate)
		print "Data Bits: " + str(self.serialPort.bytesize)
		print "Stop Bits: " + str(self.serialPort.stopbits)
		print "Parity: " + str(self.serialPort.parity)
		print "XON/XOFF: " + str(self.serialPort.xonxoff)
	def logResponseTimeProfile(self, timeProfile):
		# time profile is a list of strings describing wait times
		textview = self.get('textviewResponseTimeProfile')
		# Decimal conversion may fail if text entry is empty
		try:
			maxTime = Decimal(self.get('entryMaxWaitTime').get_text())
		except:
			maxTime = 0
		for record in timeProfile:
			if record['char'] != None:
				text = 'WAITED '+str(record['time'])+' seconds for character: '+record['char']
			else:
				text = 'WAITED '+str(record['time'])+' seconds but got NO CHARACTER.'
			textview.add_text(text+"\n")
			if Decimal(record['time']) > maxTime:
				self.get('entryMaxWaitTime').set_text(str(record['time']))

	def getDataAsPoints(self):
		treeview = self.get("treeviewData")
		model = treeview.get_model()
		points = {
			'xCoords' : [],
			'yCoords' : []
		}
		xVar = self.option('X_VAR')
		yVar = self.option('Y_VAR')
		headings = ['Voltage','Current','Resistance']
		xCol = headings.index(xVar)
		yCol = headings.index(yVar)
		row = model.get_iter_first()
		while 1:
			if not row:
				break
			x = model.get_value(row, xCol)
			y = model.get_value(row, yCol)
			# Decimal operation will fail if cell is '', None, or some non-number
			try:
				xCoord = Decimal(x)
			except:
				xCoord = 0
			try:
				yCoord = Decimal(y)
			except:
				yCoord = 0
			points['xCoords'].append(xCoord)
			points['yCoords'].append(yCoord)
			row = model.iter_next(row)
		return points
	def updateGraph(self):
		layout = self.get('layoutFigure')
		layout.queue_draw()
	def onLayoutFigureDraw(self, widget, cairoContext):
		try:
			layout = self.get('layoutFigure')
			# At the start of a draw handler, a clip region has been set on
			# the Cairo context, and the contents have been cleared to the
			# widget's background color. The docs for
			# gdk_window_begin_paint_region() give more details on how this
			# works.
			
			# Store points in a Dictionary! (members are lists)
			dummyData = {
				'xCoords' : [
					Decimal('-100'),
					Decimal('100'),
					Decimal('200'),
					Decimal('300'),
					Decimal('400'),
					Decimal('500')
				],
				'yCoords' : [
					Decimal('-0.4'),
					Decimal('0.2'),
					Decimal('0.45'),
					Decimal('0.6'),
					Decimal('0.75'),
					Decimal('0.8')
				]
			}
			realData = self.getDataAsPoints()
			if len(realData['xCoords']) > 0:
				data = realData
			else:
				#data = dummyData
				data = {
					'xCoords' : [],
					'yCoords' : []
				}
			
			# Draw graph box (axes)
			graphRectangle = Gdk.Rectangle()
			graphRectangle.height = 400
			graphRectangle.width = 400
			graphRectangle.y = 30
			graphRectangle.x = 70
			#graphRectangle.x2 = graphRectangle.x + graphRectangle.width
			#graphRectangle.y2 = graphRectangle.y + graphRectangle.height
			self.drawGraphBox(graphRectangle, cairoContext)
			
			# Write axis titles
			self.drawAxisTitles(graphRectangle, cairoContext)
			# Draw x,y axes to cross center of graph
			self.drawAxes(graphRectangle, cairoContext)
			# horizontal/vertical grid lines
			self.drawGrids(graphRectangle, cairoContext)
			# inside tick marks on left and bottom axes
			self.drawTicks(graphRectangle, cairoContext)
			
			if len(data['xCoords']) > 0:
				# axis labels
				self.drawAxisLabels(graphRectangle, data, cairoContext)
				# Plot data
				# note that points come second and appear on top of trace
				referencedPoints = self.getReferencedPoints(data, graphRectangle)
				self.drawTrace(referencedPoints, cairoContext)
				self.drawPoints(referencedPoints, cairoContext)		
		except Exception:
			self.error()
	def drawGraphBox(self, rectangle, ctx):
		lineWidth = self.option('BOX_LINE_WIDTH')
		ctx.set_line_width(lineWidth)
		left = rectangle.x
		top = rectangle.y
		width = rectangle.width
		height = rectangle.height
		r,g,b,a = self.option('BOX_COLOR').get_rgba_tuple()
		ctx.set_source_rgba(r,g,b,a)
		ctx.rectangle(left, top, width, height)
		ctx.stroke()
	def drawAxisTitles(self, rectangle, ctx):
		fontSize = 10	# in points
		size = int(fontSize * 1024)	# b/c pango wants 1/1024ths of a point, weird
		edgeSpacing = 2	# distance from edge of layout box which is 500 x 500
		
		color = self.option('AXIS_TITLE_COLOR').to_hex_no_alpha()
		layout = PangoCairo.create_layout(ctx)
		xVar = self.option('X_VAR')
		yVar = self.option('Y_VAR')
		if xVar == 'Voltage':
			bottomAxisText = "Voltage (V)"
		elif xVar == 'Current':
			bottomAxisText = "Current (A)"
		elif xVar == 'Resistance':
			bottomAxisText = "Resistance (&#8486;)"
		else:
			bottomAxisText = "Time (s)"
		if yVar == 'Voltage':
			leftAxisText = "Voltage (V)"
		elif yVar == 'Current':
			leftAxisText = "Current (A)"
		elif yVar == 'Resistance':
			leftAxisText = "Resistance (&#8486;)"
		else:
			leftAxisText = "Time (s)"

		layout.set_markup(
			"<span "
			+ "font_family='sans' "
			+ "size='" + str(size) + "' "
			+ "foreground='" + color + "'>"
			+ bottomAxisText
			+ "</span>"
		)
		textWidth, textHeight = layout.get_pixel_size()
		x = rectangle.x + (0.5 * rectangle.width) - (0.5 * textWidth)	# centered below graph box
		y = 500 - edgeSpacing - textHeight
		ctx.move_to(x, y)
		PangoCairo.show_layout (ctx, layout)
		
		layout.set_markup(
			"<span "
			+ "font_family='sans' "
			+ "size='" + str(size) + "' "
			+ "foreground='" + color + "'>"
			+ leftAxisText
			+ "</span>"
		)
		textWidth, textHeight = layout.get_pixel_size()
		x = edgeSpacing
		y = rectangle.y + (0.5 * rectangle.height) + (0.5 * textWidth)	# vertically centered to graph box
		ctx.move_to(x, y)
		ctx.save()
		ctx.rotate(-math.pi/2)	# angle in radians
		PangoCairo.update_layout(ctx, layout)
		PangoCairo.show_layout(ctx, layout)
		ctx.restore()
	def drawAxes(self, rectangle, ctx):
		r,g,b,a = self.option('AXIS_COLOR').get_rgba_tuple()
		ctx.set_source_rgba(r,g,b,a)
		lineWidth = self.option('AXIS_LINE_WIDTH')
		ctx.set_line_width(lineWidth)
		# horizontal
		ctx.move_to(rectangle.x, rectangle.y + rectangle.height/2)
		ctx.line_to(rectangle.x + rectangle.width, rectangle.y + rectangle.height/2)
		# vertical
		ctx.move_to(rectangle.x + rectangle.width/2, rectangle.y)
		ctx.line_to(rectangle.x + rectangle.width/2, rectangle.y + rectangle.height)
		ctx.stroke()
	def drawTicks(self, rectangle, ctx):
		lineWidth = self.option('TICK_MARK_LINE_WIDTH')
		ctx.set_line_width(lineWidth)
		r,g,b,a = self.option('TICK_MARK_COLOR').get_rgba_tuple()
		ctx.set_source_rgba(r,g,b,a)
		
		tickLength = self.option('TICK_MARK_LENGTH')
		divisions = self.option('DIVISIONS')
		ySpacing = rectangle.height/divisions
		xSpacing = rectangle.width/divisions
		# left ticks
		y = rectangle.y
		x = rectangle.x
		for i in range(0, divisions-1):	# no tick marks at start and end
			y += ySpacing
			ctx.move_to(x, y)
			ctx.line_to(x+tickLength, y)
		# top ticks
		# reset x,y
		y = rectangle.y
		x = rectangle.x
		for i in range(0, divisions-1):	# no tick marks at start and end
			x += xSpacing
			ctx.move_to(x, y)
			ctx.line_to(x, y+tickLength)
		# right ticks
		# reset x,y
		y = rectangle.y
		x = rectangle.x + rectangle.width
		for i in range(0, divisions-1):	# no tick marks at start and end
			y += ySpacing
			ctx.move_to(x, y)
			ctx.line_to(x-tickLength, y)
		ctx.stroke()
		# bottom ticks
		# reset x,y
		y = rectangle.y + rectangle.height
		x = rectangle.x
		for i in range(0, divisions-1):	# no tick marks at start and end
			x += xSpacing
			ctx.move_to(x, y)
			ctx.line_to(x, y-tickLength)
		ctx.stroke()
	def drawGrids(self, rectangle, ctx):
		lineWidth = self.option('GRID_LINE_WIDTH')
		ctx.set_line_width(lineWidth)
		r,g,b,a = self.option('GRID_COLOR').get_rgba_tuple()
		ctx.set_source_rgba(r,g,b,a)
		
		divisions = self.option('DIVISIONS')
		ySpacing = rectangle.height/divisions
		xSpacing = rectangle.width/divisions
		width = rectangle.width
		height = rectangle.height
		# horizontal grid
		y = rectangle.y
		x = rectangle.x
		for i in range(0, divisions-1):	# no grid line at start/end
			y += ySpacing
			ctx.move_to(x, y)
			ctx.line_to(x+width, y)
		# vertical grid
		# reset x,y
		y = rectangle.y
		x = rectangle.x
		for i in range(0, divisions-1):	# no grid line at start/end
			x += xSpacing
			ctx.move_to(x, y)
			ctx.line_to(x, y+height)
		ctx.stroke()
	def drawPoints(self, points, ctx):	
		size = self.option('POINTS_SIZE')
		shape = self.option('POINTS_SHAPE')
		r,g,b,a = self.option('POINTS_COLOR').get_rgba_tuple()
		ctx.set_source_rgba(r,g,b,a)
		for i in range(0,len(points['xCoords'])):
			x = points['xCoords'][i]
			y = points['yCoords'][i]
			if shape == 'Circle':
				ctx.arc(x, y, size/2, 0, 2*math.pi)
			elif shape == 'Up Triangle':
				r = size/2
				a = r * Decimal(math.sin(0.333 * math.pi))	# r sin(60deg)
				b = r * Decimal(math.cos(0.333 * math.pi))	# r cos(60deg)
				# remember +y is down
				ctx.set_line_width(0)
				ctx.move_to(x-a,y+b)	# left point
				ctx.line_to(x,y-r)		# bottom point
				ctx.line_to(x+a,y+b)	# right point
				ctx.line_to(x-a,y+b)	# back to left point
				ctx.fill()
			elif shape == 'Down Triangle':
				r = size/2
				a = r * Decimal(math.sin(0.333 * math.pi))	# r sin(60deg)
				b = r * Decimal(math.cos(0.333 * math.pi))	# r cos(60deg)
				# remember +y is down
				ctx.set_line_width(0)
				ctx.move_to(x-a,y-b)	# left point
				ctx.line_to(x,y+r)		# bottom point
				ctx.line_to(x+a,y-b)	# right point
				ctx.line_to(x-a,y-b)	# back to left point
				ctx.fill()
			else:	# shape == 'Square' and anything else
				pointX = x-(size/2)
				pointY = y-(size/2)
				ctx.rectangle(pointX, pointY, size, size)
			ctx.fill()
	def drawTrace(self, points, ctx):
		lineWidth = self.option('TRACE_LINE_WIDTH')
		ctx.set_line_width(lineWidth)
		r,g,b,a = self.option('TRACE_COLOR').get_rgba_tuple()
		ctx.set_source_rgba(r,g,b,a)
		for i in range(0,len(points['xCoords'])):
			x = points['xCoords'][i]
			y = points['yCoords'][i]
			# move_to the first point
			if i == 0:
				ctx.move_to(x,y)
			else:
				ctx.line_to(x,y)
		ctx.stroke()
	def drawAxisLabels(self, rectangle, points, ctx):
		labelSpacing = Decimal('5')
		fontSize = 7.5	# in points
		size = int(fontSize * 1024)	# b/c pango wants 1/1024ths of a point, weird
		color = self.option('AXIS_LABEL_COLOR').to_hex_no_alpha()
		layout = PangoCairo.create_layout(ctx)
		
		# this is the same structure that generates the tick marks and grids
		# here we use it to find the label positions
		divisions = self.option('DIVISIONS')
		ySpacing = rectangle.height/divisions
		xSpacing = rectangle.width/divisions
		width = rectangle.width
		height = rectangle.height
		
		scale = self.getScaleForPoints(points)
		xStep = Decimal(scale['xRange'] / divisions)
		yStep = Decimal(scale['yRange'] / divisions)
		# left axis labels
		py = rectangle.y
		px = rectangle.x
		for i in range(0, divisions+1):	# labels at start and end
			coord = scale['yMax'] - (i * yStep)		# data scale has maxY at the top, not the bottom
			labelText = self.getFormattedAxisLabel(coord)
			layout.set_markup(
				"<span "
				+ "font_family='monospace' "
				+ "size='" + str(size) + "' "
				+ "foreground='" + color + "'>"
				+ labelText
				+ "</span>"
			)
			labelWidth, labelHeight = layout.get_pixel_size()
			labelWidth = Decimal(labelWidth)
			labelHeight = Decimal(labelHeight)
			labelX = px - labelSpacing - labelWidth
			labelY = py - Decimal('0.5') * labelHeight
			ctx.move_to(labelX, labelY)
			PangoCairo.show_layout (ctx, layout)
			py += ySpacing
		# bottom axis labels
		# reset x,y
		py = rectangle.y + rectangle.height
		px = rectangle.x
		# rotate for vertical text
		for i in range(0, divisions+1):	# labels at start and end
			coord = scale['xMin'] + (i * xStep)
			labelText = self.getFormattedAxisLabel(coord)
			layout.set_markup(
				"<span "
				+ "font_family='monospace' "
				+ "size='" + str(size) + "' "
				+ "foreground='" + color + "'>"
				+ labelText
				+ "</span>"
			)
			labelWidth, labelHeight = layout.get_pixel_size()
			labelWidth = Decimal(labelWidth)
			labelHeight = Decimal(labelHeight)
			labelX = px - Decimal('0.5') * labelHeight
			labelY = py + labelSpacing + labelWidth
			ctx.move_to(labelX, labelY)
			ctx.save()
			ctx.rotate(-math.pi/2)	# angle in radians
			PangoCairo.update_layout(ctx, layout)
			PangoCairo.show_layout(ctx, layout)
			ctx.restore()
			PangoCairo.update_layout(ctx, layout)
			px += xSpacing
	def getScaleForPoints(self, points):
		# this bit is borrowed from the getReferencedPoints() method
		xMin = (min(points['xCoords']))
		xMax = (max(points['xCoords']))
		xRange = xMax - xMin
		yMin = (min(points['yCoords']))
		yMax = (max(points['yCoords']))
		yRange = yMax - yMin
		
		# It's possible that xRange or yRange are zero so we might need to set
		# them ourselves. We'll try to do so from any data points.
		if xRange == 0 and len(points['xCoords']) > 0:
			x = points['xCoords'][0]
			xMin = x - (x) * 2
			xMax = x + (x) * 2
			xRange = xMax - xMin
			if xRange == 0:
				# this might happen if x = 0
				xMin = -1
				xMax = +1
				xRange = xMax - xMin
		if yRange == 0 and len(points['yCoords']) > 0:
			y = points['yCoords'][0]
			yMin = y - (y) * 2
			yMax = y + (y) * 2
			yRange = yMax - yMin
			if yRange == 0:
				# this might happen if y = 0
				yMin = -1
				yMax = +1
				yRange = yMax - yMin
		
		scale = {
			'xMin' : xMin,
			'xMax' : xMax,
			'xRange' : xRange,
			'yMin' : yMin,
			'yMax' : yMax,
			'yRange' : yRange
		}
		return scale
	def getReferencedPoints(self, points, rectangle):
		scale = self.getScaleForPoints(points)
		
		# the coord lists are initialized with as many members as the data points lists
		# all set to zero at start
		referencedPoints = {
			'xCoords' : [0]*len(points['xCoords']),
			'yCoords' : [0]*len(points['yCoords'])
		}
		
		for i in range(0,len(points['xCoords'])):
			x = points['xCoords'][i]
			relativeCoord = (x - scale['xMin']) / (scale['xRange'])	# fraction 0 to 1 or xRange
			referencedCoord = rectangle.x + (relativeCoord * (rectangle.width))
			referencedPoints['xCoords'][i] = referencedCoord
		for i in range(0,len(points['yCoords'])):
			y = points['yCoords'][i]
			relativeCoord = 1 - (y - scale['yMin']) / (scale['yRange'])	# fraction 0 to 1 or yRange
			referencedCoord = rectangle.y + (relativeCoord * (rectangle.height))
			referencedPoints['yCoords'][i] = referencedCoord
		
		return referencedPoints
	def getFormattedAxisLabel(self, number):
		factor, order = expform(number)
		orderString = 'e{:+03}'.format(int(order))	# = 'e+24'
		factorString = '{: 2.2}'.format(float(factor))	# = ' 1.6'
		return factorString + orderString
	
	def onPanelButtonClick(self, button, *args):
		try:
			if not self.panelIsBusy:
				self.panelIsBusy = True
				if self.isConnected:
					# last two characters of button name represent key code for source meter
					name = Gtk.Buildable.get_name(button)
					command = ":system:key " + name[-2:]
					self.processLocalCommand(command)
					self.statusBar('Key ' + name[-2:] + ' pressed.')
					self.getSourceMeterCurrentDisplayState()
				else:
					self.statusBar('Not connected.')
				self.panelIsBusy = False
			else:
				self.statusBar('Please wait, panel is busy...')
		except Exception:
			self.error()
	def onNotebookTabsSwitchPage(self, notebook, page, pageNumber, *args):
		try:
			label = notebook.get_tab_label(page)
			tabText = label.get_text()
			# panel page has to be sensitive (a sweep isn't running) for display to update
			if tabText == 'Panel' and page.get_sensitive():
				self.updatePanelGraphics = True
				self.getSourceMeterCurrentDisplayState()
			else:
				self.updatePanelGraphics = False
			if tabText == 'Terminal':
				# we want to switch focus to the terminal command entry textbox
				# but we have to wait until after the focus switches to the notebook tab
				# so we'll use a delay
				def focusOnEntryCommand():
					self.get('entryCommand').grab_focus()
					return False	# kills the timeout so it doesn't keep repeating
				GLib.timeout_add(50,focusOnEntryCommand)	# delay in milliseconds
		except Exception:
			self.error()
	def getSourceMeterCurrentDisplayState(self, *args):
		if self.isConnected:
			if self.updatePanelGraphics:
				# read state of output light
				state = self.processSystemCommand(':output:state?')
				if state == '1':
					self.get('imageOutputLight').show()
				else:
					self.get('imageOutputLight').hide()
					
				#	top text
				state = self.processSystemCommand(':display:window1:text:state?')
				isUserMessageDisplayed = int(state)
				if isUserMessageDisplayed:
					text = self.processSystemCommand(':display:window1:text:data?')
				else:
					text = self.processSystemCommand(':display:window1:data?')
				window1Text = text[1:-1]	# strip off quotes
				reply = self.processSystemCommand(':display:window1:attributes?')
				mask = reply[1:-1]	#strip off quotes
				markup = self.getMarkupWithBlinkingCharactersHighlighted(window1Text, mask, FIXED_DISPLAY_OPTIONS['PANEL_NORMAL_COLOR'], FIXED_DISPLAY_OPTIONS['PANEL_BLINK_COLOR'])
				markup = markup.replace(chr(16), '&#181;')	# convert ASCII(16) to micro
				markup = markup.replace(chr(29), '&#62;')	# convert ASCII(29) to right arrows
				markup = markup.replace(chr(28), '&#60;')	# convert ASCII(29) to left arrows
				markup = markup.replace(chr(26), '&#8743;')	# convert ASCII(29) to up arrows
				markup = markup.replace(chr(27), '&#8744;')	# convert ASCII(29) to down arrows
				markup = markup.replace(chr(18), '&#8486;') # convert ASCII(18) to Ohm symbol
				label = self.get('labelSourceMeterWindow1')
				label.set_use_markup(True)
				label.set_markup(markup)

				#	bottom text
				state = self.processSystemCommand(':display:window2:text:state?')
				isUserMessageDisplayed = int(state)
				if isUserMessageDisplayed:
					text = self.processSystemCommand(':display:window2:text:data?')
				else:
					text = self.processSystemCommand(':display:window2:data?')
				window2Text = text[1:-1]	# strip off quotes
				reply = self.processSystemCommand(':display:window2:attributes?')
				mask = reply[1:-1]	#strip off quotes
				markup = self.getMarkupWithBlinkingCharactersHighlighted(window2Text, mask, FIXED_DISPLAY_OPTIONS['PANEL_NORMAL_COLOR'], FIXED_DISPLAY_OPTIONS['PANEL_BLINK_COLOR'])
				markup = markup.replace(chr(16), '&#181;')	# convert ASCII(16) to micro
				markup = markup.replace(chr(29), '&#62;')	# convert ASCII(29) to right arrows
				markup = markup.replace(chr(28), '&#60;')	# convert ASCII(29) to left arrows
				markup = markup.replace(chr(26), '&#8743;')	# convert ASCII(29) to up arrows
				markup = markup.replace(chr(27), '&#8744;')	# convert ASCII(29) to down arrows
				markup = markup.replace(chr(18), '&#8486;') # convert ASCII(18) to Ohm symbol
				label = self.get('labelSourceMeterWindow2')
				label.set_use_markup(True)
				label.set_markup(markup)
			return True
		else:
			return False
	def getMarkupWithBlinkingCharactersHighlighted(self, text, mask, gdkRGBANormal, gdkRGBAHighlight):
		# Some characters on the source meter display may be blinking.
		# Sending :display:window[n]:attributes? will return a mask of 
		# zeros and ones of the same length of the displayed text. A 
		# one indicates the character in that position is blinking.
		# Here we'll recreate 'text' using pango markup to highlight
		# the blinking characters.
		normalColorHex = gdkRGBANormal.to_hex_no_alpha()
		highlightColorHex = gdkRGBAHighlight.to_hex_no_alpha()
		highlightForecolorString = " foreground='"+highlightColorHex+"'"
		normalForecolorString = " foreground='"+normalColorHex+"'"
		
		lastBit = None
		markup = ''
		if len(mask) > 0:
			for i in range(0,len(mask)):
				bit = mask[i]
				if bit != lastBit and lastBit != None:
					# end previous block of characters
					markup += chunk + "</span>"
				if bit == '1' and lastBit != '1':
					# start highlighted text block
					chunk = "<span"+highlightForecolorString+">"
					chunk += text[i]
				elif bit == '1' and lastBit == '1':
					# continue building highlighted text block
					chunk += text[i]
				elif bit == '0' and lastBit != '0':
					# start normal text block
					chunk = "<span"+normalForecolorString+">"
					chunk += text[i]
				elif bit == '0' and lastBit == '0':
					# continue building normal text block
					chunk += text[i]
				lastBit = bit
			markup += chunk + "</span>"
		return markup
	def onButToggleOutputClick(self, *args):
		try:
			currentState = self.processLocalCommand(':output:state?')
			if currentState == '1':
				self.processLocalCommand(':output:state off')
			else:
				self.processLocalCommand(':output:state on')
			self.getSourceMeterCurrentDisplayState()
		except Exception:
			self.error()
	def onButToggleBeepModeClick(self, *args):
		try:
			currentState = self.processLocalCommand(':system:beep:state?')
			if currentState == '1':
				self.processLocalCommand(':system:beep:state off')
			else:
				self.processLocalCommand(':system:beep:state on')
			self.getSourceMeterCurrentDisplayState()
		except Exception:
			self.error()
	def onButRead2WireResistanceClick(self, *args):
		try:
			self.readResistance('2-wire')
		except Exception:
			self.error()
	def onButRead4WireResistanceClick(self, *args):
		try:
			self.readResistance('4-wire')
		except Exception:
			self.error()
	def readResistance(self, mode):
		self.processLocalCommand(':sense:function "resistance"')
		self.processLocalCommand(':sense:resistance:mode auto')
		range = self.option('SENSE_R_RANGE')
		if range == 'Auto':
			self.processLocalCommand(':sense:resistance:range:auto on')
		else:
			self.processLocalCommand(':sense:resistance:range:auto off')
			self.processLocalCommand(':sense:resistance:range '+range)
		self.processLocalCommand(':format:elements resistance')
		if mode == '2-wire':
			self.processLocalCommand(':system:rsense off')
		else:
			self.processLocalCommand(':system:rsense on')
		self.processLocalCommand(':trigger:count 1')
		self.processLocalCommand(':output:state on')
		reading = self.processLocalCommand(':read?')
		self.processLocalCommand(':output:state off')
		
		if mode == '2-wire':
			self.get('label2WireResistance').set_text(reading + ' &#8486;')
		else:
			self.get('label4WireResistance').set_text(reading + ' &#8486;')
	
	def onDisplayOptionsChange(self, *args):
		try:
			self.applyTerminalStyle()
			self.applyStatusBarStyles()
			self.applyGraphBackgroundColor()
			# rest of changes will be applied when graph is redrawn
			# this should happen when the user moves to the Run tab, but we'll
			# fire it anyway, just to be safe
			self.updateGraph()
		except Exception:
			self.error()
	def applyTerminalStyle(self):
		self.get('txtTerminal').override_background_color(Gtk.StateFlags.NORMAL, self.option('TERMINAL_BACKGROUND_COLOR'))
		self.get('txtTerminal').override_color(Gtk.StateFlags.NORMAL, self.option('LOCAL_COLOR'))
	def applyStatusBarStyles(self):
		self.get('statbarLocal').set_fColor(self.option('LOCAL_COLOR'))
		self.get('eventboxStatbarLocal').override_background_color(Gtk.StateFlags.NORMAL, self.option('TERMINAL_BACKGROUND_COLOR'))
		self.get('statbarReply').set_fColor(self.option('REPLY_COLOR'))
		self.get('eventboxStatbarReply').override_background_color(Gtk.StateFlags.NORMAL, self.option('TERMINAL_BACKGROUND_COLOR'))
		self.get('statbarError').set_fColor(self.option('ERROR_COLOR'))
		self.get('eventboxStatbarError').override_background_color(Gtk.StateFlags.NORMAL, self.option('TERMINAL_BACKGROUND_COLOR'))
	def applyGraphBackgroundColor(self):
		layout = self.get('layoutFigure')
		layout.override_background_color(Gtk.StateFlags.NORMAL, self.option('FIGURE_BACKGROUND_COLOR'))
	
	def onButConfigSaveAsClick(self, button, *args):
		try:
			dialog = self.getFilechooserConfigSave()
			dialog.set_current_name("pyK-Control 2400.config")
			response = dialog.run()
			dialog.hide()
			if response == 1:
				filename = dialog.get_filename()
				self.saveConfig(filename)
				self.statusBar("Configuration saved.")
		except Exception:
			self.error()
	def onButConfigSystemDefaultsClick(self, *args):
		try:
			# note, cwd is same as script, as set just before inititializing pyKControl object
			sysFile = 'pyK-Control 2400.config'
			self.saveConfig(sysFile)
			self.statusBar('Configuration saved.')
		except Exception:
			self.error()
	def onButConfigUserDefaultsClick(self, *args):
		try:
			userDir = os.path.expanduser('~')
			userFile = os.path.join(userDir,"pyK-Control 2400.config")
			self.saveConfig(userFile)
			self.statusBar('Configuration saved.')
		except Exception:
			self.error()
	def saveConfig(self, filename, forceAllOptions=False):
		self.message('Attempting to save config to ' + str(filename))
		file = open(filename, 'w')
		configParser = ConfigParser.ConfigParser()
		
		sectionsToSave = []
		if self.get("checkSaveSourceRange").get_active() or forceAllOptions:
			sectionsToSave.append('SOURCE_RANGE')
		if self.get("checkSaveRunOptions").get_active() or forceAllOptions:
			sectionsToSave.append('RUN_OPTIONS')
		if self.get("checkSaveConnectionOptions").get_active() or forceAllOptions:
			sectionsToSave.append('CONNECTION_OPTIONS')
		if self.get("checkSaveFileOptions").get_active() or forceAllOptions:
			sectionsToSave.append('FILE_OPTIONS')
		if self.get("checkSaveDisplayOptions").get_active() or forceAllOptions:
			sectionsToSave.append('DISPLAY_OPTIONS')
		if self.get("checkSaveTerminalOptions").get_active() or forceAllOptions:
			sectionsToSave.append('TERMINAL_OPTIONS')
			
		for controlID, definition in controlAndConfigDefinitions.iteritems():
			if definition['section'] in sectionsToSave:
				# some options will be set more than once since multiple controls
				# may refer to the same option, but the small amount of 
				# inefficiency won't be a problem
				self.setToConfig(definition['section'], definition['option'], configParser)
			
		configParser.write(file)
		file.close()
	def setToConfig(self, section, option, configParser):
		if not configParser.has_section(section):
			configParser.add_section(section)
		value = self.option(option, colorsAsStrings=True, noLineBreaksInText=True)
		configParser.set(section, option, str(value))
		
	def onComboSweepSpacingChange(self, combo, *args):
		if combo.get_active_text() == 'Linear':
			self.get('labelLogWarning').hide()
			self.get('comboSweepStepMode').set_sensitive(True)
		else:
			self.get('labelLogWarning').show()
			# only allow user to select Points (not step size)
			self.get('comboSweepStepMode').set_active(1)
			self.get('comboSweepStepMode').set_sensitive(False)
	def onComboSweepControlChange(self, combo, *args):
		if combo.get_active_text() == 'Manual':
			self.get('frameSweepManual').set_sensitive(True)
			self.get('frameSweepBuiltIn').set_sensitive(False)
			# in case these were disabled by onComboSweepSpecifyChange
			self.get('spinVStep').set_sensitive(True)
			self.get('spinVStepOrder').set_sensitive(True)
			self.get('spinIStep').set_sensitive(True)
			self.get('spinIStepOrder').set_sensitive(True)
		else:
			self.get('frameSweepManual').set_sensitive(False)
			self.get('frameSweepBuiltIn').set_sensitive(True)
	def onComboSweepStepModeChange(self, combo, *args):
		if combo.get_active_text() == 'Step Size':
			isStepActive = True
		else:	# number of points instead
			isStepActive = False
		self.get('spinSweepPoints').set_sensitive(not isStepActive)
		self.get('spinVStep').set_sensitive(isStepActive)
		self.get('spinVStepOrder').set_sensitive(isStepActive)
		self.get('spinIStep').set_sensitive(isStepActive)
		self.get('spinIStepOrder').set_sensitive(isStepActive)
	def onCheckSourceDelayAutoToggle(self, checkbutton, *args):
		isAutoOn = checkbutton.get_active()
		self.get('spinSourceDelay').set_sensitive(not isAutoOn)
	def onToggleLockToggle(self, toggle, *args):
		self.styleToggleButton(toggle)
		# since the only way to turn off the panel lock is to enter local
		# control, we have to force the 'local after run' option
		isLockEnabled = toggle.get_active()
		self.get('toggleLocal').set_active(isLockEnabled)
		self.get('toggleLocal').set_sensitive(not isLockEnabled)
	def styleToggleButton(self, toggle, *args):
		if toggle.get_active():
			toggle.get_children()[0].set_text('ON')
		else:
			toggle.get_children()[0].set_text('OFF')
		
	def onButImportConfigApplyClick(self, *args):
		try:
			self.statusBar("Loading configuration...")
			filename = self.get('filechooserbuttonImportConfig').get_filename()
			result = self.loadConfig(filename)
			if result == True:
				# manually trigger events that were withheld
				self.onConnectionConfigChange()
				self.onDisplayOptionsChange()
				self.statusBar("Configuration imported.")
			else:
				self.statusBar("Error importing configuration.")
		except Exception:
			self.error()
	def loadConfig(self, filename):
		self.message('Attempting to load config from '+str(filename))
		configParser = ConfigParser.ConfigParser()
		try:
			configParser.read(filename)
		except:
			return False
		self.readyForEvents = False		# don't want changes to trigger costly events
		# load config options into form controls
		for controlID in controlAndConfigDefinitions:
			self.setFromConfig(controlID, configParser)
		self.readyForEvents = True
		return True
	def setFromConfig(self, controlID, configParser):
		# look up section and option for controlID in dictionary
		control = self.get(controlID)
		configDefinition = controlAndConfigDefinitions[controlID]
		section = configDefinition['section']
		option = configDefinition['option']
		# determine if section exists in config
		# determine if option exists in config
		if configParser.has_section(section) and configParser.has_option(section, option):
			# determine type of control
			# based on type, set control value
			value = configParser.get(section, option)
			t = type(control)
			if t is Gtk.ToggleButton:
				control.set_active(boolstr(value))
			elif t is Gtk.ComboBoxText:
				control.select(value, False)
			elif t is Gtk.CheckButton:
				# two ways to set checkboxes. Option might be a list of values
				# or a boolean of the checkbox state
				if 'checkListValue' in configDefinition:
					control.set_active(configDefinition['checkListValue'] in value)
				else:
					control.set_active(boolstr(value))
			elif t is Gtk.SpinButton:
				# this could be a simple value or maybe the spin only gets 
				# part of the number (factor or exponent)
				if 'numberPart' in configDefinition:
					numberAsParts = {}
					numberAsParts['factor'], numberAsParts['order'] = expform(value)
					control.set_value(numberAsParts[configDefinition['numberPart']])
				else:
					control.set_value(Decimal(value))
			elif t is Gtk.ColorButton:
				control.set_rgba_string(value)
			elif t is Gtk.TextView:
				# new lines are indicated by <br/>
				control.set_text(value.replace('<br/>','\n'))
		else:
			# do nothing with this control, current value remains
			pass
				
	def onMenuItemViewChildActivate(self, childWidget, *args):
		# this single function was written to work for all of the selections
		# under the View menu. It just needs to switch the interface to the selected tab.
		# In glade the menuitems are setup to pass a childWidget that is located
		# on the corresponding notebook page. We use that widget to identify 
		# and select the right page.
		notebook = self.get('notebookTabs')
		pageNum = notebook.page_num(childWidget)
		notebook.set_current_page(pageNum)
	def onMenuitemFileImportConfigActivate(self, *args):
		try:
			filechooser = self.getFilechooserConfigLoad()
			response = filechooser.run()
			filechooser.hide()
			if response == 1:
				filename = filechooser.get_filename()
				self.loadConfig(filename)
				self.statusBar("Configuration imported.")
		except Exception:
			self.error()
	def onMenuitemEditCopySelectedDataActivate(self, *args):
		self.onButCopySelectedDataClick()
	def onMenuitemEditCopyAllDataActivate(self, *args):
		self.onButCopyAllDataClick()
	def onMenuItemHelpAboutActivate(self, *args):
		self.get('aboutdialog').show()
	def onAboutdialogClose(self, *args):
		self.get('aboutdialog').hide()
	def onAboutdialogDelete(self, *args):
		# we don't want to destroy the dilog if the user "x's it out"
		# we just want to hide it so we can show it again later
		self.get('aboutdialog').hide()
		# the return True prevents the normal event from occurring (the delete)
		return True
	
	def onButMaxWaitTimeClick(self, *args):
		self.get('entryMaxWaitTime').set_text('')
	def onButResponseTimeProfileClearClick(self, *args):
		self.get('textviewResponseTimeProfile').clear()
	def onButStatusBarLogClearClick(self, *args):
		self.get('textviewStatusBarLog').clear()
	def onButUnhandledExceptionsClearClick(self, *args):
		self.get('textviewUnhandledExceptions').clear()
		self.get('imageErrorIndicator').hide()
	def onButMessagesClearClick(self, *args):
		self.get('textviewMessages').clear()
		self.warn(False)
	def onImageWarningIndicatorClick(self, *args):
		self.warn(False)
	def onImageErrorIndicatorClick(self, *args):
		self.error(False)
		
if __name__ == '__main__':
	# we need to be able to find associated files so we need to be in 
	# the same directory as this script
	appDir = os.path.dirname(os.path.realpath(__file__))
	os.chdir(appDir)
	# start application
	app = pyKControl()
	Gtk.main()
