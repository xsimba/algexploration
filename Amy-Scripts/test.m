% data_Amybreathe1_Empatica = import_Empatica_folder;
% data_Amybreathe1b_Empatica = import_Empatica_folder;
% data_Amybreathe2_Empatica = import_Empatica_folder;


list = who('data*');
for i = 1:length(list)
    eval(['data = ',list{i},';']);
    if isfield(data,'physiosignal')
        if isfield(data.physiosignal.gsr,'gain')
        fprintf(list{i});
        fprintf('\n');
        end
    end
    
end




% %% Filtering
% GSR_raw = data_Sami.physiosignal.gsr.phasic.signal;
% GSR_raw_Emp =data_Empatica.GSR.GSR_data;
% if filt
%     [b,a]=butter(5,1/(32/2)); % butterworth filter: order = 5, freq = 32Hz for Sami GSR
%     GSR_filt = filtfilt(b,a,GSR_raw); % filtered GSR data
%     
%     
%     [b,a]=butter(5,1.5/(32/2)); % butterworth filter: order = 5, freq = 32Hz for Sami GSR
%     GSR_filt2 = filtfilt(b,a,GSR_raw); % filtered GSR data
%     
%     [b,a]=butter(5,1/(4/2));
%     GSR_Emp_filt = filtfilt(b,a,GSR_raw_Emp);
%     
%     [b,a]=butter(5,1.5/(4/2));
%     GSR_Emp_filt2 = filtfilt(b,a,GSR_raw_Emp);
%     % data filtered with butterworth filter and zero-phase filter
%     % lowpass filter that preserves timestamps of features
%     % cutoff = freq/(samplerate/2)      sr = 32Hz
%     clear b a
% else
%     GSR_filt = GSR_raw;
% end
% 
% % Plot filtered data
% figure(4); 
% clf;
% ha(1) = subplot(3,1,1);plot(time_Empatica,data_Empatica.GSR.GSR_data); hold on
% plot(time_Empatica,GSR_Emp_filt,'r','LineWidth',1);
% plot(time_Empatica,GSR_Emp_filt2,'g','LineWidth',1);
% title('Empatica -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (microSiemens)');
% 
% ylim;
% for j = 1:length(time_tags)
%    subplot(3,1,1);line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
% end
% % for n = 1:length(time_tags_orig)
% %    subplot(2,1,1),line([time_tags_orig(n),time_tags_orig(n)], ylim,'Color','g'); 
% % end
% 
% ha(2) = subplot(3,1,2);plot(time_Sami,data_Sami.physiosignal.gsr.phasic.signal); hold on
% plot(time_Sami,GSR_filt,'r','LineWidth',1);
% plot(time_Sami,GSR_filt2,'g','LineWidth',1);
% title('Hermes -- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (ADC units)');
% legend('Raw','Smoothed');
% 
% ylim;
% for j = 1:length(time_tags)
%    subplot(3,1,2),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
% end
% % for n = 1:length(time_tags_orig)
% %    subplot(2,1,2),line([time_tags_orig(n),time_tags_orig(n)], ylim,'Color','g'); 
% % end
% 
% 
% % Normalize datasets
% Empatica_norm = (data_Empatica.GSR.GSR_data - min(data_Empatica.GSR.GSR_data))/(max(data_Empatica.GSR.GSR_data)-min(data_Empatica.GSR.GSR_data));
% GSR_filt_norm= (GSR_filt - min(GSR_filt))/(max(GSR_filt)-min(GSR_filt));
% 
% ha(3) = subplot(3,1,3);plot(time_Empatica,Empatica_norm, time_Sami,GSR_filt_norm);
% title('Normalized Hermes + Empatica-- GSR');
% xlabel('Time (minutes)');
% ylabel('GSR (ADC units)');
% legend('Empatica','Hermes');
% 
% ylim;
% for j = 1:length(time_tags)
%    subplot(3,1,3),line([time_tags(j),time_tags(j)], ylim,'Color','r'); 
% end
% 
% axis([0 max(time_Empatica(end),time_Sami(end)) -inf inf]);
% linkaxes(ha, 'x'); 
% 
% 
% 
% 
% 
% 
% 
% NFFT = 2^nextpow2(length(GSR_raw)); % Next power of 2 from length of y
% GSR_raw_freq = fft(GSR_raw,NFFT)/length(GSR_raw);
% f = 32/2*linspace(0,1,NFFT/2+1);
% 
% figure(5) 
% clf
% % Plot single-sided amplitude spectrum.
% plot(f,2*abs(GSR_raw_freq(1:NFFT/2+1))) 
% title('Single-Sided Amplitude Spectrum of y(t)')
% xlabel('Frequency (Hz)')
% ylabel('|Y(f)|')
% hold on;
% 
% NFFT_filt = 2^nextpow2(length(GSR_filt)); % Next power of 2 from length of y
% GSR_filt_freq = fft(GSR_filt,NFFT_filt)/length(GSR_filt);
% f_filt = 32/2*linspace(0,1,NFFT_filt/2+1);
% plot(f_filt,2*abs(GSR_filt_freq(1:NFFT_filt/2+1))) 
% 
% NFFT_Emp = 2^nextpow2(length(GSR_raw_Emp)); % Next power of 2 from length of y
% GSR_raw_Emp_freq = fft(GSR_raw_Emp,NFFT_Emp)/length(GSR_raw_Emp);
% f_Emp = 32/2*linspace(0,1,NFFT_Emp/2+1);
% plot(f_Emp,2*abs(GSR_raw_Emp_freq(1:NFFT_Emp/2+1))) 