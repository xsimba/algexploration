function [events_ind, nsf_amp, risetime, half_time]=GSR_analysis(curr_dataset,varname,data_dir,rise1,rise2,filtyes)

% clearvars -except data*  time* Sami_mod;
%% 
% Written by Amy Liao on 7/14/15
% Modified from Combination of Anslab, Ledalab, and custom code
% Conducts basic analysis of GSR
% curr_dataset = dataset in structure format from Sami import using
% getPortal Snapshot or an array, must be sampled at 4 Hz (same as
% Empatica)
% varname = name of dataset
% data_dir = working directory where analysis will be stored
% rise1= Minimal increase for qualifying point (QP) [0.004]
% rise2= Minimal increase from QP to be regarded as NSF
% filtyes= Apply low pass filter



% % %% run this section if want to run as a script instead
% curr_dataset = Sami_mod;
% % curr_dataset = data_Amy_music; %dataset in structure format from Sami import using getPortal Snapshot
% varname = 'test_GSR'; %name of dataset
% data_dir = 'C:\Users\amy.liao\Documents\GSRData\scripts\Analysis'; % working directory where analysis will be stored
% rise1=30;%0.000;  %(0.000) Minimal increase for qualifying point (QP) [0.004]
% rise2=100;%0.1;    % (0.02) Minimal increase from QP to be regarded as NSF
% filtyes=0;    %    (1) Apply low pass filter


close(figure(1),figure(2),figure(3),figure(4),figure(5));
%% Load data

if isstruct(curr_dataset) % data is in v5 structure as pulled from RC console
    GSR_raw = curr_dataset.physiosignal.gsr.phasic.signal; %saves the GSR data into GSR_raw
    time = plot_Sami(curr_dataset);
else % data is a vector array
    GSR_raw = curr_dataset;
    time = 1:length(GSR_raw);
end
% acc = curr_dataset.physiosignal.gsr.phasic.signal; % saves accelerometer data into acc

savedir = ['\',date,'-GSR_analysis'];
mkdir(data_dir,savedir); warning off
red_dir=[data_dir,savedir]; 
cd(red_dir)  % cd to data_dir

%% Set threshold parameters

forward1=1;   %    (1) Window for search for QP [sec]
forward2=3.5; %  (3.5) window from QP for search for NSF [sec]
startdel=0;   %    (0) Set first startdel sec to the following value (if there is startup drift on amp)
maxwin=12;    %   (12) Window [sec] for search of valid local max after onset (=> maximal rise time)
              %        maxwin=0 switches off local maxima find, only looks for general maxima between onsets
maxblen=60;   %   (60) Maximum NSF interval length, if longer: insert NaN value (0 for NSF-rate) after maxblen/2

%%
% [SC,SR] = AskResampleData(SC,25,'EDA');  % Anslab script to Resample data to 25 Hz and save in SC, SR = orignial sampling rate
% SC = resample(SC,25,32); % Resamples Sami data (32 Hz) to 25 Hz

% len=length(SC)/25;
% len = length(GSR_raw)/32;
%% *** Initialization
samplerate=4;        % samplerate
ep=4;         % epoch size [1/ep sec]

%% Low-pass filter

if filtyes
    [b,a]=butter(5,1/(samplerate/2)); % butterworth filter: order = 5, freq = 25 Hz
    GSR_filt = filtfilt(b,a,GSR_raw); % filtered GSR data
    % data filtered with butterworth filter and zero-phase filter
    % lowpass filter that preserves timestamps of features
    % cutoff = freq/(samplerate/2)      sr = 32Hz
    clear b a
else
    GSR_filt = GSR_raw;
end
%% figure 1. Plot raw and filtered data
figure(1)
% time = plot_Sami(curr_dataset);
plot(time,GSR_raw);
hold on; plot(time, GSR_filt);
legend('Raw','Smoothed');

if exist('acc')
    figure(2)
    plot(time, acc);
end

%% Find GSR events

GSR_events; % finds GSR events using thresholds
plot_GSR_events; % plots GSR events on plot
GSR_stats;  %Determine amplitudes, rise times, and half recovery-times of nsfs
plot_GSR_stats;
% GSR_histo; % still buggy
% plot_GSR_histo; % still buggy
%%

% %*** Save reduced data
% chd(red_dir)
% S=[scl0 nsfr0 nsfv0 rtim0 hrec0];
% disp(['save ',varname,'s.txt -ascii -tabs S']);
% eval(['save ',varname,'s.txt -ascii -tabs S']);
% 
% disp('Results: mean SCL, mean NSF rate, mean SCR amplitude, mean rise time, mean half-recovery time: ');
% disp(nanmean(scl0));
% disp(nanmean(nsfr0));
% disp(nanmean(nsfv0));
% disp(nanmean(rtim0));
% disp(nanmean(hrec0));