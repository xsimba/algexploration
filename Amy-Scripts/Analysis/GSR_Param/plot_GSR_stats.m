        figure(4)
        clf     % clear figure
        if length(events_ind)

            subplot(1,2,1)
            plot(risetime,nsf_amp,'.')
            axis('square');
            title('Steepness of slopes of NSFs')
            ylabel('Amplitude [microS]')
            xlabel('Rise time [sec]')
%             x1=risetime; x2=nsf_amp;
            %n=find(isnan(x1)|isnan(x2));
            %x1(n)=[]; x2(n)=[];
            hold on
            for i=1:length(risetime)
                text(risetime(i),nsf_amp(i),int2str(i));
            end
            hold off

            subplot(1,2,2)
            plot(risetime,half_time,'.')
            i=max([nanrem(risetime(:));nanrem(half_time(:))])*1.02;
            if isempty(i)
                i=1;
            end
            axis([0 i 0 i]);
            axis('square');
            title('Waveform characteristic of NSFs')
            ylabel('Half recovery-time')
            xlabel('Rise time [sec]')
%             x1=risetime; x2=half_time;
            %n=find(isnan(x1)|isnan(x2));
            %x1(n)=[]; x2(n)=[];
            hold on
            for i=1:length(risetime)
                text(risetime(i),half_time(i),int2str(i));
            end
            hold off
            drawnow
        end
    %*** if length(events_ind)
    
    
    figure(5)
    t=(1:length(GSR_filt))/samplerate; % time in seconds
subplot(4,1,1)
% plot(t,scl0)
plot(t,GSR_filt,events_ind/samplerate,events_amp,'x')
set(gca,'XTickLabel',[]);
title(strrep(varname,'_','-'))
ylabel('uS')
subplot(4,1,2)
plot(events_ind/samplerate,nsf_amp,'o')
set(gca,'XTickLabel',[]);
title('SCR amplitude')
ylabel('uS')
legend(['Avg = ',num2str(nanmean(nsf_amp))]);
subplot(4,1,3)
plot(events_ind/samplerate,risetime,'o')
set(gca,'XTickLabel',[]);
title('SCR rise time')
ylabel('sec')
legend(['Avg = ',num2str(nanmean(risetime))]);
subplot(4,1,4)
plot(events_ind/samplerate,half_time,'o')
title('SCR half-recovery time')
xlabel('time [sec]')
ylabel('sec')
legend(['Avg = ',num2str(nanmean(half_time))]);  
    
    
clear i t