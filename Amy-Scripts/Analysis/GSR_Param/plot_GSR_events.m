events_amp=GSR_filt(events_ind)';        

figure(3)
        clf     % clears current figure
        t=1:length(GSR_filt);
        t = t/60; % convert to minutes
        plot(t/samplerate,GSR_filt)
        axis([0 max(t/samplerate) min(GSR_filt) max(GSR_filt)])
        title(['Detected SCR onsets.']);
        xlabel('sec')
        ylabel('uS');
        if length(events_ind)
            hold on
            plot(events_ind/samplerate/60,events_amp,'xg'); % Marks detected NSF's with green x
            x1=events_ind/samplerate/60; x2=events_amp;
            x1 = x1(:);
            x2 = x2(:);

            n=find(isnan(x1)|isnan(x2));
            x1(n)=[]; x2(n)=[];

            for i=1:length(x1)
                text(x1(i),x2(i),int2str(i)); % labels events
            end
            hold off
        end

drawnow  % updates figures immediately

clear x1 x2 t i n 