%% *** find events_ind - point of inflection on the bottom side
slope=diff(GSR_filt);
events_ind=find(slope(1:end-1)<=0 & slope(2:end)>0); % slope <= 0  = downward or flat slope, slope1 > 0 = next point turns up
events_ind = events_ind(:);
events_ind=[1;events_ind]+1; % index of inflection points
events_ind = events_ind(:);
%% *** find qualifying point (>rise1 uS increase within forward1 seconds)

qf=NaN*ones(size(events_ind)); % index of qualifying points
qf2=NaN*ones(size(events_ind)); % index of NSF's

if ~isempty(events_ind)
    t2=events_ind+forward1*samplerate; % sets window end point for each point in events_ind (time*sr)
    n=find(t2>length(GSR_filt));
    t2(n)=length(GSR_filt)*ones(size(n)); % if t2>length, then use length
    for i=1:length(events_ind)
        n1 = GSR_filt(events_ind(i)+1:t2(i)); % searches in window starting from events_ind to 1 window greater than events_ind
        n=find( n1-GSR_filt(events_ind(i))  >= rise1 ); % checks if data rose above events_ind by threshold during window
        if ~isempty(n) 
            qf(i)=n(1)+events_ind(i); % if data increased in window, then it's a qualifying point
        else
            qf(i)=NaN;
        end
    end
    [qf,ind]=nanrem(qf);% remove NaN's, ind = index of NaNs, qf = qf with Nans removed
    if length(ind) % if there were Nan's, remove Nan's from events_ind and qf vectors
        events_ind(ind)=[];
        qf2(ind) =[];
    end

    %% *** find NSF's  (>rise2 uS increase within forward2 seconds from qf)
    if ~isempty(qf)
        t2=qf+forward2*samplerate;  % end of window 2
        n=find(t2>length(GSR_filt));
        t2(n)=length(GSR_filt)*ones(size(n)); % if t2>length, then use length
        for i=1:length(events_ind)
            n1= GSR_filt(events_ind(i)+1:t2(i));
            n=find( n1-GSR_filt(events_ind(i))  >= rise2 );  % criterion for NSF
            if ~isempty(n) 
                qf2(i)=n(1)+events_ind(i);
            else
                qf2(i)=NaN;
            end
        end
        [qf2,ind]=nanrem(qf2);
        if length(ind)
            events_ind(ind)=[];
            qf(ind)=[];
        end

        %*** exclude amplitudes that occur after the next baseline
        for i=1:(length(qf2)-1)
            if qf2(i)>=events_ind(i+1)
               qf2(i)=NaN;
            end
        end

        [qf2,ind]=nanrem(qf2); % remove NaN's, ind = index of NaNs, qf2 = qf2 with Nans removed
        if length(ind)   % if there were Nan's, remove Nan's from events_ind and qf vectors
            events_ind(ind)=[];
            qf(ind)=[];
        end
    end  % if qf
end  % if events_ind

clear qf qf2 i n n1 t2 ind %slope

