%*** Rename some variables
nsfn=NSF; 
nsft=events_ind; 
nsfv=nsf_amp; 
hrec=half_time;
rtim=risetime;

GSR_filt_10=decfast(ipfast(GSR_filt,ep),samplerate);
% format: meany=decfast(y,area);  Anslab function
% shrinks a vector with factor area [default =2].
% mean of succeeding points of length area (fast!)
% groups so that 1 epoch = 1/ep seconds


%*** Instananous NSF-> IFI (inter-fluctuation interval)
%disp('Instananous NSF');
len=length(GSR_filt_10);

if ~isempty(nsft)
    nsftd=difffit(nsft)/samplerate; % difference of nsf_time in seconds, Nan as last point

    % insert 0 values for NSF-rate if inter-NSF intervals too long
    % insert NaN for other parameters
    d=diff(nsft); % difference in nsf_time index
    i1=find(d>maxblen*samplerate);
    i1=[i1;length(nsft)]; % indexes of events that is > maxblen (max NSF interval length)
    nsft2=nsft;
    nsftd2=nsftd;
    nsfv2=nsfv;
    rtim2=rtim;
    hrec2=hrec;

    if ~isempty(nsft2)
        for i=1:length(i1)
            % Replaces Inf or Nan into array where NSF interval exceeds threshold
            nsft2 =insert(nsft2,(nsft2(i1(i))+maxblen/2*samplerate),i1(i)+1);
            nsftd2=insert(nsftd2,Inf,i1(i)+1);
            nsfv2 =insert(nsfv2,NaN,i1(i)+1);
            rtim2 =insert(rtim2,NaN,i1(i)+1);
            hrec2 =insert(hrec2,NaN,i1(i)+1);
            % Anslab function
            %function y1=insert(y,yt,t);
            % insert one value yt into vector y at point t
            i1=i1+1;
        end
    end

    nsfr0=epoch(nsft2,60 ./ nsftd2,(samplerate/ep));  % skin conductance fluctuation rate (fluct/min)
    nsfv0=epoch(nsft2,nsfv2,(samplerate/ep));      
    rtim0=epoch(nsft2,rtim2,(samplerate/ep));      
    hrec0=epoch(nsft2,hrec2,(samplerate/ep));
    % Anslab function
    % function x1 = epoch(x,y,points);
    % Convert time- and value vectors into instantanous real-time epochs of length points
    % x:	  eventtime-vector
    % y:	  according values
    % points: no. of samples within one epoch, if not integer: rounded and resampled at end (slow)

    nsfr0=cut_fill(nsfr0,len);
    nsfv0=cut_fill(nsfv0,len);
    rtim0=cut_fill(rtim0,len);
    hrec0=cut_fill(hrec0,len);
    % Anslab function
    % function [v1,cutyes]=cut_fill(v1,lv1);
    % cutting a vector at the end to a length of len (=> cutyes=1),
    % filling it with NaN if neccessary (=> cutyes=0)
    
else % if nsft is empty, fill with zeros or NaNs
    nsfr0=zeros(len,1);
    nsfv0=zeros(len,1);
    rtim0=NaN*ones(len,1);
    hrec0=NaN*ones(len,1);
end

GSR_filt_10=GSR_filt_10(:);
nsfr0=nsfr0(:);
hrec0=hrec0(:);
rtim0=rtim0(:);
nsfv0=nsfv0(:);

% Fill NaNs at end with last valid number 
l=length(risetime);
x=rtim0(len:-1:1);
n=find(~isnan(x));
if length(n)
    n=n(1);
    if n>1
       rtim0(len-n+2:len)=risetime(l)*ones(n-1,1);
       hrec0(len-n+2:len)=hrec(l)*ones(n-1,1);
       nsfv0(len-n+2:len)=nsfv(l)*ones(n-1,1);
    end
end

% fill start and end of nsfr0 with 0
n=find(~isnan(nsfr0));
if length(n)
    n=n(1);
    if n>1 
       nsfr0(1:n-1)=zeros(n-1,1);
    end
    x=nsfr0(len:-1:1);
    n=find(~isnan(x));
    n=n(1);
    if n>1 
       nsfr0(len-n+2:len)=zeros(n-1,1);
    end
else
    nsfr0=0*one(nsfr0);
end

scl0=decfast(ipfast(GSR_filt,ep),samplerate);
nsfr0=decfast(ipfast(nsfr0,5),2);
nsfv0=decfast(ipfast(nsfv0,5),2);
rtim0=decfast(ipfast(rtim0,5),2);
hrec0=decfast(ipfast(hrec0,5),2);
reslen=length(scl0);
scl0=nan_lip(cut_fill(scl0,reslen))';
nsfr0=nan_lip(cut_fill(nsfr0,reslen))';
nsfv0=nan_lip(cut_fill(nsfv0,reslen))';
rtim0=nan_lip(cut_fill(rtim0,reslen))';
hrec0=nan_lip(cut_fill(hrec0,reslen))';


clear x n i i1 l d