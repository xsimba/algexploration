    %*** Plot epoch data
    
%     t=(1:len)/ep;

%     figure(6)
%     clf  % Amy - replaced clg with clf
%     plot(t,[nsfv0])
%     axisx(0,max(t));
%     title('NSF Amplitude');
% 
%     figure(7)
%     clf  % Amy - replaced clg with clf
%     plot(t,[nsfr0 rtim0 hrec0])
%     axisx(0,max(t));
%     title('NSF rate (y) Rise Time (m), and Half Recovery Time (c)');

%     drawnow


if all(isnan(GSR_filt)) 
   nsfn=NaN; nsft=NaN; nsfv=NaN; 
   nsfr0=NaN; scl0=NaN; nsfv0=NaN; rtim0=NaN; hrec0=NaN;
   rtim=NaN; hrec=NaN  
end 


t=(1:length(scl0))/ep;
figure(6)
% figure('position',[252   153   943   700]);
subplot(5,1,1)
% plot(t,scl0)
plot(GSR_filt)
set(gca,'XTickLabel',[]);
title('SCL')
ylabel('uS')
subplot(5,1,2)
plot(t,nsfr0)
set(gca,'XTickLabel',[]);
title('NSF rate')
ylabel('1/min')
subplot(5,1,3)
plot(t,nsfv0,events_ind/samplerate,nsf_amp,'o')
set(gca,'XTickLabel',[]);
title('SCR amplitude')
ylabel('uS')
subplot(5,1,4)
plot(t,rtim0,events_ind/samplerate,risetime,'o')
set(gca,'XTickLabel',[]);
title('SCR rise time')
ylabel('sec')
subplot(5,1,5)
plot(t,hrec0,events_ind/samplerate,half_time,'o')
title('SCR half-recovery time')
xlabel('time [sec]')
ylabel('sec')