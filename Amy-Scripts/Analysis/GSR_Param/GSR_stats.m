    %*** Determine amplitudes, rise times, and half recovery-times of nsfs
    NSF = 60*length(events_ind)/(length(nanrem(GSR_filt))/samplerate); % # events per minute
    if length(events_ind) % if events detected
        nsf_amp=NaN*ones(size(events_ind)); % amplitude of each event (searches for max)
        risetime=NaN*ones(size(events_ind)); % index of nsf_amp
        half_time =NaN*ones(size(events_ind));
        events_ind(length(events_ind)+1)=length(GSR_filt); % add last point to events_ind
        
        for i=1:length(events_ind)-1
            x1=GSR_filt(round(events_ind(i)):round(events_ind(i+1)));  % x1 = GSR between 2 events, temp
            % needed for half_time
            
            if maxwin  % maxwin = max window to search for rise time
                n1=events_ind(i); % index of event 1
                n2=events_ind(i+1); % index of event 2
                if (n2-n1)>(maxwin*samplerate) % if time between events>max win, only search in maxwin
                    n2=n1+maxwin*samplerate;
                end
                x=GSR_filt(n1:n2); % x = GSR to search (between events or up to maxwin)
                [ind,m]=findmax(x,4,1,.00000001,3);  % local maxima
                % Anslab function
                % format: [mt,mv] = findmax(y,area,back,rise,mingap,forward,rise2);
                % Detection of local maxima (MT: maxtime, MV: maxvalue)
                
                if ~isempty(m) 
                    [nsf_amp(i),ind2]=max(m); % saves max value to nsf_amp     
                    risetime(i)=ind(ind2); % saves index from start of rise of max value to risetime
                end
            else
                x=x1;
                [nsf_amp(i),risetime(i)]=max(x);
            end
            
            nsf_amp(i)=nsf_amp(i)-GSR_filt(round(events_ind(i))); % calculate amplitude = base to peak
            
            if isnan(risetime(i))
                half_time(i)=NaN;
            else

                x1(1:risetime(i))=[]; % ignore rise portion of GSR event
                n=find(x1<=GSR_filt(round(events_ind(i)))+nsf_amp(i)/2); % find all index: base value +1/2 amp
                if ~isempty(n)
                    half_time(i)=n(1)+risetime(i); % index from start of rise where GSR_filt first drops back down to 1/2 of peak amp
                end 
            end
        end

        events_ind(length(events_ind))=[];

        half_time=(half_time-1)/samplerate; % convert from index to seconds
        risetime=(risetime-1)/samplerate; % convert from index  to seconds
        [nsf_amp,ind]=nanrem(nsf_amp);  % delete if NaNs leftover from maxwin
        if length(ind) % deletes values from local maximum (only keep global maximum)
            risetime(ind)=[];
            half_time(ind)=[];
            events_ind(ind)=[];
            events_amp(ind)=[];
        end
    end
    %*** if length(events_ind)
    
    clear i ind ind2 m n n1 n2 x x1