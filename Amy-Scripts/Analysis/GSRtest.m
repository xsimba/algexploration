%
% define butterworth filter characteristics
%
close all
GSRsig = Sami_mod_raw;

time_delay = 100;
Fs=128;
N     = 8;   % Order
Fstop = 3.5;    % Stopband Frequency
Astop = 60;   % Stopband Attenuation (dB)

%designfilt('bandpassiir', 'FilterOrder', 10, 'HalfPowerFrequency1', 30/60, 'HalfPowerFrequency2', 150/60, 'SampleRate', 128);

h = fdesign.lowpass('n,fst,ast', N, Fstop, Astop, Fs);
%Hd = design(h, 'ellip');
Hd = design(h, 'cheby2');
%fil_signal=filter(Hd,signal);
simlink_Hd = dfilt.df2sos(Hd.sosMatrix, Hd.ScaleValues);  % Bi-Quad Filter

%
% compute group delay
%


freq_compensate = 0:0.001:Fstop;
g = grpdelay(simlink_Hd, freq_compensate, Fs);
g1 = max(g) - g;

%
% design compensator
%
hgd = fdesign.arbgrpdelay('N,B,F,Gd',8,1,freq_compensate,g1,Fs);
Hgd = design(hgd, 'iirlpnorm', 'MaxPoleRadius', 0.93);
SimLinkPhaseComp = dfilt.df2sos(Hgd.sosMatrix, Hgd.ScaleValues);  
%%

temp_iir = filter(simlink_Hd,GSRsig);
GSR_iir  = filter(SimLinkPhaseComp,temp_iir);

dev_sig=(diff(GSR_iir));

locations = find(dev_sig(1:end-time_delay)<=dev_sig(time_delay:end-1) & dev_sig(2:end-time_delay+1)>dev_sig(time_delay+1:end));
locations = locations-93;


figure(1);hold on
plot(GSRsig)
plot(locations(locations>0),GSRsig(locations(locations>0)),'mo','MarkerSize', 6, 'LineWidth', 2)

figure(2)
plot(GSR_iir);hold on;
plot((1:length(GSRsig))+93,GSRsig,'r');hold on;
plot((1:8:8*length(Empatica_mod))+93,Empatica_mod*1000,'g');















