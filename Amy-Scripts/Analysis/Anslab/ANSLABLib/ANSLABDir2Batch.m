function [filemat]=ANSLABDir2Batch(filepath,subfolderstatus,filestatus,pathstatus,...
    filterstatus,filter,defaultfilter,searchpart,logictype,CaseSensitiveStatus)


%	'ANSLABDir2Batch(filter)' will find files that have    the search string as part of their names. You are
%	asked to supply a parent-directory that will be
%	searched. If 'filter' is a string containing '|', for
%	each found file a counterpart is searched in its
%	directory the filename of which can be obtained
%	by replacement of the first given string after the '|'
%	with the second given string after the '|' in the
%	filename of the former file.
%	A '\n' signifies the end of the filename.
%
%	example: dir2batch('at1|at1 at2')
%
%	directory('C:\test\') content:
%
%	01.baseline.at1.ar
%	01.baseline.at2.ar
%	01.ZEROS.txt
%	02.baseline.at1
%	02.baseline.at2
%	text.txt
%	data.dat
%
%	will find:
%
%	C:\test\01.baseline.at1.ar
%	C:\test\01.baseline.at2.ar
%	C:\test\02.baseline.at1
%	C:\test\02.baseline.at2
%
%	example: dir2batch('at1\n|at1 at2\n')
%	will find:
%
%	C:\test\02.baseline.at1
%	C:\test\02.baseline.at2
%
%	'dir2batch' alone lets you choose all the options,
%	including the creation of a batch-text-file.
%
%	'[filemat]=dir2batch(filepath,subfolderstatus,...
%	batchfilestatus,pathstatus,filterstatus,filter,defaultfilter)'
%	serves as the functional form of dir2batch.
%	'filepath' is the parent-directory to be searched.
%	'subfolderstatus'  (1/0) determines wether subfolders
%	will be searched, 'batchfilestatus' ('y'/'n') determines
%	creation of a batchfile, 'pathstatus' ('y'/'n') decides
%	printing of full paths vs. filnames only, 'filterstatus'
%	('y'/'n') decides wether a filter-string is used (if 'n' all
%	files in directory are chosen), 'defaultfilter' sets a new
%	defaultfilter.


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



filter_ex = [];

if nargin<9;logictype=1;end
if nargin<8;searchpart=1;end
if nargin<7;defaultfilter=[];end
if nargin < 6; filter=[];end
if nargin< 5; filterstatus = [];end
if nargin<4;pathstatus = [];end
if nargin<3;filestatus=[];end
if nargin<2;subfolderstatus=[];end
if nargin<1;filepath=[];functionstatus = 0; filter_ex = [];endofnamestatus =0;endofnameexstatus =0;else functionstatus = 1;end
versionstring = version;
matlabnr = str2num(versionstring(1:3));


if nargin == 1
    defaultfilter = filepath;
    filter = defaultfilter;
    functionstatus = 0;
    pathstatus = 'y';
    filterstatus = 'y';
    subfolderstatus = 1;
    filestatus = 'n';
    if ~isempty(findstr(defaultfilter,'|'))
        filter_ex = defaultfilter(findstr(defaultfilter,'|')+1:end);
        filter = defaultfilter(1:strfind(defaultfilter,'|')-1);
    else
        filter_ex = [];
    end
    if ~isempty(findstr(filter,'\n'))
        filter(findstr(filter,'\n'):end)=[];
        endofnamestatus = 1;
    else
        endofnamestatus = 0;
    end
    if ~isempty(findstr(filter_ex,'\n'))
        filter_ex(findstr(filter_ex,'\n'):end)=[];
        endofnameexstatus = 1;
    else
        endofnameexstatus = 0;
    end
end

filenamecell=cell(1);
filepathcell=cell(1);

if functionstatus == 0
    if ~isempty(which('ANSLABDir2Batchutil.mat'))
        load('ANSLABDir2Batchutil.mat','defaultfilter');
    end
    if ~isempty(which('dir2batchutil.mat')) & isempty(filepath)
        load('ANSLABDir2Batchutil.mat','filepath');
    end
end

if ~isempty(filepath) & exist(filepath)==7
    rootpath=pwd;
    cd(filepath);
else
    rootpath=pwd;
end

if isempty(defaultfilter)
    defaultfilter = '.JPG';
end

if isempty(pathstatus)
    pathstatus=input('Do you wish to print the full filepaths (instead of \nonly the filenames)? (y/n) (default = y)','s');
end

if isempty(filterstatus)
    filterstatus=input('Do you wish to use a string to search for in the filenames? (y/n) (default = y)','s');
end

if isempty(filter) & ~functionstatus
    if strcmp(filterstatus,'y') | isequal(filterstatus,1) | isempty(filterstatus)

        fprintf(1,'%c',['Do you wish to use the default-search-string: >> ',defaultfilter,' <<  (y/n) (default = y)']);
        defaultfilterstatus = input('?','s');
        if strcmp(defaultfilterstatus,'y') | isempty(defaultfilterstatus)
            filter = defaultfilter;
            if ~isempty(findstr(filter,'\n'))
                filter(findstr(filter,'\n'):end)=[];
                endofnamestatus = 1;
            else
                endofnamestatus = 0;
            end
            if ~isempty(findstr(filter_ex,'\n'))
                filter_ex(findstr(filter_ex,'\n'):end)=[];
                endofnameexstatus = 1;
            else
                endofnameexstatus = 0;
            end

        else
            fprintf(1,'Please enter the search-string:\n');
            fprintf(1,'(Note that only the filenames, not the entire paths, are checked for matches. Use a\n');
            defaultfilter = input(' ''|'' seperated string for pair-search (type help for info!)): \n','s');
            filter=defaultfilter;

            if ~isempty(findstr(filter,'|'))
                filter = filter(1:findstr(filter,'|')-1);
                filter_ex = defaultfilter(findstr(defaultfilter,'|')+1:end);
            else
                filter_ex = [];
            end

            if ~isempty(findstr(filter,'\n'))
                filter(findstr(filter,'\n'):end)=[];
                endofnamestatus = 1;
            else
                endofnamestatus = 0;
            end
            if ~isempty(findstr(filter_ex,'\n'))
                filter_ex(findstr(filter_ex,'\n'):end)=[];
                endofnameexstatus = 1;
            else
                endofnameexstatus = 0;
            end
        end
    else
        filter = [];
        endofnamestatus = 0;
        endofnameexstatus = 0;
    end
else
    if strcmp(filterstatus,'y') | isequal(filterstatus,1) | isempty(filterstatus)
        if ~isempty(findstr(filter,'\n'))
            filter(findstr(filter,'\n'):end)=[];
            endofnamestatus = 1;
        else
            endofnamestatus = 0;
        end
        if ~isempty(findstr(filter_ex,'\n'))
            filter_ex(findstr(filter_ex,'\n'):end)=[];
            endofnameexstatus = 1;
        else
            endofnameexstatus = 0;
        end
    elseif strcmp(filterstatus,'n')
        filter = [];
        endofnamestatus = 0;
        endofnameexstatus = 0;
    end
end

if isempty(subfolderstatus)
	subfolder = input('Do you wish to search in subfolders? (y/n) (default = y)','s');
	if strcmp(subfolder,'y') | isempty(subfolder)
        subfolderstatus = 1;
	else
        subfolderstatus = 0;
	end
end

if isempty(filestatus)
    filestatus=input('Paths will be printed in the command window. Do you additionnally wish\n to create a batchfile? (y/n) (default = n)','s');
end

if functionstatus == 0
    if matlabnr>6.1
        startpath = ANSLABDefPath(1);
        startpath = startpath(1:max(findstr(startpath,filesep)));
        [filepath]=uigetdir([startpath],'Please choose the start folder:');
        ANSLABDefPath(2,[filepath,filesep]);
    else
        startpath = SetDefPath(1);
        [filename,filepath]=uiputfile([startpath,filesep,'*.*'],'Please create a file in the start folder:');
        if ~isequal(filepath,0)
            SetDefPath(2,filepath);
        end
    end
end

if isequal(filepath,0)
    disp('Invalid path....');
    return
else
    dir2batchpath=which('ANSLABDir2Batch.m');
    filesepvec=findstr(dir2batchpath,filesep);
    dir2batchutilpath=[dir2batchpath(1:filesepvec(end)),'dir2batchutil.mat'];
    path=filepath;
    try
        save(dir2batchutilpath,'filepath','defaultfilter');
    catch
        uiwait(errordlg('unable to write settingsfile...\nremove write protection??'));
    end
    [filenamecell,filepathcell]=ANSLABLookInDir(filepath,filter,filenamecell,filepathcell,...
        subfolderstatus,filter_ex,endofnamestatus,endofnameexstatus,searchpart,logictype,CaseSensitiveStatus);
end

if isempty(filenamecell{1}) & ~functionstatus
    uiwait(errordlg('No files found!','Warning:'));
    filemat=[];
    return
elseif isempty(filenamecell{1}) & functionstatus
    filemat=[];
    return
end

%file schreiben
%-----------------
if strcmp(filestatus,'y') | isequal(filestatus,1)
[targetfilename,targetfilepath]=uiputfile('*.m','Please enter the batchfilename:');
if ~isequal(targetfilename,0) &  ~isequal(targetfilepath,0)

	if isempty(findstr(targetfilename,'.'))
        targetfilename=[targetfilename,'.m'];
	end
	targetfile=[targetfilepath,targetfilename];
	fid = fopen(targetfile,'wt');
	if ~strcmp(pathstatus,'n')
        fprintf(fid,'%s',char([filepathcell{1},filenamecell{1}]));
	else
        fprintf(fid,'%s',char(filenamecell{1}));
	end

	for i=2:size(filenamecell,2)
        if ~strcmp(pathstatus,'n')
            fprintf(fid,'\n');
            fprintf(fid,'%s',char([filepathcell{i},filenamecell{i}]));
        else
            fprintf(fid,'\n');
            fprintf(fid,'%s',char(filenamecell{i}));
        end
	end
	fclose(fid);
	end
end


if functionstatus == 0
	%ausgabe im cw
	%-----------------
	fprintf('\n\n\n');

	fprintf([num2str(length(filenamecell)),' file(s) found! \n']);
	fprintf('\n');
	if ~strcmp(pathstatus,'n')
        fprintf(1,'%s',char([filepathcell{1},filenamecell{1}]));
	else
        fprintf(1,'%s',char(filenamecell{1}));
	end

	for i=2:size(filenamecell,2)
        if ~strcmp(pathstatus,'n')
            fprintf(1,'\n');
            fprintf(1,'%s',char([filepathcell{i},filenamecell{i}]));
        else
            fprintf(1,'\n');
            fprintf(1,'%s',char(filenamecell{i}));
        end
	end
    filemat=[];
else
    filemat=cell(1);
	if pathstatus
        filemat{1}=[filepathcell{1},filenamecell{1}];
		for i=2:size(filenamecell,2)
            filemat{i}=[filepathcell{i},filenamecell{i}];
		end
    else
        filemat{1}=[filenamecell{1}];
        for i=2:size(filenamecell,2)
            filemat{i}=[filenamecell{i}];
		end
    end
end

cd(rootpath);
