% kevintl.m   goto left event interval: <


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


j=length(evscan);
if j  evscani=evscani-skipfact;
  if evscani<1 evscani=1; %plotyes=0;
  else
   ii=evscan(evscani+1)-evscan(evscani);
   s1=evscan(evscani)-ii*skiploc; s2=evscan(evscani+1)+ii*skiploc;
   evnumyes=1;
  end;
else plotyes=0; end;
