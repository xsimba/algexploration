function [mt,mv] = findmax2(y,area,back1,back2,rise,mingap,forward);
% function [mt,mv]=findmax2(y,area,back1,back2,rise,mingap,forward);
% Detection of local maxima (MT: maxtime, MV: maxvalue).
% AREA: Size of the window that is stepped through the data.
% Within each window the biggest value is the local max.
% If 2 values are equal the first one is chosen.
% E.g., AREA=116 detects succeeding R-peak maxima in the EKG
% at a heart rate of 200 bpm (if the samplerate=384).
% A slope BACK1 points before max to max has to be greater than RISE.
% (The same for FORWARD is optional.)
% MINGAP:  Minmimal distance between two max. The bigger one is chosen.
% [Default=AREA]

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<6 mingap=area; end;
if nargin==2 back1=1; rise=0; end;

%*** Partition y into windows
len=length(y);    % fill y with 0 to a length of multiples of area
zer=area-rem(len,area);
if rem(len,area)==0 zer=0; end;
y(len+1:len+zer)=zeros(zer,1);

Y=reshape(y,area,(len+zer)/area);
[mv,mt]=max(Y);

%*** 'Reshape' maxtime-vector
mt=mt+(0:length(mt)-1)*area;
lenm=length(mt);
if mt(lenm)>len mv(lenm)=[]; mt(lenm)=[]; end;

%*** Choose bigger one of two close max
% find indices of max that are too close, n: indices of smaller values
n=maxdist(mt,mv,mingap);
mt(n)=[];
mv(n)=[];

if nargin>2

%*** Maxima may be not valid because of too short rise-time
while 1
if ((mt(1)<=back1) | (mt(1)<=back2)) mt(1)=[]; mv(1)=[];
else break; break;
end;
end;

i=length(mt);
if nargin==7
if ((mt(i)>=len-back1) | (mt(i)>=len-back2)) mt(i)=[]; mv(i)=[]; end;
else
if (mt(i)>=len-1) mt(i)=[]; mv(i)=[]; end;
end;

if length(mv)
%*** Check for general slope backward
slope=mv-y(mt-1)';
n=slope>=0;
mt=mt(n);
mv=mv(n);
end;

if length(mv)
%*** Check for general slope forward
slope=mv-y(mt+1)';
n=slope>=0;
mt=mt(n);
mv=mv(n);
end;

if length(mv)
%*** Check for valid slope backward
slope1=mv-y(mt-back1)';
slope2=mv-y(mt-back2)';
n= (slope1>rise) | (slope2>rise);
mt=mt(n);
mv=mv(n);
end;

if length(mv)
if nargin==7
%*** Check for valid slope forward
slope=mv-y(mt+forward)';
n=slope>rise;
mt=mt(n);
mv=mv(n);
end;
end;

end;

mt=mt';
mv=mv';
