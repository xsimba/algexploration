function [y,no,noc] = nan_lip(y,dispyes,run);
%function [y,no,noc] = nan_lip(y,dispyes,run);
% Linear interpolation for all NaNs between the valid values at the edge.
% Leading and last NaNs are replaced by nearest valid value.
% dispyes: display message of no and noc (default=0)
% run: if specified: NaNs replaced by last valid value
% output: y  = vector with interpolated values
%         no = number of interpolated values
%         noc= number of chains of interpolated values
% If y is a matrix: lin. ip for all rows


%y=[];
%y = [NaN NaN 3 NaN NaN NaN NaN 5 6 7 NaN  8 9 10 NaN NaN 13 NaN NaN];
%y = [10 NaN 12];


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<3 run=0; end;
if nargin<2 dispyes=0; end;

no=0; % number of interpolated values
noc=0; % number of chains of interpolated values


[rows,cols]=size(y);
if cols==1 y=y'; rows=1; end;
Y=y;

for row=1:rows

if rows>1 disp(row); end;

y=Y(row,:);

y=y(:)';

if ~isempty(y)
if any(isnan(y))
if any(~isnan(y))

i=1;           % leading NaN are set to first valid value
if isnan(y(1))
while 1
if isnan(y(i)) i=i+1; no=no+1;
else break; end;
end;
for j=1:i-1 y(j)=y(i); end;
end;
if no noc=1; end;
noold=no;

i=length(y);           % last NaN are set to last valid value
if isnan(y(i));
while 1
if isnan(y(i)) i=i-1;no=no+1;
else break; end;
end;
for j=i+1:length(y) y(j)=y(i); end;
end;
if no~=noold noc=noc+1;end

i=1;
leny=length(y)+1;
while i<leny
j=0; % counts NaN in one chain
if isnan(y(i))
   xi=i-1;
   x1=y(xi);

   noc=noc+1;

   while isnan(y(i))
         i=i+1;
         j=j+1;
   end;
   x2=y(i);
   if run
   for ii=1:j
       y(xi+ii)=x1;
       no=no+1;
   end;
   else
   d=(x2-x1)/(j+1);
   for ii=1:j
       y(xi+ii)=x1+d*ii;
       no=no+1;
   end;
   end;
end;
i=i+1;
end;
end;
end;
end;

Y(row,:)=y;

end;

y=Y;

if dispyes
str=[int2str(no),' of ',int2str(length(y)),' values '];
str=[str,'in ',int2str(noc),' chains had to be interpolated.'];
disp(str);
end;
