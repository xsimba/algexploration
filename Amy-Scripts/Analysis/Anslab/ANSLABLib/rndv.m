function x=rndv(x,v);
% rounds vector x to v valid characters, default=3


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin < 2 v=3; end;

if ~all(isnan(x))
for j=1:length(x)
i=0;
if x(j)
s=sign(x(j));
x(j)=x(j)*s;
if x(j)>=10^v
  while x(j)>10^v
    x(j)=x(j)/10;
    i=i-1;
  end;
else
  while x(j)<10^(v-1)
    x(j)=x(j)*10;
    i=i+1;
  end;
end;
end;
x(j)=round(x(j));
x(j)=x(j)/10^i;
x(j)=x(j)*s;
end;
end;
