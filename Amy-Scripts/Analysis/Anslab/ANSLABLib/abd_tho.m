% derive abdominal and thoracial volumes and ratios after total volume

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

n1=[]; n2=[]; n3=[]; n4=[];

for i=1:length(maxt)

  n = maxt(i)-5 : maxt(i)+5;

  n(n<1)=[]; n(n>length(K01))=[];  % range check

  n1(i) = max(K01(n));
  n2(i) = max(K09(n));

  n = mint(i)-5 : mint(i)+5;

  n(n<1)=[]; n(n>length(K01))=[];  % range check

  n3(i) = min(K01(n));
  n4(i) = min(K09(n));

end;

abd = ((n1-n3) * beta(SUBJECT,1))';   % abdominal tidal volume
tho = ((n2-n4) * beta(SUBJECT,2))';   % thoracic  tidal volume

%M=[abd tho maxmin abd+tho];

RibProp   = tho ./ (maxv-minv);         % proportion of rib cage in tidal volume
RibProp_=statistics(RibProp);



