% load binary file from Vitaport

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



channel=[];
chdef=[];
doffs=[];
dlen=[];
mval=[];
chflg=[];
ampl=[];
hilo=[];
hard=[];
offset=[];


disp('Analyze data of calibration-procedure:');
subj= input('Subject no.= ');
i=input('Day (1/2)  = ');
str=[int2str(subj),'calib',int2str(i),'.dat'];

%str=['fp022amb.dat'];
%str=['fp021ca1.dat'];
%str=['34calib1.dat'];
%str=['34base1.dat'];

disp(['Loading file "',str,'"']);

[fid,message]=fopen(str,'rb','l');
if fid< 0 disp(message); break; end;

header=fread(fid,22);
hdlen=header(3)*256+header(4);
chnoffs=header(5)*256+header(6);
knum=header(8);
blcksl=header(9);
blcksh=header(10);
bytes=header(11)*256+header(12);
scnrate=header(21)*256+header(22);


for i=1:knum
status=fseek(fid,chnoffs+(i-1)*40,'bof');
chdef(1:40,i)=fread(fid,40);
end;

for i=1:knum
mulfac(i)=chdef(17,i)*256+chdef(18,i);
offset(i)=chdef(19,i)*256+chdef(20,i);
divfac(i)=chdef(21,i)*256+chdef(22,i);
doffs(i)=chdef(25,i)*256^3+chdef(26,i)*256^2+chdef(27,i)*256+chdef(28,i);
dlen(i)=chdef(29,i)*256^3+chdef(30,i)*256^2+chdef(31,i)*256+chdef(32,i);
mval(i)=chdef(33,i)*256+chdef(34,i);
chflg(i)=chdef(35,i);
ampl(i)=chdef(36,i);
hilo(i)=chdef(37,i);
hard(i)=chdef(38,i);
end;

names=setstr(chdef(1:10,1:knum))'

chdef=[(1:40)' chdef];

for i=1:knum
status=fseek(fid,hdlen+doffs(i),'bof');
                          % format word: Skin conductance or Spirometer
if strcmp(names(i,1:4),'SPIR') & subj>20
   eval(['A=fread(fid,[2,dlen(i)/2]);']);
   eval(['K',twostr(i),'=A(1,:)*256+A(2,:)-offset(i);']);
   eval(['K',twostr(i),'=K',twostr(i),''';']);

else
   eval(['K',twostr(i),'=fread(fid,dlen(i))-offset(i);']);
end;

%eval(['K',twostr(i),'=(K',twostr(i),'-offset(i))*mulfac(i)/divfac(i);']);

end;

fclose('all');


clear m1 m2 m3 m4 m5 m6 m7 m8 m9 m10 m11 res cmdstr str;
clear ampl blcksh blcksl bytes chdef chflg chnoffs d dlen doffs fid hard
clear hdlen header hilo i istr knum lench message mval names offset ref
clear scnrate status str fact newdlen refnew

defspir
defspir2
figure(1)
clf
hold off

