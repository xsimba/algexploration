%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

clear
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end


%*** Settings
if ~exist('data_dir') | ~exist('red_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
    red_dir=[data_dir,'\p'];  
    r1ind=3;   % sequential number in file of CO2
end
    
 %*** Load RESP data
chd(data_dir)
%FL=filelist('*.txt',0.25);
[filename, data_dir] = uigetfile('*.txt','Select data file')
eval(['load ',filename,';']);
l=length(filename);
varname=filename(1:l-4);
subjn=str2num(varname(4:6));
studystr=varname(1:3);
eval(['PC=',varname,';']);
len=length(PC)/25;


%*** resample if necessary
[PC,SR] = AskResampleData(PC,25,'CO2');

%*** Program settings
riseset=0;     %   (0) Set rise criterion automatically (0) or manually (1)
edityes=0;     %   (0) Load edited data (1=yes [if exist], 0=no)
plotyes1=1;    %   (1) Plot PCO2 reduced data and allow recursive editing
ovl=20;        %  (20) Segment overlap in sec
winmin=60;     %  (60) Segment window length in minutes
co2_switch=1;  %   (1) Display PCO2 plateau markers in Exam
breath_switch=0;%  (0) Display breath time markers in Exam

%*** Initialization
lucas_flag=0;
delsampend=0;
byes=0;
bno=1;
bplotyes=0;

%*** Load pCO2 data
chd(data_dir)


findpco5;


%*** Resample PCO2 and RR to instantaneous 10 Hz
disp('Conversion of PCO2 parameters into 0.10 sec PCO2 and respiratory rate epochs');
ep=10;

CO2t=rtn(:);
CO2=rtnd(:);
CO20=epoch(rtn2,rtnd2,sr/ep);
b=resp_time(:);
bd=diff(b);  % problem with doubles
c=find(~bd);
b(c)=[];
bd=difffit(b);
RR0=epoch(b, 60 ./(bd/sr), sr/ep);
nn=max([length(CO20),length(RR0)]);
RR0=cut_fill(RR0,nn);
CO20=cut_fill(CO20,nn);
reslen=length(CO20);
t=(1:reslen)/ep;
t=t(:);

CO20=nan_lip(nanfill(CO20,reslen))';
RR0=nan_lip(nanfill(RR0,reslen))';

figure
subplot(2,1,1)
plot (t,CO20)
title('pCO2')
ylabel('mm Hg')
subplot(2,1,2)
plot (t,RR0)
title('Respiratory rate')
ylabel('cpm')
xlabel('time [sec]')

%*** Save reduced data
chd(red_dir)
S=[CO20 RR0];
disp(['save ',varname,'.txt -ascii -tabs S']);
eval(['save ',varname,'.txt -ascii -tabs S']);
disp(num2str(mean(CO20)));
disp(num2str(mean(RR0)));



