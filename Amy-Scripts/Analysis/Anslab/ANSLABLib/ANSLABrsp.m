%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

clear
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultTextColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersionString= version;
MVersNr = str2num(MVersionString(1:3));
if MVersNr>6
    eval('warning off;');
end

%*** Settings
if ~exist('data_dir') | ~exist('red_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
    red_dir=[data_dir,'\r'];  
end



 %*** Load RESP data
chd(data_dir)
%FL=filelist('*.txt',0.25);
[filename, data_dir] = uigetfile('*.txt','Select respiration data file')
eval(['load ',filename,';']);
varfilename = filename(1:end-4);
l=length(filename);
varname=filename(1:8);
subjn=str2num(varname(4:6));
studystr=varname(1:3);
eval(['EC=',varfilename,';']);
eval(['clear ',varfilename,';']);
R1=EC(:,1);
R2=EC(:,2);

AC = [];
[filenameacc, data_diracc] = uigetfile('*.txt','Select accelerometer data file or press cancel');
if ~isequal(filenameacc,0)&~isequal(data_diracc,0)
    eval(['load ',filenameacc,';']);
    varname=filename(1:l-4);
    eval(['AC = ',filenameacc(1:end-4),';']);
end


%*** resample if necessary
[R1,R2,AC,SR] = AskResampleDataResp(R1,R2,AC,25);


eval(['clear ',varname]);

%*** Resampling
%ecglen=length(EC);
%EC=resample(EC,2,5);


%*** RESP analysis settings (program FINDRSP5)
% Criteria for size and shape of breath (samplerate=25)
vtmin=60;        % (60) Exclusion of tidal volumes smaller than vtmin ml
usemeanvt=1;     %  (1) Use mean of inspiratory and expiratory tidal volume for exclusion
rise =50;        % (50) Milliliter tidal volume to be counted as breath 
back1=40;        % (40) Comparison to point1 in the past  1.12s 
back2=24;        % (24) Comparison to point2 in the past  0.64s 
                 %      Valid breaths rise 'rise' ml within back1/25 or back2/25 sec
area=25;         % (25) Minimal window for peak detection algorithm 
forward=-1;      % (-1) Forward comparison point (-1 = no forward comparison) 
mingap=25;       % (25) Minimal gap between two breaths
% Criteria for onset of inspiration and expiration
onset_flow=100;  %(100) Flow rate [in ml] at a certain point needs to be 
                 %      greater than  onset_flow for inspiratory onset and
                 %      smaller than -onset_flow for expiratory onset
onset_rise=30;   % (30) Additionally: onset_rise [ml] increase within onset_back 
onset_forward=16;% (16) Comparison to onset_forward point in the future
ramp=40;         % (40) Expiratory pause: set end of expiration earlier if
                 %      slope is slightly negative and change from potential other end
                 %      of expiration to current minimum is less than ramp ml
reset_check=0;   %  (0) Menue for check of resets
filtlowyes=1;    %  (1) High pass filter above .033 Hz (30 sec period)


% Load BETAs
chd(data_dir); 
eval(['load ',studystr,'c.m']);
eval(['BETA=',studystr,'c;']);
n=BETA(:,1); 
i1=find(n==subjn);
if ~isempty(i1)
   bet=BETA(i1,2:5);
else
   uiwait(errordlg('Analyze calibration first!')); return
end

%*** Additional initial settings
lucas_flag=0;
byes=0;
subject=int2str(subjn);
row=1;
bpause=0;

%*** Resp detection
findrsp5

%*** Resample RESP reduced data to instantaneous 10 Hz
ep=10;
%RS0=decfast(ipfast(RS,ep),sr);
RS0=resample(RS,ep,sr); % much slower (20x), but not really more accurate
iontd=difffit(iont);
Tt0=epoch(iont,iontd,(sr/ep)) ./sr;
RR = 60 * (length(Tt)) / sum(Tt); %little higher, eg. 11.48 vs. 11.40
RR0= 60 ./Tt0;
x=iont; iont(length(iont))=[];
iontall=x;  % for save
Vt0=epoch(x,Vt,(sr/ep));
Vmin0=epoch(iont,Vmin,(sr/ep));
Flow0=epoch(x,Flow,(sr/ep));
Ti0=epoch(x,Ti,(sr/ep));
Pi0=epoch(x,Pi,(sr/ep));
Te0=epoch(x,Te,(sr/ep));
Pe0=epoch(x,Pe,(sr/ep));
TiTt0=epoch(x,TiTt,(sr/ep));
if ~all(isnan(RibP))
RibP0=epoch(x,RibP,(sr/ep));
else
RibP0=NaN*ones(size(RR0));
end;
reslen=length(RS0);
t=(1:reslen)/ep;
t=t(:);

RS0=nan_lip(nanfill(RS0,reslen))';
RR0=nan_lip(nanfill(RR0,reslen))';
Tt0=nan_lip(nanfill(Tt0,reslen))';
Vt0=nan_lip(nanfill(Vt0,reslen))';
Vmin0=nan_lip(nanfill(Vmin0,reslen))';
Flow0=nan_lip(nanfill(Flow0,reslen))';
Ti0=nan_lip(nanfill(Ti0,reslen))';
Pi0=nan_lip(nanfill(Pi0,reslen))';
Te0=nan_lip(nanfill(Te0,reslen))';
Pe0=nan_lip(nanfill(Pe0,reslen))';
TiTt0=nan_lip(nanfill(TiTt0,reslen))';
RibP0=nan_lip(nanfill(RibP0,reslen))';
Tic0=Ti0+Pi0;
Tec0=Te0+Pe0;


%*** Plot
cfig
figure(1)
subplot(5,1,1)
plot(t,RS0);
title('Respiratory pattern');
ylabel('mL')
subplot(5,1,2)
plot(t,RR0);
title('Instantaneous respiratory rate');
ylabel('cpm')
subplot(5,1,3)
plot(t,Vt0);
title('Instantaneous tidal volume');
ylabel('mL')
subplot(5,1,4)
plot(t,Vmin0);
title('Instantaneous minute volume');
ylabel('L/min')
subplot(5,1,5)
plot(t,TiTt0);
title('Instantaneous duty cycle');
ylabel('ratio')
xlabel('sec')




%*** Save reduced data, as one file, new version
chd([red_dir]);
S=[RS0 RR0 Vt0 Vmin0 Flow0 TiTt0 Ti0 Pi0 Te0 Pe0 Tt0 Tic0 Tec0 RibP0];
disp(['save ',varname,'r.txt -ascii -tabs S']);
eval(['save ',varname,'r.txt -ascii -tabs S']);


%*** Compute statistics
c=1;
valperc=50;
n=1:length(RS0);

if ~isempty(n)
rr_(c)=meanbin(RR0(n),length(n),valperc);
vt_(c)=meanbin(Vt0(n),length(n),valperc);
vm_(c)=meanbin(Vmin0(n),length(n),valperc);
flo(c)=meanbin(Flow0(n),length(n),valperc);
tit(c)=meanbin(TiTt0(n),length(n),valperc);
ti_(c)=meanbin(Ti0(n),length(n),valperc);
pi_(c)=meanbin(Pi0(n),length(n),valperc);
te_(c)=meanbin(Te0(n),length(n),valperc);
pe_(c)=meanbin(Pe0(n),length(n),valperc);
tt_(c)=meanbin(Tt0(n),length(n),valperc);
tic(c)=meanbin(Tic0(n),length(n),valperc);
tec(c)=meanbin(Tec0(n),length(n),valperc);

if ~all(isnan(RibP0))
rib(c)=meanbin(RibP0(n),length(n),valperc);
else
rib(c)=NaN;
end

[temp,mssd] = coeffvar(nanrem(Vt));
vtv(c)=sqrt(mssd);
[temp,mssd] = coeffvar(nanrem(Tt));
ttv(c)=sqrt(mssd);

if 0
%fb=[0.004 0.14]; % like Biol Psych paper
fb=[0.025 0.14]; 
plotyes=1;
[ttc_,freqm,x,F,P]=cdm(nan_lipf(Tt0(n)),4,plotyes,fb);
[vtc_,freqm,x,F,P]=cdm(nan_lipf(Vt0(n)),4,plotyes,fb);
ttc(c)=ttc_;
vtc(c)=vtc_;

% display output
td = ( iont(length(Vt)-5) - iont(6) ) / sr;
sisec_med=(count/td*60);
m=mean(Vt);
m_ns=mean(Vt(~Si));
s_med=mean(Vt(find(Si)));
if s_med==0 s_med=NaN; end

fprintf(1,'Sigh count');
fprintf(1,'\n');
fprintf(1,'Count/min'); 
fprintf(1,'\n');
fprintf(1,'Mean TV'); 
fprintf(1,'\n'); 
fprintf(1,'Mean non-sigh TV');  
fprintf(1,'\n');
fprintf(1,'Mean Sigh TV'); 
fprintf(1,'\n');
fprintf(1,' ')
fprintf(1,'\n');
fprintf(1,num2str(count));
fprintf(1,'\n');
fprintf(1,num2str(sisec_med));
fprintf(1,'\n');
fprintf(1,num2str(m));
fprintf(1,'\n');
fprintf(1,num2str(m_ns));
fprintf(1,'\n');
fprintf(1,num2str(s_med));
fprintf(1,'\n');
end

else
rr_(c)=NaN;
vt_(c)=NaN;
vm_(c)=NaN;
flo(c)=NaN;
tit(c)=NaN;
ti_(c)=NaN;
pi_(c)=NaN;
te_(c)=NaN;
pe_(c)=NaN;
tt_(c)=NaN;
tic(c)=NaN;
tec(c)=NaN;
rib(c)=NaN;
vtv(c)=NaN;
ttv(c)=NaN;
%vtc(c)=NaN;
%ttc(c)=NaN;
%count=NaN;
%sisec_med=NaN;
%m=NaN;
%m_ns=NaN;
%s_med=NaN;
end


% Display results
fprintf(1,'\n');

fprintf(1,'Means for the variables:\n\n')
fprintf(1,[num2str(rr_(c)) ,'  -- respiratory rate']);
fprintf(1,'\n');
fprintf(1,[num2str(vt_(c)) ,'  -- tidal volume']);
fprintf(1,'\n');
fprintf(1,[num2str(vm_(c)) ,'  -- minute ventilation']);
fprintf(1,'\n');
fprintf(1,[num2str(flo(c)) ,'  -- inspiratory flow rate']);
fprintf(1,'\n');
fprintf(1,[num2str(tit(c)) ,'  -- duty cycle']);
fprintf(1,'\n');
fprintf(1,[num2str(ti_(c)) ,'  -- inspiratory time']);
fprintf(1,'\n');
fprintf(1,[num2str(pi_(c)) ,'  -- inspiratory pause']);
fprintf(1,'\n');
fprintf(1,[num2str(te_(c)) ,'  -- expiratory time']);
fprintf(1,'\n');
fprintf(1,[num2str(pe_(c)) ,'  -- expiratory pause']);
fprintf(1,'\n');
fprintf(1,[num2str(tt_(c)) ,'  -- total time']);
fprintf(1,'\n');
fprintf(1,[num2str(tic(c)) ,'  -- inspiratory time incl. pause']);
fprintf(1,'\n');
fprintf(1,[num2str(tec(c)) ,'  -- expiratory time incl. pause']);
fprintf(1,'\n');
fprintf(1,[num2str(rib(c)) ,'  -- fractional rib cage contribution to tidal volume']);
fprintf(1,'\n');
fprintf(1,[num2str(vtv(c)) ,'  -- mean square successive difference of tidal volumes']);
fprintf(1,'\n');
fprintf(1,[num2str(ttv(c)) ,'  -- mean square successive difference of total times']);
fprintf(1,'\n');

fprintf(1,'\n');
fprintf(1,'\n');
disp('The following parameters have been saved:')
fprintf(1,'\n');
disp('respiratory raw signal')
disp('respiratory rate')
disp('tidal volume')
disp('minute ventilation')
disp('inspiratory flow rate')
disp('duty cycle')
disp('inspiratory time')
disp('inspiratory pause')
disp('expiratory time')
disp('expiratory pause')
disp('total time')
disp('inspiratory time incl. pause')
disp('expiratory time incl. pause')
disp('fractional rib cage contribution to tidal volume')

% RS0 RR0 Vt0 Vmin0 Flow0 TiTt0 Ti0 Pi0 Te0 Pe0 Tt0 Tic0 Tec0 RibP0];