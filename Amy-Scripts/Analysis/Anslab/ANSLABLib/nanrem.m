function [x,ind,rownan]=nanrem(x);
% [x,ind,rownan]=nanrem(x);
% Removes NaNs from a vector x, ind=indices of removed NaNs
% If x is a matrix: ind is 0/1-matrix indicating NaNs
% If complete rows are NaN: rownan contains indices of these rows


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if ~isempty(x)

[i1,i2]=size(x);

if i1==1 | i2==1

ind=find(isnan(x));
x(ind)=[];

else
  ind=zeros(i1,i2);
  rownan=[];
  for i=1:i1
    n=x(i,:);
    n1=isnan(n);
    len=length(find(n1));
    if len==i2 rownan=[rownan;i]; end;
    n(n1)=mean(n(~n1))*ones(len,1);
    x(i,:)=n;
    ind(i,:)=n1;
  end;
end;

end;
