% Analysis of end-tidal pCO2


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


% Initialization
sr=25;         %   samplerate
ep=4;          %   epoch rate
%winsize=winmin*60*sr-ovl*sr;
%lensec=len;
%lall=lensec*sr;
%lseg=winsize;
%nseg=ceil(lall/lseg);
%remseg=rem(lall,lseg);
clear y COf CO2t CO2 CO20 artbegin artend artinterval mt mv peak_index peak breath_time breath_time_new resp_time resp_interval resp_rate;

% Analysis
y=PC;
psyfco2
if invalid_segment==1
	CO2t=[];
	CO2=[];
	CO20=[];
	RR0=[];
	resp_time=NaN;
	resp_rate=NaN;
	artbegin=NaN;
	artend=NaN;
	artinterval=NaN;
	spikeyes=NaN;
	back=NaN;
	rise=NaN;
	min_inter=NaN;
	area=NaN;
	mingap=NaN;
	absmin=NaN;
end

