function x1 = epoch(x,y,points);
% function x1 = epoch(x,y,points);
% Convert time- and value vectors into instantanous real-time epochs of length points
% x:	  eventtime-vector
% y:	  according values
% points: no. of samples within one epoch, if not integer: rounded and resampled at end (slow)
% For a valid epoch, 50% of the values within the epoch must be valid.
% Copyright (c) Frank Wilhelm, Ph.D., 2-12-99

% test:
%x= [10  20  30 40 NaN NaN  70  NaN 90 100 NaN];
%y=[ 4 NaN   6  4 NaN NaN NaN  NaN  6   4 NaN];
%points=10;

%test: x=CO2t; y=CO2; points=25/4;

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

valnan=0.50;   % percent valid

if ~all(isnan(y))

% resample at end
if floor(points)~=points
   if floor(points*4)~=points*4
     disp('Error: epoch length has to contain number of sample points that are integer if multiplied by 4.')
     c=v;  % produce error!
   end
   rsyes=1;
   pointsold=points;
   points=floor(points);
else
   rsyes=0;
end

n=find(~isnan(x));
if length(n)

i=1;                    % remove leading NaNs from x
while isnan(x(i))
      x(i)=[];
      y(i)=[];
end;

i=length(x);            % remove last NaNs from x
while isnan(x(i))
      x(i)=[];
      y(i)=[];
      i=i-1;
end;


if points==1

n=NaN*ones(length(x),1);
for i=1:length(x)-1
n(x(i):x(i+1)-1)=y(i)*ones(length(x(i):x(i+1)-1),1);
x1=n;
end;

else

dt=diff(x);

                                 % length of filled must be multiple of points
len=fix(x(length(x))/points)*points+points;
if rem(x(length(x)),points)==0 len=len-points; end;

filled=ones(1,len)*NaN;
%filled(1,1:x(1))=ones(1,x(1))*NaN; end;       % begin is not valid

lent=length(x);


i=1;
while i<lent                       % fill with values!
    if ~isnan(dt(i))               % valid
       n=x(i)+1:x(i+1);
       filled(1,round(n))=ones(1,length(n))*y(i);
       i=i+1;
    else                           % diff is NaN

       beg=x(i)+1;
       i=i+1;                      % remove following NaN
       while isnan(x(i))
             i=i+1;
       end;
       %filled(1,beg:x(i))=ones(1,x(i)-beg+1)*NaN;
    end;
end;


%if rem(x(length(x)),points)~=0                        % end not valid
 %  filled(1,x(length(x))+1:len)=ones(1,len-x(length(x)))*NaN;
%end;

filled=reshape(filled,points,len/points);


x1=mean(filled)';                 % mean, epochs with NaN -> mean=NaN
n=find(isnan(x1));                % mean, epochs with NaN approximated
for i=1:length(n)
    meancol=filled(:,n(i));
    meancol=meancol(~isnan(meancol));
    if length(meancol)>=points*valnan           % how many % valid points have to be
       x1(n(i))=mean(meancol);    % in one epoch so that this epoch
    end;                                   % is valid (typical: 50% => j=0.5)
end;

if rsyes
x1i=ipfast(x1,points*4);
x1=decfast(x1i,pointsold*4);
end

end;
% if points==1

else
  disp('only NaN in time vector');
  x1=NaN;
end; %if length(n)


else
  disp('only NaN values');
  l=nanmax(x);
  len=ceil(l/points);
  x1=NaN*ones(len,1);
end; %if length(n)

