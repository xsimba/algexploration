% klinech.m  change linetypes: L

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


disp('Change line type of variable:');
m1 = '1'; m2 = '2'; m3 = '3'; m4 = '4';
i=menue(m1,m2,m3,m4);
if i>0
disp('Desired line type:');
m1 = 'solid';m2 = 'dashed';m3 = 'dotted';m4 = 'dashdot';m5='x-mark';
m6 = 'plus';m7 = 'star';m8 = 'circle';m9 = 'points (weak)';
m10= 'invisible';
j=menue(m1,m2,m3,m4,m5,m6,m7,m8,m9,m10);
if j>0 eval(['ltv',int2str(i),'=lt',int2str(j),';']); end;
end;
