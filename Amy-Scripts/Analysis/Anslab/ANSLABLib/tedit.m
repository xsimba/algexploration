%*** tedit.m



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



if ~exist('artdiffbeg') artdiffbeg=[]; artdiffend=[]; end;

%****** Inspect and/or exclude outliers loop
inspectloop=1;
cfig; cfig; cfig;
figure(1)
if screen>800  set(1,'Position',[3 233 1017 493]); else set(1,'Position',[2 202 792 356]); end

tvn=tv;  % tv new
ttn=tt;

if exist('tv_old') tvn_old=tv_old; else tvn_old=NaN*ones(size(tvn)); end

while inspectloop

figure(1)
clf
if usecorrt
subplot(2,1,1)
plot(ttn/sr,tvn_old);
axisx(0,max(ttn/sr));
ylabel(['TWA [mV] = ',num2str(mean(nanrem(tvn_old)))]);
title('Uncorrected T-wave amplitude');
end
if usecorrt
subplot(2,1,2)
end
plot(ttn/sr,tvn);
axisx(0,max(ttn/sr));
xlabel('time [sec]');
ylabel(['TWA [mV] = ',num2str(mean(nanrem(tvn)))]);
title(['Corrected T-wave amplitude,   ',num2str(points_changed),' points changed']);
if ~exist('points_changed') points_changed=0; end

m1='Exit editing';
m2='Display of T-waves';
m3='Exclude outliers';
m4='Start again';
m5='Display segment';
outyes=menudef1('Please select',m1,m2,m3,m4,m5);

if outyes<2  inspectloop=0; end;

if outyes==2
   ax=axis;
   x=[];
   hold on
   disp(' ');
   disp('Click twice to mark suspicious intervals, end with <0>')
   disp('In EXAM: goto marked intervals with <0>');
   disp('Delete false r-spikes with <#>');
   disp('Insert correct r-spikes with <@> or <!>');
   disp('Mark begin <b> and end <e> of artifact, remove with <->');

   while 1
   [i1,i2,i3]=ginput(1);
   if i3==48
     if length(x)/2==floor(length(x)/2)
        break; break; break; break; break;
     else
        disp(' ');
        disp('Odd number of lines.  Please click once more!');
        title('Odd number of lines.  Please click once more!');
        [i1,i2,i3]=ginput(1);
     end
   end;
   x=[x i1];
   plot([i1;i1],[ax(3);ax(4)],':c');   % display vertical line
   end;
   hold off
   x=x*sr;
   lookat=reshape(x,2,length(x)/2)';
   figure(1);clf
   clear var2 var3 var4

   defblank; int=6; moveint=60; samplerate=sr; var1=y; def;
   artbegin=artdiffbeg; artend=artdiffend;
   event1=rt; event1yes=1; skiploc=.5;
   val=tvn; valtime=ttn; valueyes=1; tsub=[];
   exam;

   if exist('artinterval') if ~isempty(artinterval)
   disp('Exclusion of values within specified artifact intervals');
   setmiss=exclude(ttn,artinterval);
   if ~isempty(setmiss)
      tvn(setmiss)=ones(1,length(setmiss))*NaN;
      disp([int2str(length(setmiss)),' values were set to missing.']);
   end; end; end;
   inspectloop=1;

end;

if outyes==3
   tvn = outrect (tvn,ttn/sr,1);
   inspectloop=1;
end;

if outyes==4  ttn=tt; tvn=tv; inspectloop=1; end;

if outyes==5
   [i1,i2,i3]=ginput(2);
   i2=round(i1*sr);
   n=find(ttn>=i2(1) & ttn<=i2(2));
   n2=find(rt>=i2(1) & rt<=i2(2));
   figure(2)
   clf
   subplot(2,1,1)
   t=i2(1):i2(2);
   i=find(t<=0);
   t(i)=[];
   i=find(t>ttn(length(ttn)));
   t(i)=[];
   yn=y(t);
   %miny=min(yn); maxy=max(yn);
   %fact=(max(nanrem(tvn(n)))-min(nanrem(tvn(n))))/(maxy-miny);
   %yns=(yn-miny)*fact+min(nanrem(tvn(n)));
   plot(t/sr,yn,'w');   %,ttn(n)/sr,tvn(n),'g');
   axisx(min(t/sr),max(t/sr));
   plotline(ttn(n)/sr,'b:');
   plotline(rt(n2)/sr,'c:');
   title('Segment of EKG: Detected T-waves [blue] (R-waves [cyan] deleted)')
   xlabel('sec')
   ylabel('units')
   i=ginput(1);
end;

end;
%1****** inspect data for artifacts loop

tt=ttn;
tv=tvn;


