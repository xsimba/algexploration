% Respitrace calibration using spirometer 
% Least squares fit method
%
% Load respiratory data from Vitaport binary file and start EXAM
% In EXAM: use <z>oom,   <h> for cycle detection (starts kbmaxa.m), 
%              <u>nzoom, <q>uit,  do not use <B>!
% The following parameters will be created 
% and saved in a file (same name as .dat-file, but with extension .mat):
% bet: final weighting coefficients for bands
%      bet(1)=thorax, bet(2)=abdomen, 
%      bet(3)=thorax if abd missing, bet(4)=abd if thorax missing
% bhis: succeeding history of betas
%       rows: 1=beta(tho),   2=beta(abd), 
%             3=# taken out, 4=correlation in regression plot, 
%             5=ml error,    6=% error, 
%             7=thorax if abd missing, 
%             8=abd if thorax missing
% betataken: which ones if not final betas were taken
% BM/BMIN: max/min-value matrices from exam, incl. indices 
% E: final matrix of [pred1 pred2 crit] 


%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

clear
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end

%data directory
if ~exist('data_dir') 
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
end

%*** Load RESP data
chd(data_dir)
%FL=filelist('*.txt',0.25);
[filename, data_dir] = uigetfile('*.txt','Select data file')
eval(['load ',filename,';']);
l=length(filename);
varname=filename(1:l-4);
subjn=str2num(varname(4:6));
studystr=varname(1:3);
eval(['EC=',varname,';']);
R1 = EC(:,1);
R2 = EC(:,2);
SP = EC(:,3);
eval(['clear ',varname]);


%*** resample if necessary
[R1,R2,SP,SR] = AskResampleDataThree(R1,R2,SP,25);


SR(1) = 25;
SR(2) = 25;
SR(3) = 25;

m1='Find breath maxima and minima automatically';
m2='Manual setting of maxima and minima';
automanualstr=questdlg('Dou you wish to find breath maxima and minima...','Respiration calibration:','manually','automatically','automatically');
if strcmp(automanualstr,'manually')
    automanual = 2;
else
    automanual = 1;
end

if automanual==2
    %********************** Mark onsets end ends of inspirations in EXAM
    defcal5
    def
    radapt=0; doubleyes=0;
    clear subvarplot
    clg;
    disp(' ');
    disp('In EXAM: Use <@> to mark the onset of a valid inspiration first,');
    disp('then use <@> or <!> to mark the end of inspiration.');
    disp('Then go to another valid breath (the order is not significant).');
    disp('Delete erroneous marks with <#>.');
    disp('All options to zoom <z> or move through the signals can be used.');
    disp('Press <i> to display the whole file, then quit with <q>');
    exam
    event1=sort(event1);
    while rem(length(event1),2)
        disp('Odd number of marked events. Delete or add one.');
        exam
    end;

    t1=1:2:length(event1)-1;
    t2=t1+1;
    pred1= R1(event1(t2)) - R1(event1(t1));        % predictors: thoracic
    pred2= R2(event1(t2)) - R2(event1(t1));        %    abdominal tidal volumes


else
    %******************************** Find max of breaths automatically
    dist=16; back=12; slope=.04; win=8;
    var1=R1; var2=R2; var3=SP;
    figure('Position',[32   208   767   351])
    numbonplot=1;   % lag between consecutive breath numbering on plot
    newthresh=1;

    while newthresh
        threshtype = questdlg('Find maxima with which threshold type?','Max/Min:','default threshold','manual threshold','quit','default threshold');
        if strcmp(threshtype,'default threshold')
            newthresh = 1;
        elseif strcmp(threshtype,'manual threshold')
            newthresh = 2;
        elseif strcmp(threshtype,'quit')
            newthresh = 0;
            break;
        end
        
        if newthresh==2
            prompt={'Minimal distance between maxima: ','Comparison to a point back: ','Slope from there at least to be valid:','Window for search of maxima in other channel:'};
            name='Threshold input:';
            numlines=1;
            defaultanswer={'16','12','0.04','8'};
            answer=inputdlg(prompt,name,1,defaultanswer);
            if isempty(answer)
                dist=16;
                back=12;
                slope=.04;
                win=8;
            else
                dist = str2num(answer{1});
                back = str2num(answer{2});
                slope = str2num(answer{3});
                win = str2num(answer{4});
            end
        end

        xmax1=[]; xmax2=[]; 
        xmin1=[]; xmin2=[]; 
        mini=[];bm=[]; bmin=[]; 

        x1=var1;
        xmax1=findmax(x1,dist,back,slope)';  %thorax maxima
        x2=var2;

        for i=1:length(xmax1)
          n=xmax1(i)-win:xmax1(i)+win+2;
          n(n<1)=[]; n(n>length(x1))=[];  % range check
          [j,xmax2(i)] = max(x2(n));  
        end
        xmax2=xmax1-win-1+xmax2;

      
        
        
        %*** find between minima
        i1=xmax1;  % max index

        for i=1:length(i1)-1
            n= i1(i):i1(i+1);
            [j,xmin2(i)] = min(x2(n));
            [mini(i),xmin1(i)] = min(x1(n));
        end
        
        i1(length(i1))=[];
        xmin1=i1+xmin1-1;
        xmin2=i1+xmin2-1;

        %** first maxima not valid for breath
        xmax1(1)=[];
        xmax2(1)=[];

        out=1;

        cfig, cfig
        figure('Position',[32   208   767   351])

        while out

            %*** plot found maxima and minima
            figure(1)
            t=1:length(x1);
            plot(t,x1,t,x2)
            axis([ t(1) t(length(t)) min([x1(:);x2(:)]) max([x1(:);x2(:)]) ]) 
            title(['Respitrace units: thorax (y), abdomen (m) -  ',int2str(length(xmax1)),' valid inspirations']);
            hold on
            plot(xmax1,x1(xmax1),'oy');
            plot(xmax2,x2(xmax2),'om');
            plot(xmin1,x1(xmin1),'oy');
            plot(xmin2,x2(xmin2),'om');
            hold off

            % number the breathing cycles on plot
            for i=1:numbonplot:length(xmax1)
                str=int2str(i);
                cmdstr=['text(''Position'',[xmin1(i) x1(xmin1(i))-.05],''String'','' ',str,''')'];
                eval(cmdstr);
            end
            hold off

            outstr=inputdlg('Take out cycle no: [0 = done] ==> ','Exlude cycles:',1,{'0'});
            if isempty(outstr)
                out=0;
                break;
            else 
                outstr = outstr{1};
                if strcmp(outstr,'0')
                    out=0;
                    break;
                end
                f=findstr(outstr,'-');
                if ~isempty(f)
                    outbeg=str2num(outstr(1:f-1));
                    outend=str2num(outstr(f+1:length(outstr)));
                    out=outbeg:outend;
                else
                    out=str2num(outstr);
                end
                xmax1(out)=[]; xmax2(out)=[];
                xmin1(out)=[]; xmin2(out)=[]; mini(out)=[];
            end

        end
        %while out
    end  %while win

    pred1=x1(xmax1)-x1(xmin1);
    pred2=x2(xmax2)-x2(xmin2);
    event1=[ [xmin1 xmax1];[xmin2 xmax2] ];

end



%*** Exclude channel from calibration
ch_okstr=questdlg('Specify qualitiy of thorax and abdomen:','data quality:','Both channels ok','Only thorax ok','Only abdomen ok',Both channels ok');
if strcmp(ch_okstr,'Both channels ok')
    ch_ok==1;
elseif strcmp(ch_okstr,'Only thorax ok')
    ch_ok==2;
elseif strcmp(ch_okstr,'Only abdomen ok')
    ch_ok==3;
end

if ch_ok==1
    pred2=zeros(size(pred1));
end
if ch_ok==2
    pred1=zeros(size(pred1));
end
if ch_ok
    weight=1;
else
    weight=2;
end

%******* Regression procedure
betasucc=[];  % history of betas and other parameter
omit=[];      % omitted breaths
crit = SP;  % criterium: spirobag
pred1=pred1(:); pred2=pred2(:); crit=crit(:);
E=[pred1 pred2 crit];  % Build E-matrix with all breaths
x=pred1*weight+pred2;  % weight 2 for thorax
x1=pred1;              % only thorax valid
x2=pred2;              % only abdomen valid
t=1:length(x);
take=ones(size(t));
figure(2)
plot(t,pred1,t,pred2);
title('Voltage change of thoracic (yellow) and abdominal (magenta) bands');
xlabel('breath number');
ylabel('V');
