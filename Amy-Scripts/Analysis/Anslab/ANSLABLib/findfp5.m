% Finds maxima, minima, and PTT in finger or ear plethysmogramm
% Gross projects
% Conversion into 0.25sec epochs of instantanous data


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

% Initialization
srp=400;      %(400) samplerate for plethysmograph
ep=4;         % epoch size = 1/ep sec
sribi=srp*upsrfacecg;
if ~exist('bno')
    bno=1;
    bplotyes=1;
end
rateyes=0;
rat=0;
key=[0.9999 0.8441 0.6267 0.4092 0.1918 0.0001];
artinterval=[];
artdiffbeg=[]; artdiffend=[];

%*** new procedures for artifacts in recording
if switch1
    disp('Removal of spike artifacts');
    y=spikere3(y,5,3);
end;

%*** low pass filter
if switch3
    disp('20 Hz low-pass filter');
    y=filthigh(y,20,srp,5);
end;

%*** high pass filter
if switch4 & ~switch5
    disp('0.13 Hz high-pass filter');
    y=filtlow(y,.13,srp,5);
end;

%*** high pass filter
if switch5
    disp('0.30 Hz high-pass filter');
    y=filtlow(y,.3,srp,5);
end;

%*** load existing rtime
chd(i_dir);
file=[subject,twostr(filenum),'i'];

loadok;

if ~ok
   disp('R-times of this interval not found. Analyze EKG - HR first.');
   fileok=0;   %flag for missing file -> data not saved in anx.m


else
    fileok=1;

    %******************************************
    %*************************** find maxima
    %******************************************

    %*** exclude NaN-values in rtimes for FP
    %n=find(isnan(rtd));
    %rt(n) = NaN * ones(length(n),1);
    rtd=difffit(rt)/sribi*1000; % IBI in msec
    hr=60000./rtd;

    % adjustment of R-wave times and RR intervals for other samplerate
    r2=rt/ (sribi/srp);
    dr=diff(r2);

    % first derivative for upstroke detection
    yd=diff(y);
    yd=[yd(1);yd];

    lenr=length(dr);
    st=zeros(lenr,1);    % systole-time
    sv=zeros(lenr,1);   %  absolute systole-value
    dt=zeros(lenr,1);    % diastole-time
    dv=zeros(lenr,1);   %  absolute diastole-value
    xt=zeros(lenr,1);    %
    xv=zeros(lenr,1);   %


    for i=1:lenr
        if isnan(dr(i))
            st(i)=NaN;
            dt(i)=NaN;
            sv(i)=NaN;
            dv(i)=NaN;
        else

           t1=r2(i)+int1*dr(i);        % adjust sys/diastole detection interval here
           t2=r2(i)+int2*dr(i);        %
           if dr(i)<(600/hradj)
               t2=t2+hradd*dr(i);
           end;   % adjustment for HR > 100
           if t2>length(y)
               t2=length(y);
           end
           [sv(i),st(i)]=max(y(t1:t2));
           [dv(i),dt(i)]=min(y(t1:t2));
           st(i)=st(i)+t1-1;
           dt(i)=dt(i)+t1-1;

           if 0
               if ((sv(i)-y(st(i)-dist)) < slope) | ((y(dt(i)+dist)-dv(i)) < slope)
                  st(i)=NaN; dt(i)=NaN;     % check slope (exclude calibration stairs)
               end
           end
        end % if
    end % for

    % reverse times if dt after st to avoid crash
    n1=find((st-dt)==0);
    st(n1)=dt(n1)+mean(st-dt);
    n2=find((st-dt)<0);
    i=st(n2); st(n2)=dt(n2); dt(n2)=i;
    n3=find((sv-dv)<=0);
    n=[n1 n2 n3];

    % PTT from upstroke
    for i=1:lenr
        if isnan(dr(i))
            xt(i)=NaN;
            xv(i)=NaN;
        else
           [i1,i2]=max(yd(dt(i):st(i)));  % upstroke maximum for PTT
           xv(i)=i1; xt(i)=i2;
           xt(i)=xt(i)+dt(i)-1;
        end % if
    end % for

    xt(n)=NaN*ones(size(n));
    sv(n)=NaN*ones(size(n));
    dv(n)=NaN*ones(size(n));
    xv(n)=NaN*ones(size(n));

    st(lenr+1)=2*st(lenr)-st(lenr-1);   % adapt lengths to rtime
    dt(lenr+1)=2*dt(lenr)-dt(lenr-1);
    xt(lenr+1)=NaN;
    sv(lenr+1)=NaN;
    dv(lenr+1)=NaN;
    dr(lenr+1)=NaN;
    xv(lenr+1)=NaN;

    %************** PTT
    if ptt_is_max
        pttt=st;
    else
        pttt=xt;
    end
    n=find(isnan(r2));
    pttv=(pttt-r2)*1000/(srp);
    pttv(n)=ones(size(n))*NaN;  % missing rwave-times

    %***** exclude timing artifacts
    if 0
        n=find((st-dt)<0);            % systoles need to be after diastoles
        n1=ones(size(n))*NaN;st(n)=[];
        sv(n)=[];
        dt(n)=[];
        dv(n)=[];
        pttt(n)=[];
        pttv(n)=[];
        hr(n)=[];
        dr(n)=[];
    end

    %***** save old values
    pa=sv-dv;
    stx=st;
    svx=sv;
    dtx=dt;
    dvx=dv;
    pttvx=pttv;
    r2x=r2;
    drx=dr;
    ptttx=pttt;
    pax=pa;


    %***** Editing loop
    men=1; edval=0;
    if bno|bplotyes
        while men

            if ~bno
                men=0;
            end

            cfig, cfig, cfig
            figure(1)
            clg
            ut=st/srp;
            subplot(5,1,1)
            plot(ut,sv,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            title([subject,'-',int2str(row),'  Edit: click on graph, Menu: lower left, Set missing: upper left, Restore: upper right, OK to quit: lower right'])
            ylabel(['Max=',num2str(rndv(mean(nanrem(sv))))])

            subplot(5,1,2)
            plot(ut,dv,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Min=',num2str(rndv(mean(nanrem(dv))))])

            subplot(5,1,3)
            plot(ut,pa,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['PA=',num2str(rndv(mean(nanrem(pa))))])

            subplot(5,1,4)
            plot(ut,pttv,'c')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['PTT=',num2str(rndv(mean(nanrem(pttv))))])

            subplot(5,1,5)
            plot(ut,hr,'r')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['HR=',num2str(rndv(mean(nanrem(hr))))])

            drawnow

            set(gcf,'units','normal');
            h = axes('units','normal','position',[0 0 1 1],'XLim',[0 1],'YLim',[0 1],'Color','none','XColor',[0 0 0],'YColor',[0 0 0]);
            axes(h);
            if bno
                [h,v,asc]=ginput(1);
                if isempty(v) & isempty(h);
                    v = 0.01;h=0.99;
                    menu = 0;
                    asc=0;
                end

                if (v<0.111 & h<.1)  % left lower
                    m1  = 'Pulse max outliers';
                    m2  = 'Pulse min outliers';
                    m3  = 'Pulse amplitude outliers';
                    m4  = 'Pulse transit time outliers';
                    m5  = '';
                    m6  = 'Synchronize missing values 1-3';
                    m7  = 'Synchronize missing values 1-4';
                    m8  = 'Linear interpolation';
                    m9  = 'Restore original data';
                    m10 = 'Edit raw data (press 0)';
                    men=menu('',m1,m2,m3,m4,m5,m6,m7,m8,m9,m10);
                elseif asc~=1 | (v<0.111 & h>.9)   % right lower
                   men=0;
                else
                  if v>0.9
                    if  h<.1 men=7; end;  % left upper
                    if  h>.9 men=10; end;  % right upper
                    if  h>.1 & h<.366 men=6; end;    % first middle third
                    if  h>.366 & h<.632 men=6; end;  % 2nd middle third
                    if  h>.632 & h<.9 men=6; end;    % 3rd middle third
                  else
                    n=find(v<key);
                    men=length(n);
                  end
                end

                if men==1
                   figure
                   [sv,ind1]=outrect(sv,ut,1);
                   close
                end
                if men==2
                   figure
                   [dv,ind1]=outrect(dv,ut,1);
                   close
                end
                if men==3
                   figure
                   [pa,ind1]=outrect(pa,ut,1);
                   close
                end
                if men==4
                   figure
                   [pttv,ind1]=outrect(pttv,ut,1);
                   close
                end

                if men==6
                   ind=isnan(sv)|isnan(dv)|isnan(pa);
                   n=find(ind);
                   sv(n)=NaN*ones(size(n));
                   dv(n)=NaN*ones(size(n));
                   pa(n)=NaN*ones(size(n));
                end
                if men==7
                   ind=isnan(sv)|isnan(dv)|isnan(pa)|isnan(pttv);
                   n=find(ind);
                   sv(n)=NaN*ones(size(n));
                   dv(n)=NaN*ones(size(n));
                   pa(n)=NaN*ones(size(n));
                   pttv(n)=NaN*ones(size(n));
                end

                if men==8
                   sv=nan_lip(sv);
                   dv=nan_lip(dv);
                   pa=nan_lip(pa);
                   pttv=nan_lip(pttv);
                end

                if men==9
                    st=stx;
                    sv=svx;
                    dt=dtx;
                    dv=dvx;
                    pttv=pttvx;
                    dr=drx;
                    pttt=ptttx;
                end

                if men==10
                   dst=sort([dt;st]);
                   ax=axis;
                   x=[];
                   hold on
                   disp(' ');
                   disp('Click twice to mark suspicious intervals, end with <0>')
                   disp('In EXAM: goto marked intervals with <0>');
                   disp('Delete false maxima and minima with <#>');
                   disp('Insert maxima and minima with <@> or <!>');
                   disp('Mark begin <b> and end <e> of artifact, remove with <->');

                   while 1
                       [i1,i2,i3]=ginput(1);
                       if i3==48
                           break; break; break;
                       end;
                       x=[x i1];
                       plot([i1;i1],[ax(3);ax(4)],'c:');   % display vertical line
                   end;

                   hold off
                   x=x*srp;
                   lookat=reshape(x,2,length(x)/2)';
                   figure(1);clg; clear var1 var2 var3 var4
                   defblank; int=6; moveint=30; samplerate=srp; var1=y; def;
                   event1=dst; event1yes=1;
                   event2=r2;  event2yes=1;
                   evscan=event2; skiploc=.5;
                   yaxisstr='mV';
                   exam;

                   n=reshape(event1,2,length(event1)/2)';
                   dt=n(:,1);
                   st=n(:,2);
                   n=(1:length(st)-1);
                   sv(n)=y(round(st(n)));
                   dv(n)=y(round(dt(n)));
                   pa=sv-dv;

                   % new adjustment of R-wave times and RR intervals for other samplerate
                   %r2=rt/(400/srp);
                   %dr=diff(r2);
                   %rtd=difffit(r2)/200*1000; % IBI

                    % reverse times if dt after st to avoid crash
                    n1=find((st-dt)==0);
                    st(n1)=dt(n1)+mean(st-dt);
                    n2=find((st-dt)<0);
                    i=st(n2); st(n2)=dt(n2); dt(n2)=i;
                    n3=find(pa<0);
                    n=[n1 n2 n3];

                    % PTT from upstroke
                    for i=1:lenr
                        if isnan(dr(i))  xt(i)=NaN; xv(i)=NaN;
                        else
                           [i1,i2]=max(yd(dt(i):st(i)));  % upstroke maximum for PTT
                           xv(i)=i1; xt(i)=i2;
                           xt(i)=xt(i)+dt(i)-1;
                        end; % if
                    end; % for

                    xt(n)=NaN*ones(size(n));
                    sv(n)=NaN*ones(size(n));
                    dv(n)=NaN*ones(size(n));
                    xv(n)=NaN*ones(size(n));
                    pa(n)=NaN*ones(size(n));

                   xt(lenr+1)=NaN;
                   xv(lenr+1)=NaN;
                   pttt=xt; % time of PTT
                   n=find(isnan(r2));
                   pttt(n)=ones(size(n))*NaN;  % missing rwave-times
                   pttv=(pttt-r2)*1000/(srp);

                   if exist('artinterval')
                       if ~isempty(artinterval)
                            disp('Exclusion of values within specified artifact intervals');
                            setmiss=between(st,artinterval);
                            if ~isempty(setmiss)
                                  sv(setmiss)=ones(1,length(setmiss))*NaN;
                                  pa(setmiss)=ones(1,length(setmiss))*NaN;
                                  disp([int2str(setmiss),' values were set to missing.']);
                            end;
                            setmiss=between(dt,artinterval);
                            if ~isempty(setmiss)
                                dv(setmiss)=ones(1,length(setmiss))*NaN;
                            end;
                            setmiss=between(pttt,artinterval);
                            if ~isempty(setmiss)
                                pttv(setmiss)=ones(1,length(setmiss))*NaN;
                            end
                       end
                   end
                   inspectloop=1;
                end
           end %if bno
        end %while men

        %*** Give quality rating
        if ~exist('rateyes');
            rateyes = input('Do you wish to rate signal quality? [1=yes,0=no (default)]');
            if isempty(rateyes);rateyes = 0;end
        end
        if rateyes
            rat=menu('Signal quality rating','1 = even means not reliable','2 = not usable for FFT','3 = problematic for FFT','4 = ok for FFT','5 = excellent');
        else
            rat=0;
        end

    end% if ~bno|bplotyes

end %if fileok


