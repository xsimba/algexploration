function rect = getrect(fig)
%GETRECT Track mouse movement with rubberbanded rectangle.
%	RECT = GETRECT(FIG) tracks the movement of a mouse-drag in
%	the figure FIG.  The mouse movement is tracked with a
%	rubberbanded rectangle.  Returns the selected rectangle,
%	RECT = [left, bottom, width, height] in axes coordinates.

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<1, fig = gcf; end

figure(fig)
curse = get(gcf,'pointer');
set(gcf,'pointer','crosshair');

btndown = get(gcf,'windowbuttondownfcn');
btnup = get(gcf,'windowbuttonupfcn');
btnmotion = get(gcf,'windowbuttonmotionfcn');
set(gcf,'windowbuttondownfcn','','windowbuttonupfcn','', ...
        'windowbuttonmotionfcn','')

waitforbuttonpress;
Pt1 = get(gca,'currentpoint');
rbbox([get(gcf,'currentpoint') 0 0],get(gcf,'currentpoint'))
Pt2 = get(gca,'currentpoint');
rect = [min([Pt1(1,1:2);Pt2(1,1:2)]) abs(Pt2(1,1:2)-Pt1(1,1:2))];

set(gcf,'pointer',curse);
set(gcf,'windowbuttondownfcn',btndown,'windowbuttonupfcn',btnup, ...
        'windowbuttonmotionfcn',btnmotion)
