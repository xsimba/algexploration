function fcall=optargs(FUN,a,b,var);

%		function fcall=optargs(FUN,a,b,var);

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<1, error('Not enough input arguments.'); end
if isstr(FUN) & any(FUN<48), fcall = FUN; return, end

if nargin==1, error('Not enough input arguments.'); end
if nargin==2,
  b = a; a = 1; var = 'x';
elseif nargin==3,
  if isstr(b) | length(b)==0, var = b; b = a; a = 1; else var = 'x'; end
end

% Form call string
if length(var)~=0, comma = ','; else comma = []; end
if b>=a,
  params = [var,comma,'P',int2str(a)];
else
  params = var;
end
for i=a+1:b,
  params = [params,',P',int2str(i)];
end
if length(params)==0, fcall = FUN; else fcall = [FUN,'(',params,')']; end
