% ksetinit.m   set initial values: Q

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


i=1;
while i
disp('Specify the following points:');
m1 = 'R-wave amplitude';
m2 = 'T-wave amplitude';
m3 = 'T-wave beginning and ending';
m4 = 'All four points in successive order';
i=menue(m1,m2,m3,m4);
if ~i break;

else
disp('Click with the mouse on points to specify');
if i==1 [artx(1),arty(1)]=ginput(1);
  artx(1)=artx(1)*scalefact;end;
if i==2 [artx(2),arty(2)]=ginput(1);
  artx(2)=artx(2)*scalefact;end;
if i==3
  [artx(3),arty(3)]=ginput(1);
  artx(3)=artx(3)*scalefact;
  [artx(4),arty(4)]=ginput(1);
  artx(4)=artx(4)*scalefact;
end;
if i==4 [artx,arty]=ginput(4);
  artx=artx*scalefact;end;
end;
end;
artx=round(artx);
arty=round(arty);
rvtyp=arty(1);
tvtyp=arty(2);
tttyp=abs(artx(4)-artx(3));
disp(['R-value = ',int2str(rvtyp)]);
disp(['T-value = ',int2str(tvtyp)]);
disp(['T-value length = ',int2str(round(tttyp*1000/samplerate)),' msec']);
z=999;
