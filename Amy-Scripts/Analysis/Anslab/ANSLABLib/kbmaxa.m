% kbmaxa: find max of breaths automatically: h


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
dist=16; back=12; slope=4; win=6;

newthresh=1;
while newthresh

m1='Find maxima with default thresholds';
m2='Change default thresholds';
newthresh=menue(m1,m2);
if ~newthresh break; break; end;

if newthresh==2
dist=input('Minimal distance between maxima [8] ==> ');
if isempty(dist) dist=8; end;
back=input('Comparison to a point back [6] ==> ');
if isempty(back) back=6; end;
slope=input('Slope from there at least to be valid [2] ==> ');
if isempty(slope) slope=2; end;
win=input('Window for search of maxima in other channels [6] ==> ');
if isempty(win) win=6; end
end;

xmax1=[]; xmax2=[];
xmin1=[]; xmin2=[]; xmin3=[];
mini=[];bm=[]; bmin=[];

x3=var3(s1:s2);
xmax3=findmax(x3,dist,back,slope)';  %spirometer maxima
x1=var1(s1:s2); x2=var2(s1:s2);

for i=1:length(xmax3)
  n=xmax3(i)-win:xmax3(i)+win+2;
  n(n<1)=[]; n(n>length(x3))=[];  % range check
  [j,xmax1(i)] = min(x1(n));
  [j,xmax2(i)] = min(x2(n));
end;

xmax1=xmax3-win-1+xmax1;
xmax2=xmax3-win-1+xmax2;


%*** find between minima

i1=xmax3;  % max index

for i=1:length(i1)-1
    n= i1(i):i1(i+1);
    [j,xmin1(i)] = max(x1(n));
    [j,xmin2(i)] = max(x2(n));
    [mini(i),xmin3(i)] = min(x3(n));
end;

i1(length(i1))=[];
xmin1=i1+xmin1-1;
xmin2=i1+xmin2-1;
xmin3=i1+xmin3-1;

%** last minima not valid for breath
xmax1(length(xmax1))=[];
xmax2(length(xmax2))=[];
xmax3(length(xmax3))=[];

out=1;
while out

%*** build maxima/minima-vector, row 4 is absolute index
bmin  =[ col(x1(xmax1))' ; col(x2(xmax2))' ; col(-x3(xmax3))' ; col((xmax3+s1-1))' ];
bm=[ col(x1(xmin1))' ; col(x2(xmin2))' ; col(-x3(xmin3))' ; col((xmin3+s1-1))' ];

%*** plot found maxima and minima
figure(5)
t=1:length(x1);
subplot(2,1,1)
plot(t,x1,t,x2)
title(['Respitrace units: thorax (y), abdomen (m) -  ',int2str(length(xmax1)),' valid inspirations']);
hold on
plot(xmax1,x1(xmax1),'oy');
plot(xmax2,x2(xmax2),'om');
plot(xmin1,x1(xmin1),'oy');
plot(xmin2,x2(xmin2),'om');
hold off
subplot(2,1,2)
plot(t,-x3);
title('Spirometer tidal volume [ml]');
hold on
plot(xmax3,-x3(xmax3),'oc');
plot(xmin3,-x3(xmin3),'oc');
% number the breathing cycles on plot
for i=1:5:length(xmax3)
str=int2str(i);
cmdstr=['text(''Position'',[xmax3(i) -max(x3)-50],''String'','' ',str,''')'];
eval(cmdstr);
end;
hold off

out=input('Take out cycle no. [end] ==> ');
if isempty(out) | ~out
   out=0; break; break;
else
   xmax1(out)=[]; xmax2(out)=[]; xmax3(out)=[];
   xmin1(out)=[]; xmin2(out)=[]; xmin3(out)=[]; mini(out)=[];
end;

end;  %while out

end;  %while win

BM=[BM bm];
BMIN=[BMIN bmin];

x1=var1; x2=var2; x3=var3;
xmax1=BM(4,:);
xmin1=BMIN(4,:);

% plot all taken cyles before quit
figure(5)
clg
len=length(xmax1);
t=1:length(var1);
subplot(2,1,1)
plot(t,var1,t,var2)
title(['Respitrace units: thorax (y), abdomen (m) -  ',int2str(len),' valid inspirations']);
hold on
plot(xmax1,x1(xmax1),'oy');
plot(xmax1,x2(xmax1),'om');
plot(xmin1,x1(xmin1),'oy');
plot(xmin1,x2(xmin1),'om');
hold off
subplot(2,1,2)
plot(t,-x3);
title('Spirometer tidal volume [ml]');
hold on
plot(xmax3,-x3(xmax3),'oc');
plot(xmin3,-x3(xmin3),'oc');
% number the breathing cycles on plot
for i=1:5:length(xmax3)
str=int2str(i);
cmdstr=['text(''Position'',[xmax3(i) -max(x3)-50],''String'','' ',str,''')'];
eval(cmdstr);
end;
hold off

end;
drawnow
