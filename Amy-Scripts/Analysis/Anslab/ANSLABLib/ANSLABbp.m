%   ANSLABbp

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



clear
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultTextColor',[1 1 1])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end

%*** Settings
if ~exist('data_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
    red_dir1=[data_dir,'\f'];   %
    i_dir=[data_dir,'\i'];
end


cd(data_dir);
[filename, filedir] = uigetfile('*.txt','Select data file')
if isequal(data_dir,0);return;end
eval(['load ',filename,';']);
l=length(filename);
varfilename = filename(1:end-4);
varname=filename(1:8);
studystr=varname(1:3);
subjn=str2num(varname(4:6));
filenum=str2num(varname(7:8));
subject=twostr(subjn,3);
file=[subject,twostr(filenum,2),'i'];
eval(['FP =',varfilename,';']);
eval(['clear ',varfilename]);


[FP,SR] = AskResampleData(FP,400,'PPG');


% %*** Settings
% data_dir='c:\biodata\aul';
%
% %*** Load PPG data
chd(data_dir)
y=FP;

%*** Resampling
ecglen=length(y);
%EC=resample(y,2,5);

%*** PPG analysis settings (program FINDRTQ5)
upsrfacecg=1;  % (1,3) Upsampling factor that was used for ECG analysis
ptt_is_max=0;  %   (0) PTT based on maximum (1) or based on steepest upstroke (0, more robust)
switch1=0;     %   (0) Removal of spikes
switch3=1;     %   (1) 20 Hz low pass filtering
switch4=0;     %   (1) 0.13 Hz high pass filtering
switch5=0;     %   (1) 0.30 Hz high pass filtering
int1=0.1;      % (0.1) Fraction of RR interval where search for max and min begins
int2=0.7;      % (0.7) Fraction of RR interval where search for max and min ends
hradj=100;     % (100) Adjustment of right border of interval for HRs higher than 'hradj'
hradd=0.3;     % (0.3) Fraction of RR interval to add for these higher HRs
dist=50;       %  (50) Check slope: 'dist' points from max back (and min forward)
slope=.05;     %(0.05)               needs to be a decrease (increase) of slope units


%*** Additional initial settings
lucas_flag=0;
delsampend=0;
byes=0;
row=1;


%*** PPG detection
findfp5

if ~fileok
    return
end

%*** Resample PWA and PTT to instantaneous 10 Hz
ep=10;
disp('Conversion of bp parameters into 0.10 sec epochs');

sv_0=nan_lip(epoch(st,sv,(sr/ep)))';
dv_0=nan_lip(epoch(st,dv,(sr/ep)))';
pa_0=nan_lip(epoch(st,pa,(sr/ep)))';
ptt0=epoch(pttt,pttv,(sr/ep));
ptt0=nan_lip(nanfill(ptt0,length(pa_0)))';
t=(1:length(pa_0))/ep;
t=t(:);

%*** Plot
cfig
figure(1)
subplot(4,1,1)
plot(t,sv_0);
title('Instantaneous systolic arterial pressure amplitude');
ylabel('mm Hg')
subplot(4,1,2)
plot(t,dv_0);
title('Instantaneous diastolic arterial pressure amplitude');
ylabel('mm Hg')
subplot(4,1,3)
plot(t,pa_0);
title('Instantaneous pulse pressure amplitude');
ylabel('mm Hg')
subplot(4,1,4)
plot(t,ptt0);
title('Instantaneous arterial pressure transit time');
ylabel('msec')
xlabel('sec')


%*** Save reduced data
chd(red_dir1)
S=[sv_0 dv_0 pa_0 ptt0];
disp(['save ',varname,'f.txt -ascii -tabs S']);
eval(['save ',varname,'f.txt -ascii -tabs S']);

disp(num2str(mean(sv_0)));
disp(num2str(mean(dv_0)));
disp(num2str(mean(pa_0)));
disp(num2str(mean(ptt0)));