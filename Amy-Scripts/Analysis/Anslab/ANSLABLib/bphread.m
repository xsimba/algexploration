function [Channels,SR] = bphread(ReadFilePath)

%   bpread

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

FTellStatus = 0;
AllPrintStatus = 0;
PrintStatus = 0;
PlotStatus = 0;
ExtractChannel = 'none';


%   ChLabelCell = bphread(FilePath)
%
%   ReadBP reads in Biopac data and returns channel label information

%   Peter Peyk
%   University of Basel, Switzerland.


MVersionString = version;
MVersion = str2num(MVersionString(1:3));
ExtChanInd = [];

if isstr(ExtractChannel)
	if strcmp(ExtractChannel,'all')
        MultChanStatus = 1;
    elseif strcmp(ExtractChannel,'none')
        MultChanStatus = 0;
    else
        MultChanStatus = 0;
	end
else
    if length(ExtractChannel)>1
        MultChanStatus = 1;
        ExtChanInd = ExtractChannel;
    else
        MultChanStatus = 0;
        ExtChanInd = ExtractChannel;
    end
end

if exist('PrintLabelStatus')
    if isempty(PrintLabelStatus)
        PrintLabelStatus = 0;
    end
else
    PrintLabelStatus = 0;
end

%initialize header variables
nChannels = 0;
lBufLength = 0;
szCommentText = 0;
dSampleTime = 0;
DATAnItemHeaderLen = 0;
lVersion = 0;
lExtItemHeaderLen = 0;
nHorizAxisType = 0;
nCurChannel = 0;
dTimeOffset = 0;
dTimeScale = 0;
dTimeCursor1 = 0;
dTimeCursor2 = 0;
rcWindow = 0;
nMeasurement = 0;
fHilite = 0;
dFirstTimeOffset = 0;
nRescale = 0;
szHorizUnits1 = 0;
szHorizUnits2 = 0;
nInMemory = 0;
fGrid = 0;
fMarkers = 0;
nPlotDraft = 0;
nDispMode = 0;
nReserved = 0;
BShowToolBar = 0;
BShowChannelButtons = 0;
BShowMeasurements = 0;
BShowMarkers = 0;
BShowJournal = 0;
CurXChannel = 0;
MmtPrecision = 0;
NMeasurementRows = 0;
mmt = 0;
mmtChan = 0;
MmtCalcOpnd1 = 0;
MmtCalcOpnd2 = 0;
MmtCalcOp = 0;
MmtCalcConstant = 0;
bNewGridwithMinor = 0;
colorMajorGrid = 0;
colorMinorGrid = 0;
wMajorGridStyle = 0;
wMinorGridStyle = 0;
wMajorGridWidth = 0;
wMinorGridWidth = 0;
bFixedUnitsDiv = 0;
bMid_Range_Show = 0;
dStart_Middle_Point = 0;
dOffset_Point = 0;
hGrid = 0;
vGrid = 0;
bEnableWaveTools = 0;
horizPrecision = 0;
RESERVED = 0;
bOverlapMode = 0;
bShowHardware = 0;
bXAutoPlot = 0;
bXAutoScroll = 0;
bStartButtonVisible = 0;
bCompressed = 0;
bAlwaysStartButtonVisible = 0;
lChanHeaderLen = 0;
nNum = 0;
rgbColor = 0;
nDispChan = 0;
dVoltOffset = 0;
dVoltScale = 0;
szUnitsText = 0;
dAmplScale = 0;
dAmplOffset = 0;
nChanOrder = 0;
nDispSize = 0;
plotMode = 0;
vMid = 0;
szDescription = 0;
nVarSampleDivider = 0;
vertPrecision = 0;
nLength = 0;
nID = 0;
ByForeignData = 0;
nSize = 0;
nType = 0;
lLength = 0;
lMarkers = 0;
lSample = 0;
fSelected = 0;
fTextLocked = 0;
fPositionLocked = 0;
nTextLength = 0;
szText = 0;



disp('start reading biopac channel labels...');
disp(ReadFilePath);
fid = fopen(ReadFilePath,'rb','l');




%Readin without structures and cell (Matlab<6)
DATA = [];

% 	Item                Type    Size    Offset  Description
%=============================================================================
%=============================================================================
%=============================================================================

% 	nItemHeaderLen 	    short 	2 	    0 	Not currently used.
nItemHeaderLen = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nItemHeaderLen = ',num2str(nItemHeaderLen),' ...\n']);
end


% 	lVersion 	        long 	4 	    2 	File version identifier:
%
% 						30 = Pre-version 2.0
% 						31 = Version 2.0 Beta 1
% 						32 = Version 2.0 release
% 						33 = Version 2.0.7 (Mac)
% 						34 = Version 3.0 In-house Release 1
% 						35 = Version 3.03
% 						36 = version 3.5x (Win 95, 98, NT)
% 						37 = version of BSL/PRO 3.6.x
% 						38 = version of Acq 3.7.0-3.7.2 (Win 98, 98SE, NT, Me, 2000)
% 						39 = version of Acq 3.7.3 or above (Win 98, 98SE, 2000, Me, XP)
% 						41 = version of Acq 3.8.1 or above (Win 98, 98SE, 2000, Me, XP)
% 						42 = version of BSL/PRO 3.7.X or above (Win 98, 98SE, 2000, Me, XP)


Offset = 2;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
lVersion = fread(fid,1,'int32');
if AllPrintStatus
    fprintf(1,['lVersion = ',num2str(lVersion),' ...\n']);
end


% 	lExtItemHeaderLen   long    4 	    6 	Extended item header length.
Offset = 6;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
lExtItemHeaderLen = fread(fid,1,'int32');
if AllPrintStatus
    fprintf(1,['lExtItemHeaderLen = ',num2str(lExtItemHeaderLen),' ...\n']);
end

% 	nChannels           short   2 	    10 	Number of channels stored.
Offset = 10;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nChannels = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nChannels = ',num2str(nChannels),' ...\n']);
end

%   nHorizAxisType      short   2       12  Horizontal scale type, one of the  following
% 			0 = Time in seconds
% 			1 = Time in HMS format
% 			2 = Frequency
% 			3 = Arbitrary
Offset = 12;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nHorizAxisType = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nHorizAxisType = ',num2str(nHorizAxisType),' ...\n']);
end

% 	nCurChannel         short   2 	    14 	Currently selected channel.
Offset = 14;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nCurChannel = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nCurChannel = ',num2str(nCurChannel),' ...\n']);
end

% 	dSampleTime         double  8 	    16 	The number of milliseconds per sample.
Offset = 16;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
dSampleTime = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dSampleTime = ',num2str(dSampleTime),' ...\n']);
end

% 	dTimeOffset         double  8 	    24 	The initial time offset in milliseconds.
Offset = 24;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
dTimeOffset = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeOffset = ',num2str(dTimeOffset),' ...\n']);
end

% 	dTimeScale          double  8 	    32 	The time scale in milliseconds per division.
Offset = 32;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
dTimeScale = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeScale = ',num2str(dTimeScale),' ...\n']);
end

% 	dTimeCursor1        double  8 	    40 	Cursor 1 time position in milliseconds.
Offset = 40;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
dTimeCursor1 = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeCursor1 = ',num2str(dTimeCursor1),' ...\n']);
end

% 	dTimeCursor2        double  8 	    48 	Cursor 2 time position in milliseconds.
Offset = 48;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
dTimeCursor2 = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeCursor2 = ',num2str(dTimeCursor2),' ...\n']);
end

% 	rcWindow            RECT    8 	    56 	The chart's size and position relative to the AcqKnowledge client area. When each
%                                           RECT field is set to 0,  the chart is displayed with default a size and position.
Offset = 56;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
rcWindow = fread(fid,[1 4],'short');
if AllPrintStatus
    fprintf(1,['rcWindow = ',num2str(rcWindow),' ...\n']);
end


% 	nMeasurement[6]   	short   6*2 	64 	Describes the currently selected measurements, one of the following:
%
% 							0 = No measurement
% 							1 = Value Absolute voltage
%                           2 = Delta Voltage difference
% 							3 = Peak to peak voltage
% 							4 = Maximum voltage
% 							5 = Minimum voltage
% 							6 = Mean voltage
% 							7 = Standard deviation
% 							8 = Integral
% 							9 = Area
% 							10 = Slope
% 							11 = LinReg
% 							13 = Median
% 							15 = Time
%                           16 = Delta Time
% 							17 = Freq
% 							18 = BPM
% 							19 = Samples
% 							20 = Delta Samples
% 							21 = Time of Median
% 							22 = Time of Max
% 							23 = Time of Min
% 							25 = Calculation
% 							26 = Correlation
Offset = 64;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nMeasurement = fread(fid,[1 6],'short');
if AllPrintStatus
    fprintf(1,['nMeasurement = ',num2str(nMeasurement),' ...\n']);
end

% 	fHilite             BOOL     2 	 76 	Gray non-selected waveforms:
% 							0 = Don't gray
%                           1 = Gray.
Offset = 76;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fHilite = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['fHilite = ',num2str(fHilite),' ...\n']);
end


% 	dFirstTimeOffset    double          	8 	78 	 Initial time offset in milliseconds.
Offset = 78;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
dFirstTimeOffset = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dFirstTimeOffset = ',num2str(dFirstTimeOffset),' ...\n']);
end

% 	nRescale            short             	2 	86 	Autoscale after transforms:
% 					        0 = Don't autoscale
% 					        1 = Autoscale.
Offset = 86;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nRescale = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nRescale = ',num2str(nRescale),' ...\n']);
end

% 	szHorizUnits1       char      40 	88 	Horizontal units text.
Offset = 88;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
szHorizUnits1 = fread(fid,[1 40],'char');
if AllPrintStatus
    fprintf(1,['szHorizUnits1 = ',num2str(szHorizUnits1),' ...\n']);
end

% 	szHorizUnits2       char      10 	128 	Horizontal units text (abbreviated).
Offset = 128;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
szHorizUnits2 = fread(fid,[1 10],'char');
if AllPrintStatus
    fprintf(1,['szHorizUnits2 = ',num2str(szHorizUnits2),' ...\n']);
end

% 	nInMemory           short             	2 	138 	Keep data file in memory:
% 						0 = Keep
% 						1 = Don't keep.
Offset = 138;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nInMemory = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nInMemory = ',num2str(nInMemory),' ...\n']);
end

% 	fGrid               BOOL      2 	140 	Enable grid display.
Offset = 140;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fGrid = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['fGrid = ',num2str(fGrid),' ...\n']);
end

% 	fMarkers            BOOL      2 	142 	Enable marker display.
Offset = 142;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fMarkers = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['fMarkers = ',num2str(fMarkers),' ...\n']);
end

% 	nPlotDraft          short     2 	144 	Enable draft plotting.
Offset = 144;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nPlotDraft = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nPlotDraft = ',num2str(nPlotDraft),' ...\n']);
end

% 	nDispMode           short     2 	146 	Display mode:
% 					0 = Scope
% 					1 = Chart.
Offset = 146;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nDispMode = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nDispMode = ',num2str(nDispMode),' ...\n']);
end

% 	nReserved           short     2 	148 	Reserved.
Offset = 148;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
nReserved = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nReserved = ',num2str(nReserved),' ...\n']);
end

if lVersion >= 34
	%   Version 3.0 and above ...
	% 	Item                    Type       Size     Offset  Description
	% 	BShowToolBar            short      2 	    150
	Offset = 150;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	BShowToolBar = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowToolBar = ',num2str(BShowToolBar),' ...\n']);
	end

	% 	BShowChannelButtons     short      2 	    152
	Offset = 152;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	BShowChannelButtons = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowChannelButtons = ',num2str(BShowChannelButtons),' ...\n']);
	end

	% 	BShowMeasurements       short      2 	    154
	Offset = 154;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	BShowMeasurements = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowMeasurements = ',num2str(BShowMeasurements),' ...\n']);
	end

	% 	BShowMarkers            short      2 	    156
	Offset = 156;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	BShowMarkers = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowMarkers = ',num2str(BShowMarkers),' ...\n']);
	end

	% 	BShowJournal            short      2 	    158
	Offset = 158;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	BShowJournal = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowJournal = ',num2str(BShowJournal),' ...\n']);
	end

	% 	CurXChannel             short      2 	    160
	Offset = 160;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	CurXChannel = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['CurXChannel = ',num2str(CurXChannel),' ...\n']);
	end

	% 	MmtPrecision            short      2 	    162
	Offset = 162;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	MmtPrecision = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['MmtPrecision = ',num2str(MmtPrecision),' ...\n']);
	end

    if lVersion >=35

		%   Version 3.02 and above ...
		% 	Item                    Type        Size    Offset  Description
		% 	NMeasurementRows        short 	    2 	    164 	Number of measurement rows
		Offset = 164;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		NMeasurementRows = fread(fid,1,'short');
		if AllPrintStatus
            fprintf(1,['NMeasurementRows = ',num2str(NMeasurementRows),' ...\n']);
		end

		% 	mmt[40]  	            short 	    2 * 40  166 	Measurement functions
		Offset = 166;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		mmt = fread(fid,[1 40],'short');
		if AllPrintStatus
            fprintf(1,['mmt = ',num2str(mmt),' ...\n']);
		end

		% 	mmtChan[40]   	        short 	    2 * 40  246 	Measurement channels
		Offset = 246;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		mmtChan = fread(fid,[1 40],'short');
		if AllPrintStatus
            fprintf(1,['mmtChan = ',num2str(mmtChan),' ...\n']);
		end

        if lVersion >= 36
			%   Version 3.5x and above ...
			% 	Item                    Type        Size    Offset  Description
			% 	MmtCalcOpnd1 	        short 	    2 * 40  326 	Measurement, Calculation - Operand 1
			Offset = 326;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			MmtCalcOpnd1 = fread(fid,[1 40],'short');
			if AllPrintStatus
                fprintf(1,['MmtCalcOpnd1 = ',num2str(MmtCalcOpnd1),' ...\n']);
			end

			% 	MmtCalcOpnd2  	        short 	    2 * 40  406 	Measurement, Calculation - Operand 2
			Offset = 406;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			MmtCalcOpnd2 = fread(fid,[1 40],'short');
			if AllPrintStatus
                fprintf(1,['MmtCalcOpnd2 = ',num2str(MmtCalcOpnd2),' ...\n']);
			end

			% 	MmtCalcOp   	        short 	    2 * 40  486 	Measurement, Calculation - Operation
			Offset = 486;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			MmtCalcOp = fread(fid,[1 40],'short');
			if AllPrintStatus
                fprintf(1,['MmtCalcOp = ',num2str(MmtCalcOp),' ...\n']);
			end

			% 	MmtCalcConstant   	    double 	    8 * 40  566 	Measurement, Calculation - Constant
			Offset = 566;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			MmtCalcConstant = fread(fid,[1 40],'double');
			if AllPrintStatus
                fprintf(1,['MmtCalcConstant = ',num2str(MmtCalcConstant),' ...\n']);
			end

			%                          Version 3.7.0 and above ...
			% 	bNewGridwithMinor 	BOOL       886 	    4 	New Grid with minor line
			Offset = 886;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			bNewGridwithMinor = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bNewGridwithMinor = ',num2str(bNewGridwithMinor),' ...\n']);
			end

			% 	colorMajorGrid 	    long       890 	    4 	COLORREF
			Offset = 890;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			colorMajorGrid = fread(fid,[1 4],'char');
			if AllPrintStatus
                fprintf(1,['colorMajorGrid (ARGB) = ',num2str(colorMajorGrid),' ...\n']);
			end

			% 	colorMinorGrid 	    long       894 	    4 	COLORREF
			Offset = 894;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			colorMinorGrid = fread(fid,[1 4],'char');
			if AllPrintStatus
                fprintf(1,['colorMinorGrid  (ARGB) = ',num2str(colorMinorGrid),' ...\n']);
			end

			% 	wMajorGridStyle     short      898 	    2 	PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT,  PS_DASHDOTDOT
			Offset = 898;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			wMajorGridStyle = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMajorGridStyle = ',num2str(wMajorGridStyle),' ...\n']);
			end

			% 	wMinorGridStyle     short      900 	    2 	PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT
			Offset = 900;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			wMinorGridStyle = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMinorGridStyle = ',num2str(wMinorGridStyle),' ...\n']);
			end


			% 	wMajorGridWidth     short      902 	    2 	width of line in Pixels
			Offset = 902;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			wMajorGridWidth = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMajorGridWidth = ',num2str(wMajorGridWidth),' ...\n']);
			end

			% 	wMinorGridWidth     short      904 	    2 	width of line in Pixels
			Offset = 904;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			wMinorGridWidth = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMinorGridWidth = ',num2str(wMinorGridWidth),' ...\n']);
			end

			% 	bFixedUnitsDiv      BOOL       906 	    4 	Locked/Unlocked grid lines
			Offset = 906;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			bFixedUnitsDiv = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bFixedUnitsDiv = ',num2str(bFixedUnitsDiv),' ...\n']);
			end

			% 	bMid_Range_Show     BOOL       910 	    4 	show gridlines as MidPoint and Range
			Offset = 910;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			bMid_Range_Show = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bMid_Range_Show = ',num2str(bMid_Range_Show),' ...\n']);
			end

			% 	dStart_Middle_Point double     914 	    8 	Startpoint to draw grid
			Offset = 914;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			dStart_Middle_Point = fread(fid,1,'double');
			if AllPrintStatus
                fprintf(1,['dStart_Middle_Point = ',num2str(dStart_Middle_Point),' ...\n']);
			end

			% 	dOffset_Point       double     922 	    8 * 60 	Offset of VERTICAL value per channel
			Offset = 922;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			dOffset_Point = fread(fid,[1 60],'double');
			if AllPrintStatus
                fprintf(1,['dOffset_Point = ',num2str(dOffset_Point),' ...\n']);
			end

			% 	hGrid               double     1402 	8 	Horizontal grid spacing
			Offset = 1402;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			hGrid = fread(fid,1,'double');
			if AllPrintStatus
                fprintf(1,['hGrid = ',num2str(hGrid),' ...\n']);
			end

			% 	vGrid               double     1410 	8 * 60 	Vertical grid spacing per channel
			Offset = 1410;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			vGrid = fread(fid,[1 60],'double');
			if AllPrintStatus
                fprintf(1,['vGrid = ',num2str(vGrid),' ...\n']);
			end


			% 	bEnableWaveTools    BOOL       1890 	4 	Enable Wavetools during acquisition
			Offset = 1890;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			bEnableWaveTools = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bEnableWaveTools = ',num2str(bEnableWaveTools),' ...\n']);
			end

            if lVersion >= 39
				%  Version 3.7.3 and above ...
				%   Item               Type       Offset    Size   Description
				%   horizPrecision     short  	  1894 	    2 	    digits of precision for units in Horizontal Axis
				Offset = 1894;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
				horizPrecision = fread(fid,1,'short');
				if AllPrintStatus
                    fprintf(1,['horizPrecision = ',num2str(horizPrecision),' ...\n']);
				end

                if lVersion >= 40

					% Version 3.8.1 and above ...
					% Item                      Type    Offset  Size   Description
					% RESERVED  	            byte 	1896 	20 	    RESERVED
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					RESERVED = fread(fid,[1 20],'char');
					if AllPrintStatus
                        fprintf(1,['RESERVED = ',num2str(horizPrecision),' ...\n']);
					end

					% bOverlapMode  	        BOOL 	1916 	4 	    Overlap Mode
					Offset = 1916;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bOverlapMode = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bOverlapMode = ',num2str(bOverlapMode),' ...\n']);
					end

					% bShowHardware  	        BOOL 	1920 	4 	    Hardware visibility
					Offset = 1920;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bShowHardware = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bShowHardware = ',num2str(bShowHardware),' ...\n']);
					end

					% bXAutoPlot  	            BOOL 	1924 	4 	    Autoplot during acquisition
					Offset = 1924;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bXAutoPlot = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bXAutoPlot = ',num2str(bXAutoPlot),' ...\n']);
					end

					% bXAutoScroll  	        BOOL 	1928 	4 	    Autoscroll during acquisition
					Offset = 1928;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bXAutoScroll = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bXAutoScroll = ',num2str(bXAutoScroll),' ...\n']);
					end

					% bStartButtonVisible  	    BOOL 	1932 	4 	    Start button visibility
					Offset = 1932;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bStartButtonVisible = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bStartButtonVisible = ',num2str(bStartButtonVisible),' ...\n']);
					end

					% bCompressed  	            BOOL 	1936 	4 	    The file is compressed
					Offset = 1936;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bCompressed = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bCompressed = ',num2str(bCompressed),' ...\n']);
					end

					% bAlwaysStartButtonVisible BOOL 	1940 	4 	    Always show start button
					Offset = 1940;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					bAlwaysStartButtonVisible = fread(fid,1,'int32');
					if AllPrintStatus
                        fprintf(1,['bAlwaysStartButtonVisible = ',num2str(bAlwaysStartButtonVisible),' ...\n']);
					end
                end
            end
        end
    end
end



StartPerChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['StartPerChannelData = %g ...\n'],StartPerChannelData);
end
CumPerChannel = 0;
MarkerChan = [];
TriggerChan = [];
if PrintStatus
    disp('reading header data for channels...');
end



szCommentText = zeros(nChannels,40);
rgbColor = zeros(nChannels,4);
szUnitsText = zeros(nChannels,20);
szDescription = zeros(nChannels,128);

% 	Per Channel Data Section...
for ChanInd = 1: nChannels

    if  AllPrintStatus
            fprintf(1,'\n');
            fprintf(1,['reading header data for channel ',num2str(ChanInd),' of ',num2str(nChannels),' total channels...\n']);
            fprintf(1,['--------------------------------------------------------------------------\n']);
    end

	% 	Item               Type       Offset    Size    Description
	% 	lChanHeaderLen     long       0 	    4 	    Length of channel header.
    Offset = 0 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	lChanHeaderLen(ChanInd) = fread(fid,1,'long');
	if AllPrintStatus
        fprintf(1,['\tlChanHeaderLen{',num2str(ChanInd),') = ',num2str(lChanHeaderLen(ChanInd)),' ...\n']);
	end

	% 	nNum               short      4 	    2 	 Channel number.
    Offset = 4 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	nNum(ChanInd) = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnNum{',num2str(ChanInd),') = ',num2str(nNum(ChanInd)),' ...\n']);
    end


	% 	szCommentText      char       6 	    40 	Comment text.
    Offset = 6 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	szCommentText(ChanInd,:) = fread(fid,[1 40],'char');
	if AllPrintStatus
        fprintf(1,['\tszCommentText{',num2str(ChanInd),') = ']);
        fprintf(1,'%c',setstr(szCommentText(ChanInd,:)));
        fprintf(1,'\n');
    end

    if isstr(ExtractChannel) & ~strcmp(ExtractChannel,'all') & ~strcmp(ExtractChannel,'none')
        if strcmp(deblank(setstr(szCommentText(ChanInd,:))),ExtractChannel)
            if isempty(ExtChanInd);
                ExtChanInd = ChanInd;
            else
                ExtChanInd = [ExtChanInd ChanInd];
            end
        end
    elseif strcmp(ExtractChannel,'all')
        if isempty(ExtChanInd);
            ExtChanInd = ChanInd;
        else
            ExtChanInd = [ExtChanInd ChanInd];
        end
    end


    if ~isempty(findstr(szCommentText(ChanInd,:),'Marker'))
        MarkerChan = ChanInd;
    end
    if ~isempty(findstr(szCommentText(ChanInd,:),'Trigger'))
        TriggerChan = ChanInd;
    end


	% 	rgbColor           RGB        46 	    4 	 Color.
    Offset = 46 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	rgbColor(ChanInd,:) = fread(fid,[1 4],'char');
	if AllPrintStatus
        fprintf(1,['\trgbColor(',num2str(ChanInd),') (ARGB) = ',num2str(rgbColor(ChanInd,:)),' ...\n']);
	end


	% 	nDispChan          short      50 	    2 	Display option.
    Offset = 50 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	nDispChan(ChanInd) = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnDispChan(',num2str(ChanInd),') = ',num2str(nDispChan(ChanInd)),' ...\n']);
	end


	% 	dVoltOffset        double     52 	    8 	Amplitude offset (volts).
    Offset = 52 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	dVoltOffset(ChanInd) = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdVoltOffset(',num2str(ChanInd),') = ',num2str(dVoltOffset(ChanInd)),' ...\n']);
	end


	% 	dVoltScale         double     60 	    8 	Amplitude scale (volts/div).
    Offset = 60 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	dVoltScale(ChanInd) = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdVoltScale(',num2str(ChanInd),') = ',num2str(dVoltScale(ChanInd)),' ...\n']);
	end

	% 	szUnitsText        char       68 	    20 	Units text.
    Offset = 68 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	szUnitsText(ChanInd,:) = fread(fid,[1 20],'char');
	if AllPrintStatus
        fprintf(1,['\tszUnitsText(',num2str(ChanInd),') = ',szUnitsText(ChanInd,:)]);
        fprintf(1,'\n');
	end

	% 	lBufLength         long       88 	    4 	Number of data samples.
    Offset = 88 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	lBufLength(ChanInd) = fread(fid,1,'int32');
	if AllPrintStatus
        fprintf(1,['\tlBufLength(',num2str(ChanInd),') = ',num2str(lBufLength(ChanInd)),' ...\n']);
	end

	% 	dAmplScale         double     92 	    8 	Units/count.
    Offset = 92 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	dAmplScale(ChanInd) = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdAmplScale(',num2str(ChanInd),') = ',num2str(dAmplScale(ChanInd)),' ...\n']);
	end

	% 	dAmplOffset        double     100 	    8 	Units
    Offset = 100 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	dAmplOffset(ChanInd) = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdAmplOffset(',num2str(ChanInd),') = ',num2str(dAmplOffset(ChanInd)),' ...\n']);
	end

	% 	nChanOrder         short      108 	    2 	Displayed channel order.
    Offset = 108 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	nChanOrder(ChanInd) = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnChanOrder(',num2str(ChanInd),') = ',num2str(nChanOrder(ChanInd)),' ...\n']);
    end

	% 	nDispSize          short      110 	    2 	Channel partition size.
    Offset = 110 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	nDispSize(ChanInd) = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnDispSize(',num2str(ChanInd),') = ',num2str(nDispSize(ChanInd)),' ...\n']);
	end
    StepSize = 112;

    if lVersion >= 34
        %   Version 3.0 and above ...
		% 	Item              Type       Offset     Size  Description
		% 	plotMode          short      112 	    2
        Offset = 112 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		plotMode(ChanInd) = fread(fid,1,'short');
		if AllPrintStatus
            fprintf(1,['\tplotMode(',num2str(ChanInd),') = ',num2str(plotMode(ChanInd)),' ...\n']);
		end

		% 	vMid              double     114 	    8
        Offset = 114 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		vMid(ChanInd) = fread(fid,1,'double');
		if AllPrintStatus
            fprintf(1,['\tnvMid(',num2str(ChanInd),') = ',num2str(vMid(ChanInd)),' ...\n']);
		end
        StepSize = 122;


        if lVersion >= 38

			%   Version 3.7.0 and above ...
			% 	Item                Type      Offset    Size    Description
			% 	szDescription       char      122 	    128 	String of Channel description
            Offset = 122 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			szDescription(ChanInd,:) = fread(fid,[1 128],'char');
			if AllPrintStatus
                fprintf(1,['\tszDescription(',num2str(ChanInd),') = ']);
                fprintf(1,'%c',setstr(szDescription(ChanInd,:)));
                fprintf(1,'\n');
			end

			% 	nVarSampleDivider   short     250 	    2 	Channel divider of main frequency
            Offset = 250 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			nVarSampleDivider(ChanInd) = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['\tnVarSampleDivider(',num2str(ChanInd),') = ',num2str(nVarSampleDivider(ChanInd)),' ...\n']);
            end

            StepSize = 252;


            if lVersion >= 39
                %     tem              Type      Offset     Size    Description
                %   vertPrecision 	   short  	  252 	    2 	    digits of precision for units in Vertical Axis for each channel
                Offset = 252 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
                vertPrecision(ChanInd) = fread(fid,1,'short');
                if AllPrintStatus
                    fprintf(1,['\tvertPrecision(',num2str(ChanInd),') = ',num2str(vertPrecision(ChanInd)),' ...\n']);
                end
                StepSize = 254;
            end
        end
    end

    CumPerChannel = CumPerChannel + StepSize;

end


fclose(fid);
disp('end reading biopac channel lables...');

SR =  1000/dSampleTime;
Channels = setstr(szCommentText);


return





