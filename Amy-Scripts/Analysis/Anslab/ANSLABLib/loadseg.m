function DATA = loadseg(FilePath,ExtractChannel,MarkerChannel,SegmentMarkerVal,SamplingRate,NumberStatus,SegmentNumber,t1,t2);

%
%   SegmentNumber
%   NumberStatus
%   SamplingRate
%   SegmentMarkerVal
%   MarkerChannel
%   ExtractChannel
%   FilePath



%   [Data] = LoadBPSegment(FilePath,ExtractChannel,MarkerChannel,SegmentMarkerVal,SamplingRate)
%
%   LoadBPSegment reads data from the channel EXTRACTCHANNEL of an *.ACQ-file
%   corresponding the intervall for which MARKERCHANNEL has the value SEGMENTMARKERVAL.
%   If SAMPLINGRATE is given, the data will be resampled according to its
%   value.



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<9;t2 = [];end
if nargin<8;t1 = [];end
if nargin<7;SegmentNumber = [];end
if nargin<6;NumberStatus = [];end
if nargin<5;SamplingRate = [];end
if nargin<4;SegmentMarkerVal = [];end
if nargin<3;MarkerChannel = [];end
if nargin<2;ExtractChannel = [];end
if nargin<1;FilePath = [];end

if isempty(SegmentNumber);SegmentNumber = 0;end
if isempty(NumberStatus);NumberStatus = 0;end
if isempty(SamplingRate);SamplingRate = 400;end
if isempty(SegmentMarkerVal);SegmentMarkerVal = 1;end
if isempty(MarkerChannel);MarkerChannel = 'Marker';end
if isempty(ExtractChannel);ExtractChannel = 1;end
if isempty(FilePath);FilePath = 0;end

if nargin<8
    %find segment information
    fibpseg;

    %which marker value do we want to extract
    SegmentNr = find(Segments(:,1)==SegmentMarkerVal);
    disp(['loading segment with marker value ',num2str(SegmentMarkerVal),' ...']);
    t1 = Segments(SegmentNr,2);
    t2 = Segments(SegmentNr,3);
end

%read this segment
AllPrintStatus = 0;
PrintStatus = 0;
PlotStatus = 0;
clear DATA;
ReadFP = FilePath;
readbp;

return

