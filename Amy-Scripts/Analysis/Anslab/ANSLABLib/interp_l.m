function yn = interp_l (y_1,y_2,lenip);
%function yn = interp_l (y1,y2,len);
% Linear interpolation between y1 and y2 for len new values.
% output: vector with interpolated values
% F.W., last update 8-30-95, 10-23-95

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if lenip>0

if y_2==y_1
  yn=y_2*ones(1,lenip);
else
  d=(y_2-y_1)/(lenip+1);
  yn=y_1:d:y_2;
  yn(1)=[];
  yn(length(yn))=[];
  if length(yn)>lenip
     yn=yn(1:lenip);
  end
  if length(yn)<lenip
     yn(length(yn)+1:lenip)=yn(length(yn))*ones(1,lenip-length(yn)+1);
  end
end

else
  yn=[];
end;
