function textsave(FilePath,Data)

%		function textsave(FilePath,Data)

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


fid = fopen(FilePath,'wt');
if size(Data,1)==1
    for ColInd = 1:size(Data,2)-1
        fprintf(fid,num2str(Data(1,ColInd),'%1.4f'));
        fprintf(fid,'\t');
    end
    fprintf(fid,num2str(Data(1,ColInd+1),'%1.4f'));
else
    for RowInd = 1:size(Data,1)-1
        ColInd = 0;
        if size(Data,2)-1>1
            for ColInd = 1:size(Data,2)-1
                fprintf(fid,num2str(Data(RowInd,ColInd),'%1.4f'));
                fprintf(fid,'\t');
            end
            fprintf(fid,num2str(Data(RowInd,ColInd+1),'%1.4f'));
            fprintf(fid,'\n');
        else
            fprintf(fid,num2str(Data(RowInd,ColInd+1),'%1.4f'));
            fprintf(fid,'\n');
        end
    end
    for ColInd = 1:size(Data,2)-1
        fprintf(fid,num2str(Data(RowInd+1,ColInd),'%1.4f'));
        fprintf(fid,'\t');
    end
    fprintf(fid,num2str(Data(RowInd+1,ColInd+1),'%1.4f'));
end
fclose(fid);

return