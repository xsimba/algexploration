function varargout = ANSLABImport(action,varargin)

%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

global hANSLABImportFig ImportStatus;

if nargin<1;action='Init';end

%=========================================
%   Init
%=========================================
if strcmp(action,'Init');
    
    hANSLABImportFig = openfig('ANSLABImport.fig','reuse');
    gList = guihandles(hANSLABImportFig);
    set(hANSLABImportFig,'userdata',gList);
    
%=========================================
%   OpenFile
%=========================================
elseif strcmp(action,'OpenFile');
    
    gList = get(hANSLABImportFig,'userdata');
    DataFormatCell = {'txt','*'};
    %DataFormatCell = {'txt','DTVEE'};
    DataFormatVal = get(gList.hDataFormat,'Value');
    DataFormat = DataFormatCell{DataFormatVal};
    ChanByPointStatus = get(gList.hNPointsNChan,'Value');
    
    if DataFormatVal==1 %text data
        InitPath = ANSLABDefPath(1); 
        [File,Path] = uigetfile([InitPath,'.',DataFormat],'Please choose data file:');
        if isequal(File,0)|isequal(Path,0);return;end
        FilePath = [Path,File];
        ANSLABDefPath(2,Path);
        Data = textread(FilePath);
        if ChanByPointStatus
            Data = Data';
        end
        NChan = size(Data,1);
        NPoints = size(Data,2);
        disp(['Data from ',num2str(NChan),' channels and ',num2str(NPoints),' samples read...']);
        
        
    elseif DataFormatVal==2 %DTVEE format
        
        InitPath = ANSLABDefPath(1); 
        [File,Path] = uigetfile([InitPath,'.',DataFormat],'Please choose data file:');
        if isequal(File,0)|isequal(Path,0);return;end
        FilePath = [Path,File];
        ANSLABDefPath(2,Path);
        
        [Data,Channels]=LoadHP(FilePath);

        disp(['Data from ',num2str(size(Data,1)),' channels and ',num2str(size(Data,2)),' samples read...']);
        disp(['...']);

    else
        uiwait(errordlg('Format not supported yet!'));
        return
    end
    ChanString = num2str((1:NChan)');
            
        
    set(gList.hFilePath,'String',FilePath);
    set(gList.hNChan,'String',num2str(NChan));
    set(gList.hNPoints,'String',num2str(NPoints));
    set(gList.hSetVar1,'String',ChanString);
    set(gList.hSetVar2,'String',ChanString);
    set(gList.hSetVar3,'String',ChanString);
    set(gList.hSetVar4,'String',ChanString);
    if NChan>1
        set(gList.hSetVar2,'Value',2);
    end
    if NChan>2
        set(gList.hSetVar3,'Value',3);
    end
    if NChan>3
        set(gList.hSetVar4,'Value',4);
    end
    set(gList.hData,'userdata',Data);    
    
    ANSLABImport('EnableVariables');
    
%=========================================
%   InitPos
%=========================================    
elseif strcmp(action,'InitPos');
    try
        utilpath = strrep(which('ANSLABImport.m'),'ANSLABImport.m','ANSLABImportPos.mat');
        load(utilpath,'ImportFigPos');
        set(gcf,'position',ImportFigPos);
    catch
    end
    
%=========================================
%   SavePos
%=========================================    
elseif strcmp(action,'SavePos');    
    
    try
        ImportFigPos = get(hANSLABImportFig,'position');
        utilpath = strrep(which('ANSLABImport.m'),'ANSLABImport.m','ANSLABImportPos.mat');
        save(utilpath,'ImportFigPos');
        delete(ImportFigPos);
    catch
        delete(gcf);
    end
    
%=========================================
%   NChanNPoints
%=========================================    
elseif strcmp(action,'NChanNPoints');
    
    gList = get(hANSLABImportFig,'userdata');
    Status = get(gList.hNChanNPoints,'Value');
    if Status==1
        set(gList.hNPointsNChan,'Value',0);
        set(gList.hNChanNPoints,'Value',1);
    else
        set(gList.hNPointsNChan,'Value',1);
        set(gList.hNChanNPoints,'Value',0);
    end

%=========================================
%   NPointsNChan
%=========================================    
elseif strcmp(action,'NPointsNChan');
    
    gList = get(hANSLABImportFig,'userdata');
    Status = get(gList.hNPointsNChan,'Value');
    if Status==1
        set(gList.hNPointsNChan,'Value',1);
        set(gList.hNChanNPoints,'Value',0);
    else
        set(gList.hNPointsNChan,'Value',0);
        set(gList.hNChanNPoints,'Value',1);
    end

%=========================================
%   SetVar1,SetVar2,SetVar3,SetVar4
%=========================================    
elseif strcmp(action,'SetVar1') | strcmp(action,'SetVar2') | strcmp(action,'SetVar3') | strcmp(action,'SetVar4')
    
    gList = get(hANSLABImportFig,'userdata');
    ChanInd = str2num(action(7));

    
%=========================================
%   Color1,Color2,Color3,Color4
%=========================================    
elseif strcmp(action,'Color1') | strcmp(action,'Color2') | strcmp(action,'Color3') | strcmp(action,'Color4')
    
    gList = get(hANSLABImportFig,'userdata');
    ChanInd = str2num(action(6));
    eval(['HGui = gList.hColor',num2str(ChanInd),';']);
    prompt={'enter new color? [b,g,r,c,m,y,k]'};
    name='Enter new color name';
    numlines=1;
    ColString = get(HGui,'String');
    defaultanswer={ColString};  
    answer=inputdlg(prompt,name,numlines,defaultanswer);
    
    switch answer{1}
        case 'b'
            Color = [0 0 1];
        case 'g'
            Color = [0 1 0];
        case 'r'
            Color = [1 0 0];
        case 'c'
            Color = [0 1 1];
        case 'm'
            Color = [1 0 1];
        case 'y'
            Color = [1 1 0];
        case 'k'
            Color = [0 0 0];
    end
    
    if isempty(answer);return;end
    
    set(HGui,'String',answer{1},'ForegroundColor',Color,'BackgroundColor',Color);
    
%=========================================
%   EnableVar1,EnableVar2,EnableVar3,EnableVar4
%=========================================    
elseif strcmp(action,'EnableVar1') | strcmp(action,'EnableVar2') | strcmp(action,'EnableVar3') | strcmp(action,'EnableVar4')
    
    
    gList = get(hANSLABImportFig,'userdata');
    ChanInd = str2num(action(10));
    
    switch ChanInd
        case 1
            if get(gList.hEnableVar1,'Value')
                set(gList.hSetVar1,'Enable','on');
                set(gList.hCalib1,'Enable','on');
            else
                set(gList.hSetVar1,'Enable','off');
                set(gList.hCalib1,'Enable','off');
            end
        case 2
            if get(gList.hEnableVar2,'Value')
                set(gList.hSetVar2,'Enable','on');
                set(gList.hCalib2,'Enable','on');
            else
                set(gList.hSetVar2,'Enable','off');
                set(gList.hCalib2,'Enable','off');
            end
        case 3
             if get(gList.hEnableVar3,'Value')
                set(gList.hSetVar3,'Enable','on');
                set(gList.hCalib3,'Enable','on');
            else
                set(gList.hSetVar3,'Enable','off');
                set(gList.hCalib3,'Enable','off');
            end
        case 4
             if get(gList.hEnableVar4,'Value')
                set(gList.hSetVar4,'Enable','on');
                set(gList.hCalib4,'Enable','on');
            else
                set(gList.hSetVar4,'Enable','off');
                set(gList.hCalib4,'Enable','off');
            end
    end
    
 
%=========================================
%   EnableVariables
%=========================================  
elseif strcmp(action,'EnableVariables')
    
    gList = get(hANSLABImportFig,'userdata');
    %var1
    set(gList.hEnableVar1,'Enable','on');
       set(gList.hSetVar1,'Enable','on');
       set(gList.hCalib1,'Enable','on');
    %var2
    set(gList.hEnableVar2,'Enable','on');
    %var3
    set(gList.hEnableVar3,'Enable','on');
    %var4
    set(gList.hEnableVar4,'Enable','on');
    
     
%=========================================
%   Reset
%=========================================  
elseif strcmp(action,'Reset')
    
    gList = get(hANSLABImportFig,'userdata');
    close(hANSLABImportFig);
    ANSLABImport;
    
%=========================================
%   Close
%=========================================  
elseif strcmp(action,'Close')
    
    gList = get(hANSLABImportFig,'userdata');
    close(hANSLABImportFig);
    
%=========================================
%   Apply
%=========================================  
elseif strcmp(action,'Apply')
    
    gList = get(hANSLABImportFig,'userdata');
    Data = get(gList.hData,'userdata');
    ExportPath = strrep(which('ANSLABImport.m'),'ANSLABImport.m','ANSLABImport.mat');
    int=6;  
    moveint=60;  
    samplerate=str2num(get(gList.hSampRate,'String'));  
    
    Var1String = get(gList.hSetVar1,'String');
    Var2String = get(gList.hSetVar2,'String');
    Var3String = get(gList.hSetVar3,'String');
    Var4String = get(gList.hSetVar4,'String');
    Var1Val = get(gList.hSetVar1,'Value');
    Var2Val = get(gList.hSetVar2,'Value');
    Var3Val = get(gList.hSetVar3,'Value');
    Var4Val = get(gList.hSetVar4,'Value');
    if iscell(Var1String)
        Var1Ind = str2num(Var1String{Var1Val});
    else
        Var1Ind = str2num(Var1String(Var1Val,:));
    end
    if iscell(Var2String)
        Var2Ind = str2num(Var2String{Var2Val});
    else
        Var2Ind = str2num(Var2String(Var2Val,:));
    end
    if iscell(Var3String)
        Var3Ind = str2num(Var3String{Var3Val});
    else
        Var3Ind = str2num(Var3String(Var3Val,:));
    end
    if iscell(Var4String)
        Var4Ind = str2num(Var4String{Var4Val});
    else
        Var4Ind = str2num(Var4String(Var4Val,:));
    end
    
    
    var1 = Data(Var1Ind,:);
    yaxisstr  = get(gList.hYAxisLabel,'String');
    secvar2 = get(gList.hEnableVar2,'Value');
    secvar3 = get(gList.hEnableVar3,'Value');
    secvar4 = get(gList.hEnableVar4,'Value');
    if secvar2
        var2 = Data(Var2Ind,:);
    else
        var2 = [];
    end
    if secvar3
        var3 = Data(Var3Ind,:);
    else
        var3 = [];
    end
    if secvar4
        var4 = Data(Var4Ind,:);
    else
        var4 = [];
    end
    title1str = get(gList.hVar1Title,'String');       
    title2str = get(gList.hVar2Title,'String');       
    title3str = get(gList.hVar3Title,'String');       
    title4str = get(gList.hVar4Title,'String');  
    
    
    LineStyle1String = get(gList.hLineStyle1,'String');     
    LineStyle2String = get(gList.hLineStyle2,'String');       
    LineStyle3String = get(gList.hLineStyle3,'String');       
    LineStyle4String = get(gList.hLineStyle4,'String');     
    LineStyle1Val = get(gList.hLineStyle1,'Value');     
    LineStyle2Val = get(gList.hLineStyle2,'Value');       
    LineStyle3Val = get(gList.hLineStyle3,'Value');       
    LineStyle4Val = get(gList.hLineStyle4,'Value');     
    LineStyle1 = LineStyle1String{LineStyle1Val};     
    LineStyle2 = LineStyle1String{LineStyle2Val};        
    LineStyle3 = LineStyle1String{LineStyle3Val};      
    LineStyle4 = LineStyle1String{LineStyle4Val};  
    
    Marker1String = get(gList.hMarker1,'String');     
    Marker2String = get(gList.hMarker2,'String');       
    Marker3String = get(gList.hMarker3,'String');       
    Marker4String = get(gList.hMarker4,'String');     
    Marker1Val = get(gList.hMarker1,'Value');     
    Marker2Val = get(gList.hMarker2,'Value');       
    Marker3Val = get(gList.hMarker3,'Value');       
    Marker4Val = get(gList.hMarker4,'Value');     
    Marker1 = Marker1String{Marker1Val};     
    Marker2 = Marker1String{Marker2Val};        
    Marker3 = Marker1String{Marker3Val};      
    Marker4 = Marker1String{Marker4Val};  
    
    Color1 = get(gList.hColor1,'String');     
    Color2 = get(gList.hColor2,'String');       
    Color3 = get(gList.hColor3,'String');       
    Color4 = get(gList.hColor4,'String');  
    
    ltv1 = [Color1,LineStyle1,Marker1];
    ltv2 = [Color2,LineStyle2,Marker2];
    ltv3 = [Color3,LineStyle3,Marker3];
    ltv4 = [Color4,LineStyle4,Marker4];
    
    
    artbegin  =[];         
    artend    =[]; 
    valueyes  =0;     
    val       =0;     
    valtime   =0;   
    event1    =[]; 
    event2    =[];
    event3    =[];
    event1yes =0;
    event2yes =0; 
    event3yes =0;
    evscan    =[]; 
    clear RESPEDIT;  
    subvarplot=0;
    CheckedStatus = 1;
    save(ExportPath,'int','moveint','samplerate','var1','var2','var3','var4','yaxisstr','secvar2','secvar3',...
        'secvar4','title1str','title2str','title3str','title4str','ltv1','ltv2','ltv3','ltv4',...
        'artbegin','artend','valueyes','val','valtime','event1','event2',...
        'event3','event1yes','event2yes','event3yes','evscan','subvarplot','CheckedStatus');
    ImportStatus = 1;
    
    
end


return