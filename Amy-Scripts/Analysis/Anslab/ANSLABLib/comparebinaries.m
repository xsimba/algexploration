function comparebinaries(FilePath1,FilePath2);

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<2;
    FilePath1 = 'C:\taf\SXP04701_short1.ACQ';
    FilePath2 = 'C:\taf\SXP04701_short2.ACQ';
end
fid1 = fopen(FilePath1,'rb');
fid2 = fopen(FilePath2,'rb');
counter = 1;
CountInARow = 0;
while ~feof(fid1)
	char1 = fread(fid1,1,'char');
	char2 = fread(fid2,1,'char');
	if char1~=char2
        disp(['Unequal on byte ',num2str(counter),'!']);
        CountInARow =CountInARow +1;
    else
        CountInARow = 0;
    end
    if CountInARow == 3
        fclose all;
        return
    end
    counter = counter+1;
end
fclose all;
return
