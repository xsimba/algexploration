% kinsbeat.m   insert event1: @


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

es='1';
[i1,i2,i3]=ginput(1);
if i3==50 es='2'; [i1,i2,i3]=ginput(1); end;
if i3==51 es='3'; [i1,i2,i3]=ginput(1); end;
if i3==1

eval(['e1=event',es,';']);
[ev1,ind]=nanrem(e1);

ev1=sort([ev1;i1*scalefact]);
e1=nanrest(ev1,ind);
valtime=e1;  %for pCO2
val=var1(round(nanrem(e1)));
evscan=e1;

if exist('pttt') & strcmp(es,'1')  % rt alligned with systole deletion
  rt=nanrest(rt,ind);
end

end;
plotyes=1;

eval(['event',es,'=e1;']);



