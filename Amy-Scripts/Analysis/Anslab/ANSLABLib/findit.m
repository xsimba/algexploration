function [tv_new,points_changed]=findit(tv);

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

threshold_factor=0.3;
tvd=diff(tv);
for i=1:length(tvd)
if (isnan(tvd(i)))
tvd(i)=0;
end
end

if( (max(tvd)-mean(tvd))>abs(min(tvd)-mean(tvd)))
	threshold=(max(tvd)-mean(tvd))*threshold_factor;
else
	threshold=abs(min(tvd)-mean(tvd))*threshold_factor;
end
if(threshold/threshold_factor>20)
tv_new(1:9)=tv(1:9);
points_changed=0;
for i=10:length(tv)
	if(abs(tv(i)-tv_new(i-1))>threshold)
		tv_new(i)=mean(tv_new(i-7:i-3));
		points_changed=points_changed+1;
	else
		tv_new(i)=tv(i);
	end
end
else
points_changed=0;
tv_new=tv;
end
