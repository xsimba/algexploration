function [filenamecell,filepathcell]=ANSLABLookInDir(filepath,filter,filenamecell,filepathcell,subfolderstatus,...
    filter_ex,endofnamestatus,endofnameexstatus,logictype,searchpart,CaseSensitiveStatus)


%	ANSLABLookInDir
	
%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,


%   USA. 
if nargin < 11; CaseSensitiveStatus = 0;end
if nargin < 10; searchpart = 1;end
if nargin < 9;logictype = 1;end
if nargin < 8; endofnameexstatus = 0;end
if nargin<7; endofnamestatus = 0;end
if nargin<6; filter_ex = [];end

if logictype==1
    logicmod = 1;
elseif logictype ==2
    logicmod = 0;
else
    logicmod = 1;
end

clc
fprintf(1,'%s',['Actual path: ',filepath]);

if ~isempty(filter_ex)
    findstr = filter_ex(1:strfind(filter_ex,char(32))-1);
    replacestring = filter_ex((strfind(filter_ex,char(32))+1):length(filter_ex));
end


try
    cd(filepath);
catch
    fprintf('\n');
    fprintf(1,'%s',['Directory ',filepath,' could not be opened...']);
    return
end   

if endofnamestatus
    lengthfilter = size(filter,2);
end
if endofnameexstatus
    lenghfilter_ex = size(filter_ex,2),
end


% check for subfolders
%=============================
filepathcelltmp=dir;
subfoldervec=[];
subfolderzaehler=1;
for j=1:size(filepathcelltmp,1)
    if ~strcmp(filepathcelltmp(j).name,'.') & ~strcmp(filepathcelltmp(j).name,'..')
        %check if its a subdirectory
        if ~strcmp(filepath(end),filesep)
            filepath = [filepath,filesep];
        end
        if exist([filepath,filepathcelltmp(j).name],'dir')~=7
        
        else
            subfoldervec(subfolderzaehler)=j;
            subfolderzaehler=subfolderzaehler+1;
        end
    end
end

% search for files
%============================
if isempty(filter)
    filter = '*';
end
filepathcelltmpwild = dir(filter);
for j=1:size(filepathcelltmpwild,1)
    if ~strcmp(filepathcelltmpwild(j).name,'.') & ~strcmp(filepathcelltmpwild(j).name,'..')
        %check if its a subdirectory
        if ~strcmp(filepath(end),filesep)
            filepath = [filepath,filesep];
        end
        if exist([filepath,filepathcelltmpwild(j).name],'dir')~=7
            %filter
            if CaseSensitiveStatus
                wildcards = strfind(filter,'*');
                filterTmp{1} =filter(1:wildcards(1)-1);
                for r=1:length(wildcards)-1
                    filterTmp{r+1} =filter(wildcards(r)+1:wildcards(r+1)-1);
                end
                filterTmp{length(wildcards)+1}=filter(wildcards(end)+1:end);
                ContStatus = 1;
                for r=1:length(filterTmp)
                    if ~isempty(filterTmp{r})
                        if isempty(strfind(filepathcelltmpwild(j).name,filterTmp{r}))
                            ContStatus = 0;
                        end
                    end
                end
                if ContStatus
                    found = 0;
                    if ~isempty(filenamecell{1})
                            filenamecell{end+1}=filepathcelltmpwild(j).name;
                            filepathcell{end+1}=filepath;
                            found = 1;
                    else
                            filenamecell{1}=filepathcelltmpwild(j).name;
                            filepathcell{1}=filepath;
                            found = 1;
                    end
                end
            else
               if ~isempty(filenamecell{1})
                        filenamecell{end+1}=filepathcelltmpwild(j).name;
                        filepathcell{end+1}=filepath;
                        found = 1;
                else
                        filenamecell{1}=filepathcelltmpwild(j).name;
                        filepathcell{1}=filepath;
                        found = 1;
                end  
            end
        end
    end
end

if strcmp(filter,'*')
    filter = [];
end
if subfolderstatus==1
    for i=1:subfolderzaehler-1
        if strcmp(computer,'PCWIN')
            [filenamecell,filepathcell]=ANSLABLookInDir([filepath,filepathcelltmp(subfoldervec(i)).name,'\'],filter,filenamecell,filepathcell,subfolderstatus,filter_ex,endofnamestatus,endofnameexstatus,[],[],CaseSensitiveStatus);
        elseif strcmp(computer, 'GLNX86')
            [filenamecell,filepathcell]=ANSLABLookInDir([filepath,filepathcelltmp(subfoldervec(i)).name,'/'],filter,filenamecell,filepathcell,subfolderstatus,filter_ex,endofnamestatus,endofnameexstatus,[],[],CaseSensitiveStatus);
        end    
	end
end
return
