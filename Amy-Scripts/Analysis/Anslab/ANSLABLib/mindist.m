function n=mindist(mt,mv,mingap);
% function n=mindist(mt,mv,mingap);
% MT: mintime, MV: minvalue
% MINGAP: SIze of window for comparison of MVs.
% N : Indices of MV within a window of MINGAP that are not the minimum.


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

diffmt=diff(mt);
for i=1:length(mt)-1,
  if diffmt(i)<mingap,
   if mv(i)>mv(i+1),
      mt(i)=0;
      i=i+1;
   else
      mt(i+1)=0;
      i=i+1;
   end
  end
end
n=find(mt==0);

