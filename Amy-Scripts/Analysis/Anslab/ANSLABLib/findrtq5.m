% Detection of R-, T-, and Q-waves in ECG, samplerate 400 Hz, 0.25sec epochs
% Determination of interbeat intervals.
% off- Detection and correction of IBI-difference artifacts (e.g. extra-systoles).
% Inspection of resulting values.
% Specification of artifactual intervals.
% Finds T-waves:
% First the EKG signal is filtered to get rid of shifting baseline
% and 60 Hz noise (Butterworth bandpass filter, cutoff frequ 1 and 20 Hz).
% Then specially designed algorithm determines a baseline for T-values:
% First a interval 0.2*IBI after the T-wave to 0.8*IBI after the R-wave is
% determined. Then starting from the minimum in this interval the first pattern
% of at least 8 points of stability (deviation <=2) is found by going forwards
% and backwards at a rising distance from the minimum.
% T-values are related to these baselines.
% Inspection of raw data and editing or automatic exclusion of outliers
% Conversion into epochs of instantanous data

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


%*** Initialization
sr_orig=400;    % samplerate of original ECG data
ep=4;           % epoch size = 1/ep sec
if lucas_flag
   upsrfac=1;
   notch60yes=0;
   filteryes=1;
   filter2yes=0;
   qfindyes=0;
   qtfindyes=0;
   tfindyes=0;
end

srfac=sr_orig/400;   % adjust detction parameters (originally based on 400 Hz signal)
sr=sr_orig*upsrfac;
area=round(area*srfac*upsrfac);
mingap=area;
back=round(back*srfac*upsrfac);
q_range=round(q_range*srfac*upsrfac);
q_fixed=round(q_fixed*srfac*upsrfac);
artinterval=[];
lookatibi=[];
radd=[];
cfig, cfig, cfig
if ~exist('bno') bno=1; bplotyes=1; end
if ~bno rateyes=0; rat=0; end
if delsampend
   len=length(EC);
   EC(len-delsampend+1:len)=[];
end
y=EC;


if inverseyes
   y=-y;
   disp('Signal was reversed');
end;


%*** Filtering of signals
if notch60yes
   disp('60 Hz notch filter')
   y=notch(y,sr_orig,60,4,7,.5,0);
end
if filteryes
   disp('Lowpass filter');
   y=filthigh(y,lowfreq,sr_orig,7);
end
if filter2yes
   disp('Highpass filter');
   y=filtlow(y,0.5,sr_orig,5);
end;
y=y*scalefact; % analyzed channel

%*** Upsampling
if upsrfac>1
   disp(['Upsampling by factor ',int2str(upsrfac)]);
   y=interp(y,upsrfac);
end


% Load R-wave data if exist
reanalyze=0;
if loadrwave
   chd(red_dir);
   file=[subject,twostr(filenum),savestr(1),'.mat'];
   ok=isfile(file);
   if ok
      cmdstr=['load ',file];
      disp(cmdstr);
      eval(cmdstr)
      disp('Edited ECG file found. R-waves will be based on past ECG editing.')
      reanalyze=0;
   else
      disp('No edited ECG file found. R-waves will be based on new ECG analysis.')
      reanalyze=1;
   end
else
   reanalyze=1;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% R-wave detection

if reanalyze

if byes manualset=0; end

%*** Manual setting of thresholds
if manualset  % determined in menue of 'anx'
   if usediff
      plot(diff(y(3000*srfac*upsrfac:5000*srfac*upsrfac)))
   else
      plot(y(3000*srfac*upsrfac:5000*srfac*upsrfac))
   end
   i=input('High pass filtering (no=<0>, yes=<1>) [0] ==>  ');
   if isempty(i) i=0; end;
   lowfiltyes=0;
   if i
      j=input('Cutoff frequency in Hz [1] ==>  ');
      if isempty(j) j=1; end;
      ylowfilt=y; lowfiltyes=1;
      y=filtlow(y,j,sr,7);
      disp('Signal was highpass filtered');
   end;
   i=input('Reversal of signal (for negative R-wave, no=<0>, yes=<1>) [0] ==>  ');
   if isempty(i) i=0; end;
   if i
      y=-y;
      disp('Signal was reversed');
   end;

   if usediff
      threshold=input('Threshold for diff detection [1.5] ==>  ');
      if isempty(threshold) threshold=1.5; end;
   else
      area=input('Detection window size [150] ==>  ');
      if isempty(area) area=150; end;
      back=input('Backward comparison point [15] ==>  ');
      if isempty(back) back=15; end;
      rise1=input(['Increase from that point in mV [',num2str(rise),'] ==>  ']);
      if ~isempty(rise1) rise=rise1; end;
      mingap=area;
   end

else
  if dyn    % dynamic adaptation of rise
     if length(y)>5000*srfac*upsrfac
        n=y(3000*srfac*upsrfac:5000*srfac*upsrfac);
        n2=EC(3000*srfac:5000*srfac)*scalefact;
     else
        n=y(200*srfac*upsrfac:length(y));
        n2=EC(200*srfac:length(y))*scalefact;
     end
     rise=(max(n)-mean(n))/2.5;
     threshold=max(-diff(n))*risefactor;

     if bno
        figure;
        if usediff
           subplot(3,1,1)
           t=(1:length(n2))/sr_orig;
           plot(t,n2)
           axisx(0,length(n2)/sr_orig)
           title('Unfiltered ECG')
           subplot(3,1,2)
           t=(1:length(n))/sr;
           plot(t,n)
           axisx(0,length(n)/sr)
           title('Filtered ECG')
           subplot(3,1,3)
           t=(1:length(n)-1)/sr;
           plot(t,-diff(n));
           axisx(0,length(n)/sr)
           plotliny(threshold,'c:');
           title(['Filtered and differentiated ECG.  Threshold criterion = ',num2str(threshold)]);
           xlabel('sec')
        else
           t=(1:length(n))/sr;
           plot(t,n)
           axisx(0,length(n)/sr)
           hold on
           plot(t,ones(1,length(t))*rise,'b:')
           title(['Filtered ECG.  Slope criterion = ',num2str(rise)]);
           xlabel('sec')
        end
        thresh_ok=input('Criterion ok? (1=yes [default], 0=no)  ==> ');
        if ~isempty(thresh_ok) | thresh_ok==0
           rise1=input(['New criterion ==>  ']);
           if ~isempty(rise1)
              if usediff
                 threshold=rise1;
              else
                 rise=rise1;
              end
           end
        end;
    end; % if bno
  end; % if dyn
end; %if manualset

threshold=-threshold;

clear EC

%*** Detect maxima
disp(' ');
disp('Detection of R-waves');

if usediff
   [rt]=findrd(y,threshold,sr);
   rv=[];
else
   [rt,rv]=findmax(y,area,back,rise);
end
disp([int2str(length(rt)),' R-waves were detected']);


if ipswitch
%*** Resampling of areas around detected R-waves
%    and more accurate determination of maximum
disp(['Lowpass interpolation of areas around detected R-waves to gain higher temporal resolution [factor ',int2str(ipf),']']);
rt2=[]; rv2=[];
for i=1:length(rt)
  [rv(i),rt2(i)]=max(interp(y(rt(i)-5:rt(i)+4),ipf));
end;
rt=rt-6+(rt2(:)+ipf-1) ./ipf;
end;


%*** Automatically detect and correct IBI-difference artifacts
if usecorr
disp('Automatic correction of misdetected R-waves');
rt_old=rt;
[rt,alt_points,points_add,points_delete]=findirr(rt,corrfactor,threshfact);
end;

end
% if reanalyze


%*** Derive IBI
% rt is a vector of sample time when R-waves were detected
rtd=difffit(rt)/sr*1000; % IBI
rtn=rt;  % rt new
rtnd=rtd;  % rtd new

if isempty(rtnd) | all(isnan(rtnd))
    return
end
%*** Inspection and editing of IBI artifacts
if bno
redit
else
if bplotyes
figure(1)
%clg
subplot(2,1,1)
plot(rt/sr,rtd);
axisx(0,max(rt/sr));
xlabel('time [sec]');
ylabel('IBI [msec]');
title('Series of detected RR intervals');
drawnow
end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Q-point detection

if qfindyes
disp(' ')
disp('Q-point detection.')
qt=[0;0];
for i=1:length(rt);
    derivative_flag=0;
    ecg_diff = diff (y(rt(i)-q_range : rt(i)));     % differentiate ECG for area rt-onset to rt
    [max_diff, max_offset] = max(ecg_diff);        % compute maximal slope in this area
    for j=max_offset-1:-1:1
        if ecg_diff(j) < (max_diff*onset_factor)   % go backward from max slope to first flat slope
           onset_offset=j;
           derivative_flag=1;
           break;
        end
    end
    if derivative_flag==1
       qt(i)=rt(i)-q_range+onset_offset;
    else
       qt(i)=rt(i)-mean(rt(1:i-1)-qt(1:i-1));    % replace with average if no zero-slope is found in window
       disp('One Q-point estimated from surrounding beats')
    end
end

else
qt=rt-q_fixed;
end
qt=qt(:);
% if qfindyes



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QT-interval
% detecting qt interval using template method

if qtfindyes
   disp(' ')
   disp('QT interval detection.')
   [qtt,ttt]=qt_new(y,rt,qt,sr);
   qti=ttt(:)-qtt(:);
   plot(qtt*1000/sr,qti*1000/sr);
   xlabel('sec');
   title('QT interval');
   zoomrb
else
   qtt=[];
   ttt=[];
   qti=[];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% T-wave amplitude

if tfindyes
% Delete intervals around R-waves to provide better signal for filtering

if ~exist('lowfiltyes');lowfiltyes = [];end
if lowfiltyes y=ylowfilt; clear ylowfilt; end;
for i=1:length(rt)
  if ~isnan(rt(i))
    i1=round(rt(i)-15*upsrfac);
    i2=round(rt(i)+15*upsrfac);
    if i1<1 i1=1; end;
    if i2>length(y) i2=length(y); end;
    y(i1:i2)=interp_l(y(i1),y(i2),i2-i1+1);
  end;
end;

% Filter signal: get rid of shifting baseline
disp('T-wave amplitude analysis.');
order=3;
freq=[1/(sr/2) 20/(sr/2)];  % pass-band 1-20 Hz
[b,a]=butter(order,freq);
y=filtfilt(b,a,y);

%y=y*100000; % better scaling

dr=diff(rt);
lenr=length(dr);
tt=zeros(lenr,1);    % ttime
tva=zeros(lenr,1);   % absolute T-value
tb=zeros(lenr,1);    % tbaseline

for i=1:lenr
    if isnan(dr(i)) tt(i)=NaN; tb(i)=NaN;
    else

       t1=rt(i)+0.2*dr(i);        % adjust  T-wave detection interval here
       t2=rt(i)+0.5*dr(i);        % cf. Burch & Winsor (1960), p. 278
       if dr(i)<0.6 t2=t2+0.2*dr(i); end;   % adjustment for HR > 100
       [tva(i),tt(i)]=max(y(round(t1):round(t2)));
       tt(i)=tt(i)+t1-1;


       t1=tt(i)+0.2*dr(i);        % adjust baseline interval here
       t2=rt(i)+0.8*dr(i);
       b=y(round(t1):round(t2));
       [j,mb]=min(b);             % mb = time of min of baseline interval
       lb=length(b);
       br=b(mb+1:lb);             % right part of baseline interval
       bl=b(mb-1:-1:1);           % left part (reversed)
       j=1; flag=0;

       while 1
          n=j:j+7*upsrfac;
          if (j+7*upsrfac)<length(br)
             b8=br(n);               % 8 points of right baseline interval
             if (max(b8)-min(b8))<=rise*critb % criterium of stability
                tb(i)=mean(b8);
                break; end;          % baseline found
          else flag=1;               % flag for end of right baseline interval
          end;
          if (j+7*upsrfac)<length(bl)
             b8=bl(n);               % 8 points of left baseline interval
             if (max(b8)-min(b8))<=rise*critb % criterium of stability
                tb(i)=mean(b8);
                break; end;          % baseline found
          else
             if flag tb(i)=NaN; break; end;     % no baseline found
          end;
          j=j+8;
       end;  % while
    end; % if
end; % for


tt(lenr+1)=rt(lenr+1);   % adapt lengths to rtime
tva(lenr+1)=NaN;
tb(lenr+1)=NaN;

tv=tva-tb;        % relative T-value

%*** Edit values
if tv_out>0
tv=outliers(tv,tv_out); % automatic exlusion of outliers
end
tv_old=tv;
if usecorrt
[tv,points_changed]=findit(tv_old);
end

if bno
tedit
else
if bplotyes
figure(1)
subplot(2,1,2)
plot(tt/sr,tv);
axisx(0,max(tt/sr));
xlabel('time [sec]');
ylabel('T-wave amplitude [units]');
title('Series of detected T-wave amplitudes');
drawnow
end
end
%tv = outrect (tv,tt/sr,1);
end
% if tfindyes


%*** Give quality rating
if bno
if rateyes
rat=menu('Signal quality rating','1 = mean IBI not reliable','2 = many erratic spikes','3 = some erratic spikes, edited out','4 = some editing done','5 = excellent');
else
rat=0;
end
end

%*** Conversion of R-waves into 0.25 sec IBI-epochs
disp(' ')
disp('Conversion of R-wave-times into 0.25 sec IBI and HR epochs');
ibi0=epoch(rt,rtd,(sr/ep));          % heart period
tv0=epoch(tt,tv,(sr/ep));


%*** raw but fast estimation of hr0 from ibi0
%hr0est=23040 ./ibi0;
%differ=hr0-hr0est;    %
%n=isnan(differ); differ(n))=zeros(n,1);
%max(differnan), min(differnan), mean(differnan), std(differnan);
%n=find(differnan>.9);
% => hr0 is always bigger than hr0est, up to .95 bpm! Mean is .0165.


