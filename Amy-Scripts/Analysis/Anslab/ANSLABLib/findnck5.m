% James Gross
% Reduction of EMG channels


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
if exist('N2')~=1
    ch2yes=0;
end
sr=1000;

if ~exist('bno')
    bno=1;
    bplotyes=1;
end
if ~bno
    rateyes=0;
    rat=0;
end

if bno|bplotyes
    cfig, cfig
    if screen>800
        figure(1)
        set(1,'Position',[5 342 1016 381])
        figure(2)
        set(2,'Position',[5 117 1016 227])
        figure(3)
        set(3,'Position',[5 117 1016 227])
    else
        figure(1)
        set(1,'Position',[9 233 791 325])
        figure(2)
        set(2,'Position',[9 64 791 176])
        figure(3)
        set(3,'Position',[9 64 791 176])
    end
end


% Plot in Exam
if bno
    if examyes
        var1=N1;
        if ch2yes
            var2=N2;
        end
        defblank
        samplerate=1000;
        def
        exam
    end
end

% 10  Hz high-pass
% 500 Hz low-pass (anti-aliasing)
% time constant into cutoff frequency:
% fc=1/(2*pi*tc)
%fc=3.1831; % for 50 ms tc
fc=15.9155; % for 10 ms tc

%*** Preprocessing
N1f=N1;
if ch2yes
    N2f=N2;
end

if notchyes
    disp([num2str(notchfreq),' Hz notch filter']);
    N1f=notch(N1(:),1000,notchfreq,10,7,.5,0);
    if ch2yes
        N2f=notch(N2(:),1000,notchfreq,10,7,.5,0);
    end
end
disp('10 Hz highpass filter');
N1f=filtlow(N1f,10,sr,3);
if ch2yes
    N2f=filtlow(N2f,10,sr,3);
end

disp('Rectification, smoothing (16 Hz lowpass filter)');
N1fa=abs(N1f);
N1fa=filthigh(N1fa,fc,sr,3);
if ch2yes
    N2fa=abs(N2f);
    N2fa=filthigh(N2fa,fc,sr,3);
end

%*** Decimate to 4 Hz
disp('Decimation to 4 Hz');
N10=decfast(N1fa,250);
if ch2yes
    N20=decfast(N2fa,250);
end

if startnan>0
    N10(1:startnan)=NaN*ones(1,startnan);
    if ch2yes
        N20(1:startnan)=NaN*ones(1,startnan);
    end
end

%*** Plot
if bno|bplotyes
    figure(1)
    clg
    t=(1:length(N10))/4;
    if ch2yes
        plot(t,N10,t,N20);
        title('EMG reduced: Channel 1 (yellow), Channel 2 (magenta)');
        axisy(0,1.1*max([max(nanrem(N10)),max(nanrem(N20))]) );
    else
        plot(t,N10);
        title('EMG reduced');
        axisy(0,max([1.1*(nanrem(N10))]));
    end
    xlabel('sec')
    ylabel('mV')
    axisx(0,max(t));
    drawnow

    figure(2)
    clg
    t2=(1:length(N1))/sr;
    plot(t2,N1);
    title('EMG raw');
    axisy(min([1.1*(nanrem(N1))]),max([1.1*(nanrem(N1))]));
    xlabel('sec')
    ylabel('mV')
    axisx(0,max(t2));
    plotliny(0)
    zoomrb
    drawnow

    if ch2yes
        figure(3)
        clg
        t2=(1:length(N2))/sr;
        plot(t2,N2,'m');
        title('EMG raw: Channel 2 (magenta)');
        axisy(1.1*min(nanrem(N2)),1.1*max(nanrem(N2)));
        xlabel('sec')
        ylabel('mV')
        axisx(0,max(t2));
        plotliny(0)
        zoomrb
        drawnow
    end

    figure(1)
    zoomrb

    if bno
        edityes=1;
        while edityes>0

            if ch2yes
                edityes=input('Editing: no [0, default],  channel 1 [1],  channel 2 [2]  ==> ');
            else
                edityes=input('Editing: no [0, default],  channel 1 [1]  ==> ');
            end

            if isempty(edityes) edityes=0; end

            if edityes==1
               disp('Edit EMG signal')
               [N10,noutind]=outrect(N10,t,1);
            end
            if edityes==2
               disp('Edit EMG signal')
               [N20,noutind]=outrect(N20,t,1);
            end

        end
% while edityes>0
    end
% if bno
end
% if bno | bplotyes
