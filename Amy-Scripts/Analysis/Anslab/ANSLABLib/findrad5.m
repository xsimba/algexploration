% Reduction of rating dial channel
% James Gross projects

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

sr=400;
ep=4;

if ~exist('bno')
    bno=1;
    bplotyes=1;
end

if filtyes
    disp('Highpass filter for rating dial channel');
    RA=filthigh(RA,2,sr,7);  % not needed
end;

%*** Resample to allign with other variables (1/4 sec epochs)
rad0=decfast(RA,sr/ep);


if bno|bplotyes

    cfig, cfig
    figure(1)
    t=(1:length(rad0))/ep;
    plot(t,rad0);
    axisx(0,max(t));
    title('Rating dial');
    xlabel('time (sec)')
    ylabel(['Voltage = ',num2str(mean(rad0))]);
    drawnow


    if bno
        edityes=input('Editing: no [0, default],  yes [1]  ==> ');
        if isempty(edityes)
            edityes=0;
        end

        if edityes
           disp('Edit rating dial signal')
           [rad0,noutind]=outrect(rad0,t,1);
        end
    end
    % if bno

end
% if bno|bplotyes


