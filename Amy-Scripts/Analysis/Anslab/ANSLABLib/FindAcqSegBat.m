%   FindAcqSeg
%
%   FilePath
%   PlateauStatus
%   DiscardInitial
%   Discard
%   PlotStatus

%   FindAcqSeg reads in Biopac data and returns segmentation
%   information based on a dedicated 'Marker'-channel in the data.
%   If 'DiscardInitial' is set to 1 (default), the initial value is
%   ignored.
%   Markervalues found in 'Discard' are considered to be irrelevant.
%   Segment information containing the start and end point of any
%   relevant marker value is returned.

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
clear;


[NFiles,FileMat]=ANSLABReadFileNames;

for FileInd = 1:NFiles
    [NoUse,NoUse,ReadFP] = ANSLABGetFileNameOfMat(FileMat,FileInd);
    FindAcqSegFcn(ReadFP)
end

