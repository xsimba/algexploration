% artmhor.m   mark artifacts as horizontal lines

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

artleft=0; artright=0;
for i=1:length(artend)
if artbegin(i)>s1
if artend(i)<s2 artleft=artbegin(i); artright=artend(i);
elseif artbegin(i)<s2 artleft=artbegin(i); artright=s2;
else;
end;
else
if artend(i)<s2 artleft=s1; artright=artend(i);
elseif artend(i)>s2 artleft=s1; artright=s2;
else;
end;
end;
if artleft plot([artleft,artright] ./scalefact,i1+[.5,.5]*(yax2-yax1));
plot([artleft,artright] ./scalefact,i1+[.6,.6]*(yax2-yax1));
end;
end;
