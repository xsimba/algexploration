% kevjump.m    jump to event number : j


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

j=length(evscan);
disp(' ');
disp(['The last event number was ',int2str(evscani)]);
disp(['The max. event number is  ',int2str(j)]);
i=input('Jump to event number  ==>  ');
if i<=j & i>0
  ii=s2-s1;
  s1=evscan(i)-ii*skiploc; s2=s1+ii;
  evscani=i; evnumyes=1;
else
  disp('Event number is out of range.');
  plotyes=0;
end; z=999;
