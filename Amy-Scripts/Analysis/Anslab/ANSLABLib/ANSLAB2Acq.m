function ANSLAB2Acq(action,varargin)

%   ANSLAB2Acq

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

global hANSLAB2BPFig;
if nargin<1;action='Init';end


%=================================
%   Init
%=================================
if strcmp(action,'Init')

    hANSLAB2BPFig = openfig('ANSLAB2BP.fig','new');
    gList = guihandles(hANSLAB2BPFig);
    set(hANSLAB2BPFig,'userdata',gList);

%=================================
%   LoadPos
%=================================
elseif strcmp(action,'LoadPos')

    try
        utilpath = strrep(which('ANSLAB2BP.m'),'ANSLAB2BP.m','ANSLAB2BPPos.mat');
        load(utilpath,'ANSLAB2BPFigPos');
        set(gcf,'position',ANSLAB2BPFigPos);
    catch
    end
%=================================
%   SavePos
%=================================
elseif strcmp(action,'SavePos')

    gList = get(hANSLAB2BPFig,'userdata');
    try
        ANSLAB2BPFigPos = get(hANSLAB2BPFig,'position');
        utilpath = strrep(which('ANSLAB2BP.m'),'ANSLAB2BP.m','ANSLAB2BPPos.mat');
        save(utilpath,'ANSLAB2BPFigPos');
        delete(hANSLAB2BPFig);
    catch
        delete(gcf);
    end
%=================================
%   ChooseAcq
%=================================
elseif strcmp(action,'ChooseAcq');

    gList = get(hANSLAB2BPFig,'userdata');
    InitPath = ANSLABDefPath(1);
    [ReadFile,ReadPath] = uigetfile([InitPath,'.acq'],'Please choose original *.acq-file:');
    if isequal(ReadFile,0) | isequal(ReadPath,0);return;end
    AcqFilePath = [ReadPath,ReadFile] ;
    TargetFilePath = strrep(AcqFilePath,'.ACQ','_psy.txt')
    ANSLABDefPath(2,ReadPath);
    set(gList.hAcqFile,'String',AcqFilePath);
    [HDR] = ReadAcqHeader(AcqFilePath);
    set(gList.hHeader,'userdata',HDR);
    set(gList.hSRAcq,'String',num2str(1000/HDR.dSampleTime));
    set(gList.hNChanAcq,'String',num2str(HDR.nChannels));
    set(gList.hNSamplesAcq,'String',num2str(HDR.lBufLength{1}));
    set(gList.hNSamplesAcq,'String',num2str(HDR.lBufLength{1}));

    set(gList.hTargetFile,'String',TargetFilePath);



%=================================
%   ChooseRsp
%=================================
elseif strcmp(action,'ChooseRsp');

    gList = get(hANSLAB2BPFig,'userdata');
    InitPath = ANSLABDefPath(1);
    [ReadFile,ReadPath] = uigetfile([InitPath,'.rsp'],'Please choose rsp-file:');
    if isequal(ReadFile,0) | isequal(ReadPath,0);return;end
    TextFilePath = [ReadPath,ReadFile] ;
    ANSLABDefPath(2,ReadPath);
    set(gList.hRspFile,'String',TextFilePath);

    TextFid = fopen(TextFilePath,'rt');
    LineInd = 1;
    fprintf(1,'Start reading data from textfile:\n');
    fprintf(1,'%c',TextFilePath);
    fprintf(1,'...\n');
    DATA = textread(TextFilePath);
    [NSamples,NChan] = size(DATA);
    fprintf(1,'End reading textdata.\n');
    set(gList.hNChanRsp,'String',num2str(NChan));
    set(gList.hNSamplesRsp,'String',num2str(NSamples));
    set(gList.hData,'userdata',DATA);


%=================================
%   ChooseTarget
%=================================
elseif strcmp(action,'ChooseTarget');

    gList = get(hANSLAB2BPFig,'userdata');
    InitPath = ANSLABDefPath(1);
    [ReadFile,ReadPath] = uiputfile([InitPath,'.acq'],'Please name target-file:');
    if isequal(ReadFile,0) | isequal(ReadPath,0);return;end
    TargetFilePath = [ReadPath,ReadFile] ;
    ANSLABDefPath(2,ReadPath);
    set(gList.hTargetFile,'String',TargetFilePath);

%=================================
%   WriteBP
%=================================
elseif strcmp(action,'WriteBP');

    gList = get(hANSLAB2BPFig,'userdata');

    %search for available files
    AcqFilePath = get(gList.hAcqFile,'String');
    TargetFilePath = get(gList.hTargetFile,'String');
    [fpathstr,SearchString,fext,fversn] = fileparts(AcqFilePath);
    RspFilePath = get(gList.hRspFile,'String');
    if isempty(findstr(RspFilePath,SearchString));
        uiwait(errordlg('Unmatching or undefined rsp file!'));
        return
    end
    SearchString =['*',SearchString,'*'];
    DataDir = get(gList.hDataRoot,'String');
    DataCell = cell(1,2);
    AddCount = 1;
    HDR = get(gList.hHeader,'userdata');
    HDR1 = HDR;

    CheckWriteStatus = 1;
    TakeAllStatus = get(gList.hTakeAll,'Value');
    %search all possible subfolders....
    if TakeAllStatus
        DataDirCell = { [DataDir,filesep,'a'];...
                        [DataDir,filesep,'b'];...
                        [DataDir,filesep,'c'];...
                        [DataDir,filesep,'d'];...
                        [DataDir,filesep,'e'];...
                        [DataDir,filesep,'f'];...
                        [DataDir,filesep,'g'];...
                        [DataDir,filesep,'h'];...
                        [DataDir,filesep,'i'];...
                        [DataDir,filesep,'l'];...
                        [DataDir,filesep,'n'];...
                        [DataDir,filesep,'o'];...
                        [DataDir,filesep,'p'];...
                        [DataDir,filesep,'r'];...
                        [DataDir,filesep,'s'];...
                        [DataDir,filesep,'t']};
        TypeCell = {    ['a'];...
                        ['b'];...
                        ['c'];...
                        ['d'];...
                        ['e'];...
                        ['f'];...
                        ['g'];...
                        ['h'];...
                        ['i'];...
                        ['l'];...
                        ['n'];...
                        ['o'];...
                        ['p'];...
                        ['r'];...
                        ['s'];...
                        ['t']};
        for FolderInd = 1:length(DataDirCell)
            filenamecell=cell(1);
            filepathcell=cell(1);
            [filenamecell,filepathcell]=ANSLABLookInDir(DataDirCell{FolderInd},SearchString,filenamecell,filepathcell,1);
            if isempty(filenamecell)
                continue;
            elseif isempty(filenamecell{1})
                continue;
            else
                if length(filenamecell)~=1
                    disp('Multiple targets found:')
                    Found = 0;
                    for TmpInd = 1:length(filenamecell)
                        disp(filenamecell{TmpInd});
                        if IsBinary(filenamecell{TmpInd})
                            continue;
                        else
                            disp(['assuming file ',filenamecell{TmpInd},' to be the correct one...']);
                            DataCell{AddCount,1} = FolderInd;
                            DataCell{AddCount,2} = [DataDirCell{FolderInd},filesep,filenamecell{1}];
                            Found = 1;
                        end
                    end
                    if Found ==0
                        error('Ambiguous file names...');
                    end
                    return
                else
                    DataCell{AddCount,1} = FolderInd;
                    DataCell{AddCount,2} = [DataDirCell{FolderInd},filesep,filenamecell{1}];
                end
                AddCount = AddCount +1;
            end
        end
        fprintf(1,'\n');
        disp([num2str(AddCount-1),' assembled text files:'])
        for TmpInd = 1:size( DataCell,1)
            disp(['type: ',TypeCell{DataCell{TmpInd,1}},' path: ',DataCell{TmpInd,2}]);
        end

    %take only specific subfolders....
    else

    end

    Channels = char(HDR.szCommentText{:});
    VarLabelCell = { ...
        ['a'],{[]};...%a:
        ['b'],{[]};...%b:
        ['c'],{[]};...%c:
        ['d'],{[]};...%d:
        ['e'],{'pa_0','ptt0'};... %e:   S =[pa_0 ptt0]  --> bioppg
        ['f'],{[]};...%f:
        ['g'],{[]};...%g:
        ['h'],{[]};...%h:
        ['i'],{'hr3','ibi3','tv3'};... %i:   S=[hr3 ibi3 tv3]   --> bioibi
        ['l'],{'sN10'};...%l:   S=[N10]  --> biostl
        ['n'],{'mN10'};...%n:   S=[N10]  --> bioemg
        ['o'],{[]};...%o:
        ['p'],{'CO20','RR0'};... %p:S=[CO20 RR0] --> bioco
        ['r'],{'RS0','RR0','Vt0','Vmin0','Flow0','TiTt0','Ti0','Pi0','Te0','Pe0','Tt0','Tic0','Tec0','RibP0'};... %r:   S=[RS0 RR0 Vt0 Vmin0 Flow0 TiTt0 Ti0 Pi0 Te0 Pe0 Tt0 Tic0 Tec0 RibP0] --> bioresp
        ['s'],{'scl0','nsfr0','nsfv0','rtim0','hrec0'};...  %s:   S=[scl0 nsfr0 nsfv0 rtim0 hrec0]  --> bioeda
        ['t'],{[]}};%t:


    %New NChan & NSamples
    NNewChan = 0;
    NMaxSamples = 0;
    PaddWithZerosStatus = get(gList.hPaddWithZeros,'Value');
    PaddMaxStatus = get(gList.hPaddMax,'Value');
    PaddBothStatus = get(gList.hPaddBoth,'Value');
    PaddBeginStatus = get(gList.hPaddBegin,'Value');
    PaddEndStatus = get(gList.hPaddEnd,'Value');
    PaddMax = str2num(get(gList.hPaddNZeros,'String'));
    for FileInd=1:size(DataCell,1)
       DataCell{FileInd,4} = VarLabelCell{DataCell{FileInd,1},2};
       NExpected = length(VarLabelCell{DataCell{FileInd,1},2});
       disp(['reading data from file: ',DataCell{FileInd,2},' expecting ',num2str(NExpected),' columns...']);
       FormatString = [];
       for TmpInd = 1:NExpected
           FormatString = [FormatString ,'%20n'];
       end
       DataTmp = textread(DataCell{FileInd,2},'');
       if all(DataTmp(:,end)==0)
           disp(['last column contains only zeros ... deleting ...']);
           DataTmp = DataTmp(:,1:end-1);
       end
       if size(DataTmp,2)==NExpected
           disp([num2str(size(DataTmp,2)),' columns found... ok!']);
       else
           error([num2str(size(DataTmp,2)),' columns found... ???!']);
       end
       NNewChan = NNewChan+NExpected;
       DataCell{FileInd,3} = DataTmp;
       NSamples = size(DataCell{FileInd,3},1);
       if NSamples > NMaxSamples
           NMaxSamples = NSamples;
       end
    end
    disp(['New number of samples (found maximum): ',num2str(NMaxSamples),' ...']);
    disp(['checking for equal channel lengths ... ']);
    for FileInd=1:size(DataCell,1)
        [NSamplesTmp,NExpected] = size(DataCell{FileInd,3});
        if NMaxSamples-NSamplesTmp>0
           disp(['file contains ',num2str(NSamplesTmp),' samples ??? ',num2str(NMaxSamples),' expected!']);
           if NMaxSamples-NSamplesTmp<=PaddMax
                disp(['difference ',num2str(NMaxSamples-NSamplesTmp),' sample(s) <= ',num2str(PaddMax),' maximum allowed samples ...']);
                if PaddBothStatus
                    NDiff = NMaxSamples-NSamplesTmp;
                    NBegin = round(NDiff/2);
                    NEnd = NDiff-NBegin;
                    PaddString = ['begin #',num2str(NBegin),' and end #',num2str(NEnd)];
                    DataCell{FileInd,3} = [zeros(NBegin,NExpected);DataCell{FileInd,3};zeros(NEnd,NExpected)];
                elseif PaddBeginStatus
                    NDiff = NMaxSamples-NSamplesTmp;
                    NBegin = NDiff;
                    PaddString = ['begin #',num2str(NBegin)];
                    DataCell{FileInd,3} = [zeros(NBegin,NExpected);DataCell{FileInd,3}];
                elseif PaddEndStatus
                    NDiff = NMaxSamples-NSamplesTmp;
                    NEnd = NDiff;
                    PaddString = ['end #',num2str(NEnd)];
                    DataCell{FileInd,3} = [DataCell{FileInd,3};zeros(NEnd,NExpected)];
                end
                disp(['padding with zeros (',PaddString,')  ...']);
           else
               error(['difference ',num2str(NMaxSamples-NSamplesTmp),' sample(s) > ',num2str(PaddMax),' maximum allowed samples ???']);
           end
        else
           disp(['file contains ',num2str(NSamplesTmp),' samples ... ok!']);
        end
    end
    DATA = [DataCell{:,3}];
    %New NChan
    disp(['New number of channels: ',num2str(NNewChan),' ...']);
    %New sampling rate
    TakeSRRspStatus = get(gList.hTakeSRRsp,'Value');
    TakeSRAcqStatus = get(gList.hTakeSRAcq,'Value');
    TakeSRsettoStatus = get(gList.hTakeSRsetto,'Value');
    SetToSR = str2num(get(gList.hSetToSR,'String'));
    AdjustSampleRateStatus = get(gList.hAdjustSR,'Value');
    if ~AdjustSampleRateStatus
       NewSR = 1000/HDR.dSampleTime;
       NewST = HDR.dSampleTime;
       SRTypeString = 'taken from ACQ';
    else
       if TakeSRRspStatus
           NewSR = 25;
           NewST = 40;
           SRTypeString = 'taken from RSP';
       elseif TakeSRAcqStatus
            NewSR = 1000/HDR.dSampleTime;
            NewST = HDR.dSampleTime;
            SRTypeString = 'taken from ACQ';
       elseif TakeSRsettoStatus
            NewSR =  str2num(get(gList.hSetToSR,'String'));
            NewST = 1000/NewSR;
            SRTypeString = ['set to ',num2str(NewSR)];
       else
           error('Invalid SR-adjustment option!');
       end
    end
    disp(['New sampling rate: ',num2str(NewSR),'Hz (',SRTypeString,') ...']);
    %resample data
    for FileInd=1:size(DataCell,1)
        DataBackup = DataCell{FileInd,3};
        NChanCell = size(DataBackup,2);
        for ChanInd = 1:size(DataCell{FileInd,3},2);
            rsData = DataBackup(:,ChanInd);
            rsData = resample(rsData,NewSR,10);
            if ChanInd == 1
                DataCell{FileInd,3} = zeros(size(rsData,1),NChanCell);
            end
            DataCell{FileInd,3}(:,ChanInd)=rsData;
        end
    end
    NMaxSamples = size(DataCell{1,3},1);
    DATA = [DataCell{:,3}];
    DATA = [DATA];
    LABELS = [DataCell{:,4}];
    save(TargetFilePath,'-ASCII','-TABS','DATA');
    ok
%
%     %Assemble new header information
%     HDR = CreateAcqDefaultHeader(1);
%     HDR.nChannels = NNewChan + 1;
%     HDR.dSampleTime = NewST;
%     HDR.nNum = mat2cell([1,50+1:50+NNewChan],1,ones(1,NNewChan+1));
%     HDR.szCommentText = ['default                                 ' DataCell{:,4}];
%     for LabelInd = 1:NNewChan
%         if length(HDR.szCommentText{LabelInd+1})<40
%             HDR.szCommentText{LabelInd+1} = [HDR.szCommentText{LabelInd+1} zeros(1,40-length(HDR.szCommentText{LabelInd+1}))];
%         elseif length(HDR.szCommentText{LabelInd+1})>40
%             HDR.szCommentText{LabelInd+1} = HDR.szCommentText{LabelInd+1}(1:40);
%         end
%     end
%     HDR.rgbColor =  mat2cell(round([zeros(NNewChan+1,1) jet(NNewChan+1)*255]),ones(NNewChan+1,1),4);
%     HDR.nDispChan = mat2cell(ones(1,NNewChan+1).*2,1,ones(1,NNewChan+1));
%     DATAScale = DATA;
%     DATAMean = mean(DATA);
%     DATAScaleOffset = DATA;
%     DATAMax = max(DATAScale);
%     DATAMin = min(DATAScale);
%     Offset = mean([DATAMax;DATAMin]);
%     Offset = [0 Offset];
%     for ChanInd = 1:NNewChan
%         DATAScaleOffset(:,ChanInd) = DATAScale(:,ChanInd)- Offset(ChanInd);
%     end
%     ScaleMax = max(DATAScaleOffset);
%     ScaleMin = min(DATAScaleOffset);
%     ScaleRange = ScaleMax-ScaleMin;
%     ShortOptScale = 45000./ScaleRange; %65535 is full range --> 45000 allows editing
%     ShortOptScale = [1 ShortOptScale];
%     HDR.dVoltOffset =  mat2cell(zeros(1,NNewChan),1,ones(1,NNewChan));
%     HDR.dVoltScale = mat2cell(ones(1,NNewChan),1,ones(1,NNewChan));
%     HDR.szUnitsText = (mat2cell(repmat('ANSLABCalc             ',NNewChan,1),ones(1,NNewChan),20))';
%     HDR.lBufLength = mat2cell(ones(1,NNewChan)*NMaxSamples,1,ones(1,NNewChan));
%     HDR.dAmplScale = mat2cell(ShortOptScale,1,ones(1,NNewChan));
%     HDR.dAmplOffset = mat2cell(Offset,1,ones(1,NNewChan));
%     HDR.nChanOrder = mat2cell(1:NNewChan,1,ones(1,NNewChan));
%     HDR.nDispSize = mat2cell(ones(1,NNewChan)*300,1,ones(1,NNewChan));
%     HDR.plotMode = mat2cell(zeros(1,NNewChan),1,ones(1,NNewChan));
%     HDR.vMid =  mat2cell(zeros(1,NNewChan),1,ones(1,NNewChan));
%     HDR.szDescription = (mat2cell(zeros(NNewChan,128),ones(1,NNewChan),128))';
%     HDR.nVarSampleDivider = mat2cell(ones(1,NNewChan),1,ones(1,NNewChan));
%     HDR.vertPrecision = mat2cell(ones(1,NNewChan),1,ones(1,NNewChan));
%     HDR.nSize = mat2cell(ones(1,NNewChan)*2,1,ones(1,NNewChan));
%     HDR.nType = mat2cell(ones(1,NNewChan)*2,1,ones(1,NNewChan));
%     HDR.dFirstTimeOffset = 0;
%     HDR.mmt = zeros(size(HDR.mmt));
    %HDR.mmt(1:NNewChan) = 1;
    %HDR.mmtChan = ones(size(HDR.mmtChan)).*-1;


    %DATA = DATA';
    %SaveAcq(TargetFilePath,HDR,DATA,[],[],[],[],[],[],0);

%     if CheckWriteStatus
%         HDR2 =ReadAcqHeader(TargetFilePath);
%         FNames1 = fieldnames(HDR);
%         FNames2 = fieldnames(HDR2);
%         for FieldInd = 1:length(FNames1)
%             if strcmp(FNames1{FieldInd},FNames2{FieldInd})
%                 Tmp1 = getfield(HDR1,FNames1{FieldInd});
%                 Tmp2 = getfield(HDR2,FNames2{FieldInd});
%                 if isnumeric(Tmp1)
%                     try
%                         fprintf(1,['HDR1: ',FNames1{FieldInd},': ',num2str(Tmp1)]);
%                         fprintf(1,['   HDR2: ',FNames2{FieldInd},': ',num2str(Tmp2)]);
%                         fprintf(1,'\n');
%                     catch
%                     end
%                 else
%                     try
%                         fprintf(1,['HDR1: ',FNames1{FieldInd},': ',Tmp1]);
%                         fprintf(1,['   HDR2: ',FNames2{FieldInd},': ',Tmp2]);
%                         fprintf(1,'\n');
%                     catch
%                     end
%                 end
%             end
%         end
%     end

%=================================
%   ChooseDataRoot
%=================================
elseif strcmp(action,'ChooseDataRoot')

    gList = get(hANSLAB2BPFig,'userdata');
    data_dir = ANSLABDefPath(1);
    data_dir=uigetdir(strrep(data_dir,'*',''), 'Pick data directory:');
    ANSLABDefPath(2,[data_dir,filesep]);
    set(gList.hDataRoot,'String',data_dir);


%=================================
%   TakeSRsetto
%=================================
elseif strcmp(action,'TakeSRsetto')

    gList = get(hANSLAB2BPFig,'userdata');
    if get(gList.hTakeSRsetto,'Value')
        set(gList.hTakeSRAcq,'Value',0);
        set(gList.hTakeSRRsp,'Value',0);
        set(gList.hSRsettoSR,'Enable','on');
        set(gList.hSetToSR,'Enable','on');
    end
%=================================
%   TakeSRAcq
%=================================
elseif strcmp(action,'TakeSRAcq')

    gList = get(hANSLAB2BPFig,'userdata');
    if get(gList.hTakeSRAcq,'Value')
        set(gList.hTakeSRRsp,'Value',0);
        set(gList.hTakeSRsetto,'Value',0);
        set(gList.hSRsettoSR,'Enable','off');
        set(gList.hSetToSR,'Enable','off');
    end

%=================================
%   TakeSRRsp
%=================================
elseif strcmp(action,'TakeSRRsp')


    gList = get(hANSLAB2BPFig,'userdata');
    if get(gList.hTakeSRRsp,'Value');
        set(gList.hTakeSRAcq,'Value',0);
        set(gList.hTakeSRsetto,'Value',0);
        set(gList.hSRsettoSR,'Enable','off');
        set(gList.hSetToSR,'Enable','off');
    end


%=================================
%   EnablePaddMaxPoints
%=================================
elseif strcmp(action,'EnablePaddMaxPoints')

    gList = get(hANSLAB2BPFig,'userdata');
    Status = get(gList.hPaddMax,'Value');
    if Status
        set(gList.hPaddNZeros,'Enable','on','BackgroundColor',[1 1 1]);
    else
        set(gList.hPaddNZeros,'Enable','off','String','','BackgroundColor',[0.9 0.9 0.9]);
    end

%=================================
%   PaddBoth
%=================================
elseif strcmp(action,'PaddBoth')

    gList = get(hANSLAB2BPFig,'userdata');
    Status = get(gList.hPaddBoth,'Value');
    if Status
        set(gList.hPaddBegin,'Value',0);
        set(gList.hPaddEnd,'Value',0);
    end

%=================================
%   PaddBegin
%=================================
elseif strcmp(action,'PaddBegin')

    gList = get(hANSLAB2BPFig,'userdata');
    Status = get(gList.hPaddBegin,'Value');
    if Status
        set(gList.hPaddEnd,'Value',0);
        set(gList.hPaddBoth,'Value',0);
    end

%=================================
%   PaddEnd
%=================================
elseif strcmp(action,'PaddEnd')

    gList = get(hANSLAB2BPFig,'userdata');
    Status = get(gList.hPaddEnd,'Value');
    if Status
        set(gList.hPaddBegin,'Value',0);
        set(gList.hPaddBoth,'Value',0);
    end

%=================================
%   Cancel
%=================================
elseif strcmp(action,'Cancel');

    close(hANSLAB2BPFig);

%=================================
%   Reset
%=================================
elseif strcmp(action,'Reset');

    close(hANSLAB2BPFig);
    ANSLAB2BP;


%=================================
%   EnablePaddZeros
%=================================
elseif strcmp(action,'EnablePaddZeros');


    gList = get(hANSLAB2BPFig,'userdata');
    Status = get(gList.hPaddWithZeros,'Value');
    if Status
        set(gList.hPaddMax,'Enable','on');
        set(gList.hPaddNZeros,'Enable','on');
        set(gList.hPaddPoints,'Enable','on');
        set(gList.hPaddBoth,'Enable','on');
        set(gList.hPaddBegin,'Enable','on');
        set(gList.hPaddEnd,'Enable','on');
    else

        set(gList.hPaddMax,'Enable','off');
        set(gList.hPaddNZeros,'Enable','off');
        set(gList.hPaddPoints,'Enable','off');
        set(gList.hPaddBoth,'Enable','off');
        set(gList.hPaddBegin,'Enable','off');
        set(gList.hPaddEnd,'Enable','off');
    end
%=================================
%   EnableAdjSR
%=================================
elseif strcmp(action,'EnableAdjSR');

    gList = get(hANSLAB2BPFig,'userdata');
    Status = get(gList.hAdjustSR,'Value');
    if Status
        set(gList.hTakeSRRsp,'Enable','on');
        set(gList.hTakeSRAcq,'Enable','on');
        set(gList.hTakeSRsetto,'Enable','on');
    else
        set(gList.hTakeSRRsp,'Enable','off');
        set(gList.hTakeSRAcq,'Enable','off');
        set(gList.hTakeSRsetto,'Enable','off');
    end
%=================================
%   EnableTakeAll
%=================================
elseif strcmp(action,'EnableTakeAll');

    gList = get(hANSLAB2BPFig,'userdata');
    set(gList.hTakeOnly,'Value',~get(gList.hTakeAll,'Value'));
    if get(gList.hTakeOnly,'Value')
        set(gList.hTakeA,'Enable','on');
        set(gList.hTakeB,'Enable','on');
        set(gList.hTakeC,'Enable','on');
        set(gList.hTakeD,'Enable','on');
        set(gList.hTakeE,'Enable','on');
        set(gList.hTakeF,'Enable','on');
        set(gList.hTakeG,'Enable','on');
        set(gList.hTakeH,'Enable','on');
        set(gList.hTakeI,'Enable','on');
        set(gList.hTakeL,'Enable','on');
        set(gList.hTakeN,'Enable','on');
        set(gList.hTakeO,'Enable','on');
        set(gList.hTakeP,'Enable','on');
        set(gList.hTakeR,'Enable','on');
        set(gList.hTakeS,'Enable','on');
        set(gList.hTakeT,'Enable','on');
    else
        set(gList.hTakeA,'Enable','off');
        set(gList.hTakeB,'Enable','off');
        set(gList.hTakeC,'Enable','off');
        set(gList.hTakeD,'Enable','off');
        set(gList.hTakeE,'Enable','off');
        set(gList.hTakeF,'Enable','off');
        set(gList.hTakeG,'Enable','off');
        set(gList.hTakeH,'Enable','off');
        set(gList.hTakeI,'Enable','off');
        set(gList.hTakeL,'Enable','off');
        set(gList.hTakeN,'Enable','off');
        set(gList.hTakeO,'Enable','off');
        set(gList.hTakeP,'Enable','off');
        set(gList.hTakeR,'Enable','off');
        set(gList.hTakeS,'Enable','off');
        set(gList.hTakeT,'Enable','off');
    end
%=================================
%   EnableTakeOnly
%=================================
elseif strcmp(action,'EnableTakeOnly');

    gList = get(hANSLAB2BPFig,'userdata');
    set(gList.hTakeAll,'Value',~get(gList.hTakeOnly,'Value'));

    if get(gList.hTakeOnly,'Value')
        set(gList.hTakeA,'Enable','on');
        set(gList.hTakeB,'Enable','on');
        set(gList.hTakeC,'Enable','on');
        set(gList.hTakeD,'Enable','on');
        set(gList.hTakeE,'Enable','on');
        set(gList.hTakeF,'Enable','on');
        set(gList.hTakeG,'Enable','on');
        set(gList.hTakeH,'Enable','on');
        set(gList.hTakeI,'Enable','on');
        set(gList.hTakeL,'Enable','on');
        set(gList.hTakeN,'Enable','on');
        set(gList.hTakeO,'Enable','on');
        set(gList.hTakeP,'Enable','on');
        set(gList.hTakeR,'Enable','on');
        set(gList.hTakeS,'Enable','on');
        set(gList.hTakeT,'Enable','on');
    else
        set(gList.hTakeA,'Enable','off');
        set(gList.hTakeB,'Enable','off');
        set(gList.hTakeC,'Enable','off');
        set(gList.hTakeD,'Enable','off');
        set(gList.hTakeE,'Enable','off');
        set(gList.hTakeF,'Enable','off');
        set(gList.hTakeG,'Enable','off');
        set(gList.hTakeH,'Enable','off');
        set(gList.hTakeI,'Enable','off');
        set(gList.hTakeL,'Enable','off');
        set(gList.hTakeN,'Enable','off');
        set(gList.hTakeO,'Enable','off');
        set(gList.hTakeP,'Enable','off');
        set(gList.hTakeR,'Enable','off');
        set(gList.hTakeS,'Enable','off');
        set(gList.hTakeT,'Enable','off');
    end

end






return
