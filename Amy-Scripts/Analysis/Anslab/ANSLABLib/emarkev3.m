% emarkev3.m   mark event 3

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
n=find(event3>s1 & event3<s2);
for j=1:length(n)
  valx=event3(n(j))/scalefact;
  plot([valx;valx],[yax1+as/2;yax2-as],':b')   % display vertical line
 if 0
   if comptype<2
       cmdstr=['text(''Position'',[valx yax1+(as/2)*(comptype<2)],''String'','' E3'')'];
    else
       cmdstr=['text(valx,yax1+(as/2)*(comptype<2),'' E3'')'];
    end;
  eval(cmdstr);
 end;
end;
