% kdist.m   examine the x- and y-distances of two points: D


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

count=0;
while 1   % esc
[i1,i2,i3]=ginput(2);
if i3(1)==27 | i3(2)==27 break; end;   % esc
i=i1(2)-i1(1); j=i2(2)-i2(1);
ii=s2-s1; count=count+1;
valx=(s1+(s2-s1)*.6)/scalefact;
str=['X-dist = ',num2str(i),'    Y-dist = ',num2str(j)];
if spec
  eval(['text(.65,.46-count*.03,''',str,''',''sc'')']); %other axis
else eval(['text(valx,yax2-count*as,''',str,''')']); end;
  plotyes=0; z=999;
end;
