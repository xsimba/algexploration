% ksavedata.m  save specified data: s

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

z=999;
format loose;

disp(' '); disp('List variables?');
m1='no'; m2='yes';
i=menue(m1,m2);
if i~=1 who; end;

disp(' '); disp('Save which variables (delimited by <space> or <return>):');
disp(['(To save the variables   ', SAVEVAR, '   select 0 [zero].)']);
varstr=[];
while 1
i=input('=> ','s');
if isempty(i) break; end;
if strcmp(i,'0')
   varstr=SAVEVAR;
   else varstr=[varstr,' ',i];
end;
end;

disp(' '); disp('List current directory?');
m1='no'; m2='yes';
i=menue(m1,m2);
if i~=1 dir; end;

disp(' '); disp(' To change the directory you can use the top menue: Workspace - Load');

disp(' '); disp(['Save ',varstr]);
filestr=input('to file named (.mat is added):  ','s');

disp(' '); disp('ASCII file? (Use only when all variables have same size)');
m1='no'; m2='yes';
i=menue(m1,m2);
if i==1
   cmdstr=['save ',filestr,'.mat ',varstr];
else
   cmdstr=['save ',filestr,'.mat ',varstr,' /ascii'];
end;
disp(cmdstr);
eval(cmdstr);
