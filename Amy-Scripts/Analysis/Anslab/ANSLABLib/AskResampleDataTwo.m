function [DataResampled1,DataResampled2,OriginalSR] = AskResampleDataTwo(Data1, Data2, TargetSR)

%   AskResampleDataTwo

%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

DefaultID = 'RespTwo';
prompt={char('Data may need to be resampled for further analysis.','Please enter the sampling rate in Hz of the given data :','')};
name='sampling rate:';
numlines=1;
if ~isempty(DefaultID)
    DefaultPath = strrep(which('AskResampleData'),'AskResampleData.m',['ResampleDefault',DefaultID,'.mat']);
    if exist(DefaultPath)
        load(DefaultPath,'DefVal');
    end
    if exist('DefVal')
        if ~isempty(DefVal)
            defaultanswer={num2str(DefVal)};
        else
             defaultanswer={num2str(TargetSR)};
        end
    else
        defaultanswer={num2str(TargetSR)};
    end
else
    defaultanswer={num2str(TargetSR)};
end

answer=inputdlg(prompt,name,numlines,defaultanswer);

OriginalSR = str2num(answer{1});
DefVal = OriginalSR;

if ~isempty(DefaultID)
    save(DefaultPath,'DefVal');
end

OriginalSR = str2num(answer{1});
DataResampled1 = resample(Data1,TargetSR,OriginalSR);
DataResampled2 = resample(Data2,TargetSR,OriginalSR);
 
   
return