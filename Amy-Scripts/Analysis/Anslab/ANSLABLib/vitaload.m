%**** load channels from Vitaport binary file


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


fileonhd=0;

while ~fileonhd   % file exists?
m1='Flying  phobia data - patients (1 file: a)';
m2='Flying  phobia data - controls (1 file: amb)';
m3='Driving phobia data                (3 files: b1, b2, ex)';
m4='Flying phobia - patients calibration (2 files: c1, c2)';
m5='Flying phobia - controls calibration (2 files: cal1, cal2)';
m6='Flumazenil Calibration';
m7='Driving Phobia Calibration';
m8='Relaxation Calibration';
m9='Quiet Sitting Calibration - SPIR in WORD-format';
m10='Quiet Sitting Calibration - SPIR in BYTE-format';
m11='Quiet Sitting Calibration - 10 channels definition file';
m12='Relaxation SCL + Marker';


%m3='Spider  phobia data';
study=menue(m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12);
cali=[];
%     r  i   m  c mk s  c m r  p  b  recorded channels: samplerates
%sr= [24 384 3    6  6  3 3 24 24 192];  % spider phobia

if study==1 | study==2
 sr= [12   2 4 .1 2  6 .1 4 12 1];       % flying phobia
 data_dir='c:\fp_data';
 kan=[1 2 3 6 8 9];
 if study==1
   m1='a';
   sstr='fp';
 else
   m1='amb';
   sstr='fc';
 end;
end;
if study==3
 sr= [12   2 4 .1 2  6 .1 4 12];       % driving phobia
 data_dir='d:\dp_data';
 kan=[1 2 3 6 8 9];
 m1='b1'; m2='b2'; m3='ex';
 sstr='dp';
end;
if study==4 | study==5
sr= [12 384 3 3  6  6  3 3 12 12 1];  % calibration def-file
 data_dir='c:\fp_data';
 kan=[1 9 10];
 if study==4
   m1='c1'; m2='c2';
   sstr='fp';
 else
   m1='cal1'; m2='cal2';
   sstr='fc';
 end;
end;
if study==6
sr= [12 12 12];  % calibration def-file
 data_dir='e:\caldpflu';
 kan=[1 2 3];
 m1='calib1'; m2='calib1';
 sstr=[];
end;
if study==7
sr= [12 12 12];  % calibration def-file
 data_dir='e:\caldpflu';
 kan=[1 2 3];
 m1='c1'; m2='c2';
 sstr='dp';
end;

if study==8
 sr= [24 384 3 3  6  6  3 3 24 24];  % calibration def-file
 kan=[1 9 10];
 data_dir='d:\relaxdat';
 n1='Panic'; n2='GAD'; n3='Control';
 diag=menue(n1,n2,n3);
 m1=[]; m2=[];
 if diag==1 sstr='rcp'; end;
 if diag==2 sstr='rcg'; end;
 if diag==3 sstr='rcc'; end;
end;

if study>8 & study<12
if study==11
  sr= [12 384 3 3  6  6  3 3 12 12];
  kan=[1 9 10];
else
 sr= [12 12 12];  % calibration def-file
 kan=[1 2 3];
end;
data_dir='d:\quietdat';
kan=[1 2 3];
disp('Files: 1=Control,  2=GAD,  3=Panic, 4=New');
m1='concal'; m2='gadcal'; m3='pancal'; m4='newcal';
sstr='';
end;

if study==12
 sr= [24 384 3 3  6  24  3 3 24];  % def-file
 kan=[5 6];
 data_dir='d:\relaxdat';
 n1='Panic'; n2='GAD'; n3='Control';
 diag=menue(n1,n2,n3);
 m1=[]; m2=[];
 if diag==1 sstr='rep'; end;
 if diag==2 sstr='reg'; end;
 if diag==3 sstr='rec'; end;
end;


%*** Initialization
format compact;
channel=[]; chdef=[]; doffs=[]; dlen=[]; mval=[]; chflg=[]; ampl=[]; hilo=[]; hard=[]; offset=[];

%*** define file and interval
disp('Number of subject:');
i=input('==>  ');
subjn=i;
subject=twostr(i);

if (study==8) & (diag==1) & (subjn<3)  %different def-file
   sr= [12 12 12];  kan=[1 2 3];
end;

if study==1 | study==3 | study==4 | study==7 | study==8 | study==12
 disp('Day to analyze [1]');
 day=input('==>  ');
 if isempty(day) day=1; end;
else
 day=[];
end;
disp('File to analyze [1]');
row=input('==>  ');
if isempty(row) row=1; end;
eval(['expph=m',int2str(row),';']);
disp(['Phase of experiment: ',expph]);

%*** check file existence
fileonhd=1;
chd(data_dir);

if study>8 & study<12
   datastr=[expph,subject];
else
   datastr=[sstr,subject,int2str(day),expph];
end;
str=[datastr,'.dat'];
disp(['Loading all data from file "',str,'"']);
if ~existhd(str)
  disp('File does not exist (in this directory)');
  fileonhd=0;
end;

if study<4   %*** load marker file
str1=[sstr,subject,int2str(day),'m.mat'];
if ~existhd(str1)
  disp(['Marker file ',sstr,'mark',subject,' is missing']);
  fileonhd=0;
else
eval(['load ',str1]);
disp([str1,' loaded']);
end;
end;

if fileonhd
%*** read in header of binary file
headread;   % invoke script-file
%if row<4 names, end;
lensec=dlen ./sr;
ref=lensec(2);  % length in sec. of HR recording
disp(['Length of the complete sampled interval: ',num2str(ref/60),' min']);


%*** read in all necessary channels (apply offset, but not multiplication factor)
for i=1:length(kan)
if study~=10 & dasize(kan(i))==2
   % (format word: Skin conductance or Spirometer)
   status=fseek(fid,hdlen+doffs(kan(i)),'bof');
   eval(['A=fread(fid,[2,dlen(kan(i))/2]);']);
   eval(['K',twostr(kan(i)),'=A(1,:)*256+A(2,:)-offset(kan(i));']);
   eval(['K',twostr(kan(i)),'=K',twostr(kan(i)),''';']);
else
   status=fseek(fid,hdlen+doffs(kan(i)),'bof');
   eval(['K',twostr(kan(i)),'=fread(fid,dlen(kan(i)))-offset(kan(i));']);
end;
%eval(['K',twostr(i),'=(K',twostr(i),')*mulfac(i)/divfac(i);']);
end;

end;  %if fileonhd

end;
%*** if study ==6

end;  %while ~fileonhd

end;
%*** if study ==6

if study>5 & study<12
K09=K02; K10=K03;
end;

if study>7 & study<12
K01=decfast(K01,2);
K09=decfast(K09,2);
K10=decfast(K10,2);
end;


