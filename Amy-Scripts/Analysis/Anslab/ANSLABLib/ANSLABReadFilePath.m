function [FilePath] = ANSLABReadFilePath(FilterSpec,MessageString)

%   ANSLABReadFilePath

%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

if nargin<2;MessageString = [];end
if nargin<1;FilterSpec = [];end


if isempty(MessageString); MessageString= 'Please choose file:';end
if isempty(FilterSpec);FilterSpec = '*';end



InitPath = ANSLABDefPath(1);
[File,Path] = uigetfile([InitPath,FilterSpec],MessageString);
if isequal(File,0)|isequal(Path,0);FilePath = [];return;end
ANSLABDefPath(2,Path);
FilePath = [Path,File];



return