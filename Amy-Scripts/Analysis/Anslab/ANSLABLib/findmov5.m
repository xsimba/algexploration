% Reduction of activity-channel
%
%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

% Initialization
sr=400;
ep=4;
if ~exist('bno')
    bno=1;
    bplotyes=1;
end
if ~bno
    rateyes=0;
    rat=0;
    moveedit=0;
end

if bno|bplotyes
    cfig, cfig, cfig
    figure(1)
    if screen>800
        set(1,'Position',[272 337 748 389]);
    else
        set(1,'Position',[61 278 735 280]);
    end
    figure(2)
    if screen>800
        set(2,'Position',[272 30 748 299]);
    else
        set(2,'Position',[60 31  732 246]);
    end
    figure(1)
end

if filtyes
    %*** Highpass filter, then moving average for unprocessed channel, typically not needed
    disp('Highpass filter, then moving average for rectified channel');
    AC=filtlow(AC,.5,sr,3);
    AC=filthann(abs(AC),3);
end;

%*** Rectify and resample to allign with other variables (1/4sec epochs)
act0=decfast(abs(AC-mean(AC)),sr/ep);


if bno | bplotyes

    figure(2)
    t1=(1:length(act0))/ep;
    xp=(max(t1)-min(t1))/25; %x plus: add free space between data and axis to match outrect
    xl1=t1(1)-xp;
    xl2=max(t1)+xp;
    plot(t1,act0);
    axisx(xl1,xl2);
    axisy(0, max ([max(nanrem(act0)) 0.5])  )
    xlabel('time [sec]')
    title('Activity');
    drawnow

    figure(1)
    t1=(1:length(AC))/sr;
    xp=(max(t1)-min(t1))/25; %x plus: add free space between data and axis to match outrect
    xl1=t1(1)-xp;
    xl2=max(t1)+xp;
    plot(t1,AC);
    axisx(xl1,xl2);
    title('Activity raw signal, zoom repeatedly to inspect (left click, drag mouse), unzoom back with right clicks)');
    xlabel('time [sec]')
    drawnow
    if bno
        zoomrb,
    end

    if bno
        edityes=1;

        while edityes
            edityes=input('Editing: no [0, default],  yes [1]  ==> ');
            if isempty(edityes)
                edityes=0;
            end

            if edityes
                disp('Edit rectified activity signal')
                figure(2)
                t1=(1:length(act0))/ep;
                [act0,noutind]=outrect(act0,t1,1);
                figure(1)
                t1=(1:length(AC))/sr;
                xp=(max(t1)-min(t1))/25; %x plus: add free space between data and axis to match outrect
                xl1=t1(1)-xp;
                xl2=max(t1)+xp;
                plot(t1,AC);
                axisx(xl1,xl2);
                title('Activity raw signal, zoom repeatedly to inspect (left click, drag mouse), unzoom back with right clicks)');
                xlabel('time [sec]')
                zoomrb
            end
        end
    end
end

