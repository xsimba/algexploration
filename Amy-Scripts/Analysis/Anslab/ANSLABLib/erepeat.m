% erepeat.m  repeat skip and move fast

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if (zold==29 & comptype) | (zold==50 & ~comptype)
  ii=s2-s1; s1=s2; s2=s2+ii;
  if s2>leny disp('End of file file reached.');
    repeat=0; break; break; end;
end;
if zold==93   % goto right event: ]
   evscani=evscani+skipfact;
   j=length(evscan);
   if evscani>j  disp('End of file file reached.');
      repeat=0; break; break;
   else
      ii=s2-s1;
      s1=evscan(evscani)-ii*skiploc; s2=s1+ii;
      evnumyes=1;
end; end;
