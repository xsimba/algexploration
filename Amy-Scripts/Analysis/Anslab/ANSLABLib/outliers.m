function vn = outliers(v,sd,vmin,vmax);

%*************  Returns indices of unplausible outliers  **********************
%
%     function vn = outliers(v,sd,vmin,vmax);
%     v:       Values of epoch-data.
%     sd:      Standard Deviations around the mean that are valid.
%              sd=3 if not specified.
%     vmin:    Optional;
%              physiologically impossible small values [ < vmin ] are removed
%              before mean and SD for outliers are computed  (better estimate!).
%     vmax:    Optional;
%              physiologically impossible big values [ > vmax ] are removed
%              before mean and SD for outliers are computed  (better estimate!).
%              Can only be set if vmin has been specified.
%
%     Intervals > 3 SD beyond the mean are set to NaN.
%
%*******************************************************************************

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


n=[];
if exist('sd')~=1 sd=3;
else
     % max & min threshold for values (if specified)
   if (exist('vmin')==1) & (exist('vmax')==1)
      n=find(v>vmax | v<vmin);

   elseif exist('vmin')==1
      n=find(v<vmin);
   end;
end;

  % physiologically impossible values are removed before mean is computed
vok=v;
if ~isempty(n) vok(n)=[]; end;
vok(isnan(vok))=[];


n=find(abs(v-mean(vok)) > sd*std(vok));    % value outliers

vn=v;
if ~isempty(n) vn(n)=ones(1,length(n))*NaN; end;

