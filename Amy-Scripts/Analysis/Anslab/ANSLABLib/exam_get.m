% Ask for variables to start EXAM 2.0

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

v1=input('Variable 1 => ','s');
if isempty(v1) disp('Specify variable name!'); break; break; end
eval(['var1=',v1,';']);

v2=input('Variable 2 [return=none] => ','s');
if ~isempty(v2) eval(['var2=',v2,';']); end;

v3=input('Variable 2 [return=none] => ','s');
if ~isempty(v3) eval(['var3=',v3,';']); end;

v4=input('Variable 2 [return=none] => ','s');
if ~isempty(v4) eval(['var4=',v4,';']); end;

defblank

samplerate=input('New samplerate => ');

def
exam


