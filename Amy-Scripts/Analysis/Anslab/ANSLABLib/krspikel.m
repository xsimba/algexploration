% krspikel.m   add R-spike to the left: &

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


event1(find(~diff(event1)))=[]; %delete double events

[i1,i2,i3]=ginput(1);
if i3==1

[i,ind1]=min(abs((i1*scalefact)-event1));  %next event


ind=[ind1 ind1+1];
evnew=event1(ind(1))-(event1(ind(2))-event1(ind(1)));

event1=sort([event1;evnew']);

event1(find(~diff(event1)))=[]; %delete double events

end;
plotyes=1;
