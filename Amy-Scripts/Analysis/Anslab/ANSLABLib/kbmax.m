% kbmaxa: find max of breaths automatically: h

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

dist=16; back=12; slope=.04; win=8;
numbonplot=1;   % lag between consecutive breath numbering on plot

newthresh=1;
while newthresh

m1='Find maxima with default thresholds';
m2='Change default thresholds';
newthresh=menue(m1,m2);
if ~newthresh break; break; end;

if newthresh==2
dist=input('Minimal distance between maxima [16] ==> ');
if isempty(dist) dist=16; end;
back=input('Comparison to a point back [12] ==> ');
if isempty(back) back=12; end;
slope=input('Slope from there at least to be valid [.05] ==> ');
if isempty(slope) slope=.05; end;
win=input('Window for search of maxima in other channels [8] ==> ');
if isempty(win) win=8; end
end;

xmax1=[]; xmax2=[];
xmin1=[]; xmin2=[];
mini=[];bm=[]; bmin=[];

x1=var1(s1:s2);
xmax1=findmax(x1,dist,back,slope)';  %thorax maxima
x2=var2(s1:s2);

for i=1:length(xmax1)
  n=xmax1(i)-win:xmax1(i)+win+2;
  n(n<1)=[]; n(n>length(x1))=[];  % range check
  [j,xmax2(i)] = max(x2(n));
end;
xmax2=xmax1-win-1+xmax2;


%*** find between minima
i1=xmax1;  % max index

for i=1:length(i1)-1
    n= i1(i):i1(i+1);
    [j,xmin2(i)] = min(x2(n));
    [mini(i),xmin1(i)] = min(x1(n));
end;

i1(length(i1))=[];
xmin1=i1+xmin1-1;
xmin2=i1+xmin2-1;

%** first maxima not valid for breath
xmax1(1)=[];
xmax2(1)=[];

out=1;
while out

%*** build maxima/minima-vector, row 3 is absolute index
bmin  =[ col(x1(xmax1))' ; col(x2(xmax2))' ; col((xmax1+s1-1))' ];
bm=[ col(x1(xmin1))' ; col(x2(xmin2))' ; col((xmin1+s1-1))' ];

%*** plot found maxima and minima
subplot(1,1,1)
t=1:length(x1);
plot(t,x1,t,x2)
axis([ t(1) t(length(t)) min([x1(:);x2(:)]) max([x1(:);x2(:)]) ])
title(['Respitrace units: thorax (y), abdomen (m) -  ',int2str(length(xmax1)),' valid inspirations']);
hold on
plot(xmax1,x1(xmax1),'oy');
plot(xmax2,x2(xmax2),'om');
plot(xmin1,x1(xmin1),'oy');
plot(xmin2,x2(xmin2),'om');
hold off

% number the breathing cycles on plot
for i=1:numbonplot:length(xmax1)
str=int2str(i);
cmdstr=['text(''Position'',[xmin1(i) x1(xmin1(i))-.05],''String'','' ',str,''')'];
eval(cmdstr);
end;
hold off

out=input('Take out cycle no. [end] ==> ');
if isempty(out) | ~out
   out=0; break; break;
else
   xmax1(out)=[]; xmax2(out)=[];
   xmin1(out)=[]; xmin2(out)=[]; mini(out)=[];
end;

end;  %while out

end;  %while win






