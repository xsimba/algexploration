% findco2 plateaus -> pCO2
%function [mt,mv] = fmaxco2(y,area,back,rise,mingap,forward,rise2);
%[maxt,maxv]=fmaxco2(K09,24,12,6,16,10,20);

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

%y=CO;
y=K09*.74;   % in mmHg for QS
CO=y;


area=48;
back=6;  % .25 sec back
%rise=3;  % plateau condition:  rise <= 3 !

rise=3;  % rise <= 3mmHg, less restrictive, for QS

mingap=48;

%absmin=30; %absolute minimum
absmin=20; %mmHg for QS


%forward=24;
%risef=20;

%*** Partition y into windows
y=y(length(y):-1:1);     % revert y => maxima at the end
len=length(y);    % fill y with 0 to a length of multiples of area
zer=area-rem(len,area);
if rem(len,area)==0 zer=0; end;
y(len+1:len+zer)=zeros(zer,1);

Y=reshape(y,area,(len+zer)/area);
[mv,mt]=max(Y);

%*** 'Reshape' maxtime-vector
mt=mt+(0:length(mt)-1)*area;
lenm=length(mt);
if mt(lenm)>len mv(lenm)=[]; mt(lenm)=[]; end;

%*** reverse vectors again
mt=length(y)-mt-zer+1;
mt=mt(length(mt):-1:1);
mv=mv(length(mv):-1:1);
y=CO;

%*** Get rid of values that are too small
n=mv<absmin;
if length(n)
mt(n)=[];
mv(n)=[];
end;

%*** Choose bigger one of two close max
% find indices of max that are too close, n: indices of smaller values
n=maxdist(mt,mv,mingap);
if length(n)
mt(n)=[];
mv(n)=[];
end;

if length(mt)
%*** Maxima may be not valid because of too short rise-time
if mt(1)<=back  mt(1)=[]; mv(1)=[]; end;
i=length(mt);
if mt(i)+1>len  mt(i)=[]; mv(i)=[]; end;
end;

if length(mt)
%*** Check for general slope backward
slope=mv-y(mt-1)';
n=slope>=0;
mt=mt(n);
mv=mv(n);
end;

if length(mt)
%*** Check for general slope forward
slope=mv-y(mt+1)';
n=slope>=0;
mt=mt(n);
mv=mv(n);
end;

if length(mt)
%*** Check for valid slope backward
slope=mv-y(mt-back)';
n=slope<rise;
mt=mt(n);
mv=mv(n);
end;

if 0
if length(mt)
%*** Check for valid slope forward
slope=mv-y(mt+forward)';
n=slope>risef;
mt=mt(n);
mv=mv(n);
end;
end;

if length(mt)
%*** check for missing between breath
ni=zeros(1,length(mt));
for i=1:length(mt)-1
  n=y(mt(i):mt(i+1));
  if min(n)>4
     ni(i)=1;
  end;
end;
mt(ni)=[];
mv(ni)=[];
end;

if length(mt)
%*** check for incomplete breath at end
i=length(mt);
n=mt(i):length(y);
if min(y(n))>mv(i)-10
   mt(i)=[]; mv(i)=[];
end;
end;

mt=mt';
mv=mv';

%*****  plot preliminary results
mtx=mt;
mvx=mv;

men=1;

while men


if 0
figure(2);
clg;
subplot(211);
plot(CO,'g');
hold on
plot(mt,mv,'*');
hold off;
axis([1 length(y) min(y) max(y)+3]);
subplot(212);
end;

hold on
plot(mt,mv);
axis([1 length(y) min(mv)-1 max(mv)+1]);
plot(mt,mv,'o');
title(['pCO2=',num2str(mean(nanrem(mv)))]);
hold off
end;


m1='acceptable';
m2='exclude pCO2 outliers';
m3='restore original data';
m4='remove a point';
m5='set a point to nearest maximum';
men=menue(m1,m2,m3,m4,m5);

if men==4
   [i1,i2,i3]=ginput(1);
   if i3==1
     [i,ind]=min(abs(mt-i1));
     mt(ind)=[]; mv(ind)=[];
   end;
end;

if men==5
   win=10;
   [i1,i2,i3]=ginput(1);
   if i3==1
      i1=round(i1);
      n=(i1-win:i1+win);
      [mn,in]=max(y(n));
      if in==1 | in==2*win+1
         disp('Found maximum is at the edge. Try again by clicking more exactly.');
      else
         i1=i1-win+in-1;
         [mt,i]=sort([mt;i1]);
         mv=y(mt);
      end;
   end;
end;

if men==1 men=0; end;

if men==3
mt=mtx;
mv=mvx;
end;

if men==2
   mv=outplot(mv,mt,-3);
end;


end;
end;

else
end; %if i==2

end; %while men

pCO2t=mt';
pCO2=mv';

yshort=ipshort(y,2);  %for fast display of raw data later

savestr='p pCO2 pCO2t yshort';

