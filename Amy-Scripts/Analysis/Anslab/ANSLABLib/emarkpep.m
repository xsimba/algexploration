% emarkpep.m   mark ICG events with circles, exlcuding deleted beats

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
n=find(Bt>=s1 & Bt<=s2);    % changed iont to icg_time
if ~isempty(n)
for j=n(1):n(length(n))
  if ~deleted(j)
    plot(Bt(j)/scalefact,var1(Bt(j)),'oc')
  end
end;
end;
n=find(Zt>=s1 & Zt<=s2);
if ~isempty(n)
for j=n(1):n(length(n))
  if ~deleted(j)
    plot(Zt(j)/scalefact,var1(Zt(j)),'or')
  end
end;
end;
n=find(Xt>=s1 & Xt<=s2);
if ~isempty(n)
for j=n(1):n(length(n))
  if ~deleted(j)
    plot(Xt(j)/scalefact,var1(Xt(j)),'og')
  end
end;
end;


