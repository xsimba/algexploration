% kaddvar.m   add or hide other variables: V

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
z=999;
disp(' ');
disp('The following variables are already defined (but maybe not graphed):');
disp(['1  ==>  ',title1str]);
disp(['2  ==>  ',title2str]);
disp(['3  ==>  ',title3str]);
disp(['4  ==>  ',title4str]);
disp('Press any key'); pause;
m1 = 'Define variable 1';
m2 = 'Define variable 2';
m3 = 'Define variable 3';
m4 = 'Define variable 4';
m5 = 'Do not graph variable 2';
m6 = 'Do not graph variable 3';
m7 = 'Do not graph variable 4';
m8 = 'Graph variable 2 again';
m9 = 'Graph variable 3 again';
m10 = 'Graph variable 4 again';
i=menue(m1,m2,m3,m4,m5,m6,m7,m8,m9,m10);
if i>0 & i<5
  varstr=['var',int2str(i)]; str2=['title',int2str(i),'str'];
  disp(' ');
  disp('Name of variable in workspace:');
  str=input('==>  ','s'); disp(' ');
  disp('Desired name of the variable in title:');
  titstr=input('==>  ','s');
  if isempty(titstr) titstr=str; end;
  if exist(str)==1
    eval([varstr,'=',str,';']);
    eval([str2,'=''',titstr,''';']);
    eval(['ltv',int2str(i),'=lt',int2str(i),';']);
    eval(['secvar',int2str(i),'=1;']);
   else disp('This variable does not exist in workspace.'); pause; end;
elseif i>7
  eval(['secvar',int2str(i-6),'=1;']);
  eval(['ltv',int2str(i-6),'=lt',int2str(i-6),';']);
  disp('Desired name of the variable in title:');
  titstr=input('==>  ','s');
  str2=['title',int2str(i-6),'str'];
  eval([str2,'=''',titstr,''';']);
else
  if i
    eval(['secvar',int2str(i-3),'=0;']);
    eval(['ltv',int2str(i-3),'=[]']);
    eval(['title',int2str(i-3),'str=[];']);
  end;
end;
