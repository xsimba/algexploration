function [DataResampled1,DataResampled2, Acc, OriginalSR] = AskResampleDataResp(Data1, Data2, Acc, TargetSR)

%   AskResampleDataTwo

%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 




if isempty(Acc)
    prompt={char('Data may need to be resampled for further analysis.',['(The default for this algorithm is ',num2str(TargetSR),' Hz .)'],'Please enter the sampling rate in Hz of the given respiration data :','')};
    DefaultPath = strrep(which('AskResampleData'),'AskResampleData.m',['ResampleDefaultResp.mat']);
    if exist(DefaultPath)
        load(DefaultPath,'DefValResp');
    end
    if exist('DefValResp')
        if ~isempty(DefValResp)
            defaultanswer={num2str(DefValResp)};
        else
            defaultanswer={num2str(TargetSR)};
        end
    else
        defaultanswer={num2str(TargetSR)};
    end
else
    prompt={char('Data may need to be resampled for further analysis.','Please enter the sampling rate in Hz of the given respiration data :','');'Enter the sampling rate of the given accelerometer data:' };
    DefaultPath = strrep(which('AskResampleData'),'AskResampleData.m',['ResampleDefaultResp.mat']);
    if exist(DefaultPath)
        load(DefaultPath,'DefValResp','DefValAcc');
    end
    if exist('DefValResp')
        if ~isempty(DefValResp)
            defaultanswer{1}=num2str(DefValResp);
        else
            defaultanswer{1}=num2str(TargetSR);
        end
    else
        defaultanswer{1}=num2str(TargetSR);
    end
   if exist('DefValAcc')
        if ~isempty(DefValAcc)
            defaultanswer{2}=num2str(DefValAcc);
        else
            defaultanswer{2}=num2str(TargetSR);
        end
    else
        defaultanswer{2}=num2str(TargetSR);
   end
end
name='sampling rate:';
numlines=1;
answer=inputdlg(prompt,name,numlines,defaultanswer);
OriginalSR = str2num(answer{1});
DefValResp = OriginalSR;
if ~isempty(Acc)
    OriginalSRAcc = str2num(answer{2});
    DefValAcc = OriginalSRAcc;
else
    DefValAcc = [];
end
save(DefaultPath,'DefValResp','DefValAcc');


DataResampled1 = resample(Data1,TargetSR,OriginalSR);
DataResampled2 = resample(Data2,TargetSR,OriginalSR);
if ~isempty(Acc)
    Acc = resample(Acc,TargetSR,OriginalSRAcc);
else
    Acc = zeros(size(DataResampled1));
end
    
 
   
return