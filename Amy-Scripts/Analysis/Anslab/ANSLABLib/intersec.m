function v = intersec(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9)

% INTERSEC Form the intersection of 'sets'.
%   V = INTERSEC(X,Y) forms the intersection of the elements in X and Y,
%   removing repeated elements. X and Y may be any size, V will be
%   a sorted row vector.
%   The function accepts 0 to 10 input arguments,
%   for example U=INTERSEC(X) or U=INTERSEC(X,Y,Z).



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin==0
  v = [];
elseif nargin==1
  v = union1(x0(:).');
else
  if nargin>2
    arglist='x1';
    for k=3:nargin, arglist=[arglist ',x' num2str(k-1) ]; end
    x1 = eval(['intersec(' arglist ')']);
  end
  v = sort([union(x0) union(x1)]);
  n = length(v);
  if n > 0, v = v([0 ~(v(2:n)-v(1:n-1))]); end
end
