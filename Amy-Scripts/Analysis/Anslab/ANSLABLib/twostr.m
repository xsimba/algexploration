function str = twostr (x,digits);
%function str = twostr (x,digits);
% Changes vector of integers into strings with 'digits' [default=1]
% characters with leading 0's if needed.


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<2 digits=2; end;

x=x(:);
len=length(x);
str=tony('0',digits)';
str=tony(str,len);

for j=1:len

pos=sign(x(j));
i=abs(round(x(j)));
l=floor(log10(i)+.000000000001)+1;
if isnan(i)
    l=3;
end;
if isinf(l)
    l=1;
end;
if l>digits | ((l>digits-1)&(pos==-1))
   disp('twostr failed. Increase digits parameter!');
   d=d;
keyboard  %   break;
end;
if pos~=-1
str(j,:)=[strdup('0',digits-l)' int2str(i)];
else
str(j,:)=[strdup('0',digits-l-1)' '-' int2str(i)];
end;

end;
