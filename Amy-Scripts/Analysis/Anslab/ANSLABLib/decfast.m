function meany=decfast(y,area,nanyes);
% meany=decfast(y,area);
% shrinks a vector with factor area [default=2].
% mean of succeeding points of length area (fast!)
% nanyes: set 1 if areas with single NaN points are supposed to be not NaN
%         default=0
% takes some care of phase shifts, not as accurate as decimate or resample
% inaccuracy of phase shift is 1/area for area=even, 0 if area=odd
% e.g.: area=12 -> inaccuracy=1/12 phase shift into the past

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   anslab - Autonomic Nervous System Laboratory
%   � Copyright 2006 Frank Wilhelm & Peter Peyk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if nargin<2 area=2; end;
if nargin<3 nanyes=0; end;

if area>1 & round(area)==area
    y=y(:);

    % make phase shift of y
    s=ceil(area/2)-1;
    y=[mean(y(1:area/2))*ones(s,1);y];

    %fill y with NaN to a length of n*area
    len=length(y);
    zer=area-rem(len,area);
    if rem(len,area)==0
        zer=0;
    end
    y(len+1:len+zer)=ones(zer,1)*NaN;

    y=reshape(y,area,(len+zer)/area);
    if ~nanyes
        meany=mean(y);          % central algorithm!
    else
        meany=nanmean(y);          % central algorithm!
    end

    last=length(mean(y));
    n=y(:,last);

    if ~nanyes
        meany(last)=mean(n(~isnan(n)));
    else
        meany(last)=nanmean(n(~isnan(n)));
    end

    meany=meany(:);
elseif area>1 & round(area)~=area
    count = 2;
    while 1
        if round(area*count)==area*count
            break;
        end
        count = count +1;
    end
    NSamples = length(y);
    DataTmp = zeros(NSamples*count,1);
    for TmpInd = 1:count
        DataTmp(TmpInd:count:NSamples*count-count+TmpInd)= y;
    end
    meany = decfast(DataTmp,area*count);
elseif area~=1
    factor = 1;
    while round(area*factor)~=area*factor
        factor = factor + 1;
        if factor>30
            error('data resampling error in decfast!');
        end
    end
    UpData  = zeros(1,length(y)*factor);
    for r = 1:factor
        UpData(r:factor:length(y)*factor) =y;
    end
    meany = decfast(UpData,area*factor);
else
    meany = y;
end

