function plotline (l,str,fact,counteryes);
%function plotline (l,str,fact,counteryes);
% plot vertical lines in current graph on locations specified by l
% str specifies linetype [default='w-']
% fact: scaling factor for length of line compared to y-axis range [default=1]
% counter: put line counter on top of line


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<4 counteryes=0; end;
if nargin<3 fact=1; end;
if nargin<2 str='w-'; end;

n=axis;
hold on
for i=1:length(l)
plot([l(i) l(i)],[n(3) n(4)*fact],str);
if counteryes
text(l(i),n(4)*fact,int2str(i));
end
end;
hold off








