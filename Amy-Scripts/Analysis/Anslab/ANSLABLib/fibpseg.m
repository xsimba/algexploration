%   fibpseg
%
%   FilePath
%   PlateauStatus
%   DiscardInitial
%   Discard
%   PlotStatus

%   FindBPSegments reads in Biopac data and returns segmentation
%   information based on a dedicated 'Marker'-channel in the data.
%   If 'DiscardInitial' is set to 1 (default), the initial value is
%   ignored.
%   Markervalues found in 'Discard' are considered to be irrelevant.
%   Segment information containing the start and end point of any
%   relevant marker value is returned.

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if ~exist('PlotStatus');PlotStatus = [];end
if ~exist('Discard');Discard = [];end
if ~exist('DiscardInitial');DiscardInitial = [];end
if ~exist('PlateauStatus');PlateauStatus = [];end
if ~exist('ReadFilePath');ReadFilePath = [];end

if isempty(ReadFilePath);ReadFilePath ='F:\taf\TAF00901.ACQ';end
if isempty(PlateauStatus);PlateauStatus = 1;end
if isempty(DiscardInitial);DiscardInitial = 1;end
if isempty(Discard);Discard = 0;end
if isempty(PlotStatus);PlotStatus = 0;end

%read data
ExtractChannelBak = ExtractChannel;
ExtractChannel = 'Marker';
t1 = 1;
t2 = inf;
readbp;
ExtractChannel = ExtractChannelBak;


fprintf(1,['Marker channel contains ',num2str(size(DATA,2)),' points...\n']);
if DiscardInitial
    Changes = [];
    for SampInd = 2:size(DATA,2)
        if DATA(1,SampInd)~=DATA(1,SampInd-1)
            Changes = [Changes SampInd];
        end
    end
	for R=2:length(Changes)
        if Changes(R)==Changes(R-1)+1
            Changes(R-1) = 999;
        end
	end
	Changes(Changes==999)=[];
	MarkerList = [];
	if PlateauStatus
        NPlateaus = length(Changes) - 1;
        Segments = [];
        EventCount = 1;
        for PlatInd = 1: NPlateaus

            if isempty(find(DATA(Changes((PlatInd-1)+1))==Discard))
		Segments(EventCount,1) = DATA(Changes((PlatInd-1)+1));
		Segments(EventCount,2) = Changes((PlatInd-1)+1);
		Segments(EventCount,3) = Changes((PlatInd-1)+2);
		Segments(EventCount,4) = Segments(EventCount,2) - Segments(EventCount,1);
		EventCount = EventCount +1;
            end

        end
    else
        error('only plateau supported so far!');
    end


else
    error('not supported yet!');
end
if PlotStatus
	figure
	subplot(2,1,1);
	plot(DATA);
	subplot(2,1,2);
	plot(GradData);
end

