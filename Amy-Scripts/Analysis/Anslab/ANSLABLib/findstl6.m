% Reduction of eye-blink startle EMG channel in BIOSTL
% Based on findstl5.m
% The calculated onset and peak points are determined by the following simple rule set:
%   threshold value is 2 times the standard deviation plus the mean of the baseline
%      window, -50 to 0ms
%   analysis window, 20 to 120ms, must contain a threshold crossing with positive slope
%   the peak is then the absolute maximum in the window
%   the onset is the first point before the peak that is below the threshold
%   otherwise the trial is marked as bad
%   These rules are not applied automatically. Rather, the peak in the analysis window
%   is detected and plotted, and the criterion lines are plotted.
%   The user can then decide what to do.
%   Negative amplitudes are scored as 0, their latencies as NaN
%   Invalid responses: all parameters NaN
%   <0> selection for amplitude: amplitude is 0, latencies are NaN


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


%*** Initialization
sa=[]; st=[]; so=[]; sl=[]; sb=[];
notchdisplayyes=1;   % Display raw data as unfiltered (0) or notch filtered (1)
ty=1:1000;
sr=1000;
ep=4;
tref =tref-20;  % reference time for begin of startle window
trefb=tref;  % reference time for end of baseline window
clear STARTLE_MATRIX

% 28 Hz high-pass
% 50 or 60 Hz notch filter
% 500 Hz low-pass (anti-aliasing)
% time constant into cutoff frequency 10 ms
% fc=1/(2*pi*tc)
%fc=3.1831; % for 50 ms tc
%fc=22.7364; % for 7 ms tc
%fc=31.8310; % for 5 ms tc
fc=15.9155; % for 10 ms tc; default


%*** Preprocessing
%disp(['Highpass filter (28 Hz), notch filter ',num2str(notchfreq),' Hz, rectification, smoothing']);
SLf=filtlow(SL*1000,28,sr,1);  % high pass 28 Hz
disp([num2str(notchfreq),' Hz notch filter']);
SLf=notch(SLf,1000,notchfreq,9,7,.5,0);    % notch filter
SLfNonRect = SLf;
SLf=abs(SLf);                  % rectification
SLf=filthigh(SLf,fc,sr,1);     % low pass fc = 15.9

if ~yhigh
    yhigh=max(SLf);
end % y-axis limit


%*** Plot in EXAM
if examyes
   var2=[];
   var1=SLf;
   var2=MK;
   defblank
   int=20;
   samplerate=1000;
   def
   exam
end


%*** Detect marker onset time (when sound is played)
MK2=diff(MK);
son=find(MK2>0);  % marker on time
soff=find(MK2<0);  % marker off time
i=find(diff(son)<1000);
son(i)=[];
i=find(diff(soff)<1000);
soff(i)=[];
%son=[20; son];     % first one is missing, set to 20 ms
if length(soff)<length(son)
    soff=[540;soff];
end
s_d=soff-son;      % difference off-on time, typical 520 ms



%**************************************************************************
%*** Detect and edit startle response loop
for i=1:length(son)

    t=(son(i)+toffset+1):(son(i)+toffset+1000);
    y=SLf(t);
    if notchdisplayyes
       yraw=SLfNonRect(t);
    else
       yraw=SL(t);
    end    
    %*** Filter for onset detection
    ydet=filtlow(yraw*1000,28,sr,1); % high pass 28 Hz


    ydet=notch50(ydet);              % notch 60 Hz
    ydet=abs(ydet);                  % rectification
    ydet=filthigh(ydet,onsetfactor*fc,sr,1);% low pass less rigorous for onset detection

    %*** Define windows and means
    tbase=trefb-50+1:trefb;    % base window
    tresp=tref+20+1:tref+120;  % response window
    bm=mean(y(round(tbase)));      % base mean
    bsd=std(y(round(tbase)));      % base SD
    bt=bt_factor*bsd+bm;       % startle response onset threshold
    yresp=y(tresp);

    %*** Score response
    [ymax,ymaxi]=max(yresp);
    ylat=ymaxi;                % response latency
    yons=ydet(tref+21:tref+20+ymaxi);

    %*** Find startle onset
    l=0;
    detected=0;
    while detected==0
       l=l+1;
       if yons(l)>bt | l+1>=length(yons)
          detected=1;
       end
    end
    tons=l+20;

    %*** Plot
    figure(1)
    if screen>800
        set(1,'Position',[4 249 1016 477]);
    else
        set(1,'Position',[9 233 791 325]);
    end
    plot(ty,y)
    xlabel('Adjust: r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
    ylabel('uV')
    axisy(0,max([1.1*max(y),yhigh]));
    plotline([trefb-50,trefb],'c:');
    plotline([tref+20,tref+120],'r:');
    hold on
    plot([trefb-50,tref+120],[bm,bm],'c:');
    plot([trefb-50,tref+120],[bt,bt],'c:');
    plot(ymaxi+tref+20,ymax,'ro')
    hold off
    title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
    figure(2)
    if screen>800
        set(2,'Position',[3 61 1017 189]);
    else
        set(2,'Position',[9 64 791 176]);
    end
    plot(ty,yraw)
    plotline(tref+tons);
    xlabel('msec')
    ylabel('uV')
    title('Raw EMG')
    figure(1)


    %*** Edit points
    k=0;
    while k~=13 & k~=48 & k~=105

        [i1,i2,k] = ginput(1);

        if isempty(i1) & isempty(i2) & isempty(k)
            k = 13;
        end

        if k==13    % <Enter> = accept
           sa(i)=ymax-bm; sb(i)=bm; st(i)=t(1)+tref; so(i)=tons; sl(i)=ylat+20;
        end

        if k==48    % <0> = define amplitude as 0
           sa(i)=0; sb(i)=bm; st(i)=t(1)+tref; so(i)=NaN; sl(i)=NaN;
        end

        if k==105   % <i> = invalid response
           sa(i)=NaN; sb(i)=bm; st(i)=t(1)+tref; so(i)=NaN; sl(i)=NaN;
        end

        if k==114   % <r> = range for y-axis definition
           yhigh=input('New y-axis maximum ==> ');
           plot(ty,y)
           xlabel('Adjust: r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
           ylabel('uV')
           axisy(0,yhigh);
           plotline([trefb-50,trefb],'c:');
           plotline([tref+20,tref+120],'r:');
           hold on
           plot([trefb-50,tref+120],[bm,bm],'c:');
           plot([trefb-50,tref+120],[bt,bt],'c:');
           plot(ymaxi+tref+20,ymax,'ro')
           hold off
           title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
        end

        if k==119   % <w> = detection window definition
           temp=input('New detection window onset ==> ');
           tref=temp-20;  % actual onset window earlier than displayed
           trefb=tref;

           %*** Define windows and means
           tbase=trefb-50+1:trefb;    % base window
           tresp=tref+20+1:tref+120;  % response window
           bm=mean(y(tbase));      % base mean
           bsd=std(y(tbase));      % base SD
           bt=bt_factor*bsd+bm;       % startle response onset threshold
           yresp=y(tresp);

           %*** Score response
           [ymax,ymaxi]=max(yresp);
           ylat=ymaxi;                % response latency
           yons=ydet(tref+21:tref+20+ymaxi);

           %*** Find startle onset
           l=0;
           detected=0;
           while detected==0
              l=l+1;
              if yons(l)>bt | l+1>=length(yons)
                 detected=1;
              end
           end
           tons=l+20;

           %*** Plot
           figure(1)
           if screen>800
               set(1,'Position',[4 249 1016 477]);
           else
               set(1,'Position',[9 233 791 325]);
           end
           plot(ty,y)
           xlabel('Adjust r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
           ylabel('uV')
           axisy(0,max([1.1*max(y),yhigh]));
           plotline([trefb-50,trefb],'c:');
           plotline([tref+20,tref+120],'r:');
           hold on
           plot([trefb-50,tref+120],[bm,bm],'c:');
           plot([trefb-50,tref+120],[bt,bt],'c:');
           plot(ymaxi+tref+20,ymax,'ro')
           hold off
           title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
           figure(2)
           if screen>800
               set(2,'Position',[3 61 1017 189]);
           else
               set(2,'Position',[9 64 791 176]);
           end
           plot(ty,yraw)
           plotline(tref+tons);
           xlabel('msec')
           ylabel('uV')
           title('Raw EMG')
           figure(1)

           sa(i)=ymax-bm; sb(i)=bm; st(i)=t(1)+tref; so(i)=tons; sl(i)=ylat+20;
        end


        if k==111   % <o> = redefine onset latency
           o=ymaxi+tref+20; p=0; q=0;
           while q~=13
                figure(1)
                plot(ty,y)
                xlabel('Adjust: r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
                ylabel('uV')
                axisy(0,max([1.1*max(y),yhigh]));
                plotline([trefb-50,trefb],'c:');
                plotline([tref+20,tref+120],'r:');
                hold on
                plot([trefb-50,tref+120],[bm,bm],'c:');
                plot([trefb-50,tref+120],[bt,bt],'c:');
                plot(ymaxi+tref+20,ymax,'ro')
                plot(o,y(round(o)),'go')
                hold off
                title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
                figure(2)
                if screen>800
                    set(2,'Position',[3 61 1017 189]);
                else
                    set(2,'Position',[9 64 791 176]);
                end
                plot(ty,yraw)
                plotline(o);
                xlabel('msec')
                ylabel('uV')
                title('Raw EMG')
                figure(1)

                [o,p,q] = ginput(1);
                if q==1
                   if o-tref-1>=0
                      tons=o-tref-1;
                   else
                      tons=0;
                   end
                end
           end
           sa(i)=ymax-bm; sb(i)=bm; st(i)=t(1)+tref; so(i)=tons; sl(i)=ylat+20;
        end


        if k==108    % <l> = change response latency
           o=ymaxi+tref+20; p=0; q=0;
           while q~=13
                figure(1)
                plot(ty,y)
                xlabel('Adjust: r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
                ylabel('uV')
                axisy(0,max([1.1*max(y),yhigh]));
                plotline([trefb-50,trefb],'c:');
                plotline([tref+20,tref+120],'r:');
                hold on
                plot([trefb-50,tref+120],[bm,bm],'c:');
                plot([trefb-50,tref+120],[bt,bt],'c:');
                plot(o,y(round(o)),'bo')
                hold off
                title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
                [o,p,q] = ginput(1);
                if q==1
                    ylat=o-tref-20;
                    if ylat<=-20
                      ylat=-20;
                    end
                end
           end
           sa(i)=ymax-bm; sb(i)=bm; st(i)=t(1)+tref; so(i)=tons; sl(i)=ylat+20;
        end

        if k==98    % <b> = reset base window
            o=0; p=0; q=0;
            while q~=13
                [o,p,q] = ginput(1);
                if q==1
                   trefb=o;
                       if trefb<50 trefb=50; end
                end
                tbase=trefb-50+1:trefb;            % base window
                bm=mean(y(round(tbase)));              % base mean
                bsd=std(y(round(tbase)));              % base SD
                bt=bt_factor*bsd+bm;               % startle response onset threshold

                figure(1)
                plot(ty,y)
                xlabel('Adjust: r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
                ylabel('uV')
                axisy(0,max([1.1*max(y),yhigh]));
                plotline([trefb-50,trefb],'c:');
                plotline([tref+20,tref+120],'r:');
                hold on
                plot([trefb-50,tref+120],[bm,bm],'c:');
                plot([trefb-50,tref+120],[bt,bt],'c:');
                plot(ymaxi+tref+20,ymax,'ro')
                hold off
                title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
           end
           sa(i)=ymax-bm; sb(i)=bm; st(i)=t(1)+tref; so(i)=tons; sl(i)=ylat+20;
        end

        if k==1    % <click> = reset max manually
           ymaxi=i1-tref-20;
           ylat=ymaxi;
           ymax=y(round(i1));
           yons=ydet(tref+21:tref+20+ymaxi);
           figure(1)
           plot(ty,y)
           xlabel('Adjust: r=range, w=window, b=baseline, o=onset lat, l=resp lat   <click>=reset point  0=zero ampl  i=invalid  <Enter>=ok  [msec]')
           ylabel('uV')
           axisy(0,max([1.1*max(y),yhigh]));
           plotline([trefb-50,trefb],'c:');
           plotline([tref+20,tref+120],'r:');
           hold on
           plot([trefb-50,tref+120],[bm,bm],'c:');
           plot([trefb-50,tref+120],[bt,bt],'c:');
           plot(ymaxi+tref+20,ymax,'ro')
           hold off
           title(['Trial = ',int2str(i),'     Time = ',rndv(num2str(son(i)/1000)),'     Base = ',rndv(num2str(bm)),'     Ampl = ',rndv(num2str(ymax-bm)),'     Respone Latency = ',int2str(ylat+20),'     Onset Latency = ',int2str(tons)]);
           sa(i)=ymax-bm; sb(i)=bm; st(i)=t(1)+tref; so(i)=tons; sl(i)=ylat+20;
        end
    end
% while k~=13 & k~=48 & k~=105
end
% for i=1:length(son)


%*** Assemble results matrix
STARTLE_MATRIX=[(1:length(st))' , st(:)./sr , sa(:) , sl(:) , so(:) , sb(:)];
STARTLE_MATRIX=replace(STARTLE_MATRIX,NaN,-9999);


%*** Conversion to 1/4-sec epoch data, negative amplitudes are set to 0
%    insert NaN for longer stimulus intervals (discontinuities will be filled with NaN)
i=find(sa<0);
sa(i)=zeros(size(i));
sl(i)=NaN*ones(size(i));
d=diff(st);
i1=outliers(d);
dtyp=nanmean(i1);
i1=find(isnan(i1));
i1=[i1 length(st)];

for i=1:length(i1)
    st=insert(st,(st(i1(i))+dtyp),i1(i)+1);
    sa=insert(sa,NaN,i1(i)+1);
    sl=insert(sl,NaN,i1(i)+1);
    sb=insert(sb,NaN,i1(i)+1);
    so=insert(so,NaN,i1(i)+1);
    i1=i1+1;
end

sa0=epoch(st,sa,(sr/ep));
sl0=epoch(st,sl,(sr/ep));
so0=epoch(st,so,(sr/ep));
slg=length(SLf)/sr;        % max length in sec for statout


%*** Plot across trial data
t=(1:length(sa0))/ep;
figure(3)
subplot(3,1,1)
plot(st/sr,sa,'ro',t,sa0,'-')
title('Eye-blink Startle Response Magnitude')
xlabel('sec')
ylabel('uV')
subplot(3,1,2)
plot(st/sr,sl,'ro',t,sl0,'-')
title('Eye-blink Startle Response Latency')
xlabel('sec')
ylabel('msec')
subplot(3,1,3)
plot(st/sr,so,'ro',t,so0,'-')
title('Eye-blink Startle Onset Latency')
xlabel('sec')
ylabel('msec')


