function [cv,mssd,m,s,med,miny,maxy] = coeffvar(y);
% [cv,mssd,m,sd,med,miny,maxy] = coeffvar(y);
% derive coefficient of variation and other statistics

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

y=nanrem(y);

l=length(y);
m=mean(y);
s=std(y);
cv=s/m*100;
if ~isempty(y) med=median(y); else med=NaN; end;
miny = min(min(y));
maxy = max(max(y));
mssd = mean(diff(y).^2);

if isempty(y)
cv=NaN;
mssd=NaN;
m=NaN;
s=NaN;
med=NaN;
miny=NaN;
maxy=NaN;
end

