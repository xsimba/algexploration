function [y,r]=ztrans2(y);
% function [y,r]=ztrans2(y);
% range +-0.5 transformation of vector y
% r: factor for transformation y=(y-min(y)-r)/(r*2);
% y can contain several signals as column vectors
% F.W.: 11-10-95: y can be matrix

%[y1,ind]=nanrem(y);



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



[row,col]=size(y);
r=(max(y)-min(y))/2;
if col==1
y=(y-min(y)-r)./(r*2);
else
y=(y-tony(min(y),row)-tony(r,row)) ./ tony(r*2,row);
end;

%y=nanrest(y1,ind);



