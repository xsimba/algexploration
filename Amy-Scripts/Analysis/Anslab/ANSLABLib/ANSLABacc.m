% Analyze EMG data exported from Biopac (.emg, 1000 Hz)
% Convert to 10 Hz instantaneous EMG amplitude data


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.




clear
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultTextColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersionString= version;
MVersNr = str2num(MVersionString(1:3));
if MVersNr>6
    eval('warning off;');
end

%*** User settings
if ~exist('data_dir') | ~exist('red_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
    red_dir=[data_dir,'\a'];
    r1ind=1;   % sequential number in file of CO2
end

% settings
filtyes=0;

%*** Load tonic EMG data
chd(data_dir)
%FL=filelist('*.txt',0.25);
[filename, data_dir] = uigetfile('*.txt','Select data file')
if isequal(data_dir,0);return;end
eval(['load ',filename,';']);
varfilename = filename(1:end-4);
l=length(filename);
varname=filename(1:8);
subjn=str2num(varname(4:6));
studystr=varname(1:3);
eval(['AC=',varfilename,';']);
eval(['clear ',varfilename]);
len=length(AC)/25;

%*** resample if necessary
[AC,SR] = AskResampleData(AC,400,'ACC');


%*** call analysis program
findmov5

%*** Save reduced data
chd(red_dir)
S=[act0];
cmdstr = ['save ',varname,'a.txt -ascii -tabs S'];
disp(cmdstr);
eval(cmdstr);





