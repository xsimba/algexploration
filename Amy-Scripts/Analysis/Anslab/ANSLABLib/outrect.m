function [xn,noutind]=outrect(x,t,plotyes,str,textyes);
%[xn,noutind]=outrect(x,t,plotyes,str,textyes);
% Inspect vector data x at times t and exclude artifacts by dragging mouse.
% This defines a rectangle. Values above or below the limits of the rectangle
% within the left and right limits of the rectangle are set to NaN.
% plotyes: Create initial plot of x.
% str: drawing symbol, default='-'.
% textyes: Plot t-indices with points.
% noutind: Indices of values that were excluded.
% xn:      New vector x
% If t is empty matrix: t=1:length(x)
%
% Instructions:
% Define outlier rectangulars by dragging the mouse cursor.
% Undo by clicking once inside  the data range.
% Quit by clicking once outside the data range.

% Copyright Frank Wilhelm, 10-24-95, 3-10-97, 7-21-99, 1-16-00 (reset)


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<5 textyes=0; end;
if nargin<4 str='-'; end;
if nargin<3 plotyes=0; end;
if nargin<2 t=1:length(x); end;
if isempty(t) t=1:length(x); end;

% Test: x=R1; t=1:length(x); plotyes=1; str='-'; textyes=0;

%*** Convert x to row-vector
colvec=(size(x,1)>1);
if colvec x=x'; end;

xp=(t(length(t))-t(1))/25;  % x plus: add free space between data and axis
maxx=max(nanrem(x));
minx=min(nanrem(x));
yp=(maxx-minx)/25;
xl1=t(1)-xp;
xl2=t(length(t))+xp;
yl1=minx-yp;
yl2=maxx+yp;

if isempty(xl1)|isempty(xl2)|isempty(yl1)|isempty(yl2)
xn=x; noutind=[];
return
end

if plotyes
   plot(t,x,str);
   set(gca,'XLim',[xl1 xl2]);
   set(gca,'YLim',[yl1 yl2]);
   if textyes
   text(t,x,mat2str2(t))
   end
   ylabel(['Mean = ',num2str(nanmean(x))]);
end;
title('Drag mouse to draw exclusion boxes. left-above = draw. mid-above = unzoom. right-above: line/x');
xlabel('left = undo. right = interpolate. left-below = restore. mid-below = zoom. right-below = exit.');
noutind=[];
outnum=0;
xold=x;
xundo=x;
ok=1;

while ok
v=axis;
dx=v(2)-v(1);
dy=v(4)-v(3);
r=getrect;  % [left, bottom, width, height]
if r(3)<dx/200 & r(4)<dy/200   % only one click!
  if r(1)>v(2)           % exit, nan_lip, line style: right
     if r(2)>v(4)
        if strcmp(str,'-') str='x'; else str='-'; end;
     elseif r(2)<v(3)
        break; break; break; break; break; break;
     else
        x=nan_lipf(x);
     end
  elseif r(1)<v(1)       % undo, restore, draw: left
     if r(2)<v(3)
        x=xold;
     elseif r(2)>v(4)
        n=find(t>=xl1 & t<=xl2);
        if ~isempty(n)
          xd=x(n);
          x(n)=drawlin2(length(xd),yl1,yl2,xd);
        end
     else
        noutind=noutind(1:length(noutind)-outnum);
        x=xundo;
        title('Last exclusion was undone.')
     end
  elseif r(2)<v(3)       % zoom: button
     title('drag mouse to zoom, or click to zoom 50%')
     rz=getrect;
     if ~rz(3)|~rz(4)
        xl1=rz(1)-(xl2-xl1)/2;
        xl2=rz(1)+(xl2-xl1)/2;
        yl1=rz(2)-(yl2-yl1)/2;
        yl2=rz(2)+(yl2-yl1)/2;
     else
        xl1=rz(1);
        xl2=rz(1)+rz(3);
        yl1=rz(2);
        yl2=rz(2)+rz(4);
     end
  else r(2)>v(4)         % unzoom: top
     xp=(t(length(t))-t(1))/25;  % x plus: add free space between data and axis
     maxx=max(nanrem(x));
     minx=min(nanrem(x));
     yp=(maxx-minx)/25;
     xl1=t(1)-xp;
     xl2=t(length(t))+xp;
     yl1=minx-yp;
     yl2=maxx+yp;
  end
else
  i1=[r(1) r(1)+r(3)];
  i2=[r(2) r(2)+r(4)];
  n=find(t>=i1(1) & t<=i1(2)); % interval
  if ~isempty(n)
     nout=find(x(n)<i2(1) | x(n)>i2(2));
     nout=nout+n(1)-1;  % absolute index
     xundo=x;
     x(nout)=NaN*ones(size(nout));
     noutind=[noutind nout];
     outnum=length(nout);
     disp([int2str(outnum),' new values were set to missing.']);
   end
end;
plot(t,x,str);
set(gca,'XLim',[xl1 xl2]);
set(gca,'YLim',[yl1 yl2]);
if textyes
  text(t,x,mat2str2(t))
end
title('Drag mouse to draw exclusion boxes. left-above = draw. mid-above = unzoom. right-above: line/x');
xlabel('left = undo. right = interpolate. left-below = restore. mid-below = zoom. right-below = exit.');
ylabel(['Mean = ',num2str(nanmean(x))]);
end; % while

if colvec x=x'; end
xn=x;

if nargout==0 clear xn noutind; end
