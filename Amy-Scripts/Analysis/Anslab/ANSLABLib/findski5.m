% Gross asdaptation of algorithm for NSF detection
% instantanous NSF
% rise time and half recovery-time
% (c) Frank Wilhelm, 1/3/98, 10-7-98
% hrec and hrec0 may contain NaN (if next NSF occured earlier)
% leading and trailing zeros for nsfr0
% Last update: 7/16/99: batch enabled; 1/21/01: SCL and NSF editing saved
% 6-23-03: option to exclude intervals at the end of editing
% 6-24-03: batch enabled with PSY3


%*** Initialization
markyes=0;    %    (0) display marker info (shared code)
actyes=0;     %    (0) display activity and respiration information
ep=4;         % epoch-size [1/ep sec]
sr=25;        % samplerate
if ~exist('bno')
    bno=1;
    bplotyes=1;
end
if ~bno
    rateyes=0;
    rat=0;
end 

clear artinterval  
y=SC;

format compact
base=[];
qf=[];
amp=[];

%*** Minimal smoothing
if filtyes
    y = filthigh(y,1,sr,5);
end

inspectloop=1;

if bno|bplotyes
    cfig; cfig; cfig;
    if screen>800
        figure('Position',[132    30   890   247])
        figure('Position',[132    30   890   426])
        figure('Position',[132   277   890   178])
        figure('Position',[132   460   492   273])
        figure('Position',[134   454   886   272])
    else
        figure('Position',[28 27 766 219])
        figure('Position',[28 27 766 219])
        figure('Position',[28 247 766 125])
        figure('Position',[3 453 467 272])
        figure('Position',[28 369 766 189])
    end

    figure(3)
    clf
    if ~exist('act0');
        answer = questdlg('Accelerometer information is missing. Do you wish to load analyzed accelerometer data?','Accelerometer data:','Yes','No','Yes');
        [AccFile,AccPath] = uigetfile([data_dir,filesep,'a',filesep,'*.*'],'Please choose data file:');
        AccFilePath = [AccPath AccFile];
        if isequal(AccPath,0)|isequal(AccFile,0);error('invalid file!');
            return;
        end
        load(AccFilePath);
        if ~exist('act0');
            error('File does not contain accelerometer information!');
            return;
        end
    end
    t=(1:length(act0))/4;
    plot(t,act0,'w')
    axis([0 max(t) 0 max([max(act0), 0.5])]);
    title('Activity');
    xlabel('sec')
    ylabel('units');
end %if bplotyes

if scledityes==1 
    figure(2)
    [y,i1] = outrect(y,(1:length(y))/sr,1);
    y=nan_lip(y);
end

%*** FOLLOWING IS SHARED WITH findsc12.m

%*** find base
slope=diff(y);
slope1=slope;  % slope 1 point forward
slope1(1)=[];
slope(length(slope))=[];  
base=find(slope<=0 & slope1>0);
base=base(:);
base=[1;base]+1;

base=base(:);
qf=NaN*ones(size(base));
amp=NaN*ones(size(base));

%*** find qualifying point (>0.01uS increase within 1sec)
if ~isempty(base)
    t2=base+forward1*sr; 
    n=find(t2>length(y));
    t2(n)=length(y)*ones(size(n));
    for i=1:length(base)
        n1 = y(base(i)+1:t2(i));
        n=find( n1-y(base(i))  >= rise1 );
        if ~isempty(n) 
            qf(i)=n(1)+base(i);
        else
            qf(i)=NaN;
        end
    end
    [qf,ind]=nanrem(qf);
    if length(ind)
        base(ind)=[];
        amp(ind) =[];
    end

    %*** find amplitude   (0.05uS increase within 3.5sec from qf)
    if ~isempty(qf)
        t2=qf+forward2*sr;  
        n=find(t2>length(y));
        t2(n)=length(y)*ones(size(n));
        for i=1:length(base)
            n1= y(base(i)+1:t2(i));
            n=find( n1-y(base(i))  >= rise2 );  % criterion for NSF
            if ~isempty(n) 
                amp(i)=n(1)+base(i);
            else
                amp(i)=NaN;
            end
        end
        [amp,ind]=nanrem(amp);
        if length(ind)
            base(ind)=[];
            qf(ind)=[];
        end

        %*** exclude amplitudes that occur after the next baseline
        for i=1:(length(amp)-1)
            if amp(i)>=base(i+1)
               amp(i)=NaN;
            end
        end

        [amp,ind]=nanrem(amp);
        if length(ind)
            base(ind)=[];
            qf(ind)=[];
        end
    end  % if qf
end  % if base


rtn=base(:);
rtnd=y(base);
rtnd=rtnd(:);

%****** Inspect and/or exclude outliers loop
while inspectloop

    if ~bno
        inspectloop=0;
    end

    if bno|bplotyes
        figure(2)
        clg
        t=1:length(y);
        plot(t/sr,y,'w')
        axis([0 max(t/sr) min(y) max(y)])
        title(['Detected SCR onsets.']);
        xlabel('sec')
        ylabel('uS');
        if length(rtn)
            hold on
            plot(rtn/sr,rtnd,'xg');
            x1=rtn/sr; x2=rtnd;
            n=find(isnan(x1)|isnan(x2));
            x1(n)=[]; x2(n)=[];
            x1=x1+max(t/sr)/100;
            for i=1:length(x1)
                text(x1(i),x2(i),int2str(i));
            end
            hold off
        end

        if markyes
            plotline(M2,'m');
        end
    end %bplotyes

    drawnow

    %*** Determine amplitudes, rise times, and half recovery-times of nsfs
    NSF = 60*length(rtn)/(length(nanrem(y))/sr);
    if length(rtn)
        nsfv=NaN*ones(size(rtn));
        rtime=NaN*ones(size(rtn));
        halfr=NaN*ones(size(rtn));
        rtn(length(rtn)+1)=length(y);
        for i=1:length(rtn)-1
            x1=y(round(rtn(i)):round(rtn(i+1)));  % needed for halfr
            if maxwin
                n1=rtn(i);
                n2=rtn(i+1);
                if (n2-n1)>(maxwin*sr) 
                    n2=n1+maxwin*sr;
                end
                x=y(n1:n2);
                [ind,m]=findmax(x,4,1,.00000001,3);  % local maxima
                if ~isempty(m) 
                    [nsfv(i),ind2]=max(m);      
                    rtime(i)=ind(ind2);
                end
            else
                x=x1;
                [nsfv(i),rtime(i)]=max(x);
            end
            nsfv(i)=nsfv(i)-y(round(rtn(i))); 
            if isnan(rtime(i))
                halfr(i)=NaN;
            else

                x1(1:rtime(i))=[];
                n=find(x1<=y(round(rtn(i)))+nsfv(i)/2);
                if ~isempty(n)
                    halfr(i)=n(1)+rtime(i);
                end 
            end
        end

        rtn(length(rtn))=[];

        halfr=(halfr-1)/sr;
        rtime=(rtime-1)/sr;
        [nsfv,ind]=nanrem(nsfv);  % delete if NaNs leftover from maxwin
        if length(ind)
            rtime(ind)=[];
            halfr(ind)=[];
            rtn(ind)=[];
            rtnd(ind)=[];
        end
    end
    %*** if length(rtn)

    %*** Plot waveform control plots
    if cplotyes & (bno|bplotyes)

        figure(4)
        clg

        if length(rtn)

            subplot(1,2,1)
            plot(rtime,nsfv,'.')
            axis('square');
            title('Steepness of slopes of NSFs')
            ylabel('Amplitude [microS]')
            xlabel('Rise time [sec]')
            x1=rtime; x2=nsfv;
            %n=find(isnan(x1)|isnan(x2));
            %x1(n)=[]; x2(n)=[];
            hold on
            for i=1:length(x1)
                text(x1(i),x2(i),int2str(i));
            end
            hold off

            subplot(1,2,2)
            plot(rtime,halfr,'.')
            i=max([nanrem(rtime(:));nanrem(halfr(:))])*1.02;
            if isempty(i)
                i=1;
            end
            axis([0 i 0 i]);
            axis('square');
            title('Waveform characteristic of NSFs')
            ylabel('Half recovery-time')
            xlabel('Rise time [sec]')
            x1=rtime; x2=halfr;
            %n=find(isnan(x1)|isnan(x2));
            %x1(n)=[]; x2(n)=[];
            hold on
            for i=1:length(x1)
                text(x1(i),x2(i),int2str(i));
            end
            hold off
            drawnow
        end
    %*** if length(rtn)
    end
    %*** if cplotyes

    if bno|bplotyes
        figure(2)
        figure(3)
        figure(4)

        if bno

            m1='Exit editing';
            m2='Editing of SCR onsets';
            m3='Exclude artifacts';
            m4='Start again';
            m5='Display segment';
            m6='Exclude SCR onset';

            outyes=menu('Please select EDA editing type:',m1,m2,m3,m4,m5,m6);

            if ~outyes
                return
            end

            if outyes<2
                inspectloop=0;
            end

            if outyes==2
               ax=axis;
               x=[];
               %hold on
               disp(' ');
               disp('Click twice to mark suspicious intervals, end with <0>')
               disp('In EXAM: goto marked intervals with <0>');
               disp('Delete false SCR onsets with <#>');
               disp('Insert correct SCR onsets with <?> or <!>');
               disp('Mark begin <b> and end <e> of artifact, remove with <->');
               figure(2)
               while 1
                   [i1,i2,i3]=ginput(1);
                   if i3==48  
                        if length(x)/2==floor(length(x)/2) 
                            break
                        else
                            disp(' ');
                            disp('Odd number of lines.  Please click once more!');
                            title('Odd number of lines.  Please click once more!');        
                            [i1,i2,i3]=ginput(1);     
                        end
                   end
                   x=[x i1];
                   %plot([i1;i1],[ax(3);ax(4)],':c');   % display vertical line
               end
               %hold off
               x=x*sr;
               lookat=reshape(x,2,length(x)/2)';
               figure(1);clg
               clear var2 var3 var4
               defblank; int=60; moveint=60; samplerate=sr; var1=y; def; 
               %artbegin=artdiffbeg; artend=artdiffend;
               if exist('artinterval')
                   if ~isempty(artinterval)
                        artbegin=artinterval(:,1); artend=artinterval(:,2);
                   end
               end
               event1=rtn; event1yes=1;
               val=1:length(rtn); valtime=rtn; valueyes=1; tsub=[];
                %event2=M2*sr; event2yes=1;
               evscan=event1; skiploc=.5;
               yaxisstr='uS';
               exam; 

               rtn=event1;     %*** add values specified from inside exam  (option @)
               rtnd=var1(round(event1)); %
               y=var1;

               if exist('artinterval')
                   if ~isempty(artinterval)
                       disp('Exclusion of values within specified artifact intervals');
                       setmiss=between(rtn,artinterval);
                       if ~isempty(setmiss) 
                          rtnd(setmiss)=[];
                          rtn(setmiss)=[];
                          disp([int2str(setmiss),' values were set to missing.']);
                       end
                   end
               end
               inspectloop=1;  
            end

            if outyes==3
               figure(2)
               [y,i1] = outrect(y,(1:length(y))/sr,1);
               y=nan_lip(y);
               %[rtnd,i1] = outrect(rtnd,rtn/sr,1);
               %rtnd(i1)=[];
               %rtn(i1)=[];
               inspectloop=1; 
            end

            if outyes==4 
               rtn=base(:);
               rtnd=y(rtn);
               rtnd=rtnd(:);
               inspectloop=1; 
            end

            if outyes==5
               figure(2)
               title('Click and hold left mouse button to zoom repeatedly. Right to undo. <enter> to quit.'); 
               zoomrb   
               pause
            end

            if outyes==6
                outstr=input('Take out event # [e.g., 3, or: 1-12; <enter>=end] ==> ','s');
                if isempty(outstr)
                   out=0; break; break; 
                else 
                   f=findstr(outstr,'-');
                   if ~isempty(f)
                     outbeg=str2num(outstr(1:f-1));
                     outend=str2num(outstr(f+1:length(outstr)));
                     out=outbeg:outend;
                   else
                     out=str2num(outstr);
                   end
                   rtn(out)=[];     
                   rtnd(out)=[]; 
                end
            end
        end
    
        %if bno

        %*** Re-Determine amplitudes, rise times, and half recovery-times of nsfs
        NSF = 60*length(rtn)/(length(nanrem(y))/sr);
        if length(rtn)
            nsfv=NaN*ones(size(rtn));
            rtime=NaN*ones(size(rtn));
            halfr=NaN*ones(size(rtn));
            rtn(length(rtn)+1)=length(y);
            for i=1:length(rtn)-1
                x1=y(round(rtn(i)):round(rtn(i+1)));  % needed for halfr
                if maxwin
                     n1=rtn(i);
                     n2=rtn(i+1);
                     if (n2-n1)>(maxwin*sr) 
                        n2=n1+maxwin*sr;
                     end
                     x=y(n1:n2);
                     [ind,m]=findmax(x,4,1,.00000001,3);  % local maxima
                     if ~isempty(m) 
                       [nsfv(i),ind2]=max(m);      
                       rtime(i)=ind(ind2);
                     end
               else
                    x=x1;
                    [nsfv(i),rtime(i)]=max(x);
               end
               nsfv(i)=nsfv(i)-y(round(rtn(i)));  
               if ~isnan(rtime(i))
                   x1(1:rtime(i))=[];
                   n=find(x1<=y(round(rtn(i)))+nsfv(i)/2);
                   if ~isempty(n)
                      halfr(i)=n(1)+rtime(i);
                   end 
               end
            end
            rtn(length(rtn))=[];
            halfr=(halfr-1)/sr;
            rtime=(rtime-1)/sr;
            [nsfv,ind]=nanrem(nsfv);  % delete if NaNs leftover from maxwin
            if length(ind)
                rtime(ind)=[];
                halfr(ind)=[];
                rtn(ind)=[];
                rtnd(ind)=[];
                base=rtn;
            end
        end
        %*** if length(rtn)
    end %if bplotyes
end
%1****** inspect data for artifacts loop

%*** Rename some variables
nsfn=NSF; 
nsft=rtn;
nsfv=nsfv;
hrec=halfr;
rtim=rtime;

scl0=decfast(ipfast(y,ep),sr);

%*** Instananous NSF-> IFI (inter-fluctuation interval)
%disp('Instananous NSF');
len=length(scl0);

if ~isempty(nsft)
    nsftd=difffit(nsft)/sr; 

    % insert 0 values for NSF-rate if inter-NSF intervals too long
    % insert NaN for other parameters
    d=diff(nsft);
    i1=find(d>maxblen*sr);
    i1=[i1;length(nsft)];
    nsft2=nsft;
    nsftd2=nsftd;
    nsfv2=nsfv;
    rtim2=rtim;
    hrec2=hrec;

    if ~isempty(nsft2)
        for i=1:length(i1)
            nsft2 =insert(nsft2,(nsft2(i1(i))+maxblen/2*sr),i1(i)+1);
            nsftd2=insert(nsftd2,Inf,i1(i)+1);
            nsfv2 =insert(nsfv2,NaN,i1(i)+1);
            rtim2 =insert(rtim2,NaN,i1(i)+1);
            hrec2 =insert(hrec2,NaN,i1(i)+1);
            i1=i1+1;
        end
    end

    nsfr0=epoch(nsft2,60 ./ nsftd2,(sr/ep));  % skin conductance fluctuation rate (fluct/min)
    nsfv0=epoch(nsft2,nsfv2,(sr/ep));      
    rtim0=epoch(nsft2,rtim2,(sr/ep));      
    hrec0=epoch(nsft2,hrec2,(sr/ep));      
    nsfr0=cut_fill(nsfr0,len);
    nsfv0=cut_fill(nsfv0,len);
    rtim0=cut_fill(rtim0,len);
    hrec0=cut_fill(hrec0,len);
else
    nsfr0=zeros(len,1);
    nsfv0=zeros(len,1);
    rtim0=NaN*ones(len,1);
    hrec0=NaN*ones(len,1);
end

scl0=scl0(:);
nsfr0=nsfr0(:);
hrec0=hrec0(:);
rtim0=rtim0(:);
nsfv0=nsfv0(:);

% Fill NaNs at end with last valid number 
l=length(rtime);
x=rtim0(len:-1:1);
n=find(~isnan(x));
if length(n)
    n=n(1);
    if n>1
       rtim0(len-n+2:len)=rtime(l)*ones(n-1,1);
       hrec0(len-n+2:len)=hrec(l)*ones(n-1,1);
       nsfv0(len-n+2:len)=nsfv(l)*ones(n-1,1);
    end
end

% fill start and end of nsfr0 with 0
n=find(~isnan(nsfr0));
if length(n)
    n=n(1);
    if n>1 
       nsfr0(1:n-1)=zeros(n-1,1);
    end
    x=nsfr0(len:-1:1);
    n=find(~isnan(x));
    n=n(1);
    if n>1 
       nsfr0(len-n+2:len)=zeros(n-1,1);
    end
else
    nsfr0=0*one(nsfr0);
end

if bno|bplotyes
    %*** Plot epoch data
    figure(2)

    t=(1:len)/ep;

    figure(3)
    clg
    plot(t,[nsfv0])
    axisx(0,max(t));
    title('NSF Amplitude');

    figure(5)
    clg
    plot(t,[nsfr0 rtim0 hrec0])
    axisx(0,max(t));
    title('NSF rate (y) Rise Time (m), and Half Recovery Time (c)');

    drawnow

    if bno

        setempty=input('Set certain intervals to missing data (<Enter>=no [default], 1=yes) ==> ');
        if isempty(setempty)
            setempty=0;
        end

        while setempty
            figure
            t=(1:length(scl0))/4;
            [scl0,noutind]=outrect(scl0,t,1);
            n=find(isnan(scl0));
            nsfr0(n)=NaN*ones(size(n));
            scl0 (n)=NaN*ones(size(n));
            nsfv0(n)=NaN*ones(size(n));
            rtim0(n)=NaN*ones(size(n));
            hrec0(n)=NaN*ones(size(n));

            ind=[];
            if length(nsft)
                for i=1:length(nsft)
                    j=round(nsft(i)/sr*4);
                    if j>0
                       if isnan(scl0(j))
                          ind=[ind i];
                       end
                    end
                end
                nsft(ind)=[];
                nsfv(ind)=[];
                rtim(ind)=[];
                hrec(ind)=[];
            end
            figure(2)
            clg
            plot(t,scl0,'w');
            axis([0 max(t) nanmin(scl0) nanmax(scl0)])
            title(['Detected SCR onsets.']);
            xlabel('sec')
            ylabel('uS');
            if length(nsft)
                hold on
                plot(nsft/sr,y(nsft),'xg');
                x1=nsft/sr; x2=y(nsft);
                x1=x1(:); x2=x2(:);
                n=find(isnan(x1)|isnan(x2));
                x1(n)=[]; x2(n)=[];
                x1=x1+max(t)/100;
                for i=1:length(x1)
                    text(x1(i),x2(i),int2str(i));
                end
                hold off
            end
            title('SCL');
            figure(3)
            clg
            plot(t,nsfv0);
            axisx(0,max(t));
            title('NSF amplitude');
            figure(5)
            clg
            plot(t,[nsfr0 rtim0 hrec0])
            axisx(0,max(t));
            title('NSF rate (y) Rise Time (m), and Half Recovery Time (c)');

            setempty=input('Set certain intervals to missing data? (<Enter>=no [default], 1=yes) ==> ');
            if isempty(setempty)
                setempty=0;
            end
        end
        % while ~setempty
    end
    %if bno
end
%if bno|bplotyes

if all(isnan(y)) 
   nsfn=NaN; nsft=NaN; nsfv=NaN; 
   nsfr0=NaN; scl0=NaN; nsfv0=NaN; rtim0=NaN; hrec0=NaN;
   rtim=NaN; hrec=NaN  
end 



