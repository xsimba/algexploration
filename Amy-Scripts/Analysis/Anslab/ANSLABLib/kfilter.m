%***************************************************************************
%                                                                          *
%         Several filters can be testet, spectral analysis, etc.           *
%              ("Kfilter.m". Invoked from inside exam.m)                *
%                                                                          *
%***************************************************************************


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


s1old=s1;   % save first value
told=t;
s1=s1+1;
t(1)=[];
if ~spec
   y1old=y1;
   y1(1)=[];
end;

filtok=1;  % 0 = end of main-loop
notfilt=0; % flag for: leave out filter section, go to spectral analysis
bstr=[];   % initialize variable
fign=2;    % startup window
mm1=' '; mm0=1;
mn1=' '; mn0=1;
mo1=' '; mo0=1;




%*************************************** Linear interpolation of missing values
if any(isnan(y1))
[y1,no,noc]=nan_lipf(y1);
disp(' ');
disp([int2str(no),' of ',int2str(length(y1)),' values']);
disp(['in ',int2str(noc),' chains were interpolated.']);
disp('The filtering or spectral analysis can now be done.');
end

while filtok     %******************************************** Filter test loop

shc
disp(' ');
disp('Type of zero-phase filter:');
mm1='Butterworth';
mm2='Chebyshev I';
mm3='Chebyshev II';
mm4='Elliptic';
mm5='Pronys';
mm6='Yule-Walker';
mm7='Moving Average';
mm8='Spectral Analysis without filtering';
i=menue(mm1,mm2,mm3,mm4,mm5,mm6,mm7,mm8);
mm0=i;
if i==0 break; end;
if i==8
   notfilt=1;
else               %************************************ Begin of filter-section
   notfilt=0;
   disp(' ');
   disp('Order of the filter [ <Return> => Design ]');
   order=input('==>  ');
   if isempty(order)
      order=0;
      Wp=input('Passband corner frequency [Hz]  ==>  ');
      Ws=input('Stopband corner frequency [Hz]  ==>  ');
      disp(' ');
      disp(' Maximum permissible passband ripple/loss in dB, typical=3:');
      Rp=input('Passband attenuation      [dB]  ==>  ');
      disp(' ');
      disp(' Number of dB the stopband is down from the passband (e.g.=15).');
      Rs=input('Stopband attenuation      [dB]  ==>  ');
      Wp=Wp/(samplerate/2);
      Ws=Ws/(samplerate/2);
   end;

   if i<7
   disp(' ');
   disp('Filter range:');
   mn1='Low-Pass';
   mn2='High-Pass';
   mn3='Band-Pass';
   mn4='Band-Stop';
   band=menue(mn1,mn2,mn3,mn4);
   mn0=band;
   if band>2
      freq= input('First  cutoff frequency in Hz ==>  ');
      freq2=input('Second cutoff frequency in Hz ==>  ');
      if freq2<freq
         temp=freq2;freq2=freq;freq=temp;end;
      freq=[freq freq2];
   else
      freq= input('Cutoff frequency in Hz ==>  ');
   end;

   freq=freq ./(samplerate/2);

   bstr=[];
   if band==2 bstr=',''high'''; end;
   if band==4 bstr=',''stop'''; end;
   end; %if i<7

   if i==1
   if ~order
      [order,Wn]=buttord(Wp,Ws,Rp,Rs);
      j=Wn*samplerate/2;
      disp(' ');
      disp([' Order = ',int2str(order)]);
      disp(['Natural frequency to achieve the specifications: ',num2str(j)]);
      disp('Do you want to use this frequency?');
      j=menue('Yes','No');
      if j==1 freq=Wn; end;
   end;
   cmdstr=['[b,a]=butter(order,freq',bstr,');'];
   eval(cmdstr);
   end;

   if i==2
   rip=input('dB of ripple in the passband  ==>  ');
   if ~order
      [order,Wn]=cheb1ord(Wp,Ws,Rp,Rs);
      j=Wn*samplerate/2;
      disp(' ');
      disp([' Order = ',int2str(order)]);
      disp(['Natural frequency to achieve the specifications: ',num2str(j)]);
      disp('Do you want to use this frequency?');
      j=menue('Yes','No');
      if j==1 freq=Wn; end;
   end;
   cmdstr=['[b,a]=cheby1(order,rip,freq',bstr,');'];
   eval(cmdstr);
   end;

   if i==3
   rip=input('dB of ripple in the stopband down from peak in passband  ==>  ');
   if ~order
      [order,Wn]=cheb1ord(Wp,Ws,Rp,Rs);
      j=Wn*samplerate/2;
      disp(' ');
      disp([' Order = ',int2str(order)]);
      disp(['Natural frequency to achieve the specifications: ',num2str(j)]);
      disp('Do you want to use this frequency?');
      j=menue('Yes','No');
      if j==1 freq=Wn; end;
   end;
   cmdstr=['[b,a]=cheby2(order,rip,freq',bstr,');'];
   eval(cmdstr);
   end;

   if i==4
   rip=input('dB of ripple in the passband  ==>  ');
   rip2=input('dB of ripple in the stopband down from peak in passband  ==>  ');
   cmdstr=['[b,a]=ellip(order,rip,rip2,freq',bstr,');'];
   eval(cmdstr);
   end;

   if i==5
   disp('Sorry. Not yet implemented.');
   %[b,a]=prony(order,....freq/samplerate);
   end;

   if i==6
   disp('Sorry. Not yet implemented.');
   %[b,a]=yulewalk(order,....freq/samplerate);
   end;

   if i==7
   a=1;
   disp(' ');
   disp('Type of weighting window:');
   mo1='Boxcar (Unmweighted MA)';
   mo2='Triangular';
   mo3='Hanning';
   mo4='Hamming';
   mo5='Bartlett';
   mo6='Blackman';
   mo7='Chebyshev';
   mo8='Kaiser';
   j=menue(mo1,mo2,mo3,mo4,mo5,mo6,mo7,mo8);
   mo0=j;
   if j==1 weights=boxcar(order);end;
   if j==2 weights=triang(order);end;
   if j==3 weights=hanning(order);end;
   if j==4 weights=hamming(order);end;
   if j==5 weights=bartlett(order);end;
   if j==6 weights=blackman(order);end;
   if j==7 weights=chebwin(order);end;
   if j==8 weights=kaiser(order);end;
   b=weights ./sum(weights);
   end;

   %****************************************************************  Filtering

   tf0=clock;
   y2=filtfilt(b,a,y1);
   disp(' ');
   disp('The filtering process is now going on.');

   eval(['str1=mm',int2str(mm0),';']);
   eval(['str2=mn',int2str(mn0),';']);
   eval(['str3=mo',int2str(mo0),';']);
   filttype=[str1,'  ',str2,'  ',str3];

   figure(fign)

   clg
   subplot(311);
   titlestr=[filttype,'  Unfiltered Data'];
   eplotint(y1,s1,s2,titlestr,scale,samplerate,ltv1,1);
   subplot(312);
   titlestr='Filtered Data';
   eplotint(y2,s1,s2,titlestr,scale,samplerate,ltv1,1);
   y3=y1-y2;
   subplot(313);
   titlestr='Residuals';
   eplotint(y3,s1,s2,titlestr,scale,samplerate,ltv1);

   disp(' ');
   disp('Press any key to continue');
   pause
   disp(' ');
   m1='Try another filter';
   m2='Spectral Analysis of unfiltered data';
   m3='Spectral Analysis of filtered data';
   m4='Both Spectral Analysis of unfiltered and filtered data';
   m5='Spectral Analysis of the frequencies that were filtered out';
   m6='Filter the whole data with last specifications (takes time!)';
   m7='Statistics of filtered data';
   filtok=menue(m1,m2,m3,m4,m5,m6,m7);

end; %if i==8       %************************************ End of filter-section

if filtok==0 break; end;   %********************************* Spectral Analysis
if filtok==2 | notfilt==1
   clg;
   ax_reset;
   espcplf2(y1,samplerate);
end;
if filtok==3
   clg;
   ax_reset;
   espcplf2(y2,samplerate);
end;
if filtok==4
   clg;
   ax_reset;
   espcplf2(y1,samplerate,y2);
 end;
if filtok==5
   clg;
   ax_reset;
   espcplf2(y3,samplerate);
end;
if filtok==6
   tf0=clock;
   disp('The filtering process is going on. ');
   var1=filtfilt(b,a,var1);

   if 0
   str=[datastr,'f.mat'];
   disp(['Save filtered data as ',str,'?']);
   m1='No';
   m2='Yes';
   m3='Other name';
   i=menue(m1,m2,m3);
   if i==2
      cmdstr=['save ',str,' var1'];
      eval(cmdstr);
   end;
   if i==3
      str=input('Name for file  ==>  ','s');
      cmdstr=['save ',str,' var1'];
      eval(cmdstr);
   end;
   end;
end;
if filtok==7
   y1=y2;
   estatist; end;

end;  %while filtok

clg

s1=s1old;  % restore first value
t=told;
if ~spec
   y1=y1old;
end;

