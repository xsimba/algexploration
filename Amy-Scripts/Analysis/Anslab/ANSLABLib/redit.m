% Inspection and editing of artifacts in ECG




%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


% (called from findr)

% FW last revision 1-5-03: adapted to findrtp5

MVersion = version;
MVersNr = str2num(MVersion(1:3));

zoomyes=0;   % switch zoom function for option 2 (editing) on/off



artbegin=[]; artend=[];



%****** Inspect and/or exclude outliers loop

inspectloop=1;

cfig; cfig; cfig;

figure(1)

if screen>800  set(1,'Position',[3 233 1017 493]); else set(1,'Position',[2 202 792 356]); end



while inspectloop



figure(1)

clf



if exist('usecorr')

if usecorr==1;

subplot(2,1,1)

plot(rt_old/sr,difffit(rt_old)/sr*1000);

axisx(0,max(rt_old/sr));

plotline(alt_points/sr)

ylabel(['IBI [msec] = ',num2str(mean(nanrem(difffit(rt_old)/sr*1000)))]);

title('Uncorrected RR intervals');

subplot(2,1,2)

end

end



plot(rtn/sr,rtnd);

axisx(0,max(rtn/sr));

xlabel('time [sec]');

ylabel(['IBI [msec] = ',num2str(mean(nanrem(rtnd)))]);

title('RR intervals');



if zoomyes

zoomrb

end



m1='Exit editing';

m2='Editing of ECG';

m3='Exclude outliers';

m4='Start again';

m5='Display segment';

m6='Interpolate';

m7='Exclude intervals (incl. rectangular)';



outyes=menudef1('Please select',m1,m2,m3,m4,m5);



if ~outyes break; break; break; end;



if outyes<2  inspectloop=0; end;



if outyes==2

   ax=axis;

   x=[];

   hold on

   disp(' ');

   disp('Click twice to mark suspicious intervals, end with <0> to enter EXAM')



   while 1

   [i1,i2,i3]=ginput(1);

   if i3==48

     if length(x)/2==floor(length(x)/2)

        break; break; break; break; break;

     else

        disp(' ');

        disp('Odd number of lines.  Please click once more!');

        title('Odd number of lines.  Please click once more!');

        [i1,i2,i3]=ginput(1);

     end

   end;

   x=[x i1];

   plot([i1;i1],[ax(3);ax(4)],':c');   % display vertical line

   end;

   hold off

   x=x*sr;

   lookat=reshape(x,2,length(x)/2)';

   figure(1); clg

   clear var2 var3 var4



   var1=y;

   defblank

   int=6;

   moveint=60;

   samplerate=sr;

   title1str ='ECG';

   def

   if exist('artinterval')

      if ~isempty(artinterval)

         artbegin=artinterval(:,1); artend=artinterval(:,2);

      end

   end

   doubleyes=0;

   radapt=0;

   %markeryes=1;

   %markert=rtn;

   %markert=qt;  % requires to have Q-point detection pasted before IBI editing section in findrtq5

   %evscan=markert;

   event1=rtn;

   event1yes=1;

   yaxisstr='mV';




   disp(' ');
   disp('Editing of ECG in EXAM:');
   disp('Goto marked intervals with <0> and <9>');
   disp('Zoom into reasonably small interval with <z>');
   disp('Delete false R-waves with <#>');
   disp('Insert correct R-waves with <?>  (exact click) or <!> (click near peak)');
   disp('Interpolate missing R-waves with <$> (1 R-wave) or <%> (2 R-waves)');
   disp('Mark begin <b> and end <e> of artifact periods, remove with <->');
   disp('Page through beat-by-beat with <shift>  and > (forward)  and < (backward) .');
   disp('Jump to any event number with <j>.');



    exam;



   rtn=event1;     %*** add values specified from inside exam  (option @)

   rtnd=difffit(rtn)/sr*1000; % IBI



   if exist('artinterval') if ~isempty(artinterval)

   disp('Exclusion of values within specified artifact intervals');

   setmiss=between(rtn,artinterval);

   if ~isempty(setmiss)

      rtnd(setmiss)=ones(1,length(setmiss))*NaN;

      disp([int2str(setmiss),' values were set to missing.']);

   end; end; end;

   inspectloop=1;



end;



if outyes==3

   rtnd = outrect(rtnd,rtn/sr,1);

   inspectloop=1;

end;



if outyes==4  rtn=rt; rtnd=rtd; inspectloop=1; end;



if outyes==5

   [i1,i2,i3]=ginput(2);

   i2=round(i1*sr);

   n=find(rtn>=i2(1) & rtn<=i2(2));

   figure(2)

   clg

   t=i2(1):i2(2);

   i=find(t<=0);

   t(i)=[];

   i=find(t>rtn(length(rtn)));

   t(i)=[];

   yn=y(t);

   miny=min(yn); maxy=max(yn);

   fact=(max(nanrem(rtnd(n)))-min(nanrem(rtnd(n))))/(maxy-miny);

   yns=(yn-miny)*fact+min(nanrem(rtnd(n)));

   plot(t/sr,yns,'w',rtn(n)/sr,rtnd(n),'g');

   title('Segment of IBI series')

   xlabel('sec')

   i=ginput(1);

end;



if outyes==6

   rtnd=nan_lip(rtnd);

   inspectloop=1;

end;



if outyes==7

   [lookat,ind1]=inspect(rtn,rtnd);

   if ~isempty(ind1) rtnd(ind1)=NaN*ones(length(ind1),1); end;

   [i1,i]=size(lookat);

   for i=1:i1

       t1=ceil(lookat(i,1));

       t2=floor(lookat(i,2));

       n=find((rtn>t1)&(rtn<t2));

       rtnd(n)=NaN*ones(length(n),1);

   end;

end;



end;

%1****** inspect data for artifacts loop



rt=rtn;

rtd=rtnd;



