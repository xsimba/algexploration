function [ChLables,SR] = getlabel(CH,ReadFilePath,PrintStatus);

%   getlable
%
%   script to retrieve channel labels from a biopac datafile
%   in corresponding order as in CH.

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

SR = [];
EmptyChStatus = 0;
if nargin<3;PrintStatus = 0;end
if nargin<2;ReadFilePath = [];end
if nargin<1;
    CH= ['EC';    % ECG                        % Variable names
         'R1';    % Resp
         'SC';    % SCL
         'RA';    % Rating
         'IC';    % ICG
         'FP';    % FPA
         'EP';    % EPA
         'TP';    % Temp
         'AC';    % Activity
         'R1';    % Respiration 1 (thoracic)
         'R2';    % Respiration 2 (abdominal)
         'BP';    % Finapres Blood Pressure
         'PC';    % PCO2
         'N1';    % Startle EMG 1
         'N2';    % Startle EMG 2
         'BL'];    % Blush];
     EmptyChStatus = 1;
     PrintStatus = 1;
end

%Strings to look for
%EcgLabels,SclLabels,RatingLabels,IcgLabels,FPALabels,EPALabels,TempLabels,ActivityLables,RespThorLabels,
%RespAbdLabels,BPLables,PCo2Lables,Startle1Lables,Startle2Lables,BlushLabels
EcgLabels =     ['ECG Lead II';
                 'EKG        ';
                 'EC         '];

SclLabels =     ['EDA    ';
                 'SCL    ';
                 'SC     '];

RatingLabels =  ['Rating ';
                 'RA     ';
                 'rating '];

IcgLabels =     ['dz/dt  ';
                 'ImpCard';
                 'icg    '];

FPALabels =     ['FPA    ';
                 'FP     ';
                 'Pleth  ';
                 'pleth  '];

EPALabels =     ['EPA    ';
                 'EP     ';
                 'Pleth  ';
                 'pleth  '];

TempLabels =    ['Temp   ';
                 'Tmp    ';
                 'temp   '];

ActivityLables =['Accel  ';
                 'ACC    ';
                 'AC     '];

RespThorLabels= ['RespTho ';
                 'RespThor';
                 'R1      ';
                 'r1      '];

RespAbdLabels=  ['RespAbd';
                 'R2     ';
                 'r2     '];

BPLables     =  ['BP        ';
                 'BloodPress';
                 'bp        '];

PCo2Lables   =  ['pCO2     ';
                 'PCO2     ';
                 'PcO2     ';
                 'pco2     ';
                 'pco2     ';
                 'pcO2     ';
                 'CO2      ';
                 'co2      '];

Startle1Lables= ['EMG1   ';
                 'EMG 1  ';
                 'emg 1  ';
                 'emg1   '];

Startle2Lables= ['EMG2   ';
                 'EMG 2  ';
                 'emg 2  ';
                 'emg2   '];

BlushLabels   = ['Blush  ';
                 'BL     ';
                 'blush  ';
                 'bl     '];


%   which acq-file you whish to know the labels of
if isempty(ReadFilePath);
    ReadFilePath ='F:\sxp\SXP00101.ACQ';
    [ReadFile,ReadPath] = uigetfile('*.acq','Please choose *.acq-file:');
    ReadFilePath = [ReadPath,ReadFile];
end

%  start reading label in acq-file
[ChLabelCell,SR] = bphread(ReadFilePath);
if PrintStatus
    fprintf(1,'Found the following channels labels:\n');
    for ChanInd = 1:size(ChLabelCell,1)
        fprintf(1,ChLabelCell(ChanInd,:));
        fprintf(1,'\n');
    end
end
if EmptyChStatus
    return
end
NLabels = size(CH,1);
ChLables =zeros(NLabels,40);
for ChanInd = 1: size(CH,1)
    Chan = CH(ChanInd,:);

    if ~isempty(findstr(ReadFilePath,'.ACQ'))
        DefaultPath = strrep(ReadFilePath,'.ACQ','ChanChoice.mat');
    elseif ~isempty(findstr(ReadFilePath,'.acq'))
        DefaultPath = strrep(ReadFilePath,'.acq','ChanChoice.mat');
    else
        error('Unknown file type!');
    end
    DefaultVarName =  [Chan,'Choice'];
    if ~isfile(DefaultPath); eval([DefaultVarName,'=1;']);
    else;load(DefaultPath);eval(['if ~exist(''',DefaultVarName,''');',DefaultVarName,'=1;end;']);end


    if strcmp(Chan,'EC')% ECG

        for StringInd = 1:size(EcgLabels,1)
            Found = lookup(ChLabelCell,deblank(EcgLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following ECG-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);

                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found) & ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'SC')% SCL


        for StringInd = 1:size(SclLabels,1)
            Found = lookup(ChLabelCell,deblank(SclLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following EDA-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];


    elseif strcmp(Chan,'RA')% Rating


        for StringInd = 1:size(RatingLabels,1)
            Found = lookup(ChLabelCell,deblank(RatingLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following rating-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];


    elseif strcmp(Chan,'IC')% ICG



       for StringInd = 1:size(IcgLabels,1)
            Found = lookup(ChLabelCell,deblank(IcgLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following ICG-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];


    elseif strcmp(Chan,'FP')% FPA


        for StringInd = 1:size(FPALabels,1)
            Found = lookup(ChLabelCell,deblank(FPALabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following FPA-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'EP')% EPA



        for StringInd = 1:size(EPALabels,1)
            Found = lookup(ChLabelCell,deblank(EPALabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following EPA-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'TP')% Temp


        for StringInd = 1:size(TempLabels,1)
            Found = lookup(ChLabelCell,deblank(TempLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following temperature-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'AC')% Activity

          for StringInd = 1:size(ActivityLables,1)
            Found = lookup(ChLabelCell,deblank(ActivityLables(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following activity-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'R1')% Respiration 1 (thoracic)

        for StringInd = 1:size(RespThorLabels,1)
            Found = lookup(ChLabelCell,deblank(RespThorLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following thor.respiration-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'R2')% Respiration 2 (abdominal)


        for StringInd = 1:size(RespAbdLabels,1)
            Found = lookup(ChLabelCell,deblank(RespAbdLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following abd.respiration-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'BP')% Finapres Blood Pressure


        for StringInd = 1:size(BPLables,1)
            Found = lookup(ChLabelCell,deblank(BPLables(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following bloodpressure-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'PC')% PCO2



        for StringInd = 1:size(PCo2Lables,1)
            Found = lookup(ChLabelCell,deblank(PCo2Lables(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following pCO2-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'N1')% Startle EMG 1


        for StringInd = 1:size(Startle1Lables,1)
            Found = lookup(ChLabelCell,deblank(Startle1Lables(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following EMG1-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'N2')% Startle EMG 2


        for StringInd = 1:size(Startle2Lables,1)
            Found = lookup(ChLabelCell,deblank(Startle2Lables(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following EMG2-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1
                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             edit('loadstu2.m');
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    elseif strcmp(Chan,'BL')% Blush

         for StringInd = 1:size(BlushLabels,1)
            Found = lookup(ChLabelCell,deblank(BlushLabels(StringInd,:)));
            if length(Found)>1
                fprintf(1,'The following blush-channels were found:\n');
                for TmpInd = 1:length(Found)
                    fprintf(1,[num2str(TmpInd),') ',ChLabelCell(Found(TmpInd),:)]);
                    fprintf(1,'\n');
                end
                eval(['DefaultVal = ',DefaultVarName,';']);
                Type = input(['Which one do you wish to use? [default = ',num2str(DefaultVal),']']);
                if isempty(Type)
                    eval(['Type = ',DefaultVarName,';']);
                end
                eval([DefaultVarName,' = Type;']);
                eval(['save(''',DefaultPath,''',''',DefaultVarName,''');']);
                ChLables(ChanInd,1:size(ChLabelCell(Found(TmpInd),:),2))=ChLabelCell(Found(TmpInd),:);
                break;
            elseif length(Found)==1

                ChLables(ChanInd,1:size(ChLabelCell(Found,:),2))=ChLabelCell(Found,:);
                break;
            end
        end
        if isempty(Found)& ~EmptyChStatus
             disp(['Unable to find corresponding channel label for ',CH(ChanInd,:),'!']);
             error(['Check your settings in ''loadstu2.m''!']);
        end
        Found = [];

    end
end

if PrintStatus
    fprintf(1,'Found the following channels to analyze:\n');
    for ChanInd = 1:size(ChLables,1)
        fprintf(1,setstr(ChLables(ChanInd,:)));
        fprintf(1,'\n');
    end
end




return


