% kpep.m   edit points in ICG: '

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


samr=samplerate;

m1='BLUE:    Q';
m2='YELLOW:  T';
m3='Exclude complete QT';
ed=menu('',m1,m2,m3);

%if ed==4
%   title('Add cycle by clicking on 3 points that define missing beat (order not relevant)');
%   x=ginput(3);
%elseif ed==5
if ed==3
   title('Click near end of T wave you want to exclude')
   x=ginput(1);
else
   x=ginput(2);
end;

x(:,2)=[];
x=round(x*samr);

if ed==1
[i,mini]=min(abs(qtn-x(1)));    % ******** change var names here
qtn(mini)=x(2);
event2=qtn;
qti=ttn-qtn;
qtin=qti;

end;
if ed==2
[i,mini]=min(abs(ttn-x(1)));
x(2);

ttn(mini)=x(2);
qti=ttn-qtn;
qtin=qti;
event1=ttn;
end;
if ed==3
[i,mini]=min(abs(ttn-x(1)));
ttn(mini)=NaN;
event1=ttn;
qti=ttn-qtn;
qtin=qti;
end;
%if ed==4
%icg_time(length(icg_time)+1)=x(1);
%icg_peak_time(length(icg_peak_time)+1)=x(2);
%X_time(length(X_time)+1)=x(3);
%end;
%Vti(maxi)=[];
%Vte(maxi)=[];
%Tt(maxi)=[];
end;


% take care of wrong order input
% M=breath time matrix
% construct n first for faster sorting
%n=[(icg_time(1:length(icg_time)-1))';icg_peak_time';X_time';mint'];

qti=ttn-qtn;

