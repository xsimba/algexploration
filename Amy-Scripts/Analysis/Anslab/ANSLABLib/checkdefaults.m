% Setting start parameters for EXAM 2.0
% F.W. last update 10-24-95


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



MVersion = version;
MVersNr = str2num(MVersion(1:3));


if exist('var1')~=1   % first variable
    answer = questdlg('No data loaded. Import data now?','Data import:','Yes','No','Yes');
    if strcmp(answer,'No');
        QuitStatus = 1;
        return
    else
        ANSLABImport;
        QuitStatus = 1;
    end
end
if ~exist('int');int=6;end        % Display interval [sec]
if ~exist('moveint');moveint =60;end   % Move interval [sec]
if ~exist('samplerate');disp('No sample rate given, defaulting to 100Hz...');samplerate = 100;
else;disp('Sample rate of ',num2str(samplerate),'...');end
if ~exist('title1str'); title1str=[];  end  % title of graph
if ~exist('ltv1');ltv1='-';end   % linetype (appears in title, too)
if ~exist('yaxisstr');yaxisstr = 'Units' ;end
if ~exist('secvar2');secvar2=0;end
if ~exist('secvar3');secvar3=0;end
if ~exist('secvar4');secvar4=0; end
if ~exist('var2')
   var2 = [];
   secvar2=0;
   title2str = [];
   ltv2     ='-';
end;
if ~exist('var3')
   secvar3=0;
   var3 = [];
   title3str = [];
   ltv3      ='-';
end;
if ~exist('var4')
   secvar4=0;
   var4 = [];
   title4str = [];
   ltv4      ='-';
end;


ExistStatus = 1;

if ~exist('radapt');ExistStatus = 0;end     % adapt y-axis range of second variable
if ~exist('shift');ExistStatus = 0;end         % shift of y in x-direction
if ~exist('shifty');ExistStatus = 0;end           % shift of var2 in y-direction
if ~exist('s1');ExistStatus = 0;end     % begin of first graph interval [seconds]
if ~exist('z');ExistStatus = 0;end       % first key
if ~exist('circle');ExistStatus = 0;end      % circles as additional info on plot of var1: C
if ~exist('scale');ExistStatus = 0;end       % scale of x-axis (1=points, 2=seconds)
if ~exist('gridyes');ExistStatus = 0;end       % grid
if ~exist('doubleyes');ExistStatus = 0;end       % two plots on one screen
if ~exist('odd');ExistStatus = 0;end         % first plot in upper(2) or lower(1) window
if ~exist('zoomf');ExistStatus = 0;end         % zoom factor  (2)
if ~exist('skiploc');ExistStatus = 0;end        % location of event in window during skip through (left:0  to  right:1)
if ~exist('skipfact');ExistStatus = 0;end       % skip factor: S
if ~exist('artmarkyes');ExistStatus = 0;end      % mark artifacts by horizontal lines
if ~exist('markeryes');ExistStatus = 0;end
if ~exist('spec');ExistStatus = 0;end            % flag for spectrum as lower graph  (0)
if ~exist('speclog');ExistStatus = 0;end         % flag for log-scale for specplot   (0)
if ~exist('chz1');ExistStatus = 0;end          % truncate spectrum plot below chz1 Hz (0)
if ~exist('chz2');ExistStatus = 0;end          % truncate spectrum plot above chz2 Hz
if ~exist('res');ExistStatus = 0;end            % Factor (power of 2) to reduce highest resolution of FFT. If res>16: res determines window size of FFT.
if ~exist('spectype');ExistStatus = 0;end      % type of spectrum plot, 1-7   (1)
if ~exist('rems1');ExistStatus = 0;end          % remember-vector left and right
if ~exist('rems2');ExistStatus = 0;end
if ~exist('E');ExistStatus = 0;end              % calibration

CheckedStatus = 1;

if ExistStatus==0
    answer = questdlg(char('One ore more setting variables are undefined. ','',...
    'You must load settings from a default file before','exploring a signal. Do you wish to load a standard',...
        'settings-file?'),'No settings:','Yes','No','Cancel','Yes');
    if strcmp(answer,'Cancel');
        return
    elseif strcmp(answer,'No');
        return
    end
else
    return
end

ExamKeys = 1;
radapt    = 0;          % adapt y-axis range of second variable
shift     = 0;          % shift of y in x-direction
shifty    = 0;          % shift of var2 in y-direction

s1        =0;   % begin of first graph interval [seconds]
z         =29;  % first key
circle    =0;   % circles as additional info on plot of var1: C
scale     =2;   % scale of x-axis (1=points, 2=seconds)
gridyes   =0;   % grid
doubleyes =1;   % two plots on one screen
odd       =2;   % first plot in upper(2) or lower(1) window
zoomf      =2;   % zoom factor  (2)
skiploc   =.1;  % location of event in window during skip through
                % (left:0  to  right:1)
skipfact  =1;   % skip factor: S
artmarkyes=1;   % mark artifacts by horizontal lines

%if exist('markersec')
%markert= markersec*samplerate;  % time of marker (each press is graphed)
%markeryes =1;   % display marker as vertical line
%else
 markeryes =0;
%end;

spec      =0;   % flag for spectrum as lower graph  (0)
speclog   =0;   % flag for log-scale for specplot   (0)
chz1      =0;   % truncate spectrum plot below chz1 Hz (0)
chz2      =samplerate/2;   % truncate spectrum plot above chz2 Hz
res       =1;   % Factor (power of 2) to reduce highest resolution of FFT
                % If res>16: res determines window size of FFT.
spectype  =1;   % type of spectrum plot, 1-7   (1)

rems1     =[];  % remember-vector left and right
rems2     =[];
E         =[];  % calibration


%************************ not to change ***************************

if scale==1 scalefact=1;               % factor for the x-axis scale
else scalefact=samplerate; end;
if s1==0 s1=1; else s1=samplerate*s1; end;
s2=s1+samplerate*int;
moveint=(moveint*samplerate)-(s2-s1);  % move-fast interval [points]

plotyes   =1;   % plot first graph
s1old     =s1;  % old graph x-limits before zoom
s2old     =s2;
ylimitstr =[];  % display min and max of other variable in title
ytrunc    =0;   % truncate y-axis
others    =0;   % other menue under graph
menuefirst=0;   % first new menue under graph (clg is required!)
evnumyes  =0;   % display event number on graph
evscani   =0;   % index of event scan
recallnumyes=0; % display remember number on graph
lookatyes =0;   % display lookat number (option O and 9 -> artifactual interval)
remi      =0;   % remember-index of copy
%lookat    =[];  % lookat-interval 2-column vector
lind      =0;   % lookat interval index
menueyes  =1;   % display menue

menuestr  = ['  ZoomUnzoomAdjustYtrunc=adapt       Grid PointsCircleDouble';
             '  Dist Prop BeginAEndArt-remov       Vars SpectrFilterCopy  '] ;
menuevec  = [122,117,97,121,61,120,103,112, 99,100,100;
              68, 80,98,101,45,120, 86, 83, 70, 67, 67];
ni        = 0:.097:1;  % index for display of menuestr
lt1='-';lt2='--';lt3=':';lt4='-.';lt5='x';  % linetypes
lt6='+';lt7='*';lt8='o';lt9='.';lt10='.i';
keystr    ='^'; % dummy for display of last pressed key

%ynan=y(~isnan(var1));
%yax1=min(ynan); yax2=max(ynan);     % hold y-axis for all intervals
%axishold=1;


if exist('var1')==1 var1=var1(:); end;
if exist('var2')==1 var2=var2(:); end;
if exist('var3')==1 var3=var3(:); end;
if exist('var4')==1 var4=var4(:); end;

%*** Define events
% any event must be adapted to correct samplerate,
% e.g. event1=rtime*samplerate/384

if ~exist('artbegin')
artbegin  =[];          % artifact marker
artend    =[];          % artifact marker
end;

valueyes  =0;          % display values of events
val       =0;          % value
valtime   =0;          % time of event for value
doubleyes = 0;

event1    =[]; %mintime;  % events 1-3 to mark
event2    =[];
event3    =[];
event1yes =0;
event2yes =0;
event3yes =0;
evscan    =[];  % define which event to skip through: k

clear RESPEDIT;  %indicates plot of circles in exam
subvarplot=0;
