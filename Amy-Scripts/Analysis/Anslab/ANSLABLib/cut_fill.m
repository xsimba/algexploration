function [v1,cutyes]=cut_fill(v1,lv1);
% function [v1,cutyes]=cut_fill(v1,lv1);
% cutting a vector at the end to a length of len (=> cutyes=1),
% filling it with NaN if neccessary (=> cutyes=0).

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

cutyes=0;
lv2=length(v1);
if lv1>lv2
   v1=nan_fill(v1,lv1);
else if lv1<lv2
   cutyes=1;
   v1(lv1+1:lv2)=[]; end;
end;

