function yf=notch(y,sr,freq,width,order,ripple,plotyes);

%NOTCH General IIR Notch Filter.
%   yf=notch(y,sr,freq,width,order,ripple,plotyes);
%   Returns data after being passed through a
%   Chebyshev Type I IIR digital notch filter.
%
%   sr: samplerate in Hz [1000 Hz]
%   freq: frequency to be filtered out [60 Hz]
%   width: stop band width in Hz [4 Hz]
%   order: order of the filter (effectively doubled by filtfilt) [7]
%   ripple: dB ripple in the passband [.5 dB]
%   plotyes: plot filtered and unfiltered signal and spectra


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<2 sr=1000; end
if nargin<3 freq=60; end
if nargin<4 width=4; end
if nargin<5 order=7; end
if nargin<6 ripple=.5; end
if nargin<7 plotyes=0; end


% y=IC(1:10000); sr=400; freq=60; width=4; order=7; ripple=.5; plotyes=1;

freq=freq ./(sr/2);
width=width ./(sr/2);
w1=freq-width/2;
w2=freq+width/2;
wn=[w1 w2];
[B,A]=cheby1(order,ripple,wn,'stop');
yf = filtfilt(B,A,y);

if plotyes

figure
t=(1:length(y))/sr;
plot(t,yf,t,y)
title('Filtered signal (yellow), unfiltered signal (magenta)')
figure
subplot(3,1,1)
spectrum(y,[],[],[],sr)
title('Spectrum of unfiltered signal')
subplot(3,1,2)
spectrum(yf,[],[],[],sr)
title('Spectrum of filtered signal')
x=y-yf;
subplot(3,1,3)
spectrum(x,[],[],[],sr)
title('Spectrum of difference signal')


if 1  % test against simple low-pass
yf2=filthigh(yf,50,sr,9);
figure
subplot(3,1,1)
spectrum(y,[],[],[],sr)
title('Spectrum of unfiltered signal')
subplot(3,1,2)
spectrum(yf2,[],[],[],sr)
title('Spectrum of low-pass filtered signal')
x2=y-yf2;
subplot(3,1,3)
spectrum(x2,[],[],[],sr)
title('Spectrum of difference signal')
end



%IC=filtlow(IC,1,sr,7);

end




