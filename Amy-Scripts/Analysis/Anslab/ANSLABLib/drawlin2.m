function y=drawlin2(len,ampl1,ampl2,y,filtdefyes);
% function y=drawlin2(len,ampl1,ampl2,y,filtdefyes);
% draw a complex trend
% len    x-axis length of field to draw to [100]
% ampl1/2 y-axis (low/high)
% y      input of vector to modify
% filtdefyes: get menu for input of fiter parameters [default=no=0: use sr=25 Hz for respiration]
% mouse button functions:
% left:  draw lines
% right: delete intervals
% f-key: call menue for filtering (press twice)
% c-key: cut floor (used for resets) (press twice)
%        then click with mouse on interval where floor
%         needs to be cut off.
%         The y-axis value of the first click defines
%           the y-value of the new line
% exit with any other key (press twice)

%test
%len=length(yr);ampl1=min(nanrem(yr))-i*.2;ampl2=max(nanrem(yr))+i*.2;y=yr;

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<5 filtdefyes=0; end;
if nargin<4 y=ones(len,1)*NaN; end;
if nargin<3 ampl2=100; end;
if nargin<2 ampl1=-100; end;
if nargin<1 len=100; end;

yback = y;
try

if 0
% adjust y-axis to y if necessary
n=nanrem(y);
if length(n)>1
   ampl1=min(n);
   ampl2=max(n);
elseif length(n)
   ampl1=-abs(n);
   ampl2=abs(n);
end;
end;

if len<length(y) len=length(y); end;

plusy=(ampl2-ampl1)/20;

i3=1;
while i3<4 | i3==102

clg
plot(y);
axis([1 len ampl1-plusy ampl2+plusy]);
title('Draw triangles with 3 clicks. Interpolate with 2+1 click.');
xlabel('<f> twice = filter to smooth edges.  Right-below twice = exit.');

[i1,i2,i3]=ginput(2);

if i1(1)>len & i2(1)<(ampl1-plusy)  % quit
   i3=5; break; break;
else
if i1(1)<1 i1(1)=1; end;
if i1(2)<1 i1(2)=1; end;
if i1(1)>len i1(1)=len; end;
if i1(2)>len i1(2)=len; end;
end

if i3==1

new=0;  % flag for at least one new point
if isnan(y(round(i1(1))))
   y(i1(1))=i2(1);
   new=1;
end;
if isnan(y(round(i1(2))))
   y(i1(2))=i2(2);
   new=1;
end;

if new
   [i1,ind]=sort(i1);
   i2=i2(ind);
   i1=round(i1);
   y(i1(1)+1:i1(2)-1)=interp_l(y(i1(1)),y(i1(2)),i1(2)-i1(1)-1);
else
   [i1(3),i2(3),i3]=ginput(1);
   [i1,ind]=sort(i1);
   i2=i2(ind);
   i1=round(i1);
   y(i1(1)+1:i1(2)-1)=interp_l(y(i1(1)),i2(2),i1(2)-i1(1)-1);
   y(i1(2))=i2(2);
   y(i1(2)+1:i1(3)-1)=interp_l(y(i1(2)),y(i1(3)),i1(3)-i1(2)-1);
end;

end; %i3==1

if i3==3
   n=i1(1):i1(2);
   y(n)=ones(length(n),1)*NaN;
end;

if i3==99   % c: cut floor

   [i1,i2,i3]=ginput(2);
   [i1,ind]=sort(i1);
   y1=y(i1(1):i1(2));
   n=find(y1<i2(1));
   y1(n)=NaN*ones(size(n));
   y1=nan_lip(y1);
   y(i1(1):i1(2))=y1;

end;

if i3==102  % f: filter
 if ~filtdefyes
   freq=0; freq2=1; samplerate=25; order=5;
   ynan=find(isnan(y));
   y=nan_lip(y);
   if freq==0
      y=filthigh(y,freq2,samplerate,order);
   elseif freq2==0
      y=filtlow(y,freq,samplerate,order);
   else
      y=filtband(y,freq,freq2,samplerate,order);
   end;
   y(ynan)=ones(length(ynan),1)*NaN;
 else
  i=input('Undo previous filtering? [<enter>=no, 1=yes] ==> ');
  if i==1
   if exist('y1') y=y1; end;
  else
   y1=y;
   freq=input('Lower cutoff frequency [<enter>=0 Hz] ==> ');
   if isempty(freq) freq=0; end;
   freq2=input('Upper cutoff frequency [<enter>=1.0 Hz, none=0] ==> ');
   if isempty(freq2) freq2=1; end;
   samplerate=input('Samplerate [<enter>=25 Hz] ==> ');
   if isempty(samplerate) samplerate=25; end;
   order=input('Order [<enter>=5] ==> ');
   if isempty(order) order=5; end;
   ynan=find(isnan(y));
   y=nan_lip(y);
   if freq==0
      y=filthigh(y,freq2,samplerate,order);
   elseif freq2==0
      y=filtlow(y,freq,samplerate,order);
   else
      y=filtband(y,freq,freq2,samplerate,order);
   end;
   y(ynan)=ones(length(ynan),1)*NaN;
  end;
 end
end;


end; %while

catch
y = yback;
end
return