function [v3,v4,d]=cut_len(v1,v2);
%function [v3,v4,d]=cut_len(v1,v2);
% adapt lengths of two vectors by cutting the bigger one at the end
% v3, v4: new vectors
% d=length(v1)-length(v2);

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

l=min([length(v1) length(v2)]);
v3=v1(1:l);
v4=v2(1:l);
d=length(v1)-length(v2);
