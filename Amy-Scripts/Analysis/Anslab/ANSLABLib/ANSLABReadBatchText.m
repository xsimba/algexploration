function [InFileMat,NFiles] = ReadBatchText(ReadStatus,SpecString,BatchFilePath)
%	ReadBatchText.m
	
%   EMEGS - Electro Magneto Encephalography Software                           
%   � Copyright 2005 Markus Junghoefer & Peter Peyk                            
%   Implemented programs from: Andrea de Cesarei, Thomas Gruber,               
%   Olaf Hauk, Andreas Keil, Olaf Steinstraeter, Nathan Weisz                  
%   and Andreas Wollbrink.                                                     
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
if nargin<3; BatchFilePath = [];end
if nargin<2; SpecString=[]; end
if nargin<1; ReadStatus=[]; end

MatlabVersionString=version;
MainMatlabVersion=str2num(MatlabVersionString(1));
if MainMatlabVersion<5; 
	fprintf(1,'Sorry, can not use ReadWriteBatchText.m with Matlab versions below Matlab5.'); 
	return;
end
InFileMat=[];
NFiles=0;

[ReadStatus]=IfEmptyInputSpecVal(ReadStatus,[1 2],1,...
'Do you want to',...
'read all lines or          [1]',...
'special masked lines       [2]');


if ReadStatus==2
	[SpecString]=IfEmptyInputString('Please choose the string to look for:',[],SpecString,'at1',1);
end

	
TotalFileIndex=0;
if isempty(BatchFilePath)
    [File,Path]=uigetfile('*','Open text file of file names: ');
else
    FilePath = BatchFilePath;
    [File,Path] = SepFilePath(FilePath);
end
FilePath=[Path,File];
[BatchFid] = fopen(FilePath, 'r');
if BatchFid==-1; error('Bad fid in ReadBatch.m'); end
NFiles=0;
InFileMat=[];
while feof(BatchFid) == 0;
	NFiles=NFiles+1;
	TextLine=fgetl(BatchFid);
	if NFiles==1
		InFileMat=char(TextLine);
	else
		InFileMat=char(InFileMat,TextLine);
	end
end
if ReadStatus==2
	FileIndexVec=0;
	for FileIndex=1:NFiles
		s=findstr(InFileMat(FileIndex,:),SpecString);
		if ~isempty(s)
			TotalFileIndex=TotalFileIndex+1;
			FileIndexVec(TotalFileIndex)=FileIndex;
		end
	end
	if TotalFileIndex==0
		fprintf(1,'Sorry, no file with this special string found.')
		NFiles=0;
		InFileMat=[];
	else
		InFileMat=InFileMat(FileIndexVec,:);
		NFiles=length(FileIndexVec);
	end
end
if ReadStatus==3 | (ReadStatus==2 & TotalFileIndex~=0)
	fprintf(1,'Read the following %g file names:\n\n',NFiles);
	disp(InFileMat)
end
	
return;
