function FindAcqSegFcn(ReadFP)


%   FindAcqSeg
%
%   FilePath
%   PlateauStatus
%   DiscardInitial
%   Discard
%   PlotStatus

%   FindAcqSeg reads in Biopac data and returns segmentation
%   information based on a dedicated 'Marker'-channel in the data.
%   If 'DiscardInitial' is set to 1 (default), the initial value is
%   ignored.
%   Markervalues found in 'Discard' are considered to be irrelevant.
%   Segment information containing the start and end point of any
%   relevant marker value is returned.

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if ~exist('PlotStatus');PlotStatus = [];end
if ~exist('Discard');Discard = [];end
if ~exist('DiscardInitial');DiscardInitial = [];end
if ~exist('PlateauStatus');PlateauStatus = [];end
if ~exist('ReadFP');ReadFP = [];end
if ~exist('OutFilePath');OutFilePath = [];end

if isempty(ReadFP);
    InitPath = PsyPath(1);
    [AcqFile,AcqPath] = uigetfile([InitPath,'*.ACQ'],'Please choose ACQ-file:');
    if isequal(AcqFile,0)|isequal(AcqPath,0);return;end;
    PsyPath(2,AcqPath);
    AcqFilePath = [AcqPath,AcqFile];
    ReadFP = AcqFilePath;
    FileNr =str2num(AcqFile(7:8));
end

if isempty(PlateauStatus);PlateauStatus = 1;end
if isempty(DiscardInitial);DiscardInitial = 1;end
if isempty(Discard);Discard = 0;end
if isempty(PlotStatus);PlotStatus = 1;end
if isempty(OutFilePath);
    InitPath = PsyPath(1);
    OutFilePath = strrep(ReadFP,'.ACQ','.EVT');
end

%read data
if ~exist('ExtractChannel');ExtractChannel = [];end
ExtractChannelBak = ExtractChannel;
ExtractChannel = 'Marker';
t1 = 1;
t2 = inf;
PlotStatus = 0;
readbp;
PlotStatus = 1;
ExtractChannel = ExtractChannelBak;

SR = 1000/dSampleTime;
fprintf(1,['Marker channel contains ',num2str(size(DATA,2)),' points...\n']);
fprintf(1,['Sample rate is :',num2str(SR),' Hz...\n']);
NSamples = size(DATA,2);
if DiscardInitial
    Changes = [];
    for SampInd = 2:size(DATA,2)
        if DATA(1,SampInd)~=DATA(1,SampInd-1)
            Changes = [Changes SampInd];
        end
        if mod(SampInd,20000)==0
            fprintf(1,'.')
        end
        if mod(SampInd,1000000)==0
            fprintf(1,'\n')
            fprintf(1,[num2str(SampInd),' of ',num2str(NSamples),' samples read ...\n']);
        end
    end
    fprintf(1,'\n')
	for R=2:length(Changes)
        if Changes(R)==Changes(R-1)+1
            Changes(R-1) = 999;
        end
	end
	Changes(Changes==999)=[];
	MarkerList = [];
	if PlateauStatus
        NPlateaus = length(Changes) - 1;
        Segments = [];
        EventCount = 1;
        for PlatInd = 1: NPlateaus
            if isempty(find(DATA(Changes((PlatInd-1)+1))==Discard))
                Segments(EventCount,1) = DATA(Changes((PlatInd-1)+1));
                Segments(EventCount,2) = Changes((PlatInd-1)+1);
                Segments(EventCount,3) = Changes((PlatInd-1)+2);
                EventCount = EventCount +1;
            end
        end
    else
        error('only plateau supported so far!');
    end
    Segments(:,4) = Segments(:,3)-Segments(:,2);
else
    error('not supported yet!');
end

if PlotStatus
	figure('Name',ReadFP,'Numbertitle','off')
    h = axes;
	plot(DATA);
    hold on
    YVec1 = DATA(Segments(:,2));
    YVec2 = DATA(Segments(:,3));
    plot(Segments(:,2),YVec1,'bO','parent',h);
end
SegmentsBak = Segments(:,1);
[SegmentsBak,Order] = sort(SegmentsBak);
Segments = Segments(Order,:);

PointS = 1/SR;
Segments(:,2) = Segments(:,2).*PointS;
Segments(:,3) = Segments(:,3).*PointS;
Segments(:,4) = Segments(:,4).*PointS;


fid = fopen(OutFilePath,'wt');
fprintf(fid,['0\t0\t0\t0\t0;\n']);
for LineInd = 1:size(Segments,1)
    fprintf(fid,['1\t']);
    fprintf(fid,[num2str(Segments(LineInd,1)),'\t']);
    fprintf(fid,[num2str(Segments(LineInd,2)),'\t']);
    fprintf(fid,[num2str(Segments(LineInd,3)),'\t']);
    fprintf(fid,[num2str(Segments(LineInd,4)),';\n']);
end
fclose(fid);
edit(OutFilePath);

return

