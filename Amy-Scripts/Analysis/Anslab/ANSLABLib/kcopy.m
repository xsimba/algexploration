% kcopy.m   copy recent graph: c

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

rems1=[rems1 s1]; rems2=[rems2 s2];
plotyes=0;
ii=s2-s1;
valx=(s1+(s2-s1)/2)/scalefact;
str=num2str(length(rems1));

if comptype<2
   cmdstr=['text(''Position'',[valx yax2-as],''String'',''',str,''')'];
else
   cmdstr=['text(valx,yax2-as,''',str,''')'];
end;

hold on; eval(cmdstr); hold off;
z=999;
