function [window,noverlap,k,left]=fftwin(x,win,nol,adjtype,dispyes)
% function [window,noverlap,k,left]=fftwin(x,win,nol,adjtype,dispyes)
% Preprocessing of parameters for Welch's averaged periodogram method (e.g., PSD)
% to obtain optimal inclusion of all points of a signal X.  Conventional use
% of PSD with window segments of length WINDOW - overlapping by NOVERLAP - usually
% excludes considerable number of points at end of signal from spectral analysis.
% x: input signal to determine length, alternatively length(x) can be specified
% win: length of window segments, default=240
% nol: points overlap between consecutive segments, default=win/2
% adjtype: type of adjustment to include all points
%          1 = adjust overlap (default)
%          2 = adjust window size and overlap to keep overlap/window ratio
%          3 = adjust window size  (not yet implemented)
% window: resulting window size
% noverlap: resulting overlapping number of points
% k: number of segments
% left: points at end of signal left out from analysis
% dispyes: display results


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


% [noverlap,window,k,left]=fftwin(479,240,120,1,1)
% n=479; win=240; nol=120; adjtype=1; dispyes=1;

if nargin<5 dispyes=1; end
if nargin<4 adjtype=1; end
if nargin<2 win=240; end
if nargin<3 nol=win/2; end
if length(x)==1 n=x; else n=length(x); end;

if n<win window=win; noverlap=0; k=1; left=0; return; end

r=nol/win;   % overlap ratio

% Compute original fit for desired windows
k = fix((n-r*win)/(win-r*win));       % Number of windows
left = rem((n-r*win),(win-r*win));    % Remaining points not included
i1=win;
i2=nol;
i3=k;
i4=left;


if adjtype==1

%*** Bigger overlap
nol1 = ceil(((k+1)*win-n)/(k));
k1   = fix((n-nol1)/(win-nol1));
left1= rem((n-nol1),(win-nol1));
r1=nol1/win;

%*** Smaller overlap
nol2 = ceil((k*win-n)/(k-1));
k2   = fix((n-nol2)/(win-nol2));
left2= rem((n-nol2),(win-nol2));
r2=nol2/win;

%*** Select overlap ratio that is closer to desired
d1=abs(r-r1);
d2=abs(r-r2);
if (d1<d2)
   window=win;
   noverlap=nol1;
   k=k1;
   left=left1;
else
   window=win;
   noverlap=nol2;
   k=k2;
   left=left2;
end
end;

if adjtype==2

%*** Smaller window
win1 = fix(n/(r*(-k)+k+1));  % Smaller window and overlap to include all points
w1 = ceil(r*win1);
k1 = fix((n-w1)/(win1-w1));
left1 = rem((n-w1),(win1-w1));

%*** Bigger window
win2 = fix(n/(r*(1-k)+k));
w2 = ceil(r*win2);
k2 = fix((n-w2)/(win2-w2));
left2 = rem((n-w2),(win2-w2));

%*** Select window that is closer to desired size
d1=abs(win-win1);
d2=abs(win-win2);
if (d1<d2)
   window=win1;
   noverlap=w1;
   k=k1;
   left=left1;
else
   window=win2;
   noverlap=w2;
   k=k2;
   left=left2;
end

end

% not yet implemented
if adjtype==3
% disp('Not yet implemented.')
end


if dispyes
            disp('Spectral analysis windowing parameters:')
    disp(sprintf('   x        = (length %g)',n))
            disp('Desired parameters:')
    disp(sprintf('   window   = %g',i1))
    disp(sprintf('   noverlap = %g',i2))
    disp(sprintf('   k        = %g',i3))
    disp(sprintf('   left     = %g',i4))
            disp('Best fit window to include all points:')
    disp(sprintf('   window   = %g',window))
    disp(sprintf('   noverlap = %g',noverlap))
    disp(sprintf('   k        = %g',k))
    disp(sprintf('   left     = %g',left))
end

