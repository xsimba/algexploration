% emarkrsp.m   mark events with circles

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
n=find(iont>=s1 & iont<=s2);
if ~isempty(n)
for j=n(1):n(length(n))
    plot(iont(j)/scalefact,var1(iont(j)),'oc')
end;
end;
n=find(maxt>=s1 & maxt<=s2);
if ~isempty(n)
for j=n(1):n(length(n))
    plot(maxt(j)/scalefact,var1(maxt(j)),'or')
end;
end;
n=find(eont>=s1 & eont<=s2);
if ~isempty(n)
for j=n(1):n(length(n))
    plot(eont(j)/scalefact,var1(eont(j)),'oy')
end;
end;
n=find(mint>=s1 & mint<=s2);
if ~isempty(n)
for j=n(1):n(length(n))
    plot(mint(j)/scalefact,var1(mint(j)),'or')
end;
end;




