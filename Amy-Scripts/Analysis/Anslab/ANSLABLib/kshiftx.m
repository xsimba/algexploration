% kshiftx.m   shift var1 in x-direction (left or right): X

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

disp(' ');
disp([' Var1 is now x-shifted ',num2str(shiftx/scalefact)]);
i=input('Shift var1 additionally how far (999=original) ==>  ');
if isempty(i) i=999; end;
if i==999 shiftx=0;
else shiftx=shiftx+i*scalefact; end;
