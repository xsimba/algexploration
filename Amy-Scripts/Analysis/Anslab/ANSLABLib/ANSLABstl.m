%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 
clear

% Settings
notchfreq = 60;   %  (60) Line AC noise notch filter frequency in Hz
examyes=0;        %  (0) Plot raw EMG and event marker data first in EXAM
toffset=25;       % (25) (50 in SP1) time delay [ms] of marker onset to startle tone onset
tref=300;         %(400)(200 for SP1,#1) start of 100 ms startle response window
yhigh=0;          %  (0) default startle response upper limit (set to zero for maximum of entire file)
bt_factor=5;      %  (5) factor for multiplying the standard deviation of the baseline
onsetfactor=6;    %  (6) Sensitivity for startle onset detection

% Initialization
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultTextColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end

if ~exist('data_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
    red_dir=[data_dir,'\l'];
end

red_dir=[data_dir,'\l']; % folder with reduced data
r1ind=2;                 % sequential number in file of EMG
r2ind=1;                 % sequential number in file of startle marker
analprog='findstl6';
expmax=2;


% Start of program
chd(data_dir)
[filename, data_dir] = uigetfile('*.txt','Select data file');
if isequal(data_dir,0);return;end
    
eval(['load ',filename,';']);
l=length(filename);
varname=filename(1:l-4);
subjn=str2num(varname(4:6));
subjnstr = varname(4:6);
studystr=varname(1:3);
filenum=str2num(varname(7:8));
eval(['EC=',varname,';']);
SL=EC(:,r1ind);
MK=EC(:,r2ind);
eval(['clear ',varname]);
len=length(SL)/1000;

%*** resample if necessary
[SL,SR] = AskResampleData(SL,1000,'STL');
[MK] = decfast(MK,SR/1000);

%*** run analysis program
findstl6

%*** Save reduced data
chd(red_dir)
S=[sa0 sl0 so0];
disp(['save ',varname,'.txt -ascii -tabs S']);
eval(['save ',varname,'.txt -ascii -tabs S']);

disp(['save ',subjnstr,twostr(filenum),'l.txt STARTLE_MATRIX -ascii -tabs']);
eval(['save ',subjnstr,twostr(filenum),'l.txt STARTLE_MATRIX -ascii -tabs'])


