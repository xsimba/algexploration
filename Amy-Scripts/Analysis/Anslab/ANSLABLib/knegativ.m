% knegativ.m  mirror a variable at an x-axis parallel line: N

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


[i1,i2,i]=ginput(1);
i=i-48;
if i==1 var1=-var1; title1str=['-',title1str]; end;
if i==2 if secvar2 var2=-var2; title2str=['-',title2str]; end;end;
if i==3 if secvar3 var3=-var3; title3str=['-',title3str]; end;end;
if i==4 if secvar4 var4=-var4; title4str=['-',title4str]; end;end;
