% kaddev.m

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

disp(' ');
m1 = 'Define event 1';
m2 = 'Define event 2';
m3 = 'Define event 3';
m4 = 'Define values to display';
m5 = 'Toggle event 1';
m6 = 'Toggle event 2';
m7 = 'Toggle event 3';
m8 = 'Toggle marker';

i=menue(m1,m2,m3,m4,m5,m6,m7,m8);
if i>0 & i<4
   varstr=['event',int2str(i)];
   disp('Timevector of event in workspace:');
   str=input('==>  ','s'); disp(' ');
   if exist(str)==1
      eval([varstr,'=',str,';']);
      eval(['event',int2str(i),'yes=1;']);
   else disp('This variable does not exist in workspace.'); pause;
   end;
end;
if i==4
  disp('Timevector of event in workspace:');
  str=input('==>  ','s'); disp(' ');
  if exist(str)==1
  eval(['valtime=',str,';']);
  disp('Values of event in workspace:');
  str=input('==>  ','s'); disp(' ');
  if exist(str)==1
     eval(['val=',str,';']);
     eval(['valueyes=1;']);
  else disp('This variable does not exist in workspace.'); pause; end;
else disp('This variable does not exist in workspace.'); pause;
end; end;

if i==5
   if event1yes==1 event1yes=0; else event1yes=1; end
end
if i==6
   if event2yes==1 event2yes=0; else event2yes=1; end
end
if i==7
   if event3yes==1 event3yes=0; else event3yes=1; end
end
if i==8
   if markeryes==1 markeryes=0; else markeryes=1; end
end

z=999;
