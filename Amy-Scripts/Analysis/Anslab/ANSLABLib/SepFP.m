function[File,Path]=SepFilePath(FilePath);
%	SepFilePath.m



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
else
	  eval('filesep = ''\'';');
end


if isempty(FilePath); File=[]; Path=[]; return; end;
Tmp=findstr(FilePath,filesep);
if isempty(Tmp)
	Path=[];
	File=FilePath;
else
	Path=[FilePath(1:Tmp(length(Tmp)))];
	if length(FilePath)>Tmp(length(Tmp))
		File=[FilePath(Tmp(length(Tmp))+1:length(FilePath))];
	else
		File=[];
	end
end
return;
