function [RR,Vmin,Vti,Flow,Ti,Pi,Te,Pe,Tt,TiTt,abd,tho,RibP,Vte,iont,maxt,eont,mint,vtind]=resptime(K11,maxt,mint,iont,eont,samr,betas,K01,K08,vtmin,usemeanvt);
% function [RR,Vmin,Vti,Flow,Ti,Pi,Te,Pe,Tt,TiTt,abd,tho,RibP,Vte,iont,maxt,eont,mint]=resptime(K11,maxt,mint,iont,eont,samr,betas,K01,K08,vtmin);
% Compute variables of respiration channels
% K01: thoracic, K08: abdominal respiration
% Version updated for Paul
% vtind=index of excluded cycles (<vtmin)
% usemeanvt=1: for exclusion of small cycles: use Vtm instead of Vti
% Author: Frank Wilhelm, Stanford University, Tel.(415)-858 3914
% Date: 07/24/94
% Copyright (c) 1994 by Frank Wilhelm
% 9/15/94:  Vmin now is a vector like the other variables


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<11 usemeanvt=0; end;

maxt=maxt(:); mint=mint(:); iont=iont(:); eont=eont(:);
maxv=K11(maxt); minv=K11(mint); ionv=K11(iont); eonv=K11(eont);
maxv=maxv(:); minv=minv(:); ionv=ionv(:); eonv=eonv(:);

Vti   =maxv-ionv(1:length(ionv)-1);                % inspiratory tidal volume
Vte   =eonv-minv;                                  % expiratory tidal volume;
Vtm=(Vti+Vte)/2;

% exclude tidal volumes smaller than vtmin
if usemeanvt
   vtind=find(Vtm<vtmin);
else
   vtind=find(Vti<vtmin);
end;

Vti(vtind)=[];
Vte(vtind)=[];
Vtm(vtind)=[];
iont(vtind)=[];
maxt(vtind)=[];
eont(vtind)=[];
mint(vtind)=[];

Ti   =(maxt-iont(1:length(iont)-1)) ./samr;       % inspiratory time [sec]
Te   =(mint-eont) ./samr;                         % expiratory time
Tt   =diff(iont)  ./samr;                         % total cycle duration
Pe   =(iont(2:length(iont))-mint)   ./samr;       % expiratory pause
Pi   =(eont-maxt) ./samr;                         % inspiratory pause
TiTt =Ti ./Tt;                                    % timing of respiratory control mechanisms []
RR   =60 * (length(Tt)) / sum(Tt);

% exclude missing Tt (Vt is indicator of artifactual breath) from Vmin time
% n1=find(isnan(Vti(1:length(Vti)-1)));
% Vttime=Tt;
% if length(Vttime) Vttime(n1)=[]; end;

%Vminall =(sum(Vti)*60/1000) / sum(Tt);
Vmin =(Vti*60/1000) ./ Tt;
Flow =Vti ./Ti;                                    % mean inspiratory flow [ml/sec]

%** derive abdominal and thoracial volumes and ratios after total volume
if betas(1) & betas(2)    % not equal 0
n1=[]; n2=[]; n3=[]; n4=[];

if 0    %takes phasic delay between tho and abd into account
for i=1:length(maxt)
  n = maxt(i)-5 : maxt(i)+5;
  n(n<1)=[]; n(n>length(K01))=[];  % range check

  n1(i) = max(K01(n));
  n2(i) = max(K08(n));

  n = mint(i)-5 : mint(i)+5;
  n(n<1)=[]; n(n>length(K01))=[];  % range check

  n3(i) = min(K01(n));
  n4(i) = min(K08(n));
end;

else   % takes no phasic delay between tho and abd into account
       % but is faster and more accurate for paradoxical breathing
  n1 = K01(maxt);
  n2 = K08(maxt);
  n3 = K01(iont(1:length(iont)-1));
  n4 = K08(iont(1:length(iont)-1));
end;

tho = n1-n3;   % abdominal tidal volume
abd = n2-n4;   % thoracic  tidal volume

RibP   = tho ./ Vti;          % proportion of rib cage in tidal volume

else     % one beta was zero
  disp('One beta was zero. RibP set to NaN.')
  abd=NaN;
  tho=NaN;
  RibP=NaN;
end;

