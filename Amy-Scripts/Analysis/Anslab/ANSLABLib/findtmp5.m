% Reduction of temperature channel


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


sr=400;
ep=4;
tempunittype = 0;    %  (0) 0 = degree celsius; 1 = degree Fahrenheit

if ~exist('bno')
    bno=1;
    bplotyes=1;
end
if ~bno
    rateyes=0;
    rat=0;
end

if filtyes
    %*** Lowpass filter or moving average for unprocessed channel
    disp('Lowpass filter or moving average for temperature channel');
    TP=filtlow(TP,1,sr,3);  % typically not needed
    %TP=filthann(abs(TP),3);
end;

%*** Resample to allign with other variables (1/4sec epochs)
tmp0=decfast(TP,sr/ep);

if bno|bplotyes

    cfig, cfig
    figure(1)
    t=(1:length(tmp0))/ep;
    plot(t,tmp0);
    if tempunittype
        axis([0 max(t) min([min(tmp0) 60]) max([max(tmp0) 90])]);
        title('Temperature');
        ylabel('degrees Fahrenheit')
    else
        axis([0 max(t) min([min(tmp0) 35.9]) max([max(tmp0) 37.2])]);
        title('Temperature');
        ylabel('degrees Celsius')
    end
    drawnow
end

if bno
    edityes=input('Editing: no [0, default],  yes [1]  ==> ');
    if isempty(edityes)
        edityes=0;
    end

    if edityes
       disp('Edit temperature signal')
       [tmp0,noutind]=outrect(tmp0,t,1);
    end
end % if bno

