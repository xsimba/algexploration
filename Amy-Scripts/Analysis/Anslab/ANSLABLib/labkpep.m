% kpep.m   edit points in ICG: '


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

m1='BLUE:    Ejection onset point';
m2='RED:     Maximum';
m3='GREEN:   X-junction';
%m4='Add cycle (click 3 times)';
m4='Exclude beat';
ed=menu('',m1,m2,m3,m4);

%if ed==4
%   title('Add cycle by clicking on 3 points that define missing beat (order not relevant)');
%   x=ginput(3);
%elseif ed==5

if ed==4
   title('Click near maximum of beat you want to exclude')
   x=ginput(1);
else
   x=ginput(2);
end;

x(:,2)=[];
x=round(x*fs);


if ed==1
[i,mini]=min(abs(Bt-x(1)));    % ******** change var names here
Bt(mini)=x(2);
end;

if ed==2
[i,mini]=min(abs(Zt-x(1)));
Zt(mini)=x(2);
end;

if ed==3
[i,mini]=min(abs(Xt-x(1)));
Xt(mini)=x(2);
end;

if ed==4

if x<1 x=1; end;
if x>length(IC) x=length(IC); end;
[i,maxi]=min(abs(Zt-x));

deleted(maxi)=1;

end;




