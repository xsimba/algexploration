% kninterv.m   new interval to display: n

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

s11=s1/scalefact; s22=s2/scalefact;
disp(' ');
disp(['Interval is now set between ',int2str(s11),' and ',num2str(s22)]);
disp(' New Interval: ');
disp([' (Max. = ',num2str(round(length(var1)/scalefact)),')']);

s1=        input('Start at [0]  ==>  ');
i=int;
int=        input([' + Interval  [',num2str(int),']  ==>  ']);

if isempty(s1) s1=1;
else s1=scalefact*s1;
end;

if isempty(int) s2=s1+i*scalefact;
else s2=s1+int*scalefact;
end;

z=999;
