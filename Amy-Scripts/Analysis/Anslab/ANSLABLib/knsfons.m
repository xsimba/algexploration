% knsfons: find lokal maxima for Skin Conductance:  W

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

%area=input('SIZE of the window that is stepped through the data [SR]= ');
area=24;

%disp('A slope BACK points before max to max has to be greater than RISE:');
%back=input('BACK [SIZE/2] = ');
back=6;

forward=-1;

%rise=input('RISE [50]= ');
rise=.0005;

%mingap=input('MINGAP [SIZE] = ');
mingap=area;

if forward==-1
   [maxt,maxv]=findmax(var1,area,back,rise,mingap);
else
   [maxt,maxv]=findmax(var1,area,back,rise,mingap,forward);
end;


% find between minima

minv=zeros(length(maxt)-1,1);
mint=zeros(length(maxt)-1,1);

for i=1:length(maxt)-1
    [minv(i),mint(i)] = min( var1(maxt(i) : maxt(i+1)));
    mint(i) = mint(i) + maxt(i) -1;
end;

if ~isempty(maxt)
maxt(length(maxt))=[];
maxv(length(maxv))=[];
end;

minfrequ=length(mint)*60*samplerate / length(var1);
                                 % frequency of min per minute (NSF!)
maxmin=maxv-minv;

disp([num2str(length(mint)),' minima and maxima found.']);
disp(['Frequency: ',num2str(minfrequ),' per minute.']);
disp('Mark found maxima with             <m>, [1]  -> "maxt" ');
disp('Mark found minima with             <m>, [2]  -> "mint" ');
disp('Or: Display values of max-min with <m>  [4]  -> "mint", "maxmin" ');

