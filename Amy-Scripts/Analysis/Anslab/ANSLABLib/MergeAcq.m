function MergeAcq(AcqFilePath,TextFilePath,TargetFilePath)

%   MergeAcq
%   function to write biopac file from an analyzed text file containing one channel
%   as column (more columns must be deleted beforehand) and the original acq-file.
%   data can be up-/downsampled if text-file and acq-file have different number
%   ofsamles. Conversion must be supplied in a 'multiplicator/divisor'-pair:
%   to upsample from 25hz to 1000Hz enter multiplicator = 40, divisor = 1!
%   If the resulting length differs from acq-data only by less than 3 samples
%   you can choose to padd the difference with zeros.



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<3;TargetFilePath = [];end
if nargin<2;TextFilePath = [];end
if nargin<1;AcqFilePath = [];end

%   which channels to read in, don't change
ExtractChannel = 'all';

%   which intervall (in samples) to read in, don't change
t1 = 1;
t2 = inf;

%   which source-acq-file
if isempty(AcqFilePath)
    InitPath = ANSLABDefPath(1);
    [ReadFile,ReadPath] = uigetfile([InitPath,'.acq'],'Please choose original *.acq-file:');
    if isequal(ReadFile,0) | isequal(ReadPath,0);return;end
    AcqFilePath = [ReadPath,ReadFile] ;
    ANSLABDefPath(2,ReadPath);
end

%   which exported text-file with the data to add
if isempty(TextFilePath)
    InitPath = ANSLABDefPath(1);
    [ReadFile,ReadPath] = uigetfile([InitPath,'.txt'],'Please choose text-file to add:');
    if isequal(ReadFile,0) | isequal(ReadPath,0);return;end
    TextFilePath = [ReadPath,ReadFile] ;
    ANSLABDefPath(2,ReadPath);
end

%   name target file
if isempty(TargetFilePath)
    InitPath = ANSLABDefPath(1);
    [ReadFile,ReadPath] = uiputfile([InitPath,'.acq'],'Please name merged ACQ-file:');
    if isequal(ReadFile,0) | isequal(ReadPath,0);return;end
    if isempty(findstr(ReadFile,'.acq'));
        ReadFile = [ReadFile,'.acq'];
    end
    TargetFilePath = [ReadPath,ReadFile] ;
    ANSLABDefPath(2,ReadPath);
end


%  read in sourcefile header
[HDR] = ReadBPHeader(AcqFilePath);


%ask for merge settings
if ~isempty(findobj('tag','hMergeAcgFig'));close(findobj('tag','hMergeAcgFig'));end
MergeUtilPath = strrep(which('MergeAcq.m'),'MergeAcq.m','MergeAcqUtil.mat');
hMergeAcgFig = openfig('MergeAcq.fig','new');
gList = guihandles(hMergeAcgFig);
Channels = char(HDR.szCommentText{:});
set(gList.hCopyChan,'String',Channels);
try
    load(MergeUtilPath,'ChanNr','Label','CopyChanVal');
    set(gList.hChanNr,'String',num2str(ChanNr));
    set(gList.hChanLabel,'String',Label);
    set(gList.hCopyChan,'Value',CopyChanVal);
catch
end
waitfor(hMergeAcgFig,'userdata');
Status = get(hMergeAcgFig,'userdata');
if Status==1;close(hMergeAcgFig);return;end
ChanNr = str2num(get(gList.hChanNr,'String'));
Label = get(gList.hChanLabel,'String');
CopyChanVal = get(gList.hCopyChan,'Value');
close(hMergeAcgFig);
try
    save(MergeUtilPath,'ChanNr','Label','CopyChanVal');
    set(gList.hChanNr,'String',num2str(ChanNr));
    set(gList.hLabel,'String',Label);
    set(gList.hCopyChan,'Value',CopyChanVal);
catch
end

CopyChan = deblank(Channels(CopyChanVal,:));
%   command to read in textfile and save merged in WriteFilePath
SavBPCom(AcqFilePath,TextFilePath,TargetFilePath,ChanNr,Label,CopyChan);


return
