% kvarch.m  exchange variables: E
% FW: last update for Paul: 10-23-95
% hold on; plot(var1), plot(var2,'m'),plot(var3,'c'),plot(var4,'r'); hold off


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

disp(' ');
disp(' ');
disp('Which variable shall be exchanged with variable 1 (yellow)');
m1 = 'First copy variable 1 to variable 5';
m2 = 'Variable 2 (magenta)';
m3 = 'Variable 3 (cyan)';
m4 = 'Variable 4 (red)';
m5 = 'Variable 5 (copied variable 1)';

i=menue(m1,m2,m3,m4,m5);

if i>1 & i<6
eval(['str=''var',int2str(i),''';']);
if exist(str)==1
  temp=var1;
  eval(['var1=var',int2str(i),';']);
  eval(['var',int2str(i),'=temp;']);
  clear temp;
  str=title1str;
  eval(['title1str=title',int2str(i),'str;']);
  eval(['title',int2str(i),'str=str;']);
else disp('This variable does not exist in workspace.');
end;

elseif i==1
var5=var1;
end;


