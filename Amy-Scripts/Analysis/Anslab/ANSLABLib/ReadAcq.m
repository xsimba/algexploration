function [HDR,DATA] = ReadAcq(FilePath,ExtractChannel,t1,t2,AllPrintStatus,PrintStatus,PlotStatus,FTellStatus)

%   ReadAcq
%
%   [HDR,Data] = ReadAcq(FilePath,Extract,t1,t2,ExtractChannel,NoS,...
%   StartS,AllPrintStatus,PrintStatus)
%
%   ReadAcq reads in Biopac data and returns header information and data
%   for the channel specified in EXTRACTCHANNEL. 
%   EXTRACTCHANNEL can be a number (e.g. the 3. channel in the file) or
%   one of the following strings:
%
% 		'Zo','Pleth','Accel','CO2wave','RespTho','RespAbd','Temp-L','dz/dt',
%       'ECG Lead I','ECG Lead II','EDA	EMG1','D0' ['D1','D2',...,'D20'],'EMG RMS',
%       'Heart Rate','Marker','none' or 'all'
%
%   If EXTRACTCHANNEL is not given or is set to 'none', only header information
%   is returned. Use 'all' to extract all channels;
%   Data is extracted from sample t1 to sample t2; if not both t1 
%   and t2 are given, the all samples are returned.


%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

if nargin<8;FTellStatus = [];end
if nargin<7;PlotStatus = [];end
if nargin<6;PrintStatus = [];end
if nargin<5;AllPrintStatus = [];end
if nargin<4;t2 = [];end
if nargin<3;t1 = [];end
if nargin<2;ExtractChannel = [];end
if nargin<1;FilePath = [];end

if isempty(FilePath);[FilePath] = ANSLABReadFilePath('.acq','Please choose ACQ-file:');end
if isempty(t1);t1 =1;end
if isempty(t2);t2 =inf;end
if t2<t1;error('Endpoint is greater than or equal to startpoint!');end
if isempty(AllPrintStatus);AllPrintStatus = 1;end
if isempty(PrintStatus);PrintStatus = 1;end
if AllPrintStatus;PrintStatus = 1;end
if isempty(ExtractChannel);ExtractChannel = 'none';end
if isempty(PlotStatus);PlotStatus = 0;end
if isempty(FTellStatus);FTellStatus = 0;end

if nargin<1;PrintStatus = 0;AllPrintStatus = 0;end

ExtChanInd = [];
if ~isnumeric(ExtractChannel)
	switch ExtractChannel
        case 'all'
            MultChanStatus = 1;
        case 'none'
            MultChanStatus = 0;
        otherwise
            MultChanStatus = 0;
	end
else
    if length(ExtractChannel)>1
        MultChanStatus = 1;
        ExtChanInd = ExtractChannel;
    else
        MultChanStatus = 0;
        ExtChanInd = ExtractChannel;
    end
end



disp('start reading biopac file: ');
disp(FilePath);

fid = fopen(FilePath,'rb');
HDR = struct('nItemHeaderLen',[]);
DATA = [];




% 	Item                Type    Size    Offset  Description 
%=============================================================================
%=============================================================================
%=============================================================================


% 	nItemHeaderLen 	    short 	2 	    0 	Not currently used.
HDR.nItemHeaderLen = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nItemHeaderLen = ',num2str(HDR.nItemHeaderLen),' ...\n']);
end


% 	lVersion 	        long 	4 	    2 	File version identifier:
%
% 						30 = Pre-version 2.0
% 						31 = Version 2.0 Beta 1
% 						32 = Version 2.0 release
% 						33 = Version 2.0.7 (Mac)
% 						34 = Version 3.0 In-house Release 1
% 						35 = Version 3.03
% 						36 = version 3.5x (Win 95, 98, NT)
% 						37 = version of BSL/PRO 3.6.x
% 						38 = version of Acq 3.7.0-3.7.2 (Win 98, 98SE, NT, Me, 2000)
% 						39 = version of Acq 3.7.3 or above (Win 98, 98SE, 2000, Me, XP)
% 						41 = version of Acq 3.8.1 or above (Win 98, 98SE, 2000, Me, XP)
% 						42 = version of BSL/PRO 3.7.X or above (Win 98, 98SE, 2000, Me, XP)


Offset = 2;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.lVersion = fread(fid,1,'int32');
if AllPrintStatus
    fprintf(1,['lVersion = ',num2str(HDR.lVersion),' ...\n']);
end

   
% 	lExtItemHeaderLen   long    4 	    6 	Extended item header length.
Offset = 6;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.lExtItemHeaderLen = fread(fid,1,'int32');
if AllPrintStatus
    fprintf(1,['lExtItemHeaderLen = ',num2str(HDR.lExtItemHeaderLen),' ...\n']);
end

% 	nChannels           short   2 	    10 	Number of channels stored.
Offset = 10;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nChannels = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nChannels = ',num2str(HDR.nChannels),' ...\n']);
end

%   nHorizAxisType      short   2       12  Horizontal scale type, one of the  following
% 			0 = Time in seconds
% 			1 = Time in HMS format
% 			2 = Frequency
% 			3 = Arbitrary
Offset = 12;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nHorizAxisType = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nHorizAxisType = ',num2str(HDR.nHorizAxisType),' ...\n']);
end

% 	nCurChannel         short   2 	    14 	Currently selected channel.
Offset = 14;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nCurChannel = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nCurChannel = ',num2str(HDR.nCurChannel),' ...\n']);
end

% 	dSampleTime         double  8 	    16 	The number of milliseconds per sample.
Offset = 16;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.dSampleTime = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dSampleTime = ',num2str(HDR.dSampleTime),' ...\n']);
end

% 	dTimeOffset         double  8 	    24 	The initial time offset in milliseconds.
Offset = 24;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.dTimeOffset = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeOffset = ',num2str(HDR.dTimeOffset),' ...\n']);
end

% 	dTimeScale          double  8 	    32 	The time scale in milliseconds per division.
Offset = 32;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.dTimeScale = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeScale = ',num2str(HDR.dTimeScale),' ...\n']);
end

% 	dTimeCursor1        double  8 	    40 	Cursor 1 time position in milliseconds.
Offset = 40;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.dTimeCursor1 = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeCursor1 = ',num2str(HDR.dTimeCursor1),' ...\n']);
end

% 	dTimeCursor2        double  8 	    48 	Cursor 2 time position in milliseconds.
Offset = 48;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.dTimeCursor2 = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dTimeCursor2 = ',num2str(HDR.dTimeCursor2),' ...\n']);
end

% 	rcWindow            RECT    8 	    56 	The chart's size and position relative to the AcqKnowledge client area. When each 
%                                           RECT field is set to 0,  the chart is displayed with default a size and position.
Offset = 56;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.rcWindow = fread(fid,[1 4],'short');
if AllPrintStatus
    fprintf(1,['rcWindow = ',num2str(HDR.rcWindow),' ...\n']);
end


% 	nMeasurement[6]   	short   6*2 	64 	Describes the currently selected measurements, one of the following:
%
% 							0 = No measurement
% 							1 = Value Absolute voltage
%                           2 = Delta Voltage difference
% 							3 = Peak to peak voltage
% 							4 = Maximum voltage
% 							5 = Minimum voltage
% 							6 = Mean voltage
% 							7 = Standard deviation
% 							8 = Integral
% 							9 = Area
% 							10 = Slope
% 							11 = LinReg
% 							13 = Median
% 							15 = Time
%                           16 = Delta Time
% 							17 = Freq
% 							18 = BPM
% 							19 = Samples
% 							20 = Delta Samples
% 							21 = Time of Median
% 							22 = Time of Max
% 							23 = Time of Min
% 							25 = Calculation
% 							26 = Correlation
Offset = 64;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nMeasurement = fread(fid,[1 6],'short');
if AllPrintStatus
    fprintf(1,['nMeasurement = ',num2str(HDR.nMeasurement),' ...\n']);
end

% 	fHilite             BOOL     2 	 76 	Gray non-selected waveforms:
% 							0 = Don't gray
%                           1 = Gray.
Offset = 76;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.fHilite = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['fHilite = ',num2str(HDR.fHilite),' ...\n']);
end


% 	dFirstTimeOffset    double          	8 	78 	 Initial time offset in milliseconds.
Offset = 78;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.dFirstTimeOffset = fread(fid,1,'double');
if AllPrintStatus
    fprintf(1,['dFirstTimeOffset = ',num2str(HDR.dFirstTimeOffset),' ...\n']);
end

% 	nRescale            short             	2 	86 	Autoscale after transforms:
% 					        0 = Don't autoscale
% 					        1 = Autoscale.
Offset = 86;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nRescale = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nRescale = ',num2str(HDR.nRescale),' ...\n']);
end

% 	szHorizUnits1       char      40 	88 	Horizontal units text.
Offset = 88;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.szHorizUnits1 = fread(fid,[1 40],'char');
if AllPrintStatus
    fprintf(1,['szHorizUnits1 = ',num2str(HDR.szHorizUnits1),' ...\n']);
end

% 	szHorizUnits2       char      10 	128 	Horizontal units text (abbreviated).
Offset = 128;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.szHorizUnits2 = fread(fid,[1 10],'char');
if AllPrintStatus
    fprintf(1,['szHorizUnits2 = ',num2str(HDR.szHorizUnits2),' ...\n']);
end

% 	nInMemory           short             	2 	138 	Keep data file in memory:
% 						0 = Keep
% 						1 = Don't keep.
Offset = 138;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nInMemory = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nInMemory = ',num2str(HDR.nInMemory),' ...\n']);
end

% 	fGrid               BOOL      2 	140 	Enable grid display.
Offset = 140;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.fGrid = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['fGrid = ',num2str(HDR.fGrid),' ...\n']);
end

% 	fMarkers            BOOL      2 	142 	Enable marker display.
Offset = 142;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.fMarkers = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['fMarkers = ',num2str(HDR.fMarkers),' ...\n']);
end

% 	nPlotDraft          short     2 	144 	Enable draft plotting.
Offset = 144;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nPlotDraft = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nPlotDraft = ',num2str(HDR.nPlotDraft),' ...\n']);
end

% 	nDispMode           short     2 	146 	Display mode:
% 					0 = Scope
% 					1 = Chart.
Offset = 146;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nDispMode = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nDispMode = ',num2str(HDR.nDispMode),' ...\n']);
end

% 	nReserved           short     2 	148 	Reserved.
Offset = 148;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nReserved = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nReserved = ',num2str(HDR.nReserved),' ...\n']);
end

if HDR.lVersion >= 34
	%   Version 3.0 and above ...
	% 	Item                    Type       Size     Offset  Description
	% 	BShowToolBar            short      2 	    150 	
	Offset = 150;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.BShowToolBar = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowToolBar = ',num2str(HDR.BShowToolBar),' ...\n']);
	end
	
	% 	BShowChannelButtons     short      2 	    152 	
	Offset = 152;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.BShowChannelButtons = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowChannelButtons = ',num2str(HDR.BShowChannelButtons),' ...\n']);
	end
	
	% 	BShowMeasurements       short      2 	    154 
	Offset = 154;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.BShowMeasurements = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowMeasurements = ',num2str(HDR.BShowMeasurements),' ...\n']);
	end
		
	% 	BShowMarkers            short      2 	    156 
	Offset = 156;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.BShowMarkers = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowMarkers = ',num2str(HDR.BShowMarkers),' ...\n']);
	end
		
	% 	BShowJournal            short      2 	    158 
	Offset = 158;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.BShowJournal = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['BShowJournal = ',num2str(HDR.BShowJournal),' ...\n']);
	end
		
	% 	CurXChannel             short      2 	    160 
	Offset = 160;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.CurXChannel = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['CurXChannel = ',num2str(HDR.CurXChannel),' ...\n']);
	end
		
	% 	MmtPrecision            short      2 	    162 
	Offset = 162;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.MmtPrecision = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['MmtPrecision = ',num2str(HDR.MmtPrecision),' ...\n']);
	end
    
    if HDR.lVersion >=35

		%   Version 3.02 and above ...
		% 	Item                    Type        Size    Offset  Description 
		% 	NMeasurementRows        short 	    2 	    164 	Number of measurement rows
		Offset = 164;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		HDR.NMeasurementRows = fread(fid,1,'short');
		if AllPrintStatus
            fprintf(1,['NMeasurementRows = ',num2str(HDR.NMeasurementRows),' ...\n']);
		end
		
		% 	mmt[40]  	            short 	    2 * 40  166 	Measurement functions
		Offset = 166;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		HDR.mmt = fread(fid,[1 40],'short');
		if AllPrintStatus
            fprintf(1,['mmt = ',num2str(HDR.mmt),' ...\n']);
		end
		
		% 	mmtChan[40]   	        short 	    2 * 40  246 	Measurement channels
		Offset = 246;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		HDR.mmtChan = fread(fid,[1 40],'short');
		if AllPrintStatus
            fprintf(1,['mmtChan = ',num2str(HDR.mmtChan),' ...\n']);
		end

        if HDR.lVersion >= 36
			%   Version 3.5x and above ...
			% 	Item                    Type        Size    Offset  Description
			% 	MmtCalcOpnd1 	        short 	    2 * 40  326 	Measurement, Calculation - Operand 1
			Offset = 326;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.MmtCalcOpnd1 = fread(fid,[1 40],'short');
			if AllPrintStatus
                fprintf(1,['MmtCalcOpnd1 = ',num2str(HDR.MmtCalcOpnd1),' ...\n']);
			end
			
			% 	MmtCalcOpnd2  	        short 	    2 * 40  406 	Measurement, Calculation - Operand 2
			Offset = 406;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.MmtCalcOpnd2 = fread(fid,[1 40],'short');
			if AllPrintStatus
                fprintf(1,['MmtCalcOpnd2 = ',num2str(HDR.MmtCalcOpnd2),' ...\n']);
			end
			
			% 	MmtCalcOp   	        short 	    2 * 40  486 	Measurement, Calculation - Operation
			Offset = 486;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.MmtCalcOp = fread(fid,[1 40],'short');
			if AllPrintStatus
                fprintf(1,['MmtCalcOp = ',num2str(HDR.MmtCalcOp),' ...\n']);
			end
			
			% 	MmtCalcConstant   	    double 	    8 * 40  566 	Measurement, Calculation - Constant
			Offset = 566;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.MmtCalcConstant = fread(fid,[1 40],'double');
			if AllPrintStatus
                fprintf(1,['MmtCalcConstant = ',num2str(HDR.MmtCalcConstant),' ...\n']);
			end
			
			%                          Version 3.7.0 and above ... 
			% 	bNewGridwithMinor 	BOOL       886 	    4 	New Grid with minor line
			Offset = 886;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.bNewGridwithMinor = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bNewGridwithMinor = ',num2str(HDR.bNewGridwithMinor),' ...\n']);
			end
			
			% 	colorMajorGrid 	    long       890 	    4 	COLORREF
			Offset = 890;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.colorMajorGrid = fread(fid,[1 4],'char');
			if AllPrintStatus
                fprintf(1,['colorMajorGrid (ARGB) = ',num2str(HDR.colorMajorGrid),' ...\n']);
			end
			
			% 	colorMinorGrid 	    long       894 	    4 	COLORREF
			Offset = 894;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.colorMinorGrid = fread(fid,[1 4],'char');
			if AllPrintStatus
                fprintf(1,['colorMinorGrid  (ARGB) = ',num2str(HDR.colorMinorGrid),' ...\n']);
			end
			
			% 	wMajorGridStyle     short      898 	    2 	PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT,  PS_DASHDOTDOT
			Offset = 898;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.wMajorGridStyle = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMajorGridStyle = ',num2str(HDR.wMajorGridStyle),' ...\n']);
			end
			
			% 	wMinorGridStyle     short      900 	    2 	PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT
			Offset = 900;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.wMajorGridStyle = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMajorGridStyle = ',num2str(HDR.wMajorGridStyle),' ...\n']);
			end
			
			
			% 	wMajorGridWidth     short      902 	    2 	width of line in Pixels
			Offset = 902;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.wMajorGridWidth = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMajorGridWidth = ',num2str(HDR.wMajorGridWidth),' ...\n']);
			end
			
			% 	wMinorGridWidth     short      904 	    2 	width of line in Pixels
			Offset = 904;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.wMinorGridWidth = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['wMinorGridWidth = ',num2str(HDR.wMinorGridWidth),' ...\n']);
			end
			
			% 	bFixedUnitsDiv      BOOL       906 	    4 	Locked/Unlocked grid lines
			Offset = 906;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.bFixedUnitsDiv = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bFixedUnitsDiv = ',num2str(HDR.bFixedUnitsDiv),' ...\n']);
			end
			
			% 	bMid_Range_Show     BOOL       910 	    4 	show gridlines as MidPoint and Range
			Offset = 910;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.bMid_Range_Show = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bMid_Range_Show = ',num2str(HDR.bMid_Range_Show),' ...\n']);
			end
			
			% 	dStart_Middle_Point double     914 	    8 	Startpoint to draw grid
			Offset = 914;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.dStart_Middle_Point = fread(fid,1,'double');
			if AllPrintStatus
                fprintf(1,['dStart_Middle_Point = ',num2str(HDR.dStart_Middle_Point),' ...\n']);
			end
			
			% 	dOffset_Point       double     922 	    8 * 60 	Offset of VERTICAL value per channel
			Offset = 922;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.dOffset_Point = fread(fid,[1 60],'double');
			if AllPrintStatus
                fprintf(1,['dOffset_Point = ',num2str(HDR.dOffset_Point),' ...\n']);
			end
			
			% 	hGrid               double     1402 	8 	Horizontal grid spacing
			Offset = 1402;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.hGrid = fread(fid,1,'double');
			if AllPrintStatus
                fprintf(1,['hGrid = ',num2str(HDR.hGrid),' ...\n']);
			end
			
			% 	vGrid               double     1410 	8 * 60 	Vertical grid spacing per channel
			Offset = 1410;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.vGrid = fread(fid,[1 60],'double');
			if AllPrintStatus
                fprintf(1,['vGrid = ',num2str(HDR.vGrid),' ...\n']);
			end
			
			
			% 	bEnableWaveTools    BOOL       1890 	4 	Enable Wavetools during acquisition
			Offset = 1890;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.bEnableWaveTools = fread(fid,1,'int32');
			if AllPrintStatus
                fprintf(1,['bEnableWaveTools = ',num2str(HDR.bEnableWaveTools),' ...\n']);
			end

            if HDR.lVersion >= 39
				%  Version 3.7.3 and above ... 
				%   Item               Type       Offset    Size   Description
				%   horizPrecision     short  	  1894 	    2 	    digits of precision for units in Horizontal Axis
				Offset = 1894;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
				HDR.horizPrecision = fread(fid,1,'short');
				if AllPrintStatus
                    fprintf(1,['horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
				end
                
                if HDR.lVersion >= 40
				
					% Version 3.8.1 and above ... 
					% Item                      Type    Offset  Size   Description
					% RESERVED  	            byte 	1896 	20 	    RESERVED
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.RESERVED = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['RESERVED = ',num2str(HDR.RESERVED),' ...\n']);
					end					
					
					% bOverlapMode  	        BOOL 	1916 	4 	    Overlap Mode
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bOverlapMode = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bOverlapMode = ',num2str(HDR.bOverlapMode),' ...\n']);
					end	
					
					% bShowHardware  	        BOOL 	1920 	4 	    Hardware visibility
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bShowHardware = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bShowHardware = ',num2str(HDR.bShowHardware),' ...\n']);
					end	
					
					% bXAutoPlot  	            BOOL 	1924 	4 	    Autoplot during acquisition
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bXAutoPlot = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bXAutoPlot = ',num2str(HDR.bXAutoPlot),' ...\n']);
					end
					
					% bXAutoScroll  	        BOOL 	1928 	4 	    Autoscroll during acquisition
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bXAutoScroll = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bXAutoScroll = ',num2str(HDR.bXAutoScroll),' ...\n']);
					end
					
					% bStartButtonVisible  	    BOOL 	1932 	4 	    Start button visibility
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bStartButtonVisible = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bStartButtonVisible = ',num2str(HDR.bStartButtonVisible),' ...\n']);
					end
					
					% bCompressed  	            BOOL 	1936 	4 	    The file is compressed
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bCompressed = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bCompressed = ',num2str(HDR.bCompressed),' ...\n']);
					end
					
					% bAlwaysStartButtonVisible BOOL 	1940 	4 	    Always show start button
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					HDR.bAlwaysStartButtonVisible = fread(fid,1,'short');
					if AllPrintStatus
                        fprintf(1,['bAlwaysStartButtonVisible = ',num2str(HDR.bAlwaysStartButtonVisible),' ...\n']);
					end
                end                
            end
        end
    end
end



StartPerChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['StartPerChannelData = %g ...\n'],StartPerChannelData);
end
CumPerChannel = 0;
MarkerChan = [];
TriggerChan = [];
if PrintStatus
    disp('reading header data for channels...');
end
% 	Per Channel Data Section...
for ChanInd = 1: HDR.nChannels
    
    if  AllPrintStatus
            fprintf(1,'\n');
            fprintf(1,['reading header data for channel ',num2str(ChanInd),' of ',num2str(HDR.nChannels),' total channels...\n']);
            fprintf(1,['--------------------------------------------------------------------------\n']);
    end
    
	% 	Item               Type       Offset    Size    Description 
	% 	lChanHeaderLen     long       0 	    4 	    Length of channel header.
    Offset = 0 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.lChanHeaderLen{ChanInd} = fread(fid,1,'long');
	if AllPrintStatus
        fprintf(1,['\tlChanHeaderLen{',num2str(ChanInd),'} = ',num2str(HDR.lChanHeaderLen{ChanInd}),' ...\n']);
	end	
	
	% 	nNum               short      4 	    2 	 Channel number.
    Offset = 4 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.nNum{ChanInd} = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnNum{',num2str(ChanInd),'} = ',num2str(HDR.nNum{ChanInd}),' ...\n']);
    end	
    
    
	% 	szCommentText      char       6 	    40 	Comment text.
    Offset = 6 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.szCommentText{ChanInd} = fread(fid,[1 40],'char');
	if AllPrintStatus
        fprintf(1,['\tszCommentText{',num2str(ChanInd),'} = ']);
        fprintf(1,'%c',char(HDR.szCommentText{ChanInd}));
        fprintf(1,'\n');
    end	
    if ~isnumeric(ExtractChannel) & ~strcmp(ExtractChannel,'all') & ~strcmp(ExtractChannel,'none')
        if strcmp(deblank(char(HDR.szCommentText{ChanInd})),ExtractChannel)
            if isempty(ExtChanInd);
                ExtChanInd = ChanInd;
            else
                ExtChanInd = [ExtChanInd ChanInd];
            end
        end
    elseif strcmp(ExtractChannel,'all')
        if isempty(ExtChanInd);
            ExtChanInd = ChanInd;
        else
            ExtChanInd = [ExtChanInd ChanInd];
        end
    end
        
        
    if ~isempty(findstr(HDR.szCommentText{ChanInd},'Marker'))
        MarkerChan = ChanInd;
    end
    if ~isempty(findstr(HDR.szCommentText{ChanInd},'Trigger'))
        TriggerChan = ChanInd;
    end
    
    
	% 	rgbColor           RGB        46 	    4 	 Color.
    Offset = 46 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.rgbColor{ChanInd} = fread(fid,[1 4],'char');
	if AllPrintStatus
        fprintf(1,['\trgbColor{',num2str(ChanInd),'} (ARGB) = ',num2str(HDR.rgbColor{ChanInd}),' ...\n']);
	end	
        
    
	% 	nDispChan          short      50 	    2 	Display option.
    Offset = 50 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.nDispChan{ChanInd} = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnDispChan{',num2str(ChanInd),'} = ',num2str(HDR.nDispChan{ChanInd}),' ...\n']);
	end	
        
    
	% 	dVoltOffset        double     52 	    8 	Amplitude offset (volts).
    Offset = 52 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.dVoltOffset{ChanInd} = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdVoltOffset{',num2str(ChanInd),'} = ',num2str(HDR.dVoltOffset{ChanInd}),' ...\n']);
	end	
        
    
	% 	dVoltScale         double     60 	    8 	Amplitude scale (volts/div).
    Offset = 60 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.dVoltScale{ChanInd} = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdVoltScale{',num2str(ChanInd),'} = ',num2str(HDR.dVoltScale{ChanInd}),' ...\n']);
	end	    
    
	% 	szUnitsText        char       68 	    20 	Units text.
    Offset = 68 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.szUnitsText{ChanInd} = fread(fid,[1 20],'char');
	if AllPrintStatus
        fprintf(1,['\tszUnitsText{',num2str(ChanInd),'} = ',HDR.szUnitsText{ChanInd}]);
        fprintf(1,'\n');
	end	    
    
	% 	lBufLength         long       88 	    4 	Number of data samples.
    Offset = 88 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.lBufLength{ChanInd} = fread(fid,1,'int32');
	if AllPrintStatus
        fprintf(1,['\tlBufLength{',num2str(ChanInd),'} = ',num2str(HDR.lBufLength{ChanInd}),' ...\n']);
	end	    
    
	% 	dAmplScale         double     92 	    8 	Units/count.
    Offset = 92 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.dAmplScale{ChanInd} = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdAmplScale{',num2str(ChanInd),'} = ',num2str(HDR.dAmplScale{ChanInd}),' ...\n']);
	end	    
    
	% 	dAmplOffset        double     100 	    8 	Units
    Offset = 100 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.dAmplOffset{ChanInd} = fread(fid,1,'double');
	if AllPrintStatus
        fprintf(1,['\tdAmplOffset{',num2str(ChanInd),'} = ',num2str(HDR.dAmplOffset{ChanInd}),' ...\n']);
	end	    
    
	% 	nChanOrder         short      108 	    2 	Displayed channel order.
    Offset = 108 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.nChanOrder{ChanInd} = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnChanOrder{',num2str(ChanInd),'} = ',num2str(HDR.nChanOrder{ChanInd}),' ...\n']);
    end	    
    
	% 	nDispSize          short      110 	    2 	Channel partition size.
    Offset = 110 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	HDR.nDispSize{ChanInd} = fread(fid,1,'short');
	if AllPrintStatus
        fprintf(1,['\tnDispSize{',num2str(ChanInd),'} = ',num2str(HDR.nDispSize{ChanInd}),' ...\n']);
	end	
    StepSize = 112;
    
    if HDR.lVersion >= 34
        %   Version 3.0 and above ...
		% 	Item              Type       Offset     Size  Description 
		% 	plotMode          short      112 	    2 
        Offset = 112 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		HDR.plotMode{ChanInd} = fread(fid,1,'short');
		if AllPrintStatus
            fprintf(1,['\tplotMode{',num2str(ChanInd),'} = ',num2str(HDR.plotMode{ChanInd}),' ...\n']);
		end	    
        
		% 	vMid              double     114 	    8 	 
        Offset = 114 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		HDR.vMid{ChanInd} = fread(fid,1,'double');
		if AllPrintStatus
            fprintf(1,['\tnvMid{',num2str(ChanInd),'} = ',num2str(HDR.vMid{ChanInd}),' ...\n']);
		end	    
        StepSize = 122;
      
        
        if HDR.lVersion >= 38
	
			%   Version 3.7.0 and above ...
			% 	Item                Type      Offset    Size    Description 
			% 	szDescription       char      122 	    128 	String of Channel description
            Offset = 122 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.szDescription{ChanInd} = fread(fid,[1 128],'char');
			if AllPrintStatus
                fprintf(1,['\tszDescription{',num2str(ChanInd),'} = ']);
                fprintf(1,'%c',char(HDR.szDescription{ChanInd}));
                fprintf(1,'\n');
			end	    
            
			% 	nVarSampleDivider   short     250 	    2 	Channel divider of main frequency
            Offset = 250 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			HDR.nVarSampleDivider{ChanInd} = fread(fid,1,'short');
			if AllPrintStatus
                fprintf(1,['\tnVarSampleDivider{',num2str(ChanInd),'} = ',num2str(HDR.nVarSampleDivider{ChanInd}),' ...\n']);
            end	
            
            StepSize = 252;
            
            
            if HDR.lVersion >= 39
                %     tem              Type      Offset     Size    Description
                %   vertPrecision 	   short  	  252 	    2 	    digits of precision for units in Vertical Axis for each channel
                Offset = 252 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
                HDR.vertPrecision{ChanInd} = fread(fid,1,'short');
                if AllPrintStatus
                    fprintf(1,['\tvertPrecision{',num2str(ChanInd),'} = ',num2str(HDR.vertPrecision{ChanInd}),' ...\n']);
                end	
                StepSize = 254;
            end
        end
    end
    
    CumPerChannel = CumPerChannel + StepSize;
	
end



%EventStatus
EventStatus = [];
if isempty(MarkerChan) & isempty(TriggerChan)
    if PrintStatus
        disp('No event channel found...');
    end
    EventStatus = 0;
elseif ~isempty(MarkerChan) & isempty(TriggerChan)
    if PrintStatus
        disp('Using marker channel for event information ...');
    end
    EventStatus = 2;
elseif isempty(MarkerChan) & ~isempty(TriggerChan)
    if PrintStatus
        disp('Using trigger channel for event information ...');
    end
    EventStatus = 1;
elseif ~isempty(MarkerChan) & ~isempty(TriggerChan)
    if PrintStatus
        disp('Marker and Trigger channel found! Using trigger channel for event information ...');
    end
    EventStatus = 3;
end
    
    

StartForeignData = ftell(fid);
if FTellStatus
    fprintf(1,['StartForeignData = %g ...\n'],StartForeignData);
end;
if PrintStatus
    fprintf(1,'Start reading foreign data section...\n');
end


% 	Foreign Data Section...
% 	Item               Type       Size      Offset  Description 
% 	nLength            short      2 	    0 	    Total length of foreign data packet.
Offset = 0 + StartForeignData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nLength = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nLength = ',num2str(HDR.nLength),' ...\n']);
end	


% 	nID                short      2 	    2 	ID of foreign data.
Offset = 2 + StartForeignData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.nID = fread(fid,1,'short');
if AllPrintStatus
    fprintf(1,['nID = ',num2str(HDR.nID),' ...\n']);
end	

% 	ByForeignData      BYTE       nLength   4 	Foreign data.
Offset = 4 + StartForeignData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
HDR.ByForeignData = fread(fid,HDR.nLength-4,'char');
if AllPrintStatus
    if HDR.nLength>100
        fprintf(1,['ByForeignData too large to display ...\n']);
    else
        fprintf(1,['ByForeignData = ',num2str(HDR.ByForeignData),' ...\n']);
    end
end	

	
StartPerChannelDataTypes = ftell(fid);
if FTellStatus
    fprintf(1,['StartPerChannelDataTypes = %g ...\n'],StartPerChannelDataTypes);
end;

CumChanSum = 0;
MarkerVal = [];
if AllPrintStatus
    fprintf(1,'Start reading per channel data types section...\n');
end

ChannelByteSizeMat = [];
% 	Per Channel Data Types Section...	
for ChanInd = 1: HDR.nChannels
       
    % 	This block is repeated for as many channels that were detected in the graph header packet nChannels field.
    % 	Item               Type       Size      Offset          	Description 
    % 	nSize              short      2 	    0 	                Channel data size in bytes.
    Offset = 0 + CumChanSum + StartPerChannelDataTypes;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.nSize{ChanInd} = fread(fid,1,'short'); 
    if AllPrintStatus
        fprintf(1,['nSize{',num2str(ChanInd),'} = ',num2str(HDR.nSize{ChanInd}),' ...\n']);
    end	

    % 	nType              short      2 	    2 	Channel data type:
    % 								1 = double
    % 								2 = int
    Offset = 2 + CumChanSum + StartPerChannelDataTypes;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.nType{ChanInd} = fread(fid,1,'short');
    if HDR.nType{ChanInd}==1
        if AllPrintStatus
            fprintf(1,['nType{',num2str(ChanInd),'} = double ...\n']);
        end
    elseif HDR.nType{ChanInd}==2
        if AllPrintStatus
            fprintf(1,['nType{',num2str(ChanInd),'} = integer ...\n']);
        end
    end
    CumChanSum = CumChanSum + 4;
end


% 32310
ChannelByteSizeMat = vertcat(HDR.nSize{:});
StartChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['StartChannelData = %g ...\n'],StartChannelData);
end;

if AllPrintStatus
    fprintf(1,'Start reading channel data ...\n');
end

% 	Channel Data Section...
% 	The individual channel data is stored after the Per Channel Data Types Section. The channel data is in an interleaved format.


%check for number of points in channels...
if AllPrintStatus
    fprintf(1,['number of channels in file: ',num2str(HDR.nChannels),'\n']);
end
%check for equal number of samples
InitNSample = HDR.lBufLength{1};
Equal = 1;
if AllPrintStatus
    fprintf(1,['number of samples and in channels:\t sampling divider:\n']);  
end
for ChanInd = 1:HDR.nChannels
    if  HDR.lBufLength{ChanInd} ~= InitNSample
        Equal = 0;
    end
    if AllPrintStatus
        fprintf(1,['channel ',num2str(ChanInd),': ',num2str([HDR.lBufLength{ChanInd}]','%20.0f'),' samples']);
        fprintf(1,[': \t\t',num2str([HDR.nVarSampleDivider{ChanInd}]','%20.0f')]);
        fprintf(1,'\n'); 
	end
end  
if AllPrintStatus
	if Equal
        fprintf(1,'Number of samples are equal in all channels...\n');
	end
end




%case all equal sampling rates
if ~isempty(ExtractChannel) & ~strcmp(ExtractChannel,'none');
    if Equal


        %single channel
        if ~MultChanStatus

            NVecChan = 1;
            ChannelInitOffset = sum(ChannelByteSizeMat(1:ExtChanInd-1));
            ChannelSkipVal = sum(ChannelByteSizeMat([1:ExtChanInd-1,ExtChanInd+1:end]));
            AllChannelSkipVal = sum(ChannelByteSizeMat);

            %all samples
            if t1 == 1 & t2 == inf
                status = fseek(fid, ChannelInitOffset, 'cof');
                if status; error('fseek error!'); end

                NSample = InitNSample;
                DATA = zeros(1,NSample);
                if PrintStatus
                   fprintf(1,['reading data from channel ',num2str(ExtChanInd),' (']);
                   fprintf(1,'%c',char(HDR.szCommentText{ExtChanInd}));
                   fprintf(1,') ....\n');
                end
                TmpData = [];Count = 0;
                if HDR.nType{ExtChanInd} == 1 %double
                    [TmpData, Count] = fread(fid,[1 NSample],'double',ChannelSkipVal);
                    TmpData = TmpData * HDR.dAmplScale{ExtChanInd} - HDR.dAmplOffset{ExtChanInd};                    
                elseif HDR.nType{ExtChanInd} == 2 %int                    
                    [TmpData, Count] = fread(fid,[1 NSample],'short',ChannelSkipVal);
                    TmpData = TmpData * HDR.dAmplScale{ExtChanInd} - HDR.dAmplOffset{ExtChanInd};
                else
                    fclose(fid);
                    error(['Invalid data format for channel ',num2str(ChanInd),' ... ']);
                end
                if Count~=NSample
                    if feof(fid)
                        warning('Unexpected end of file...');
                    end
                else
                    fprintf(1,[num2str(NSample),' samples read...\n']);
                end

                DATA(1,:) = TmpData;

            %specified segment
            else

                NPointsToRead = t2 - t1 + 1;

                status = fseek(fid, ChannelInitOffset, 'cof');
                if status; error('fseek error!'); end
                status = fseek(fid, AllChannelSkipVal * (t1 - 1),'cof');
                if status; error('fseek error!'); end

                NSample = InitNSample;
                DATA = zeros(1,NPointsToRead);
                if PrintStatus
                   fprintf(1,['reading data from channel ',num2str(ExtChanInd),' (']);
                   fprintf(1,'%c',char(HDR.szCommentText{ExtChanInd}));
                   fprintf(1,') ....\n');
                end
                TmpData = [];Count = 0;
                if HDR.nType{ExtChanInd} == 1 %double
                    [TmpData, Count] = fread(fid,[1 NSample],'double',ChannelSkipVal);
                    TmpData = TmpData * HDR.dAmplScale{ExtChanInd} - HDR.dAmplOffset{ExtChanInd};
                elseif HDR.nType{ExtChanInd} == 2 %int
                    [TmpData, Count] = fread(fid,[1 NPointsToRead],'short',ChannelSkipVal);
                    TmpData = TmpData * HDR.dAmplScale{ExtChanInd} - HDR.dAmplOffset{ExtChanInd};
                else
                    fclose(fid);
                    error(['Invalid data format for channel ',num2str(ChanInd),' ... ']);
                end
                if Count~=NPointsToRead
                    if feof(fid)
                        warning('Unexpected end of file...');
                    else
                        warning('Less points read than requested! Reason unknown!');
                    end
                else
                    fprintf(1,[num2str(NPointsToRead),' samples read, starting with sample ',num2str(t1),' ...\n']);
                end
                %calibrate
                DATA(1,:) = TmpData;
            end

        %several channels
        else

            %all samples
            if t1 == 1 & t2 == inf
                ChanVec = ExtChanInd;
                NVecChan = length(ExtChanInd);
                NSample = InitNSample;
                DATA = zeros(NVecChan,NSample);
                TypeCell = {'double','integer','other'};
                for VecChanInd = 1: NVecChan

                    fseek(fid,StartChannelData,'bof');
                    ExtChanInd = ChanVec(VecChanInd);
                    ChannelInitOffset = sum(ChannelByteSizeMat(1:ExtChanInd-1));
                    ChannelSkipVal = sum(ChannelByteSizeMat([1:ExtChanInd-1,ExtChanInd+1:end]));
                    status = fseek(fid, ChannelInitOffset, 'cof');
                    if PrintStatus
                        fprintf(1,['Reading channel # ',num2str(ExtChanInd),' (']);
                        fprintf(1,'%c',HDR.szCommentText{ExtChanInd});
                        fprintf(1,[') of type ',TypeCell{HDR.nType{ExtChanInd}},' with initial offset of ',num2str(ChannelInitOffset),', a skipval of ',num2str(ChannelSkipVal),' ...\n']);
                    end
                    if status; error('fseek error!'); end

                    if PrintStatus
                       fprintf(1,['reading data from channel ',num2str(ExtChanInd),' (']);
                       fprintf(1,'%c',char(HDR.szCommentText{ExtChanInd}));
                       fprintf(1,') ....\n');
                    end
                    TmpData = [];Count = 0;
                    if HDR.nType{ExtChanInd} == 1 %double
                        [TmpData, Count] = fread(fid,[1 NSample],'double',ChannelSkipVal);
                        TmpData = TmpData * HDR.dAmplScale{VecChanInd} + HDR.dAmplOffset{VecChanInd};
                    elseif HDR.nType{ExtChanInd} == 2 %int
                        [TmpData, Count] = fread(fid,[1 NSample],'short',ChannelSkipVal);
                        TmpData = TmpData * HDR.dAmplScale{VecChanInd} + HDR.dAmplOffset{VecChanInd};
                    else
                        fclose(fid);
                        error(['Invalid data format for channel ',num2str(ChanInd),' ... ']);
                    end
                    if Count~=NSample
                        if feof(fid)
                            warning('Unexpected end of file...');
                        end
                    else
                        if PrintStatus
                            fprintf(1,[num2str(NSample),' samples read...\n']);
                        end
                    end
                    DATA(VecChanInd,:) = TmpData; 
                end

            %specified segment   
            else

                NPointsToRead = t2 - t1 + 1;
                NSample = InitNSample;
                ChanVec = ExtChanInd;
                NVecChan = length(ExtChanInd);
                DATA = zeros(NVecChan,NPointsToRead);
                TypeCell = {'double','integer','other'};
                AllChannelSkipVal = sum(ChannelByteSizeMat);

                for VecChanInd = 1: NVecChan

                    fseek(fid,StartChannelData,'bof');
                    ExtChanInd = ChanVec(VecChanInd);
                    ChannelInitOffset = sum(ChannelByteSizeMat(1:ExtChanInd-1));
                    ChannelSkipVal = sum(ChannelByteSizeMat([1:ExtChanInd-1,ExtChanInd+1:end]));

                    %channel offset
                    status = fseek(fid, ChannelInitOffset, 'cof');
                    if status; error('fseek error!'); end

                    %segment offset
                    status = fseek(fid, AllChannelSkipVal * (t1 - 1),'cof');
                    if status; error('fseek error!'); end


                    if PrintStatus
                        fprintf(1,['Reading channel # ',num2str(ExtChanInd),' (']);
                        fprintf(1,'%c',HDR.szCommentText{ExtChanInd});
                        fprintf(1,[') of type ',TypeCell{HDR.nType{ExtChanInd}},' with initial offset of ',num2str(ChannelInitOffset),', a skipval of ',num2str(ChannelSkipVal),' ...\n']);
                        fprintf(1,['Reading sample # ',num2str(t2),' to sample # ',num2str(t2),'...\n']);
                    end

                    TmpData = [];Count = 0;
                    if HDR.nType{ExtChanInd} == 1 %double
                        [TmpData, Count] = fread(fid,[1 NPointsToRead],'double',ChannelSkipVal);
                        TmpData = TmpData * HDR.dAmplScale{VecChanInd} + HDR.dAmplOffset{VecChanInd};
                    elseif HDR.nType{ExtChanInd} == 2 %int
                        [TmpData, Count] = fread(fid,[1 NPointsToRead],'short',ChannelSkipVal);
                        TmpData = TmpData * HDR.dAmplScale{VecChanInd} + HDR.dAmplOffset{VecChanInd};
                    else
                        fclose(fid);
                        error(['Invalid data format for channel ',num2str(ChanInd),' ... ']);
                    end
                    if Count~=NSample
                        if feof(fid)
                            warning('Unexpected end of file...');
                        end
                    else
                        if PrintStatus
                            fprintf(1,[num2str(NPointsToRead),' samples read...\n']);
                        end
                    end
                    DATA(VecChanInd,:) = TmpData; 
                end
            end
        end  
    else
        fclose(fid);
        error('Unequal samling rates not supported yet!');
    end
else
    NSample = InitNSample;
end

               
                
EndChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['EndChannelData = %g ...\n'],EndChannelData);
end 

[status] = fseek(fid,StartChannelData,'bof');
if status;disp(ferror(fid));fclose(fid);return;end
TotalDataOffset = NSample*sum(ChannelByteSizeMat(:));
[status] = fseek(fid,TotalDataOffset,'cof');
if status;disp(ferror(fid));fclose(fid);return;end

%Markers Header Section...
%Item               Type          Size    Offset          	Description 
% lLength            long           4 	0 	Total length of all markers.
HDRftell = ftell(fid);
if FTellStatus
    fprintf(1,['BeginMarkerSection = %g ...\n'],HDRftell);
end
HDR.lLength = fread(fid,1,'int32'); 
if AllPrintStatus
    fprintf(1,['lLength = ',num2str(HDR.lLength),' ...\n']);
end	

% lMarkers           long           4 	4 	Number of markers.  
HDR.lMarkers = fread(fid,1,'int32');  
if AllPrintStatus
    fprintf(1,['lMarkers = ',num2str(HDR.lMarkers),' ...\n']);
end	

MarkerItemStart = ftell(fid);
if FTellStatus
    fprintf(1,['BeginMarkerData = %g ...\n'],MarkerItemStart);
end
CumMarkSum = 0;
for MarkerInd = 1:HDR.lMarkers    
    %Marker Item Section...
    %Item               Type          Size    Offset          	Description 
    % lSample            long            4 	0 	Location of marker. 
    Offset = 0 + MarkerItemStart + CumMarkSum ;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.lSample{MarkerInd} = fread(fid,1,'int32');  
    if AllPrintStatus
        fprintf(1,['lSample{',num2str(MarkerInd),'} = ',num2str(HDR.lSample{MarkerInd}),' ...\n']);
    end	 

    % fSelected          BOOL          	2 	4 	Select this marker. 
    Offset = 4 + MarkerItemStart + CumMarkSum ;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.fSelected{MarkerInd} = fread(fid,1,'short');   
    if AllPrintStatus
        fprintf(1,['fSelected{',num2str(MarkerInd),'} = ',num2str(HDR.fSelected{MarkerInd}),' ...\n']);
    end	

    % fTextLocked        BOOL          	2 	6 	Lock this text.
    Offset = 6 + MarkerItemStart + CumMarkSum ;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.fTextLocked{MarkerInd} = fread(fid,1,'short');   
    if AllPrintStatus
        fprintf(1,['fTextLocked{',num2str(MarkerInd),'} = ',num2str(HDR.fTextLocked{MarkerInd}),' ...\n']);
    end	

    % fPositionLocked    BOOL          	2 	8 	Lock this location.
    Offset = 8 + MarkerItemStart + CumMarkSum ;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.fPositionLocked{MarkerInd} = fread(fid,1,'short');   
    if AllPrintStatus
        fprintf(1,['fPositionLocked{',num2str(MarkerInd),'} = ',num2str(HDR.fPositionLocked{MarkerInd}),' ...\n']);
    end	

    % nTextLength        short            	2 	10 	Length of marker text (including NULL).
    Offset = 10 + MarkerItemStart + CumMarkSum ;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.nTextLength{MarkerInd} = fread(fid,1,'short');  
    if AllPrintStatus
        fprintf(1,['nTextLength{',num2str(MarkerInd),'} = ',num2str(HDR.nTextLength{MarkerInd}),' ...\n']);
    end	 

    % szText             char              	nTextLength  12 	Marker text string.
    Offset = 12 + MarkerItemStart + CumMarkSum ;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    HDR.szText{MarkerInd} = fread(fid,[1 HDR.nTextLength{MarkerInd}],'char');   
    if AllPrintStatus
        fprintf(1,['szText{',num2str(MarkerInd),'} = ',num2str(HDR.szText{MarkerInd}),' ...\n']);
    end	
    OffSetSum = 12 + HDR.nTextLength{MarkerInd};
    
    CumMarkSum = CumMarkSum + OffSetSum;
end

disp('end reading biopac file...');
fclose(fid);

if PlotStatus
	AxesPosVec = zeros(NVecChan,4);
	CameraUpVector = zeros(NVecChan,3);
	CameraTarget = zeros(NVecChan,3);
    figure('units','normal','position',[0.3 0.02 0.5 0.9]);
    if MultChanStatus
        subplot(NVecChan,1,1);
        for ChanInd = 1:NVecChan
            h = subplot(NVecChan,1,ChanInd);
            AxesPosVec(ChanInd,:)=get(h,'position');
            CameraUpVector(ChanInd,:) = get(h,'CameraUpVector');
            CameraTarget(ChanInd,:) = get(h,'CameraTarget');
        end
    end
    Counter = 1;
    if t1==1 & t2==inf
        for ChanInd = 1:NVecChan
            h = subplot(NVecChan,1,ChanInd);
            axes(h);
            cla(h);
            hold on;
            plot(DATA(ChanInd,1:end));
        end
    else
        ZoomVal = min(1000,t2-t1);
        if (Counter)*ZoomVal+1<=size(DATA,2)
            for ChanInd = 1:NVecChan
                h = subplot(NVecChan,1,ChanInd);
                axes(h);
                cla(h);
                hold on;
                plot(DATA(ChanInd,(Counter-1)*ZoomVal+1:(Counter)*ZoomVal+1));
            end
        end
    end
end
        

return



















%

