% kreset.m   edit resets of respiration (breath holding study): )


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

disp(' ');
disp('	To define new triangle of signal:');
disp('	Click on left end to anchor new signal to old,');
disp('	then click on new value, then anchor at other end.');
disp('	For linear interpolation: anchor left, click again, anchor right.');
disp('  Press <c> twice to activate <cut floor> option,');
disp('  then click with mouse on interval where floor needs to be cut off.');
disp('  The y-axis value of the first click defines the y-value of the new line.');
disp('	These procedures can be done repeatedly.');
disp('	Then optional: call filter menu with <f> to smooth edges.');
disp('	Quit by pressing <space> twice or click twice in lower right corner.');

vi=1;
%if exist('ptt0')
%vi=input('Edit which variable [1] ==> ');
%if isempty(vi) vi=1; end;
%end;

eval(['yr=y',int2str(vi),';']);
i=max(nanrem(yr))-min(nanrem(yr));
figure(3)
yr=drawlin2(length(yr),min(nanrem(yr))-i*.2,max(nanrem(yr))+i*.2,yr);
figure(1)
eval(['var',int2str(vi),'(s1:s2)=yr;']);

% exchange edited var1 with original variable in BP value editing of paul
if exist('bppaul')
  eval(['var',int2str(bppaul),'=var1;']);
end;




