function [] = SaveBP(FilePath,HDR,DATA,t1,t2,ExtractChannel,AllPrintStatus,PrintStatus,AskOverWrite)

%   SaveBP
%
%   SaveBP(FilePath,HDR,DATA,t1,t2,ExtractChannel,AllPrintStatus,PrintStatus,AskOverWrite)
%
%   SaveBP save DATA in Biopac format using information in HDR.
%   Use a HDR loaded from an original BP file. If t1 and t2 are
%   given, data is written from sample t1 to sample t2;
%   If you wish to only save specific channels, pass the EXTRACTCHANNEL
%   number, as one of the following strings
%
% 		'Zo','Pleth','Accel','CO2wave','RespTho','RespAbd','Temp-L','dz/dt',
%       'ECG Lead I','ECG Lead II','EDA	EMG1','D0' ['D1','D2',...,'D20'],'EMG RMS',
%       'Heart Rate','Marker','none' or 'all'
%
%   or as vector containing the channels to write, for instance
%
%   [HDR,DATA] = ReadBP('C:\data\TestACQ');
%   SaveBP('C:\data\TestWrite.ACQ',HDR,DATA,[],[],[3 9 12]);
%
%   will save all points of the channels 3,9 and 12.



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



if nargin<8;AskOverWrite = [];end
if nargin<7;PlotStatus = [];end
if nargin<6;PrintStatus = [];end
if nargin<5;AllPrintStatus = [];end
if nargin<6;ExtractChannel = [];end
if nargin<5;t2 = [];end
if nargin<4;t1 = [];end
if nargin<3;DATA = [];end
if nargin<2;HDR = [];end
if nargin<1;FilePath = [];end


if isempty(AskOverWrite);AskOverWrite = 1;end
if isempty(HDR);error('Need header information!');end
if isempty(DATA);error('No data to write!');end
if isempty(FilePath);[n,p] = uiputfile('*.ACQ','Name data file:');if isequal(n,0)|isequal(p,0);return;end;FilePath = [p,n];end
if isempty(t1);t1 =1;end
if isempty(t2);t2 =inf;end
if isempty(ExtractChannel) | strcmp(ExtractChannel,'all');ExtractChannel = [1:HDR.nChannels];
elseif ~isnumeric(ExtractChannel);ExtractChannelString = ExtractChannel;ExtractChannel = [];
for ChanInd = 1:HDR.nChannels;if ~isempty(findstr(HDR.szCommentText{ChanInd},ExtractChannelString))
ExtractChannel = ChanInd; end;end;end
if isempty(ExtractChannel);error('Unknown channel name!');end
if isempty(AllPrintStatus);AllPrintStatus = 0;end
if isempty(PrintStatus);PrintStatus = 1;end
if AllPrintStatus;PrintStatus = 1;end

FTellStatus = 0;
OriginalNChan = HDR.nChannels;
HDR.nChannels = length(ExtractChannel);




disp('start writing biopac file: ');
disp(FilePath);

fid = fopen(FilePath,'rb');
if fid~=-1
    if AskOverWrite
        OverWrite = questdlg('File exists! Continuing will overwrite existing file. Continue?','Warning','Yes','No','Yes');
        if strcmp(OverWrite,'No')
            fclose(fid);
            return
        end
    end
    fclose(fid);
end
fid = fopen(FilePath,'wb');


% 	Item                Type    Size    Offset  Description
%=============================================================================
%=============================================================================
%=============================================================================


% 	nItemHeaderLen 	    short 	2 	    0 	Not currently used.
fwrite(fid,HDR.nItemHeaderLen,'short');
if AllPrintStatus
    fprintf(1,['writing nItemHeaderLen = ',num2str(HDR.nItemHeaderLen),' ...\n']);
end


% 	lVersion 	        long 	4 	    2 	File version identifier:
%
% 						30 = Pre-version 2.0
% 						31 = Version 2.0 Beta 1
% 						32 = Version 2.0 release
% 						33 = Version 2.0.7 (Mac)
% 						34 = Version 3.0 In-house Release 1
% 						35 = Version 3.03
% 						36 = version 3.5x (Win 95, 98, NT)
% 						37 = version of BSL/PRO 3.6.x
% 						38 = version of Acq 3.7.0-3.7.2 (Win 98, 98SE, NT, Me, 2000)
% 						39 = version of Acq 3.7.3 or above (Win 98, 98SE, 2000, Me, XP)
% 						41 = version of Acq 3.8.1 or above (Win 98, 98SE, 2000, Me, XP)
% 						42 = version of BSL/PRO 3.7.X or above (Win 98, 98SE, 2000, Me, XP)


Offset = 2;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.lVersion,'int32');
if AllPrintStatus
    fprintf(1,['writing lVersion = ',num2str(HDR.lVersion),' ...\n']);
end


% 	lExtItemHeaderLen   long    4 	    6 	Extended item header length.
Offset = 6;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.lExtItemHeaderLen,'int32');
if AllPrintStatus
    fprintf(1,['writing lExtItemHeaderLen = ',num2str(HDR.lExtItemHeaderLen),' ...\n']);
end

% 	nChannels           short   2 	    10 	Number of channels stored.
Offset = 10;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nChannels,'short');
if AllPrintStatus
    fprintf(1,['writing nChannels = ',num2str(HDR.nChannels),' ...\n']);
end

%   nHorizAxisType      short   2       12  Horizontal scale type, one of the  following
% 			0 = Time in seconds
% 			1 = Time in HMS format
% 			2 = Frequency
% 			3 = Arbitrary
Offset = 12;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nHorizAxisType,'short');
if AllPrintStatus
    fprintf(1,['writing nHorizAxisType = ',num2str(HDR.nHorizAxisType),' ...\n']);
end

% 	nCurChannel         short   2 	    14 	Currently selected channel.
Offset = 14;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nCurChannel,'short');
if AllPrintStatus
    fprintf(1,['writing nCurChannel = ',num2str(HDR.nCurChannel),' ...\n']);
end

% 	dSampleTime         double  8 	    16 	The number of milliseconds per sample.
Offset = 16;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.dSampleTime,'double');
if AllPrintStatus
    fprintf(1,['writing dSampleTime = ',num2str(HDR.dSampleTime),' ...\n']);
end

% 	dTimeOffset         double  8 	    24 	The initial time offset in milliseconds.
Offset = 24;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.dTimeOffset,'double');
if AllPrintStatus
    fprintf(1,['writing dTimeOffset = ',num2str(HDR.dTimeOffset),' ...\n']);
end

% 	dTimeScale          double  8 	    32 	The time scale in milliseconds per division.
Offset = 32;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.dTimeScale,'double');
if AllPrintStatus
    fprintf(1,['writing dTimeScale = ',num2str(HDR.dTimeScale),' ...\n']);
end

% 	dTimeCursor1        double  8 	    40 	Cursor 1 time position in milliseconds.
Offset = 40;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.dTimeCursor1,'double');
if AllPrintStatus
    fprintf(1,['writing dTimeCursor1 = ',num2str(HDR.dTimeCursor1),' ...\n']);
end

% 	dTimeCursor2        double  8 	    48 	Cursor 2 time position in milliseconds.
Offset = 48;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.dTimeCursor2,'double');
if AllPrintStatus
    fprintf(1,['writing dTimeCursor2 = ',num2str(HDR.dTimeCursor2),' ...\n']);
end

% 	rcWindow            RECT    8 	    56 	The chart's size and position relative to the AcqKnowledge client area. When each
%                                           RECT field is set to 0,  the chart is displayed with default a size and position.
Offset = 56;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.rcWindow,'short');
if AllPrintStatus
    fprintf(1,['writing rcWindow = ',num2str(HDR.rcWindow),' ...\n']);
end


% 	nMeasurement[6]   	short   6*2 	64 	Describes the currently selected measurements, one of the following:
% 							0 = No measurement
% 							1 = Value Absolute voltage
%                           2 = Delta Voltage difference
% 							3 = Peak to peak voltage
% 							4 = Maximum voltage
% 							5 = Minimum voltage
% 							6 = Mean voltage
% 							7 = Standard deviation
% 							8 = Integral
% 							9 = Area
% 							10 = Slope
% 							11 = LinReg
% 							13 = Median
% 							15 = Time
%                           16 = Delta Time
% 							17 = Freq
% 							18 = BPM
% 							19 = Samples
% 							20 = Delta Samples
% 							21 = Time of Median
% 							22 = Time of Max
% 							23 = Time of Min
% 							25 = Calculation
% 							26 = Correlation
Offset = 64;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nMeasurement,'short');
if AllPrintStatus
    fprintf(1,['writing nMeasurement = ',num2str(HDR.nMeasurement),' ...\n']);
end

% 	fHilite             BOOL     2 	 76 	Gray non-selected waveforms:
% 							0 = Don't gray
%                           1 = Gray.
Offset = 76;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.fHilite,'short');
if AllPrintStatus
    fprintf(1,['writing fHilite = ',num2str(HDR.fHilite),' ...\n']);
end


% 	dFirstTimeOffset    double          	8 	78 	 Initial time offset in milliseconds.
Offset = 78;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.dFirstTimeOffset,'double');
if AllPrintStatus
    fprintf(1,['writing dFirstTimeOffset = ',num2str(HDR.dFirstTimeOffset),' ...\n']);
end

% 	nRescale            short             	2 	86 	Autoscale after transforms:
% 					        0 = Don't autoscale
% 					        1 = Autoscale.
Offset = 86;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nRescale,'short');
if AllPrintStatus
    fprintf(1,['writing nRescale = ',num2str(HDR.nRescale),' ...\n']);
end

% 	szHorizUnits1       char      40 	88 	Horizontal units text.
Offset = 88;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.szHorizUnits1,'char');
if AllPrintStatus
    fprintf(1,['writing szHorizUnits1 = ',num2str(HDR.szHorizUnits1),' ...\n']);
end

% 	szHorizUnits2       char      10 	128 	Horizontal units text (abbreviated).
Offset = 128;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.szHorizUnits2,'char');
if AllPrintStatus
    fprintf(1,['writing szHorizUnits2 = ',num2str(HDR.szHorizUnits2),' ...\n']);
end

% 	nInMemory           short             	2 	138 	Keep data file in memory:
% 						0 = Keep
% 						1 = Don't keep.
Offset = 138;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nInMemory,'short');
if AllPrintStatus
    fprintf(1,['writing nInMemory = ',num2str(HDR.nInMemory),' ...\n']);
end

% 	fGrid               BOOL      2 	140 	Enable grid display.
Offset = 140;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.fGrid,'short');
if AllPrintStatus
    fprintf(1,['writing fGrid = ',num2str(HDR.fGrid),' ...\n']);
end

% 	fMarkers            BOOL      2 	142 	Enable marker display.
Offset = 142;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
%fwrite(fid,HDR.fMarkers,'short');
fwrite(fid,0,'short');
if AllPrintStatus
    fprintf(1,['writing fMarkers = ',num2str(HDR.fMarkers),' ...\n']);
end

% 	nPlotDraft          short     2 	144 	Enable draft plotting.
Offset = 144;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nPlotDraft,'short');
if AllPrintStatus
    fprintf(1,['writing nPlotDraft = ',num2str(HDR.nPlotDraft),' ...\n']);
end

% 	nDispMode           short     2 	146 	Display mode:
% 					0 = Scope
% 					1 = Chart.
Offset = 146;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nDispMode,'short');
if AllPrintStatus
    fprintf(1,['writing nDispMode = ',num2str(HDR.nDispMode),' ...\n']);
end

% 	nReserved           short     2 	148 	Reserved.
Offset = 148;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nReserved,'short');
if AllPrintStatus
    fprintf(1,['writing nReserved = ',num2str(HDR.nReserved),' ...\n']);
end

if HDR.lVersion >= 34
	%   Version 3.0 and above ...
	% 	Item                    Type       Size     Offset  Description
	% 	BShowToolBar            short      2 	    150
	Offset = 150;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.BShowToolBar,'short');
	if AllPrintStatus
        fprintf(1,['writing BShowToolBar = ',num2str(HDR.BShowToolBar),' ...\n']);
	end

	% 	BShowChannelButtons     short      2 	    152
	Offset = 152;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.BShowChannelButtons,'short');
	if AllPrintStatus
        fprintf(1,['writing BShowChannelButtons = ',num2str(HDR.BShowChannelButtons),' ...\n']);
	end

	% 	BShowMeasurements       short      2 	    154
	Offset = 154;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.BShowMeasurements,'short');
	if AllPrintStatus
        fprintf(1,['writing BShowMeasurements = ',num2str(HDR.BShowMeasurements),' ...\n']);
	end

	% 	BShowMarkers            short      2 	    156
	Offset = 156;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	%fwrite(fid,HDR.BShowMarkers,'short');
	fwrite(fid,0,'short');
	if AllPrintStatus
        fprintf(1,['writing BShowMarkers = ',num2str(HDR.BShowMarkers),' ...\n']);
	end

	% 	BShowJournal            short      2 	    158
	Offset = 158;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.BShowJournal,'short');
	if AllPrintStatus
        fprintf(1,['writing BShowJournal = ',num2str(HDR.BShowJournal),' ...\n']);
	end

	% 	CurXChannel             short      2 	    160
	Offset = 160;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.CurXChannel,'short');
	if AllPrintStatus
        fprintf(1,['writing CurXChannel = ',num2str(HDR.CurXChannel),' ...\n']);
	end

	% 	MmtPrecision            short      2 	    162
	Offset = 162;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.MmtPrecision,'short');
	if AllPrintStatus
        fprintf(1,['writing MmtPrecision = ',num2str(HDR.MmtPrecision),' ...\n']);
	end

    if HDR.lVersion >=35

		%   Version 3.02 and above ...
		% 	Item                    Type        Size    Offset  Description
		% 	NMeasurementRows        short 	    2 	    164 	Number of measurement rows
		Offset = 164;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		fwrite(fid,HDR.NMeasurementRows,'short');
		if AllPrintStatus
            fprintf(1,['writing NMeasurementRows = ',num2str(HDR.NMeasurementRows),' ...\n']);
		end

		% 	mmt[40]  	            short 	    2 * 40  166 	Measurement functions
		Offset = 166;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		fwrite(fid,HDR.mmt,'short');
		if AllPrintStatus
            fprintf(1,['writing mmt = ',num2str(HDR.mmt),' ...\n']);
		end

		% 	mmtChan[40]   	        short 	    2 * 40  246 	Measurement channels
		Offset = 246;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		fwrite(fid,HDR.mmtChan,'short');
		if AllPrintStatus
            fprintf(1,['writing mmtChan = ',num2str(HDR.mmtChan),' ...\n']);
		end

        if HDR.lVersion >= 36
			%   Version 3.5x and above ...
			% 	Item                    Type        Size    Offset  Description
			% 	MmtCalcOpnd1 	        short 	    2 * 40  326 	Measurement, Calculation - Operand 1
			Offset = 326;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.MmtCalcOpnd1,'short');
			if AllPrintStatus
                fprintf(1,['writing MmtCalcOpnd1 = ',num2str(HDR.MmtCalcOpnd1),' ...\n']);
			end

			% 	MmtCalcOpnd2  	        short 	    2 * 40  406 	Measurement, Calculation - Operand 2
			Offset = 406;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.MmtCalcOpnd2,'short');
			if AllPrintStatus
                fprintf(1,['writing MmtCalcOpnd2 = ',num2str(HDR.MmtCalcOpnd2),' ...\n']);
			end

			% 	MmtCalcOp   	        short 	    2 * 40  486 	Measurement, Calculation - Operation
			Offset = 486;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.MmtCalcOp,'short');
			if AllPrintStatus
                fprintf(1,['writing MmtCalcOp = ',num2str(HDR.MmtCalcOp),' ...\n']);
			end

			% 	MmtCalcConstant   	    double 	    8 * 40  566 	Measurement, Calculation - Constant
			Offset = 566;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.MmtCalcConstant,'double');
			if AllPrintStatus
                fprintf(1,['writing MmtCalcConstant = ',num2str(HDR.MmtCalcConstant),' ...\n']);
			end

			%                          Version 3.7.0 and above ...
			% 	bNewGridwithMinor 	BOOL       886 	    4 	New Grid with minor line
			Offset = 886;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.bNewGridwithMinor,'int32');
			if AllPrintStatus
                fprintf(1,['writing bNewGridwithMinor = ',num2str(HDR.bNewGridwithMinor),' ...\n']);
			end

			% 	colorMajorGrid 	    long       890 	    4 	COLORREF
			Offset = 890;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.colorMajorGrid,'char');
			if AllPrintStatus
                fprintf(1,['writing colorMajorGrid (ARGB) = ',num2str(HDR.colorMajorGrid),' ...\n']);
			end

			% 	colorMinorGrid 	    long       894 	    4 	COLORREF
			Offset = 894;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.colorMinorGrid,'char');
			if AllPrintStatus
                fprintf(1,['writing colorMinorGrid  (ARGB) = ',num2str(HDR.colorMinorGrid),' ...\n']);
			end

			% 	wMajorGridStyle     short      898 	    2 	PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT,  PS_DASHDOTDOT
			Offset = 898;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.wMajorGridStyle,'short');
			if AllPrintStatus
                fprintf(1,['writing wMajorGridStyle = ',num2str(HDR.wMajorGridStyle),' ...\n']);
			end

			% 	wMinorGridStyle     short      900 	    2 	PS_SOLID, PS_DASH, PS_DOT, PS_DASHDOT, PS_DASHDOTDOT
			Offset = 900;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.wMajorGridStyle,'short');
			if AllPrintStatus
                fprintf(1,['writing wMajorGridStyle = ',num2str(HDR.wMajorGridStyle),' ...\n']);
			end


			% 	wMajorGridWidth     short      902 	    2 	width of line in Pixels
			Offset = 902;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.wMajorGridWidth,'short');
			if AllPrintStatus
                fprintf(1,['writing wMajorGridWidth = ',num2str(HDR.wMajorGridWidth),' ...\n']);
			end

			% 	wMinorGridWidth     short      904 	    2 	width of line in Pixels
			Offset = 904;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.wMinorGridWidth,'short');
			if AllPrintStatus
                fprintf(1,['writing wMinorGridWidth = ',num2str(HDR.wMinorGridWidth),' ...\n']);
			end

			% 	bFixedUnitsDiv      BOOL       906 	    4 	Locked/Unlocked grid lines
			Offset = 906;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.bFixedUnitsDiv,'int32');
			if AllPrintStatus
                fprintf(1,['writing bFixedUnitsDiv = ',num2str(HDR.bFixedUnitsDiv),' ...\n']);
			end

			% 	bMid_Range_Show     BOOL       910 	    4 	show gridlines as MidPoint and Range
			Offset = 910;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.bMid_Range_Show,'int32');
			if AllPrintStatus
                fprintf(1,['writing bMid_Range_Show = ',num2str(HDR.bMid_Range_Show),' ...\n']);
			end

			% 	dStart_Middle_Point double     914 	    8 	Startpoint to draw grid
			Offset = 914;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.dStart_Middle_Point,'double');
			if AllPrintStatus
                fprintf(1,['writing dStart_Middle_Point = ',num2str(HDR.dStart_Middle_Point),' ...\n']);
			end

			% 	dOffset_Point       double     922 	    8 * 60 	Offset of VERTICAL value per channel
			Offset = 922;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.dOffset_Point,'double');
			if AllPrintStatus
                fprintf(1,['writing dOffset_Point = ',num2str(HDR.dOffset_Point),' ...\n']);
			end

			% 	hGrid               double     1402 	8 	Horizontal grid spacing
			Offset = 1402;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.hGrid,'double');
			if AllPrintStatus
                fprintf(1,['writing hGrid = ',num2str(HDR.hGrid),' ...\n']);
			end

			% 	vGrid               double     1410 	8 * 60 	Vertical grid spacing per channel
			Offset = 1410;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.vGrid,'double');
			if AllPrintStatus
                fprintf(1,['writing vGrid = ',num2str(HDR.vGrid),' ...\n']);
			end


			% 	bEnableWaveTools    BOOL       1890 	4 	Enable Wavetools during acquisition
			Offset = 1890;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.bEnableWaveTools,'int32');
			if AllPrintStatus
                fprintf(1,['writing bEnableWaveTools = ',num2str(HDR.bEnableWaveTools),' ...\n']);
			end

            if HDR.lVersion >= 39
				%  Version 3.7.3 and above ...
				%   Item               Type       Offset    Size   Description
				%   horizPrecision     short  	  1894 	    2 	    digits of precision for units in Horizontal Axis
				Offset = 1894;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
				fwrite(fid,HDR.horizPrecision,'short');
				if AllPrintStatus
                    fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
				end

                if HDR.lVersion >= 40

					% Version 3.8.1 and above ...
					% Item                      Type    Offset  Size   Description
					% RESERVED  	            byte 	1896 	20 	    RESERVED
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bOverlapMode  	        BOOL 	1916 	4 	    Overlap Mode
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bShowHardware  	        BOOL 	1920 	4 	    Hardware visibility
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bXAutoPlot  	            BOOL 	1924 	4 	    Autoplot during acquisition
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bXAutoScroll  	        BOOL 	1928 	4 	    Autoscroll during acquisition
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bStartButtonVisible  	    BOOL 	1932 	4 	    Start button visibility
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bCompressed  	            BOOL 	1936 	4 	    The file is compressed
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

					% bAlwaysStartButtonVisible BOOL 	1940 	4 	    Always show start button
					Offset = 1896;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
					fwrite(fid,HDR.horizPrecision,'short');
					if AllPrintStatus
                        fprintf(1,['writing horizPrecision = ',num2str(HDR.horizPrecision),' ...\n']);
					end

                end
            end
        end
    end
end



StartPerChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['StartPerChannelData = %g ...\n'],StartPerChannelData);
end
CumPerChannel = 0;
MarkerChan = [];
TriggerChan = [];
if PrintStatus
    disp('writing header data for channels...');
end

% 	Per Channel Data Section...
for ChanInd = 1: HDR.nChannels

    ChanVal = ExtractChannel(ChanInd);
    if  AllPrintStatus
        fprintf(1,'\n');
        fprintf(1,['writing header data for channel ',num2str(ChanVal),' of ',num2str(HDR.nChannels),' total channels...\n']);
        fprintf(1,['--------------------------------------------------------------------------\n']);
    end

	% 	Item               Type       Offset    Size    Description
	% 	lChanHeaderLen     long       0 	    4 	    Length of channel header.
    Offset = 0 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.lChanHeaderLen{ChanVal},'long');
	if AllPrintStatus
        fprintf(1,['\twriting lChanHeaderLen{',num2str(ChanVal),'} = ',num2str(HDR.lChanHeaderLen{ChanVal}),' ...\n']);
	end

	% 	nNum               short      4 	    2 	 Channel number.
    Offset = 4 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.nNum{ChanInd},'short');
	if AllPrintStatus
        fprintf(1,['\twriting nNum{',num2str(ChanVal),'} = ',num2str(HDR.nNum{ChanVal}),' ...\n']);
    end


	% 	szCommentText      char       6 	    40 	Comment text.
    Offset = 6 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.szCommentText{ChanVal},'char');
	if AllPrintStatus
        fprintf(1,['\twriting szCommentText{',num2str(ChanVal),'} = ']);
        fprintf(1,'%c',char(HDR.szCommentText{ChanVal}));
        fprintf(1,'\n');
	end

	% 	rgbColor           RGB        46 	    4 	 Color.
    Offset = 46 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.rgbColor{ChanVal},'char');
	if AllPrintStatus
        fprintf(1,['\twriting rgbColor{',num2str(ChanVal),'} (ARGB) = ',num2str(HDR.rgbColor{ChanVal}),' ...\n']);
	end


	% 	nDispChan          short      50 	    2 	Display option.
    Offset = 50 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.nDispChan{ChanInd},'short');
	if AllPrintStatus
        fprintf(1,['\twriting nDispChan{',num2str(ChanVal),'} = ',num2str(HDR.nDispChan{ChanVal}),' ...\n']);
	end


	% 	dVoltOffset        double     52 	    8 	Amplitude offset (volts).
    Offset = 52 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.dVoltOffset{ChanVal},'double');
	if AllPrintStatus
        fprintf(1,['\twriting dVoltOffset{',num2str(ChanVal),'} = ',num2str(HDR.dVoltOffset{ChanVal}),' ...\n']);
	end


	% 	dVoltScale         double     60 	    8 	Amplitude scale (volts/div).
    Offset = 60 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.dVoltScale{ChanVal},'double');
	if AllPrintStatus
        fprintf(1,['\twriting dVoltScale{',num2str(ChanVal),'} = ',num2str(HDR.dVoltScale{ChanVal}),' ...\n']);
	end

	% 	szUnitsText        char       68 	    20 	Units text.
    Offset = 68 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.szUnitsText{ChanVal},'char');
	if AllPrintStatus
        fprintf(1,['\twriting szUnitsText{',num2str(ChanVal),'} = ',HDR.szUnitsText{ChanVal}]);
        fprintf(1,'\n');
	end

	% 	lBufLength         long       88 	    4 	Number of data samples.
    Offset = 88 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	%whole file
    if t1==1 & t2==inf
        fwrite(fid,HDR.lBufLength{ChanVal},'int32');
    else
        NPointsToWrite = t2 - t1 + 1;
        fwrite(fid,NPointsToWrite,'int32');
    end
	if AllPrintStatus
        fprintf(1,['\twriting lBufLength{',num2str(ChanVal),'} = ',num2str(HDR.lBufLength{ChanVal}),' ...\n']);
	end

	% 	dAmplScale         double     92 	    8 	Units/count.
    Offset = 92 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.dAmplScale{ChanVal},'double');
	if AllPrintStatus
        fprintf(1,['\twriting dAmplScale{',num2str(ChanVal),'} = ',num2str(HDR.dAmplScale{ChanVal}),' ...\n']);
	end

	% 	dAmplOffset        double     100 	    8 	Units
    Offset = 100 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.dAmplOffset{ChanVal},'double');
	if AllPrintStatus
        fprintf(1,['\twriting dAmplOffset{',num2str(ChanVal),'} = ',num2str(HDR.dAmplOffset{ChanVal}),' ...\n']);
	end

	% 	nChanOrder         short      108 	    2 	Displayed channel order.
    Offset = 108 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.nChanOrder{ChanInd},'short');
	if AllPrintStatus
        fprintf(1,['\twriting nChanOrder{',num2str(ChanVal),'} = ',num2str(HDR.nChanOrder{ChanVal}),' ...\n']);
    end

	% 	nDispSize          short      110 	    2 	Channel partition size.
    Offset = 110 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
	fwrite(fid,HDR.nDispSize{ChanVal},'short');
	if AllPrintStatus
        fprintf(1,['\twriting nDispSize{',num2str(ChanVal),'} = ',num2str(HDR.nDispSize{ChanVal}),' ...\n']);
	end
    StepSize = 112;

    if HDR.lVersion >= 34
        %   Version 3.0 and above ...
		% 	Item              Type       Offset     Size  Description
		% 	plotMode          short      112 	    2
        Offset = 112 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		fwrite(fid,HDR.plotMode{ChanVal},'short');
		if AllPrintStatus
            fprintf(1,['\twriting plotMode{',num2str(ChanVal),'} = ',num2str(HDR.plotMode{ChanVal}),' ...\n']);
		end

		% 	vMid              double     114 	    8
        Offset = 114 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
		fwrite(fid,HDR.vMid{ChanVal},'double');
		if AllPrintStatus
            fprintf(1,['\twriting nvMid{',num2str(ChanVal),'} = ',num2str(HDR.vMid{ChanVal}),' ...\n']);
		end
        StepSize = 122;


        if HDR.lVersion >= 38

			%   Version 3.7.0 and above ...
			% 	Item                Type      Offset    Size    Description
			% 	szDescription       char      122 	    128 	String of Channel description
            Offset = 122 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.szDescription{ChanInd},'char');
			if AllPrintStatus
                fprintf(1,['\twriting szDescription{',num2str(ChanVal),'} = ']);
                fprintf(1,'%c',char(HDR.szDescription{ChanVal}));
                fprintf(1,'\n');
			end

			% 	nVarSampleDivider   short     250 	    2 	Channel divider of main frequency
            Offset = 250 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.nVarSampleDivider{ChanVal},'short');
			if AllPrintStatus
                fprintf(1,['\twriting nVarSampleDivider{',num2str(ChanVal),'} = ',num2str(HDR.nVarSampleDivider{ChanVal}),' ...\n']);
			end

            %     tem              Type      Offset     Size    Description
            %   vertPrecision 	   short  	  252 	    2 	    digits of precision for units in Vertical Axis for each channel
            Offset = 252 + CumPerChannel + StartPerChannelData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
			fwrite(fid,HDR.vertPrecision{ChanVal},'short');
			if AllPrintStatus
                fprintf(1,['\twriting vertPrecision{',num2str(ChanVal),'} = ',num2str(HDR.vertPrecision{ChanVal}),' ...\n']);
			end
            StepSize = 254;
        end
    end
    CumPerChannel = CumPerChannel + StepSize;
end

%EventStatus
EventStatus = [];
if isempty(MarkerChan) & isempty(TriggerChan)
    if PrintStatus
        disp('No event channel found...');
    end
    EventStatus = 0;
elseif ~isempty(MarkerChan) & isempty(TriggerChan)
    if PrintStatus
        disp('Using marker channel for event information ...');
    end
    EventStatus = 2;
elseif isempty(MarkerChan) & ~isempty(TriggerChan)
    if PrintStatus
        disp('Using trigger channel for event information ...');
    end
    EventStatus = 1;
elseif ~isempty(MarkerChan) & ~isempty(TriggerChan)
    if PrintStatus
        disp('Marker and Trigger channel found! Using trigger channel for event information ...');
    end
    EventStatus = 3;
end

StartForeignData = ftell(fid);
if FTellStatus
    fprintf(1,['StartForeignData = %g ...\n'],StartForeignData);
end

if PrintStatus
    fprintf(1,'Start writing foreign data section...\n');
end


% 	Foreign Data Section...
% 	Item               Type       Size      Offset  Description
% 	nLength            short      2 	    0 	    Total length of foreign data packet.
Offset = 0 + StartForeignData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nLength,'short');
if AllPrintStatus
    fprintf(1,['writing nLength = ',num2str(HDR.nLength),' ...\n']);
end

% 	nID                short      2 	    2 	ID of foreign data.
Offset = 2 + StartForeignData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.nID,'short');
if AllPrintStatus
    fprintf(1,['writing nID = ',num2str(HDR.nID),' ...\n']);
end

% 	ByForeignData      BYTE       nLength   4 	Foreign data.
Offset = 4 + StartForeignData;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
fwrite(fid,HDR.ByForeignData,'char');
if AllPrintStatus
    if HDR.nLength>100
        fprintf(1,['written ByForeignData too large to display ...\n']);
    else
        fprintf(1,['writing ByForeignData = ',num2str(HDR.ByForeignData),' ...\n']);
    end
end


StartPerChannelDataTypes = ftell(fid);
if FTellStatus
    fprintf(1,['StartPerChannelDataTypes = %g ...\n'],StartPerChannelDataTypes);
end
CumChanSum = 0;
MarkerVal = [];
if AllPrintStatus
    fprintf(1,'Start writing per channel data types section...\n');
end

ChannelByteSizeMat = [];
% 	Per Channel Data Types Section...

for ChanInd = 1: HDR.nChannels


    ChanVal = ExtractChannel(ChanInd);

    % 	This block is repeated for as many channels that were detected in the graph header packet nChannels field.
    % 	Item               Type       Size      Offset          	Description
    % 	nSize              short      2 	    0 	                Channel data size in bytes.
    Offset = 0 + CumChanSum + StartPerChannelDataTypes;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    fwrite(fid,HDR.nSize{ChanVal},'short');
    if AllPrintStatus
        fprintf(1,['writing nSize{',num2str(ChanVal),'} = ',num2str(HDR.nSize{ChanVal}),' ...\n']);
    end

    % 	nType              short      2 	    2 	Channel data type:
    % 								1 = double
    % 								2 = int
    Offset = 2 + CumChanSum + StartPerChannelDataTypes;Pos = ftell(fid);if Pos~=Offset; fclose(fid);error('wrong position...');end
    fwrite(fid,HDR.nType{ChanVal},'short');
    if AllPrintStatus
        if HDR.nType{ChanVal}==1
            fprintf(1,['writing nType{',num2str(ChanVal),'} = double ...\n']);
        elseif HDR.nType{ChanVal}==2
            fprintf(1,['writing nType{',num2str(ChanVal),'} = integer ...\n']);
        else
            error('Unknown datatype!');
        end
    end
    CumChanSum = CumChanSum + 4;
end
ChannelByteSizeMat = vertcat(HDR.nSize{:});


StartChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['StartChannelData = %g ...\n'],StartChannelData);
end

if AllPrintStatus
    fprintf(1,'Start writing channel data ...\n');
end

% 	Channel Data Section...
% 	The individual channel data is stored after the Per Channel Data Types Section. The channel data is in an interleaved format.
%check for number of points in channels...
if AllPrintStatus
    fprintf(1,['number of channels in file: ',num2str(HDR.nChannels),'\n']);
end
%check for equal number of samples
InitNSample = HDR.lBufLength{ExtractChannel(1)};
Equal = 1;
if AllPrintStatus
    fprintf(1,['number of samples and in channels:\t sampling divider:\n']);
end
for ChanInd = 1:HDR.nChannels
    if  HDR.lBufLength{ChanVal} ~= InitNSample
        Equal = 0;
    end
    if AllPrintStatus
        fprintf(1,['channel ',num2str(ChanInd),': ',num2str([HDR.lBufLength{ChanInd}]','%20.0f'),' samples']);
        fprintf(1,[': \t\t',num2str([HDR.nVarSampleDivider{ChanInd}]','%20.0f')]);
        fprintf(1,'\n');
	end
end
if AllPrintStatus
	if Equal
        fprintf(1,'Number of samples are equal in all channels...\n');
	end
end

StartDataSection = ftell(fid);


%case all equal sampling rates
%-----------------------------------------
if Equal

    %whole file
    %==========
    if t1==1 & t2==inf
        %write data

        %what is written
        NChanToWrite = length(ExtractChannel);
        fprintf(1,['writing data of ',num2str(length(ExtractChannel)),' channel(s) (',num2str(ExtractChannel,'%1.0f '),') to file ....\n']);
        NSample = InitNSample;

        %update ByteMat for subset of channels
        NotExtracted = setdiff([1:OriginalNChan],ExtractChannel);
        ChannelByteSizeMat(NotExtracted) = 0;
        AllChannelSkipVal = sum(ChannelByteSizeMat);

        %only for displaying info
        TypeCell = {'double','integer'};

        %prewrite data section
        NBytes = AllChannelSkipVal*size(DATA,2);
        testdata = zeros(1,NBytes);
        fwrite(fid,testdata,'char');

        for WriteChanInd = 1: NChanToWrite

            ChanVal = ExtractChannel(WriteChanInd);
            WriteChanVal = ExtractChannel(WriteChanInd);
            WriteChanDataVal =ExtractChannel(WriteChanInd);
            WriteChanOffsetVal =ExtractChannel(WriteChanInd);
            WriteChanSkipVal = ExtractChannel(WriteChanInd);

            %reset file position indicator
            fseek(fid,StartDataSection,'bof');

            ChannelInitOffset = sum(ChannelByteSizeMat(1:WriteChanOffsetVal-1));
            ChannelSkipVal = sum(ChannelByteSizeMat([1:WriteChanSkipVal-1,WriteChanSkipVal+1:end]));

            if PrintStatus
                fprintf(1,['Writing channel # ',num2str(WriteChanVal),' (']);
                fprintf(1,'%c',HDR.szCommentText{WriteChanVal});
                fprintf(1,[') of type ',TypeCell{HDR.nType{WriteChanVal}},' with initial offset of ',num2str(ChannelInitOffset),', a skipval of ',num2str(ChannelSkipVal),' ...\n']);
            end

            %initial offset
            status = fseek(fid, ChannelInitOffset, 'cof');
            if status; error('fseek error!'); end

            StartChan(WriteChanVal) = ftell(fid);
            if FTellStatus
                fprintf(1,['StartChan(',num2str(WriteChanVal),') = ',num2str(StartChan(WriteChanVal)),' ...\n']);
                fprintf(1,'Writing channel #%g with initial offset %g and skipval %g ...\n',WriteChanVal,ChannelInitOffset,ChannelSkipVal);
            end

            if HDR.nType{WriteChanVal} == 1 %double
                TmpData = (DATA(WriteChanDataVal,:) - HDR.dAmplOffset{WriteChanDataVal})./HDR.dAmplScale{WriteChanDataVal};
                TmpDataStart = TmpData(1);
                TmpData = TmpData(2:end);
                count2 = fwrite(fid,TmpDataStart,'double');
                count = fwrite(fid,TmpData,'double',ChannelSkipVal);
                if count+count2~=NSample
                    error('Write error!');
                end
            elseif HDR.nType{WriteChanVal} == 2 %int
                TmpData = (DATA(WriteChanDataVal,:) - HDR.dAmplOffset{WriteChanDataVal})./HDR.dAmplScale{WriteChanDataVal};
                TmpDataStart = TmpData(1);
                TmpData = TmpData(2:end);
                count2 = fwrite(fid,TmpDataStart,'short');
                count = fwrite(fid,TmpData,'short',ChannelSkipVal);
                if count+count2~=NSample
                    error('Write error!');
                end
            else
                fclose(fid);
                error(['Invalid data format for channel ',num2str(WriteChanVal),' ... ']);
            end

            if PrintStatus
                fprintf(1,[num2str(count),' samples of channel %g written...\n'],WriteChanVal);
            end
        end

    %specific segment
    %=================
    else
        %select segment
        DATA = DATA(:,t1:t2);

        %what is written
        NChanToWrite = length(ExtractChannel);
        fprintf(1,['writing data of ',num2str(length(ExtractChannel)),' channel(s) (',num2str(ExtractChannel,'%1.0f '),') to file ....\n']);
        NSample = size(DATA,2);

        %adjust ByteMat if only subset of channels to write
        NotExtracted = setdiff([1:OriginalNChan],ExtractChannel);
        ChannelByteSizeMat(NotExtracted) = 0;
        AllChannelSkipVal = sum(ChannelByteSizeMat);

        %only for displaying info
        TypeCell = {'double','integer','other'};

        %prewrite data section
        NBytes = AllChannelSkipVal*size(DATA,2);
        testdata = zeros(1,NBytes);
        fwrite(fid,testdata,'char');

        for WriteChanInd = 1: NChanToWrite

            ChanVal = ExtractChannel(WriteChanInd);
            WriteChanVal = ExtractChannel(WriteChanInd);
            WriteChanDataVal =ExtractChannel(WriteChanInd);
            WriteChanOffsetVal =ExtractChannel(WriteChanInd);
            WriteChanSkipVal = ExtractChannel(WriteChanInd);

            %reset file position indicator
            fseek(fid,StartDataSection,'bof');

            ChannelInitOffset = sum(ChannelByteSizeMat(1:WriteChanOffsetVal-1));
            ChannelSkipVal = sum(ChannelByteSizeMat([1:WriteChanSkipVal-1,WriteChanSkipVal+1:end]));

            if PrintStatus
                fprintf(1,['Writing channel # ',num2str(WriteChanVal),' (']);
                fprintf(1,'%c',HDR.szCommentText{WriteChanVal});
                fprintf(1,[') of type ',TypeCell{HDR.nType{WriteChanVal}},' with initial offset of ',num2str(ChannelInitOffset),', a skipval of ',num2str(ChannelSkipVal),' ...\n']);
            end

            %initial offset
            status = fseek(fid, ChannelInitOffset, 'cof');
            if status; error('fseek error!'); end

            StartChan(WriteChanVal) = ftell(fid);
            if FTellStatus
                fprintf(1,['StartChan(',num2str(WriteChanVal),') = ',num2str(StartChan(WriteChanVal)),' ...\n']);
                fprintf(1,'Writing channel #%g with initial offset %g and skipval %g ...\n',WriteChanVal,ChannelInitOffset,ChannelSkipVal);
            end

            if HDR.nType{WriteChanVal} == 1 %double
                TmpData = (DATA(WriteChanDataVal,:) - HDR.dAmplOffset{WriteChanDataVal})./HDR.dAmplScale{WriteChanDataVal};
                TmpDataStart = TmpData(1);
                TmpData = TmpData(2:end);
                count2 = fwrite(fid,TmpDataStart,'double');
                count = fwrite(fid,TmpData,'double',ChannelSkipVal);
                if count+count2~=NSample
                    error('Write error!');
                end
            elseif HDR.nType{WriteChanVal} == 2 %int
                TmpData = (DATA(WriteChanDataVal,:) - HDR.dAmplOffset{WriteChanDataVal})./HDR.dAmplScale{WriteChanDataVal};
                TmpDataStart = TmpData(1);
                TmpData = TmpData(2:end);
                count2 = fwrite(fid,TmpDataStart,'short');
                count = fwrite(fid,TmpData,'short',ChannelSkipVal);
                if count+count2~=NSample
                    error('Write error!');
                end
            else
                fclose(fid);
                error(['Invalid data format for channel ',num2str(WriteChanVal),' ... ']);
            end

            if PrintStatus
                fprintf(1,[num2str(count),' samples of channel %g written...\n'],WriteChanVal);
            end
        end
    end
else
    fclose(fid);
    error('Unequal samling rates not supported yet!');
end


EndChannelData = ftell(fid);
if FTellStatus
    fprintf(1,['EndChannelData = %g ...\n'],EndChannelData);
end

[status] = fseek(fid,StartChannelData,'bof');
if status;disp(ferror(fid));fclose(fid);return;end
TotalDataOffset = NSample*sum(ChannelByteSizeMat(:));
[status] = fseek(fid,TotalDataOffset,'cof');
if status;disp(ferror(fid));fclose(fid);return;end


%Markers Header Section...
%Item               Type          Size    Offset          	Description
% lLength            long           4 	0 	Total length of all markers.

HDRftell = ftell(fid);
if FTellStatus
    fprintf(1,['BeginMarkerSection = %g ...\n'],HDRftell);
end
fwrite(fid,HDR.lLength,'long');
if AllPrintStatus
    fprintf(1,['writing lLength = ',num2str(HDR.lLength),' ...\n']);
end

% lMarkers           long           4 	4 	Number of markers.
fwrite(fid,HDR.lMarkers,'long');
if AllPrintStatus
    fprintf(1,['writing lMarkers = ',num2str(HDR.lMarkers),' ...\n']);
end


MarkerItemStart = ftell(fid);
if FTellStatus
    fprintf(1,['BeginMarkerData = %g ...\n'],MarkerItemStart);
end

for MarkerInd = 1:HDR.lMarkers

    %Marker Item Section...
    %Item               Type          Size    Offset          	Description
    % lSample            long            4 	0 	Location of marker.
    fwrite(fid,HDR.lSample{MarkerInd},'int32');
    if AllPrintStatus
        fprintf(1,['writing lSample{',num2str(MarkerInd),'} = ',num2str(HDR.lSample{MarkerInd}),' ...\n']);
    end

    % fSelected          BOOL          	2 	4 	Select this marker.
    fwrite(fid,HDR.fSelected{MarkerInd},'short');
    if AllPrintStatus
        fprintf(1,['writing fSelected{',num2str(MarkerInd),'} = ',num2str(HDR.fSelected{MarkerInd}),' ...\n']);
    end

    % fTextLocked        BOOL          	2 	6 	Lock this text.
    fwrite(fid,HDR.fTextLocked{MarkerInd},'short');
    if AllPrintStatus
        fprintf(1,['writing fTextLocked{',num2str(MarkerInd),'} = ',num2str(HDR.fTextLocked{MarkerInd}),' ...\n']);
    end

    % fPositionLocked    BOOL          	2 	8 	Lock this location.
    fwrite(fid,HDR.fPositionLocked{MarkerInd},'short');
    if AllPrintStatus
        fprintf(1,['writing fPositionLocked{',num2str(MarkerInd),'} = ',num2str(HDR.fPositionLocked{MarkerInd}),' ...\n']);
    end

    % nTextLength        short            	2 	10 	Length of marker text (including NULL).
    fwrite(fid,HDR.nTextLength{MarkerInd},'short');
    if AllPrintStatus
        fprintf(1,['writing nTextLength{',num2str(MarkerInd),'} = ',num2str(HDR.nTextLength{MarkerInd}),' ...\n']);
    end

    % szText             char              	nTextLength                    	12 	Marker text string.
    fwrite(fid,HDR.szText{MarkerInd},'char');
    if AllPrintStatus
        fprintf(1,['writing szText{',num2str(MarkerInd),'} = ',num2str(HDR.szText{MarkerInd}),' ...\n']);
    end
end
disp('end writing biopac file...');
fclose(fid);

return



















%

