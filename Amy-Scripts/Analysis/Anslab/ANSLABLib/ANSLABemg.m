%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

clear

% Settings
examyes=0;        %  (0) Plot data first in EXAM for detailed exploration of artifacts
notchyes=1;       %  (1) Line AC noise notch filter
notchfreq = 60;   %  (60) Notch filter frequency in Hz
ch2yes=1;         %  (1) Also analyze second channel if available
startnan=2;       %  (2) Set first startnan 1/4-sec epochs to NaN (often startup spikes)

% Initializations
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultTextColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end

%*** Settings
if ~exist('data_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
    red_dir=[data_dir,'\n']; 
end


%*** Load tonic EMG data
chd(data_dir)
%FL=filelist('*.txt',0.25);
[filename, data_dir] = uigetfile('*.txt','Select data file');
if isequal(data_dir,0);return;end

eval(['load ',filename,';']);
l=length(filename);
varname=filename(1:l-4);
subjn=str2num(varname(4:6));
studystr=varname(1:3);
eval(['N1=',varname,';']);
eval(['clear ',varname]);
len=length(N1)/25;


%*** resample if necessary
[N1,SR] = AskResampleData(N1,1000,'EMG');

findnck5

figure
plot(t,N10)
title('EMG magnitude')
ylabel('units')
xlabel('time [sec]')

  
%*** Save reduced data
chd(red_dir)
S=[N10];
disp(['save ',varname,'.txt -ascii -tabs S']);
eval(['save ',varname,'.txt -ascii -tabs S']);

disp('Results: mean EMG magnitude: ');
disp(nanmean(N10));



