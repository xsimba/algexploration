

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



clear
close all
closeANSLABlaunchpad
set(0,'DefaultFigureColor',[0 0 0])
set(0,'DefaultTextColor',[1 1 1])
set(0,'DefaultAxesXColor',[1 1 1])
set(0,'DefaultAxesYColor',[1 1 1])
set(0,'DefaultAxesZColor',[1 1 1])
set(0,'DefaultAxesColor',[0 0 0])
set(0,'DefaultLineColor',[1 1 1])
ColorMat =  [1     1     0;...
             1     0     1;...
             0     1     1;...
             1     0     0;...
             0     1     0;...
             0     0     1];
set(0,'DefaultAxesColorOrder',ColorMat)

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end

%*** Settings
if ~exist('data_dir')
    data_dir = PsyPath(1);
    if MVersNr>6.1
        data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');
    else
        [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
    end
    if isequal(data_dir,0);return;end
    PsyPath(2,[data_dir,filesep]);
end
%*** Load ECG data
red_dir1=[data_dir,'\i'];
chd(data_dir);


[filename, data_dir] = uigetfile('*.txt','Select data file');
eval(['load ',filename,';']);
l=length(filename);
varfilename = filename(1:end-4);
varname=filename(1:8);
studystr=varname(1:3);
subjn=str2num(varname(4:6));
filenum=str2num(varname(7:8));
subject=twostr(subjn,3);
file=[subject,twostr(filenum,2),'i'];
eval(['EC=',varfilename,';']);
eval(['clear ',varfilename]);


%*** resample if necessary
[EC,SR] = AskResampleData(EC,400,'ECG');
if isempty(EC);return;end


ecglen=length(EC);

%*** ECG analysis settings (program FINDRTQ5)
upsrfac=1;      %   (3) Upsampling factor (1=no upsampling, 3 for best RSA and PEP resolution)
loadrwave=0;    %   (0) Load edited R-wave info first for further editing (if existing)
usediff=0;      %   (1) Use differentiating algorithm (1, detecting steepest negative slope)
                %       or peak detection algorithm   (0, detecting local peak using certain criteria)
inverseyes=0;   %   (0) Invert signal first
qfindyes=0;     %   (1) Q-point detection (needed for ICG analysis)
qtfindyes=0;    %   (0) QT interval detection (very slow)
tfindyes=1;     %   (1) T-wave amplitude detection
usecorr=0;      %   (0) Use IBI irregularity correction algorithm
usecorrt=1;     %   (1) Use T-wave irregularity correction algorithm
notch60yes=0;   %   (1) Apply 60 Hz notch filter
filteryes=1;    %   (1) Apply low pass filter to filter out noise
lowfreq=40;     %  (40) Filter frequency cutoff for low pass filter (decrease down to 20 possible, if lower, RSA less accurate)
filter2yes=1;   %   (1) Apply high pass filter to filter out baseline wander
% Secondary settings
area=150;       % (150) Peak detection window, 200 detects succeeding peaks at IBIs of 200/400 msec
mingap=150;     % (150) Minimal gap between peaks, if less: the higher one is chosen
back=15;        %  (15) A slope 'back' points before peak to peak has to be greater than 'rise'
dyn=1;          %   (1) Dynamic adaptation of 'rise' from the raw ECG (yes=1, no=0))
rise=10;        %  (10) Fixed 'rise' in mV if 'dyn' is set to 0
critb=0.1;      % (0.1) Criterium of stability for T-wave amplitude (rise*critb)
tv_out=0;       %   (0) Exclusion of outliers in TWA greater than tv_out SD (0=no exclusion)
manualset=0;    %   (0) Ask for manual detection settings (0=no, 1=yes)
rateyes=0;      %   (0) Ask for signal quality rating
ipswitch=0;     %   (0) Switch for resampling data around R-spike
                %       with a factor of ipf using lowpass interpolation
                %       resulting in higher resolution for IBIs (slow)
ipf=4;          %   (4) Interpolation factor
corrfactor=150; % (150) Correction factor for IBI irregularity correction
threshfact=0.15;%(0.15) Threshold factor for IBI irregularity correction
risefactor=0.5; % (0.5) Automatic threshold, ratio from maximal slope
q_range=18;     %  (18) Window for Q-point search backward from R
onset_factor=.2;% (0.2) Ratio of maximum slope to be reached in backward Q-point search
q_fixed=10;     %  (10) If qfindyes is 0: set Q-point q_fixed sample points before R-wave
delsampend=4;   %   (4) Delete delsampend samples at the end (sometimes large spikes)
scalefact=1;    %(100000) Scaling factor for ECG raw signal


if 0
%*** HRV analysis settings
bpvswitch=0;                % Analysis applied to IBI (0) or SBP (1)
plotrsayes=1;               % Plot spectral results
plotcdmyes=1;               % Plot CDM results
plotporgyes=1;              % Plot Porges filtering results
pauseyes=1;                 % Pause after plots
fb=[0.025 0.07 0.13 0.5];   % ([0.025 0.07 0.13 0.5]) Frequency band delimiters for HRV in ECG

%*** General spectral analysis settings
nfft=2048;                  % (2048) Size of FFT window
Lspect=480;                 % (240) Segment size in 1/4-sec epoch points, if smaller: last value is padded
noverlap=Lspect/2;          % (L/2) Number of overlapping points for consecutive FFT segments
trunc=[0 fb(length(fb))];   % ([0 fb(length(fb)]) Truncation of HRV plot
dtorder=0;                  % (0) Order of detrending before complex demodulation analysis
valperc=50;                 % valid percentage of data to be included in CDM analysis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variability measures:
%
% Complex demodulation:
% cdx: amplitude of spectral oscillations in the very low frequency band (VLF, .025-.07)
% cdy: amplitude of spectral oscillations in the low frequency band (LF, .07-.13)
% cdz: amplitude of spectral oscillations in the high frequency band (HF, .13-.5)
% cdr: LF/HF-ratio for amplitude
%
% Spectral analysis:
% sax: amplitude of spectral oscillations in the very low frequency band (VLF, .025-.07)
% say: amplitude of spectral oscillations in the low frequency band (LF, .07-.13)
% saz: amplitude of spectral oscillations in the high frequency band (HF, .13-.5)
% sar: LF/HF-ratio for amplitude
% spx: ln power of spectral oscillations in the very low frequency band (VLF, .025-.07)
% spy: ln power of spectral oscillations in the low frequency band (LF, .07-.13)
% spz: ln power of spectral oscillations in the high frequency band (HF, .13-.5) (typical HF power RSA)
% spr: LF/HF-ratio for power
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

%*** Additional initial settings
lucas_flag=0;
delsampend=0;
byes=0;

%*** R wave detection
findrtq5

%*** Resample IBI and HR to instantaneous 10 Hz
disp('Conversion of ECG parameters into 0.10 sec IBI and TWA epochs');
ibi1=epoch(rt,rtd,40);
ibilen=round(ecglen/2.5/40);
ibi2=nanfill(ibi1,ibilen);
ibi3=nan_lip(ibi2);
ibi3=ibi3(:);
t=(1:length(ibi3))/10;
t=t(:);

hr1=epoch(rt,60000 ./ rtd,40);
hr2=nanfill(hr1,ibilen);
hr3=nan_lip(hr2);
hr3=hr3(:);

tv1=epoch(tt,tv,40);
tv2=nanfill(tv1,ibilen);
tv3=nan_lip(tv2);
tv3=tv3(:);


%*** Plot
cfig
figure(1)
subplot(3,1,1)
plot(t,ibi3);
title('Instantaneous interbeat interval');
ylabel('msec')
subplot(3,1,2)
plot(t,hr3);
title('Instantaneous heart rate');
ylabel('bpm')
subplot(3,1,3)
plot(t,tv3);
title('Instantaneous T-wave amplitude');
ylabel('mV')
xlabel('sec')


%*** Save reduced data
chd(red_dir1)
S=[hr3 ibi3 tv3];
disp(['save ',varname,'i.txt -ascii -tabs S']);
eval(['save ',varname,'i.txt -ascii -tabs S']);

cmdstr=['save ',file, ' rt sr'];
disp(cmdstr);
eval(cmdstr)


if 0
%***************** Spectral analysis
i=1;
cfig
cfig
figure(1)
figure(2)
len=length(ibi0);
if len<Lspect+1
   ibi0(len+1:Lspect+1)=ibi0(len).*ones(size(len+1:Lspect+1));
end

fname='RR intervals and spectral analysis';
[Amp,PD,PDM,T]=rsa(ibi0,ep,plotrsayes,fb,trunc,nfft,Lspect,noverlap,0,fname);
if plotrsayes
    if pauseyes
        pause;
    end;
end

sax(i)=Amp(1);
say(i)=Amp(2);
saz(i)=Amp(3);
sar(i)=say(i)./saz(i);
spx(i)=log(PD(1));
spy(i)=log(PD(2));
spz(i)=log(PD(3));
spr(i)=spy(i)./spz(i);
rf_(i)=T(3);

% Porges RSA analysis
figure(3)
ibidec=decfast(ibi0,2);
[Vp,yporg,yporg_raw] = porges (ibidec,plotporgyes);
por(i)=nanmean(Vp);


% Complex demodulation analysis
[ampm,freqm,A,temp1,temp2]=cdm(nan_lip(ibi0),ep,plotcdmyes,fb,[],[],[],dtorder);
cdx(i)=meanbin(A(:,1),length(ibi0),valperc);
cdy(i)=meanbin(A(:,2),length(ibi0),valperc);
cdz(i)=meanbin(A(:,3),length(ibi0),valperc);
cdr(i)=cdy(i)./cdz(i);
if plotcdmyes if pauseyes pause; end; end
cfig(2)
end


fprintf(1,'Results: mean HR, mean RR-interval, mean T-wave amplitude: \n');
fprintf(1,'\n');
fprintf(1,[num2str(nanmean(hr3)),'\n']);
fprintf(1,[num2str(nanmean(ibi3)),'\n']);
fprintf(1,[num2str(nanmean(tv3)),'\n']);
% fprintf(1,[num2str(sax),'\n']);
% fprintf(1,[num2str(say),'\n']);
% fprintf(1,[num2str(saz),'\n']);
% fprintf(1,[num2str(sar),'\n']);
% fprintf(1,[num2str(spx),'\n']);
% fprintf(1,[num2str(spy),'\n']);
% fprintf(1,[num2str(spz),'\n']);
% fprintf(1,[num2str(spr),'\n']);
% fprintf(1,[num2str(cdx),'\n']);
% fprintf(1,[num2str(cdy),'\n']);
% fprintf(1,[num2str(cdz),'\n']);
% fprintf(1,[num2str(cdr),'\n']);
% fprintf(1,[num2str(por),'\n']);



