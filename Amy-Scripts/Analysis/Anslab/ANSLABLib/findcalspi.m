% Calibration of respitrace with 800 ccm spirobags for NIH study
% Least squares fit method
% In EXAM: Use <@> to mark the onset of a valid inspiration first,
% then mark the end of inspiration, then go to the next valid
% breath.  All options to move through the signals can be used.
%


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


spirometervolume=800;
sr=25;
filtlowyes=0;

%*** Filter signals
R1=filthigh(R1,1,sr,5);
R2=filthigh(R2,1,sr,5);
if filtlowyes
    R1=filtlow(R1,.033,sr,5);
    R2=filtlow(R2,.033,sr,5);
end

cfig, cfig
m1='Find breath maxima and minima automatically';
m2='Manual setting of maxima and minima';
automanual=menu('',m1,m2);

if automanual==2
    %********************** Mark onsets end ends of inspirations in EXAM
    defcal5
    def
    radapt=0; doubleyes=0;
    clear subvarplot
    clg;
    disp(' ');
    disp('In EXAM: Use <@> to mark the onset of a valid inspiration first,');
    disp('then use <@> or <!> to mark the end of inspiration.');
    disp('Then go to another valid breath (the order is not significant).');
    disp('Delete erroneous marks with <#>.');
    disp('All options to zoom <z> or move through the signals can be used.');
    disp('Press <i> to display the whole file, then quit with <q>');
    exam
    event1=sort(event1);
    while rem(length(event1),2)
        disp('Odd number of marked events. Delete or add one.');
        exam
    end;

    t1=1:2:length(event1)-1;
    t2=t1+1;
    pred1= R1(event1(t2)) - R1(event1(t1));        % predictors: thoracic
    pred2= R2(event1(t2)) - R2(event1(t1));        %    abdominal tidal volumes


else
    %******************************** Find max of breaths automatically
    dist=16; back=12; slope=.04; win=8;
    var1=R1; var2=R2;
    figure('Position',[32   208   767   351])
    numbonplot=1;   % lag between consecutive breath numbering on plot
    newthresh=1;

    while newthresh
        m1='Find maxima with default thresholds';
        m2='Change default thresholds';
        m3='Quit';
        newthresh=menu('',m1,m2,m3);
        if newthresh==3
            break; break;
        end;

        if newthresh==2
            shc
            dist=input('Minimal distance between maxima [16] ==> ');
            if isempty(dist)
                dist=16;
            end
            back=input('Comparison to a point back [12] ==> ');
            if isempty(back)
                back=12;
            end
            slope=input('Slope from there at least to be valid [.04] ==> ');
            if isempty(slope)
                slope=.04
            end
            win=input('Window for search of maxima in other channel [8] ==> ');
            if isempty(win)
                win=8;
            end
        end

        xmax1=[]; xmax2=[];
        xmin1=[]; xmin2=[];
        mini=[];bm=[]; bmin=[];

        x1=var1;
        xmax1=findmax(x1,dist,back,slope)';  %thorax maxima
        x2=var2;

        for i=1:length(xmax1)
          n=xmax1(i)-win:xmax1(i)+win+2;
          n(n<1)=[]; n(n>length(x1))=[];  % range check
          [j,xmax2(i)] = max(x2(n));
        end
        xmax2=xmax1-win-1+xmax2;


        %*** find between minima
        i1=xmax1;  % max index

        for i=1:length(i1)-1
            n= i1(i):i1(i+1);
            [j,xmin2(i)] = min(x2(n));
            [mini(i),xmin1(i)] = min(x1(n));
        end

        i1(length(i1))=[];
        xmin1=i1+xmin1-1;
        xmin2=i1+xmin2-1;

        %** first maxima not valid for breath
        xmax1(1)=[];
        xmax2(1)=[];

        out=1;

        cfig, cfig
        figure('Position',[32   208   767   351])

        while out

            %*** plot found maxima and minima
            figure(1)
            t=1:length(x1);
            plot(t,x1,t,x2)
            axis([ t(1) t(length(t)) min([x1(:);x2(:)]) max([x1(:);x2(:)]) ])
            title(['Respitrace units: thorax (y), abdomen (m) -  ',int2str(length(xmax1)),' valid inspirations']);
            hold on
            plot(xmax1,x1(xmax1),'oy');
            plot(xmax2,x2(xmax2),'om');
            plot(xmin1,x1(xmin1),'oy');
            plot(xmin2,x2(xmin2),'om');
            hold off

            % number the breathing cycles on plot
            for i=1:numbonplot:length(xmax1)
                str=int2str(i);
                cmdstr=['text(''Position'',[xmin1(i) x1(xmin1(i))-.05],''String'','' ',str,''')'];
                eval(cmdstr);
            end;
            hold off

            outstr=input('Take out cycle no. [e.g., 3, or: 1-12; <enter>=end] ==> ','s');
            if isempty(outstr)
               out=0; break; break;
            else
               f=findstr(outstr,'-');
               if ~isempty(f)
                 outbeg=str2num(outstr(1:f-1));
                 outend=str2num(outstr(f+1:length(outstr)));
                 out=outbeg:outend;
               else
                 out=str2num(outstr);
               end;
               xmax1(out)=[]; xmax2(out)=[];
               xmin1(out)=[]; xmin2(out)=[]; mini(out)=[];
            end;

        end
        %while out
    end  %while win

    pred1=x1(xmax1)-x1(xmin1);
    pred2=x2(xmax2)-x2(xmin2);
    event1=[ [xmin1 xmax1];[xmin2 xmax2] ];

end

%if automanual






%*** Exclude channel from calibration
ch_ok=menu('','Both channels ok','Only thorax ok','Only abdomen ok')-1;
if ch_ok==1
    pred2=zeros(size(pred1));
end
if ch_ok==2
    pred1=zeros(size(pred1));
end
if ch_ok
    weight=1;
else
    weight=2;
end

%******* Regression procedure
betasucc=[];  % history of betas and other parameter
omit=[];      % omitted breaths
crit = spirometervolume*ones(size(pred1));  % criterium: spirobag
pred1=pred1(:); pred2=pred2(:); crit=crit(:);
E=[pred1 pred2 crit];  % Build E-matrix with all breaths
x=pred1*weight+pred2;  % weight 2 for thorax
x1=pred1;              % only thorax valid
x2=pred2;              % only abdomen valid
t=1:length(x);
take=ones(size(t));
cfig, cfig
figure('Position',[32   208   767   351])
plot(t,pred1,t,pred2);
title('Voltage change of thoracic (yellow) and abdominal (magenta) bands');
xlabel('breath number');
ylabel('V');

figure('Position',[32   208   767   351])
while 1
    fact=spirometervolume/mean(x(take));
    fact1=spirometervolume/mean(x1(take));
    fact2=spirometervolume/mean(x2(take));
    bet(1)=fact*weight*(ch_ok==1|~ch_ok);
    bet(2)=fact*(ch_ok==2|~ch_ok);
    bet(3)=bet(1);
    bet(4)=bet(2);
    if ~ch_ok
       bet(3)=fact1;
       bet(4)=fact2;
    end
    y=pred1*bet(1)+pred2*bet(2);
    y(~take)=NaN*ones(size(find(~take)));
    plot(t,y);
    title(['Beta-weights:   ',num2str(bet(1)),' / ',num2str(bet(2))]);
    xlabel('breath number');
    ylabel('ccm');
    m=mean(nanrem(y));
    plotliny (m,'g-');
    plotliny ([m+200 m-200],'g:');
    cycn=input('Take which breath out of equation? (<enter>=end, 99=start again) ==> ');
    if isempty(cycn)
        cycn=0;
    end
    betasucc=[betasucc bet'];
    omit=[omit cycn];
    if ~cycn
        break; break;
    end;
    if cycn==99
       take=ones(size(t));
    else
       take(cycn)=0;
    end
end

%*** Automatic save into nih_beta.txt
betasucc=[betasucc; [0 omit(1:length(omit)-1)]];
disp(' ');
disp('History [rows: 1=beta(thorax), 2=beta(abdomen), 3=beta(only thorax), 4=beta(only abdomen), 5=excluded breath]:');
disp(betasucc);
m0='Save which betas to be used in analysis of respiration:';
m1='Last betas';
m2='Other betas';
m3='Do not save betas (can be changed manually in file nih_beta.txt)';
m=menu(m0,m1,m2,m3);
betataken=length(omit);
if m==2
   disp(' ');
   disp('History [rows: 1=beta(thorax), 2=beta(abdomen), 3=beta(only thorax), 4=beta(only abdomen), 5=excluded breath]:');
   disp(betasucc);
   i=input('Which betas from the history of parameters?  No. ==> ');
   bet=betasucc(1:4,i);
   bet=bet';
   disp(['Selected betas:']);
   disp(bet)
   betataken=i;
end;
if m==2 | m==1
   chd(mark_dir);
   load nih_beta.txt;
   nih_beta(subjn,1:4)=bet;
   disp(['save nih_beta.txt']);
   save nih_beta.txt nih_beta -ascii -tabs
   bhis=betasucc;
   savestr='c bet bhis betataken event1 E'; % E-matrix with all tidal volumes
end;
if m<1 | m>2
   skip=1; q=1;
end;



