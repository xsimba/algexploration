% kdelbeat.m   delete event1: #


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

es='1';
[i1,i2,i3]=ginput(1);
if i3==50 es='2'; [i1,i2,i3]=ginput(1); end;
if i3==51 es='3'; [i1,i2,i3]=ginput(1); end;
if i3==1

eval(['e1=event',es,';']);

% get rid of IBI = 0;
evd=diff(e1);
n=find(evd==0);
if length(n)
e1(n+1)=[];
end;

[ev1,indn]=nanrem(e1);

[i,ind]=min(abs(ev1-(i1*scalefact)));

ev1(ind)=-999;

e1=nanrest(ev1,indn);

e1(e1==-999)=[];

%valtime=e1;  %for pCO2
%val=var1(nanrem(e1));


if ~isempty(evscan)
evscan(ind)=[];
end;

if ~exist('tsub');tsub = [];end
tsub=[tsub;ind];   % for deletion of tvalues later

end;
plotyes=1;

eval(['event',es,'=e1;']);
