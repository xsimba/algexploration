% findco2 plateaus -> pCO2
% PSY studies
% Low pass filtering
%
% Author: Frank Wilhelm, Stanford University, Tel.(650)-858 3914
% Date: 3-9-2003
% Copyright (c) 2003 by Frank Wilhelm


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



useold=0;     % use old detection algorithm
spikeyes=0;   % removal of spike artifacts
back=6;       % 6=.24 sec back
rise=4;       % 4; plateau condition: rise from back !
	      % a higher value makes the plateau criterion looser
%set in pdaco: riseset=1;    % set rise criterion manually
%set in pdaco: edityes=0;    % load edited data, if exist
resplead=2;   % respiration leads by "resplead" sec

% not so important settings
min_inter=8;  % 8; minimum pCO2 between 2 maxima needs to be greater
area=38;      % 38 area of segmenting into matrix
mingap=area;  % minimal gap between maxima
absmin=12;    % absolute minimum
maxblen=12;   % maximum interbreath interval length, if longer: insert NaN value after maxblen/2


% Initialization
markyes=0;
rateyes=0;
artbegin=[];
artend=[];
artinterval=[];

if riseset
    risei=input(['Plateau criterion [',int2str(rise),'] (higher number is looser criterion) ==> ']);
    if ~isempty(risei)
        rise=risei;
    end
end

if exist('R')==1
    R=(R-mean(R))/1000+.5;
    R=cut_fill(R,length(y));
    plotresp=1;
    l=length(R);
    r=resplead*sr;
    R=[ R(1)*ones(r,1); R(1:l-r) ];
else
    plotresp=0;
end

% new procedures for artifacts in recording
if spikeyes
    disp('Removal of spike artifacts');
    y=spikere3(y,1,3);
    %y=nan_lip(y);
    %y=filthigh(y,25,200,3);
end;

% lowpass filter
y=filthigh(y,2,sr,5);
y=y(:);
COf=y;


if useold
    detect1;
else
    psyco3;
end


if  (invalid_segment~=1)

    %*** Load existing editing data
    if edityes
        chd(red_dir);
        file=[subject,twostr(filenum),'o'];
        loadok;
        if ~ok
           disp('Edited data of this interval not found. This is the first editing.');
        else
           disp('Loading edited data.');
           mt= CO2t(:);
           mv=CO2(:);
           n=find(isnan(mt)|isnan(mv));
           mt(n)=[]; mv(n)=[];
        end
    end

    rtn=mt(:);
    rtnd=mv(:);

    % insert NaN values if interbreath intervals too long
    d=diff(rtn);
    i1=find(d>maxblen*sr);
    i1=[i1;length(rtn)];
    rtn2=rtn;
    rtnd2=rtnd;
    if ~isempty(rtn2)
        for i=1:length(i1)
            rtn2 =insert(rtn2,(rtn2(i1(i))+maxblen/2*sr),i1(i)+1);
            rtnd2=insert(rtnd2,NaN,i1(i)+1);
            i1=i1+1;
        end
    end

    %*** Plot summary
    if bno|bplotyes

        figure(3)
        clf
        if ~isempty(rtn2)
            subplot(2,1,1)
            plot(resp_time(1:length(resp_time)-1)/sr/60,60./(diff(resp_time(:))/sr))
            xlabel('min');
            ylabel('breath/min');
            title('breathing rate');
            subplot(2,1,2)
            plot(rtn2/sr/60,rtnd2)
            axisy(0,max(nanrem(rtnd2))+2);
            xlabel('min')
            ylabel('mm Hg')
            title('End-tidal pCO2 before editing  -  ')
            drawnow
        end

    end
    % if bno|bplotyes

    %****** Inspect and/or exclude outliers loop
    if bno
        inspectloop=1;

        while inspectloop

            if plotyes1

                % insert NaN values if interbreath intervals too long
                d=diff(rtn);
                i1=find(d>maxblen*sr);
                i1=[i1;length(rtn)];
                rtn2=rtn;
                rtnd2=rtnd;
                if ~isempty(rtn2)
                    for i=1:length(i1)
                        rtn2 =insert(rtn2,(rtn2(i1(i))+maxblen/2*sr),i1(i)+1);
                        rtnd2=insert(rtnd2,NaN,i1(i)+1);
                        i1=i1+1;
                    end
                end

                figure(2)
                clf
                plot(rtn2/sr/60,rtnd2)
                axisy(0,max(nanrem(rtnd2))+2);
                title(['End-tidal pCO2  ','    Mean pCO2 = ',num2str(mean(nanrem(rtnd))),' mmHg']);
                xlabel('min')
                ylabel('mmHg');
                if markyes
                    plotline(M2,'m',.2);
                end;
                drawnow

                m1='Exit editing';
                m2='Editing of pCO2 plateaus';
                m3='Exclude outliers';
                m4='Start again';
                m5='Display segment';

                outyes=menu('Please select pCO2 editing option:',m1,m2,m3,m4,m5);
            else
                outyes=2;
                inspectloop=0;
            end

            % plotyes2

            if ~outyes
                return
            end;

            if outyes<2
                inspectloop=0;
            end;

            if outyes==2
                ax=axis;
                x=[];
                hold on
                disp(' ');
                disp('Click twice to mark suspicious intervals, end with <0>')
                disp('In EXAM: goto marked intervals with <0>');
                disp('Delete false plateaus with <#>');
                disp('Insert correct plateaus with <?> or <!>');
                disp('Mark begin <b> and end <e> of artifact, remove with <->');

                while 1
                    if plotyes1
                       [i1,i2,i3]=ginput(1);
                       if i3==48
                            if length(x)/2==floor(length(x)/2)
                                break;
                            else
                                disp(' ');
                                disp('Odd number of lines.  Please click once more!');
                                title('Odd number of lines.  Please click once more!');
                                [i1,i2,i3]=ginput(1);
                            end
                       end
                       x=[x i1];
                       plot([i1;i1],[ax(3);ax(4)],':c');   % display vertical line
                    else
                        x=[0 60];
                        return
                    end
                end

                %while
                hold off
                x=x*sr*60;
                lookat=reshape(x,2,length(x)/2)';
                figure(1); clg
                clear var3 var4
                defblank;
                int=60; moveint=60; samplerate=sr;
                var1=y; title1str =''; ltv1='y-'; yaxisstr='mm Hg';
                ev1varyes=2; % display values with 2 digits
                if plotresp==1
                    var2=R; secvar2=1; subvarplot=1; title2str =''; ltv2='c-';
                    ev1valyes=2;  % display 2 digit pCO2 values
                else
                    subvarplot=0;
                end
                if (co2_switch==1 & breath_switch==0)
                    event1=rtn; event1yes=1;
                elseif(co2_switch==0 & breath_switch==1)
                    event2=resp_time; event2yes=1; event1yes=0;
                elseif(co2_switch==1 & breath_switch==1)
                    event1=rtn; event1yes=1;
                    event2=resp_time-6; event2yes=1;
                else
                    event1=rtn; event1yes=1;
                end
                if markyes
                    event2=M2*sr; event2yes=1;
                    evscan=event2; skiploc=.5;
                else
                    evscan=event1; skiploc=.7;
                end
                yaxisstr='mmHg';
                %RESPEDIT=[];  %indicates plot of circles in exam
                def
                if plotresp==1
                    radapt=0;
                    rangetwo=0;
                end
                exam;

                %*** add values specified from inside exam  (option @)
                if co2_switch==1
                   rtn=event1;
                   n=find(isnan(rtnd));
                   rtnd=var1(round(event1)); %
                   rtnd(n)=NaN*ones(size(n));
                   y=var1;
                end

                clear var1 var2

                if exist('artinterval')
                    if ~isempty(artinterval)
                       disp('Exclusion of values within specified artifact intervals');
                       setmiss=between(rtn,artinterval);
                       if ~isempty(setmiss)
                          rtnd(setmiss)=ones(1,length(setmiss))*NaN;
                          disp([int2str(setmiss),' values were set to missing.']);
                       end
                    end
                end

                inspectloop=1;
                if ~plotyes1
                    inspectloop=0;
                end
            end

            if outyes==3
               [rtnd,n] = outrect(rtnd,rtn/(sr*60),1);
               rtnd(n)=NaN*ones(size(n)); %rtn(n)=[];
               inspectloop=1;
            end

            if outyes==4
                rtn=mt(:);
                rtnd=mv(:);
                inspectloop=1;
            end

            if outyes==5
               figure(2)
               title('Click and hold left mouse button to zoom repeatedly. Right to undo. <enter> to quit.');
               zoomrb
               pause
            end

        end

        %1****** inspect data for artifacts loop

        %*** Give quality rating
        if rateyes
            disp(['Existing rating = ',int2str(rat)]);
            rating=menu('Signal quality rating','1 = mean CO2 not reliable','2 = many erratic breaths','3 = some erratic breaths, edited out','4 = little editing done','5 = excellent','6 = existing rating');
            if rating~=6
                rat=rating;
            end;
        else
            rat=0;
        end
    end
    %***if bno


    % insert NaN values if interbreath intervals too long
    d=diff(rtn);
    i1=find(d>maxblen*sr);
    i1=[i1;length(rtn)];
    rtn2=rtn;
    rtnd2=rtnd;
    if ~isempty(rtn2)
        for i=1:length(i1)
            rtn2 =insert(rtn2,(rtn2(i1(i))+maxblen/2*sr),i1(i)+1);
            rtnd2=insert(rtnd2,NaN,i1(i)+1);
            i1=i1+1;
        end
    end


    % Conversion
    CO2t=rtn(:);
    CO2=rtnd(:);
    disp('Conversion into instantaneous data');
    CO20=epoch(rtn2,rtnd2,sr/ep);
    b=resp_time(:);
    bd=diff(b);  % problem with doubles
    c=find(~bd);
    b(c)=[];
    bd=difffit(b);
    RR0=epoch(b, 60 ./(bd/sr), sr/ep);
    nn=max([length(CO20),length(RR0)]);
    RR0=cut_fill(RR0,nn);
    CO20=cut_fill(CO20,nn);

    if 0
        figure
        t=(1:length(CO20))/ep;
        plot(t,CO20)
        pause
    end

end

