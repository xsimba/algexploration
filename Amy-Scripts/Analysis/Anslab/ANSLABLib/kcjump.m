% kcjump.m   jump to copy number : <shift>+j

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
j=length(rems1);
disp(' ');
disp(['The last copy number was ',int2str(remi)]);
disp(['The max. copy number is  ',int2str(j)]);
i=input('Jump to copy number  ==>  ');
if i<=j & i>0
  s1=rems1(i); s2=rems2(i);
  remi=i; recallnumyes=1;
else
  disp('Event number is out of range.');
  plotyes=0;
end; z=999;
