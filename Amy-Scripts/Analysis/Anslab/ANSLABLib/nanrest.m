function x=nanrest(x,ind);
% function y = nanrest (x,ind);
% Fills NaN-values into the old vector at the locations specified by ind.
% [y,ind] are the output of the function nanrem(y).
% The new vector is therefore a restored version of y, normally longer.


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if length(ind)

n=find(~isnan(x));
if length(n)
ii=1;

for i=1:length(x)+length(ind)

if i==ind(ii)
   x(i+1:length(x)+1)=x(i:length(x));
   x(i)=NaN;
   ii=ii+1;
   if ii>length(ind) break; break; end;
end;

end;

else   % only NaNs
x=ones((length(x)+length(ind)),1)*NaN;
end;

end;