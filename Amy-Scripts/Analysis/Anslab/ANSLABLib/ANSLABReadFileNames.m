function[NFiles,FileMatNew,VersionString,BatchFilePath,FileMatNotExisting]=ReadFileNames(FileMat,filterSpec,dialogTitle,x,y,PrintStatus,ChFilterSpecPathStatus,MaxNFiles);

%   ReadFileNames

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<8; MaxNFiles=inf; end
if nargin<7; ChFilterSpecPathStatus=1; end
if nargin<6; PrintStatus=1; end
if nargin<5; y=0; end
if nargin<4; x=0; end
if nargin<3; dialogTitle=[]; end
if nargin<2; filterSpec=[]; end
if nargin<1; FileMat=[]; end

if isempty(MaxNFiles); MaxNFiles=inf; end
VersionString=version;
if ~isempty(FileMat);
    FileMatNew=FileMat;
    NFiles=size(FileMat,1);
    [NonUse,NonUse,BatchFilePath]=GetFileNameOfMat(FileMat,1);
    BatchFilePath = [];
    return;
end
if isempty(x); x=0; end
if isempty(y); y=0; end
if isempty(PrintStatus); PrintStatus=1; end

OneMoreFile=1;
FileIndex=0;
LineIndex=0;
IgnoreAllStatus=0;
NNotExistFiles=0;
ExistStatusTmp=[];
DefBatchPath=[];
FileMatNotExisting=[];
if isempty(filterSpec)
    filterSpec=SetDefPath(1,'*');
end
while OneMoreFile & FileIndex<MaxNFiles
    FileIndex=FileIndex+1;
    if FileIndex==1
        if PrintStatus; fprintf('\n\n'); fprintf(1,'Choose a first data or batch file (use e.g. Dir2Batch):\n\n\n'); end
        if isempty(dialogTitle);
            if MaxNFiles>1
                dialogTitleTmp=['Choose a first data or batch file (use e.g. Dir2Batch):'];
            else
                dialogTitleTmp=['Please choose a data file:'];
            end
        else;
            dialogTitleTmp=dialogTitle;
        end
        [File,Path]=uigetfile(filterSpec,dialogTitleTmp);
        pause(.1)
        if FileIndex==1 & File==0
            NFiles=0;
            FileMatNew=[];
            BatchFilePath=[];
            return;
        end
        BatchFilePath=[Path,File];
        SetDefPath(2,Path);
        if MaxNFiles>1
            fid=fopen(BatchFilePath,'r','b');
            NFiles=0;
            ExistStatus=1;
            while feof(fid)==0 & ExistStatus~=0;    %Check if file is a batch file with existing files
                NFiles=NFiles+1;
                ExistStatus=1;
                ActFid=ftell(fid);
                FseekStatus=fseek(fid,0,'eof');
                Eof=ftell(fid);
                FseekStatus=fseek(fid,ActFid,'bof'); %rewind
                CharTest=fread(fid,'char',min([100 Eof])); %Test for binary; if binary => no batch file
                if all(CharTest>0 & CharTest<123)     %if is not binary
                    FseekStatus=fseek(fid,ActFid,'bof'); %rewind
                    FilePath=deblank(fgetl(fid));
                    LineIndex=LineIndex+1;
                    if isempty(FilePath);
                        FilePath=['NoEntry'];
                        fprintf(1,'No entry at line %g\n',NFiles);
                        NFiles=NFiles-1;
                    end
                else                                %if file is binary => data file
                    FilePath=BatchFilePath;
                    ExistStatus=0;                  %ExistStatus means File in batch file does not exist
                end                                 %test for binary
                if strcmp(FilePath(1),'*')  %if line in batch file begins with a *, DefBatchPath is taken from "DefaultBatchPath.txt"
                                            %helpfull if data is stored on external harddrives
                    if isempty(DefBatchPath)
                        %=================================================================
                        [nouse1,Plot2dFolder]=SepFilePath(which('emegs2d.m'));
                        DefFilePath = [Plot2dFolder,'emegs2dUtil',filesep,'ViewDefault',filesep,'DefaultBatchPath.txt'];
                        DefFilePathFid = fopen(DefFilePath,'r','b');
                        if DefFilePathFid==-1
                            fprintf(1,'The file:\n\n')
                            disp(DefFilePath); fprintf(1,'\n\n');
                            fprintf(1,'does not exist or can\n\n')
                            fprintf(1,'not be opened for reading.\n\n')
                        else
                            DefBatchPath=fscanf(DefFilePathFid,'%s',1);
                        end
                        fclose(DefFilePathFid);
                        %===========================================================
                    end
                    FilePath=[DefBatchPath,FilePath(2:end)];
                end
                if ~strcmp(FilePath,BatchFilePath)  %if file is not binary check for existance
                    TestFid=fopen(FilePath,'r','b');
                    if TestFid==-1
                        ExistStatus=0;
                    else
                        fclose(TestFid);
                        disp(FilePath)
                        ExistStatus=1;
                    end
                else                                %if file is binary
                    fprintf(1,'Take data file:\n\n')
                    disp(FilePath); fprintf(1,'\n\n');
                end                                 % end of if file is not binary check for existance
                if ExistStatus==0 & NFiles==1 & PrintStatus
                    clc;
                    fprintf(1,'The file:\n\n')
                    disp(FilePath); fprintf(1,'\n\n');
                    fprintf(1,'does not exist or can\n\n')
                    fprintf(1,'not be opened for reading.\n\n')
                    fprintf(1,'The file\n\n')
                    fprintf(1,'%c',BatchFilePath); fprintf(1,'\n\n');
                    fprintf(1,'seems not to be batch file !\n\n');
                    BatchFilePath = [];
                end
                if ExistStatus==0 & NFiles>1 & PrintStatus & ~strcmp(FilePath,'NoEntry')
                    fprintf(1,'The file\n\n')
                    disp(FilePath); fprintf(1,'\n\n');
                    fprintf(1,'does not exist or can\n\n')
                    fprintf(1,'not be opened for reading.\n\n')
                    if ~IgnoreAllStatus
                        Message=char(['The file in batch line ',int2str(LineIndex)]);
                        Message=char(Message,'');
                        Message=char(Message,FilePath);
                        Message=char(Message,'');
                        Message=char(Message,'does not exist or can not be opened for reading.');
                        Message=char(Message,'');
                        ButtonName=questdlg(Message,'','Ignore','Ignore all','Cancel','Ignore');
                        %uiwait
                    else
                        ButtonName='Ignore';
                    end
                    switch ButtonName,
                        case 'Ignore',
                            disp(['Ignore file: ',FilePath,' and continue ...']);
                            NFiles=NFiles-1;
                            NNotExistFiles=NNotExistFiles+1;
                            if NNotExistFiles==1;
                                FileMatNotExisting=char(FilePath);
                            else
                                FileMatNotExisting=char(FileMatNotExisting,FilePath);
                            end
                        case 'Ignore all',
                            disp(['Ignore file: ',FilePath,' and continue ...']);
                            IgnoreAllStatus=1;
                            NFiles=NFiles-1;
                        case 'Cancel',
                            disp('Cancel and Return')
                            NFiles=0; FileMatNew=[]; BatchFilePath=[];
                            return;
                    end % switch
                end
                if ExistStatus
                    if NFiles==1
                        FileMatNew=char(FilePath);
                    else
                        FileMatNew=char(FileMatNew,FilePath);
                    end
                end
                if NFiles>1 & ~ExistStatus
                    ExistStatus=1;
                end
            end %while feof(fid)==0 & ExistStatus~=0;
        else    %if MaxNFiles==1
            ExistStatus=1;
            NFiles=1;
            FilePath=BatchFilePath;
            FileMatNew=char(FilePath);
        end
        if ExistStatus~=0 & PrintStatus
            fprintf('\n\n'); clc;
            disp(FileMatNew)
            File = 0;
            FileIndex = size(FileMatNew,1)+1;
            return;
        end
    else    %if FileIndex>1
        if PrintStatus; fprintf('\n\n'); fprintf(1,'Please choose the %g  file or press "Cancel" to finish entry: ',FileIndex); end
        if isempty(dialogTitle);
            dialogTitleTmp=['Choose the ',int2str(FileIndex),' file or press "Cancel" to be done:'];
        else;
            dialogTitleTmp=dialogTitle;
        end
        [File,Path]=uigetfile(filterSpec,dialogTitleTmp);
    end
    if ChFilterSpecPathStatus & File~=0;
        SepStrVec=findstr(filterSpec,filesep);
        if ~isempty(SepStrVec)
            filterSpec=[Path,filterSpec(SepStrVec(length(SepStrVec))+1:length(filterSpec))];
        else
            filterSpec=[Path,filterSpec];
        end
    end
    if File==0
        OneMoreFile=0;
        NFiles=FileIndex-1;
    else
        FilePath=[Path,File];
        if FileIndex==1
            FileMatNew=char(FilePath);
        else
            FileMatNew=char(FileMatNew,FilePath);
        end
    end
    if PrintStatus;
        fprintf('\n\n'); clc;
        disp(FileMatNew);
    end
end
fclose(fid);
if NFiles==0; FileMatNew=[]; end
NFiles=size(FileMatNew,1);
return;
