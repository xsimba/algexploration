% edevnum.m  display event number

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

valx=(s1+(s2-s1)*skiploc)/scalefact;
str=num2str(evscani);

if comptype<2
   cmdstr=['text(''Position'',[valx yax2-as],''String'',''',str,''')'];
else
   cmdstr=['text(valx,yax2-as,''',str,''')'];
end;
eval(cmdstr);
j=length(evscan);
if evscani<(j-1)
   iv=(evscan(evscani+1)-evscan(evscani))/samplerate;
   ivmin=floor(iv/60);
   ivsec=iv-ivmin*60;
   str1=([num2str(ivmin),':',twostr(ivsec),'min  marked interval']);
   str2=[]; str3=[]; str4=[];
 if evscani<(j-2)
    iv=(evscan(evscani+2)-evscan(evscani+1))/samplerate;
    ivmin2=floor(iv/60);
    ivsec2=iv-ivmin2*60;
    str2=(['    Next=',num2str(ivmin2),':',twostr(ivsec2),'min']);
  if evscani<(j-3)
     iv=(evscan(evscani+3)-evscan(evscani+2))/samplerate;
     ivmin3=floor(iv/60);
     ivsec3=iv-ivmin3*60;
     str3=([', ',num2str(ivmin3),':',twostr(ivsec3),'min']);
  if evscani<(j-4)
     iv=(evscan(evscani+4)-evscan(evscani+3))/samplerate;
     ivmin4=floor(iv/60);
     ivsec4=iv-ivmin4*60;
     str4=([', ',num2str(ivmin4),':',twostr(ivsec4),'min']);
  end;
  end;
 end;

xlabel([str1 str2 str3 str4]);
end;

evnumyes=0;
