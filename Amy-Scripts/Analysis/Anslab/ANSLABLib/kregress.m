% Ei_regress.m   regression of breathing procedure: B   (with spirometer)
% Spirometer needs to be already scaled correctly
% pred1=abd, pred2=tho, crit=spir

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


format compact;
betasucc=[];
newtake=1;

%1*** start with new best ratio breaths
while newtake

if 1
%test
pred1=[  12   8   10   15  14   7  30  23  34  35  28];
pred2=[  30  23   34   35  28  12   8  10  15  14   7];
crit= [1000 800 1100 1200 950 400 800 600 900 900 600];

else

pred1= BM(1,:) - BMIN(1,:);        % predictors
pred2= BM(2,:) - BMIN(2,:);
crit = abs(BM(3,:) - BMIN(3,:));    % spirometer

% exclude existing 0 tidal volume breaths
n=find(crit<50);
pred1(n)=[]; pred2(n)=[]; crit(n)=[];

end;

% repeat exclusion of outliers with rectangular
rect=1;
while rect
figure(3); clg;

% plot points and regression line
x=pred1./crit;
y=pred2./crit;
p=polyfit(x,y,1)
yfit=p(1)*x + p(2);
cc=corrcoef(pred1./crit,pred2./crit);
b2=1/p(2);
b1=1/(-p(2)/p(1));
plot(x,y,'o');
title(['corr= ',num2str(rndv(cc(1,2))),'  beta1=',num2str(rndv(b1)),'  beta2=',num2str(rndv(b2))]);
ylabel('tho/spi');
xlabel('abd/spi');
hold on
plot(x,yfit,'m')
hold off

% number the breathing cycles on plot
for i=1:length(x)
str=int2str(i);
cmdstr=['text(''Position'',[x(i) y(i)],''String'','' ',str,''')'];
eval(cmdstr);
end;

pause

% equal scales on x- and y-axis to see absolute regression
%n=axis;
%i=max([x(:);y(:)])*1.02;
%axis([0 i 0 i]);
%pause
%axis(n)

m1='Accept';
m2='Remove outliers';
rect=menue(m1,m2)-1;
if ~rect break; end;


% rectangular for outliers
disp('Define rectangular to exclude breaths with 2 clicks');
t=1:length(x);
[i1,i2,i3]=ginput(2);
i1=sort(i1);
i2=sort(i2);
n=find(x>i1(1) & x<i1(2) & y>i2(1) & y<i2(2)); % interval
disp([int2str(length(n)),' breaths were excluded.']);

pred1(n)=[]; pred2(n)=[]; crit(n)=[];

end;
%*** while rect


if 0
pt=input('How many breaths were in "predominantly thoracic" condition [10] ==> ');
if isempty(pt) pt=10; end;

rat=pred1./pred2;
figure(4)
t=1:length(pred1);
med=median(rat);
plot(t,rat,'*');
hold on
plot([1;length(rat)+1],[med;med],'b');
plot([pt+.5;pt+.5],[min(rat);max(rat)],'b');
hold off

n=input('Take how many cycles per condition with the most extreme abd/tho-ratio [6] ==> ');
if isempty(n) n=6; end;

rat1=rat(pt+1:length(rat));
i1=max(rat1);
range=max(rat1)-min(rat1);
while 1
  i1=i1-.01*range;
  maxn=find( rat1 > i1 );
  if length(maxn) >= n break; end;
end;
maxn=maxn+pt;

rat2=rat(1:pt);
i2=min(rat2);
range=max(rat2)-min(rat2);
while 1
  i2=i2+.01*range;
  minn=find( rat2 < i2 );
  if length(minn) >= n break; end;
end;

take=[minn maxn];
hold on
plot(t(take),rat(take),'*r');
plot(t(take),rat(take),'or');
hold off
pred1=pred1(take);
pred2=pred2(take);
crit=crit(take);

end;  %if 0



E=[pred1' pred2' crit'];

j=1;


%2*** take cycles out of equation
while j~=0

% correlation matrix
i=size(E);
e=ones(i(1),1);
X=E-e*mean(E);
X=X ./ (e*std(X));   % normalized data
corr=X'*X/(length(e)-1);   % corrcoef
disp('      abd       tho       spi');
disp(corr);

% regression
x=E(:,1)./E(:,3);
y=E(:,2)./E(:,3);
p=polyfit(x,y,1)
yfit=p(1)*x + p(2);
cc=corrcoef(pred1./crit,pred2./crit);
b2=1/p(2);
b1=1/(-p(2)/p(1));
fit=b1*E(:,1)+b2*E(:,2);
beta=[b1;b2];

if 0
% Alternative method
A=[E(:,1) E(:,2)];
beta=A\E(:,3);
fit=A*beta;
end;

fit_error=(E(:,3)-fit);
fit_error_rel=fit_error ./E(:,3);
[max_error1,maxi1] = max( abs( fit_error ) );
[max_error2,maxi2] = max( abs( fit_error_rel ) );
max_error2=max_error2*100;

% betas assuming one channel is missing
A=[E(:,1) zeros(size(E(:,1)))];
beta1=A\E(:,3);
fit1=A*beta1;
A=[zeros(size(E(:,2))) E(:,2)];
beta2=A\E(:,3);
fit2=A*beta2;

disp('Approximations for beta if one respiration channel is missing:');
disp(['For only Resp1 valid: beta=',num2str(rndv(beta1(1)))]);
disp(['For only Resp2 valid: beta=',num2str(rndv(beta2(2)))]);
disp('History of beta-weights for 2 valid channels:');

betasucc=[betasucc beta]
disp([int2str(length(e)),' remaining valid breaths']);
disp(['Least abs. fit: ',int2str(maxi1),'. Error=',num2str(max_error1),'ml']);
disp(['Least rel. fit: ',int2str(maxi2),'. Error=',num2str(max_error2),'%']);

figure(4); clg;
t=1:length(e);
plot(t,E(:,3),'oy',t,fit,'-y',t,fit1,':r',t,fit2,':b');
title('o = Spir, Estimation based on Resp: 1+2 (yellow), 1 (red), 2 (blue)');
ylabel('tidal volume [ml]');

%for i=1:length(e)   % display absolute fit errors on plot
%    text( i , E(i,3) , int2str( fit_error(i) ) );
%end;

figure(3); clg;
axis('auto');
x=E(:,1)./E(:,3);
y=E(:,2)./E(:,3);
plot(x,y,'o');
p=polyfit(x,y,1)
yfit=p(1)*x + p(2);
cc=corrcoef(x,y);
b2=1/p(2);
b1=1/(-p(2)/p(1));
plot(x,y,'o');
title(['corr= ',num2str(rndv(cc(1,2))),'  beta1=',num2str(rndv(b1)),'  beta2=',num2str(rndv(b2))]);
ylabel('tho/spi');
xlabel('abd/spi');
hold on
plot(x,yfit,'m')
hold off

% number the breathing cycles on plot
for i=1:length(x)
str=int2str(i);
cmdstr=['text(''Position'',[x(i) y(i)],''String'','' ',str,''')'];
eval(cmdstr);
end;

j=input('Take out which cycle? (99=Rectangle; 0=end)  ');
if ~j  break; break; end;
if j~=99  E(j,:)=[]; end;
if j==99
disp('Define rectangular to exclude breaths with 2 clicks');
t=1:length(x);
[i1,i2,i3]=ginput(2);
i1=sort(i1);
i2=sort(i2);
n=find(x>i1(1) & x<i1(2) & y>i2(1) & y<i2(2)); % interval
disp([int2str(length(n)),' breaths were excluded.']);
E(n,:)=[];
end;

end;
%2*** take out cycles

m1='Accept';
m2='Start new';
newtake=menue(m1,m2)-1;
if newtake<1 break; break; break; end;

end;
%1*** takenew

plotyes=0;

