% klag.m   examine the lag between 2 time-series

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



if spec
count=0;
while 1   % esc
[i1,i2,i3]=ginput(1);
if i3==27 break; break; end;   % esc

lag = (i2/360) * (1/i1);

count=count+1;
n=axis;
x=n(1)+(n(2)-n(1))*.65;
as=(n(4)-n(3))*.08;
str=['Var1 lags ',num2str(lag),' sec behind Var2'];
eval(['text(x , n(4) - count*as,''',str,''')']); %other axis
plotyes=0; z=999;
end;
end;


