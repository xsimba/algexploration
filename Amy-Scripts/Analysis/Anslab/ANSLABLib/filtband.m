function y2=filtband(y1,freq,freq2,samplerate,order);
% function y2=filtband(y1,freq,freq2,samplerate,order)
% Band-pass filter with corner Fs of freq and freq2.
% Default order = 5.

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<5 order=5; end;
if nargin<4 samplerate=2; end;

freq=sort([freq freq2]);
freq=freq ./(samplerate/2);
[b,a]=butter(order,freq');
y2=filtfilt(b,a,y1);
