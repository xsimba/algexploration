function [Data,Channels] = loadjim(fname,numchannels);
% load sample t1 to t2 (default=start-end) from binary file fname in current directory
% load with 16 bits precision in signed integer format
% Read in header information and data of DTVEE binary file: only one channel
%
% A/D board: +/- 2.5 Volts, 12 bit resolution
% 500     ECG (1.0 V)
%   0.976 SCL (10.0 uS; 0.249 uS)
%   0.976 SCR (1.0 uS; 0.0026 uS)
%   1     FP (1.0)
%   1     EP (1.0)
%   1     Temp (9; 81.5)
%   1.000 Activity (1.0)
%   1.000 Resp1 tho (1.0)
%   1.000 Resp2 abd (1.0)
%  0.01   BP (1.0)
%  1. (A/D-2048)/4096 X 5 volts full scale = A/D volts
%  2. Divide A/D volts by gain in first column (e.g., 1.0 for TEMP)
%  3. Multiple by first number in parentheses (e.g., 9 for TEMP)
%  4. Add by second number in parentheses (e.g., 81.5 for TEMP), which is an
%     empirically determined offset.



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

t2=inf;
t1=1;
prompt={'How many channels are in the file?'};
name='Number of channels';
numlines=1;
defaultanswer={'10'};
answer=inputdlg(prompt,name,numlines,defaultanswer);
numchannels = str2num(answer{1});
% Data specification
%chan=1;    % Channel to load and display  (1-10)
%fname='jon01.dat';

% Read header info
[fid,message]=fopen(fname,'rb','l');
if fid<0
    disp(message);
    return
end;
hdlen=fread(fid,1)+fread(fid,1)*256;
h=fread(fid,hdlen-2);
%version=h(2)+h(1)*256;
% boardname=setstr(h(3:42))';
% subsysname=setstr(h(43:82))';
% datawidth=h(84)+h(83)*256;
% resolution=h(86)+h(85)*256;
% encoding=h(88)+h(87)*256;
% minvolts=sum( h(89:96)'  .* ((ones(1,8)*256) .^(0:7))  ) ;
% maxvolts=sum( h(97:104)' .* ((ones(1,8)*256) .^(0:7))  ) ;
% numchannels=h(105)+h(106)*256;
% frequency=sum( h(107:114)' .* ((ones(1,8)*256) .^(7:-1:0))  ) ;
% timedate=sum( h(115:122)'  .* ((ones(1,8)*256) .^(7:-1:0))  ) ;

% Read channel info
fseek(fid,hdlen-4*numchannels,'bof');
hh=fread(fid,4*numchannels,'char');
% for i=1:numchannels
%     channel(i) = hh(i*4-2)*256 + hh(i*4-3);
%     gain(i)    = hh(i*4-0)*256 + hh(i*4-1);
% end;
DataStart = ftell(fid);
% Read in data for one channel
for i=1:numchannels
    fseek(fid,DataStart,'bof');
    status=fseek(fid,(i-1)*2+0*numchannels*2,0);
    if i==1
        DataTmp = fread(fid,inf,'short',(numchannels-1)*2);
        NPoints = size(DataTmp,1);
        Data = zeros(numchannels,NPoints);
        Data(i,:)= DataTmp';
    else
        DataTmp = fread(fid,inf,'short',(numchannels-1)*2);
        Data(i,:) = DataTmp;
    end
end
Channels = [];
for LabelInd = 1:numchannels
    if LabelInd ==1
        Channels = num2str(LabelInd);
    else
        Channels = char(Channels,num2str(LabelInd));
    end
end
fclose(fid);

