function HDR = CreateAcqDefaultHeader(Type)

% 	function HDR = CreateAcqDefaultHeader(Type)

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<1;Type = [];end
if isempty(Type);Type = 1;end


if Type == 1  % 1 channel 25 Hz

    UtilPath1 = strrep(which('CreateAcqDefaultHeader.m'),'.m','1.mat');
    load(UtilPath1);
    HDR = HDR1;

elseif Type == 2  % all channels 25 Hz

    UtilPathAll = strrep(which('CreateAcqDefaultHeader.m'),'.m','All.mat');
    load(UtilPathAll,'HDRAll');
    HDR = HDRAll;

end



return