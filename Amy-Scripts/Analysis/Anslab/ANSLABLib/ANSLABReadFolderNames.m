function[NPaths,PathMatNew,VersionString,BatchFilePath]=ReadFolderNames(PathMat,filterSpec,dialogTitle,x,y,PrintStatus,ChFilterSpecPathStatus,MaxNFiles);
%	ReadFolderNames

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


if nargin<8; MaxNFiles=inf; end
if nargin<7; ChFilterSpecPathStatus=1; end
if nargin<6; PrintStatus=1; end
if nargin<5; y=0; end
if nargin<4; x=0; end
if nargin<3; dialogTitle=[]; end
if nargin<2; filterSpec=[]; end
if nargin<1; PathMat=[]; end

if isempty(MaxNFiles); MaxNFiles=inf; end
VersionString=version;
if ~isempty(PathMat);
    PathMat=PathMat;
    NFiles=size(PathMat,1);
    [NonUse,NonUse,BatchFilePath]=GetFileNameOfMat(PathMat,1);
    return;
end
if isempty(x); x=0; end
if isempty(y); y=0; end
if isempty(PrintStatus); PrintStatus=1; end

OneMoreFile=1;
BatchFileStatus = 1;
FileIndex=0;
SepPathStr=filesep;
ExistStatusTmp=[];
if isempty(filterSpec)
    filterSpec=SetDefPath(1,'*');
end
while OneMoreFile & FileIndex<MaxNFiles
    FileIndex=FileIndex+1;
    if FileIndex==1
        if PrintStatus; fprintf('\n\n'); fprintf(1,'Please choose a batch file (use e.g. Dir2Batch)  or press cancel:\n\n\n'); end
        if isempty(dialogTitle);
            if MaxNFiles>1
                if FileIndex==1
                    dialogTitleTmp=['Please choose a batch file (use e.g. Dir2Batch) or press cancel:'];
                else
                     dialogTitleTmp=['Please choose a directory:'];
                end

            else
                dialogTitleTmp=['Please choose a directory:'];
            end
        else;
            dialogTitleTmp=dialogTitle;
        end
        if FileIndex==1
            [File,Path]=uigetfile(strrep(filterSpec,'*','\*.*'),dialogTitleTmp);
            BatchFilePath = [Path,File];
        else
            [Path]=uigetdir(filterSpec,dialogTitleTmp);
            BatchFilePath=[Path];
        end
        if FileIndex==1 & Path==0

            dialogTitleTmp=['Please choose a directory:'];
            [Path]=uigetdir(filterSpec,dialogTitleTmp);
            BatchFilePath=[Path];
            BatchFileStatus = 0;
            if Path==0
                NPaths=0;
                PathMatNew=[];
                BatchFilePath=[];
                return;
            end
        end
        SetDefPath(2,Path);
        if MaxNFiles>1
            if BatchFileStatus==1
                fid=fopen(BatchFilePath,'r','b');
                NFiles=0;
                ExistStatus=1;
                while feof(fid)==0 & ExistStatus~=0;
                    NFiles=NFiles+1;
                    ExistStatus=1;
                    FilePath=deblank(fgetl(fid));
                    Test=exist(FilePath);
                    if Test~=7
                        ExistStatus=0;
                    else
                        disp(FilePath)
                    end
                    if ExistStatus==0 & NFiles==1 & PrintStatus
                        clc;
                        fprintf(1,'The directory:\n\n')
                        disp(FilePath); fprintf(1,'\n\n');
                        fprintf(1,'does not exist or can\n\n')
                        fprintf(1,'not be opened for reading.\n\n')
                        fprintf(1,'The file\n\n')
                        fprintf(1,BatchFilePath); fprintf(1,'\n\n');
                        fprintf(1,'seems not to be batch file !\n\n')
                    end
                    if ExistStatus==0 & NFiles>1 & PrintStatus
                        fprintf(1,'The directory\n\n')
                        disp(FilePath); fprintf(1,'\n\n');
                        fprintf(1,'does not exist or can\n\n')
                        fprintf(1,'not be opened for reading.\n\n')
                        Message=char(['The directory in batch line ',int2str(NFiles)])
                        Message=char(Message,'')
                        Message=char(Message,FilePath)
                        Message=char(Message,'')
                        Message=char(Message,'does not exist or can not be opened for reading.')
                        Message=char(Message,'')
                        ButtonName=questdlg(Message,'','Ignore','Cancel','Ignore');
                        %uiwait
                        switch ButtonName,
                            case 'Ignore',
                                disp(['Ignore directory: ',FilePath,' and continue ...']);
                                NFiles=NFiles-1;
                            case 'Cancel',
                                disp('Cancel and Return')
                                NFiles=0; FileMatNew=[]; BatchFilePath=[];
                                return;
                        end % switch
                    end
                    if ExistStatus
                        if NFiles==1
                            PathMatNew=char(FilePath);
                        else
                            PathMatNew=char(PathMatNew,FilePath);
                        end
                    end
                    if NFiles>1 & ~ExistStatus
                        ExistStatus=1;
                    end
                end
            else
                ExistStatus=0;
            end

        else
            ExistStatus=1;
            NFiles=1;
            FilePath=BatchFilePath;
            PathMatNew=char(FilePath);
        end
        if ExistStatus~=0 & PrintStatus
            fprintf('\n\n'); clc;
            disp(PathMatNew);
            NPaths=size(PathMatNew,1);

            return;
        end
    else
        if PrintStatus; fprintf('\n\n'); fprintf(1,'Please choose the %g  path or press "Cancel" to be done: ',FileIndex); end
        if isempty(dialogTitle);
            dialogTitleTmp=['Choose the ',int2str(FileIndex),' directory or press "Cancel" to be done:'];
        else;
            dialogTitleTmp=dialogTitle;
        end

        filterSpec = SetDefPath(1);
        [Path]=uigetdir(strrep(filterSpec,'*',''),dialogTitleTmp);
        SetDefPath(2,Path);
    end
    if ChFilterSpecPathStatus & File~=0;
        SepStrVec=findstr(filterSpec,SepPathStr);
        if ~isempty(SepStrVec)
            filterSpec=[Path,filterSpec(SepStrVec(length(SepStrVec))+1:length(filterSpec))];
        else
            filterSpec=[Path,filterSpec];
        end
    end
    if Path==0
        OneMoreFile=0;
        NFiles=FileIndex-1;
    else
        NewPath=[Path];
        if FileIndex==1
            PathMatNew=char(NewPath);
        else
            PathMatNew=char(PathMatNew,NewPath);
        end
    end
    if PrintStatus;
        fprintf('\n\n'); clc;
        disp(PathMatNew);
    end
end
if NFiles==0; PathMatNew=[]; end
NPaths=size(PathMatNew,1);
return;
