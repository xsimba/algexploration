% kinterp

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


i3=1;

hold on;

[i1,i2]=ginput(2);
i1=round(i1); % i2=round(i2);
plot(i1,i2,'x');
y1(i1)=i2;
var1(i1)=i2;

len=i1(2)-i1(1)-1;
n=interp_l(i2(1),i2(2),len);
y1(i1(1)+1:i1(2)-1)=n;
var1(i1(1)+1:i1(2)-1)=n;

end;

hold off;
