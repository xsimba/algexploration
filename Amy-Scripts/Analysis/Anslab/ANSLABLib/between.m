function n = between (time,int);
%n = between (time,int);
% finds all times that are between (<>=) intervals
% intervals has to be a 2-columns vector (begins and ends of intervals)
% FW 5/16/95

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.
time=time(:);
[len,j]=size(int);
n = [];
for i=1:len
    j = find(  time>=int(i,1) & time<=int(i,2)  );

    n=[n;j];
end;

n=intersec(sort(n));

