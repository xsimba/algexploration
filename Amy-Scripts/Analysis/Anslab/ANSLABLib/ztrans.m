function [y,m,s]=ztrans(y);
%function [y,m,s]=ztrans(y);
% z-transformation (standardization) of vector or matrix y
% NaNs are removed


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


[rows,cols]=size(y);
[y1,ind]=nanrem(y(:));

m=mean(y1);
s=std(y1);
y1=(y1-m)/s;

y=nanrest(y1,ind);
y=reshape(y,rows,cols);


