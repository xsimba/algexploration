function [M,S]=meanbin(X,binsize,valperc,dispyes,detrendyes);
% function [M,S]=meanbin(X,binsize,valperc,dispyes,detrendyes);
% Each row of matrix X is divided into bins of length binsize
% and the mean and SD of these bins is returned in matrix M and S.
% If less than valperc percent of points in a bin are valid,
% the mean and SD of this bin are set to NaN [default=50];
% If dispyes specified: successive numbers of rows are displayed
%    to trace loop progress
% If detrendyes specified: bins are detrended for SD


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<5 detrendyes=0; end;
if nargin<4 dispyes=0; end;
if nargin<3 valperc=50; end;
if detrendyes disp('meanbin: detrending option is active'); end;

if ~binsize
M=NaN; S=NaN;
else

[r,c]=size(X);
if min([r c])==1
   nrow=1;
   if r>c X=X'; end;
else
   nrow=r;
end;

x=X(1,:);
bin=1:binsize;
segm=length(x)/binsize;
segmrem=rem(length(x),binsize);
b=floor(segm);

M=zeros(nrow,segm);
S=zeros(nrow,segm);

for row=1:nrow;

if dispyes disp([int2str(row),'/',int2str(nrow)]); end;

x=X(row,:);

for i=1:b
  xbin=x(bin+(i-1)*binsize);
  m=nanremf(xbin);
  if length(m)/binsize*100>=valperc
     y(i)=mean(m);
     if nargout>1
        if detrendyes m=detrend(m); end;
        s(i)=std(m);
     end;
  else
     y(i)=NaN;
     s(i)=NaN;
  end;
end;

%last bin
i=i+1;
if segmrem
  xbin=x(length(x)-segmrem+1:length(x));
  m=nanremf(xbin);
  if length(m)/binsize*100>=valperc
     y(i)=mean(m);
     if nargout>1
        if detrendyes m=detrend(m); end;
        s(i)=std(m);
     end;
  else
     y(i)=NaN;
     s(i)=NaN;
  end;
end;

M(row,1:length(y))=y;
if nargout>1 S(row,1:length(s))=s; end;

end;

if nrow==1
    if r>c M=M(:); S=S(:);
    end;
end;

end
