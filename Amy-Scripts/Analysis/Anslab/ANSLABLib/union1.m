function u = union(x0,x1,x2,x3,x4,x5,x6,x7,x8,x9)

% UNION Form the union of 'sets'.
%   U = UNION(X,Y) forms the union of the elements in X and Y,
%   removing repeated elements. X and Y may be any size, U will be
%   a sorted row vector.
%   The function accepts 0 to 10 input arguments,
%   for example U=UNION(X) or U=UNION(X,Y,Z).



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.


x = [];
for k=1:nargin
  eval(['x=[x x' num2str(k-1) '(:).''];' ]);
end

u = sort(x);
n = length(u);
if n > 1
    tmpind = round([0 ~(u(2:n)-u(1:n-1))]);
    tmpind(tmpind<1) =[];
    if ~isempty(tmpind)
        u(tmpind) = [];
    end
end
