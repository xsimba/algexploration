% kspectyp.m  type of spectrum: T

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

disp(' ');
disp('Type of spectrum:');
m1='Spectrum of variable 1';
m2='Spectrum of variable 2';
m3='Cross-spectrum';
m4='Transfer function phase';
m5='Coherence spectrum';
m6='Spectrum of variable 1 and 2';
m7='Coherence and phase spectra in one graph';
m8='Log-scale on/off';
i=spectype;
spectype=menue(m1,m2,m3,m4,m5,m6,m7,m8);
if spectype==8 spectype=i;
   if speclog==1 speclog=0;
   else speclog=1; end; end;
if ~spectype spectype=1; spec=0; plotyes=0; end;


