%		defrsp5

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

%**********************   define variables  ****************************
% used for editing of onsets in respiration of PSY studies

int=60;
moveint=60;
samplerate= 25;        % <-- depends on the channel, for 1/2 sec epochs = 2

var1         = RS;      % first variable
title1str ='Respiration';       % title of graph
ltv1      ='y-';         % linetype (appears in title, too)
yaxisstr  ='ccm';

var2      = DR;         % second variable
secvar2    =1;            % flag for second variable to plot (0)
title2str ='Tho-Abd'; %'CO2';           %'EKG (15Hz lowpass)';
ltv2      ='c:';

var3      = AC;       % third variable
secvar3   = 1;          % flag for third variable to plot (0)
title3str = 'Act';   %'THO';         %
ltv3      = 'g:';         %':';

%var4      =resp1;      % fourth variable
secvar4   = 0;          % flag for third variable to plot (0)
title4str = [];         %'Respiration 2';
ltv4      = [];         %'-.';


%**********************   define events  ****************************

% any event must be adapted to samplerate here, e.g. event1=rtime*samplerate/384

if ~exist('artbegin')
artbegin  =[];          % artifact marker
artend    =[];          % artifact marker
end;

valueyes  =0;           % display values of events
val       =0;          % value
valtime   =0;          % time of event for value

event1    =[];
event2    =[];
event3    =[];
event1yes =0;
event2yes =0;
event3yes =0;
evscan    =event1;  % define which event to skip through: k
