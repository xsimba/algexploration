% kresp.m   edit onset of respiration (breath holding study): X

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

samr=samplerate;

m1='BLUE:    inspiration onset';
m2='High RED:  inspiration end';
m3='YELLOW:  expiration onset';
m4='Low RED:   expiration end';
m5='Add cycle (click 4 times)';
m6='Exclude complete breathing cycle';
ed=menu('',m1,m2,m3,m4,m5,m6);

if ed==5
   title('Add cycle by clicking on 4 points that define missing cycle (order not relevant)');
   x=ginput(4);
elseif ed==6
   title('Click near maximum of breath you want to exclude')
   x=ginput(1);
else
   x=ginput(2);
end;

x(:,2)=[];
x=round(x*samr);

if ed==1
[i,mini]=min(abs(iont-x(1)));
iont(mini)=x(2);
end;
if ed==3
[i,mini]=min(abs(eont-x(1)));
eont(mini)=x(2);
end;
if ed==4
[i,mini]=min(abs(mint-x(1)));
mint(mini)=x(2);
end;
if ed==2
[i,mini]=min(abs(maxt-x(1)));
maxt(mini)=x(2);
end;

if ed==5
mint(length(mint)+1)=x(1);
iont(length(iont)+1)=x(2);
maxt(length(maxt)+1)=x(3);
eont(length(eont)+1)=x(4);
end;

if ed==6
if x<1 x=1; end;
if x>length(RS) x=length(RS); end;
[i,maxi]=min(abs(maxt-x));
mint(maxi)=[];
iont(maxi)=[];
maxt(maxi)=[];
eont(maxi)=[];
%Vti(maxi)=[];
%Vte(maxi)=[];
%Tt(maxi)=[];
end;


% take care of wrong order input
% M=breath time matrix
% construct n first for faster sorting
n=[(iont(1:length(iont)-1))';maxt';eont';mint'];
n=[n(:);iont(length(iont))];
Mvec=sort(n);

% exclude diff=0 ? not neccessary!
%Md=diff(Mvec);
%n=find(~Md);
%Mvec(n+1)=Mvec(n)+1;

iontlast=Mvec(length(Mvec));
Mvec(length(Mvec))=[];
[i1,i2]=size(Mvec);
M=reshape(Mvec,4,i1/4);
M=[M;[M(1,2:i1/4) iontlast]];

iont=[M(1,:)';iontlast];
maxt=[M(2,:)'];
eont=[M(3,:)'];
mint=[M(4,:)'];



