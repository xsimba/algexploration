% PCO2 plateau detection in PSY projects



%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.



drop_factor_mv=.85;        % .85 is normal
drop_factor_breath=0.6;    %  .6 is normal
threshold_low_factor=1.2;  % 1.2 is normal
invalid_segment=0;
back_points=2;
threshold_low=10;
threshold_high=15;
count=0;
value_before=0;
index_temp=0;
peak_count=0;

for i=6:length(y)-20

	if(y(i)>y(i-1)&y(i)>=y(i+1))		%find a peak
		flag=0;
		for j=1:back_points
			if(abs(y(i)-y(i-j))<5)	%check back to exclude spikes
				flag=1;
			else
				flag=0;
			end
		end
		if(flag==1)

				[next_max,index]=max(y(i+1:i+20));	%check forward to exclude spikes
				if(y(i)>next_max&y(i)>10&y(i)<70)

					peak_count=peak_count+1;
					peak(peak_count)=y(i);		%get peak value
					peak_index(peak_count)=i;	%get peak time

				end


		end
	end
end

count=0;
i=1;
breath_count=0;
last_peak_time=peak_index(1);
while(count<2)
	if(i<length(peak_index))
	min_between=min(y(last_peak_time:peak_index(i+1)));		%check minimun between to peaks to avoid fake plateau

	if(min_between<threshold_low&peak(i)>threshold_high)	%for real plateaus, record this plateau
		count=count+1;
		breath_count=breath_count+1;
		mv(count)=peak(i);
		last_peak_time=peak_index(i);
		mt(count)=peak_index(i);
		breath_time(breath_count)=peak_index(i);
		i=i+1;
	else
		i=i+1;
	end
	end

	if(i==length(peak_index))
		break;
	end
end
if(i<length(peak_index))
temp=i;
for i=temp:peak_count-2

	min_back=min(y(breath_time(breath_count):peak_index(i)));
	min_front=min(y(peak_index(i):peak_index(i+1)));
	mean_around=(mv(count)+peak(i)+peak(i-1)+peak(i+1))/4;		%mean plateau values around the current one
	local_min=0;
	min_counter=0;
	for k=1:4
		if(min(y(peak_index(i+k-3):peak_index(i+k-2)))<15)
			local_min=min(y(peak_index(i+k-3):peak_index(i+k-2)))+local_min;
			min_counter=min_counter+1;
		end
	end

	%threshold_low=min(y(mt(count-1):peak_index(i+2)))*threshold_low_factor;
	threshold_low=local_min*threshold_low_factor/min_counter;	%adaptively set the lower threshold
	if(threshold_low<10)
		threshold_low=10;
	end
	if(min_back<threshold_low&min_front<threshold_low&peak(i)>threshold_high) %check both backward and forward for real plateau
		if(peak(i)/mean_around>drop_factor_mv)
			count=count+1;
			mv(count)=peak(i);
			mt(count)=peak_index(i);
		end
		if(peak(i)/mean_around>drop_factor_breath)	%if it drops below threshold, count it as a breath, not end co2
			breath_count=breath_count+1;
			breath_time(breath_count)=peak_index(i);
		end
	end

	if(min_back<threshold_low&min_front>=threshold_low&peak(i)>threshold_high)
	found_next=0;
	j=1;
	while(found_next==0)
		min_temp=min(y(peak_index(i):peak_index(i+j)));
		if(min_temp<threshold_low|i+j>length(peak)-2)
			found_next=1;
		else
		j=j+1;
		end
	end
	if(max(peak(i:i+j-1))/mean_around>drop_factor_mv)
		count=count+1;
		[mv(count),time_temp]=max(peak(i:i+j-1));
		mt(count)=peak_index(i+time_temp-1);
	end
	if(max(peak(i:i+j-1))/mean_around>drop_factor_breath)
		breath_count=breath_count+1;
		breath_time(breath_count)=peak_index(i+time_temp-1);
	end
	end

end
	temp=i+1;
	%i=i+1;
	for i=temp:temp+1
	min_back=min(y(peak_index(i-1):peak_index(i)));
	if(min_back<threshold_low&peak(i)/mean_around>drop_factor_mv&peak(i)>threshold_high)
		count=count+1;
		mv(count)=peak(i);
		mt(count)=peak_index(i);
	end

	if(min_back<threshold_low&peak(i)/mean_around>drop_factor_breath&peak(i)>threshold_high)
		breath_count=breath_count+1;
		breath_time(breath_count)=peak_index(i);
	end
	end
%interpolate breath time

for i=1:6
	breath_time_new(i)=breath_time(i);
end
breath_count_new=6;
for i=6:breath_count
	mean_breath_interval=mean(diff(breath_time(i-5:i)));	%get the mean breath interval around the current one
	if(breath_time(i)-breath_time(i-1)>1.8*mean_breath_interval)
		cross10_found=0;
		l=0;
		while(cross10_found==0)
			l=l+1;
			if(y(breath_time(i)-l)<=10|breath_time(i)-l-breath_time(i-1)<1)
				cross10_found=1;
			end
		end
		if(breath_time(i)-l-breath_time(i-1)>1.8*mean_breath_interval) 	%inset a point if it is greater than 1.8 times the mean
			add_point=round((breath_time(i)-l-breath_time(i-1))/mean_breath_interval);
			for j=1:add_point-1
				breath_count_new=breath_count_new+1;
				breath_time_new(breath_count_new)=NaN;		%set the new point's value to NaN
			end
		end
			breath_count_new=breath_count_new+1;
			breath_time_new(breath_count_new)=breath_time(i);	%store the original value
	else
		breath_count_new=breath_count_new+1;
		breath_time_new(breath_count_new)=breath_time(i);
	end

end
resp_time=nan_lip(breath_time_new);  %linear interpolated respiration time
resp_interval=(breath_time(breath_count)-breath_time(1))/(breath_count_new*25); %get the respiration interval
resp_rate=60/resp_interval;		%get the respiration rate
%resp_time=breath_time;

else
invalid_segment=1;
resp_time=[];
resp_interval=NaN;
resp_rate=NaN;
breath_time_new=[];
breath_time=[];
mt=peak_index;
mv=[];
end



