function yf=notch60(y,sr,width);

%NOTCH60 60Hz IIR Notch Filter.
%   yf=notch60(y,sr,width);
%   Returns data after being passed through a 7th order
%   Chebyshev Type I IIR 60Hz digital notch filter.
%   sr: samplerate in Hz
%   width: stop band width in Hz
%


%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

if nargin<2 sr=1000; end
if nargin<3 width=6; end

yf=notch(y,sr,50,width,7,.5,0);






