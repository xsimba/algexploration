% Detection of minima, maxima and onsets of in- and expiration in breathing signal
% Check for reiontasets and movement artifacts from EXAM possible
%    [option ')' => edit resets]
%    ['b', 'e' => mark beginnings and ends of artifactual intervals]
%    [saved as artbegin and artend]
% Computation of respiratory parameters (volume, timing,..)
% Editing of onsets of in-/expiration from EXAM
% Conversion to epoch data and plot of important variables
% Adjust artifact times
%              display lines around artifact intervals
%              convert artifact intervals into 1/2sec epochs (art0)
%              Display I/E-ratio
%              Detect movement artifacts with filtering routine
% Vt can be mean of Vti and Vte, stable during posture shifts

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

sr=25;       % samplerate
ep=4;        % epoch size= 1/ep sec

% Initialization
if ~exist('bno')
    bno=1;
    bplotyes=1;
end
if ~bno
    rateyes=0;
    rat=0;
end

set(0,'DefaultAxesFontSize',7);
key=[0.9999 0.8441 0.7491 0.6559 0.5645 0.4713 0.3781 0.2814 0.1918 0.0001];
rating=[];
artbegin=[];
artend=[];
redo=1;
examyes=0;

%*** Create figure windows
if bno|bplotyes
    cfig, cfig, cfig, cfig
    if screen>800
        figure('Position',[52 298 967 429]);  % for exam
        figure('Position',[237 22 784 703]);
        figure('Position',[237 432 784 294])
        figure('Position',[237 136 784 294])
        figure('Position',[237 22 784 147])
    else
        figure('Position',[59 237 730 320]);  % for exam
        figure('Position',[237 22 559 536]);
        figure('Position',[237 360 560 198])
        figure('Position',[237 143 560 212])
        figure('Position',[237 24 562 125])
    end
end

%**** Assign beta coefficients
ch_tk=0;
if bno
    ch_tk=3;
    t=(1:length(R2))/sr;
    while ch_tk>2
        figure(5)
        plot(t,AC)
        axisx(0,max(t));
        title('accelerometer')
        figure(4)
        plot(t,R2)
        axisx(0,max(t));
        title('abdomen')
        figure(3)
        plot(t,R1)
        axisx(0,max(t));
        title('thorax')
        ch_tk=menu('Respiration edit options:','Both channels ok','Only thorax ok','Only abdomen ok','Edit thorax','Edit abdomen')-1;
        if ch_tk==3
           figure(2)
           R1=nan_lipf(outrect(R1,[],1));
        end
        if ch_tk==4
           figure(2)
           R2=nan_lipf(outrect(R2,[],1));
        end
    end
end

if lucas_flag
    ch_tk=1;
end

n=BETA(:,1);
i1=find(n==subjn);

if ~isempty(i1)
   bet=BETA(i1,2:5);
else
   disp('Analyze calibration first!');
   return
end

if ch_tk==0
    bet=bet(1:2);
end
if ch_tk==1
    bet=[bet(3) 0];
end
if ch_tk==2
    bet=[0 bet(4)];
end
if all(bet==0)
    disp('Analyze calibration first!');
    return
end
disp(['Beta coefficients are ',num2str(bet(1)),' / ',num2str(bet(2))]);
R1 = R1(:) * bet(1);  % thoracic
R2 = R2(:) * bet(2);  % abdominal

% Mothers & Daughters study adjustment for child
childyes=0;
if strcmp(studystr,'mnd')
    childyes=menu('Child?','no','yes')-1;
    if childyes
        disp('Respiration of child. Signals will be amplified by a factor of two.')
        R1=R1*2;
        R2=R2*2;
    end
end

if 0
    %**** Detect movement artifacts with filtering routine (slow)
    rh1=filtlow(R1,0.5,sr);
    rh2=filtlow(R2,0.5,sr);
    rl12=filthigh(R1,0.04,sr);
    rl22=filthigh(R2,0.08,sr);
    % index of restlessness
    n1=decfast(abs(rh1),12);
    n2=filthann(n1,16);
    plot(n2);
end;

if reset_check
    %**** Check for resets
    t=2160:4320;
    plot(t,R1(t),t,R2(t));
    disp('Check for resets?');
    i=menue('no','yes');
    if i==2
        clear artbegin artend RESPEDIT
        defres0
        def
        radapt=0;
        exam
        R1=var1; R2=var2;
        % save edited resp1/2
        i=input('Save Resp 1 or 2, 3=both, 0=do not save [1] ==>  ');
        if isempty(i)
            i=1;
        end;
        if i==1
            cmdstr=['save ',subject,'e R1'];
        elseif i==2
            cmdstr=['save ',subject,'e R2'];
        else
            cmdstr=['save ',subject,'e R1 R2'];
        end;
        if i
            chd(red_dir)
            disp(cmdstr);
            eval(cmdstr)
        end
    end  %check for resets?
end  %if reset_check


%**** Sum up calibrated signal (->RS), smoothing
R1=filthigh(R1,1,sr,5);
R2=filthigh(R2,1,sr,5);
if filtlowyes
    R1=filtlow(R1,.033,sr,5);
    R2=filtlow(R2,.033,sr,5);
end
RS = R1(:) + R2(:);

%RS=filthigh(RS,1,sr,5);
%if filtlowyes
%RS=filtlow(RS1,.033,sr,5);
%end

%**** Create control signals (difference of bands, movement)
DR = ztrans(R1)-ztrans(R2);
DR = ztrans2(DR)+1.5;
if exist('AC')~=1
    AC_empty=1;
else
    AC_empty=0;
end
AC=ztrans2(AC)+.5;

%****************** redo analysis loop
while redo==1

    if ~bno
        redo=0;
        examyes=0;
    end

    %**** Find maxt=end of inspiration and mint=end of expiration
    [maxt,maxv]=findmax2(RS,area,back1,back2,rise,mingap);

    %*** if maxima found
    if length(maxt)<3
        subplot(2,1,2);
        plot(RS);
        error('No maxima found. Probably artifactual respiration signal.');
    else

        %*** find minima
        minv=zeros(length(maxt)-1,1);
        mint=zeros(length(maxt)-1,1);
        dy= [0;diff(RS)];

        for i=1:length(maxt)-1
            n=maxt(i) : maxt(i+1);
            [minv(i),mint(i)] = min(RS(n));

            n=maxt(i) : mint(i)+maxt(i)-1;  % better minimum before?
            m = ((dy(n+1) > -1) & (RS(n)-minv(i)) < ramp) ;

            j = find (m==1);
            if ~isempty(j)
                mint(i) = j(1)+maxt(i)-1;
            else
                mint(i) = mint(i) + maxt(i) -1;
            end;
        end;

        % exclude maxima and minima that are equal
        nt=maxt;
        nv=maxv;
        nt(length(nt))=[];
        nv(length(nv))=[];
        if length(nt) & length(mint)
            i=find(nt==mint);
            if ~isempty(i)
                maxt(i)=[]; mint(i)=[]; maxv(i)=[]; minv(i)=[];
            end;
        end;

        %***  onset of inspiration -> expiratory pause times
        iont = zeros (length(mint)-1,1);

        for i=1:length(maxt)-1
            n = mint(i):maxt(i+1);
            n((n+onset_forward)>length(RS))=[];

            m = ((dy(n+1) >= onset_flow/sr) & (RS(n+onset_forward)-RS(n)) > onset_rise) ;

            j = find (m==1);                        % first one is valid
            if isempty(j)
                j(1)=2;
            end;
            iont(i) = j(1) + mint(i) - 1;
        end;

        %***  onset of expiration -> inspiratory pause times
        eont = zeros (length(mint)-1,1);

        for i=1:length(maxt)-1
            n = maxt(i):mint(i);
            n((n+onset_forward)>length(RS))=[];
                                                    % at least 3 succeeding
            m = ((dy(n+1) <= -onset_flow/sr) & (RS(n+onset_forward)-RS(n)) < -onset_rise) ;

            j = find (m==1);                        % first one is valid
            if isempty(j)
                j(1)=2;
            end;
            eont(i) = j(1) + maxt(i) - 1;
        end;

        maxt(1)=[]; %incomplete cycle
        eont(1)=[];
        mint(1)=[];
        maxt(length(maxt))=[];

        %**** Derive respiratory parameters
        [RR,Vmin,Vti,Flow,Ti,Pi,Te,Pe,Tt,TiTt,abd,tho,RibP,Vte,iont,maxt,eont,mint,vtind]=resptime(RS,maxt,mint,iont,eont,sr,bet,R1,R2,vtmin,usemeanvt);
        disp([int2str(length(vtind)),' breaths excluded due to small tidal volume']);

    end
% if length(maxt)


    if examyes==1
    %**** edit onsets from exam

        figure(1)
        RESPEDIT=[];  %indicates plot of circles in exam
        if AC_empty
            AC=zeros(size(RS)) +.5;    % proxy movement indicator
        end
        defrsp5
        def
        radapt=0;
        rangetwo=0;
        subvarplot=1;
        disp(' ');
        disp('	Editing of onsets, adding, and deletion of cycles:');
        disp('	First zoom into reasonably small interval with <z>,');
        disp('	Then get menue for editing with <''>.');
        disp('	Edit signal artifacts with <)>. Mark artifactual intervals with <b>, then <e>.');
        disp('	Quit EXAM with <q>');
        exam
        RS=var1;
        vtmin=0;  % accept all edited cycles
        [RR,Vmin,Vti,Flow,Ti,Pi,Te,Pe,Tt,TiTt,abd,tho,RibP,Vte,iont,maxt,eont,mint,vtind]=...
            resptime(RS,maxt,mint,iont,eont,sr,bet,R1,R2,vtmin,usemeanvt);
    end

    %**** if examyes

    %*** compute indicator of movement and Vmin data
    Vdiff=Vti-Vte;
    Vt=(Vti+Vte)/2;
    Flow = Vt  ./Ti;    % mean inspiratory flow [ml/sec]
    Vmin =(Vt*60/1000) ./ Tt;
    Vminall =(sum(Vt)*60/1000) / sum(Tt);

    %**** Set RibP to NaN if only one beta
    if any(bet==0)
        RibP=NaN;
    end

    %**** convert to epoch data
    % change channels if R1 is not thoracic breathing
    %i=tho; tho=abd; abd=i;
    %RibP=ones(length(RibP),1)-RibP;

    disp('Conversion of respiration data into 0.25 sec epochs')

    %******* 1/4 sec epochs
    RS0=decfast(ipfast(RS,4),25);
    %RS0=resample(RS,25,100); % much slower (20x), but not really more accurate

    iontd=difffit(iont);
    Tt0=epoch(iont,iontd,(sr/ep)) ./sr;
    RR = 60 * (length(Tt)) / sum(Tt); %little higher, eg. 11.48 vs. 11.40
    RR0= 60 ./Tt0;
    x=iont; iont(length(iont))=[];
    iontall=x;  % for save
    Vt0=epoch(x,Vt,(sr/ep));
    Vmin0=epoch(iont,Vmin,(sr/ep));
    Flow0=epoch(x,Flow,(sr/ep));

    Ti0=epoch(x,Ti,(sr/ep));
    Pi0=epoch(x,Pi,(sr/ep));
    Te0=epoch(x,Te,(sr/ep));
    Pe0=epoch(x,Pe,(sr/ep));
    TiTt0=epoch(x,TiTt,(sr/ep));

    if ~all(isnan(RibP))
        RibP0=epoch(x,RibP,(sr/ep));
    else
        RibP0=NaN*ones(size(RR0));
    end

    %**** Compute artifact times (applied later!)
    l=round(length(R1)/sr*ep);
    art0=ones(l,1);
    if ~isempty(artbegin)
        art1=floor(artbegin/sr)*ep;
        art2=ceil(artend/sr)*ep;
        n=find(art1<1);
        if ~isempty(n)
            art1(n)=one(n);
        end
        n=find(art2>l);
        if ~isempty(n)
            art2(n)=l*one(n);
        end
        for i=1:length(art1)
            n=art1(i):art2(i);
            art0(n)=NaN*ones(size(n));
        end
    end

    %*** adapt lengths and exclude artifactual intervals
    len=floor(length(RS)/sr*ep);
    art0=cut_fill(art0,len);
    Ti0=cut_fill(Ti0,len);
    Te0=cut_fill(Te0,len);
    Pi0=cut_fill(Pi0,len);
    Pe0=cut_fill(Pe0,len);
    TiTt0=cut_fill(TiTt0,len);
    RR0=cut_fill(RR0,len);
    Vt0=cut_fill(Vt0,len);
    %Vti0=cut_fill(Vtm0,len);
    Vmin0=cut_fill(Vmin0,len);
    Flow0=cut_fill(Flow0,len);
    RibP0=cut_fill(RibP0,len);
    RS0=cut_fill(RS0,len);

    Ti0=Ti0 .* art0;
    Te0=Te0 .* art0;
    Pi0=Pi0 .* art0;
    Pe0=Pe0 .* art0;
    TiTt0=TiTt0 .* art0;
    RR0=RR0 .* art0;
    Vt0=Vt0 .* art0;
    %Vti0=Vti0 .* art0;
    Vmin0=Vmin0 .* art0;
    Flow0=Flow0 .* art0;
    RibP0=RibP0 .* art0;

    %*** Preserve variables
    RSx=RS0;
    RRx=RR0;
    Vtx=Vt0;
    Vminx=Vmin0;
    Flowx=Flow0;
    Tix=Ti0;
    Tex=Te0;
    Pix=Pi0;
    Pex=Pe0;
    TiTtx=TiTt0;
    RibPx=RibP0;
    men=1; edval=0;
    ut=(1:len)/ep;

    %***** Editing loop
    if bno|bplotyes
        while men

            if ~bno
                men=0;
            end

            figure(2)
            clg
            subplot(9,1,1)
            plot(ut,RR0,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            title([subject,'-',int2str(row),'  Edit: click on graph, Menu: lower left, Set missing: upper left, Restore: upper right, OK to quit: lower right'])
            ylabel(['RR=',num2str(rndv(mean(nanrem(RR0))))])

            subplot(9,1,2)
            plot(ut,Vmin0,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['V=',num2str(rndv(mean(nanrem(Vmin0))))])

            subplot(9,1,3)
            plot(ut,TiTt0,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Duty=',num2str(rndv(mean(nanrem(TiTt0))))])

            subplot(9,1,4)
            plot(ut,Pe0,'m')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Pe=',num2str(rndv(mean(nanrem(Pe0))))])

            subplot(9,1,5)
            plot(ut,Ti0,'r');
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Ti=',num2str(rndv(mean(nanrem(Ti0))))])

            subplot(9,1,6)
            plot(ut,Vt0,'r')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Vt=',num2str(rndv(mean(nanrem(Vt0))))])

            subplot(9,1,7)
            plot(ut,Flow0,'r');
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Flow=',num2str(rndv(mean(nanrem(Flow0))))])

            subplot(9,1,8)
            plot(ut,Te0,'c')
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Te=',num2str(rndv(mean(nanrem(Te0))))])

            subplot(9,1,9)
            plot(ut,Pi0,'c');
            set(gca,'XLim',[-10 ut(length(ut))+10]);
            ylabel(['Pi=',num2str(rndv(mean(nanrem(Pi0))))])
            xlabel('time [sec]')

            drawnow
            if ~bno & bpause
                tic,
                while toc<bpause
                end;
            end

            set(gcf,'units','normal');
            h = axes('units','normal','position',[0 0 1 1],'XLim',[0 1],'YLim',[0 1],'Color','none','XColor',[0 0 0],'YColor',[0 0 0]);


            if bno
                [h,v,asc]=ginput(1);
                if isempty(h) & isempty(v) & isempty(asc)
                    men=0;
                elseif (v<0.111 & h<.1)  % left lower
                    m1='Respiratory rate outliers';
                    m2='Minute ventilation outliers';
                    m3='Duty cycle outliers';
                    m4='Expiratory pause outliers';
                    m5='Inspiratory time outliers';
                    m6='Tidal volume outliers';
                    m7='Inspiratory flow rate outliers';
                    m8='Expiratory time outliers';
                    m9='Inspiratory pause outliers';
                    m10='Synchronize missing values 1-4';
                    m11='Synchronize missing values 5-7';
                    m12='Synchronize missing values 8-9';
                    m13='Synchronize all missing values';
                    m14='Linear interpolation';
                    m15='Restore original data';
                    men=menu('',m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15);
                elseif asc~=1 | (v<0.111 & h>.9)   % right lower or any key
                    men=0;
                else
                    if v>0.9
                        if  h<.1
                            men=13;
                        end;  % left upper
                        if  h>.9
                            men=16;
                        end;  % right upper
                        if  h>.1 & h<.366
                            men=10;
                        end;    % first middle third
                        if  h>.366 & h<.632
                            men=11;
                        end;  % 2nd middle third
                        if  h>.632 & h<.9
                            men=12;
                        end;    % 3rd middle third
                    else
                        n=find(v<key);
                        men=length(n);
                    end;
                end;

                if men==1
                   figure
                   [RR0,ind1]=outrect(RR0,ut,1);
                   close
                end;
                if men==2
                   figure
                   [Vmin0,ind2]=outrect(Vmin0,ut,1);
                   close
                end;
                if men==3
                   figure
                   [TiTt0,ind3]=outrect(TiTt0,ut,1);
                   close
                end;
                if men==4
                   figure
                   [Pe0,ind3]=outrect(Pe0,ut,1);
                   close
                end;
                if men==5
                   figure
                   [Ti0,ind3]=outrect(Ti0,ut,1);
                   close
                end;
                if men==6
                   figure
                   [Vt0,ind3]=outrect(Vt0,ut,1);
                   close
                end;
                if men==7
                   figure
                   [Flow0,ind3]=outrect(Flow0,ut,1);
                   close
                end;
                if men==8
                   figure
                   [Te0,ind3]=outrect(Te0,ut,1);
                   close
                end;
                if men==9
                   figure
                   [Pi0,ind3]=outrect(Pi0,ut,1);
                   close
                end;
                if men==10
                   ind=isnan(RR0)|isnan(Vmin0)|isnan(TiTt0)|isnan(Pe0);
                   n=find(ind);
                   RR0(n)=NaN*ones(size(n));
                   Vmin0(n)=NaN*ones(size(n));
                   TiTt0(n)=NaN*ones(size(n));
                   Pe0(n)=NaN*ones(size(n));
                end;
                if men==11
                   ind=isnan(Ti0)|isnan(Vt0)|isnan(Flow0);
                   n=find(ind);
                   Ti0(n)=NaN*ones(size(n));
                   Vt0(n)=NaN*ones(size(n));
                   Flow0(n)=NaN*ones(size(n));
                end;
                if men==12
                   ind=isnan(Te0)|isnan(Pi0);
                   n=find(ind);
                   Te0(n)=NaN*ones(size(n));
                   Pi0(n)=NaN*ones(size(n));
                end;
                if men==13
                   ind=isnan(RR0)|isnan(Vmin0)|isnan(TiTt0)|isnan(Pe0)|isnan(Ti0)|isnan(Vt0)|isnan(Flow0)|isnan(Te0)|isnan(Pi0);
                   n=find(ind);
                   RR0(n)=NaN*ones(size(n));
                   Vmin0(n)=NaN*ones(size(n));
                   TiTt0(n)=NaN*ones(size(n));
                   Pe0(n)=NaN*ones(size(n));
                   Ti0(n)=NaN*ones(size(n));
                   Vt0(n)=NaN*ones(size(n));
                   Flow0(n)=NaN*ones(size(n));
                   Te0(n)=NaN*ones(size(n));
                   Pi0(n)=NaN*ones(size(n));
                end;
                if men==14
                   RR0=nan_lip(RR0);
                   Vmin0=nan_lip(Vmin0);
                   TiTt0=nan_lip(TiTt0);
                   Pe0=nan_lip(Pe0);
                   Ti0=nan_lip(Ti0);
                   Vt0=nan_lip(Vt0);
                   Flow0=nan_lip(Flow0);
                   Te0=nan_lip(Te0);
                   Pi0=nan_lip(Pi0);
                end;
                if men==15
                    RS0=RSx;
                    RR0=RRx;
                    Vt0=Vtx;
                    Vmin0=Vminx;
                    Flow0=Flowx;
                    Ti0=Tix;
                    Te0=Tex;
                    Pi0=Pix;
                    Pe0=Pex;
                    TiTt0=TiTtx;
                    RibP0=RibPx;
                end;

            end
        %if bno
        end
    %while men, editing loop
    end %if bno|bplotyes

    if bno|bplotyes
        %*** Plot artifact indicators
        figure(5)
        plot(ut,RibP0,'m')
        set(gca,'XLim',[-10 ut(length(ut))+10]);
        ylabel(['RibP=',num2str(rndv(mean(nanrem(RibP0))))])

        figure(4)
        t1=iont/sr;
        plot(t1,Vti,'y',t1,Vte,'m',t1,Vti-Vte,'r');
        axis([-10 ut(length(ut))+10 min((Vt-Vte))-20 max([Vt;Vte])+20]);
        title('Tidal volumes: inspiratory (yellow), expiratory (magenta), difference (red)');

        figure(3)
        plot(ut,RS0,'m')
        set(gca,'XLim',[-10 ut(length(ut))+10]);
        ylabel(['Raw=',num2str(rndv(mean(nanrem(RS0))))])
        drawnow
    end

    if bno
        examyes=menu('Edit artifacts','no','yes')-1;
        redo=examyes;
    end

end;
%*************** While redo analysis

if bno
    %*** Give quality rating
    if ~exist('rateyes');
        rateyes = [];
    end
    if rateyes
        rat=menu('Signal quality rating','1 = even means not reliable',...
            '2 = not usable for FFT','3 = problematic for FFT','4 = ok for FFT','5 = excellent');
    else
        rat=0;
    end
end

%*** Edit RibP
if 0
    figure(4)
    [RibP0,ind]=outrect(RibP0,ut,1);
else
    n=find(RibP>=1|RibP<=0); RibP(n)=NaN*ones(size(n));
end;

%*** Compute missing value count
nanind=isnan(RR0)|isnan(Vmin0)|isnan(TiTt0)|isnan(Pe0)|isnan(Ti0)|isnan(Vt0)|isnan(Flow0)|isnan(Te0)|isnan(Pi0);
nan=find(nanind);

%*** save NaN positions
artb=artbegin;
arte=artend;
ionta=iontall;
Vmina=Vminall;
RRn=find(isnan(RR0));
Vminn=find(isnan(Vmin0));
TiTtn=find(isnan(TiTt0));
Pen=find(isnan(Pe0));
Tin=find(isnan(Ti0));
Vtn=find(isnan(Vt0));
Flown=find(isnan(Flow0));
Ten=find(isnan(Te0));
Pin=find(isnan(Pi0));
RibPn=find(isnan(RibP0));

%*** Adjustments for Mothers & Daughters study for child
if childyes
    RS0=RS0 ./2;
    Vt=Vt ./2;
    Vti=Vti ./2;
    Vte=Vte ./2;
    Vmin=Vmin ./2;
    Flow=Flow ./2;
    abd=abd ./2;
    tho=tho ./2;
    Vmina=Vmina ./2;
end



