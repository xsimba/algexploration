%		tbprw.m

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

clear;
ExtractChannel = 'all';
t1 = 1;
t2 = inf;

ReadFilePath ='F:\AUL08902.acq';
ReadFP =ReadFilePath;
TextFilePath = 'F:\AUL08902.txt';
WriteFilePath = 'F:\AUL08902_added.ACQ';
CopyChannel ='Heart Rate';
AddLabel = 'Peter';
TargetChanNum = 1;

readbp;
savbpcom;