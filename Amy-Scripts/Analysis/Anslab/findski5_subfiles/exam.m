% EXAM 2.0: Program to display and edit physiological signals
% Paging through data, zooming, editing, filtering, spectral analysis, etc.
% Get a table of function keys with <edit manual> or <type manual>.
%     (file c:\matlab\toolbox\exam20\manual.m)
% Get help within EXAM by pressing <o> for online help.
% Call of EXAM for interactive exploration of signals:
%    >>  var1=<my_variable_1>;
%    >>  var2=<my_variable_2>;   <- optional
%    >>  var3=<my_variable_3>;   <- optional
%    >>  var4=<my_variable_4>;   <- optional
%    >>  defblank                <- variable assignments
%    >>  samplerate=<my_samplerate>;
%    >>  def                     <- program settings
%    >>  exam
%  The variables must have equal sample rates
%    (use INTERP and DECIMATE to adapt sample rates)
%  There is also a large library of definition files in the
%    subdirectory "exam20\def". For EKG: type e.g., >> defe; def; exam

%   ANSLAB - Autonomic Nervous System Laboratory
%   � Copyright 2005 Frank Wilhelm & Peter Peyk
%
%   This program is free software; you can redistribute it and/or
%   modify it under the terms of the GNU General Public License
%   as published by the Free Software Foundation; either version 2
%   of the License, or (at your option) any later version.
%
%   This program is distributed in the hope that it will be useful,
%   but WITHOUT ANY WARRANTY; without even the implied warranty of
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%   GNU General Public License for more details.
%   You should have received a copy of the GNU General Public License
%   along with this program; if not, write to the Free Software
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA.

disp(' ');
disp('You are in EXAM 2.0 now. Online help with <o>. Quit with <q>.');




%*** New modifications

% figure changes with zoom
fig=1; figz=1;
zoomf=2;  % zoom factor

% exam called by findreqs: plot of circles
if exist('RESPEDIT') resp=1; else resp=0; end;
if exist('ICGEDIT') icg_pep=1; else icg_pep=0; end;
% 9/14/94: single plot: scale variables half for editing of resp in FP
% no more implemented??

% range for second variables adjusted to lie in lower part of graph
if ~exist('rangetwo') rangetwo=0; end;

% var2 and 3 are plotted in upper window: called from findresp
if ~exist('subvarplot') subvarplot=0; end;

% collision with shift.m, all def-files would need change
shiftx=0;
clear shift

%*** Initialization
hold off;
i=computer; % 2=Mac (Matlab 3.5); 0=Sparkstation; 1=PCWIN (Matlab 4.0)
if i(1)=='M' comptype=2;
elseif i(1)=='P' comptype=1;
else comptype=0; end;
menueyes=(comptype==2);
global comptype;
repeat    =0;   % repeat skip through events or going forward fast until ctrl-c
if ~z z=29; end;

%1****** Main loop until quit
while z

    %2****** Graph section
    if plotyes

        %*** stay within borders
        if ~shiftx
           if s1<1  s1=1; end
           if s2>length(var1)  s2=length(var1); end
        else % x-shifted var1
           if (s1-shiftx)<1  s1=round(s1+shiftx)+1; end
           if (s2-shiftx)>length(var1)  s2=round(length(var1)+shiftx)-1; end
        end;

        %*** assign values to plot
        y1=var1(s1-shiftx:s2-shiftx);
        if secvar2  y2=var2(s1:s2)+shifty; end;
        if secvar3  y3=var3(s1:s2); end;
        if secvar4  y4=var4(s1:s2); end;

        %***  Exclude NaNs before min and max for axis are determined
        y1nan=nanrem(y1);
        miny1=min(y1nan); maxy1=max(y1nan);
        minyax=miny1;
        if secvar2 y2nan=nanrem(y2);
        if radapt & ~subvarplot
          miny=min(y2nan); maxy=max(y2nan);
          fact=(maxy1-miny1)/(maxy-miny);
          if rangetwo
            y2=(y2-miny)*fact/2+miny1-(maxy1-miny1)/2;
            minyax=min([min(nanrem(y2)) miny1]);
          else
            y2=(y2-miny)*fact+miny1;
          end;
        else y1nan=[y1nan;y2nan]; end;
        end;
        if secvar3
        y3nan=nanrem(y3);
        if radapt & ~subvarplot
          miny=min(y3nan); maxy=max(y3nan);
          fact=(max(y1nan)-min(y1nan))/(maxy-miny);
          if rangetwo
            y3=(y3-miny)*fact/2+miny1-maxy1+miny1;
            minyax=min([min(nanrem(y3)) miny1]);
          else
            y3=(y3-miny)*fact+miny1;
          end;
        else y1nan=[y1nan;y3nan];end;
        end;
        if secvar4
        y4nan=nanrem(y4);
        if radapt & ~subvarplot
          miny=min(y4nan); maxy=max(y4nan);
          fact=(max(y1nan)-min(y1nan))/(maxy-miny);
          if rangetwo
            y4=(y4-miny)*fact/2+miny1-maxy1+miny1;
            minyax=min([min(nanrem(y4)) miny1]);
          else
            y4=(y4-miny)*fact+miny1;
          end;
        else y1nan=[y1nan;y4nan]; end;
        end;

        miny=min(y1nan); maxy=max(y1nan);
        miny=round(miny*10)/10; maxy=round(maxy*10)/10;
        if radapt ylimitstr=['[Min= ',num2str(miny),', Max= ',num2str(maxy),'] '];
        else ylimitstr=[]; end;     % display limits of other var in title

        %3****** specify limits of plot
        if all(isnan(y1))
            disp('Only NaN-values in this interval. Cannot plot.');
            disp(['Interval: ',num2str(s1/scalefact),'-',num2str(s2/scalefact)]);
        else
            t=(s1:s2) ./scalefact;
            if ytrunc
                i1=ylim1;
                i2=ylim2;
            else
                i1=miny1;
                i2=maxy1;
            end; %i1=minyax;

            %*** add space on y-axis
            as=(i2-i1)*0.08;
            if ~as as=1; end;
            yax1=i1-as; yax2=i2+as; asy=(i2-i1)/40;
            xax1=(s1-1)/scalefact; xax2=(s2+1)/scalefact; asx=(xax2-xax1)*0.005;

            %*** single-plot
            if ~doubleyes ax_reset; clf; %Amy - replaced clg with clf 
                subplot(111); odd=1;
            else % mutual subplots
              if odd==1 odd=2;
              else odd=1; clf; %Amy - replaced clg with clf 
              end;
              if spec odd=1; clf; %Amy - replaced clg with clf 
              end;
              eval(['subplot(2,1,',num2str(odd),')']);
              hold off;
            end;

            %*** x-axis label
            if scale==1
               labelx=[int2str(xax2-xax1),' points'];
            else
               iv=xax2-xax1;
               if iv<10
                  str=[num2str(xax2-xax1),' sec'];
               else
                  ivmin=floor(iv/60);
                  ivsec=iv-ivmin*60;
                  str=([num2str(ivmin),':',num2str(ivsec),'min']);
               end;
               labelx=[str];
            end;

            %*** plot up to 4 variables
            figure(fig)

            if ~subvarplot
                if ~secvar2 & ~secvar3 & ~secvar4 ylimitstr=[]; plot(t,y1,[ltv1]); end;
                if secvar2 & secvar3 & secvar4
                   plot(t,y1,ltv1,t,y2,ltv2,t,y3,ltv3,t,y4,ltv4);end;
                if secvar2  & secvar3  & ~secvar4  plot(t,y1,ltv1,t,y2,ltv2,t,y3,ltv3);end;
                if secvar2  & ~secvar3 & secvar4   plot(t,y1,ltv1,t,y2,ltv2,t,y4,ltv4);end;
                if ~secvar2 & secvar3  & secvar4   plot(t,y1,ltv1,t,y3,ltv3,t,y4,ltv4);end;
                if secvar2  & ~secvar3 & ~secvar4  plot(t,y1,ltv1,t,y2,ltv2); end;
                if ~secvar2 & secvar3  & ~secvar4  plot(t,y1,ltv1,t,y3,ltv3); end;
                if ~secvar2 & ~secvar3 & secvar4   plot(t,y1,ltv1,t,y4,ltv4); end;
            else
                % for editing of respiration
                if secvar2  & secvar3  & ~secvar4
                subplot(2,1,1)
                plot(t,y2,ltv2,t,y3,ltv3);
                axis([xax1 xax2 0 2]);
                subplot(2,1,2)
                plot(t,y1,ltv1)
                axisx(xax1,xax2);
                end;
                % for editing of pCO2, maximum 42 mmHG
                if secvar2  & ~secvar3  & ~secvar4
                subplot(2,1,1)
                plot(t,y2,ltv2);
                axisx(xax1,xax2);
                subplot(2,1,2)
                plot(t,y1,ltv1)
                yax1=0; yax2=max([42 max(y1)]);
                axis([xax1 xax2 yax1 yax2]);
                end;
            end

            % if ~subvarplot

            %*** set axis
            if ~subvarplot
               axis([xax1 xax2 yax1 yax2]);
            end

            %4****** additional lines and text
            hold on;

            %5****** leave next out for higher speed
            if ~repeat

                %*** additional circles on plot
                if circle  plot(t,y1,['w',lt8]); end;

                %*** title-string
                %str1=[title1str,ltv1,'    ',title2str,ltv2,'    '];
                %str2=[title3str,ltv3,'    ',title4str,ltv4,'    '];
                str1=[title1str,'    ',title2str,'    '];
                str2=[title3str,'    ',title4str,'    '];
                title([str1,str2,ylimitstr]);
                xlabel(labelx);
                ylabel(yaxisstr);
                if gridyes grid; end;

                %*** Min and Max at right y-scale
                if ~isempty(ylimitstr) edminmax; end;

                %*** display menue under plot: M
                if menueyes  edmenue; end;

                %*** display values
                if valueyes  edvalues;  end;

                %*** display copy number
                if recallnumyes  edcopy;  end;

                %*** display lookat number
                if lookatyes  edlookat;  end;

                %*** mark artifacts as horizontal lines
                if artmarkyes   eartmhor;  end;

                %*** markers as vertical lines
                if(~icg_pep)
                    if markeryes  eartmver;  end;
                    if event1yes  emarkev1;  end;
                    if event2yes  emarkev2;  end;
                    if event3yes  emarkev3;  end;
                    else
                    if markeryes  eartmver;  end;
                    if event1yes  emrkpep1;  end;
                    if event2yes  emrkpep2;  end;
                    if event3yes  emrkpep3;  end;
                end

                %*** mark events (onsets of breaths) with circles, needed for findreqs
                if resp  emarkrsp; end;
                if icg_pep emarkpep; end;

            end;
            %5****** if repeat  -> higher speed

            %*** display event number
            if evnumyes  edevnum;  end;

            hold off;
            %4****** end of 'hold on'

            %*** plot spectrum
            if spec  especpl;  end;

        end;
        %3****** all NaN + limits of plot


    end;
    %2****** if plotyes

    plotyes=1;



    %2****** repeat skip and move fast, leave out input section
    if repeat   erepeat;

    %*** input section
    else

        if z==999  z=zold; end;
        zold=z; z=[];

        h = axes('units','normal','position',[0 0 1 1]);
        axis off;
        axes(h);

        while isempty(z)
           [i1,i2,z]=ginput(1);
        end;

        % react on mouse click
        if z==1
          if i2<.3
            if i1<.3 z=44;
            elseif i1>.7 z=46;
            else z=31;
            end;
            if i2<0  %.05   %upper/lower part of menue
              if i2<.025  i=1;
              else i=2;
              end
              try
                z=menuevec(i,round(((i1-.077)/.95+.14)*10));   % /.97
              catch
                  continue
              end
            end
          elseif i2>.7
            if i1<.3 z=4;
            elseif i1>.7 z=12;
            else z=30;
            end;
          else
            if i1<.3 z=28;
            elseif i1>.7 z=29;
            else z=zold;
            end;
          end;
          if i1>.9 & i2<.11 z=113; end % quit
        end;

        %*** display pressed key:    leave out: problem of slow replot on PC
        if 0
            if comptype==2
                keystr=z;
            else
                keystr=setstr(z);
            end;
            ii=s2-s1;
            valx=s1/scalefact;
            str=num2str(keystr);
            if comptype<2
               cmdstr=['text(''Position'',[valx yax2+(as/2)*(comptype<2)],''String'',''',str,''')'];
            else
               cmdstr=['text(valx,yax2+(as/2)*(comptype<2),''',str,''')'];
            end;
            hold on;
            eval(cmdstr);
            hold off;
        end;


        %3****** check menue input
        if z==32 z=zold;  % old option: <space>

        else

            %*** no cursor keys on Sparc and PC
            if ~comptype
              if z==51 z=31; end; %down
              if z==52 z=30; end; %up
              if z==50 | z==3  z=29; end; %right
              if z==49 | z==2  z=28; end; %left
              if z==54 z=12; end; %page down
              if z==53 z=4;  end; %end
            end;
            if comptype==1  %PC: num-lock keypad
              if z==50 z=31; end;
              if z==52 z=28; end;
              if z==51 z=12; end;
              if z==49 z=4; end;
              if z==54 z=29; end;
              if z==56 z=30; end;
            end;

            %=================================
            %4****** split check 1
            %=================================
            if z<64

                %if z==4    % move backward fast: <end>
                if z==111    % move backward fast: o
                    s3=abs(moveint+(s2-s1));
                    if s1<s3 s3=s1-1;
                    end;
                    s1=s1-s3;
                    s2=s2-s3;
                end

                %if z==12    % move forward fast: <page down>
                if z==112    % move forward fast: p
                    s3=s2-s1+moveint;
                    if (s2+s3)>length(var1)
                       s3=length(var1)-s2;
                    end;
                    s1=s1+s3;
                    s2=s2+s3;
                end

                if z==28    % move backward: <cursor left>
                    s3=s2-s1;
                    if s1<s3
                        s3=s1-1;
                    end;
                    s1=s1-s3;
                    s2=s2-s3;
                end

                if z==29    % move forward: <cursor right>
                    s3=s2-s1;
                    if (s2+s3)>length(var1)
                        s3=length(var1)-s2;
                    end;
                    s1=s1+s3;
                    s2=s2+s3;
                end

                if z==30    % zoom out: <cursor up>
                    i=round((s2-s1)/zoomf);
                    s1=s1-(zoomf-1)*i;
                    s2=s2+(zoomf-1)*i;
                    z=999;
                end

                if z==31    % zoom in: <cursor down>
                    i=round((s2-s1)/(2*zoomf));
                    s1=s1+(zoomf-1)*i;
                    s2=s2-(zoomf-1)*i;
                    z=999; % marker for z=zold later
                end

                if z==33   %  insert a beat, program finds near maximum: !
                    kinsbmax;
                end;

                if z==34   %  examine the lag between 2 time-series: "
                    klag;
                end;

                if z==35   % delete event manually: #
                    kdelbeat;
                end;

                if z==36   % find half between event: $
                    k2beat;
                end;

                if z==37   % find thirds between event: %
                    k3beat;
                end;

                %if z==38   % add 128sec interval in event-vector to right: &
                %kadd128r; end;
                if z==38   % add R-spike to left: &
                    krspikel;
                end;

                if z==39   % edit respiration times for breath holding study: '
                    if resp==1
                       kresp;
                    end
                    if exist('qtedit')==1
                       if qtedit==1
                          kqt;   % qt editing
                       end
                    end
                    if exist('icg_pep')==1
                       if icg_pep==1
                          labkpep;  % PEP editing
                       end
                    end
                end;

                if z==41   % edit respiration resets for breath holding study: )
                    kreset;
                end;

                if z==42   % print current plot: *
                    print;
                end;

                if z==43   % adjust move fast add factor: +
                    plotyes=0;
                    moveint=input('Move window how many seconds  ==>  ');
                    moveint=(moveint*samplerate)-(s2-s1);
                    z=12;
                end;

                if z==44   % move forward slow: >
                    s3=round((s2-s1)/2);
                    s1=s1-s3;
                    s2=s2-s3;
                end

                if z==45   % remove marked artifact interval: -
                    kartrem;
                end;

                if z==46   % move backward slow: <
                    s3=round((s2-s1)/2);
                    s1=s1+s3;
                    s2=s2+s3;
                end

                if z==47   % truncate spectrum plot: /
                    z=999;
                    disp('Truncate frequencies below Hz [<return>= no change]');
                    i=input('==>  ');
                    if ~isempty(i)
                        chz1=i;
                    end;
                    disp('Truncate frequencies above Hz [<return>= no change]');
                    i=input('==>  ');
                    if ~isempty(i)
                        chz2=i;
                    end;
                end;

                if z==48   % goto next lookat-interval: 0
                    z=999;
                    if exist('lookat')
                        [i,j]=size(lookat);
                        if i
                            lind=lind+1; lookatyes=1;
                            if lind>i
                                lind=i;
                                plotyes=0;
                            end;
                            s1=lookat(lind,1);
                            s2=lookat(lind,2);
                        end;
                    end;
                end;

                if z==57   % goto previous lookat-interval: 9
                    z=999;
                    if exist('lookat')
                        [i,j]=size(lookat);
                        if i
                            lind=lind-1; lookatyes=1;
                            if lind<1
                                lind=1;
                                plotyes=0;
                            end;
                            s1=lookat(lind,1);
                            s2=lookat(lind,2);
                        end;
                    end;
                end;

                if z==60  % goto left event interval: shift - <
                    kevintl;
                end;

                if z==61   % adapt y-axis range of variables: =
                    z=999;
                    if radapt
                        radapt=0;
                    else radapt=1;
                    end;
                end;

                if z==62  % goto right event interval: shift - >
                    kevintr;
                end;

               if z==63   % insert missing beats manually: @
                    kinsbeat;
                end;

            %=================================
            %4****** split check 2
            %=================================
            elseif z<100



                if z==65   % script file for current analysis: A
                    kanalys;
                end;

                if z==66   % regression of breathing procedure: B
                    kregress;
                end;

                if z==67   % copy recent graph: C
                    kcopy;
                end;

                if z==68   % examine the x- and y-distances of two points: D
                    kdist;
                end;

                if z==69   % exchange variables: E
                    kvarch;
                end;

                if z==70  % filter: F
                    z=999;
                    kfilter;
                end;

                if z==71  % load binary file from Vitaport: G
                    z=999;
                    vitaload;
                    defcal;
                    defcal2;
                    figure(1);
                    fig=1;
                    clf; %Amy - replaced clg with clf
                end;

                if z==72  % hard copy of spectrum plot: H
                    kspecsav;
                end;

                if z==73  % Interpolate: I
                    kinterp;
                end;

                if z==74   % jump to copy number : J
                    kcjump;
                end;

                if z==75   % define factor for skip through events: K
                    skipfact=input('How many events to drop during skipping through  ==>  ');
                end;

                if z==76   % change linetypes: L
                    klinech;
                end;

                if z==77   % menue under graph on/off: M
                    if menueyes
                        menueyes=0;
                    else
                        menueyes=1;
                    end;
                    z=999;
                end;

                if z==78   % Negativate one var: N
                    knegativ;
                end;

                if z==79   % change points manually: O
                    kpoints;
                end;

                if z==80   % examine the x- and y-proportions of 3 points: P
                    kprop;
                end;

                if z==81  % set initial values: Q
                    ksetinit;
                end;

                if z==82    % repeat skip through events or move: R
                    repeat=1;
                end;

                if z==83  % spectrum plot: S
                    z=999;
                    plotyes=1;
                    odd=2;
                    doubleyes=1;
                    if spec==1
                        spec=0;
                        clf; %Amy - replaced clg with clf
                    else
                        spec=1;
                    end;
                end;

                if z==84  % type of spectrum: T
                    kspectyp;
                end;

                if z==85  % mark breath maxima in calibration: U
                    kbmax;
                end;

                if z==86  % add or hide other variables: V
                    kaddvar;
                end;

                if z==87  % find onsets of NSFs: W
                    knsfons;
                end;

                if z==88   % shift var1 in x-direction (left or right): X
                    kshiftx;
                    end;

                if z==89   % shift var2 in y-direction (up or down): Y
                    kshifty;
                end;

                if z==90  % Statistics: Z
                    kstatist;
                end;

                if z==91   % goto left event: [
                    kevleft;
                end;

                if z==93   % goto right event: ]
                    kevright;
                end;

                %if z==94   % delete wrong tvalue manually: ^
                %kdeltwav; end;
                %if z==94   % add 128sec interval in event-vector to left: ^
                %kadd128l; end;
                if z==94   % add R-spike to right: ^
                    krspiker;
                end;


                if z==97   % adjust fine: a
                    kadjust;
                end;

                if z==98   % begin of artifact: b
                    kartbeg;
                end;

                if z==99   % circles on plot of var1 on/off: c
                    if circle
                        circle=0;
                    else
                        circle=1;
                    end;
                    z=999;
                end;

            %=================================
            %4****** split check 3
            %=================================
            else

                if z==100  % double/single plot: d
                    if doubleyes==1
                        doubleyes=0;
                    else
                        doubleyes=1;
                    end;
                    plotyes=1;
                    spec=0;
                    odd=2;
                    z=999;
                end;

                if z==101  % end of artifact: e
                    kartend;
                end;

                if z==102  % find lokal maxima: f
                    %kfmax;
                    findpco2;
                    %findresp;
                    valueyes  =1;           % display values of events
                    val       =mv;          % value
                    valtime   =mt;          % time of event for value
                    event1    =[];  % events 1-3 to mark
                    event2    =[]; %mint;
                    event3    =[];
                    event1yes =0;
                    event2yes =0;
                    event3yes =0;
                    evscan    =[];  % define which event to skip through: k
                end;

                if z==103  % grid on/off: g
                    if gridyes gridyes=0;
                    else
                        gridyes=1;
                    end;
                    z=999;
                end;

                if z==104  % find breathmax in calibration procedure automatically: h
                    kbmaxa;
                end;
                %kbmaxa3;  % for Spiraobag; manual definition

                if z==105  % show whole interval: i
                    kinterv;
                end;


                if z==106  % jump to event number : j
                    kevjump;
                end;

                if z==107  % define which event to skip through: k
                    kevskip;
                    kevright;
                end;

                if z==108  % location of events in window: l
                    plotyes=0;
                    skiploc=input('Location of events in window (0-1) ==>  ');
                    z=999;
                end;

                if z==109  % mark events: m
                    kaddev;
                end;

                if z==110  % new interval to display: n
                    kninterv;
                end;

                if z==111  % online help
                    khelp;
                end;

                if z==112  % points/lines for var1: p
                    if strcmp(ltv1,'x')
                        ltv1='-';
                        scalefact=1;
                        scale=1;
                    else
                        ltv1='x';
                        scalefact=samplerate;
                        scale=2 ;
                    end;
                    z=999;
                end;

                if z==113  % quit: q
                    z=0;
                end;

                if z==114  % load (read) specified data: r
                    kloaddat;
                end;

                if z==115  % save specified data: s
                    ksavedat;
                end;

                if z==117  % unzoom: u
                    s1=s1old;
                    s2=s2old;
                    fig=1;
                    z=999;
                end

                if z==118   % display values and value-markers on graph on/off: v
                    z=zold;
                    if valueyes
                        valueyes=0;
                    else
                        valueyes=1;
                    end;
                    z=999;
                end;

                if z==119  % which events to display: w
                    kevdisp;
                end;

                if z==120  % x-axis scale: x
                    if scale==1
                        scale=2;
                        scalefact=samplerate;
                    else
                        scale=1;
                        scalefact=1;
                    end;
                    z=999;
                end;

                if z==121  % y-axis truncate: y
                    ktrunc;
                end

                if z==122  % zoom: z
                    if ~spec
                        [i1,i2,z]=ginput(2);
                        i1=sort(i1) .*scalefact;
                        s1old=s1; s2old=s2;
                        s1=i1(1); s2=i1(2);
                        z=999;
                        figz=figz;
                        fig=figz;
                    else
                        plotyes=0;
                    end;
                end;

                if z==123  % goto left copy:  <shift>+[
                    recallnumyes=1;
                    remi=remi-1;
                    if remi<1 remi=1; %plotyes=0;
                    else  s1=rems1(remi); s2=rems2(remi); end;
                end;

                if z==125  % goto right copy:  <shift>+]
                    recallnumyes=1;
                    remi=remi+1; j=length(rems1);
                    if remi>j remi=j; %plotyes=0;
                    else  s1=rems1(remi); s2=rems2(remi); end;
                end;

                if z==126   % insert specified no. of missing beats manually: ~
                kallbeat; end;


            end;
            %4****** split check 1-3

        end;
        %3****** if z==32 (old option)

    end;
    %2****** if repeat

end;
%1****** while z

%*** if artifacts were marked
artbegin=sort(artbegin);
artend=sort(artend);
[artbegin,artend,d]=cut_len(artbegin,artend);
if d
  if d<0 disp([int2str(-d),' begin of artifact markers were missing.']);
  else   disp([int2str(d),' end of artifact markers were missing.']);
end; end;
artinterval=[artbegin artend];
