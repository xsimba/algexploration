function ANSLABeda(curr_dataset,varname,data_dir)

%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

% Modified by Amy 6/9/15

% clear
% clearvars -except data_Empatica data_Empatica_mod data_Powerlab data_Sami
close all

%%
%%%%Amy  % or uncomment out "Have user select a folder" and "load RESP data" sections (lines 73-100)
% data_Empatica = import_Empatica_folder;
% SC = data_Empatica_mod.GSR.GSR_data;

% % % % curr_dataset = data_Empatica_mod;
% % % % SC = curr_dataset.GSR.GSR_data;
% % % % varname = curr_dataset.filename;
% % % % data_dir = curr_dataset.folderpath;
% % % % % act0 = curr_dataset.accel.magaccel;

% temp start
SC = curr_dataset;

% temp end

mkdir(data_dir,'\ANSLAB_plots');
red_dir=[data_dir,'\ANSLAB_plots']; 



%%%%

%% *** Program settings
rise1=0.000;  %(0.000) Minimal increase for qualifying point (QP) [0.004]
rise2=0.2;    % (0.02) Minimal increase from QP to be regarded as NSF
forward1=1;   %    (1) Window for search for QP [sec]
forward2=3.5; %  (3.5) window from QP for search for NSF [sec]
filtyes=1;    %    (1) Apply low pass filter
cplotyes=1;   %    (1) Display control plots for parameter distribution
startdel=0;   %    (0) Set first startdel sec to the following value (if there is startup drift on amp)
maxwin=12;    %   (12) Window [sec] for search of valid local max after onset (=> maximal rise time)
              %        maxwin=0 switches off local maxima find, only looks for general maxima between onsets
scledityes=1; %    (0) Display SCL first and enable outrect function
maxblen=60;   %   (60) Maximum NSF interval length, if longer: insert NaN value (0 for NSF-rate) after maxblen/2


%% Sets defaults for how plots are displayed
% set(0,'DefaultFigureColor',[0 0 0])
% set(0,'DefaultAxesXColor',[1 1 1])
% set(0,'DefaultTextColor',[1 1 1])
% set(0,'DefaultAxesYColor',[1 1 1])
% set(0,'DefaultAxesZColor',[1 1 1])
% set(0,'DefaultAxesColor',[0 0 0])
% set(0,'DefaultLineColor',[1 1 1])
% ColorMat =  [1     1     0;...
%              1     0     1;...
%              0     1     1;...
%              1     0     0;...
%              0     1     0;...
%              0     0     1];
% set(0,'DefaultAxesColorOrder',ColorMat)

% Check to make sure Matlab version greater than 6
MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
end

%% Have user select a folder and save folderpath to data_dir
% if ~exist('data_dir') | ~exist('red_dir')
%     data_dir = PsyPath(1);
% %     cd 'C:\Users\amy.liao\Documents\GSRData\database';
%     if MVersNr>6.1
%         data_dir=uigetdir(strrep(data_dir,'*',''), 'Select study folder:');        %Choose data_dir
%     else
%         [NoUse,data_dir]=uiputfile(strrep(data_dir,'*',''), 'Create file in study folder:');
%     end
%     if isequal(data_dir,0);return;end
%     PsyPath(2,[data_dir,filesep]);
% %     red_dir=[data_dir,'\s']; 
%     red_dir=[data_dir,''];  %%Amy - set red_dir = data_dir
% end
    
%% *** Load RESP data
chd(data_dir)  % cd to data_dir
%FL=filelist('*.txt',0.25);
% % % [filename, data_dir] = uigetfile('*.txt','Select data file');
% % % if isequal(data_dir,0);return;end
% % % eval(['load ',filename,';']);
% % % varfilename = filename(1:end-4);
% % % l=length(filename);
% % % varname=filename(1:8);
% % % subjn=str2num(varname(4:6));
% % % studystr=varname(1:3);
% % % eval(['SC=',varfilename,';']); 
% % % eval(['clear ',varfilename,';']);






%% *** resample if necessary
[SC,SR] = AskResampleData(SC,25,'EDA');  % Resample data to 25 Hz and save in SC
% format: [DataResampled,OriginalSR] = AskResampleData(Data, TargetSR, DefaultID)


%% *** Initialization
lucas_flag=0;
delsampend=0;
byes=0;
bno=1;
bplotyes=0;

len=length(SC)/25;

% act0=zeros(size(SC));

%% Call findski to analyze eda data
findski5_2


%% *** Resample SCL and NSF to instantaneous 10 Hz
disp('Conversion of EDA parameters into 0.10 sec SCL and NSF epochs');
ep=10;
scl0=decfast(ipfast(y,ep),sr);
nsfr0=decfast(ipfast(nsfr0,5),2);
nsfv0=decfast(ipfast(nsfv0,5),2);
rtim0=decfast(ipfast(rtim0,5),2);
hrec0=decfast(ipfast(hrec0,5),2);
reslen=length(scl0);
scl0=nan_lip(cut_fill(scl0,reslen))';
nsfr0=nan_lip(cut_fill(nsfr0,reslen))';
nsfv0=nan_lip(cut_fill(nsfv0,reslen))';
rtim0=nan_lip(cut_fill(rtim0,reslen))';
hrec0=nan_lip(cut_fill(hrec0,reslen))';


%% Plot analysis graphs
t=(1:length(scl0))/ep;
figure('position',[252   153   943   700]);
subplot(5,1,1)
plot(t,scl0)
set(gca,'XTickLabel',[]);
title('SCL')
ylabel('uS')
subplot(5,1,2)
plot(t,nsfr0)
set(gca,'XTickLabel',[]);
title('NSF rate')
ylabel('1/min')
subplot(5,1,3)
plot(t,nsfv0)
set(gca,'XTickLabel',[]);
title('SCR amplitude')
ylabel('uS')
subplot(5,1,4)
plot(t,rtim0)
set(gca,'XTickLabel',[]);
title('SCR rise time')
ylabel('sec')
subplot(5,1,5)
plot(t,hrec0)
title('SCR half-recovery time')
xlabel('time [sec]')
ylabel('sec')

%% *** Displays reduced data
cd(red_dir)
S=[scl0 nsfr0 nsfv0 rtim0 hrec0];
disp(['save ',varname,'s.txt -ascii -tabs S']);
eval(['save ',varname,'s.txt -ascii -tabs S']);

disp('Results: mean SCL, mean NSF rate, mean SCR amplitude, mean rise time, mean half-recovery time: ');
disp(nanmean(scl0));
disp(nanmean(nsfr0));
disp(nanmean(nsfv0));
disp(nanmean(rtim0));
disp(nanmean(hrec0));
end