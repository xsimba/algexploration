function [DefFilePath,DefPath] = PsyPath(RWStatus,PathOrFileExt,FileStr,PrintStatus);
%
%	SetDefPath  	function [DefFilePath] = SetDefPath(RWStatus,PathOrFileExt,FileStr);
%	RWStatus==0  | Clear  | clear;	RWStatus==1  | R  | r  => Read; 	RWStatus==2  | W  | w => Write	;
%

%   ANSLAB - Autonomic Nervous System Laboratory                          
%   � Copyright 2005 Frank Wilhelm & Peter Peyk                                      
%                                                                              
%   This program is free software; you can redistribute it and/or              
%   modify it under the terms of the GNU General Public License                
%   as published by the Free Software Foundation; either version 2             
%   of the License, or (at your option) any later version.                     
%                                                                              
%   This program is distributed in the hope that it will be useful,            
%   but WITHOUT ANY WARRANTY; without even the implied warranty of             
%   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              
%   GNU General Public License for more details.                               
%   You should have received a copy of the GNU General Public License          
%   along with this program; if not, write to the Free Software                
%   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
%   USA. 

MVersion = version;
MVersNr = str2num(MVersion(1:3));
if MVersNr>6
    eval('warning off;');
else
	eval('filesep = ''\''');
end
if nargin<4;PrintStatus = 0;end
if nargin<3;FileStr=[];end
if nargin<2;PathOrFileExt=[];end
if nargin<1;RWStatus='r'; end
if isempty(RWStatus); return; end

if strcmp(RWStatus,'r') | strcmp(RWStatus,'R') | strcmp(RWStatus,'read') | strcmp(RWStatus,'Read')
	RWStatus=1;
elseif strcmp(RWStatus,'w') | strcmp(RWStatus,'W') | strcmp(RWStatus,'write') | strcmp(RWStatus,'Write')
	RWStatus=2;
elseif strcmp(RWStatus,'Clear') | strcmp(RWStatus,'clear')
	RWStatus=0;
end
DefPath=0;
DefFile=['ANSLABDefPath.',computer];
%=================================================================
[nouse1,ANSLABFolder]=SepFP(which('ANSLAB'));
DefFilePath = [ANSLABFolder,'ANSLABUtil',filesep,'ANSLABDefault',filesep,DefFile];
fid = fopen(DefFilePath,'r','b');
if fid==-1
    fid = fopen(DefFilePath,'wt');
    fprintf(fid,pwd);
end
fclose(fid);
%===========================================================
if RWStatus==0	 %Clear;
   	if ~strcmp(DefFilePath,'..') & ~strcmp(DefFilePath,'');
		DefFilePathFid=fopen(DefFilePath,'w','b');
		if DefFilePathFid~=-1
			fprintf(DefFilePathFid,'%s','');
			fclose(DefFilePathFid);
            if PrintStatus
				fprintf(1,'Clear default path:\n\n');
				disp(DefFilePath);
            end
			return;
		else
            if PrintStatus
				fprintf(1,'Can not clear:\n\n');
				fprintf(1,['DefaultPath.' computer]);
				fprintf(1,'File can not be opened.\n\n');
            end
			DefFilePath=[];
			return;
		end
	else
        if PrintStatus
		    fprintf(1,'Can not clear:\n\n');
		    fprintf(1,['DefaultPath.' computer]);
		    fprintf(1,'Can not find this file in Matlab search path.')
        end
		DefFilePath=[];
		return;
	end
end
%===========================================================
if RWStatus==2; %Write;
   	if ~strcmp(DefFilePath,'..') & ~strcmp(DefFilePath,'');
		DefFilePathFid=fopen(DefFilePath,'w','b');
		if DefFilePathFid~=-1
			if ~isempty(PathOrFileExt)
				fprintf(DefFilePathFid,'%s',PathOrFileExt);
				fclose(DefFilePathFid);
                if PrintStatus
				    fprintf(1,'Write default path:\n\n')
				    disp(PathOrFileExt);
				    fprintf(1,'to "path default file":\n\n')
				    disp(DefFilePath);
                end
				return;
			end
		else
            if PrintStatus
			    fprintf(1,'Can not write:\n\n')
			    fprintf(1,PathOrFileExt); fprintf(1,'\n\n');
			    fprintf(1,'to "path default file" since the file:\n\n')
			    fprintf(1,['DefaultPath.' computer]); fprintf('\n\n');
			    fprintf(1,'can not be opened.')
            end
			DefFilePath=[];
			return;
		end
	end

    if PrintStatus
	    fprintf(1,'Can not write:\n\n')
	    disp(PathOrFileExt); fprintf('\n\n');
	    fprintf(1,'to "path default file" since the file:\n\n')
	    fprintf(1,['DefaultPath.' computer]); fprintf('\n\n');
	    fprintf(1,'does not exist.')
    end
	DefFilePath=[];
	return;
end
%===========================================================
if ~isempty(PathOrFileExt) & strcmp(computer,'PCWIN')
 	PathOrFileExt=[filesep,PathOrFileExt];
end
if ~strcmp(DefFilePath,'..') & ~strcmp(DefFilePath,'');
    DefFilePathFid=fopen(DefFilePath,'r','b');
    if DefFilePathFid~=-1
        DefPath=fgetl(DefFilePathFid);
        if MVersNr>6
        	if isequal(DefPath,-1);DefPath = [];end
        else
        	if ~isstr(DefPath);DefPath = [];end
        end
        fclose(DefFilePathFid);
        IndSepPathStr=findstr(DefPath,filesep);
        if strcmp(computer,'MAC2')
            NSepStr=length(IndSepPathStr)-1;
        else
            NSepStr=length(IndSepPathStr);
        end;
        ExistDefPath=0;
        while ExistDefPath~=7 & NSepStr>0
            ExistDefPath=exist(DefPath);
            if ExistDefPath==7 %Dir
                DefFilePath=[DefPath,PathOrFileExt];
            else
                DefPath=DefPath(1:IndSepPathStr(NSepStr));
                NSepStr=NSepStr-1;
            end
        end
        if ExistDefPath~=7 & ~isempty(FileStr)
            DefFilePath=which(FileStr);
            if ~strcmp(DefFilePath,'..');
                [DefFile,DefPath]=SepFP(DefFilePath);
            end
        end
    end
end
if ~isempty(PathOrFileExt)
    if isempty(DefPath) | DefPath==0
        DefFilePath=which(PathOrFileExt);
        if strcmp(DefFilePath,'..');
            DefFilePath=PathOrFileExt;
        else
            [DefFile,DefPath]=SepFP(DefFilePath);
            DefFilePath=[DefPath,PathOrFileExt];
        end
    else
        if strcmp(DefPath(size(DefPath,2)),filesep)
            if strcmp(PathOrFileExt(1),filesep)
                DefFilePath=[DefPath,PathOrFileExt(2:size(PathOrFileExt,2))];
            else
                DefFilePath=[DefPath,PathOrFileExt];
            end
        else
            if strcmp(PathOrFileExt(1),filesep)
                DefFilePath=[DefPath,PathOrFileExt];
            else
                DefFilePath=[DefPath,filesep,PathOrFileExt];
            end
        end
    end
else
    if DefPath==0
        DefFilePath=[pwd,'*'];
    else
        DefFilePath=[DefPath,'*'];
    end
end

%==========================
return;
