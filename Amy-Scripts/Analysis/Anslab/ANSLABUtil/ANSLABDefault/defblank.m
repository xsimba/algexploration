% Blank definition file for EXAM: type defblank, then def, then exam
% F.W. last update 10-24-95

MVersion = version;
MVersNr = str2num(MVersion(1:3));



%*** Define variables
int=6;        % Display interval [sec]
moveint=60;   % Move interval [sec]
samplerate=2;  

secvar2=0; secvar3=0; secvar4=0; 

if exist('var1')~=1   % first variable
    disp('The variable you want to display needs to be renamed to var1.');
    if MVersNr>6
       var1 = [];
    end
end;
title1str =[];        % title of graph
ltv1      ='-';       % linetype (appears in title, too)
yaxisstr  = 'Units' ;

if exist('var2')==1
   secvar2=1;          % flag for second variable to plot (0)
   
   if MVersNr>6
       var2 = [];
   end
   [var1,var2]=adaptlen(var1,var2);  
end;
title2str = []; 
ltv2      ='-';

if exist('var3')==1
   secvar3=1;
   
   if MVersNr>6
       var3 = [];
   end
   [var1,var3]=adaptlen(var1,var3); 
end;
title3str = [];
ltv3      = '-';         

if exist('var4')==1
   secvar4=1;
   
   if MVersNr>6
       var4 = [];
   end
   [var1,var4]=adaptlen(var1,var4); 
end;
title4str = []; 
ltv4      ='-';
 
%*** Define events
% any event must be adapted to correct samplerate, 
% e.g. event1=rtime*samplerate/384

if ~exist('artbegin')
artbegin  =[];          % artifact marker 
artend    =[];          % artifact marker
end;

valueyes  =0;          % display values of events
val       =0;          % value
valtime   =0;          % time of event for value

event1    =[]; %mintime;  % events 1-3 to mark
event2    =[];
event3    =[];
event1yes =0;
event2yes =0; 
event3yes =0;
evscan    =[];  % define which event to skip through: k

clear RESPEDIT;  %indicates plot of circles in exam
subvarplot=0;

