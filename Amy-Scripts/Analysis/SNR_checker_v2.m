%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <SNR_checker>
% Content   : Overall script to check the (pseudo) SNR of the signal for ECG or PPG 
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
%%% Modified to take in v5 data structure
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
% 
%  Modification and Version History: 
%  | Developer  | Version |    Date    |     Changes      |
%  | EC Wentink | 0.1     | 18/02/2015 | first version    |
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input:
% data: in the format of the data struct from the Rosetta.xls
% signal type: < 'ecg', 'ppg.a', 'ppg.b', ....>
% Note: the data struct must have the detected beats on the signal
% available else it will not provide the SNR.
% 
% Input examples:
% [data] = SNR_checker(data,'ecg')
% [data] = SNR_checker(data,'ppg.f')
%
% Accompanying scipts:
% <AVGBDet2>
% 
% Output:
% the SNR estimation in time domain of the input signal
% Steps:
% The signal is cut in windows. <SNR_checker> 
% The detected beats are found in each window to detect all the R-peaks or upstrokes in the window. <SNR_checker>
% For each window a fixed beat duration interval of 1Hz is chosen to work <SNR_checker>
% best
% Each beat is now cut out using this beat duration interval, so each beat is equally long.<AVGBDet2> 
% Then the average beat is calculated. <AVGBDet2> 
% The Average beat is then used to reconstruct the signal and in every beat location an �average � beat is relocated. <AVGBDet2>
% The subsequent signal is then the �signal� and this subtracted from the original segment that was used as input.<AVGBDet2>
% The signal that is left over is called the �noise� <AVGBDet2>
% From this the power in the signal and in the noise are calculated:       
% SNR = (RMS(signal)/RMS(noise))^2 <AVGBDet2>
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [SNR,t_snr] = SNR_checker_v2(signal,timestamps,beats,SigID,wind_sec)

Char = char(SigID);
SigType1 = cellstr(Char(1:3)); % First 3 characters
SigType = SigType1{1};
Fs=128;
e=1;
pause on;


if strcmpi(SigType1,'ecg')
%     if isfield(data.(SigType),'beats')
        Wn = 0.4/64;                   % Normalized cutoff frequency        
        [z,p] = butter(2,Wn,'high');   %initialize the filter
        minHR=1; %minimal HR, determines the width of the average beat
        wind = wind_sec*Fs; % determines the windowsize15
        udr = 1*Fs; %determines the update rate
        STDqrs = zeros(5,udr);
        tt= 0.3; % set the time in seconds where to start the window before the R-peak
        for a = 1:udr:size(signal,2)-wind
            ECG4sec = filtfilt(z,p,signal(a:a+wind)); % HP filter to get rid of some MA
            t1 = timestamps(a); % the start-timestamp beloning to the window
            t2 = t1+(wind/Fs); % the stop-timestamp beloning to the window
            sta = find(beats>t1,1);% find the first beat after the start timestamp in the window
            sto = find(beats>t2,1)-1; % find the last beat before the stop timestamp in the window
            RP = beats(1,sta:sto)-t1; % the R-peaks inside the window
            if length(RP)>1
%                [SNR(e),STDqrs(e,:)] = SNRcalc2(ECG4sec',Fs,minHR,RP ); % calculate the SNR
               [SNR(e),STDqrs(e,:)]=AVGBDet2(ECG4sec',RP,Fs,minHR,tt);
            else
               SNR(e)=NaN; % if no R-peaks set SNR to really low
               STDqrs(e,:)=NaN; % if no R-peaks the SD is 0 (also unlikely)
            end
            e=e+1;
        end
        t_snr= (0:udr/Fs:length(SNR)*(udr/Fs)-1) +(wind/Fs)+ timestamps(1); % the timestamp for the SNR

%         % plot the results
%         if (1)
%         f = figure;
%         A(1)=subplot(2,1,1);plot(timestamps,signal);hold on;%plot(data.(SigType).beats.signal,data.(SigType).beats.debug.usamp,'r+'); 
%         title(['original signal ' SigID]);ylabel('ECG')
%         A(2)=subplot(2,1,2);plot(t_snr,SNR);title('SNR');ylabel('SNR');hline(0.5)
% %         A(3)=subplot(3,1,3);plot(t_snr,mean(STDqrs,2)); title('SD of the avarage beat');ylabel('SD');xlabel('Time (s)')
%         linkaxes(A,'x');
%         end

        % add SNR to the data struct
%         data.(SigType).snr.signal=SNR;
%         data.(SigType).snr.timestamps=t_snr;
    
%     else
%         disp(['no detected beats available in ' SigType])
%         return
%     end
%     % 
% else
%     chan1 = cellstr(Char(5)); % last character
%     chan = chan1{1};
% %     if isfield(data.(SigType).(chan), 'beats')
%     
%         minHR=1; % minimal HR, determines the width of the average beat
%         wind = 4*Fs; % determines the windowsize
%         udr = 1*Fs; %determines the update rate
%         Wn = 0.4/64;                   % Normalized cutoff frequency        
%         [z,p] = butter(2,Wn,'high');   % make filter
%         STDqrs = zeros(5,udr);
%         tt= 0.2 ;% set the time in seconds where to start the window before the R-peak
% 
%         for a = 1:udr:size(signal,2)-wind
%             PPG4sec = filtfilt(z,p,signal(a:a+wind)); % get rid of some MA
%             t1 = data.timestamps(a); % the start-timestamp beloning to the window
%             t2 = t1+(wind/Fs); % the stop-timestamp beloning to the window
%             sta = find(beats.upstroke>t1,1); % find the first beat after the start timestamp in the window
%             sto = find(beats.upstroke>t2,1)-1; % find the last beat before the stop timestamp in the window
%             RP = beats.upstroke(1,sta:sto)-t1; % the R-peaks inside the window
%             if length(RP)>1
%                [SNR(e),STDqrs(e,:),d(e)]=AVGBDet2(PPG4sec',RP,Fs,minHR,tt);
% 
%             else
%                SNR(e)=0.01; % if no upstrokes set SNR to really low
%                STDqrs(e,:)=0; % if no upstrokes set SD to 0
%                d(e)=0;
%             end
%             e=e+1;
% %             pause(0.25) % set to something like 0.25 if you want to look at data
%         %     in slow motion. Works well if you set the figure in SNRcalc on
%         clear RP
%         end
% 
%         t_snr= (0:udr/Fs:length(SNR)*(udr/Fs)-1) +(wind/Fs)+timestamps(1); % the timestamp for the SNR
% 
%         % 
%         if (1)
%         figure;
%         A(1)=subplot(2,1,1);plot(timestamps,signal);hold on;plot(beats.upstroke,beats.debug.usamp,'r+'); title(['original signal ' SigID]);ylabel('PPG')
%         A(2)=subplot(2,1,2);plot(t_snr,SNR);title('SNR');ylabel('SNR');hline(1)
% %         A(3)=subplot(3,1,3);plot(t_snr,d);title('DTW per average wave, needs update still');hline(23000);hline(70000)
% %         A(3)=subplot(3,1,3);plot(t_snr,mean(STDqrs,2)); title('SD of the avarage beat');ylabel('SD');xlabel('Time (s)')
%         linkaxes(A,'x');
%         end
%         % add SNR to the data struct
% %         data.(SigType).(chan).snr.signal=SNR;
% %         data.(SigType).(chan).snr.timestamps=t_snr;
% %     else
% %             disp(['no detected beats available in ' SigType])
% %             return
% %     end
end



