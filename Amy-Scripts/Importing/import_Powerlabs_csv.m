function [data] = import_Powerlabs_csv

%% Import data from text file.
% Function for importing data from individual text files collected from Empatica:

%% Initialize variables.
[file,path] = uigetfile;    %Open browse folder to select file
filename = [path file];
% filename = filepath;

%% Create output variable
% data = [dataArray{1:end-1}];
data_raw = csvread(filename, 9, 0);
% data_raw_header = csvread(filename, 0,1,[0,1,8,1]);

data.GSR.GSR_time_1 = csvread(filename, 1,1,[1,1,1,1]);
% data.GSR.GSR_samplerate = 1/csvread(filename, 0,1,[0,1,0,1]);
% data.GSR.GSR_range = csvread(filename, 5,1,[5,1,5,1]);
data.GSR.GSR_samplerate = 1/(data_raw(3,1)-data_raw(2,1));

data.GSR.GSR_data = data_raw(1:end,2);
data.GSR.GSR_time = data_raw(1:end,1);

%% Clear temporary variables
% clearvars filename delimiter formatSpec fileID dataArray ans;

end