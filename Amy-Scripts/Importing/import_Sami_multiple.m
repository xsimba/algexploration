%% Import parameters for ECG data
% timestart = [1436897579,1436902860,1436907780,1436914979]*1000; %'Arvind','David','Arjun','Reana' 
% timeend = [1436899680,1436904479,1436910180,1436916360]*1000; %'Arvind','David','Arjun','Reana'
% timestart = [1437687600,1437689280,1437758399,1437760500]*1000; %'Tiffany','Mike','Jasmine','Praveen'
% timeend = [1437690059,1437691919,1437760500,1437763200]*1000; %'Tiffany','Mike','Jasmine','Praveen'
% timestart = [1436833200,1436829599]*1000; %'Amy','Monique'
% timeend = [1436905800,1436905380]*1000;%'Amy','Monique'
% timestart = [1438212299, 1438305599,1438347944]*1000; %Arjun2,Arvind2
% timeend = [1438215600, 1438310699,1438350300]*1000;%Arjun2,Arvind2
% timestart = [1438627860,1438638539,1438639980,1438711799,1438719899,1438797300,1438890900]*1000; %'Nishtha','Praveen','Jasmine','Ryan','Swati','Asif','Geoffrey'
% timeend = [1438628040,1438638960,1438640190,1438718100,1438722000,1438798800,1438807499]*1000;%'Nishtha','Praveen','Jasmine','Ryan','Swati','Asif','Geoffrey'
% timestart = [1438711799,1438719899,1438797300,1438890900]*1000; %'Ryan','Swati','Asif','Geoffrey'
% timeend = [1438718100,1438722000,1438798800,1438893899]*1000;%'Ryan','Swati','Asif','Geoffrey'
% timestart = [1438807440,1439402699,1439412299,1439414999,1439421599]*1000; %Dafina,Lindsay,Will,Narendra,Yelei
% timeend = [1439402100,1439404199,1439414700,1439416859,1439423459]*1000;%Dafina,Lindsay,Will,Narendra,Yelei

timestart = [1438627860,1438638539,1438639980];
timeend = [1438628040,1438638960,1438640190];
sdid = {'b30bcdf318a64cd3bcd6e88d81060cec', '7e46f8617d4b4318a512481ff963720e','8fb253cbf5c644408f47716b7b6dc6d4',...
    'e0261e74d6fa436bb6684a3b475e1681','bc097e5cdf5a4ae9be5e4a274c2343fc','9ccf0942cff74b4a8e4279c9f2456c5b'};
name = {'Nishta','Praveen','Jasmine'}; % name in order same as time
% band = {'SS','PtIr','Hudson','Acree','UHV','UHVclasp'}; % bandname in order same as sdid
band = {'SS','UHV','Hudson','PtIr','UHVclasp','Acree2'}; % bandname in order same as sdid

%% Import parameters for Hermes GSR data
% % timestart = [1436138519,1436565600,1437168599,1437523200,1437605999,1436134440]*1000;
% % timeend = [1436141219,1436568900,1437171299,1437526199,1437608999,1436135999]*1000;
% 
% sdid = {'290bd7a314b9492ab1344dfca3ebbee0'};
% % name = {'Amy','Arvind','Karem','Geoffrey','Arjun','Amy-base'}; % name in order same as time
% band = {'Hermes'}; % bandname in order same as sdid
% 
% 
% timestart = [1438655400,1438727339]*1000;
% timeend = [1438659179,1438729679]*1000;
% name = {'080315Arvind','080415Lindsay'};
%% Import from each band at each time
for i = 1:length(timestart) % loop through all experiments
    for j = 1:length(sdid) % loop through all bands
        p.startDate = timestart(i);
        p.endDate = timeend(i);
        p.sdids = sdid{j};
        p.uid = '736c02daae5e4ae980d8e65e00b01402';
 
%         data = readSamiPortalData(p,0,'aligned',@simbaSchemaSamiV4);
    try
        data = readSamiPortalData(p,1,'not aligned');
        eval(['data_',name{i},'_',band{j},'=data;']); % outputs unix time in ms
        
        if ~isfield(data,'physiosignal')
            fprintf(['data_',name{i},'_',band{j},'\n']);
        end
    catch
        fprintf(['data_',name{i},'_',band{j},'\n']);
    end
    end
end


