function [data, sampling_rate, timestamp_1] = import_Empatica_csv(filepath,datastart)

%% Import data from text file.
% Function for importing data from individual text files collected from Empatica:

%% Initialize variables.
% [file,path] = uigetfile;    %Open browse folder to select file
% filename = [path file];
filename = filepath;

%% Create output variable
% data = [dataArray{1:end-1}];
try
    data = csvread(filepath);

    if datastart>2
        timestamp_1 = data(1,1);
        sampling_rate = data(2,1);
    elseif datastart > 1
        timestamp_1 = data(1,1);
        sampling_rate = NaN;
    else
        timestamp_1 = NaN;
        sampling_rate = NaN; 
    end
    data = data(datastart:end,:);
catch 
    timestamp_1 = NaN;
    sampling_rate = NaN; 
    data = [];
end
%% Clear temporary variables
% clearvars filename delimiter formatSpec fileID dataArray ans;

end