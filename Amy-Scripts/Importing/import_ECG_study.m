datdir = 'C:\Users\amy.liao\Documents\GSRData\database\071415_ElectrodeVar_ECG\';
mkdir(datdir,'AnalysisPlots');
cd ([datdir, 'AnalysisPlots']);
list = who('data*');
offset = 45;
c=1;
subplotind = [1 2 3 7 8 9; 4 5 6 10 11 12];

for i = 1:length(list)
    name = strsplit(list{i},'_');

    % format data_structure
    eval(['temp_data = ',list{i},'.physiosignal.ecg.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.unixTimeStamps;']); 
    
    eval(['ECGdata.' name{2},'.',name{3},'.signal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.time_unix_all = temp_time;']);
    
    eval(['ECGdata.' name{2},'.',name{3},'.signal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.time_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.time_seconds = (ECGdata.' name{2},'.',name{3},'.time_unix - ECGdata.' name{2},'.',name{3},'.time_unix(1))/1000;']);
    
    
    eval(['temp_data = ',list{i},'.physiosignal.ecg.confidenceraw.signal;']);
    eval(['temp_time = ',list{i},'.physiosignal.ecg.confidenceraw.unixTimeStamps;']); 
    
    eval(['ECGdata.' name{2},'.',name{3},'.CIsignal_all = temp_data;']);
    eval(['ECGdata.' name{2},'.',name{3},'.CItime_unix_all = temp_time;']);
    
    eval(['ECGdata.' name{2},'.',name{3},'.CIsignal = temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.CItime_unix = temp_time(intersect(find(temp_time>=timestart_unix(',num2str(i),')),find(temp_time<=timeend_unix(',num2str(i),'))));']);
    eval(['ECGdata.' name{2},'.',name{3},'.CItime_seconds = (ECGdata.' name{2},'.',name{3},'.CItime_unix - ECGdata.' name{2},'.',name{3},'.CItime_unix(1))/1000;']);        

    
    eval(['ECGdata.' name{2},'.',name{3},'.CI_average = nanmean(temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')+offset*1000),find(temp_time<=timeend_unix(',num2str(i),')-offset*1000))));']);
    eval(['fprintf(''%s - %s: CI_mean = %d\n'',name{2},name{3},ECGdata.',name{2},'.',name{3},'.CI_average);']);
        
    eval(['ECGdata.' name{2},'.',name{3},'.CI_std = std(temp_data(intersect(find(temp_time>=timestart_unix(',num2str(i),')+offset*1000),find(temp_time<=timeend_unix(',num2str(i),')-offset*1000))));']);
    eval(['fprintf(''%s - %s: CI_std = %d\n\n'',name{2},name{3},ECGdata.',name{2},'.',name{3},'.CI_std);']);
    
    
    figure(floor((i-1)/6)+1)
    eval(['h',num2str(i),'(1) = subplot(4,3,',num2str(subplotind(1,c)),');']);
    eval(['plot(ECGdata.' name{2},'.',name{3},'.time_seconds, ECGdata.' name{2},'.',name{3},'.signal);']);
    xlabel('time (min)');
    ylabel('ECG');
    title(['ECG-raw: ',name{2},' - ',name{3}]);
    eval(['h',num2str(i),'(2) = subplot(4,3,',num2str(subplotind(2,c)),');']);
    eval(['bar(ECGdata.' name{2},'.',name{3},'.CItime_seconds, ECGdata.' name{2},'.',name{3},'.CIsignal);']);
    eval(['line(xlim,[ECGdata.' name{2},'.',name{3},'.CI_average, ECGdata.' name{2},'.',name{3},'.CI_average],''Color'',''r'');']);
    xlabel('time (min)');
    ylabel('CI');
    title(['ECG-CI: ',name{2},' - ',name{3}]);

    if c<6
        c=c+1;
    else
        c=1;
        saveas(figure(floor((i-1)/6)+1),[datdir, 'AnalysisPlots\','ECG-CI-',name{2}]);
        saveas(figure(floor((i-1)/6)+1),[datdir, 'AnalysisPlots\','ECG-CI-',name{2},'.jpg']);
    end
    
    
    eval(['linkaxes(h',num2str(i),', ''x'');']); 
    axis([0 180 -inf inf]);
    
  
    
    
    
end
