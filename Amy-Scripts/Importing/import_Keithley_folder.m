function data = import_Keithley_folder(fpathfolder)

% Adds Matlab code to path
addpath(genpath('C:\Users\amy.liao\Documents\GSRData\scripts'));


% clear all;  % COMMENT OUT FOR BATCH PROCESSING %
warning('off', 'MATLAB:MKDIR:DirectoryExists');
warning('off', 'MATLAB:Axes:NegativeDataInLogAxis');



%% Get names of all folders in fpath
d = dir(fpathfolder);
isub = [d(:).isdir]; %# returns logical vector
nameFolds = {d(isub).name}';
nameFolds(ismember(nameFolds,{'.','..'})) = [];

for j = 1:numel(nameFolds)
%% change directory to folder
    fpath = [fpathfolder,'\',nameFolds{j}];
    cd(fpath);

%% Load all datasets
    files = dir(fullfile(fpath,'*.csv'));
    files = {files.name}';
% analysisFolder = ['MatlabAnalysis_' date];  % Where to store plots and data
% mkdir(analysisFolder);


%% import data from all folders into 1 data structure
    for i = 1:numel(files)

        fname = files{i};
        disp(sprintf('File #%d: %s', i, fname));
    
        [data_temp,subjectID, electrodeID, runID] = import_Keithley_csv(fname);
        subjectID = nameFolds{j};
        lev = data_temp(:,1);
        res = data_temp(:,2);
        eval(['data.' subjectID '.' electrodeID '.' runID '.raw.level = lev;']);
        eval(['data.' subjectID '.' electrodeID '.' runID '.raw.resistance = res;']);
        
        % Calculate Statistics using last 5 data points for each
        % measurement
        num_levels = length(lev)/10;
        for k = 1:num_levels
            lev_avg(k) = mean(lev(((k-1)*10+6):((k-1)*10+10)));
            res_avg(k) = mean(res(((k-1)*10+6):((k-1)*10+10)));
            res_std(k) = std(res(((k-1)*10+6):((k-1)*10+10)));
            res_min(k) = min(res(((k-1)*10+6):((k-1)*10+10)));
            res_max(k) = max(res(((k-1)*10+6):((k-1)*10+10)));
        end
        eval(['data.' subjectID '.' electrodeID '.' runID '.level = lev_avg;']);
        eval(['data.' subjectID '.' electrodeID '.' runID '.resistance = res_avg;']);
        eval(['data.' subjectID '.' electrodeID '.' runID '.std = res_std;']);
        eval(['data.' subjectID '.' electrodeID '.' runID '.min = res_min;']);
        eval(['data.' subjectID '.' electrodeID '.' runID '.max = res_max;']);
    end

end
















