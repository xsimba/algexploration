function [data] = import_Powerlabs_mat
%% Import data from text file.
% Function for importing data from individual text files collected from Empatica:

%% Initialize variables.
[file,path] = uigetfile('*.mat', 'Select Powerlabs .mat file');    %Open browse folder to select file
filename = [path file];
% filename = filepath;

load(filename);



%% Create output variable
% data = [dataArray{1:end-1}];
% data_raw = csvread(filename, 9, 0);
% data_raw_header = csvread(filename, 0,1,[0,1,8,1]);
data_raw = data;
clearvars data;

data.GSR.GSR_time_1 = datastart;
% data.GSR.GSR_samplerate = 1/csvread(filename, 0,1,[0,1,0,1]);
% data.GSR.GSR_range = csvread(filename, 5,1,[5,1,5,1]);
data.GSR.GSR_samplerate = samplerate(1,1);

data.GSR.GSR_data = (data_raw*10^6)';
data.GSR.GSR_time = (datastart(1,1)/samplerate(1,1):1/samplerate(1,1):dataend(end)/samplerate(1,1))';

data.GSR.GSR_time = data.GSR.GSR_time/60;

data.folderpath = path;
data.filename = file;
%% Clear temporary variables
% clearvars filename delimiter formatSpec fileID dataArray ans;

end