function data = import_Empatica_folder

%% Import data from text file.
% Function for importing data from entire Empatica dataset
% Will store data for EDA, BVP, accelerometer, HR, and temperature in 1
% data structure for further analysis.

%% Initialize variables.

data_folder = uigetdir('C:\Users\amy.liao\Documents\GSRData\database\','Select Empatica folder');

%% Save data
data.folderpath = data_folder;
temp = strsplit(data_folder,'\');
data.filename = temp(end);


[data.GSR.GSR_data, data.GSR.GSR_samplerate, data.GSR.GSR_time_1] = import_Empatica_csv([data_folder '\EDA.csv'],3);
[data.BVP.BVP_data, data.BVP.BVP_samplerate, data.BVP.BVP_time_1] = import_Empatica_csv([data_folder '\BVP.csv'],3);
[accel_data, data.accel.accel_samplerate, data.accel.accel_time_1] = import_Empatica_csv([data_folder '\ACC.csv'],3);
data.accel.xaccel_data = accel_data(:,1);
data.accel.yaccel_data = accel_data(:,2);
data.accel.zaccel_data = accel_data(:,3); 
data.accel.magaccel_data = sqrt((data.accel.xaccel_data).^2+(data.accel.yaccel_data).^2+(data.accel.zaccel_data).^2);
clearvars accel_data;
[data.HR.HR_data, data.HR.HR_samplerate, data.HR.HR_time_1] = import_Empatica_csv([data_folder '\HR.csv'],3);
[data.temperature.temp_data, data.temperature.temp_samplerate, data.temperature.temp_time_1] = import_Empatica_csv([data_folder '\TEMP.csv'],3);
[data.tags.tags_data_raw] = import_Empatica_csv([data_folder '\tags.csv'],1);
[data.tags.tags_data] = (data.tags.tags_data_raw-data.GSR.GSR_time_1)/60;

data.GSR.GSR_time = 0:1/data.GSR.GSR_samplerate/60:(length(data.GSR.GSR_data)-1)/data.GSR.GSR_samplerate/60;
data.accel.magaccel_time = 0:1/data.accel.accel_samplerate/60:(length(data.accel.magaccel_data)-1)/data.accel.accel_samplerate/60;

end