RC_setup
c = loadSessionData('MySessions.xlsx');
%sessionList = {'10232014_sitstand'}; % name of the session to retrieve
sessionList = {'10242014_treadmill4'}; % name of the session to retrieve
InitializeTracksFromCSV(c,sessionList);

data.ssb.age = 30;
data.ssb.height = 178;
data.ssb.height = 80;

AddTracks(c,sessionList);
%naiveDataDisplay(data);
%fourChanComboDisplay(data);


% freq based HR
AddFreqInterbeatTracks_v2(c,sessionList);

% biosem
AddBiosemTracks(c,sessionList,'ibi_freq2');

% 
fMetric = setMetricsSessionData(c, sessionList{1});
load(fMetric);
heartRateQualityPlot(data);
