function hrci = HRCI( bb_biosem_CI, ciRaw, k, sigmaCutOff )
    

    ciDefault = ones(size(ciRaw));
    ciDefault(ciRaw==1) = 1;
    ciDefault(ciRaw==2) = 3;
    ciDefault(ciRaw==3) = 4;
    ciDefault(ciRaw==4) = 5;
    ciDefault(ciRaw==5) = 5;
    
    biosem_ci_prob = 0.5+(atan(k*(bb_biosem_CI-sigmaCutOff))/pi);
    hrci = round(ciDefault.*biosem_ci_prob);
    hrci = max(ones(size(hrci)), hrci);
    

end

