%
% Research Console Test Script
%
% This script demonstrates loading a dataset from Sami into
% the matlab workspace.
%
% A. Khalak, 8/14/2014

%
% load session references and select WebGuiCsv
%
c = loadSessionData('./MySessions.xlsx');
sessionList = {'test003', 'test002', 'test004'};
for i = 1:length(sessionList),
    [sc, time] = setSamiSessionData(c, sessionList{i});
    data = getSimbandSamiSrc(sc, time, 0, 'v1');
    filename = strcat(sessionList{i}, '_data');
    save(filename, 'data');
end

%[sc, time] = setSamiSessionData(c, 'test003');

% get data from SAMI
%data = getSimbandSamiSrc(sc,time, 0, 'v1');
