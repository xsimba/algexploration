%main function

% sessionId      = 'hometrainer1';
sessionId      = 'bioSem_Imec1';
% sessionId      = 'bioSem3-AllIn';

% c = loadSessionData('./MySessions.xlsx');
c = loadSessionData('./BioSemanticDevel-server2_1.xlsx');
% c = loadSessionData('./BioSemanticDevel-server2.xlsx');

data = AddAcfInterbeatTracks(c, {sessionId}, 1);

%
% make plot
%
tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
getHRString = ['acf_HR = 60 ./ data.', tracks{2}, '.ibi_acf'];
eval(getHRString);

figure;
plot(acf_HR, '.');

