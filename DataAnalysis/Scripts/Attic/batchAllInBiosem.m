%
% Load WebGuiCsv and aave to Researcher Console v0 format
%

%
% load session references and select WebGuiCsv
%
c = loadSessionData('./BioSemanticDevel_Sessions.xlsx');

sessionList = {...
    'ssicApollo3.5_data2'};

AddTracks(c,sessionList);
AddInterbeatTimeTracks(c,sessionList);


