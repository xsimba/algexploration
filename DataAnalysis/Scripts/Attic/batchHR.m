%
% Load WebGuiCsv and aave to Researcher Console v0 format
%

%
% load session references and select WebGuiCsv
%
c = loadSessionData('./BioSemanticDevel.xlsx');

sessionList = {...
    'bioSem1'};

AddTracks(c,sessionList);
AddInterbeatTimeTracks(c,sessionList);
AddBiosemTracks(c, sessionList);