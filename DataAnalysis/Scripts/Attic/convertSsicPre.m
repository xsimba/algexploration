%
% Load WebGuiCsv and aave to Researcher Console v0 format
%
RC_setup;

%
% load session references and select WebGuiCsv
%
c = loadSessionData('./BioSemanticDevel_Sessions.xlsx');

sessionList = {'ssicApollo3.5_data1'};

for i = 1:length(sessionList),
    csvFilename = setWebGuiSessionData(c, sessionList{i});
    matFilename = [csvFilename(1:end-3),'mat'];
    % load data into workspace in v0 format
    data = getSimbandCsv(csvFilename, '2v0');
    data.fs = 128;
    %
    save (matFilename, 'data');
end


