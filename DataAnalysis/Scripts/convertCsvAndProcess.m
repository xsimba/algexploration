% define script inputs.  
%
% This is set up to run a single session, '09302014_biking' through the
% processing pipeline.  However, to batch multiple runs it is just a matter
% of adding them to sessionList below.
%  
%
sessionId = '10022014_treadmill';
sessionList = {sessionId};
formatVersion = '2v0';
firmwareVersion = 'v0.21.1';

%
% load session references and select WebGuiCsv
%
c = loadSessionData('./BioSemanticDevel_Sessions.xlsx');

%
% create metrics file and add computed tracks
%
disp('Converting CSV to Matlab format');
InitializeTracksFromCSV(c, sessionList, formatVersion);

%
% optionally, run the following to load the data into the workspace 
% and plot before proceeding.  First line gets the filename of the session,
% second line loads it into the workspace, and third line plots it.
% 
% fMat = setV0MatSessionData(c, sessionId);
% load(fMat);
% naiveDataCheckout(data);
%
disp('running IMEC algorithms');
AddTracks(c, sessionList, firmwareVersion);

disp('running SSIC algos');
AddAcfInterbeatTracks(c, sessionList, 0);
AddBiosemTracks(c, sessionList, 'ibi_acf');
AddFreqInterbeatTracks(c, sessionList);

disp('loading data into memory and plotting first four channels');
fMetrics = setMetricsSessionData(c, sessionId);
load(fMetrics);
fourChanComboDisplay(data);

pause

fourChanComboDisplay2(data);

%
% Alternate displays -- 
%
% fourChanComboDisplay(data);      % shows channels 5-8.
%
% signalQualityHistograms(data);   % shows statistics on the signal quality:
%
% naiveDataCheckout(data);         % shows the available signals among ECG/PPG/BioZ/GSR/Acc  
%
 




