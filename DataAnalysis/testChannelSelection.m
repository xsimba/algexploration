clear all; close all; clc

RC_setup
c = loadSessionData('SessionPPGSelection.xlsx');

sessionList = {'collection-Thu Oct 30 2014 13-18-07 GMT-0700 (PDT).csv',...
    'collection-Fri Oct 31 2014 19-56-36 GMT-0700 (PDT).csv',...
    'collection-Fri Oct 31 2014 18-01-35 GMT-0700 (Pacific Daylight Time).csv',...
    'collection-Fri Oct 31 2014 18-09-30 GMT-0700 (Pacific Daylight Time).csv'}; 

InitializeTracksFromCSV(c,sessionList);

AddTracks(c,sessionList);

ppgChannels = ['abcdefgh'];

for sesIdx = 1:numel(sessionList)
    
    modeCI = [];
    modeFreqCI = [];
    averageCI = [];
    trueCI = [];
    
    load(setMetricsSessionData(c, sessionList{sesIdx}));
    
    for ppgIdx = 1:numel(ppgChannels)
        trueCI(:,ppgIdx) = data.ppg.(ppgChannels(ppgIdx)).CI_raw;
        bSig = buffer(trueCI(:,ppgIdx),10,9);
        [m,f] = mode(bSig,1);
        modeCI(:,ppgIdx) = m;
        modeFreqCI(:,ppgIdx) = f;
        averageCI(:,ppgIdx) = mean(bSig,1);
    end
    
    % weigth the mode with the frequency and push forward the latest
    % channels since those are generally better 
    modeCI = fliplr(modeCI.*modeFreqCI);
    [maxMode,bestChannelByMode] = max(modeCI,[],2);
    % corrects to the rigth channel number
    bestChannelByMode =  9-bestChannelByMode;
     
    [maxAverage,bestChannelByAverage] = max(averageCI,[],2);
    [maxCI,bestTrueChannel] = max(trueCI,[],2);
    
    % emulates the structure as in the Band
    ppgMVselection.signal = bestChannelByAverage-1;
    % no other time reference: should be the same than matlab CI
    ppgMVselection.timestamps = 0:numel(bestChannelByAverage)-1;
    
    ppgFWMselection.signal = bestChannelByMode-1;
    % no other time reference: should be the same than matlab CI
    ppgFWMselection.timestamps = 0:numel(bestChannelByMode)-1;
    
    
    figureName = sessionList{sesIdx};
    plotAllMatlabCI(data)
    saveas(gcf,[figureName(1:end-4),'-CI'],'fig');
    saveas(gcf,[figureName(1:end-4),'-CI'],'png');
    close all ; 
    
    plotChannelSelection(data,ppgMVselection);
    saveas(gcf,[figureName(1:end-4),'-selectionMV'],'fig');
    saveas(gcf,[figureName(1:end-4),'-selectionMV'],'png');
    close all ; 
    
    plotChannelSelection(data,ppgFWMselection);
    saveas(gcf,[figureName(1:end-4),'-selectionFWM'],'fig');
    saveas(gcf,[figureName(1:end-4),'-selectionFWM'],'png');
    close all ; 
    
    figure ;
    subplot(211) ; plot(bestChannelByAverage,'LineWidth',2);xlim([14,size(modeCI,1)]);
    ylabel('MV'); ylim([0.5 8.5]); set(gca,'FontSize',17);
    set(gca,'YTick',1:1:8); set(gca,'YTickLabel',{'A','B','C','D','E','F','G','H'})
    subplot(212) ; plot(bestChannelByMode,'LineWidth',2);xlim([14,size(modeCI,1)]);
    ylabel('FWM'); ylim([0.5 8.5]); set(gca,'FontSize',17);
    set(gca,'YTick',1:1:8); set(gca,'YTickLabel',{'A','B','C','D','E','F','G','H'})
    saveas(gcf,[figureName(1:end-4),'-activations'],'fig');
    saveas(gcf,[figureName(1:end-4),'-activations'],'png');
    close all ; 
    
end
