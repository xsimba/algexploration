function [aligned_d, nonaligned_d] = PythonToMatlab(srcdata,data, deviceSchema)
if ~exist('deviceSchema', 'var'),
    deviceSchema = @simbaSchemaSamiV3;
end
schema = deviceSchema();

for num = 1:2,
    if num == 1,
        dataIn = srcdata;
        outData.timestamps = srcdata.timestamps;
    else
        dataIn = data;
    end
        namesIn = fieldnames(dataIn);
        for i = 1:length(namesIn),
            nameIdx = strcmp(namesIn{i}, schema(:,1));
            assert(sum(nameIdx)<=1, 'Schema corrupted.');
            if (sum(nameIdx) == 1),
                fieldIdx = find(nameIdx);
                fieldMatlab{i} = schema{fieldIdx, 2};
                [rows,columns] = eval(['size(dataIn.',namesIn{i},');']);
                if rows == 3,
                    eval(['outData.',fieldMatlab{i},'.timestamps = dataIn.',namesIn{i},'(1,:);']);
                    eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(2,:);']);
                    eval(['outData.',fieldMatlab{i},'.samiTimestamps = dataIn.',namesIn{i},'(3,:);']);
                    eval(['outData.',fieldMatlab{i},'.unixTimeStamps = outData.',fieldMatlab{i},'.timestamps;']);
                    %eval(['outData.',fieldMatlab{i},'.startUnixTime = outData.',fieldMatlab{i},'.timestamps(1);']);
                    if (num == 1),
                        eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.timestamps(1))/1000;']);
                    else
                        eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.',fieldMatlab{i},'.timestamps(1))/1000;']);
                    end
                end
                if rows == 2,
                    eval(['outData.',fieldMatlab{i},'.timestamps = dataIn.',namesIn{i},'(1,:);']);
                    eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(2,:);']);
                    eval(['outData.',fieldMatlab{i},'.unixTimeStamps = outData.',fieldMatlab{i},'.timestamps;']);
                    if (num == 1),
                        eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.timestamps(1))/1000;']);
                    else
                        eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.',fieldMatlab{i},'.timestamps(1))/1000;']);
                    end
                end
                if rows == 1,
                    eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(1,:);']);
                end
        %         
        %                 
                %eval(['outData.',fieldMatlab{i},' = dataIn.',namesIn{i}, ';']);
            %elseif (~strcmp(namesIn{1},'timestamps')),
                %disp(['SAMI field ', namesIn{i},' not found in schema.']);
                %
                % TODO: add a input flag to handle strict vs. loose typing (for
                % debug)
                %
                % outData.(namesIn{i}) = dataIn.(namesIn{i});
            end
        end
        if num == 1,
            aligned_d = outData;
        else
            nonaligned_d = outData;
        end
end