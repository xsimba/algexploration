clear all; 

IFPLOT = 0 ;

% SSB 
ssb.age = 42;
ssb.weight = 80;
ssb.height = 170;
ssbList = {ssb,ssb};

% BP Measures
bpMeasureSitting.sbp = [125 117 119 111];
bpMeasureSitting.dbp = [64  73  72  66];
bpMeasureSitting.timestamps = [23.000 83.000 263.000 383.000]; 

bpMeasureStanding.sbp = [111 120 121 119];
bpMeasureStanding.dbp = [66  70  71  68];
bpMeasureStanding.timestamps = [383.000 443.000 563.000 623.000];

bpMeasuresList = {bpMeasureSitting,bpMeasureStanding};

% Context
contextList = {'sitting','standing'};


RC_setup
c = loadSessionData('MySessions.xlsx');
sessionList = {'10232014_sitting','10232014_standing'}; % name of the session to retrieve
InitializeTracksFromCSV(c,sessionList);

%
% adding SSB
%
AddSSBTracks(c,sessionList,ssbList);

%
% adding Reference BP
%
AddBPMeasureTracks(c,sessionList,bpMeasuresList);

%
% adding Context
%
AddContextTracks(c,sessionList,contextList);

% computes AllIn
AddTracks(c,sessionList);

% freq based HR
AddFreqInterbeatTracks(c,sessionList);
% corr based HR
AddAcfInterbeatTracks(c,sessionList);
% biosem
AddBiosemTracks(c,sessionList,'ibi');

% 

%
% process sitting session
%

fMetricSitting = setMetricsSessionData(c, sessionList{1});
load(fMetricSitting);



% adding context 
data.context.posture = {'sitting'};

if IFPLOT
    naiveDataDisplay(data);
    fourChanComboDisplay(data);
    heartRateQualityPlot(data);
    plotPatBp(data);
end

keyboard();

%
% process sitting session
%
fMetricStanding = setMetricsSessionData(c, sessionList{2});
load(fMetricStanding);


% adding context 
data.context.posture = {'standing'};

if IFPLOT
    naiveDataDisplay(data);
    fourChanComboDisplay(data);
    heartRateQualityPlot(data);
    plotBp(data);
end