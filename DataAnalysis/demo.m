RC_setup
c = loadSessionData('MySessions.xlsx');
sessionList = {'10232014_sitstand'}; % name of the session to retrieve
InitializeTracksFromCSV(c,sessionList);

data.ssb.age = 30;
data.ssb.height = 178;
data.ssb.height = 80;

AddTracks(c,sessionList);
fMetric = setMetricsSessionData(c, sessionList{1});
load(fMetric);
naiveDataDisplay(data);
fourChanComboDisplay(data);


% freq based HR
AddFreqInterbeatTracks(c,sessionList);
% corr based HR
AddAcfInterbeatTracks(c,sessionList);
% biosem
AddBiosemTracks(c,sessionList,'ibi');
heartRateQualityPlot(data);