%
% RC console setup 
%

%
% On installation, edit this script to point to the relevant paths on local 
% machine
%

global RC_CONSOLE_DATABASE RC_DATA_ACCESS_LAYER RC_MATLAB_DIR 
global IMEC_ALGO_DIR SSIC_ALGO_DIR EXTERNAL_DATABASE

%
% see README files in this directory for definitions of these
%
RC_CONSOLE_DATABASE = './DataBases';
RC_DATA_ACCESS_LAYER = '../ResearcherConsole/DataAccessLayer';
RC_MATLAB_DIR = '../ResearcherConsole/Matlab';
SSIC_ALGO_DIR = '..';
IMEC_ALGO_DIR = '../imecAlgorithms';
%change to set external database
EXTERNAL_DATABASE = '../../simband_data/PAT_based_BP_partial_IEEE_data_for_BP_model/'; %e.g. '../../simband_data'

addpath('./Scripts');
addpath(RC_MATLAB_DIR);
setRCpath(RC_MATLAB_DIR);
