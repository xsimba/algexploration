# -*- coding: utf-8 -*-
"""
Created on Thu May 22 15:59:32 2014

@author: asif.khalak
"""
import requests
import json
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from websocket import create_connection

#
#  Pseudo-code
#
#  1) Set up connection parameters: urls, headers, etc.


#
# vc_asif1 credentials
#
userIDListen1 = '51392'
tokenListen1 = '1ec8949da0c04b6fb35e65de8e85b49f'
sourceIDListen1 = 'ca36306ba3244a42917d04da477aa11d'

#
# luiz credentials
#
userIDListen3 = '51392'
tokenListen3 = '1ec8949da0c04b6fb35e65de8e85b49f'
sourceIDListen3 = 'ca36306ba3244a42917d04da477aa11d'


#
# simbaff credentials
#
userIDListen2 = '42740ae2dddb46a8a6e0c4aeadc0ed95'
sourceIDListen2_duke = '64153234c88246b5be0a8f265c6ff3a5'
tokenListen2_duke = '614b3f9e29474853b62b247b0c7a25df'

sourceIDListen2_band1 = '116d889638104e86a4462c5892073a30'
tokenListen2_band1 = 'a9d7312cc8174624b05333ff75ca60e7'

sourceIDListen2_band2 = 'dbb22352180f469fb2e478c5344c033b'
tokenListen2_band2 = '7748f77ee0fc49e0880848990dd09af9'

sourceIDListen2_band3 = 'a5a4ee484f3249f19bcfb852bb74cbd9'
tokenListen2_band3 = '599dcb34548c4120b2e9b86fcf5b6ade'

sourceIDListen2_band4 = 'aca5b8eda6ce4075abd67d0568e32714'
tokenListen2_band4 = '6037dd3cb8ec4e27b05e63953d2750c0'


ananavBand = 'c1717a85481b4f2aa0a1b231a1905941'
ananavToken = '1def59e56d1d4a06acf85e3cd926d2cb'


#sIDListen = userIDListen2
tokenListen  = ananavToken
sourceIDListen = ananavBand
livestreamUrl = 'wss://api.samihub.com/v1.1/live' + '?sourceId=' + sourceIDListen + \
             '&Authorization=bearer+' + tokenListen

userIDPost = '51392'
tokenPost = '1ec8949da0c04b6fb35e65de8e85b49f'
sourceIDPost = 'ca36306ba3244a42917d04da477aa11d'
posturl = 'https://api.samihub.com/v1.1/message'

postHeader = {'Authorization': 'bearer '+tokenPost, \
'Content-type': 'application/json'
}

#  3) Open websocket
ws = create_connection(livestreamUrl)

#
#  4) While loop (infinite)
while 1:
#     A) pull a message from the websocket
    result = ws.recv()
#    time.sleep(0.02) # wait 20ms per poll
    print(result)

