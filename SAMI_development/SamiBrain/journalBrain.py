# -*- coding: utf-8 -*-
"""
journalCheckout.py

Created on Wed May 21 09:24:28 2014

@author: asif.khalak
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import simbaJournal as simbaJ

def gettimes(vector, timerange):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0], vector[:,0] < timerange[1]))
    return ind

# journal outputs timestamps in microseconds
Fs = 128.0
tickClock = 1.0e6

# raw block sizes are mismatched from sample rate...
#dataDir = r'/home/mkedwards/yosi'
dataDir = r'c:\code\simbase\canned\dryrun'
#dataDir = r'C:\code\simbase\canned\can5'
# dataDir = r'/home/mkedwards/journal-chair'
#dataDir = r'/home/mkedwards/simbase/canned/can6'

# journal outputs in microseconds
sampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}
journal = simbaJ.getJournalAll(dataDir, freq=sampleFreq)

# unpack the journal
ppg1 = journal['ppg1']
ppg2 = journal['ppg2']
ecg = journal['ecg']
ecgBeats = journal['ecgBeats']
ppgBeats = journal['ppgBeats']
pat = journal['pat']


#%% iterate over blocks

#
# Get Data   
#


#
# get the pre sample rate converted data
#
rawecg = parseJournalData(rawecgFile,  sampFreq=512.0)
rawppg = parseJournalData(rawppgFile, sampFreq=50.0)
  
#
# get the SRC ecg file
#
ecg = parseJournalData(ecgFile, Fs, 1.0/tickClock)

#
# get the SRC ppg file (Chan1 out of 2)
#
ppg = parseJournalData(ppgFile, Fs, 1.0/tickClock)

#
# get the detected beats
#

ecgBeats = parseJournalData(ecgBeatFile, Fs, 1.0/tickClock)
ppgBeats = parseJournalData(ppgBeatFile, Fs, 1.0/tickClock)

#
# get the PAT
#
pat = parseJournalData(patFile, Fs, 1.0/tickClock)

#
# lead on file not parsable in the journal right now
# 


#%% Process data
times = (880, 920)
indP = gettimes(ppg, times)
indPB = gettimes(ppgBeats, times)
ppgBeatTime = ppgBeats[indPB,0]
ppgBeatAmp  = np.interp(ppgBeatTime, ppg[indP,0], ppg[indP,2])

#%% Plot data
plt.figure(6)
plt.clf()
srvplot = plt.plot(ppg[indP,0], ppg[indP,2]-np.median(ppg[indP,2]), 'g')
plt.setp(srvplot, 'linewidth', 2.0)
plt.hold(True)
ppgBeatPlot = plt.plot(ppgBeatTime, ppgBeatAmp-np.median(ppg[indP,2]), 'x')
plt.setp(ppgBeatPlot, 'markeredgewidth', 2.0)
plt.ylabel('PPG-1')
plt.xlabel('Acquisition time [s]')
plt.title('Data from Can5')

