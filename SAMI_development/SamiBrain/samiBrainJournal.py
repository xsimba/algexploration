# -*- coding: utf-8 -*-
"""
Created on Thu May 22 15:59:32 2014

@author: asif.khalak
"""
import requests
import json
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import simbaJournal as simbaJ


#
# initialize with an impossible timestamp to set initialzation guard
#
startTimestamp = -1.0


def update_line(hl, new_xdata, new_ydata):
    hl.set_xdata(np.append(hl.get_xdata(), new_xdata))
    hl.set_ydata(np.append(hl.get_ydata(), new_ydata))
    plt.draw()


#
# pull data from journal
#
# journal outputs in microseconds
Fs = 128.0
FreqClock = 1.0e6
minTime = 1e10
sampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}

do_raw = False

#
# avoid reload if running interactively only
#
# raw block sizes are mismatched from sample rate...
#dataDir = r'/home/mkedwards/yosi'
dataDir = r'c:\code\simbase\canned\can11_stefan_may23'
# dataDir = r'/home/mkedwards/journal-chair'
#dataDir = r'/home/mkedwards/simbase/canned/can6'

try:
    journal.keys()
    assert (True)
except:
    journal = simbaJ.getJournalAll(dataDir, freq=sampleFreq)


# unpack the journal
ppgJournal = journal['ppg1']
ecgJournal = journal['ecg']
ecgBeatsJournal = journal['ecgBeats']
ppgBeatsJournal = journal['ppgBeats']

#
#  Pseudo-code
#
#  1) Set up connection parameters: urls, headers, etc.
devel = True

if not devel:
    userIDPost = '51392'
    tokenPost = '1ec8949da0c04b6fb35e65de8e85b49f'
    sourceIDPost = 'ca36306ba3244a42917d04da477aa11d'
    posturl = 'https://api.samihub.com/v1.1/message'

# SAMI development server
#
if devel:
    userIDPost = '676e14a608bc4683bd154dd234e5fcb9'
    tokenPost =  '9ac60af716b84682831baf5b9f4d390f'
    sourceIDPost = '656cea9368d24224b1416c484a3a317c'
    posturl = 'https://api-dev.samihub.com/v1.1/message'


postHeader = {'Authorization': 'bearer '+tokenPost, \
'Content-type': 'application/json'
}

#
# set timestamp
#
currtime= int(time.mktime(time.localtime())*1000.0)
maxBeats = 500

#  2) Initialize 
#     A) internal data: PPG beats, ECG beats
#     B) output data  : ecgInterbeatTime, ppgInterbeatTime
#     C) screen plot.

ppgBeats = np.empty([0,2])
ecgBeats = np.empty([0,2])
ppgInterBeatTime = np.empty([1])
ecgInterBeatTime = np.empty([1])
firstTime = -1;

plt.figure(1)
plt.clf()
plt.hold(True)
plt.xlabel('Acquisition Time [s]')
plt.ylabel('Interbeat Time [s]')

#ehl, = plt.plot([], [], 'rx')
#phl, = plt.plot([], [], 'bx')

plt.axis([0, 100, 0 ,1.7])
plt.ioff()


dataList1 = []
dataList2 = []

i = 0
j = 0
flop = 0
#
#  4) While loop (infinite)
while 1 and i+j < maxBeats:
#     A) pull a message from the websocket / journal
    if flop == 0:
        flop = 1
        dataDict = {'sensorType' : 'ECGHeartBeat'}
        dictResult = { 'data': {'content' : {'ECGHeartBeat' : [ecgBeatsJournal[i,1]]}}, 'ts' : ecgBeatsJournal[i,0]}
        i = i + 1
    elif flop == 1:
        flop = 0
        dataDict = {'sensorType' : 'HeartBeat'}
        dictResult = { 'data': {'content' : {'HeartBeat' : [ppgBeatsJournal[j,1]]}}, 'ts' : ppgBeatsJournal[j,0]}
        j = j + 1

    time.sleep(0.5)
        
    dataList1.append(dataDict)
    dataList2.append(dictResult)
        
#     B) Filter on simba-devices, channel type.

    if dataDict['sensorType'] == 'ECGHeartBeat':
# On success:
#        1. If initalization/warmup period, then build up data queue
#        2. Otherwise, 
#            a. add data to queue 
#            b. compute relevant quantity
#            c. plot it to the screen
#            d. post it as a result to the SimbaAnalytics device

#        timestamp = dictResult['ts']*1000.0 + currtime
        currtime= int(time.mktime(time.localtime())*1000.0)
        timestamp = currtime
        if firstTime < 0:
            firstTime = timestamp / 1000.0
        thistime = timestamp/1000.0 # - firstTime
        ampl     = dictResult['data']['content']['ECGHeartBeat']
# hack for streaming 'real-time'
#        ecgBeats = np.append(ecgBeats, np.array([(thistime, ampl[0])]), axis=0) 
        ecgBeats = np.append(ecgBeats, np.array([(dictResult['ts'], ampl[0])]), axis=0) 

        if (np.size(ecgBeats,0) > 2):
            
            thisInterbeat = ecgBeats[-1,0] - ecgBeats[-2,0]
            #update_line(ehl, ecgBeats[-2,0],np.array([thisInterbeat]))            
            ecgInterBeatTime = np.append(thisInterbeat, np.array([thisInterbeat]))
            
            #
            # form POST payload
            #
            payloadDict = {"ecgInterpulseTime": thisInterbeat}
            postDictE = {"sdid" : sourceIDPost, \
            "ts" : timestamp, \
             "data" : payloadDict
            }
            postPayString = json.dumps(postDictE)
            if (thisInterbeat > 0.4 and thisInterbeat < 1.6):
                a = requests.post(posturl, data=postPayString, headers=postHeader)

        
    if dataDict['sensorType'] == 'HeartBeat':
# On success:
#        1. If initalization/warmup period, then build up data queue
#        2. Otherwise, 
#            a. add data to queue 
#            b. compute relevant quantity
#            c. plot it to the screen
#            d. post it as a result to the SimbaAnalytics device

#        timestamp = dictResult['ts']*1000.0 + currtime
        currtime= int(time.mktime(time.localtime())*1000.0)
        timestamp = currtime
        if firstTime < 0:
            firstTime = timestamp / 1000.0

        thistime = timestamp/1000.0 # - firstTime
        ampl     = dictResult['data']['content']['HeartBeat']
        ppgBeats = np.append(ppgBeats, np.array([(dictResult['ts'], ampl[0])]), axis=0) 
#        ppgBeats = np.append(ppgBeats, np.array([(thistime, ampl[0])]), axis=0) 

        if (np.size(ppgBeats,0) > 2):
            
            thisInterbeat = ppgBeats[-1,0] - ppgBeats[-2,0]
            ppgInterBeatTime = np.append(ppgInterBeatTime, np.array([thisInterbeat]))
            #update_line(phl, ppgBeats[-2,0],np.array([thisInterbeat]))
            
            #
            # form POST payload
            #
            payloadDict = {"ppgInterpulseTime": thisInterbeat}
            postDictP = {"sdid" : sourceIDPost, \
            "ts" : timestamp, \
            "data" : payloadDict
            }
            postPayString = json.dumps(postDictP)
            if (thisInterbeat > 0.4 and thisInterbeat < 1.6):
                a = requests.post(posturl, data=postPayString, headers=postHeader)

#            e. (optional) partially flush queue if needed.
#