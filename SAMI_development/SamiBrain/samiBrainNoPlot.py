# -*- coding: utf-8 -*-
"""
Created on Thu May 22 15:59:32 2014

@author: asif.khalak
"""
import requests
import json
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from websocket import create_connection

#
#  Pseudo-code
#
#  1) Set up connection parameters: urls, headers, etc.


#
# vc_asif1 credentials
#
userIDListen1 = '51392'
tokenListen1 = '1ec8949da0c04b6fb35e65de8e85b49f'
sourceIDListen1 = 'ca36306ba3244a42917d04da477aa11d'

#
# luiz credentials
#
userIDListen3 = '51392'
tokenListen3 = '1ec8949da0c04b6fb35e65de8e85b49f'
sourceIDListen3 = 'ca36306ba3244a42917d04da477aa11d'


#
# simbaff credentials
#
userIDListen2 = '42740ae2dddb46a8a6e0c4aeadc0ed95'
sourceIDListen2_duke = '64153234c88246b5be0a8f265c6ff3a5'
tokenListen2_duke = '614b3f9e29474853b62b247b0c7a25df'

sourceIDListen2_band1 = '116d889638104e86a4462c5892073a30'
tokenListen2_band1 = 'a9d7312cc8174624b05333ff75ca60e7'

sourceIDListen2_band2 = 'dbb22352180f469fb2e478c5344c033b'
tokenListen2_band2 = '7748f77ee0fc49e0880848990dd09af9'

sourceIDListen2_band3 = 'a5a4ee484f3249f19bcfb852bb74cbd9'
tokenListen2_band3 = '599dcb34548c4120b2e9b86fcf5b6ade'

sourceIDListen2_band4 = 'aca5b8eda6ce4075abd67d0568e32714'
tokenListen2_band4 = '6037dd3cb8ec4e27b05e63953d2750c0'


userIDListen = userIDListen2
tokenListen  = tokenListen2_band3
sourceIDListen = sourceIDListen2_band3
#tokenListen  = tokenListen2_duke
#sourceIDListen = sourceIDListen2_duke


livestreamUrl = 'wss://api.samihub.com/v1.1/live' + '?userId=' + userIDListen + \
             '&Authorization=bearer+' + tokenListen


devel = True

if not devel:
    userIDPost = '51392'
    tokenPost = '1ec8949da0c04b6fb35e65de8e85b49f'
    sourceIDPost = 'ca36306ba3244a42917d04da477aa11d'
    posturl = 'https://api.samihub.com/v1.1/message'

# SAMI development server
#
if devel:
    userIDPost = '676e14a608bc4683bd154dd234e5fcb9'
    tokenPost =  '9ac60af716b84682831baf5b9f4d390f'
    sourceIDPost = '656cea9368d24224b1416c484a3a317c'
    posturl = 'https://api-dev.samihub.com/v1.1/message'

postHeader = {'Authorization': 'bearer '+tokenPost, \
'Content-type': 'application/json'
}


#
# set timestamp
#
#time = int(time.mktime(time.localtime())*1000.0)



#  2) Initialize 
#     A) internal data: PPG beats, ECG beats
#     B) output data  : ecgInterbeatTime, ppgInterbeatTime
#     C) screen plot.

ppgBeats = np.empty([0,2])
ecgBeats = np.empty([0,2])
ppgInterBeatTime = np.empty([1])
ecgInterBeatTime = np.empty([1])
firstTime = -1;


def update_line(hl, new_xdata, new_ydata):
    hl.set_xdata(np.append(hl.get_xdata(), new_xdata))
    hl.set_ydata(np.append(hl.get_ydata(), new_ydata))
    plt.draw()

plt.figure(1)
plt.clf()
plt.hold(True)
plt.xlabel('Acquisition Time [s]')
plt.ylabel('Interbeat Time [s]')

#ehl, = plt.plot([], [], 'rx')
#phl, = plt.plot([], [], 'bx')

#plt.axis([0, 100, 0 ,1.7])
plt.draw()


#  3) Open websocket
ws = create_connection(livestreamUrl)

#
#  4) While loop (infinite)
while 1:
#     A) pull a message from the websocket
    result = ws.recv()
#    time.sleep(0.02) # wait 20ms per poll

#     B) Filter on simba-devices, channel type. 
    dictResult = json.loads(result)
    if not dictResult.has_key('sdtid'):
        continue
    if not (dictResult['sdtid'] == 'simba_device'):
        continue
    
    dataDict = dictResult['data']['metadata']
    
    
    if dataDict['sensorType'] == 'ECGHeartBeat':
# On success:
#        1. If initalization/warmup period, then build up data queue
#        2. Otherwise, 
#            a. add data to queue 
#            b. compute relevant quantity
#            c. plot it to the screen
#            d. post it as a result to the SimbaAnalytics device

        timestamp = dictResult['ts']
        if firstTime < 0:
            firstTime = timestamp / 1000.0
        thistime = timestamp/1000.0 - firstTime
        ampl     = dictResult['data']['content']['ECGHeartBeat']
        ecgBeats = np.append(ecgBeats, np.array([(thistime, ampl[0])]), axis=0) 

        if (np.size(ecgBeats,0) > 2):
            
            thisInterbeat = ecgBeats[-1,0] - ecgBeats[-2,0]
#            update_line(ehl, ecgBeats[-2,0],np.array([thisInterbeat]))            
#            ecgInterBeatTime = np.append(ecgInterBeatTime, )
            
            #
            # form POST payload
            #
            payloadDict = {"ecgInterpulseTime": thisInterbeat}
            postDictE = {"sdid" : sourceIDPost, \
            "ts" : timestamp, \
            "data" : payloadDict
            }
            postPayString = json.dumps(postDictE)
            a = requests.post(posturl, data=postPayString, headers=postHeader)

        
    if dataDict['sensorType'] == 'HeartBeat':
# On success:
#        1. If initalization/warmup period, then build up data queue
#        2. Otherwise, 
#            a. add data to queue 
#            b. compute relevant quantity
#            c. plot it to the screen
#            d. post it as a result to the SimbaAnalytics device

        timestamp = dictResult['ts']
        if firstTime < 0:
            firstTime = timestamp / 1000.0

        thistime = timestamp/1000.0 - firstTime
        ampl     = dictResult['data']['content']['HeartBeat']
        ppgBeats = np.append(ppgBeats, np.array([(thistime, ampl[0])]), axis=0) 

        if (np.size(ppgBeats,0) > 2):
            
            thisInterbeat = ppgBeats[-1,0] - ppgBeats[-2,0]
#            ppgInterBeatTime = np.append(ppgInterBeatTime, np.array([thisInterbeat]))
#            update_line(phl, ppgBeats[-2,0],np.array([thisInterbeat]))            
            
            #
            # form POST payload
            #
            payloadDict = {"ecgInterpulseTime": thisInterbeat}
            postDictP = {"sdid" : sourceIDPost, \
            "ts" : timestamp,  \
            "data" : payloadDict
            }
            postPayString = json.dumps(postDictP)
            a = requests.post(posturl, data=postPayString, headers=postHeader)



#            e. (optional) partially flush queue if needed.
#

