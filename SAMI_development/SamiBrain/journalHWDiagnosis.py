# -*- coding: utf-8 -*-
"""
journalCheckout.py

Created on Wed May 21 09:24:28 2014

@author: asif.khalak
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

# journal outputs in microseconds
Fs = 128.0
FreqClock = 1.0e6

def parseJournalDataRaw(filename):
    # load journal data into memory
    get_quoted_value = lambda x: float(x.strip('"'))
    data = np.genfromtxt(filename, delimiter =',', skiprows=1, converters = {0:get_quoted_value})        
    return data

def parseJournalData(filename, sampFreq, timeUnits=1e-6):
    # load journal data into memory
    get_quoted_value = lambda x: float(x.strip('"'))
    data = np.genfromtxt(filename, delimiter =',', skiprows=1, converters = {0:get_quoted_value}, filling_values = {0: -1.0})
        
    # scale timestamps to seconds from start of experiment
    startTimestamp = data[0,0]
    for idx, val in enumerate(data):
        if (val[0] < 0):
            data[idx,0] = data[idx-1,0] + 1/sampFreq
        else:
            data[idx,0] = (data[idx,0] - startTimestamp) * timeUnits
            
    return data

def parseJournalBlock(filename, blockNo=0, blockSize=128, sampFreq=128, timeUnits=1e-6):
    # load journal data into memory
    get_quoted_value = lambda x: float(x.strip('"'))
    data = np.genfromtxt(filename, delimiter =',', skiprows=1, converters = {0:get_quoted_value}, filling_values = {0: -1.0})
        
    # scale timestamps to seconds from start of experiment
    startTimestamp = data[0,0]
    for idx, val in enumerate(data):
        if (val[0] < 0):
            data[idx,0] = data[idx-1,0] + 1/sampFreq
        else:
            data[idx,0] = (data[idx,0] - startTimestamp) * timeUnits
            
    return data


def gettimes(vector, timerange):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0], vector[:,0] < timerange[1]))
    return ind

#%%    

#
# get the pre sample rate converted data
#
# raw block sizes are mismatched from sample rate...
dataDir = r'C:\code\simbase\canned\can5'
rawecgFile = dataDir +r'\ecgraw.csv'
rawecg = parseJournalData(rawecgFile,  sampFreq=512.0)

rawppgFile = dataDir +r'\ppgpre-1.csv'
rawppg = parseJournalData(rawppgFile, sampFreq=50.0)

#
# compare raw clocks
#
plt.figure(1)
plt.clf()
plt.subplot(211)
plt.plot(np.r_[0:rawecg[:,0].size]/512.0, rawecg[:,0], 'r.')
plt.hold(True)
plt.plot(np.r_[0:rawppg[:,0].size]/50.0, rawppg[:,0], 'g.')
plt.ylabel('raw time from start [s]');
plt.xlabel('nominal time from start [s]');
plt.legend(('ECG', 'PPG'), loc=2)
plt.title('Distinction in raw timestamps, Can5')
plt.subplot(212)
  
#%%

#
# get the SRC ecg file
#
ecgFile = dataDir +r'\ecg.csv'
ecg = parseJournalData(ecgFile, Fs, 1.0/FreqClock)

#
# get the SRC ppg file (Chan1 out of 2)
#
ppgFile = dataDir + r'\ppg-1.csv'
ppg = parseJournalData(ppgFile, Fs, 1.0/FreqClock)

#
# get the detected beats
#
ecgBeatFile = dataDir + r'\ecgheartbeat.csv'
ppgBeatFile = dataDir + r'\heartbeat.csv'

ecgBeats = parseJournalData(ecgBeatFile, Fs, 1.0/FreqClock)
ppgBeats = parseJournalData(ppgBeatFile, Fs, 1.0/FreqClock)

#
# get the PAT
#
patFile = dataDir + r'\pat.csv'
pat = parseJournalData(patFile, Fs, 1.0/FreqClock)

#%%

#
# plot raw and SRC data on the same axes to compare
#
times = (880, 920)
plt.figure(5)
plt.clf()
indE = gettimes(ecg, times)
indERaw = gettimes(rawecg, times)
indEB = gettimes(ecgBeats, times)
plt.plot(rawecg[indERaw,0]+1.595, rawecg[indERaw,1], 'b')
plt.hold(True)
srvplot = plt.plot(ecg[indE,0], ecg[indE,1], 'r')
plt.setp(srvplot, 'linewidth', 2.0)
ecgBeatTime = ecgBeats[indEB,0]
ecgBeatAmp  = np.interp(ecgBeatTime, ecg[indE,0], ecg[indE,1])
ecgBeatPlot = plt.plot(ecgBeatTime, ecgBeatAmp, 'kx')
plt.setp(ecgBeatPlot, 'markeredgewidth', 2.0)
plt.ylabel('ECG')
plt.xlabel('Acquisition time [s]')
plt.title('Data from Can5, Red=SRC; Blue=Raw, Raw offset 1.595s')

plt.figure(6)
plt.clf()
indP = gettimes(ppg, times)
indPRaw = gettimes(rawppg, times)
indPB = gettimes(ppgBeats, times)
plt.plot(rawppg[indPRaw,0]-3.055, rawppg[indPRaw,2]-np.median(rawppg[indPRaw,2]), 'b')
plt.hold(True)
srvplot = plt.plot(ppg[indP,0], ppg[indP,2]-np.median(ppg[indP,2]), 'g')
plt.setp(srvplot, 'linewidth', 2.0)
indPB = gettimes(ppgBeats, times)
ppgBeatTime = ppgBeats[indPB,0]
ppgBeatAmp  = np.interp(ppgBeatTime, ppg[indP,0], ppg[indP,2])
ppgBeatPlot = plt.plot(ppgBeatTime, ppgBeatAmp-np.median(ppg[indP,2]), 'x')
plt.setp(ppgBeatPlot, 'markeredgewidth', 2.0)
plt.ylabel('PPG-1')
plt.xlabel('Acquisition time [s]')
plt.title('Data from Can5, Green=SRC; Blue=Raw, Raw offset -3.055s')

#%%

#
# compare the clocks
#
plt.figure(2)
plt.clf()
plt.plot(ecg[:,0], 'r.')
plt.hold(True)
plt.plot(ppg[:,0], 'g.')
plt.xlabel('index');
plt.ylabel('time [s]');
plt.legend(('ECG', 'PPG'), loc=2)
plt.title('Distinction in timestamps, Can5')

plt.figure(3)
plt.clf()
plt.plot(ecgBeats[:,0],np.ones(ecgBeats[:,0].size), 'rx')
plt.hold(True)
plt.plot(ppgBeats[:,0],np.ones(ppgBeats[:,0].size),  'gx')
plt.plot(pat[:,0],pat[:,1],  'bx')
plt.xlabel('index');
plt.ylabel('time [s]');
plt.legend(('ECG', 'PPG', 'PAT'), loc=2)
plt.title('Distinction in timestamps, Can5')
    
#%%

times = (880, 920)
plt.figure(4)
plt.clf()
plt.subplot(311)
indE = gettimes(ecg, times)
plt.plot(ecg[indE,0], ecg[indE,1], 'r')
plt.hold(True)
indEB = gettimes(ecgBeats, times)
ecgBeatTime = ecgBeats[indEB,0]
ecgBeatAmp  = np.interp(ecgBeatTime, ecg[indE,0], ecg[indE,1])
ecgBeatPlot = plt.plot(ecgBeatTime, ecgBeatAmp, 'kx')
plt.setp(ecgBeatPlot, 'markeredgewidth', 2.0)
plt.ylabel('ECG')
plt.title('Data from Can5')

plt.subplot(312)
indP = gettimes(ppg, times)
plt.plot(ppg[indP,0], ppg[indP,2], 'r')
plt.hold(True)
indPB = gettimes(ppgBeats, times)
ppgBeatTime = ppgBeats[indPB,0]
ppgBeatAmp  = np.interp(ppgBeatTime, ppg[indP,0], ppg[indP,2])
ppgBeatPlot = plt.plot(ppgBeatTime, ppgBeatAmp, 'x')
plt.setp(ppgBeatPlot, 'markeredgewidth', 2.0)
plt.ylabel('PPG-1, Green')

plt.subplot(313)
ind = gettimes(pat, times)
patPlot = plt.plot(pat[ind,0], pat[ind,1], 'r.')
plt.setp(patPlot, 'markeredgewidth', 2.0)
plt.hold(True)
plt.ylabel('PAT [s]')
plt.xlabel('time')

plt.show()
