# -*- coding: utf-8 -*-
"""
Created on Fri May 23 19:55:19 2014

simbaJournal contains stuff to get data from a simba Journal
.csv dump

@author: asif.khalak
"""

import numpy as np
import os

#
# now just a library of functions, but may want to 
# promote this to a class at some point to make it 
# simpler.
#

# journal outputs in microseconds
Fs = 128.0
FreqClock = 1.0e6

def parseJournalDataRaw(filename):

    # load journal data into memory
    get_quoted_value = lambda x: float(x.strip('"'))
    data = np.genfromtxt(filename, delimiter =',', skiprows=1, converters = {0:get_quoted_value})        
    return data

def parseJournalData(filename, sampFreq, timeUnits=1e-6):
    #
    # check to see if the file has any data worth parsing
    # it must exist, contain a real header, and have
    # at least 1 timestamp worth of data
    if not os.path.isfile(filename):
        return []
        
    fid = open(filename, 'r')
    if (fid.readline()[0:3] != 'id='):
        fid.close()
        return []
    elif (fid.readline() == ''):
        fid.close()
        return []
        
    # pull the timestamp out of the function attribute
    startTimestamp = parseJournalData.startTimestamp

    # load journal data into memory
    get_quoted_value = lambda x: float(x.strip('"'))
    data = np.genfromtxt(filename, delimiter =',', skiprows=1, converters = {0:get_quoted_value}, filling_values = {0: -1.0})
        
    # scale timestamps to seconds from start of experiment
    if startTimestamp < 0.0:
        parseJournalData.startTimestamp = data[0,0]
        startTimestamp = data[0,0]
        print(filename +  "   " + str(startTimestamp))
    else:
        print(filename +  "   " + str(data[0,0]))
        
    for idx, val in enumerate(data):
        if (val[0] < 0):
            data[idx,0] = data[idx-1,0] + 1/sampFreq
        else:
            data[idx,0] = (data[idx,0] - startTimestamp) * timeUnits

    print(filename +  " ->" + str(data[0,0]))
            
    return data

#
# initialize with an impossible timestamp to set initialzation guard
#
parseJournalData.startTimestamp = -1.0


#
# shift all of the journal fields to have positive timestamps
#
def setPositiveTimestamps (journal):

    #
    # first loop over the journal to find the min time
    #
    minTime = 1.0e14
    for key in journal.keys():
        data = journal[key]
        if journal[key] != []:
            minTime = np.min([minTime, data[0,0]])
    
    #
    # second loop over the journal to subract it off
    #
    for key in journal.keys():
        data = journal[key]
        if journal[key] != []:
            data[:,0] = data[:,0] - minTime
            
    return
    

#
# this pulls in journal data if available into a journal dict
#
defaultSampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}
def getJournalAll(dataDir, freq=defaultSampleFreq):

    journal = dict()    
    Fs= freq['default']
    FreqClock = freq['clock']
    
    rawppgFile1 = dataDir + r'/ppgpre-1.csv'
    ppgFiltFile1 = dataDir + r'/ppgfilt-1.csv'
    ppgFile1 = dataDir + r'/ppg-1.csv'
    ppgVisFile1 = dataDir + r'/ppg-visual-1.csv'

    rawppgFile2 = dataDir + r'/ppgpre-2.csv'
    ppgFiltFile2 = dataDir + r'/ppgfilt-2.csv'
    ppgFile2 = dataDir + r'/ppg-2.csv'
    ppgVisFile2 = dataDir + r'/ppg-visual-2.csv'
 
    ppgBeatFile = dataDir + r'/heartbeat.csv'

    rawecgFile = dataDir + r'/ecgraw.csv'
    ecgFile = dataDir + r'/ecg.csv'
    ecgBeatFile = dataDir + r'/ecgheartbeat.csv'
    ecgVisFile = dataDir + r'/ecgvisual.csv'
    patFile = dataDir + r'/pat.csv'

    journal['rawppg1']  = parseJournalData(rawppgFile1, sampFreq=freq['ppgRaw'])
    journal['ppgFilt1'] = parseJournalData(ppgFiltFile1, Fs, 1.0/FreqClock)
    journal['ppg1']     = parseJournalData(ppgFile1, Fs, 1.0/FreqClock)
    journal['ppgVis1']   = parseJournalData(ppgVisFile1, 0.5*Fs, 1.0/FreqClock)

    journal['rawppg2']  = parseJournalData(rawppgFile2, sampFreq=freq['ppgRaw'])
    journal['ppgFilt2'] = parseJournalData(ppgFiltFile2, Fs, 1.0/FreqClock)
    journal['ppg2']     = parseJournalData(ppgFile2, Fs, 1.0/FreqClock)
    journal['ppgVis2']  = parseJournalData(ppgVisFile2, 0.5*Fs, 1.0/FreqClock)

    journal['ppgBeats'] = parseJournalData(ppgBeatFile, Fs, 1.0/FreqClock)

    journal['rawecg']   = parseJournalData(rawecgFile,  sampFreq=freq['ecgRaw']) 
    journal['ecg']      = parseJournalData(ecgFile, Fs, 1.0/FreqClock)
    journal['ecgBeats'] = parseJournalData(ecgBeatFile, Fs, 1.0/FreqClock)
    journal['ecgVis']   = parseJournalData(ecgVisFile, Fs, 1.0/FreqClock)

    journal['pat']      = parseJournalData(patFile, Fs, 1.0/FreqClock)

    print "Shifting series for positive times"
    setPositiveTimestamps(journal)

    return journal

def getJournalEcg(dataDir, freq=defaultSampleFreq):

    journal = dict()    
    Fs= freq['default']
    FreqClock = freq['clock']
    
    rawecgFile = dataDir + r'/ecgraw.csv'
    ecgFile = dataDir + r'/ecg.csv'
    ecgBeatFile = dataDir + r'/ecgheartbeat.csv'

    journal['rawecg']   = parseJournalData(rawecgFile,  sampFreq=freq['ecgRaw']) 
    journal['ecg']      = parseJournalData(ecgFile, Fs, 1.0/FreqClock)
    journal['ecgBeats'] = parseJournalData(ecgBeatFile, Fs, 1.0/FreqClock)

    print "Shifting series for positive times"
    setPositiveTimestamps(journal)

    return journal
