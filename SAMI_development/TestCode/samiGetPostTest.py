# -*- coding: utf-8 -*-
"""
Created on Thu May 22 11:03:47 2014

@author: asif.khalak
"""

import requests
import json
import time

#
# try GET and POST to SAMI
#
userID = '51392'
token = '1ec8949da0c04b6fb35e65de8e85b49f'
sourceID = 'ca36306ba3244a42917d04da477aa11d'


#
# setup details
#
livestreamUrl = 'wss://api.samihub.com/v1.1/live' + '?userId=' + userID + \
             '&Authorization=bearer+' + token

postHeader = {'Authorization': 'bearer '+token, \
'Content-type': 'application/json'
}
posturl = 'https://api.samihub.com/v1.1/message'

# post message
#
#/message

#
# set timestamp
#
time = int(time.mktime(time.localtime())*1000.0)

#
# form payload
#
iept = []
ippt = [0.891]
if (len(iept) == 0):
    payloadDict = {"ppgInterpulseTime": ippt}
elif (len(ippt) == 0):
    payloadDict = {"ecgInterpulseTime": iept}
else:
    payloadDict = {"ecgInterpulseTime": iept, "ppgInterpulseTime": ippt}

#
# form JSON dict
#
postDict1 = {"sdid" : sourceID, \
"ts" : time, \
"data" : payloadDict
}
jpostString1 = postDict1

#
# post test
#
postPayString = json.dumps(jpostString1)
a = requests.post(posturl, data=postPayString, headers=postHeader)


