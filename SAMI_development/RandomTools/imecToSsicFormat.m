function data = imecToSsicFormat(outputDataFilename)
%

    baseoutputName = outputDataFilename(1:end-4);
    inputDataFilename = [baseoutputName,'_SRCout.mat'];

    % load data into workspace in v0 format
    load(inputDataFilename);
    
    % verify that all necessary channels are present
    %
    schema = getSRCTrackConv('Athena3.5SRC');
    tracksIn = schema(:,1);
    tracksOut = schema(:,2);    

    try
        for j = 1:length(tracksIn),
            curfieldIn = tracksIn{j};
            curfieldOut = tracksOut{j};
            evalString=['data.',curfieldOut,' = output.',curfieldIn,';'];
            eval(evalString);
        end
    catch
        disp(['Input file has no field: ',curfieldIn]);
    end
    
    data.timestamps = (1:length(output.ecg.signal))/data.fs;
    
%
% 