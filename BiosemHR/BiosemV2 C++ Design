BiosemV2 C++ Design
====================

Coming out of HeartRateFreqAnalysis.h
    struct Result
    {
        float ibi_freq[20];
        float amp_freq[20];
        float CI_freq[20];
        float timestep;
        double time;
        size_t num_pks;
        bool action_flag;
    };


Defines
-------

#define HEARTRATE_CHANGELIMIT_NEGATIVE_FREQ   -0.15f
#define HEARTRATE_CHANGELIMIT_POSITIVE_FREQ   0.15f

#define FFT_AMP_POWERCUT 0.10f
#define FREQ_MIN_SIGMA 4.0f
#define FREQ_WINDOWSIZE 5
#define FREQ_CVCUTOFF 2.5f
#define SIGMA_GROWTH_PARAM_FREQ 	0.4f;


Enums
-----
typedef enum {
	INIT,
	NORMAL,
	RESET,
	POSTRESET
} StochModelType_E;

Typedefs
--------

FreqAlgoParam_t  equiv of AlgoParam

FreqCandidatesList_t,
	float array of frequencies
	float array of amplitude
	int array of knockoutFlag
	float strongestPeak


FreqIbiQueue_t, equivalent to fbbq:recentHrFilt
	float array of ibis
	uint32_t array of times
	int enqueueIdx - current value
	int stochModelType
	float muHR
	float sigmaHR

FreqBBQParams_t, container for bioLimits + algoParams
	bioLimits_t 
	FreqAlgoParam_t 



Public Methods
--------------

bool initBiosemFreqHR 
 -- Takes in a FreqBBQParams_t and a FreqIbiBuffer
 ++ Calls initFreqBBQ
 ++ Calls initFreqIbiQueue

bool initFreqCandidatesList (public)
 -- Takes in an empty candidate list
 -- Initializes it based on shape

float calcBiosemFreqHR (public)
 -- Takes in a time, freqCandidatesList_t, FreqBBQParams_t, FreqIbiBuffer  
 ++ Method called by C++ to hook into the algo.
 -- Returns an IBI (or NAN?)

Private Methods
--------------

void initFreqBBQ
 -- Takes in a FreqBBQParams_t
 -- Assigns values based on #defines

void initFreqIbiQueue
 -- Takes in a FreqBBQParams_t and a FreqIbiBuffer
 -- Initializes the buffer

void frequency_select
 -- Takes a pointer to a freqCandidatesList_t and FreqAlgoParam_t
 -- Mutates the freqCandidatesList

float runFreqBiosemBinaryQualifier
 -- Takes in time, freqCandidatesList, FreqIbiBuffer, FreqBBQParams
 ++ There are 2*Ncandidates + 1 options to consider
 ++ Compute a likelihood of each 
 ++ Select the max likelihood and determine the assoc. frequency
 -- Returns a frequency (or NAN if all rejected)   

float updateFreqState 
 -- Takes a time, strongestFreq, outputFreq, FreqIbiBuffer, FreqBBQParams
 ++ Calls updateFreqBuffer, and then updateState
 -- Mutates the FreqIbiBuffer
 -- outputs an IBI, which is the return value of calcBiosemHR

float updateFreqBuffer
 -- Takes a time, strongestFreq, outputFreq, FreqIbiBuffer, algoParam
 -- Mutates the FreqIbiBuffer.ibiList & stochModelType
 ++ "Remove" older values from the buffer, unless it is the last one left
 ++ Update stochModelType based on state logic
 ++ If an outputFreq was selected, add it to the buffer. 
 -- outputs an IBI

void updateFreqStats
 -- Takes a FreqIbiBuffer, FreqBBQParams
 -- Updates the FreqIbiBuffer mu and sigma
 ++ Switches on stochModelType
 ++ Must compute the sum over the circular buffer

