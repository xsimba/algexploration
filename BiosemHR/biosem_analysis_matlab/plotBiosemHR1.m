function plt = plotBiosemHR1(data, ch, figNum)
%
% function plotHistoLengthscale(data, label)
%

if ~exist('ch', 'var'),
    ch = 3;
end

if ~exist('figNum', 'var'),
    figNum = 5;
end

figure(figNum);
clf
colring = 'kcgrbmycgrb';
tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

curTrack = tracks{ch};
eval (['thisdata = data.',curTrack,';']);
time = thisdata.biosemTime;
short = thisdata.biosemStatShort;
med = thisdata.biosemStatMed;
long = thisdata.biosemStatLong;
interbeat = thisdata.interbeat;
rchang = thisdata.biosemChangeRateHR;
biosemQual = thisdata.biosemQual;

%
% 10-beat Median code
%
Nrbeats2calc = 10;
interbeatCut = interbeat(:,interbeat(2,:) > 60/200 & interbeat(2,:) < 60/40);
MedianFiltHR = ones(1, length(interbeatCut(1,:)))*80;
for j = (Nrbeats2calc+1):length(MedianFiltHR),
    MedianFiltHR(j) = 60 ./ median(interbeatCut(2,j-Nrbeats2calc:j));
end

plot(interbeat(1,:), 60./interbeat(2,:), [colring(ch),'x'], 'MarkerSize', 6);
hold on;

plot(interbeatCut(1,:), MedianFiltHR, ['b-'], 'MarkerSize', 6, 'Color', [0.3 0.3 1]);

%
% confidence calculation: binary AND fusion of high rate of change and high variance
%
lowConf = biosemQual.medConfidence > 12;
lowConf = lowConf(1:length(time));
%lowConf = time*0;
highConf = ~lowConf;


% else
%     
%     for k = 5:length(time),
%         biosemChangeStat(k) = std(rchang(k-4:k));
%     end
%     
%     lowConf = biosemChangeStat > 0.9 & med.sigmaHR > 10;
%     highConf = ~lowConf;
%     
% end


plot(time(highConf), med.muHR(highConf), 'k.', 'LineWidth', 2);
plot(time(lowConf), med.muHR(lowConf), '.', 'LineWidth', 2, 'Color', [0.75 0.75 0.75]);

set(gca, 'YLim', [40 200]);
ylabel(curTrack);
xlabel('time [s]');
title('Biosemantic HR vs. Median HR');

