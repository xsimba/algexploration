function writeBiosemDat(data, filePrefix)
%
% verify that all necessary channels are present
% prereq is to perform a load of a data file.  E.g., 
% load('\\105.140.2.7\share\DataAnalysis\DataBases\biosemantic HR\matfiles\imec_bicycle_sep11_beats_metrics.mat')

tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
tracks = {tracksPPG{:}};

for i = 1:8,
    trackName = tracksPPG{i};
    eval(['ibi_time = data.',trackName,'.beats(1,2:end);']);
    eval(['ibi = data.',trackName,'.ibi;']);
    
    eval(['biosemOutTime = data.',trackName,'.biosemInterbeats(1,:);']);
    eval(['biosemOutIbi = data.',trackName,'.biosemInterbeats(2,:);']);    
    eval(['biosemTypeDebug = data.',trackName,'.biosemDebug.biosemType;']);
    eval(['biosemMuTime = data.',trackName,'.biosemTime;']);
    eval(['biosemMu = data.',trackName,'.biosemStatShort.muHR;']);    
    eval(['biosemSigma = data.',trackName,'.biosemStatShort.sigmaHR;']);
    eval(['biosemDeltaHrFilt = data.',trackName,'.biosemDebug.trialChangeHr;']);


%
% inputs = input beat metrics, output = biosem beat metrics, outputB =
% biosemantic filtered HR metrics
%
%    inputs = [ibi_time(:) ibi(:) biosemDeltaHrFilt(:)];
%    outputs = [biosemOutTime(:) biosemOutIbi(:) biosemTypeDebug(:)];
    inputs = [ibi_time(:) ibi(:) biosemTypeDebug(:)];
    outputs = [biosemOutTime(:) biosemOutIbi(:) ];
    outputsB = [biosemMuTime(:) biosemMu(:) biosemSigma(:)];

%     csvwrite([filePrefix, '-inp',num2str(i),'.csv'], inputs);
%     csvwrite([filePrefix, '-out',num2str(i),'.csv'], outputs);

    fid = fopen([filePrefix, '-inp',num2str(i),'.csv'], 'w+');
    fprintf(fid,'%12.3f, %12.3f, %12.3f,\n',inputs');
    fclose(fid);
    
    fid = fopen([filePrefix, '-out',num2str(i),'.csv'], 'w+');
%    fprintf(fid,'%12.3f, %12.3f, %12.3f,\n',outputs');
    fprintf(fid,'%12.3f, %12.3f,\n',outputs');
    fclose(fid);
    
    fid = fopen([filePrefix, '-outB',num2str(i),'.csv'], 'w+');
    fprintf(fid,'%12.3f, %12.3f, %12.3f, \n',outputsB');
    fclose(fid);
    

end
