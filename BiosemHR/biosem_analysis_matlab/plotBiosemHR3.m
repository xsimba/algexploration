function plt = plotBiosemHR3(data, ch, figNum)
%
% function plotHistoLengthscale(data, label)
%

if ~exist('ch', 'var'),
    ch = 3;
end

if ~exist('figNum', 'var'),
    figNum = 5;
end

figure(figNum);
%clf
colring = 'kcgrbmycgrb';
tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

curTrack = tracks{ch};
eval (['thisdata = data.',curTrack,';']);
time = thisdata.biosemTime;
med = thisdata.biosemStatMed;
biosemQual = thisdata.biosemQual;

%
% confidence calculation: binary AND fusion of high rate of change and high variance
%
lowConf = biosemQual.medConfidence > 12;
lowConf = lowConf(1:length(time));
%lowConf = time*0;
highConf = ~lowConf;


% else
%     
%     for k = 5:length(time),
%         biosemChangeStat(k) = std(rchang(k-4:k));
%     end
%     
%     lowConf = biosemChangeStat > 0.9 & med.sigmaHR > 10;
%     highConf = ~lowConf;
%     
% end


plot(time(highConf), med.muHR(highConf), 'k.', 'LineWidth', 2);
hold on;
plot(time(lowConf), med.muHR(lowConf), '.', 'LineWidth', 2, 'Color', [0.75 0.75 0.75]);

set(gca, 'YLim', [40 200]);
ylabel(curTrack);
xlabel('time [s]');
title('Biosemantic Beats HR');

