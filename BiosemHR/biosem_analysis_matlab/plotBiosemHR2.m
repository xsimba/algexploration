function plt = plotBiosemHR3(data, refData, ch, figNum, offset)
%
% function plotHistoLengthscale(data, label)
%

if ~exist('ch', 'var'),
    ch = 3;
end

if ~exist('figNum', 'var'),
    figNum = 5;
end

if ~exist('offset', 'var'),
    offset = 0;
end

figure(figNum);
clf
colring = 'kcgrbmycgrb';
tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

curTrack = tracks{ch};
eval (['thisdata = data.',curTrack,';']);
time = thisdata.biosemTime;
short = thisdata.biosemStatShort;
med = thisdata.biosemStatMed;
long = thisdata.biosemStatLong;
%interbeat = thisdata.interbeat;

%interbeat(1,:) = thisdata.beats(1,2:end);
%interbeat(2,:) = thisdata.ibi;

interbeat(1,:)  = (1:length(thisdata.ibi_acf))/2;
interbeat(2,:) = thisdata.ibi_acf;

rchang = thisdata.biosemChangeRateHR;
biosemQual = thisdata.biosemQual;

%
% 10-beat Median code
%
Nrbeats2calc = 10;
interbeatCut = interbeat(:,interbeat(2,:) > 60/200 & interbeat(2,:) < 60/40);
MedianFiltHR = ones(1, length(interbeatCut(1,:)))*80;
for j = (Nrbeats2calc+1):length(MedianFiltHR),
    MedianFiltHR(j) = 60 ./ median(interbeatCut(2,j-Nrbeats2calc:j));
end

plot(interbeat(1,:), 60./interbeat(2,:), [colring(ch),'x'], 'MarkerSize', 6);
hold on;

%plot(interbeatCut(1,:), MedianFiltHR, ['b-'], 'MarkerSize', 6, 'Color', [0.3 0.3 1]);


%
% confidence calculation: binary AND fusion of high rate of change and high variance
%
lowConf = biosemQual.medConfidence > 12;
lowConf = lowConf(1:length(time));
%lowConf = time*0;
highConf = ~lowConf;


% else
%     
%     for k = 5:length(time),
%         biosemChangeStat(k) = std(rchang(k-4:k));
%     end
%     
%     lowConf = biosemChangeStat > 0.9 & med.sigmaHR > 10;
%     highConf = ~lowConf;
%     
% end

plot(time(highConf), med.muHR(highConf), 'k.', 'LineWidth', 2);
plot(time(lowConf), med.muHR(lowConf), '.', 'LineWidth', 2, 'Color', [0.75 0.75 0.75]);

plot((1:length(refData))+offset, refData, 'r-', 'LineWidth', 1, 'Color', [1 0 0.6]); 

legend({'raw data', 'Biosem HR', 'Biosem HR (low conf)', 'Polar'});

set(gca, 'YLim', [40 200]);
ylabel(curTrack);
xlabel('time [s]');
title('Biosemantic HR (black) vs. Polar (purple)');

