function out = trimVal (in, percent)
%
% generate trimmed version of the input
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014

l = length(in);

if percent>1,
    error('2nd argument should be a percentage');
end

if l < 10,
    out = in;
    return
end

sortedIn = sort(in);
minInd = max(round(l * percent/2),1);
maxInd = round(l * 1-percent/2);

out = in(minInd:maxInd);

end