function data = computeBiosemBeatFilter(data, curTrack, bioLimits, metric)
%
% compute the running stats
%
% Inputs: data requires the 'metric' field (default interbeat) for each of the
% five listed tracks.
%
% Outputs: produces 'biosemInterbeats' and 'changeRateHR' tracks.
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014

blockSize = 0.5;

if ~exist('curTrack', 'var'),
    curTrack = 'ppg.a';
end

if ~exist('bioLimits', 'var'),
    minHR = 30;
    maxHR = 260;
    rateLimitDown = 0.3;
    rateLimitUp = 0.3;
    windowSize = 5;
else
    minHR = bioLimits.minHR;
    maxHR = bioLimits.maxHR;
    rateLimitDown = bioLimits.rateLimitDown;
    rateLimitUp   = bioLimits.rateLimitUp;
    windowSize = bioLimits.windowSize;
end

if ~exist('metric', 'var'),
    metric = 'ibi';
end

%
% confirm that the track is available to be processed
%
evalString=['assert(isfield(data.',curTrack,', ''', metric, '''));'];
eval(evalString);

%
% prepare inputs for beat-based vs periodic time-based processing
%
if (strcmpi(metric, 'interbeat')),
    eval(['ibeats = data.',curTrack,'.interbeat;']);
elseif (strcmpi(metric, 'ibi')), % ibi's are beat based
    eval(['ibeats(1,:) = data.',curTrack,'.beats(1,2:end);']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
elseif (strcmpi(metric, 'ibi_acf')), % ibi's are beat based
    % assume block-based (assume 0.5s blocks)
    eval(['ibeats(1,:) = (1:length(data.',curTrack,'.', metric, '))*blockSize;']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
elseif (strcmpi(metric, 'ibi_freq')), % ibi's are beat based
    % assume second-based 
    eval(['ibeats(1,:) = (1:length(data.',curTrack,'.', metric, '));']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
else
    error(['unexpected ibi track type, ', curTrack,'.',metric]);
end

counter = 1;
changeRateHR = [];
changeRateHR(1) = 0;
biosemInterbeats = [];
biosemTypeDebug = [0];
biosemInterbeats(1:2,1) = ibeats(1:2,1);
biosemInterbeats(2,1) = 60/80; % initialize (todo make this better!)

%
% process ibeats, one-by-one
%
for k = 2:length(ibeats(1,:)),
    
    thisIbeat = ibeats(:,k);
    
    thisTime = thisIbeat(1);
    thisIbi  = thisIbeat(2);
    
    %
    % create buffers for interbeats
    %
    recentHR = staticBioSemStatFit(ibeats, thisTime, windowSize);
    recentHRFilt = staticBioSemStatFit(biosemInterbeats, thisTime, windowSize);
    
    %
    % filter rule scalars
    %    
    lastHR      = recentHR.muHR;
    lastSig     = recentHR.sigmaHR;
    lastHRFilt  = recentHRFilt.muHR;
    lastSigFilt = recentHRFilt.sigmaHR;
    
    thisHR = 60 / thisIbi;
    trialHR = 2 * thisHR;
    
    deltaHR = (thisHR - lastHR) / mean([thisHR lastHR]);
    deltaHRFilt = (thisHR - lastHRFilt) / mean([thisHR lastHRFilt]); 
    
    trialDeltaHRFilt = (trialHR - lastHRFilt) / mean([trialHR lastHRFilt]);
    
    startupFlag = counter < 20;
%    startupFlag = 0;

    % does this beat period make the biosemantic cut?  If not, ibiType == 0
    ibiType = biosemBeatFilter (startupFlag, thisHR, deltaHR, ...
        deltaHRFilt, trialDeltaHRFilt, lastHRFilt, lastSigFilt, lastSig, bioLimits);
        
    if ibiType > 0,
        counter = counter + 1;
        if ibiType == 3,
            thisIbeat(2) = thisIbeat(2) / 2;
        end
        biosemInterbeats(:,counter) = thisIbeat;
    end
    
    %
    % debug output
    %
    biosemTypeDebug(k) = ibiType;
    lastHRDebug(k)  = recentHR;
    lastHRFiltDebug(k)  = recentHRFilt;    
    thisHRDebug(k) = thisHR;
    changeRateHR(k) = deltaHR;
    changeRateHRFilt(k) = deltaHRFilt;
    trialHRDebug(k) = trialHR;
    trialChangeHRFilt(k) = trialDeltaHRFilt;
        
end

eval(['data.',curTrack,'.biosemInterbeats    = biosemInterbeats;']);
eval(['data.',curTrack,'.biosemChangeRateHr  = changeRateHRFilt;']);
eval(['data.',curTrack,'.biosemDebug.trialChangeHR  = trialChangeHRFilt;']);
eval(['data.',curTrack,'.biosemDebug.lastHRDebug  = lastHRDebug;']);
eval(['data.',curTrack,'.biosemDebug.lastHRFiltDebug  = lastHRFiltDebug;']);
eval(['data.',curTrack,'.biosemDebug.biosemType    = biosemTypeDebug;']);

end

function beatType = biosemBeatFilter(startupFlag, thisHR, deltaHR, ...
        deltaHRFilt, trialDeltaHRFilt, lastHRFilt, lastSigFilt, ...
        lastSig, bioLimits)
%
% Case logic for hypothesis testing.  There are some tricks in here
% about incrementing lastInd and counter only under certain circumstances.
% The counter is designed to index over the new list (not needed if
% biosemInterbeats is a container with 'push_back').
%
% Also, the lastInd is designed to skip over beats that are too short
% to keep extra beats from corrupting the calculations.
%

beatType = 0; % ignore by default

% filter based on heart rate absolute value
if (thisHR < bioLimits.minHR || thisHR > bioLimits.maxHR),
    return
end

% hypothesis test based on rate of change of HR & various
% hypotheses.  The first hypothesis is only
%
if ((startupFlag || isnan(lastSigFilt) || ...
        lastSig < lastSigFilt) && ...
        deltaHR < bioLimits.rateLimitUp && ...
        deltaHR > -bioLimits.rateLimitDown),
    beatType = 1;
elseif (deltaHRFilt < bioLimits.rateLimitUp && ...
        deltaHRFilt > -bioLimits.rateLimitDown && ...
        abs(thisHR-lastHRFilt)/lastSig < 1.5),
    beatType = 2;
elseif (deltaHRFilt < -bioLimits.rateLimitDown && ...
        trialDeltaHRFilt < bioLimits.rateLimitUp && ...
        trialDeltaHRFilt  > -bioLimits.rateLimitDown && ...
        thisHR*2<bioLimits.maxHR && ...
        abs(thisHR*2-lastHRFilt)/lastSigFilt < 1.5),        % skipped beat!
    beatType = 3;
end

end
