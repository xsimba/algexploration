function data = computeBiosemStaticStat(data, curTrack, metric)
%
% compute the running stats
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014

if ~exist('curTrack', 'var'),
    curTrack = 'ppg.a';
end

if ~exist('metric', 'var'),
    metric = 'ibi';
end

timeGridInterval = 1.0;
blockSize = 0.5;

%
% confirm that the track is available to be processed
%
evalString=['assert(isfield(data.',curTrack,', ''', metric, '''));'];
eval(evalString);

%
% prepare inputs for beat-based vs periodic time-based processing
%
if (any(strcmpi(metric, {'interbeat', 'biosemInterbeats'}))),
    eval(['thisIbeat = data.',curTrack,'.', metric, ';']);    
elseif (strcmpi(metric, 'ibi')), % ibi's are beat based
    eval(['thisIbeat(1,:) = data.',curTrack,'.beats(1,2:end);']);
    eval(['thisIbeat(2,:) = data.',curTrack,'.', metric, ';']);
else % otherwise block-based (assume 0.5s blocks)
    eval(['thisIbeat(1,:) = (1:length(data.',curTrack,'.', metric, '))*blockSize;']);
    eval(['thisIbeat(2,:) = data.',curTrack,'.', metric, ';']);
end

biosemStatShort = {};
biosemStatMed = {};
biosemStatLong = {};


minTime = thisIbeat(1,1);
maxTime = thisIbeat(1,size(thisIbeat,2));

%
% Update a second at a time, over 5s, 30s, and 120s windows.
%
% TODO: use a Bayesian prior to warm up the windows.
%

biosemTime = round(minTime):timeGridInterval:round(maxTime);

for k = 1:length(biosemTime),
    biosemStatShort{k} = staticBioSemStatFit(thisIbeat, biosemTime(k), 5.0);
    biosemStatMed{k} = staticBioSemStatFit(thisIbeat, biosemTime(k), 20.0);
    biosemStatLong{k} = staticBioSemStatFit(thisIbeat, biosemTime(k), 120.0);
end

eval(['data.',curTrack,'.biosemTime = biosemTime;']);
if (eval(['isfield(data.',curTrack,', ''biosemStatShort'' )'])),
    evalstring = ['data.',curTrack,'.biosemStatShort = [];'];
    eval(evalstring);
    evalstring = ['data.',curTrack,'.biosemStatMed = [];'];
    eval(evalstring);
    evalstring = ['data.',curTrack,'.biosemStatLong= [];'];
    eval(evalstring);
end

for k = 1:length(biosemTime),
    eval(['data.',curTrack,'.biosemStatShort.mu(k)    = biosemStatShort{k}.mu;']);
    eval(['data.',curTrack,'.biosemStatShort.sigma(k) = biosemStatShort{k}.sigma;']);
    eval(['data.',curTrack,'.biosemStatShort.muHR(k)  = biosemStatShort{k}.muHR;']);
    eval(['data.',curTrack,'.biosemStatShort.sigmaHR(k) = biosemStatShort{k}.sigmaHR;']);
    
    eval(['data.',curTrack,'.biosemStatMed.mu(k)    = biosemStatMed{k}.mu;']);
    eval(['data.',curTrack,'.biosemStatMed.sigma(k) = biosemStatMed{k}.sigma;']);
    eval(['data.',curTrack,'.biosemStatMed.muHR(k)  = biosemStatMed{k}.muHR;']);
    eval(['data.',curTrack,'.biosemStatMed.sigmaHR(k) = biosemStatMed{k}.sigmaHR;']);
    
    eval(['data.',curTrack,'.biosemStatLong.mu(k)    = biosemStatLong{k}.mu;']);
    eval(['data.',curTrack,'.biosemStatLong.sigma(k) = biosemStatLong{k}.sigma;']);
    eval(['data.',curTrack,'.biosemStatLong.muHR(k)  = biosemStatLong{k}.muHR;']);
    eval(['data.',curTrack,'.biosemStatLong.sigmaHR(k) = biosemStatLong{k}.sigmaHR;']);
end

end
