function gaussFit = staticBioSemStatFit(interbeats, t, windowSize, config)
% 
% compute a gaussian fit to the quantity in interbeats at the 
% specified time range, filtered with the parameters in config.
% which corresponds to a firm min/max heart rate cutoff and outlier
% trimming to the specified percentage.
%
% presumes that interbeats is a 2 x Ntime vector where the first row is
% time in seconds and second row is interbeats in seconds
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014


tRange = [max(0,t-windowSize), t];

%
% constants
%
if ~exist('config', 'var'),
    minHr = 30;
    maxHr = 260;
    trimPercent = 0;
else
    minHr = config.minHr;
    maxHr = config.maxHr;
    trimPercent = config.trimPerc;
end

mu = {};
sigma = {};


%
% calculate gaussian fits (stationary case)
%
thisdat = interbeats;

% time range
rng = find(thisdat(1,:)>=tRange(1) & thisdat(1,:) < tRange(2));

% outlier rejection based on rate limits
ind = find(thisdat(2,rng) <= 60/minHr & thisdat(2,rng) >= 60/maxHr);
val = trimVal(thisdat(2,rng(ind)), trimPercent);

% fit to a gaussian
gaussFit.mu = mean(val);
gaussFit.sigma = std(val);
gaussFit.muHR = mean(60./val);
gaussFit.sigmaHR = std(60./val);


