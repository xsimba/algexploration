function data = computeBiosemBeatFilterOld(data, curTrack, bioLimits, metric)
%
% compute the running stats
%
% Inputs: data requires the 'metric' field (default interbeat) for each of the
% five listed tracks.
%
% Outputs: produces 'biosemInterbeats' and 'changeRateHR' tracks.
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014

blockSize = 0.5;

if ~exist('curTrack', 'var'),
    curTrack = 'ppg.a';
end

if ~exist('bioLimits', 'var'),
    minHR = 30;
    maxHR = 260;
    rateLimitDown = 0.3;
    rateLimitUp = 0.3;
    windowSize = 5;
else
    minHR = bioLimits.minHR;
    maxHR = bioLimits.maxHR;
    rateLimitDown = bioLimits.rateLimitDown;
    rateLimitUp   = bioLimits.rateLimitUp;
    windowSize = bioLimits.windowSize;
end

if ~exist('metric', 'var'),
    metric = 'ibi';
end

%
% confirm that the track is available to be processed
%
evalString=['assert(isfield(data.',curTrack,', ''', metric, '''));'];
eval(evalString);

%
% prepare inputs for beat-based vs periodic time-based processing
%
if (strcmpi(metric, 'interbeat')),
    eval(['thisIbeat = data.',curTrack,'.interbeat;']);
elseif (strcmpi(metric, 'ibi')), % ibi's are beat based
    eval(['thisIbeat(1,:) = data.',curTrack,'.beats(1,2:end);']);
    eval(['thisIbeat(2,:) = data.',curTrack,'.', metric, ';']);
else % otherwise block-based (assume 0.5s blocks)
    eval(['thisIbeat(1,:) = (1:length(data.',curTrack,'.', metric, '))*blockSize;']);
    eval(['thisIbeat(2,:) = data.',curTrack,'.', metric, ';']);
end

counter = 2;
changeRateHR = [];
changeRateHR(1) = 0;
biosemInterbeats = [];
biosemTypeDebug = [0];
biosemInterbeats(1:2,1) = thisIbeat(1:2,1);
biosemInterbeats(2,1) = 60/80; % initialize (todo make this better!)
lastInd = 1;

for k = 2:length(thisIbeat(1,:)),
    %
    % estimate the rate of change of naive HR
    % the '60' seconds is redundant for debugging.
    %
    
    %
    % also for debugging store the history of temp variables for
    % all k
    %
    
    thisTime = thisIbeat(1,k);
    recentHR = staticBioSemStatFit(thisIbeat, thisTime, windowSize);
    recentHRFilt = staticBioSemStatFit(biosemInterbeats, thisTime, windowSize);
    lastHRFiltDebug(k)  = recentHRFilt;
    lastHRDebug(k)  = recentHR;
    lastHR =  recentHR.muHR;
    lastHRFilt  = recentHRFilt.muHR;
    
    thisHR = 60/thisIbeat(2,k);
    thisHRDebug(k) = thisHR;
    
    changeRateHR(k) = (thisHR - lastHR) / mean([thisHR lastHR]);
    changeRateHRFilt(k) = (thisHR - lastHRFilt) / mean([thisHR lastHRFilt]);
    
    trialHR = 2 * 60 ./thisIbeat(2,k);
    trialHRDebug(k) = 2 * 60 ./thisIbeat(2,k);
    trialChangeHRFilt(k) = (trialHR - lastHRFilt) / mean([trialHR lastHRFilt]);
    trialChangeHR(k) = (trialHR - lastHR) / mean([trialHR lastHR]);
    
    %
    % Nasty case logic for hypothesis testing.  There are some tricks in here
    % about incrementing lastInd and counter only under certain circumstances.
    % The counter is designed to index over the new list (not needed if
    % biosemInterbeats is a container with 'push_back').
    %
    % Also, the lastInd is designed to skip over beats that are too short
    % to keep extra beats from corrupting the calculations.
    %
    
    % filter based on heart rate absolute value
    if (thisHR < minHR),
        lastInd = k;
        continue
    elseif(thisHR > maxHR),
        continue
    end
    
    % hypothesis test based on rate of change of HR & various
    % hypotheses.  The first hypothesis is only
    %
    if ((counter < 20 || isnan(recentHRFilt.sigmaHR) || ...
            recentHR.sigmaHR < recentHRFilt.sigmaHR) && ...
            changeRateHR(k) < rateLimitUp && ...
            changeRateHR(k) > -rateLimitDown),
        biosemInterbeats(:,counter) = thisIbeat(1:2,k);
        biosemTypeDebug(counter) = 1;
        lastInd = k;
        counter = counter + 1;
    elseif (changeRateHRFilt(k) < rateLimitUp && ...
            changeRateHRFilt(k) > -rateLimitDown && ...
            abs(thisHR-recentHRFilt.muHR)/recentHR.sigmaHR < 1.5),
        biosemInterbeats(:,counter) = thisIbeat(1:2,k);
        biosemTypeDebug(counter) = 2;
        lastInd = k;
        counter = counter + 1;
    elseif (changeRateHRFilt(k) < -rateLimitDown && ...
            trialChangeHRFilt(k) < rateLimitUp && ...
            trialChangeHRFilt(k) > -rateLimitDown && ...
            thisHR*2<maxHR && ...
            abs(thisHR*2-recentHRFilt.muHR)/recentHRFilt.sigmaHR < 1.5),            % skipped beat!
        lastInd = k;
        biosemInterbeats(1,counter) = thisIbeat(1,k);
        biosemInterbeats(2,counter) = thisIbeat(2,k)/2;
        biosemTypeDebug(counter) = 3;
        counter = counter+1;
    end
end

eval(['data.',curTrack,'.biosemInterbeats    = biosemInterbeats;']);
eval(['data.',curTrack,'.biosemChangeRateHr  = changeRateHRFilt;']);
eval(['data.',curTrack,'.biosemDebug.trialChangeHR  = trialChangeHRFilt;']);
eval(['data.',curTrack,'.biosemDebug.lastHRDebug  = lastHRDebug;']);
eval(['data.',curTrack,'.biosemDebug.lastHRFiltDebug  = lastHRFiltDebug;']);
eval(['data.',curTrack,'.biosemDebug.biosemType    = biosemTypeDebug;']);



end
