function data = computeBiosemQuality(data, curTrack, metricScaling)
%
% compute the running stats
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014

if ~exist('curTrack', 'var'),
    curTrack = 'ppg.a';
end

if ~exist('metricScaling', 'var'),
    metricScaling.changeRate = 0.7;
    metricScaling.estHrSigma = 3;
end


eval (['thisdata = data.',curTrack,';']);
time      = thisdata.biosemTime;
short     = thisdata.biosemStatShort;
med       = thisdata.biosemStatMed;
long      = thisdata.biosemStatLong;
rchang    = thisdata.biosemChangeRateHr;
rchangTim = thisdata.biosemInterbeats(1,:);

%
% confidence calculation: std in rate of change (over short time) and
%       estimate covariance
%
% biosemChangeStat = [];
% for k = 5:length(time),
%     biosemChangeStat(k) = std(rchang(k-4:k));
% end

biosemQual.changeRate = rchang / metricScaling.changeRate;
biosemQual.medConfidence = med.sigmaHR / metricScaling.estHrSigma;

%
% add biosemQual to data
%
thisdata.biosemQual = biosemQual;
eval (['data.',curTrack,' = thisdata;']);


end