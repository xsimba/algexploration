function data = computeBiosemBeatFilter(data, curTrack, bioLimits, metric)
%
% compute the running stats
%
% Inputs: data requires the 'metric' field (default interbeat) for each of the
% five listed tracks.
%
% Outputs: produces 'biosemInterbeats' and 'changeRateHr' tracks.
%
% Asif Khalak
% asif.khalak@samsung.com
% 4 Sept 2014

blockSize = 0.5;
defaultHrSigma = 0;

if ~exist('curTrack', 'var'),
    curTrack = 'ppg.a';
end

if ~exist('bioLimits', 'var'),
    bioLimits.minHR = 30;
    bioLimits.maxHR = 260;
    bioLimits.rateLimitDown = 0.15;
    bioLimits.rateLimitUp = 0.15;
    windowSize = 5;
else
    windowSize = bioLimits.windowSize;
end

if ~exist('metric', 'var'),
    metric = 'ibi';
end

%
% confirm that the track is available to be processed
%
evalString=['assert(isfield(data.',curTrack,', ''', metric, '''));'];
eval(evalString);

%
% prepare inputs for beat-based vs periodic time-based processing
%
if (strcmpi(metric, 'interbeat')),
    eval(['ibeats = data.',curTrack,'.interbeat;']);
elseif (strcmpi(metric, 'ibi')), % ibi's are beat based
    eval(['ibeats(1,:) = data.',curTrack,'.beats(1,2:end);']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
elseif (strcmpi(metric, 'ibi_acf')), % ibi's are beat based
    % assume block-based (assume 0.5s blocks)
    eval(['ibeats(1,:) = (1:length(data.',curTrack,'.', metric, '))*blockSize;']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
elseif (strcmpi(metric, 'ibi_freq')), % ibi's are beat based
    % assume second-based
    eval(['ibeats(1,:) = (1:length(data.',curTrack,'.', metric, '));']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
elseif (strcmpi(metric, 'ibi_hilbert')), % ibi's are hilbert transform based
    eval(['ibeats(1,:) = (1:length(data.',curTrack,'.', metric, '))*blockSize;']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, ';']);
    
elseif (strcmpi(metric, 'tde_ibi')), % ibi's are hilbert transform based
    %     eval(['ibeats(1,:) = data.',curTrack,'.beats(1,2:end);']);
    eval(['ibeats(1,:) = data.',curTrack,'.', metric, '.timestamps;']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, '.ibi;']);
elseif (strcmpi(metric, 'hilbert_fusion')), % ibi's are hilbert transform based
%     eval(['ibeats(1,:) = data.',curTrack,'.beats(1,2:end);']);
    eval(['ibeats(1,:) = data.',curTrack,'.', metric, '.timestamps;']);
    eval(['ibeats(2,:) = data.',curTrack,'.', metric, '.ibi;']);
    
else
    error(['unexpected ibi track type, ', curTrack,'.',metric]);
end

counter = 1;
changeRateHr = [];
changeRateHr(1) = 0;
biosemInterbeats = [];
biosemTypeDebug = [0];
biosemInterbeats(1:2,1) = ibeats(1:2,1);
biosemInterbeats(2,1) = 60/80; % initialize (todo make this better!)

%
% create buffers for interbeats with initialization
%
% recentHr.buffer = [60/biosemInterbeats(2,1)];
% recentHr.bufferTime = [biosemInterbeats(1,1)];
% recentHr.len = 1;
% recentHr.sum = 60/biosemInterbeats(2,1);
% recentHr.sqsum = 60*60/biosemInterbeats(2,1)/biosemInterbeats(2,1);

if (isnan(ibeats(2,1)) || 60/ibeats(2,1) < bioLimits.minHR || 60/ibeats(2,1)> bioLimits.maxHR)
    recentHr.buffer = [];
    recentHr.bufferTime = [];
    recentHr.len = 0;
    recentHr.sum = 0;
    recentHr.sqsum = 0;
    recentHrFilt.buffer = [];
    recentHrFilt.bufferTime = [];
    recentHrFilt.len = 0;
    recentHrFilt.sum = 0;
    recentHrFilt.sqsum = 0;
else
    recentHr.buffer = [60/ibeats(2,1)];
    recentHr.bufferTime = [ibeats(1,1)];
    recentHr.len = 1;
    recentHr.sum = 60/ibeats(2,1);
    recentHr.sqsum = 60*60/ibeats(2,1)/ibeats(2,1);
    recentHrFilt.buffer = [60/biosemInterbeats(2,1)];
    recentHrFilt.bufferTime = [biosemInterbeats(1,1)];
    recentHrFilt.len = 1;
    recentHrFilt.sum = 60/biosemInterbeats(2,1);
    recentHrFilt.sqsum = 60*60/biosemInterbeats(2,1)/biosemInterbeats(2,1);
end

lastHrSig = defaultHrSigma;
lastHrSigFilt = defaultHrSigma;

%
% process ibeats, one-by-one
%
for k = 2:length(ibeats(1,:)),
    
    thisIbeat = ibeats(:,k);
    
    thisTime = thisIbeat(1);
    thisIbi  = thisIbeat(2);
    
    thisHr = 60 / thisIbi;
    trialHr = 2 * thisHr;
    
    % filter based on heart rate absolute value
    if (thisHr < bioLimits.minHR || thisHr > bioLimits.maxHR || isnan(thisHr)),
        biosemTypeDebug(k) = 0;
        lastHrDebug(k)  = recentHr;
        lastHrFiltDebug(k)  = recentHrFilt;
        lastHrSigFiltDebug(k)  = 0;
        thisHrDebug(k) = 0;
        changeRateHr(k) = 0;
        changeRateHrFilt(k) = 0;
        trialHrDebug(k) = 0;
        trialChangeHrFilt(k) = 0;
        continue
    end
    
    %
    % trim all items on the buffers older than windowSize except
    % the last one
    %
    while (recentHr.len > 1 && thisTime - recentHr.bufferTime(1,end) > windowSize),
        recentHr.sum = recentHr.sum - recentHr.buffer(end);
        recentHr.sqsum = recentHr.sqsum - recentHr.buffer(end)*recentHr.buffer(end);
        recentHr.buffer = recentHr.buffer(:,1:end-1);
        recentHr.bufferTime = recentHr.bufferTime(:,1:end-1);
        recentHr.len = recentHr.len - 1;
    end
    
    while (recentHrFilt.len > 1 && thisTime - recentHrFilt.bufferTime(1,end) > windowSize),
        recentHrFilt.sum = recentHrFilt.sum - recentHrFilt.buffer(end);
        recentHrFilt.sqsum = recentHrFilt.sqsum - recentHrFilt.buffer(end)*recentHrFilt.buffer(end);
        recentHrFilt.buffer = recentHrFilt.buffer(:,1:end-1);
        recentHrFilt.bufferTime = recentHrFilt.bufferTime(:,1:end-1);
        recentHrFilt.len = recentHrFilt.len -1;
    end
    
    %
    % filter rule scalars.  Slight difference from original in that the
    % sigma value is taken on the period (and converted to Hr units),
    % rather than in Hr units directly.
    %
    lastHrMu      = recentHr.sum / max(1, recentHr.len);
    lastHrMuFilt  = recentHrFilt.sum / max(1, recentHrFilt.len);
    if (lastHrMu < bioLimits.minHR),
        lastHrMu = NaN;  % replace with a code if NaN not acceptable
    end
    if (lastHrMuFilt < bioLimits.minHR),
        lastHrMuFilt = NaN;  % replace with a code if NaN not acceptable
    end
    
    if (recentHr.len > 1),
        lastVar     = (recentHr.sqsum - recentHr.sum*recentHr.sum/recentHr.len)/(recentHr.len-1);
        lastHrSig     = sqrt(abs(lastVar));  % possible roundoff negative if buffer elements equal.
        assert(abs(lastHrSig - std(recentHr.buffer)) < 0.01);
    else
        lastHrSig     = defaultHrSigma;
    end
    
    if (recentHrFilt.len > 1),
        lastVarFilt     = (recentHrFilt.sqsum - recentHrFilt.sum*recentHrFilt.sum/recentHrFilt.len)/(recentHrFilt.len-1);
        lastHrSigFilt     = sqrt(abs(lastVarFilt));
        assert(abs(lastHrSigFilt - std(recentHrFilt.buffer)) < 0.01);
    else
        lastHrSigFilt     = defaultHrSigma;
    end
    
    %
    % compute rate of change of candidate beat from recent
    %
    %    deltaHr = (thisHr - lastHrMu) / mean([thisHr lastHrMu]);
    %    deltaHrFilt = (thisHr - lastHrMuFilt) / mean([thisHr lastHrMuFilt]);
    %    trialDeltaHrFilt = (trialHr - lastHrMuFilt) / mean([trialHr lastHrMuFilt]);
    deltaHr = (thisHr - lastHrMu) / lastHrMu;
    deltaHrFilt = (thisHr - lastHrMuFilt) / lastHrMuFilt;
    trialDeltaHrFilt = (trialHr - lastHrMuFilt) / lastHrMuFilt;
    
    startupFlag = counter < 20;
    
    %
    % add the beat to the recentHr buffer, and update stats
    %
    recentHr.len    = recentHr.len + 1;
    recentHr.sum    = recentHr.sum + thisHr;
    recentHr.sqsum  = recentHr.sqsum + thisHr*thisHr;
    recentHr.buffer = [thisHr, recentHr.buffer];
    recentHr.bufferTime = [thisTime, recentHr.bufferTime];
    
    %
    % Matlab check to make sure that buffer based sums are self-consistent
    %
    assert(abs(recentHr.sum - sum(recentHr.buffer)) < 1e-6);
    assert(abs(recentHr.sqsum - sum(recentHr.buffer.*recentHr.buffer)) < 1e-3);
    
    % does this beat period make the biosemantic cut?  If not, ibiType == 0
    ibiType = biosemBeatFilter (startupFlag, thisHr, deltaHr, ...
        deltaHrFilt, trialDeltaHrFilt, lastHrMuFilt, lastHrSigFilt, lastHrSig, bioLimits);
    
    if ibiType > 0,
        counter = counter + 1;
        if ibiType == 3,
            thisHr       = trialHr;
            thisIbeat(2) = thisIbi/2;
        end
        biosemInterbeats(:,counter) = thisIbeat;
        
        %
        % add filtered beat to the buffer, and update stats
        %
        recentHrFilt.len = recentHrFilt.len + 1;
        recentHrFilt.sum   = recentHrFilt.sum + thisHr;
        recentHrFilt.sqsum = recentHrFilt.sqsum + thisHr * thisHr;
        recentHrFilt.buffer = [thisHr, recentHrFilt.buffer];
        recentHrFilt.bufferTime = [thisTime, recentHrFilt.bufferTime];
    end
    
    %
    % assertion check (for debugging) on the buffer
    %
    assert(abs(recentHrFilt.sum - sum(recentHrFilt.buffer)) < 1e-6);
    assert(abs(recentHrFilt.sqsum - sum(recentHrFilt.buffer.*recentHrFilt.buffer)) < 1e-3);
    
    %
    % debug output
    %
    biosemTypeDebug(k) = ibiType;
    lastHrDebug(k)  = recentHr;
    lastHrFiltDebug(k)  = recentHrFilt;
    lastHrSigFiltDebug(k)  = lastHrSigFilt;
    thisHrDebug(k) = thisHr;
    changeRateHr(k) = deltaHr;
    changeRateHrFilt(k) = deltaHrFilt;
    trialHrDebug(k) = trialHr;
    trialChangeHrFilt(k) = trialDeltaHrFilt;
    
end

eval(['data.',curTrack,'.biosemInterbeats    = biosemInterbeats;']);
eval(['data.',curTrack,'.biosemChangeRateHr  = changeRateHrFilt;']);
eval(['data.',curTrack,'.biosemDebug.trialChangeHr  = trialChangeHrFilt;']);
eval(['data.',curTrack,'.biosemDebug.lastHrDebug  = lastHrDebug;']);
eval(['data.',curTrack,'.biosemDebug.lastHrFiltDebug  = lastHrFiltDebug;']);
eval(['data.',curTrack,'.biosemDebug.biosemType    = biosemTypeDebug;']);
eval(['data.',curTrack,'.biosemDebug.lastHrSigFilt = lastHrSigFiltDebug;']);


end


function beatType = biosemBeatFilter(startupFlag, thisHr, deltaHr, ...
    deltaHrFilt, trialDeltaHrFilt, lastHrFilt, lastHrSigFilt, ...
    lastHrSig, bioLimits)
%
% Case logic for hypothesis testing.  There are some tricks in here
% about incrementing lastInd and counter only under certain circumstances.
% The counter is designed to index over the new list (not needed if
% biosemInterbeats is a% ignore by default
optionType(1) = 0;
optionWeight(1) = 0.1;


% hypothesis test based on rate of change of Hr & various
% hypotheses.  Replace if-elseif-else block with weights and then take a
% max over weights.
%

% Restart hypothesis.  If starting, or if filtered sigma is 0 permissively
% add to filtered beats.
optionType(2) = 1;
optionWeight(2) = 0.9 * ((startupFlag || lastHrSigFilt == 0 || ...
    lastHrSig < lastHrSigFilt) && ...
    deltaHr < bioLimits.rateLimitUp && ...
    deltaHr > -bioLimits.rateLimitDown);

%
% 92% confidence interval of lastHr
%
optionType(3) = 2;
optionWeight(3) = 0.8 * (deltaHrFilt < bioLimits.rateLimitUp && ...
    deltaHrFilt > -bioLimits.rateLimitDown && ...
    abs(thisHr-lastHrFilt)/lastHrSig < 1.5);

%
% skipped beat!  Exploring turning this off with reduction in BD holdoff.
%
optionType(4) = 3;
optionWeight(4) = 0.0* (deltaHrFilt < -bioLimits.rateLimitDown && ...
    trialDeltaHrFilt < bioLimits.rateLimitUp && ...
    trialDeltaHrFilt  > -bioLimits.rateLimitDown && ...
    thisHr*2<bioLimits.maxHR && ...
    abs(thisHr*2-lastHrFilt)/lastHrSigFilt < 1.5);

[junk, ind] = max(optionWeight);
beatType = optionType(ind);

end

