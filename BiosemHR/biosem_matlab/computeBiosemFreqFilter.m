function data = computeBiosemFreqFilter(data, curTrack, bioLimits, algoParam)
%
% Compute the biosemantic filtering on the frequency features.
%
%
% Outputs: produces 'biosemInterbeats' and 'changeRateHr' tracks.
%
% Asif Khalak
% asif.khalak@samsung.com
% 26 Oct 2014

%%
%%%%%%%%%%%%%%%%%%%%%%% initialization %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% constants
%
if ~exist('algoParam', 'var'),
    algoParam.signalPowerCut = 0.10;
    algoParam.deltaT1   = 15;                                  % 5 secs reset time;
    algoParam.deltaT2   = 25;                                 % 10 secs post-reset time;
    algoParam.minSigma   = 4;                                 % standard deviation threshold
    algoParam.windowSize = 10.4;
    algoParam.initialHR = 80;
    algoParam.cvCutoff = 2.5;
    algoParam.diffusionConstant = 0.8;
end

if ~exist('curTrack', 'var'),
    curTrack = 'ppg.a';
end

%
% todo: change this to an assertion block and require to pass in.
% Parametric studies changing the default parameters are fragile.
%
if ~exist('bioLimits', 'var'),
    bioLimits.minHR = 30;
    bioLimits.maxHR = 260;
    bioLimits.rateLimitDown = 0.15;             % .15 or .30
    bioLimits.rateLimitUp = 0.15;
end

%%
% Unpack outputs of HR features
%
eval(['biosemTime = data.',curTrack,'.freq_HR.time;']);
eval(['freq = data.',curTrack,'.freq_HR.ibi_freq;']);
eval(['amp = data.',curTrack,'.freq_HR.amp_freq;']);
eval(['action_flag = data.',curTrack,'.freq_HR.action_flag;']);
eval(['CI = data.',curTrack,'.freq_HR.CI_freq;']);
eval(['numPeaks = data.',curTrack,'.freq_HR.num_pks;']);

biosemInterbeats(1,:) = biosemTime;
mu = zeros(size(biosemTime));
sigma = zeros(size(biosemTime));

recentHrFilt.buffer = [];
recentHrFilt.bufferTime = [];
recentHrFilt.len = 0;
recentHrFilt.stochModelType = 0; % 0 = init, 1 = normal, 2 = reset, 3 = post-reset
recentHrFilt.sigma = (bioLimits.maxHR-bioLimits.minHR)*2;
recentHrFilt.mu = 80;

freqCandidates.freq = [];
freqCandidates.amp  = [];
freqCandidates.filtFlag = [];
freqCandidates.strongestPeakFreq = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%
% Assign period associated with each time step
%
for i = 1:length(biosemTime),
    
    % pack the candidate list
    aFlag = action_flag(i);
    peakLim = numPeaks(i);
    thisTime = biosemTime(i);
%     thisTime
%     thisTime
    
    freqCandidates.freq = freq(:,i);
    freqCandidates.amp  = amp(:,i);
    if aFlag == 1,
        freqCandidates.filtFlag = CI(:,i);
    else
        freqCandidates.filtFlag = ones(size(CI(:,i)));
    end
    freqCandidates.strongestPeakFreq = 0;
    
    %% start Frequency Select %%%
    
    %
    % Perform frequency subtraction for motion compensation.
    % Since the CI = 0 for a knockout (accel) frequency and
    % CI=1 for an open frequency, the product is zero for occluded
    % frequencies
    %
    freqCandidates = frequency_select(freqCandidates, peakLim, algoParam.signalPowerCut);

    
    %% Frequency BBQ %%%%
    
    %
    % if there are candidate amplitudes, then determine status using
    % biosemFreqFilter
    
    if isnan(freqCandidates.strongestPeakFreq),
        outputType = 0;
        outputFreq = 0;
    else
        [outputType, outputFreq] = FreqBiosemBinaryFilter( ...
            thisTime, freqCandidates, recentHrFilt, bioLimits, algoParam);
    end
    
    
    %% Update State %%%%
    
    %
    % assign values.  Currently, the biosemChangeRateHr is a dummy value
    % so as not to break other code using the outputs.
    %
    [biosemInterbeats(2,i), recentHrFilt, biosemChangeRateHr(i)] = update_state(...
        thisTime, freqCandidates.strongestPeakFreq, outputType, outputFreq, ...
        recentHrFilt, algoParam, bioLimits);
%     60/biosemInterbeats(2,i)
%     recentHrFilt.mu 
    mu(i) = recentHrFilt.mu;
    sigma(i) = recentHrFilt.sigma;
    
    
end


%% pack output
%
eval(['data.',curTrack,'.biosemInterbeats    = biosemInterbeats;']);
eval(['data.',curTrack,'.biosemChangeRateHr  = zeros(size(biosemInterbeats));']);
eval(['data.',curTrack,'.biosemDebug.muHR    = mu;']);
eval(['data.',curTrack,'.biosemDebug.sigmaHR  = sigma;']);

end


%%
% subfunction: Frequency Select
%
function  freqCandidates = frequency_select(freqCandidates, peakLim, signalPowerCut)
%function [final_candidates, strongestPeakFreq] = frequency_select(freq_candidates, thisAmp, ...
%    thisCI, peakLim, signalPowerCut)

freqCandidates.filtFlag(peakLim+1:end) = 0;

prunedAmp = freqCandidates.amp .* freqCandidates.filtFlag;
prunedAmp(peakLim+1:end) = 0;

%
% apply the criterion that only peaks greater than signalPowerCut fraction
% of the total *feasible* power are passed.
%
vecofsquares = prunedAmp.*prunedAmp;
totalPower = sum(vecofsquares);
ampRatio = (vecofsquares) / totalPower;

powerCut = ampRatio > signalPowerCut;
freqCandidates.filtFlag = freqCandidates.filtFlag .* powerCut;

%
% pick the strongest peak
%
anyPeaks = sum(freqCandidates.filtFlag);
if (anyPeaks > 0),
    ind = find(freqCandidates.filtFlag==1);
    freqCandidates.strongestPeakFreq = freqCandidates.freq(ind(1));
else
    freqCandidates.strongestPeakFreq = nan;
end

end

%%
% Frequency BBQ***************************************************************
%
function [outputType, outputFreq] = FreqBiosemBinaryFilter(...
    thisTime, freqCandidates, recentHrFilt,  bioLimits, algoParam)


%
% pull stats from buffer.  Do the Langevin projection on sigma here so
% that we don't corrupt the original sigma in the buffer.
%
diffusion_constant = algoParam.diffusionConstant;
mu = recentHrFilt.mu;
if (recentHrFilt.stochModelType == 1),
    sigma = recentHrFilt.sigma + diffusion_constant*sqrt(thisTime - recentHrFilt.bufferTime(1)-0.5);
else
    sigma = recentHrFilt.sigma;
end

%
% Hypothesis testing.  This is set up in terms of probabilites, which
% unrolls the logic a bit.
%
Ncandidates = length(freqCandidates.freq);
optionType = zeros(2*Ncandidates+1,1);
optionWeight = zeros(2*Ncandidates+1,1);
optionFreq = zeros(2*Ncandidates+1,1);

%
% default case. This is switched depending on whether it is the
% first sample or not
%
optionWeight(1) = 0.1;


%
% pick out the first harmonic that is biophysically correct.  This assumes
% that the list of candidates has already been sorted coming into biosemHR
% in order of amplitude.
%
% include 92% confidence interval of lastHr
%
for k = 1:Ncandidates,
    if (freqCandidates.filtFlag(k) ~= 0),
        optionType(k+1) = 1;
        thisFreq = freqCandidates.freq(k);
        optionFreq(k+1) = thisFreq;
        
        if (recentHrFilt.stochModelType~=0),
            deltaHrFilt = (thisFreq*60 - mu) / mean([mu thisFreq*60]);
        else
            deltaHrFilt = 0;
        end
        
        optionFlag = (abs(60*thisFreq - mu)/sigma < algoParam.cvCutoff) && ...
            (deltaHrFilt < bioLimits.rateLimitUp) && ...
            (deltaHrFilt > -bioLimits.rateLimitDown) && ...
            (thisFreq*60 > bioLimits.minHR) && ...
            (thisFreq*60 < bioLimits.maxHR);
        
        if (optionFlag == 1),
            optionWeight(k+1) = 0.9 - 0.1*(k/Ncandidates);
        else
            optionWeight(k+1) = 0;
        end
    end
end

% look for candidates from the second harmonic
%
% include 92% confidence interval of lastHr
%
harmNum = 2;
for k = 1:Ncandidates,
    if (freqCandidates.filtFlag(k) ~= 0),
        
        optionType(Ncandidates+k+1) = 1;
        thisFreq = freqCandidates.freq(k);
        optionFreq(Ncandidates+k+1) = thisFreq/harmNum;
        
        if (recentHrFilt.stochModelType~=0),
            deltaHrFilt = (thisFreq *60/harmNum - mu) / mu;
        else
            deltaHrFilt = 0;
        end
        
        optionWeight(Ncandidates+k+1) =  (recentHrFilt.stochModelType == 1) && ...
            (abs(60*(thisFreq /harmNum) - mu)/sigma < algoParam.cvCutoff) && ...
            (deltaHrFilt < bioLimits.rateLimitUp) && ...
            (deltaHrFilt > -bioLimits.rateLimitDown) && ...
            (thisFreq /harmNum*60 > bioLimits.minHR) && ...
            (thisFreq /harmNum*60 < bioLimits.maxHR);
        
        %    if (optionFlag == 1),
        % turn off the 2nd harmonic
        if (0),
            optionWeight(Ncandidates+k+1) =  0.8 - 0.1*(k/Ncandidates);
        else
            optionWeight(Ncandidates+k+1) = 0;
        end
    end
end

[junk, ind] = max(optionWeight);

outputType = optionType(ind);
outputFreq = optionFreq(ind);

end


%%
% subfunction: Update State ***********************************************
%
function [outputInterBeatPeriod, recentHrFilt, this_biosemChangeRateHr] = update_state(...
    thisTime, strongestPeakFreq, outputType, outputFreq, recentHrFilt, algoParam, bioLimits)

this_biosemChangeRateHr = 0.05;   % only for compatibility with beats approach

% update the buffer with selected beat (forgetting old info), and change
% the stochastic model, if necessary.  Compute the output IBI
%
[outputInterBeatPeriod, recentHrFilt] = update_buffer(thisTime, strongestPeakFreq, ...
    outputType, outputFreq, recentHrFilt, algoParam);


% calculate recent stats for comparisons, for an empty buffer accept
% anything.
%
recentHrFilt = update_stats(recentHrFilt, algoParam, bioLimits);

end

%%
% subfunction: Update Buffer ***************************************
%
% Depending on the BBQ output and stochModelType, update the buffer accordingly.
%

function [outputBeatPeriod, recentHrFilt] = ...
    update_buffer(thisTime, strongestPeakFreq, outputType, outputFreq, ...
    recentHrFilt, algoParam)

%
% trim buffer
%
while (recentHrFilt.len > 1 && thisTime - recentHrFilt.bufferTime(end) > algoParam.windowSize),
    recentHrFilt.buffer = recentHrFilt.buffer(1:end-1);
    recentHrFilt.bufferTime = recentHrFilt.bufferTime(1:end-1);
    recentHrFilt.len = recentHrFilt.len - 1;
end

switch (outputType),
    case 0,
        if recentHrFilt.stochModelType == 0,
            outputBeatPeriod = 60/algoParam.initialHR;  % default value 
            return
        end
        
        delayTime = thisTime - recentHrFilt.bufferTime(1);
        if delayTime < algoParam.deltaT1
            recentHrFilt.stochModelType = 1;
            outputBeatPeriod = 60./recentHrFilt.mu;
            return
        end
        
        % at this point the delay is longer than deltaT1, so reset        
        if isnan(strongestPeakFreq)  % no selected freq.
            outputBeatPeriod = 60./mean(recentHrFilt.buffer);
            recentHrFilt.stochModelType = 2;
        elseif  delayTime > algoParam.deltaT2 % exceeded post-reset time
            outputBeatPeriod = 1/strongestPeakFreq;  % apply strongest peak
            recentHrFilt.buffer  = 60/outputBeatPeriod;
            recentHrFilt.bufferTime = thisTime;
            recentHrFilt.len = 1;
            recentHrFilt.stochModelType = 3; % post-reset            
        else % during the reset period
            outputBeatPeriod = 1/strongestPeakFreq;
            recentHrFilt.stochModelType = 2;
        end
        
    case 1,
        recentHrFilt.stochModelType = 1;
        outputBeatPeriod = 1 / outputFreq;
        
        % add the frequency to the buffer
        recentHrFilt.buffer = [60*outputFreq recentHrFilt.buffer];
        recentHrFilt.bufferTime = [thisTime recentHrFilt.bufferTime];
        recentHrFilt.len = recentHrFilt.len +1;
end

end


%%
% subfunction: Update Stats ***********************************************
%

function recentHrFilt = update_stats(recentHrFilt, algoParam, bioLimits)

switch (recentHrFilt.stochModelType),
    case (0), % initialization case, fixed, unbounded sigma
        recentHrFilt.sigma = (bioLimits.maxHR-bioLimits.minHR)*2;
        recentHrFilt.mu = algoParam.initialHR;
        
    case (1), % normal case, quasi-steady, robust gaussian statistics
        recentHrFilt.mu = mean(recentHrFilt.buffer);
        recentHrFilt.sigma = std(recentHrFilt.buffer);
        if (recentHrFilt.sigma < algoParam.minSigma),
            recentHrFilt.sigma = algoParam.minSigma;
        end
    case (2), % reset case, static, unbounded sigma
        recentHrFilt.sigma = (bioLimits.maxHR-bioLimits.minHR)*2;
    case (3),  % post-reset case, quasi-steady, unbounded sigma
        recentHrFilt.mu      = mean(recentHrFilt.buffer);
        recentHrFilt.sigma = (bioLimits.maxHR-bioLimits.minHR)*2;               
end

end


