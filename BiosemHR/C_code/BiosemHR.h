#ifndef BIOSEMHR
#define BIOSEMHR

/********************* Precompiler Directives **********************/
#define IBI_BUFFER_LEN ((int) ( 4 * 5 * (260 / 60) + 1))  // 5 seconds of the maximum heart rate (factor of 4 extra space for noise/artifacts)


/********************* Enumerations **********************/
typedef enum {
	RAW, // Use these to idicate which 
	QUALIFIED,
	NUM_HRSTATSTYPES
} HRStatsType_E;


/********************* Typedefs **********************/
typedef struct {
	int len;
	float IBI[IBI_BUFFER_LEN];
	float time[IBI_BUFFER_LEN];
	int enqueueIdx;
	int initCount;
} IBIqueue_t;

typedef struct {
	float minHR;
	float maxHR;
	float rateLimitDown;
	float rateLimitUp;
} bioLimits_t;

typedef struct {
	float mu; // of HR in beats per minute
	float sigma; // of HR in beats per minute
	float normalizedDelta; // unitless ratio
	bool isValid;
} HRStats_t;

typedef struct {
	bioLimits_t bioLimits;
	HRStats_t HRStats[NUM_HRSTATSTYPES];
} BioSemBeatQualifierParams_t;


/********************* Public Function Prototypes **********************/
bool initBiosemHR(BioSemBeatQualifierParams_t * pBSBQParams, IBIqueue_t * rawIBIBuffer, IBIqueue_t * qualifiedIBIBuffer, float * emafAlpha);
float calcBiosemHR(float candidateTime, float candidateIBI, BioSemBeatQualifierParams_t * bsbqParams, IBIqueue_t * rawIBIBuffer, IBIqueue_t * qualifiedIBIBuffer);
float smoothBiosemHR(float biosemHR, float smoothedBiosemHR, float emafAlpha);

#endif