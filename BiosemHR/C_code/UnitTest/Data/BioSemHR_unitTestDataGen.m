% Generate some IBIs for Unit Testing BioSemantic Heart Rate

% Normal - slow
% normalSlowHR = 60 + cumsum(randn(100,1));
normalSlowHR = 60*ones(100,1);
NormalSlowIBI = 60./normalSlowHR;
time_NormalSlowIBI = cumsum(NormalSlowIBI);

% Normal - fast
% normalFastHR = 160 + cumsum(3*randn(100,1));
normalFastHR = 160*ones(100,1);
NormalFastIBI = 60./normalFastHR;
time_NormalFastIBI = cumsum(NormalFastIBI);

% Missing Beat Detection - slow
missedBeatDetectSlowHR = normalSlowHR;
missedBeatDetectSlowHR(50) = normalSlowHR(50)/2;
missedBeatDetectSlowIBI = 60./missedBeatDetectSlowHR;
time_missedBeatDetectSlowIBI = cumsum(missedBeatDetectSlowIBI);


% Missing Beat Detection - fast
missedBeatDetectFastHR = normalFastHR;
missedBeatDetectFastHR(50) = normalFastHR(50)/2;
missedBeatDetectFastIBI = 60./missedBeatDetectFastHR;
time_missedBeatDetectFastIBI = cumsum(missedBeatDetectFastIBI);

% Extra Beat Detection - slow
extraBeatDetectSlowHR = normalSlowHR;
extraBeatDetectSlowHR(50) = normalSlowHR(50)*2;
extraBeatDetectSlowIBI = 60./extraBeatDetectSlowHR;
time_extraBeatDetectSlowIBI = cumsum(extraBeatDetectSlowIBI);

% Extra Beat Detection - fast
extraBeatDetectFastHR = normalFastHR;
extraBeatDetectFastHR(50) = normalFastHR(50)*2;
extraBeatDetectFastIBI = 60./extraBeatDetectFastHR;
time_extraBeatDetectFastIBI = cumsum(extraBeatDetectFastIBI);

figure
plot(time_NormalSlowIBI, normalSlowHR, '', time_NormalFastIBI,normalFastHR, '', ...
     time_missedBeatDetectSlowIBI, missedBeatDetectSlowHR, '', time_missedBeatDetectFastIBI, missedBeatDetectFastHR, '', ...
      time_extraBeatDetectSlowIBI, extraBeatDetectSlowHR, '', time_extraBeatDetectFastIBI, extraBeatDetectFastHR, '')

csvwrite('BioSemHR_UnitTestData_IBItime.csv', [time_NormalSlowIBI(:), time_NormalFastIBI(:), time_missedBeatDetectSlowIBI(:), time_missedBeatDetectFastIBI(:), time_extraBeatDetectSlowIBI(:), time_extraBeatDetectFastIBI(:)])
csvwrite('BioSemHR_UnitTestData_IBI.csv', [NormalSlowIBI(:), NormalFastIBI(:), missedBeatDetectSlowIBI(:), missedBeatDetectFastIBI(:), extraBeatDetectSlowIBI(:), extraBeatDetectFastIBI(:)])