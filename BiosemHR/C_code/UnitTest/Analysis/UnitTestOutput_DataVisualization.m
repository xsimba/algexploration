% clear all, close all, clc

for i = 1:8,
    
    ppgNum = i;
    
    inputFilename = ['../Data/10222014_biking-inp', num2str(ppgNum), '.csv'];
    refStandardFilename = ['../Data/10222014_biking-out', num2str(ppgNum), '.csv'];
    unitTestOutputFilename = ['../BiosemHR_UnitTest/UT_BioSemBeatQualifier_10222014_biking_inp', num2str(ppgNum), '_output.csv'];
    
    input = importBiosemHR_UnitTestInputData(inputFilename);
    refStandard = importBiosemHR_UnitTestRefStandardData(refStandardFilename);
    output = importBiosemHR_UnitTestOutputData(unitTestOutputFilename);
    
    figure (1);
    clf;
    x1(1) = subplot(211); plot(input(:,1), 60./input(:,2), 'g.', refStandard(:,1), 60./refStandard(:,2), 'b-x', output((output(:,2) > 30),1), output((output(:,2) > 30), 2), 'r-x', output(:,1), output(:,3), 'k') %, 1:length(ref), ref, 'm')
    set(gca, 'YLim', [30 240]);
    legend('Initial Raw HR', 'MATLAB Implementation', 'C code implementation', 'Smoothed C Code Implementation'), ylabel('Heart Rate (BPM)')
    
    %x1(2) = subplot(212); plot(refStandard(:,1), refStandard(:,3), 'b-x')
    x1(2) = subplot(212); plot(input(:,1), input(:,3), 'b-x')
    xlabel('Time (s)')
    linkaxes(x1, 'x')
    
    pause;
end
