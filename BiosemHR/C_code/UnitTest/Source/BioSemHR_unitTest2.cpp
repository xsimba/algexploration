/* Biosemantic Heart Rate Unit Test */

/********************* Includes **********************/
#define _CRT_SECURE_NO_DEPRECATE // Prevents MS Visual Studio from trying to use fopen_s() vs. fopen()
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <memory>
#include <vector>
#include <iostream>

#include "MemoryManager.h"
#include "packdef.h"
#include "BiosemanticBeatQualifier.h"
#include "BioSemHR_unitTestData.h"
#include "BioSemHR.h"
#include "TestUtil.hpp"

//class BiosemanticBeatQualifier;

/********************* Precompiler Directives **********************/


/********************* Enumerations **********************/


/********************* Typedefs **********************/


/********************* Function Prototypes **********************/
bool UT_BioSemBeatQualifier(const float IBIdata[][3], int rows, char * outputFilename);


/********************* Function Definitions **********************/
// @TODO: Add doxygen headers
void main()
{

	std::vector<float> CI_freq = ReadFile<float>("C:/code/alg_devel/BiosemHR/C_code/UnitTest/Source", "CI_freq6.bin");
	std::vector<float> ibi_freq = ReadFile<float>("C:/code/alg_devel/BiosemHR/C_code/UnitTest/Source", "ibi_freq6.bin");
	std::vector<float> amp_freq = ReadFile<float>("C:/code/alg_devel/BiosemHR/C_code/UnitTest/Source", "amp_freq6.bin");
	std::vector<float> num_pks = ReadFile<float>("C:/code/alg_devel/BiosemHR/C_code/UnitTest/Source", "num_pks6.bin");
	std::vector<float>::iterator i, j, k, l;

	algo::FreqCandidatesList_t * candidate = new algo::FreqCandidatesList_t;
	algo::FreqBBQParams_t * params = new algo::FreqBBQParams_t;
	algo::FreqIbiQueue_t * ibiBuffer = new algo::FreqIbiQueue_t;
	algo::initBiosemFreqHR(ibiBuffer, params);

/*	params->algoParams->cvCutoff = FREQ_CVCUTOFF;
	params->algoParams->deltaT1 = FREQ_DELTAT1;
	params->algoParams->deltaT2 = FREQ_DELTAT2;
	params->algoParams->minSigma = FREQ_MIN_SIGMA;
	params->algoParams->signalPowerCut = FFT_AMP_POWERCUT;
	params->algoParams->windowSize = FREQ_WINDOWSIZE;
	params->algoParams->peakLim = FREQ_CANDIDATE_LIM;

	params->bioLimits->maxHR = MAX_ALLOWED_HR;
	params->bioLimits->minHR = MIN_ALLOWED_HR;
	params->bioLimits->rateLimitDown = HEARTRATE_CHANGELIMIT_NEGATIVE_FREQ;
	params->bioLimits->rateLimitUp = HEARTRATE_CHANGELIMIT_POSITIVE_FREQ; */


    params->algoParams.cvCutoff = 2.5;
	params->algoParams.deltaT1 = 15.0;
	params->algoParams.deltaT2 = 25.0;
	params->algoParams.minSigma = 4.0;
	params->algoParams.signalPowerCut = 0.1;
	params->algoParams.windowSize = 5.0;
	params->algoParams.peakLim = 20;
	params->algoParams.diffConstant = 0.8;

	params->bioLimits.maxHR = 260;
	params->bioLimits.minHR = 30;
	params->bioLimits.rateLimitDown = -0.3;
	params->bioLimits.rateLimitUp = 0.3; 


	std::ofstream ofile("./outputdat6.csv");

	ofile << "index, time, muHR, sigmaHR, muHRprefilt" << std::endl;

	float freq[20];
	float amp[20];
	int filterFlag[20];
	int len;
	uint32_t time = 0;
	int index = 0;

	i = amp_freq.begin();
	j = ibi_freq.begin();
	k = CI_freq.begin();
	l = num_pks.begin();
	while (i < amp_freq.end()) {

		// unpack the next row and load into a candidate
		for (int rowNum = 0; rowNum < 20; rowNum++) {
			amp[rowNum] = *i++;
			freq[rowNum] = *j++;
			filterFlag[rowNum] = (int)*k++;
		}
		len = (int)*l++;

		// assign to a candidate
		candidate->strongestPeakFreq = 0.0f;
		candidate->len = len;
		for (int i = 0; i < 20; i++) // For every sample in the test 
		{
			candidate->amp[i] = amp[i];
			candidate->freq[i] = freq[i];
			candidate->filterFlag[i] = filterFlag[i];
		}
	
		float ibi = calcBiosemFreqHR(time, candidate, ibiBuffer, params);

//		ofile << "index, time, muHR, sigmaHR, instFreq" << std::endl;
		ofile << index++ << "," << time << "," << ibiBuffer->muHR
			<< "," << ibiBuffer->sigmaHR 
			<< "," << 60/ibi << std::endl;

		time += 16384;

	}

}



void mainOld()
{
    bool UT_Result[8];




	//UT_Result[0] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp1, (sizeof(Sep11BikeBeat_inp1) / sizeof(Sep11BikeBeat_inp1[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp1_output.csv");
	//UT_Result[1] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp2, (sizeof(Sep11BikeBeat_inp2) / sizeof(Sep11BikeBeat_inp2[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp2_output.csv");
	//UT_Result[2] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp3, (sizeof(Sep11BikeBeat_inp3) / sizeof(Sep11BikeBeat_inp3[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp3_output.csv");
	//UT_Result[3] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp4, (sizeof(Sep11BikeBeat_inp4) / sizeof(Sep11BikeBeat_inp4[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp4_output.csv");
	//UT_Result[4] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp5, (sizeof(Sep11BikeBeat_inp5) / sizeof(Sep11BikeBeat_inp5[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp5_output.csv");
	//UT_Result[5] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp6, (sizeof(Sep11BikeBeat_inp6) / sizeof(Sep11BikeBeat_inp6[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp6_output.csv");
	//UT_Result[6] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp7, (sizeof(Sep11BikeBeat_inp7) / sizeof(Sep11BikeBeat_inp7[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp7_output.csv");
	//UT_Result[7] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp8, (sizeof(Sep11BikeBeat_inp8) / sizeof(Sep11BikeBeat_inp8[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp8_output.csv");

	UT_Result[0] = UT_BioSemBeatQualifier(D10222014_biking_inp1, (sizeof(D10222014_biking_inp1) / sizeof(D10222014_biking_inp1[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp1_output.csv");
	UT_Result[1] = UT_BioSemBeatQualifier(D10222014_biking_inp2, (sizeof(D10222014_biking_inp2) / sizeof(D10222014_biking_inp2[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp2_output.csv");
	UT_Result[2] = UT_BioSemBeatQualifier(D10222014_biking_inp3, (sizeof(D10222014_biking_inp3) / sizeof(D10222014_biking_inp3[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp3_output.csv");
	UT_Result[3] = UT_BioSemBeatQualifier(D10222014_biking_inp4, (sizeof(D10222014_biking_inp4) / sizeof(D10222014_biking_inp4[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp4_output.csv");
	UT_Result[4] = UT_BioSemBeatQualifier(D10222014_biking_inp5, (sizeof(D10222014_biking_inp5) / sizeof(D10222014_biking_inp5[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp5_output.csv");
	UT_Result[5] = UT_BioSemBeatQualifier(D10222014_biking_inp6, (sizeof(D10222014_biking_inp6) / sizeof(D10222014_biking_inp6[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp6_output.csv");
	UT_Result[6] = UT_BioSemBeatQualifier(D10222014_biking_inp7, (sizeof(D10222014_biking_inp7) / sizeof(D10222014_biking_inp7[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp7_output.csv");
	UT_Result[7] = UT_BioSemBeatQualifier(D10222014_biking_inp8, (sizeof(D10222014_biking_inp8) / sizeof(D10222014_biking_inp8[0][0]) / 3), "UT_BioSemBeatQualifier_10222014_biking_inp8_output.csv");
    //UT_ResultReport(UT_Result);

	UT_BioSemBeatQualifier(testData, (sizeof(testData) / sizeof(testData[0][0]) / 3), "testData.csv");
	//getchar(); // Hold the cmd window open to view results - @TODO: remove following debug period
}


bool UT_BioSemBeatQualifier(const float IBIdata[][3], int rows, char * outputFilename)
{
	// declrations needed to UNIT TEST calcBiosemHR()
	int i;
	float  candidateTime, candidateIBI;
	FILE *fid;
	float biosemHR = NAN;
	float smoothedBiosemHR = NAN;
	
	std::shared_ptr<algo::Pack> pack;
	pack = algo::MemoryManager::AllocatePack(1);
	
	std::shared_ptr<algo::BiosemanticBeatQualifier> m_ppgBiosemHRQualifiers;
	m_ppgBiosemHRQualifiers = algo::BiosemanticBeatQualifier::createBiosemanticBeatQualifier();

	// Open a file to output unit test data to
	fid = fopen(outputFilename, "w+");
	if (fid == NULL)
	{
		printf("The data output file was not opened\n");
	}



	
	// Loop through every IBI sample in the Unit Test run call
	for (i = 0; i < rows; i++) // For every sample in the test 
    {
		candidateTime = IBIdata[i][0];
		candidateIBI = IBIdata[i][1];

		pack->timestamp = candidateTime * 32768;

		// Call the module/function under test
		m_ppgBiosemHRQualifiers->OnBeat(pack);

		// print results to output file
		char * formatStr = "%f  %f  %f\n";
		fprintf(fid, formatStr, candidateTime, m_ppgBiosemHRQualifiers->m_biosemHR, m_ppgBiosemHRQualifiers->m_smoothedBiosemHR);  //print to the file
	}

	fclose(fid);

    return 1; // @TODO: Make this return true/false based on passing the test
}









