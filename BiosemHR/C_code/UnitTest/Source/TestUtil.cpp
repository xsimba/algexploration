#include "TestUtil.hpp"

#include <fstream>
#include <stdexcept>

std::unique_ptr<std::ifstream> OpenFile(const std::string& filename)
{
  std::unique_ptr<std::ifstream> f(new std::ifstream(filename, std::ios::binary));
  if (!(*f)) throw std::runtime_error(std::string("could not open file: ") + filename);
  return std::move(f);
}

size_t FileSize(std::ifstream& f)
{
  auto pos = f.tellg();
  f.seekg(0, std::ios_base::end);
  std::streamsize size = size_t(f.tellg());
  f.seekg(pos, std::ios_base::beg);
  return (size_t)size;
}

TestFile::TestFile(const std::string& filename)
: m_filename(filename)
, m_file(OpenFile(filename))
, m_size(FileSize(*m_file))
{
}

void TestFile::Read(char * data, size_t size)
{
  if (size > 0)
  {
    m_file->read(data, size);
    if (m_file->gcount() != size) throw std::runtime_error(std::string("failed to read file: ") + m_filename);
  }
}
