#pragma once

#include <vector>
#include <string>
#include <algorithm>
#include <fstream>
#include <memory>

class TestFile
{
public:
  TestFile(const std::string& filename);
  
  void Read(char * data, size_t size);

  template <typename T> std::vector<T> Read()
  {
    std::vector<T> data(m_size / sizeof(T));
    Read((char*)data.data(), m_size);
    return data;
  }

private:
  std::string m_filename;
  std::unique_ptr<std::ifstream> m_file;
  size_t m_size;
};

template <typename T>
std::vector<T> ReadFile(const std::string& filename)
{
  TestFile file(filename);
  return file.Read<T>();
}

template <typename T>
std::vector<T> ReadFile(const char * path, const char * file)
{
  std::string p(path);
  return ReadFile<T>((p + '/' + file).c_str());
}

template <typename T>
bool Equals(const std::vector<T>& x, const std::vector<T>& y)
{
    if (x.size() != y.size())
        return false;
    for (size_t i = 0; i < x.size(); ++i)
    {
        if (x[i] != y[i])
            return false;
    }
    return true;
}

inline std::vector<float> Intersect(const std::vector<float>& x, const std::vector<float>& y)
{
    std::vector<float> z(x.size());
    auto it = std::set_intersection(x.begin(), x.end(), y.begin(), y.end(), z.begin());
    z.resize(it - z.begin());
    return z;
}
