/* Biosemantic Heart Rate Unit Test */

/********************* Includes **********************/
#define _CRT_SECURE_NO_DEPRECATE // Prevents MS Visual Studio from trying to use fopen_s() vs. fopen()
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "BioSemHR_unitTestData.h"
#include "BioSemHR.h"


/********************* Precompiler Directives **********************/


/********************* Enumerations **********************/


/********************* Typedefs **********************/


/********************* Function Prototypes **********************/
bool UT_BioSemBeatQualifier(const float IBIdata[][2], int rows, char * outputFilename);
bool initBiosemHR(BioSemBeatQualifierParams_t * pBSBQParams, IBIqueue_t * rawIBIBuffer, IBIqueue_t * qualifiedIBIBuffer, float * emafAlpha);


/********************* Function Definitions **********************/
// @TODO: Add doxygen headers
void main()
{
    bool UT_Result[8];

	UT_Result[0] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp1, (sizeof(Sep11BikeBeat_inp1) / sizeof(Sep11BikeBeat_inp1[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp1_output.csv");
	//UT_Result[1] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp2, (sizeof(Sep11BikeBeat_inp2) / sizeof(Sep11BikeBeat_inp2[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp2_output.csv");
	//UT_Result[2] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp3, (sizeof(Sep11BikeBeat_inp3) / sizeof(Sep11BikeBeat_inp3[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp3_output.csv");
	//UT_Result[3] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp4, (sizeof(Sep11BikeBeat_inp4) / sizeof(Sep11BikeBeat_inp4[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp4_output.csv");
	//UT_Result[4] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp5, (sizeof(Sep11BikeBeat_inp5) / sizeof(Sep11BikeBeat_inp5[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp5_output.csv");
	//UT_Result[5] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp6, (sizeof(Sep11BikeBeat_inp6) / sizeof(Sep11BikeBeat_inp6[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp6_output.csv");
	//UT_Result[6] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp7, (sizeof(Sep11BikeBeat_inp7) / sizeof(Sep11BikeBeat_inp7[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp7_output.csv");
	//UT_Result[7] = UT_BioSemBeatQualifier(Sep11BikeBeat_inp8, (sizeof(Sep11BikeBeat_inp8) / sizeof(Sep11BikeBeat_inp8[0][0]) / 2), "UT_BioSemBeatQualifier_Sep11BikeBeat_inp8_output.csv");

    //UT_ResultReport(UT_Result);

	//getchar(); // Hold the cmd window open to view results - @TODO: remove following debug period
}


bool UT_BioSemBeatQualifier(const float IBIdata[][2], int rows, char * outputFilename)
{
	// declrations needed to UNIT TEST calcBiosemHR()
	int i;
	float  candidateTime, candidateIBI;
	FILE *fid;
	float biosemHR = NAN;
	float smoothedBiosemHR = NAN;
	
	// declarations needed for the operation of calcBiosemHR()
	IBIqueue_t rawIBIBuffer, qualifiedIBIBuffer;
    BioSemBeatQualifierParams_t bsbqParams;
    float emafAlpha;
	
	// Open a file to output unit test data to
	fid = fopen(outputFilename, "w+");
	if (fid == NULL)
	{
		printf("The data output file was not opened\n");
	}

	// Initialize the data structs for storage of buffers and params needed for calcBiosemHR()
	initBiosemHR(&bsbqParams, &rawIBIBuffer, &qualifiedIBIBuffer, &emafAlpha);
	
	// Loop through every IBI sample in the Unit Test run call
	for (i = 0; i < rows; i++) // For every sample in the test 
    {
		candidateTime = IBIdata[i][0];
		candidateIBI = IBIdata[i][1];

		// Call the module/function under test
		biosemHR = calcBiosemHR(candidateTime, candidateIBI, &bsbqParams, &rawIBIBuffer, &qualifiedIBIBuffer);

		// Smooth the output of calcBiosemHR() with a 1st order exponential moving average filter
		smoothedBiosemHR = smoothBiosemHR(biosemHR, smoothedBiosemHR, emafAlpha);

		// print results to output file
		char * formatStr = "%f  %f  %f\n";
		fprintf(fid, formatStr, candidateTime, biosemHR, smoothedBiosemHR);  //print to the file
	}

	fclose(fid);

    return 1; // @TODO: Make this return true/false based on passing the test
}









