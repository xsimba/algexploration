/* Biosemantic Heart Rate */

/********************* Includes **********************/
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include <BiosemHR.h>

/********************* Precompiler Directives **********************/
#define PI ((float) 3.14159265358979323846)

// Biolimits for testing in Biosemantic Heart Rate
#define MIN_ALLOWED_HR 30 
#define MAX_ALLOWED_HR 260 
#define HEARTRATE_CHANGELIMIT_NEGATIVE  ((float) -0.3 )
#define HEARTRATE_CHANGELIMIT_POSITIVE  ((float) 0.3 )

#define ALGO_INIT_BEATS_REQUIRED 20

#define HR_FC 0.05
#define HR_FS 2

#define SECS_PER_MIN 60


/********************* Enumerations **********************/
typedef enum {
	CANDIDATE_UNCLASSIFIED,
	CANDIDATE_INITIALIZE,
	CANDIDATE_VALID,
	CANDIDATE_MISSEDBEAT,
	NUM_BIOSEM_IBITYPES
} BIOSEM_IBITYPE_E;

/********************* Typedefs **********************/


/********************* Private Function Prototypes **********************/
void initBioSemBeatQualifier(BioSemBeatQualifierParams_t * pBSBQParams);
void initIBIqueue(IBIqueue_t * pIBIqueue);
void updateIBIBuffer(IBIqueue_t * pIBIqueue, float time, float IBI);
void calc5secStats(IBIqueue_t * pIBIqueue, BioSemBeatQualifierParams_t * pBSBQParams, HRStatsType_E HRStatsType, float candidateTime, float candidateIBI);
BIOSEM_IBITYPE_E runBioSemBeatQualifier(float candidateIBI, IBIqueue_t * pQualifiedIBIqueue, BioSemBeatQualifierParams_t * pBSBQParams);
float calcHeartRate(IBIqueue_t * pQualifiedIBIqueue);
float order1_low_pass_filter_get_constant(float Fc, float Fs);
float order1_low_pass_filter_calculate(float yn1, float x, float alpha);


/********************* Public Function Definitions **********************/

bool initBiosemHR(BioSemBeatQualifierParams_t * pBSBQParams, IBIqueue_t * pRawIBIBuffer, IBIqueue_t * pQualifiedIBIBuffer, float * emafAlpha)
{		
	// Initialize the bioSemBeatQualifier_hist structure
	initBioSemBeatQualifier(pBSBQParams); // Initialize the BioSemBeatQualifierParams_t structure
	initIBIqueue(pRawIBIBuffer);
	initIBIqueue(pQualifiedIBIBuffer);

	*emafAlpha = order1_low_pass_filter_get_constant(HR_FC, HR_FS);

	return 1;  //@TODO: Make return error code if not successful or ill-advised cut-off/sampling frequency pair.
}


float calcBiosemHR(float candidateTime, float candidateIBI, BioSemBeatQualifierParams_t * pBsbqParams, IBIqueue_t * pRawIBIBuffer, IBIqueue_t * pQualifiedIBIBuffer)
{
	float candidateHR, newIBI_val;
	BIOSEM_IBITYPE_E ibiType;

	candidateHR = SECS_PER_MIN / candidateIBI;

	if ((candidateHR > pBsbqParams->bioLimits.minHR) && (candidateHR < pBsbqParams->bioLimits.maxHR))
	{
		// Extract the 5 seconds of IBIs that are relevent
		// Calculate the average, standard deviation, rate of change 
		calc5secStats(pRawIBIBuffer, pBsbqParams, RAW, candidateTime, candidateIBI); // Excludes candidate beat from stats
		calc5secStats(pQualifiedIBIBuffer, pBsbqParams, QUALIFIED, candidateTime, candidateIBI); // Includes candidate beat for stats

		// Manage fixed size buffer of time/ibi pairs to be pared for insertion in the algorithm exectution queue
		updateIBIBuffer(pRawIBIBuffer, candidateTime, candidateIBI);

		// Run the Biosemantic beat qualifier on it
		ibiType = runBioSemBeatQualifier(candidateIBI, pQualifiedIBIBuffer, pBsbqParams);

		// Update the qualifiedIBIBuffer only if ibiType was valid
		switch (ibiType)
		{
		case CANDIDATE_INITIALIZE:
			// No break - Intentional fall through 
		case CANDIDATE_VALID:
			newIBI_val = candidateIBI;
			break;

		case CANDIDATE_MISSEDBEAT:
			newIBI_val = (candidateIBI / 2);
			break;

		case CANDIDATE_UNCLASSIFIED:
			newIBI_val = NAN;
			break;

		default:
			newIBI_val = NAN;
			//assert();  // @TODO: what is our error return ? What do we fill the stats with ?
			break;
		}
	}
	else
	{
		ibiType = 0;
		newIBI_val = NAN;
	}

	// Update the IBI qualified buffer if the data is valid
	if (!isnan(newIBI_val))
	{
		updateIBIBuffer(pQualifiedIBIBuffer, candidateTime, newIBI_val);
	}

	return (SECS_PER_MIN / newIBI_val);
}


/********************* Private Function Definitions **********************/

// @TODO: Add doxygen headers
void initBioSemBeatQualifier(BioSemBeatQualifierParams_t * pBSBQParams)
{
	pBSBQParams->bioLimits.minHR = MIN_ALLOWED_HR;
	pBSBQParams->bioLimits.maxHR = MAX_ALLOWED_HR;
	pBSBQParams->bioLimits.rateLimitDown = HEARTRATE_CHANGELIMIT_NEGATIVE;
	pBSBQParams->bioLimits.rateLimitUp = HEARTRATE_CHANGELIMIT_POSITIVE;

	pBSBQParams->HRStats[RAW].mu = NAN;
	pBSBQParams->HRStats[RAW].sigma = NAN;
	pBSBQParams->HRStats[RAW].normalizedDelta = NAN;
	pBSBQParams->HRStats[RAW].isValid = false;

	pBSBQParams->HRStats[QUALIFIED].mu = NAN;
	pBSBQParams->HRStats[QUALIFIED].sigma = NAN;
	pBSBQParams->HRStats[QUALIFIED].normalizedDelta = NAN;
	pBSBQParams->HRStats[QUALIFIED].isValid = false;
}


void initIBIqueue(IBIqueue_t * pIBIqueue)
{
	int i;
	pIBIqueue->len = IBI_BUFFER_LEN;
	pIBIqueue->enqueueIdx = 0;
	pIBIqueue->initCount = 0;

	for (i = 0; i < pIBIqueue->len; i++) // For every sample in the test 
	{
		pIBIqueue->time[i] = 0;
		pIBIqueue->IBI[i] = 0;
	}
}


void updateIBIBuffer(IBIqueue_t * pIBIqueue, float time, float IBI)
{
	if (pIBIqueue->enqueueIdx >= pIBIqueue->len)  // Handle wrap in circular buffer
	{
		pIBIqueue->enqueueIdx = 0;
	}

	pIBIqueue->time[pIBIqueue->enqueueIdx] = time; // Update buffer values wih the new value
	pIBIqueue->IBI[pIBIqueue->enqueueIdx] = IBI;

	if (ALGO_INIT_BEATS_REQUIRED > pIBIqueue->initCount) // Increment the initialization counter
	{
		pIBIqueue->initCount++;
	}
	pIBIqueue->enqueueIdx++;	// Increment the buffer write index
}


// Take the mean and standard deviation of the samples in the buffer that are 
//    1) within the Heart Rate bioLimits and
//    2) within the 5 second window
void calc5secStats(IBIqueue_t * pIBIqueue, BioSemBeatQualifierParams_t * pBSBQParams, HRStatsType_E HRStatsType, float candidateTime, float candidateIBI)
{
	int denominatorCounter = 0; // counts number of samples in the running stats
	int i = 0;
	int readIdx = 0;
	float runningSum = 0;
	float runningSumOfSquares = 0;
	float candidateHR, HR;

	if (NULL == pIBIqueue)
	{
		//assert();  @TODO: what is our assert function?
	}

	// run over the buffer and compute sum and sum-of-squares.  Could be a running sum (todo)
	for (i = 0; i < pIBIqueue->len; i++)
	{
		if ((pIBIqueue->time[i] >(candidateTime - 5)) && (pIBIqueue->time[i] <= candidateTime))  // use values further in the past than 5 seconds @TODO: Magic Number : 5 second window size
		{
			if (pIBIqueue->IBI[i] != 0)
			{
				HR = SECS_PER_MIN / pIBIqueue->IBI[i];

				// Maintain running stats used for final stats output
				runningSum = runningSum + HR;
				runningSumOfSquares = runningSumOfSquares + HR * HR;

				denominatorCounter++; // Increment number of samples in the running stats
			}
		}
	}

	// calcuate mu and sigma from sum and sum-of-squares, and also compute fractional deviation
	HRStats_t *thisStat = &pBSBQParams->HRStats[HRStatsType];

	if (denominatorCounter > 0)  // samples were found, calculate and output stats
	{
		thisStat->mu = runningSum / denominatorCounter;
		thisStat->sigma = sqrt((runningSumOfSquares - runningSum*runningSum / denominatorCounter) / (denominatorCounter - 1));

		candidateHR = SECS_PER_MIN / candidateIBI;
		thisStat->normalizedDelta = (candidateHR - thisStat->mu) / ((candidateHR + thisStat->mu) / 2);

		pBSBQParams->HRStats[HRStatsType].isValid = true;
	}
	else // Handle case when there are no samples within the last 5 seconds
	{
		thisStat->mu = NAN;
		thisStat->sigma = NAN;
		thisStat->normalizedDelta = NAN;

		thisStat->isValid = false;
	}


}


BIOSEM_IBITYPE_E runBioSemBeatQualifier(float candidateIBI, IBIqueue_t * pQualifiedIBIqueue, BioSemBeatQualifierParams_t * pBSBQParams)
{
	bool initializationTimeDone = false;
	bool withinRateLimits_raw = true; // default to TRUE
	bool withinRateLimits_qualified = false;
	bool deviatesFromMean = false;
	bool steepNegativeRateChange = false;
	bool withinHRLimits = false;
	bool withinRateLimits_trial = false;
	bool deviatesFromMean_trial = false;
	bool withinHRLimits_trial = false;
	float candidateHR, candidateHR_trial, numStdsFromMean, trial_normalizedDelta, numStdsFromMean_trial;
	BIOSEM_IBITYPE_E beatType = CANDIDATE_UNCLASSIFIED; // default output to unclassified beat type

	candidateHR = SECS_PER_MIN / candidateIBI;
	candidateHR_trial = 2 * candidateHR;

	// Initialization Time
	initializationTimeDone = (pQualifiedIBIqueue->initCount >= ALGO_INIT_BEATS_REQUIRED);

	// Heart Rate Limits
	withinHRLimits = candidateHR < pBSBQParams->bioLimits.maxHR && candidateHR > pBSBQParams->bioLimits.minHR;

	withinHRLimits_trial = candidateHR_trial < pBSBQParams->bioLimits.maxHR && candidateHR_trial > pBSBQParams->bioLimits.minHR;

	// Rate Change Limit
	if (pBSBQParams->HRStats[RAW].isValid)
	{
		withinRateLimits_raw = ((pBSBQParams->HRStats[RAW].normalizedDelta < pBSBQParams->bioLimits.rateLimitUp) &&
			(pBSBQParams->HRStats[RAW].normalizedDelta > pBSBQParams->bioLimits.rateLimitDown));

		// withinRateLimits_raw defaults to TRUE allowing beat to enter buffer if normalizedDelta is NAN
	}

	if (pBSBQParams->HRStats[QUALIFIED].isValid)
	{
		// Deviation from the Mean	
		numStdsFromMean = fabsf(candidateHR - pBSBQParams->HRStats[QUALIFIED].mu) / pBSBQParams->HRStats[RAW].sigma;
		deviatesFromMean = numStdsFromMean > 1.5;

		numStdsFromMean_trial = fabsf(candidateHR_trial - pBSBQParams->HRStats[QUALIFIED].mu) / pBSBQParams->HRStats[QUALIFIED].sigma;
		deviatesFromMean_trial = numStdsFromMean_trial > 1.5;

		// deviatesFromMean and deviatesFromMean_trial default to FASLE (allowing beat to enter buffer) if sigma is NAN

		// Rate Change Limit
		steepNegativeRateChange = pBSBQParams->HRStats[QUALIFIED].normalizedDelta < pBSBQParams->bioLimits.rateLimitDown;

		withinRateLimits_qualified = ((pBSBQParams->HRStats[QUALIFIED].normalizedDelta < pBSBQParams->bioLimits.rateLimitUp) &&
			(pBSBQParams->HRStats[QUALIFIED].normalizedDelta > pBSBQParams->bioLimits.rateLimitDown));

		trial_normalizedDelta = (candidateHR_trial - pBSBQParams->HRStats[QUALIFIED].mu) / ((candidateHR_trial + pBSBQParams->HRStats[QUALIFIED].mu) / 2);
		withinRateLimits_trial = ((trial_normalizedDelta < pBSBQParams->bioLimits.rateLimitUp) &&
			(trial_normalizedDelta > pBSBQParams->bioLimits.rateLimitDown));

		// withinRateLimits_trial defaults to TRUE (allowing beat to enter buffer) if sigma is zero
	}


	// Classify the beat into the different types depending on the conditional inputs created above
	if ((!initializationTimeDone || !pBSBQParams->HRStats[RAW].isValid || !pBSBQParams->HRStats[QUALIFIED].isValid) &&
		withinRateLimits_raw) // If stat values are not valid (NAN as when the buffer is empty), we will fall into this case by default
	{
		beatType = CANDIDATE_INITIALIZE;
	}
	else if (withinRateLimits_qualified && !deviatesFromMean)
	{
		beatType = CANDIDATE_VALID;
	}
	else if (withinHRLimits_trial && steepNegativeRateChange && withinRateLimits_trial && !deviatesFromMean_trial)
	{
		beatType = CANDIDATE_MISSEDBEAT;
	}
	else
	{
		beatType = CANDIDATE_UNCLASSIFIED;
	}

	return beatType;
}


float calcHeartRate(IBIqueue_t * pQualifiedIBIqueue)
{
	float currentIBI, currentHR;

	// @TODO: What if that insertion was long ago?  That should likely not be reported.  There should be a timeout.

	currentIBI = pQualifiedIBIqueue->IBI[pQualifiedIBIqueue->enqueueIdx];
	currentHR = SECS_PER_MIN / currentIBI;

	return currentHR;
}


float smoothBiosemHR(float biosemHR, float filtHisoricValue, float emafAlpha)
{
	float smoothedBiosemHR = NAN;

	// LPF HR for smoothed output
	if (!isnan(biosemHR) && !isnan(filtHisoricValue))
	{
		smoothedBiosemHR = order1_low_pass_filter_calculate(filtHisoricValue, biosemHR, emafAlpha);
	}
	else if (!isnan(biosemHR))
	{
		smoothedBiosemHR = biosemHR;
	}
	else if (!isnan(filtHisoricValue))
	{
		smoothedBiosemHR = filtHisoricValue;
	}

	return smoothedBiosemHR;
}


/*!
@brief	 <order1_low_pass_filter_get_constant>
@details <Computes the alpha constant for a first-order filter based on sample
frequency and a given cutoff frequency.>

@param	 <float Fc> <cutoff frequency, in Hz>
@param	 <float Fs> <sample frequency, in Hz>
@return	 <parameter> <purpose/use of the return parameter>

@note
Fc = -Fs * ln(alpha) / (2*pi)
2*pi*Fc = -Fs * ln(alpha)
ln(alpha) = -(2*pi*Fc/Fs)
alpha = exp(-2*pi*Fc/Fs)>]
*/
float order1_low_pass_filter_get_constant(float Fc, float Fs)
{
	float return_val = NAN;

	// Divide-by-zero check
	if (0.0 != Fs)
	{
		return_val = exp(-2 * PI * Fc / Fs);  // -2.0 is derived in the note above which is documented above.
	}

	return return_val;
}


/*!
@brief	 <order1_low_pass_filter_calculate>
@details <Calculates a first order filter on incoming samples>

@param	 <float yn1>   <input, previous output>
@param	 <float x>     <input, current input>
@param	 <float alpha> <inupt, filter constant (alpha)>
@return	 <float>       <filter output>
*/
float order1_low_pass_filter_calculate(float yn1, float x, float alpha)
{
	float y;

	y = alpha * yn1 + (1 - alpha) * x;

	return y;
}