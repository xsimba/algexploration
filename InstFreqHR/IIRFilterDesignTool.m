%
% custom IIR bandpass filter design tool
%

%
% define butterworth filter characteristics
%
Fs = 128;
start_band = 30/60;
stop_band  = 220/60;

%designfilt('bandpassiir', 'FilterOrder', 10, 'HalfPowerFrequency1', 30/60, 'HalfPowerFrequency2', 150/60, 'SampleRate', 128);

Dbutter = fdesign.bandpass('N,F3dB1,F3dB2', 10, start_band, stop_band, Fs);
%Hbutter = design(Dbutter, 'butter', 'SystemObject', true);
Hbutter = design(Dbutter, 'butter');
SimLinkButter = dfilt.df2sos(Hbutter.sosMatrix, Hbutter.ScaleValues);


%
% compute group delay
%
freq_compensate = start_band:0.001:stop_band;
g = grpdelay(Hbutter, freq_compensate, Fs); 
g1 = max(g) - g;

%
% design compensator
%
hgd = fdesign.arbgrpdelay('N,B,F,Gd',10,1,freq_compensate,g1,Fs);
Hgd = design(hgd, 'iirlpnorm', 'MaxPoleRadius', 0.95);
SimLinkPhaseComp = dfilt.df2sos(Hgd.sosMatrix, Hgd.ScaleValues);



%%

