function data = motionMasking(data)
%Usage: motionMasking(data)
%
%when the motion flag is set to 1, ibi_hilbert corresponding to the motion
%flag is set to nan. In addition to setting the ibi_hilbert to nan, the
%next 2 ibi's in the hilbert (1 sec) is also set to nan. This takes care of
%the ripple effect. 


    
disp('Running motion masking');
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
for j = 1:8,
    trackNum = j;
    track = tracksPPG{j};
    eval(['ibi = squeeze(data.',track,'.ibi_hilbert);']);
    eval(['hrTime = data.',track,'.hilbert_timestamps;']);
    ind = find(data.motion_flag.signal == 1); %finds the indices when the motion flag is 1
    masked_ibi = ibi;
    masked_ibi(ind) = nan; %sets the corresponding indices in ibi_hilbert to nan

    %The following loop sets 1 sec of ibi_hilbert after the last "nan" to
    %nan
    new_masked_ibi = masked_ibi;
    i = 1;
    while (i< (length(masked_ibi)-2))
        if isnan(masked_ibi(i))
            if ~isnan(masked_ibi(i+1))
                new_masked_ibi(i+1) = nan;
                new_masked_ibi(i+2) = nan;
                i = i+2;
            end
        end
        i = i + 1;
    end
    if(isnan(masked_ibi(length(masked_ibi)-2)))
        new_masked_ibi(length(masked_ibi) - 1) = nan;
    end
    eval(['data.',track,'.ibi_hilbert = new_masked_ibi;']);
end
    %save (metricsFilename, 'data');
    %Run Biosemantic on the new masked ibi hilbert
    %AddBiosemTracks(c, sessionList,'masked_ibi_hilbert');
end
    

             