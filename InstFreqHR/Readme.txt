To call this model from MATLAB, you must have a Simulink license and have it installed. 

The following variables must be in the workspace:
	data - structure from the ReasearcherConsole with the standard PPG and timestamp data
	channel - a integer that describes the PPG channel (base one, not base zero) the data you want to analyze is in

Run the following command in MATLAB:
	options = simset('SrcWorkspace', 'current');
	[timestamps, ~, HR] = sim('InstFreqHR', [0, 600], options);
	InstFreqHR is the name of the Simulink Model, InstFreqHR.slx
	A vecotor of the [start end] time in seconds that you want the model to run over.  
If you run the model outside of the provided data set, you will get unpredictable output results.


Following the run, you will have the following variable in the workspace:
	timestamps - timestamp vector
	HR - Heart Rate vector through time matching the timestamps

Usage
AddInstFreqHRTracks(c, sessionList)

