function data = envelopeMaskInvestigation(data)


    
disp('Running envelope masking');
%
% tune chi^2 filter mask on ppg signal.
%
% the chi2 mask looks for a match between a slow and fast smoothing on
% the hilbert transform envelope.  Match is presumed to be a limit on the
% absolute value of the log ratio of the two filters.  We presume that when
% the fast and slow filters are 
%

%
% prepare inputs
% 
envelope_raw_thresh_up = 2;
envelope_raw_thresh_down = 2;
envelope_smooth_thresh_up = 0.8;
envelope_smooth_thresh_down = 0.8;


hilbEnvSig      = data.ppg.e.hil_ppg_env.signals.values;
hilbEnvTime     = data.ppg.e.hil_ppg_env.time;
if (0),
    biosemIbiTime   = data.ppg.e.biosemInterbeats(1,:);
    biosemIbi       = data.ppg.e.biosemInterbeats(2,:);
else
    biosemIbiTime   = (1:length(data.ppg.e.ibi_hilbert))/2;
    biosemIbi       = data.ppg.e.ibi_hilbert;
end

time            = data.timestamps;
xaccel          = data.acc.x.signal;
motionFlagTime    = data.motion_flag.time;
motionFlagSignal  = data.motion_flag.signal;



%
% 
[filters] = load('maskInvestigationFilters.mat');
% LPF3rdOrder_fc0p0667_fs128 = load('LPF3rdOrder_fc0p0667_fs128.mat');
% LPF3rdOrder_fc0p5_fs128 = load('LPF3rdOrder_fc0p5_fs128.mat');
fastFollow =  filter(filters.LPF3rdOrder_fc0p0667_fs128, hilbEnvSig);
slowFollow = filter(filters.LPF3rdOrder_fc0p5_fs128, hilbEnvSig);

mask2filt = log(fastFollow./hilbEnvSig).*log(slowFollow./hilbEnvSig);
mask2filt(mask2filt > envelope_raw_thresh_up) = 0;
mask2filt(mask2filt < envelope_raw_thresh_down) = 0;
mask2filt= abs(sign(mask2filt));

indRaw = log(fastFollow./(slowFollow+0.0001)) > envelope_smooth_thresh_up| ...
    log(fastFollow./(slowFollow+0.0001)) < -envelope_smooth_thresh_down | ... 
    mask2filt;
indPre = interp1(hilbEnvTime, single(indRaw), biosemIbiTime(2:end), 'nearest','extrap');

indMot = interp1(motionFlagTime, single(motionFlagSignal), biosemIbiTime(2:end), 'nearest','extrap');

ind = indPre;
for i = 3:length(ind),
    ind(i) = indPre(i-2) | indPre(i-1) | indPre(i);
end

ind1 = find(ind | indMot);
ind2 = find(~ind & ~indMot);
% ind1 = find(ind);
% ind2 = find(~ind);
%ind2 = find(~ind & ~indMot);

data.ppg.h.ibi_hilbert(ind1) = nan;
% data = RunBiosemTracksNoSave(data,'ibi_hilbert');

%
% plotting section
%

figure(1)
clf;
ax(1) = subplot(311); 
plot(biosemIbiTime(ind1), 60./biosemIbi(ind1), 'r.' )
hold on;
plot(biosemIbiTime(ind2), 60./biosemIbi(ind2), 'b.' )

ax(2) = subplot(312); 
plot(hilbEnvTime, hilbEnvSig, 'b', hilbEnvTime, fastFollow, 'g', hilbEnvTime, slowFollow, 'r')

ax(3) = subplot(313); 
plot(hilbEnvTime, log(fastFollow./slowFollow), 'r')
hold on;
plot(hilbEnvTime, mask2filt, 'b')

linkaxes(ax, 'x')

    %figure(2);
    %clf;
    %plot(time, xaccel);


