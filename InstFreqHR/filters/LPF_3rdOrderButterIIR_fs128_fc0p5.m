function y = LPF_3rdOrderButterIIR_fs128_fc0p5(x)
%LPF_3RDORDERBUTTERIIR_FS128_FC0.5 Filters input x and returns output y.

% MATLAB Code
% Generated by MATLAB(R) 8.3 and the DSP System Toolbox 8.6.
% Generated on: 16-Dec-2014 13:28:20

%#codegen

% To generate C/C++ code from this function use the codegen command.
% Type 'help codegen' for more information.

persistent Hd;

if isempty(Hd)
    
    % The following code was used to design the filter coefficients:
    %
    % N    = 3;    % Order
    % F3dB = 0.5;  % 3-dB Frequency
    % Fs   = 128;  % Sampling Frequency
    %
    % h = fdesign.lowpass('n,f3db', N, F3dB, Fs);
    %
    % Hd = design(h, 'butter', ...
    %     'SystemObject', true);
    
    Hd = dsp.BiquadFilter( ...
        'Structure', 'Direct form II', ...
        'SOSMatrix', [1 2 1 1 -1.97516119624904 0.975756257094535; 1 1 0 1 ...
        -0.975752649932377 0], ...
        'ScaleValues', [0.000148765211373601; 0.0121236750338117; 1]);
end

y = step(Hd,x);

clear Hd;


