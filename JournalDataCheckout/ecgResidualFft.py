# -*- coding: utf-8 -*-
"""
Created on Fri May 23 11:37:56 2014

load the raw and sample rate converted ECG signals and plot an fft
of the residual (log amplitude and phase)

@author: asif.khalak
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import simbaJournal as simbaJ

# journal outputs in microseconds
Fs = 128.0
EcgRawSampRate = 512.0
FreqClock = 1.0e6

def gettimes(vector, timerange, buffer=0):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0] + buffer, vector[:,0] < timerange[1]- buffer))
    return ind

def computeFft(x, samp_rate):
    fx = np.fft.fft(x)
    L = np.round(fx.size/2)
    fNyq = samp_rate/2
    
    spectrumFreq  = np.r_[0:L]/L*fNyq
    spectrumAmp   = np.log(np.abs(fx[0:L]))
#    spectrumPhase = np.unwrap(np.angle(fx[0:L]))

    return (spectrumFreq, spectrumAmp, spectrumPhase)

def plotFft(freq, amp, phase, dataType = 'unknown', dataTitle = 'unlabeled', startFigNo=2):
    plt.figure(startFigNo)
    plt.clf()
    plt.plot(freq, amp)
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Log Magnitude of residual FFT')
    plt.title('Amplitude of '+ dataType +' in ' + dataTitle)
    
    plt.figure(startFigNo+1)
    plt.clf()
    plt.plot(freq, phase)
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Phase of residual FFT (radians)')
    plt.title('Phase of '+ dataType +' in ' + dataTitle)


#
# get the data, bring in the beats for the heck of it.
#
dataTitle = 'can5'
dataDir = r'c:\code\simbase\canned\can8'
# dataDir = r'/home/mkedwards/journal-chair'
#dataDir = r'/home/mkedwards/simbase/canned/can6'

journal = simbaJ.getJournalEcg(dataDir)

rawecg = journal['rawecg']
ecg = journal['ecg']
ecgBeats = journal['ecgBeats']

if (rawecg == [] or ecg == []):
    print 'data not available'
    assert(False)

#
# deterimine the subset of data to be analyzed 
#
times = (0,1000)
indE = gettimes(ecg, times)
indERaw = gettimes(rawecg, times)
indEB = gettimes(ecgBeats, times)



#
# find the beat amplitude on the SRC data
#
ecgBeatTime = ecgBeats[indEB,0]
ecgBeatAmp  = np.interp(ecgBeatTime, ecg[indE,0], ecg[indE,1])
ecgBeatPlot = plt.plot(ecgBeatTime, ecgBeatAmp, 'kx')


#
# plot raw and SRC data on the same axes to compare
#
plt.figure(1)
plt.clf()
#plt.subplot(211)
plt.plot(rawecg[indERaw,0]+0.000, rawecg[indERaw,1], 'b')
plt.hold(True)
srvplot = plt.plot(ecg[indE,0], ecg[indE,1], 'r')
plt.setp(srvplot, 'linewidth', 2.0)
plt.setp(ecgBeatPlot, 'markeredgewidth', 2.0)
plt.ylabel('ECG')
plt.xlabel('Acquisition time [s]')
plt.title('Data from '+dataTitle+', Red=SRC; Blue=Raw, Raw offset 0.000s')


#
# interpolate/upsample ecg to raw rate, and compute FFT
#
interpECGSRC = np.interp(rawecg[indERaw,0], ecg[indE,0], ecg[indE,1]) 
resid = rawecg[indERaw,1]-interpECGSRC
(spectrumFreq, spectrumAmp, spectrumPhase) = computeFft(resid, EcgRawSampRate)

#
# plot the FFT
#
dataType = 'ECG Residual (SRC-Raw)'
plotFft(spectrumFreq, spectrumAmp, spectrumPhase, dataType, dataTitle, startFigNo=2)


