# -*- coding: utf-8 -*-
"""
journalCheckout.py

Created on Wed May 21 09:24:28 2014

@author: asif.khalak

Modified by: Navya Davuluri

Modified on Mon June 23 
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import simbaJournal as simbaJ
import os
from scipy.io import savemat

do_raw = False
do_vis = False
do_ecg = False
do_ppg = True


#
times = (0,30000)


def gettimes(vector, timerange, buffer=0):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0] + buffer, vector[:,0] < timerange[1]- buffer))
    return ind

#%%    

#
# get the pre sample rate converted data
#
# raw block sizes are mismatched from sample rate...
dataFile = r'./ppg_1test1.csv'

# journal outputs in microseconds
Fs = 128.0
FreqClock = 1.0e6
minTime = 1e10
sampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}

journal = simbaJ.getJournal(dataFile)

plt.plot(journal['time'], journal['Column2'] - np.mean(journal['Column2']))
plt.hold(True)
plt.plot(journal['time'], journal['Column1'] - np.mean(journal['Column1']), 'r')

#matFileName= dataFile.rpartition(os.sep)[2] + '.mat'
#
#times = (np.max([times[0], ppg1[0,0], ppg2[0,0], ecg[0,0]]), \
#         np.min([times[1], ppg1[-1,0], ppg2[-1,0], ecg[-1,0]]))
#
#ppg1 = ppg1[gettimes(ppg1, times, buffer=0.1),:]
#ppg2 = ppg2[gettimes(ppg2, times, buffer=0.1),:]
#ecg  = ecg[gettimes(ecg, times, buffer=0.1),:]
#
##
## pack with data interpolated to ECG timestamps
##
#data = { \
#    'ECG' : ecg[:,1], \
#    'PPG1R' : np.interp(ecg[:,0],ppg1[:,0],ppg1[:,1]), \
#    'PPG1G' : np.interp(ecg[:,0],ppg1[:,0],ppg1[:,2]), \
#    'PPG2R' : np.interp(ecg[:,0],ppg2[:,0],ppg2[:,1]), \
#    'PPG2G' : np.interp(ecg[:,0],ppg2[:,0],ppg2[:,2]), \
#    'simbaEcgBeats' : ecgBeats - times[0], \
#    'simbaPpgBeats' : ppgBeats - times[0], \
#    'simbaPat'      : pat \
#    }
#
#savemat(matFileName, data, oned_as = 'column')

plt.show(block=False)




