# -*- coding: utf-8 -*-
"""
journalCheckout.py

Created on Wed May 21 09:24:28 2014

@author: asif.khalak

Modified by: Navya Davuluri

Modified on Mon June 23 
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import simbaJournal as simbaJ
import os
from scipy.io import savemat

do_raw = False
do_vis = False
do_ecg = True
do_ppg = True


#
times = (0,30000)


def gettimes(vector, timerange, buffer=0):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0] + buffer, vector[:,0] < timerange[1]- buffer))
    return ind

#%%    

#
# get the pre sample rate converted data
#
# raw block sizes are mismatched from sample rate...
#dataDir = r'/home/mkedwards/yosi'
dataDir = r'c:\Code\simbase\canned\can5'
# dataDir = r'/home/mkedwards/journal-chair'
#dataDir = r'/home/mkedwards/simbase/canned/can6'

# journal outputs in microseconds
Fs = 128.0
FreqClock = 1.0e6
minTime = 1e10
sampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}
try:
    journal.keys()
    assert (True)
except:
    journal = simbaJ.getJournalAll(dataDir, freq=sampleFreq)

# unpack the journal
ppg = journal['ppg1']
rawppg = journal['rawppg1']
ppgFilt = journal['ppgFilt1']
ppgVis = journal['ppgVis1']
rawecg = journal['rawecg']
ecg = journal['ecg']
ecgVis = journal['ecgVis']
ecgBeats = journal['ecgBeats']
ppgBeats = journal['ppgBeats']
ppg1 = journal['ppg1']
ppg2 = journal['ppg2']
pat = journal['pat']




#%%

#
# plot raw and SRC data on the same axes to compare
#

if do_ecg:
    plt.figure(1)
    plt.clf()
    #plt.subplot(211)
    indE = gettimes(ecg, times)
    indEB = gettimes(ecgBeats, times, 0.5)
    if do_raw:
        indERaw = gettimes(rawecg, times)
        plt.plot(rawecg[indERaw,0]+0.000, rawecg[indERaw,1], 'b')
        plt.hold(True)
    if do_vis:
        indEVis = gettimes(ecgVis, times)
        plt.plot(ecgVis[indEVis,0]+0.000, ecgVis[indEVis,1], 'b')
        plt.hold(True)
    srvplot = plt.plot(ecg[indE,0], ecg[indE,1], 'r')
    plt.hold(True)
    plt.setp(srvplot, 'linewidth', 2.0)
    ecgBeatTime = ecgBeats[indEB,0]
    ecgBeatAmp  = np.interp(ecgBeatTime, ecg[indE,0], ecg[indE,1])
    ecgBeatPlot = plt.plot(ecgBeatTime, ecgBeatAmp, 'kx')
    plt.setp(ecgBeatPlot, 'markeredgewidth', 2.0)
    plt.ylabel('ECG')
    plt.xlabel('Acquisition time [s]')
    plt.title('Data, Red=SRC; Blue=Raw, Raw offset 0.000s, Black X = Peak')
    plt.hold(False)

    plt.figure(2)
    plt.clf()
    plt.hist(ecg[:,1],500)
    plt.ylabel('#Frequency')
    plt.xlabel('ECG')
    plt.title('Histogram of ECG Data')
    


if do_ppg:
    plt.figure(3)
    plt.clf()
    #plt.subplot(212)
    indP = gettimes(ppg, times)
    indPB = gettimes(ppgBeats, times)
    if do_raw:
        indPRaw = gettimes(rawppg, times)
        indPFilt = gettimes(ppgFilt, times)
        plt.plot(rawppg[indPRaw,0]-0.000, rawppg[indPRaw,2]-np.median(rawppg[indPRaw,2]), 'b')
        plt.hold(True)
        plt.plot(ppgFilt[indPFilt,0]-0.000, ppgFilt[indPFilt,2]-np.median(ppgFilt[indPFilt,2]), 'r')
        plt.hold(True)
    if do_vis:
        indPVis = gettimes(ppgVis, times)
        plt.plot(ppgVis[indPVis,0]-0.000, ppgVis[indPVis,2]-np.median(ppgVis[indPVis,2]), 'b')
        plt.hold(True)
    srvplot = plt.plot(ppg[indP,0], ppg[indP,2]-np.median(ppg[indP,2]), 'g')
    plt.hold(True)
    plt.setp(srvplot, 'linewidth', 2.0)
    ppgBeatTime = ppgBeats[indPB,0]
    ppgBeatAmp  = np.interp(ppgBeatTime, ppg[indP,0], ppg[indP,2])
    ppgBeatPlot = plt.plot(ppgBeatTime, ppgBeatAmp-np.median(ppg[indP,2]), 'kx')
    plt.setp(ppgBeatPlot, 'markeredgewidth', 2.0)
    if do_raw:
        ppgBeatFiltAmp  = np.interp(ppgBeatTime, ppgFilt[indPFilt,0]-0.000, ppgFilt[indPFilt,2])
        ppgBeatFiltPlot = plt.plot(ppgBeatTime, ppgBeatFiltAmp-np.median(ppgFilt[indPFilt,2]), 'x')
        plt.setp(ppgBeatFiltPlot, 'markeredgewidth', 2.0)
    if do_vis:
        ppgBeatVisAmp  = np.interp(ppgBeatTime, ppgVis[indPVis,0]-0.000, ppgVis[indPVis,2])
        ppgBeatVisPlot = plt.plot(ppgBeatTime, ppgBeatVisAmp-np.median(ppgVis[indPVis,2]), 'x')
        plt.setp(ppgBeatVisPlot, 'markeredgewidth', 2.0)
    plt.ylabel('PPG-1')
    plt.xlabel('Acquisition time [s]')
    plt.title('Data, Green=SRC; Red=Filt; Blue=Raw, Raw offset -0.000s, Black X = Peak')

    plt.figure(4)
    plt.clf()
    plt.hist(ppg[:,1],500)
    plt.ylabel('#Frequency')
    plt.xlabel('PPG-1')
    plt.title('Histogram of PPG Data')
    




matFileName= dataDir + os.sep + dataDir.rpartition(os.sep)[2] + '.mat'

times = (np.max([times[0], ppg1[0,0], ppg2[0,0], ecg[0,0]]), \
         np.min([times[1], ppg1[-1,0], ppg2[-1,0], ecg[-1,0]]))

ppg1 = ppg1[gettimes(ppg1, times, buffer=0.1),:]
ppg2 = ppg2[gettimes(ppg2, times, buffer=0.1),:]
ecg  = ecg[gettimes(ecg, times, buffer=0.1),:]

#
# pack with data interpolated to ECG timestamps
#
data = { \
    'ECG' : ecg[:,1], \
    'PPG1R' : np.interp(ecg[:,0],ppg1[:,0],ppg1[:,1]), \
    'PPG1G' : np.interp(ecg[:,0],ppg1[:,0],ppg1[:,2]), \
    'PPG2R' : np.interp(ecg[:,0],ppg2[:,0],ppg2[:,1]), \
    'PPG2G' : np.interp(ecg[:,0],ppg2[:,0],ppg2[:,2]), \
    'simbaEcgBeats' : ecgBeats - times[0], \
    'simbaPpgBeats' : ppgBeats - times[0], \
    'simbaPat'      : pat \
    }

savemat(matFileName, data, oned_as = 'column')

plt.show(block=False)




