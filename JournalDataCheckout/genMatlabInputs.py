# -*- coding: utf-8 -*-
"""
Created on Sat May 24 16:23:20 2014

@author: asif.khalak
"""

import numpy as np
import matplotlib.mlab as mlab
import simbaJournal as simbaJ
import os
from scipy.io import savemat 



def gettimes(vector, timerange, buffer=0):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0] + buffer, vector[:,0] < timerange[1]- buffer))
    return ind

#%%    

#
# get the pre sample rate converted data
#
# raw block sizes are mismatched from sample rate...
#dataDir = r'/home/mkedwards/yosi'
dataDir = r'c:\code\simbase\canned\dryrun'
# dataDir = r'/home/mkedwards/journal-chair'
#dataDir = r'/home/mkedwards/simbase/canned/can6'

matFileName= dataDir + os.sep + dataDir.rpartition(os.sep)[2] + '.mat'


# journal outputs in microseconds
sampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}
journal = simbaJ.getJournalAll(dataDir, freq=sampleFreq)

# unpack the journal
ppg1 = journal['ppg1']
ppg2 = journal['ppg2']
ecg = journal['ecg']
ecgBeats = journal['ecgBeats']
ppgBeats = journal['ppgBeats']
pat = journal['pat']

#
# Determine a good time range for all of the data
#
times = (0,3000)
times = (np.max([times[0], ppg1[0,0], ppg2[0,0], ecg[0,0]]), \
         np.min([times[1], ppg1[-1,0], ppg2[-1,0], ecg[-1,0]]))

ppg1 = ppg1[gettimes(ppg1, times, buffer=0.1),:]
ppg2 = ppg2[gettimes(ppg2, times, buffer=0.1),:]
ecg  = ecg[gettimes(ecg, times, buffer=0.1),:]

#
# pack with data interpolated to ECG timestamps
#
data = { \
    'ECG' : ecg[:,1], \
    'PPG1R' : np.interp(ecg[:,0],ppg1[:,0],ppg1[:,1]), \
    'PPG1G' : np.interp(ecg[:,0],ppg1[:,0],ppg1[:,2]), \
    'PPG2R' : np.interp(ecg[:,0],ppg2[:,0],ppg2[:,1]), \
    'PPG2G' : np.interp(ecg[:,0],ppg2[:,0],ppg2[:,2]), \
    'simbaEcgBeats' : ecgBeats - times[0], \
    'simbaPpgBeats' : ppgBeats - times[0], \
    'simbaPat'      : pat \
    }

savemat(matFileName, data, oned_as = 'column')