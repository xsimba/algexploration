# -*- coding: utf-8 -*-
"""
journalCheckout.py

Created on Wed May 21 09:24:28 2014

@author: asif.khalak
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import simbaJournal as simbaJ

do_raw = False
do_vis = False
do_ecg = True
do_ppg = True
do_pat = True

#
times = (0,3000)


def gettimes(vector, timerange, buffer=0):
    ind = mlab.find(np.logical_and(vector[:,0] > timerange[0] + buffer, vector[:,0] < timerange[1]- buffer))
    return ind

#%%    

#
# get the pre sample rate converted data
#
# raw block sizes are mismatched from sample rate...
#dataDir = r'/home/mkedwards/yosi'
dataDir = r'c:\Code\simbase\canned\can5'
# dataDir = r'/home/mkedwards/journal-chair'
#dataDir = r'/home/mkedwards/simbase/canned/can6'

# journal outputs in microseconds
Fs = 128.0
FreqClock = 1.0e6
minTime = 1e10
sampleFreq = {'ecgRaw' : 512.0, 'ppgRaw' : 50.0, \
                     'clock': 1.0e6, 'default' : 128.0}
try:
    journal.keys()
    assert (True)
except:
    journal = simbaJ.getJournalAll(dataDir, freq=sampleFreq)

# unpack the journal
ppg = journal['ppg1']
rawppg = journal['rawppg1']
ppgFilt = journal['ppgFilt1']
ppgVis = journal['ppgVis1']
rawecg = journal['rawecg']
ecg = journal['ecg']
ecgVis = journal['ecgVis']
ecgBeats = journal['ecgBeats']
ppgBeats = journal['ppgBeats']
pat = journal['pat']


#%%


if do_raw:
    #
    # compare raw clocks
    #
    plt.figure(1)
    plt.clf()
    plt.subplot(211)
    if do_ecg:
        plt.plot(np.r_[0:rawecg[:,0].size]/512.0, rawecg[:,0], 'r.')
        if do_ppg:
            plt.hold(True)
    if do_ppg:
        plt.plot(np.r_[0:rawppg[:,0].size]/50.0, rawppg[:,0], 'g.')
    plt.ylabel('raw time from start [s]');
    plt.xlabel('nominal time from start [s]');
    if do_ecg and do_ppg:
        plt.legend(('ECG', 'PPG'), loc=2)
    elif do_ecg:
        plt.legend(('ECG', ), loc=2)
    else:
        plt.legend(('PPG', ), loc=2)
    plt.title('Distinction in raw timestamps, Can5')
    plt.subplot(212)


#
# plot raw and SRC data on the same axes to compare
#

if do_ecg:
    plt.figure(5)
    plt.clf()
    #plt.subplot(211)
    indE = gettimes(ecg, times)
    indEB = gettimes(ecgBeats, times, 0.5)
    if do_raw:
        indERaw = gettimes(rawecg, times)
        plt.plot(rawecg[indERaw,0]+0.000, rawecg[indERaw,1], 'b')
        plt.hold(True)
    if do_vis:
        indEVis = gettimes(ecgVis, times)
        plt.plot(ecgVis[indEVis,0]+0.000, ecgVis[indEVis,1], 'b')
        plt.hold(True)
    srvplot = plt.plot(ecg[indE,0], ecg[indE,1], 'r')
    plt.setp(srvplot, 'linewidth', 2.0)
    ecgBeatTime = ecgBeats[indEB,0]
    ecgBeatAmp  = np.interp(ecgBeatTime, ecg[indE,0], ecg[indE,1])
    ecgBeatPlot = plt.plot(ecgBeatTime, ecgBeatAmp, 'kx')
    plt.setp(ecgBeatPlot, 'markeredgewidth', 2.0)
    plt.ylabel('ECG')
    plt.xlabel('Acquisition time [s]')
    plt.title('Data from Can5, Red=SRC; Blue=Raw, Raw offset 0.000s')

if do_ppg:
    plt.figure(6)
    plt.clf()
    #plt.subplot(212)
    indP = gettimes(ppg, times)
    indPB = gettimes(ppgBeats, times)
    if do_raw:
        indPRaw = gettimes(rawppg, times)
        indPFilt = gettimes(ppgFilt, times)
        plt.plot(rawppg[indPRaw,0]-0.000, rawppg[indPRaw,2]-np.median(rawppg[indPRaw,2]), 'b')
        plt.hold(True)
        plt.plot(ppgFilt[indPFilt,0]-0.000, ppgFilt[indPFilt,2]-np.median(ppgFilt[indPFilt,2]), 'r')
        plt.hold(True)
    if do_vis:
        indPVis = gettimes(ppgVis, times)
        plt.plot(ppgVis[indPVis,0]-0.000, ppgVis[indPVis,2]-np.median(ppgVis[indPVis,2]), 'b')
        plt.hold(True)
    srvplot = plt.plot(ppg[indP,0], ppg[indP,2]-np.median(ppg[indP,2]), 'g')
    plt.setp(srvplot, 'linewidth', 2.0)
    ppgBeatTime = ppgBeats[indPB,0]
    ppgBeatAmp  = np.interp(ppgBeatTime, ppg[indP,0], ppg[indP,2])
    ppgBeatPlot = plt.plot(ppgBeatTime, ppgBeatAmp-np.median(ppg[indP,2]), 'x')
    plt.setp(ppgBeatPlot, 'markeredgewidth', 2.0)
    if do_raw:
        ppgBeatFiltAmp  = np.interp(ppgBeatTime, ppgFilt[indPFilt,0]-0.000, ppgFilt[indPFilt,2])
        ppgBeatFiltPlot = plt.plot(ppgBeatTime, ppgBeatFiltAmp-np.median(ppgFilt[indPFilt,2]), 'x')
        plt.setp(ppgBeatFiltPlot, 'markeredgewidth', 2.0)
    if do_vis:
        ppgBeatVisAmp  = np.interp(ppgBeatTime, ppgVis[indPVis,0]-0.000, ppgVis[indPVis,2])
        ppgBeatVisPlot = plt.plot(ppgBeatTime, ppgBeatVisAmp-np.median(ppgVis[indPVis,2]), 'x')
        plt.setp(ppgBeatVisPlot, 'markeredgewidth', 2.0)
    plt.ylabel('PPG-1')
    plt.xlabel('Acquisition time [s]')
    plt.title('Data from Can5, Green=SRC; Red=Filt; Blue=Raw, Raw offset -0.000s')


#%%

#
# compare the clocks
#
if do_ecg and do_ppg:
    plt.figure(2)
    plt.clf()
    plt.plot(ecg[:,0], 'r.')
    plt.hold(True)
    plt.plot(ppg[:,0], 'g.')
    plt.xlabel('index');
    plt.ylabel('time [s]');
    plt.legend(('ECG', 'PPG'), loc=2)
    plt.title('Distinction in timestamps, Can5')

    plt.figure(3)
    plt.clf()
    plt.plot(ecgBeats[:,0],np.ones(ecgBeats[:,0].size), 'rx')
    plt.hold(True)
    plt.plot(ppgBeats[:,0],np.ones(ppgBeats[:,0].size),  'gx')
    if do_pat:
        plt.plot(pat[:,0],pat[:,1],  'bx')
    plt.xlabel('index');
    plt.ylabel('time [s]');
    plt.legend(('ECG', 'PPG'), loc=2)
    plt.title('Distinction in timestamps, Can5')
    
#%%

plt.figure(4)
plt.clf()
if do_ecg:
    ecgPlot = plt.subplot(311)
    indE = gettimes(ecg, times)
    plt.plot(ecg[indE,0], ecg[indE,1], 'r')
    plt.hold(True)
    indEB = gettimes(ecgBeats, times)
    ecgBeatTime = ecgBeats[indEB,0]
    ecgBeatAmp  = np.interp(ecgBeatTime, ecg[indE,0], ecg[indE,1])
    ecgBeatPlot = plt.plot(ecgBeatTime, ecgBeatAmp, 'kx')
    plt.setp(ecgBeatPlot, 'markeredgewidth', 2.0)
    plt.ylabel('ECG')
    plt.title('Data from Can5')

if do_ppg:
    plt.subplot(312, sharex=ecgPlot)
    indP = gettimes(ppg, times)
    plt.plot(ppg[indP,0], ppg[indP,2], 'r')
    plt.hold(True)
    indPB = gettimes(ppgBeats, times)
    ppgBeatTime = ppgBeats[indPB,0]
    ppgBeatAmp  = np.interp(ppgBeatTime, ppg[indP,0], ppg[indP,2])
    ppgBeatPlot = plt.plot(ppgBeatTime, ppgBeatAmp, 'x')
    plt.setp(ppgBeatPlot, 'markeredgewidth', 2.0)
    plt.ylabel('PPG-1, Green')

if do_pat:
    plt.subplot(313, sharex=ecgPlot)
    ind = gettimes(pat, times)
    patPlot = plt.plot(pat[ind,0], pat[ind,1], 'r.')
    plt.setp(patPlot, 'markeredgewidth', 2.0)
    plt.hold(True)
    plt.ylabel('PAT [s]')
    plt.xlabel('time')

plt.show()


