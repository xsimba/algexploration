function plotname = generate_boxplots( data,groups,params )
    figure('units','normalized','outerposition',[0 0 1 1])
    boxplot(data,groups);
    grid on;
    xlabel(params.xlabel)
    ylabel(params.ylabel);
    xlim([0 min(20,max(groups))+.5])
    ylim([0 5])
    plotname = [params.plotname,params.name_suffix];
    saveas(gcf,plotname,'fig');
    saveas(gcf,plotname,'png');
    close all
    
end

