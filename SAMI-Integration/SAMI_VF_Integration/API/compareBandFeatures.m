function metrics = compareBandFeatures( computedOutput, signal )
%compareBandFeatures compares the output of doTest with simband features
%   Usage Example: 
%       output = doTest(alg,signal,Cfg);
%       metrics = compareBandFeatures( output,signal )
%           where metrics is a struct with
%           metrics.truePositiveRate
%           metrics.tpositivePredictiveValue
%           metrics.falseNegativeRate

    dataset_directory = fullfile('DataBases','SAMI');
    % loads data
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,'*.mat'));           
    load(fullfile(pwd,dataset_directory,test_files(1).name));
    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
    % CHECK THE PPG REFERENCE HERE: THERE EXIST ONLY ONE %%%%%%
    % HEARTBEAT WHEN THE NUMBER OF PPG IS 4 %%%%%%%%%%%%%%%%%%%
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
    switch signal
        case 'ecg'
            try  
                timestamps = data.ECG(1,:);
                reference = data.ECGHeartBeat(1,:);
            catch err
                disp('signal not found');
            end

        case 'ppg0'
            try  
                timestamps = data.PPG_10(1,:);
                reference = data.HearBeat(1,:);
            catch err
                disp([' signal not found']);
            end

        case 'ppg1'                            
            try  
                timestamps = data.PPG_11(1,:);
                reference = data.HearBeat(1,:);
            catch err
                disp([' signal not found']);
            end

        case 'ppg2' 
            try  
                timestamps = data.PPG_20(1,:);
                reference = data.HearBeat(1,:);
            catch err
                disp([' signal not found']);
            end

        case 'ppg3'                            
            try  
                timestamps = data.PPG_21(1,:);
                reference = data.HearBeat(1,:);
            catch err
                disp([' signal not found']);
            end

    end
    
        reference = (reference-timestamps(1))/1000;
        time_tolerance = 0.05:0.05:.75;
        x_points = numel(time_tolerance);
    
        true_positive_rate = zeros(1,x_points);
        positive_predictive_value = zeros(1,x_points);
        false_negative_rate = zeros(1,x_points);
        
        detections = repmat(computedOutput,numel(reference),1);
        time_boundaries = abs(bsxfun(@minus,detections,reference'));
        
        for tolIdx = 1:x_points
            
            agreements = sum(time_boundaries<=time_tolerance(tolIdx),2);
            num_true_positive = sum(agreements==1);
            num_false_positive = sum(agreements>0)-sum(agreements==1);
            num_false_negative = sum(agreements==0);
            
            true_positive_rate(tolIdx) = num_true_positive/(num_true_positive+num_false_negative);
            positive_predictive_value(tolIdx) = num_true_positive/(num_true_positive+num_false_positive);
            false_negative_rate(tolIdx) = num_false_negative/(num_true_positive+num_false_negative);
                          
        end
    
    metrics.time_tolerance = time_tolerance ; 
    metrics.truePositiveRate = true_positive_rate;
    metrics.falseNegativeRate = false_negative_rate;
    metrics.positivePredictiveValue = positive_predictive_value;

end

