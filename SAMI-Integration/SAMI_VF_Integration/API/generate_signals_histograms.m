function plotnames = plotChannelHistograms(list_of_signals)

    dataset_directory = fullfile('..','DataBases');
        
    
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,'SAMI','*.mat'));    
           
    plotnames.signals = cell(numel(test_files),length(list_of_signals));
    plotnames.timestamps = cell(numel(test_files),length(list_of_signals));
    
    % looks for the signals to test
    for testIdx = 1:numel(test_files)
        data = load(fullfile(pwd,dataset_directory,'SAMI',test_files(testIdx).name));
        
        for sigIdx =1:numel(list_of_signals)

            switch list_of_signals{sigIdx}
                case 'ecg'
                    try
                        this_signal = data.ecg;
                        this_timestamps  = data.ecgTime;
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'ppg_red'
                    try
                        this_signal = data.ppg_red;
                        this_timestamps  = data.ppg_redTime;
                    catch err
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            this_signal = NaN;
                        else
                            disp(err.identifier);
                        end
                    end

                    case 'ppg_blue'                            
                        try
                            this_signal = data.ppg_blue;
                            this_timestamps = data.ppg_blueTime;
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'ppg_green_center' 
                        try
                            this_signal = data.ppg_green_center;
                            this_timestamps = data.ppg_green_centerTime;                                
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'ppg_green_side'                            
                        try
                            this_signal = data.ppg_green_side;
                            this_timestamps = data.ppg_green_sideTime;                                
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'accx'                            
                        try
                            this_signal = data.x_accel;
                            this_timestamps = data.x_accelTime;                                
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'accy'                            
                        try
                            this_signal = data.y_accel;
                            this_timestamps = data.y_accelTime;                                
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'accz'                            
                        try
                            this_signal = data.z_accel;
                            this_timestamps = data.z_accelTime;
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end
            end
                % correct timestamps to seconds 
                this_timestamps = (this_timestamps - this_timestamps(1))/1000;
                % timestamps histogram
                params = struct();
                params.name_suffix = [list_of_signals{sigIdx},'-',num2str(testIdx),'-timestamps'];
                params.xlabel = 'Time [secs]';
                params.ylabel = '';
                params.nbins = 50;
                params.plotname = 'Timestamps_Histogram_';
                % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
                plotnames.timestamps{testIdx,sigIdx} = generate_histogram(this_timestamps,params);

                % signals histogram
                params = struct();
                params.name_suffix = [list_of_signals{sigIdx},'-',num2str(testIdx),'-signal'];
                params.xlabel = 'Signal Values';
                params.ylabel = '';
                params.nbins = 50;
                params.plotname = 'Signal_Histogram_';
                % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
                plotnames.signals{testIdx,sigIdx} = generate_histogram(this_signal,params);
                
        end
    end

end

