function computed_stats = computeAlgStats(algOutput,stats)
%computeAlgStats generates statistics on the output of the algorithm
    
    switch stats
        case 'histogram'
            binrange = [min(algOutput) max(algOutput)];
            [bincount] = histc(algOutput,binrange);
            computed_stats.bincount = bincount;
            computed_stats.binrange = binrange;
        case 'interbeat-histogram'
            interbeat = diff(algOutput);
            binrange = [min(interbeat) max(interbeat)];
            [bincount] = histc(interbeat,binrange);
            computed_stats.bincount = bincount;
            computed_stats.binrange = binrange;
    end
            
 

end

