function computed_stats = computeSignalStats(signal,stats)
%computeSignalStats generates statistics on the signal
% types of statistics histgram


    dataset_directory = fullfile('DataBases','SAMI');
    % loads data
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,'*.mat'));           
    load(fullfile(pwd,dataset_directory,test_files(1).name));
    

    switch signal

        case 'ecg'
            try
                this_signal = data.ECG(2,:);
                this_timestamps  = data.ECG(1,:);
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'ppg0'
            try
                this_signal = data.PPG_10(2,:);
                this_timestamps  = data.PPG_10(1,:);
            catch err
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    disp([this_signals_to_test{sigIdx},' signal not found']);
                    this_signal = NaN;
                else
                    disp(err.identifier);
                end
            end

        case 'ppg1'                            
            try
                this_signal = data.PPG_11(2,:);
                this_timestamps  = data.PPG_11(1,:);
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'ppg2' 
            try
                this_signal = data.PPG_20(2,:);
                this_timestamps  = data.PPG_20(1,:);                             
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'ppg3'                            
            try
                this_signal = data.PPG_21(2,:);
                this_timestamps  = data.PPG_21(1,:);                              
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'accx'                            
            try
                this_signal = data.Accelerometer0(2,:);
                this_timestamps = data.Accelerometer0(1,:);                            
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'accy'                            
            try
                this_signal = data.Accelerometer1(2,:);
                this_timestamps = data.Accelerometer1(1,:);                     
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'accz'                            
            try
                this_signal = data.Accelerometer2(2,:);
                this_timestamps = data.Accelerometer2(1,:);   
            catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

    end
    
    switch stats
        case 'histogram'
            binrange = [min(this_timestamps) max(this_timestamps)];
            [bincount] = histc(this_timestamps,binrange);
            computed_stats{1}.bincount = bincount;
            computed_stats{1}.binrange = binrange;
            binrange = [min(this_signal) max(this_signal)];
            [bincounts] = histc(this_signal,binrange);
            computed_stats{2}.bincount = bincount;
            computed_stats{2}.binrange = binrange;
    end
            
 

end

