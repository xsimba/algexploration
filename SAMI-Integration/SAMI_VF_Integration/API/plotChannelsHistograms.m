function plotnames = plotChannelsHistograms(list_of_signals)

    dataset_directory = fullfile('DataBases');
        
    
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,'SAMI','*.mat'));    
           
    plotnames.signals = cell(1,length(list_of_signals));
    plotnames.timestamps = cell(1,length(list_of_signals));
    
    load(fullfile(pwd,dataset_directory,'SAMI',test_files(1).name));
        
        for sigIdx =1:numel(list_of_signals)

            switch list_of_signals{sigIdx}

                case 'ecg'
                    try
                        this_signal = data.ECG(2,:);
                        this_timestamps  = data.ECG(1,:);
                    catch err
                        disp(['signal not found']);
                    end

                case 'ppg0'
                    try
                        this_signal = data.PPG_10(2,:);
                        this_timestamps  = data.PPG_10(1,:);
                    catch err
                        disp(['signal not found']);
                    end

                case 'ppg1'                            
                    try
                        this_signal = data.PPG_11(2,:);
                        this_timestamps  = data.PPG_11(1,:);
                    catch err
                        disp(['signal not found']);
                    end

                case 'ppg2' 
                    try
                        this_signal = data.PPG_20(2,:);
                        this_timestamps  = data.PPG_20(1,:);                             
                    catch err
                        disp(['signal not found']);
                    end
                case 'ppg3'                            
                    try
                        this_signal = data.PPG_21(2,:);
                        this_timestamps  = data.PPG_21(1,:);                              
                    catch err
                        disp(['signal not found']);
                    end

                case 'accx'                            
                    try
                        this_signal = data.Accelerometer0(2,:);
                        this_timestamps = data.Accelerometer0(1,:);                            
                    catch err
                        disp(['signal not found']);
                    end

                case 'accy'                            
                    try
                        this_signal = data.Accelerometer1(2,:);
                        this_timestamps = data.Accelerometer1(1,:);                     
                    catch err
                        disp(['signal not found']);
                    end

                case 'accz'                            
                    try
                        this_signal = data.Accelerometer2(2,:);
                        this_timestamps = data.Accelerometer2(1,:);   
                    catch err
                        disp(['signal not found']);
                    end
            end
            
            % correct timestamps to seconds 
            this_timestamps = (this_timestamps - this_timestamps(1))/1000;
            % timestamps histogram
            params = struct();
            params.name_suffix = [list_of_signals{sigIdx},'-timestamps'];
            params.xlabel = 'Time [secs]';
            params.ylabel = '';
            params.nbins = 50;
            params.plotname = 'Timestamps_Histogram_';
            params.output_directory = 'plots';
            % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
            plotnames.timestamps{sigIdx} = generate_histogram(this_timestamps,params);

            % signals histogram
            params = struct();
            params.name_suffix = [list_of_signals{sigIdx},'-signal'];
            params.xlabel = 'Signal Values';
            params.ylabel = '';
            params.nbins = 50;
            params.plotname = 'Signal_Histogram_';
            params.output_directory = 'plots';
            % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
            plotnames.signals{sigIdx} = generate_histogram(this_signal,params);

        end

end

