function listOfOutputs = performAllTests( ...
    listOfAlgorithms,...
    listOfSignals,...
    listOfConfigurations )
%performAllTests runs all test for a list of algorithms
%   INPUTS: 
%       - listOfAlgorithms
%       - listOfSignals
%       - listOfConfigurations

%   OUTPUT:
%       - listOfOutputs
%   Usage Example for Beat Detectors: 
%       o = performAllTest(a,s,c)
%       where
%           a = {'BD_ONE','BD_TWO','BD_TRE'}
%           s = {'ecg','ppg0','ppg1'}
%           c = {CfgOne,CfgTwo,CfgTre}
    
    listOfOutputs = cell(1,numel(listOfAlgorithms);
    for idx = 1:numel(listOfAlgorithms)
        listOfOutputs{idx} = doTest(...
            listOfAlgorithms{idx},...
            listOfSignals{idx},...
            listOfConfigurations{idx});
    end
    
end

