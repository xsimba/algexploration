function data = getData(samiCredentials,times)
%getData Retrieves data from SAMI and saves data in .mat file in ./DataBases/SAMI
%   INPUTS: 
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%   OUTPUT:
%       - data: struct containing Simband data
%   Usage Example: 
%       samiCredentials.uid    = 'xxx'
%       samiCredentials.did    = 'xxx'
%       samiCredentials.dToken = 'xxx'
%       times.sdate            = '123456789' - Unix Time
%       times.edate            = '123499999' - Unix Time
%       data = getData(samiCredentials,times);   
% dataset_directory = fullfile('DataBases','SAMI');

    saving_dir = fullfile('.','DataBases','SAMI');
    
    hashString = genHashString(samiCredentials, times);
    
    filename = fullfile(               ...
            saving_dir, ...
            [hashString, '.mat']);
    
    if ~(exist(filename,'file')==2)
            
            % FIX THIS WITH A PLATFORM INDEPENDENT OS CALL
            %python_command = '/Users/pcasale/Library/Enthought/Canopy_64bit/User/bin/python ';
            python_command = 'python ';
            python_code = fullfile('.','DataAccessLayer','getSamiData.py');
            s1 = ' --uid '   ;
            s2 = ' --did '   ;
            s3 = ' --dtoken ';
            s4 = ' --sdate ' ;
            s5 = ' --edate ' ;
            string = [python_command python_code ...
                s1 samiCredentials.uid           ...
                s2 samiCredentials.did           ...
                s3 samiCredentials.dToken        ...
                s4 times.sDate                   ...
                s5 times.eDate];
            
            system (string);
            data = load('MatlabTransfer.mat');
            hashString = genHashString(samiCredentials, times);
            save(fullfile(saving_dir, hashString),'data');
            delete('MatlabTransfer.mat')
    else
        disp('File Exists');
        load(filename);
    end
end

function hash = genHashString(samiCredentials, times)
    z = java.lang.String([samiCredentials.uid,samiCredentials.did,...
        samiCredentials.dToken,times.sDate,times.eDate]);
    md5 = java.security.MessageDigest.getInstance('MD5');
    adapter = javax.xml.bind.annotation.adapters.HexBinaryAdapter;
    hString = adapter.marshal(md5.digest(z.getBytes()));
    hash = char(hString);
end