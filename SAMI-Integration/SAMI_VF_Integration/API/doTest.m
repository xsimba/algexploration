function output = doTest( algorithm,signal,configuration )
%doTest runs algorithm on signal
%   INPUTS: 
%       - algorithm: algorithm to test. It shoould be stored in the pathr
%           "Algorithms/algorithm"
%       - signal: channnel used by the algorithm
%       - configuration: all the parameters needed embedded in a struct
%   OUTPUT:
%       - output: output of the algorithm
%   Usage Example for Beat Detector: 
%       detected_beats_timestamps = doTest('BDBasic','ecg',Cfg)
%       where
%            "BDBasic" is stored in ./Algorithms/BDBasic
%            Cfg.Fs = 128 ; struct containing the algorithms parameters  
    
    % addpath 
    dataset_directory = fullfile('DataBases','SAMI');
    algorithms_directory = fullfile('Algorithms',algorithm);          
    addpath(fullfile(pwd,algorithms_directory));
    
    % loads data
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,'*.mat'));    
           
    load(fullfile(pwd,dataset_directory,test_files(1).name));
    

    switch signal

        case 'ecg'
            try
                this_signal = data.ECG(2,:);
                this_timestamps  = data.ECG(1,:);
                beat_detections_from_simband = data.ECGHeartBeat(1,:);
            catch err
                disp('signal not found');
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'ppg0'
            try
                this_signal = data.PPG_10(2,:);
                this_timestamps  = data.PPG_10(1,:);
                beat_detections_from_simband = data.HearBeat(1,:);
            catch err
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    disp([' signal not found']);
                    this_signal = NaN;
                else
                    disp(err.identifier);
                end
            end

        case 'ppg1'                            
            try
            this_signal = data.PPG_11(2,:);
            this_timestamps  = data.PPG_11(1,:);
            beat_detections_from_simband = data.HearBeat(1,:);
        catch err
            disp([' signal not found']);
            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                this_signal = NaN;
            end
            end

        case 'ppg2' 
            try
            this_signal = data.PPG_20(2,:);
            this_timestamps  = data.PPG_20(1,:);    
            beat_detections_from_simband = data.HearBeat(1,:);
        catch err
            disp([' signal not found']);
            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                this_signal = NaN;
            end
            end

        case 'ppg3'                            
            try
                this_signal = data.PPG_21(2,:);
                this_timestamps  = data.PPG_21(1,:);        
                beat_detections_from_simband = data.HearBeat(1,:);
            catch err
                disp([' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'accx'                            
            try
                this_signal = data.Accelerometer0(2,:);
                this_timestamps = data.Accelerometer0(1,:);                            
            catch err
                disp([' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'accy'                            
            try
                this_signal = data.Accelerometer1(2,:);
                this_timestamps = data.Accelerometer1(1,:);                     
            catch err
                disp([' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'accz'                            
            try
                this_signal = data.Accelerometer2(2,:);
                this_timestamps = data.Accelerometer2(1,:);   
            catch err
                disp([' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

    end

    % runs the function here
    output = feval(algorithm,this_signal,configuration);
    
    rmpath(fullfile(pwd,algorithms_directory));
    
end

