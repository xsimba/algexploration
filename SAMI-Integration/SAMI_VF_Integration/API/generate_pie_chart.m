function plotname = generate_pie_chart( data,params )
    
    nelem = histc(data,params.breakpoints);
    figure('units','normalized','outerposition',[0 0 1 1])
    try
        pie(nelem,params.element_names);
    catch err
        disp('Bug found...check the code')
    end
    title(params.title);
    plotname = params.plotname;
    saveas(gcf,fullfile(params.output_directory,plotname),'fig');
    saveas(gcf,fullfile(params.output_directory,plotname),'png');
    close all
    
end

