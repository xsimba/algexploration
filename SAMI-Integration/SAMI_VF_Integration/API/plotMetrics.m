function plotnames = plotMetrics( metrics )


        % True Positive Rate vs Time Tolerance

        plotname = 'truepositive';
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.truePositiveRate,'LineWidth',1.5);grid on;
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel('True Positive');
        xlabel('Time Tolerance')
        saveas(gcf,fullfile('.','plots',plotname),'fig');
        saveppt('temp.ppt');
        saveas(gcf,fullfile('.','plots',plotname),'png');
        close all
        plotnames{1} = fullfile('.','plots',plotname) ;

        % False Negative Rate vs Time Tolerance
        plotname = 'falsenegative';
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.falseNegativeRate,'LineWidth',1.5);grid on;
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel('False Negative');
        xlabel('Time Tolerance');
        saveas(gcf,fullfile('.','plots',plotname),'fig');
        
        saveas(gcf,fullfile('.','plots',plotname),'png');
        close all
        plotnames{2} = fullfile('.','plots',plotname) ;

        % Positive Predictivity Value vs Time Tolerance
        plotname = 'positivepredictivity';
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.positivePredictiveValue,'LineWidth',1.5);grid on;
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel('Positive Predictivity');
        xlabel('Time Tolerance');
        saveas(gcf,fullfile('.','plots',plotname),'fig');
        
        saveas(gcf,fullfile('.','plots',plotname),'png');
        close all
        plotnames{3} = fullfile('.','plots',plotname) ;
        
end

