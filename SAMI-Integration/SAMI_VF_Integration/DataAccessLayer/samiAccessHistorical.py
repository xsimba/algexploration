# -*- coding: utf-8 -*-
"""
Created on Tue Jun 24 23:39:29 2014
getData() class takes userID, deviceID, tokenID as inputs.
example constructor
data = getData(userID,deviceID,tokenID)
@author: n.davuluri
"""
import requests
import json
from datetime import datetime
from time import mktime
import sensorSchema as schema
import sys


def blockTimeMessageSum(blockNum, tstart, tend, tLoadTotal, nextMsg=True):
    tLoad = (tend - tstart) / 1000.
    tLoadTotal = tLoadTotal + tLoad
    if (nextMsg):
        print 'block', blockNum, 'read complete in ', tLoad, 's.  Reading next block'
    else:
        print 'block', blockNum, 'read complete in ', tLoad, 's. All done.'
        print 'Total Data Access Time: ', tLoadTotal
    sys.stdout.flush()
    return tLoadTotal
    
def timerSample():
    thisTime = datetime.now()
    sec_since_epoch = mktime(thisTime.timetuple()) + thisTime.microsecond/1000000.0
    return sec_since_epoch * 1000


class getSamiHandle():
    userID = ''
    deviceID = ''
    tokenID = ''
    count = 0
    
    def __init__(self,userID,deviceID,tokenID, startDate, endDate):
        self.userID = userID
        self.deviceID = deviceID
        self.tokenID = tokenID
        self.startDate = startDate
        self.endDate = endDate
        self.nextParameter = ''
    
    def vcExtractSingleData(self,datum):
        timeStamp = datum['ts']
        timeList = self.firstTime()
        timeFirst = timeList[1]
        thisTime = timeStamp/1000.0 - timeFirst
        parsedictData =datum['data']           
        if parsedictData.has_key('ecg'):
            ampl = parsedictData['ecg']
        else:
            ampl = None
        return (thisTime, ampl)
        
        
    def vcEcgData(self):
        
        '''This method returns a tuple with ecg amplitudes and the corresponding
           timestamp (converted into milliseconds and adjusted to the first timestamp)
           from the Vital Connect device'''
           
        getHeaders = {'Authorization': 'bearer '+self.tokenID}
        ecgAmp =[]
        '''print totalNumberMessages, numDataPerCall, numAPICalls'''
        getHeaders = {'Authorization':'bearer '+self.tokenID}
        offset = 00
        tstart = []
        tend = []
        count = 0
        while True:
            try:
                tstart[count] = datetime.now()
                getURL = 'https://api.samihub.com/v1.1/historical/normalized/messages?uid='+self.userID+\
                    '&offset='+ str(offset) +'&count=1000&startDate='+ self.startDate + '&endDate=' + self.endDate
                response = requests.request('GET', getURL, headers = getHeaders)
                tend[count] = datetime.now()                
                dictResult = json.loads(response.content)
                data = dictResult['data']
                processData = map(self.vcExtractSingleData, data)
                ecgAmp.extend(processData)
                print offset
                if (dictResult['size'] < 1000):
                    return ecgAmp
                offset += len(processData)
                
            except ValueError, e:
                print e
                return ecgAmp
            count += 1
        print tstart,tend
                
        return ecgAmp
    
    #
    # note the only thing that is simba about this is the command to 
    # map "schema.simbaE
    # 
    #            
    def getSimbaData(self):
        getHeaders = {'Authorization':'bearer '+self.tokenID}
        allData=[]
        count = 0
        getURL='https://portal.samihub.com/v1.1/historical/normalized/messages?uid=' + self.userID +\
        '&startDate=' + self.startDate + '&endDate=' + self.endDate

        print 'GET: ', getURL        
        
        tLoadTotal = 0
                
        tstart = timerSample()
        response = requests.request('GET', getURL, headers = getHeaders)
        tend = timerSample()
        
        dictResult = json.loads(response.content)
        if dictResult.has_key('next'):
            nextParameter = dictResult['next']
            data = dictResult['data']
            processData = map(schema.simbaExtractSingleData, data)
            allData.extend(processData)
            tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=True)
        else:
            data = dictResult['data']
            processData = map(schema.simbaExtractSingleData, data)
            allData.extend(processData)
            tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=False)
            return allData
            
        while True:
            try:
                getURL='https://portal.samihub.com/v1.1/historical/normalized/messages?uid='+self.userID+\
                '&startDate=' + self.startDate + '&endDate=' + self.endDate + '&offset=' + nextParameter
                count = count + 1
                tstart = timerSample()
                response = requests.request('GET', getURL, headers = getHeaders)
                tend = timerSample()
                
                # print 'time to retrieve block in milliseconds', (tend-tstart)
                dictResult = json.loads(response.content)
                if dictResult.has_key('next'):
                    nextParameter = dictResult['next']
                    # print 'nextparameter',nextParameter
                    data = dictResult['data']
                    processData = map(schema.simbaExtractSingleData, data)
                    allData.extend(processData)
                    tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=True)

                else:
                    data = dictResult['data']
                    processData = map(schema.simbaExtractSingleData, data)
                    allData.extend(processData)
                    tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=False)
                    return allData

            except ValueError, e:
                print e
                return allData

        return allData
        
