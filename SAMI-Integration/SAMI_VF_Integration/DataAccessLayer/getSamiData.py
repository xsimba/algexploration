# -*- coding: utf-8 -*-
"""
Created on Tue Jun 24 23:34:49 2014

@author: n.davuluri

Retrieves all the messages from a specified device and parses all the data
Prints the time to taken retrieve each set of 1000 messages and also the
total length of data in seconds.

Currently for simband data only.. To do: make it more flexible.

"""

import sys, getopt
import samiAccessHistorical as samiX
import sensorSchema as ss 
from scipy.io import savemat

def main(argv):
    userID = ''
    deviceID = ''
    deviceToken = ''
    startDate = ''
    endDate = ''
    
    try:
        opts,args = getopt.getopt(argv, "h",["uid=","did=","dtoken=","sdate=","edate="])
    except getopt.GetoptError:
        print 'getSamiData.py --uid <userID> --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'getSamiData.py --uid <userID> --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
            sys.exit()
        elif opt == "--uid":
            userID = arg
        elif opt == "--did":
            deviceID = arg
        elif opt == "--dtoken":
            deviceToken = arg
        elif opt == "--sdate":
            startDate = arg
        elif opt == "--edate":
            endDate = arg
    credentials = {}
    pointer = {}
    credentials['userID'] = userID
    credentials['deviceID'] = deviceID
    credentials['deviceToken'] = deviceToken
    pointer['startDate'] = startDate
    pointer['endDate'] = endDate
    return credentials, pointer
    
if __name__ == "__main__":
    credentials, pointer = main(sys.argv[1:]) 

userID = credentials['userID']
deviceID = credentials['deviceID']
deviceToken = credentials['deviceToken']
startDate = pointer['startDate']
endDate = pointer['endDate']

#
# Create a connector to the device
#
f = samiX.getSamiHandle(userID,deviceID,deviceToken,startDate,endDate)

#
# to do: use the information from SAMI to determine the
# correct parsing and send into an if-else block
#
allDataJson = f.getSimbaData()
data = ss.parseSimba(allDataJson)

#
# to do: give it a temp name and ? clean up afterwards.
#
savemat ('MatlabTransfer.mat', data);
