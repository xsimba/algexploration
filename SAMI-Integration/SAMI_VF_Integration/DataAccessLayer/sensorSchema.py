# -*- coding: utf-8 -*-
"""
Created on Wed Jul 30 07:53:57 2014

@author: n.davuluri
"""


# simbaKeyWords: Creates a dictionary of keywords
#
# ak (8/12/14):  Modified schema to come from the 
# spreadsheet Rosetta.xlsx (column H)
#
def simbaKeyWords(origin):
    keywords = {}
    
    keywords['com.samsung.simba/HRV'] = 'HRV'
    keywords['com.samsung.simba/HR'] = 'HR'
    keywords['com.samsung.simba/PAT'] = 'PAT'
    keywords['com.samsung.simba/ECGHeartBeat'] = 'ECGHeartBeat'
    keywords['com.samsung.simba/HeartBeat'] = 'HeartBeat'
    keywords['com.samsung.simba/ECGRaw'] = 'ECGRaw'
    keywords['com.samsung.simba/ppg/blue'] = 'ppg_blue'
    keywords['com.samsung.simba/ppg/red'] = 'ppg_red'
    keywords['com.samsung.simba/ppg/green/center'] = 'ppg_green_center'
    keywords['com.samsung.simba/ppg/green/side'] = 'ppg_green_side'
    keywords['com.samsung.simba/ECG'] = 'ECG'
    keywords['com.samsung.simba/PPG/2'] = 'PPG_2'
    #
    keywords['com.samsung.simba/PPG/1'] = 'PPG_1'
    #
    keywords['com.samsung.simba/Accelerometer'] = 'Accelerometer'
    #
    #
    keywords['com.samsung.simba/PPG/3/VISUAL'] = 'PPG_3_VISUAL'
    keywords['com.samsung.simba/PPG/1/VISUAL'] = 'PPG_1_VISUAL'
    keywords['com.samsung.simba/PPG/2/VISUAL'] = 'PPG_2_VISUAL'
    keywords['com.samsung.simba/PPG/0/VISUAL'] = 'PPG_0_VISUAL'
    keywords['com.samsung.simba/ECG/VISUAL'] = 'ECG_VISUAL'
    keywords['com.samsung.simba/PPGFilt/1'] = 'PPGFilt_1'
    #
    keywords['com.samsung.simba/PPGFilt/2'] = 'PPGFilt_2'
    #
    keywords['com.samsung.simba/PPGPre/1'] = 'PPGPre_1'
    #
    keywords['com.samsung.simba/PPGPre/2'] = 'PPGPre_2'
    #
    keywords['com.samsung.simba/ppg/green/side:visual'] = 'ppg_green_side_visual'
    keywords['com.samsung.simba/ppg/green/center:visual'] = 'ppg_green_center_visual'
    keywords['com.samsung.simba/ppg/blue:visual'] = 'ppg_blue_visual'
    keywords['com.samsung.simba/ppg/red:visual'] = 'ppg_red_visual'
    keywords['com.samsung.simba/BP'] = 'BP'
    #
    keywords['com.samsung.simba/BioZ'] = 'BioZ'
    keywords['com.samsung.simba/BioZRaw'] = 'BioZRaw'
    keywords['com.samsung.simba/ppg/ir1:visual'] = 'ppg_ir1_visual'
    keywords['com.samsung.simba/ppg/ir1'] = 'ppg_ir1'
    keywords['com.samsung.simba/ppg/ir2:visual'] = 'ppg_ir2_visual'
    keywords['com.samsung.simba/ppg/ir2'] = 'ppg_ir2'
    keywords['com.samsung.simba/PPG/4/VISUAL'] = 'PPG_4_VISUAL'
    #
    return(keywords[origin])



def simbaSimulatorKeyWords(origin):
    keywords = {}
    
    keywords['com.samsung.ssic.simba.simulator/PPGPre/1'] = 'PPGPre_1'
    keywords['com.samsung.ssic.simba.simulator/PPGPre/2'] = 'PPGPre_2'
    keywords['com.samsung.ssic.simba.simulator/PPGFilt/1'] = 'PPGFilt_1'
    keywords['com.samsung.ssic.simba.simulator/PPGFilt/2'] = 'PPGFilt_2'
    keywords['com.samsung.ssic.simba.simulator/PPG/0/VISUAL'] = 'PPG_0_VISUAL'
    keywords['com.samsung.ssic.simba.simulator/PPG/1/VISUAL'] = 'PPG_1_VISUAL'
    keywords['com.samsung.ssic.simba.simulator/PPG/2/VISUAL'] = 'PPG_2_VISUAL'
    keywords['com.samsung.ssic.simba.simulator/PPG/3/VISUAL'] = 'PPG_3_VISUAL'
    keywords['com.samsung.ssic.simba.simulator/ECG'] = 'ECG'
    keywords['com.samsung.ssic.simba.simulator/ECGFiltered'] = 'ECGFiltered'
    keywords['com.samsung.ssic.simba.simulator/ECG/VISUAL'] = 'ECG_VISUAL'
    keywords['com.samsung.ssic.simba.simulator/PPG/2'] = 'PPG_2'
    keywords['com.samsung.ssic.simba.simulator/PPG/1'] = 'PPG_1'
    keywords['com.samsung.ssic.simba.simulator/ECGRaw'] = 'ECGRaw'
    keywords['com.samsung.ssic.simba.simulator/HeartBeat'] = 'HeartBeat'
    keywords['com.samsung.ssic.simba.simulator/Accelerometer'] = 'Accelerometer'
    keywords['com.samsung.ssic.simba.simulator/PAT'] = 'PAT'
    keywords['com.samsung.ssic.simba.simulator/BloodPressure'] = 'BloodPressure'
    keywords['com.samsung.ssic.simba.simulator/HR'] = 'HR'
    keywords['com.samsung.ssic.simba.hrv'] = 'HRV'
    keywords['com.samsung.ssic.simba.simulator/ECGHeartBeat'] = 'ECGHeartBeat'
    keywords['com.samsung.ssic.simba.hr'] = 'HR'
    keywords['com.samsung.ssic.simba.bp'] = 'BP'
    
    return(keywords[origin])


#
# extract data from JSON packet and interpolate timestamp based on
# sample rate if it is a vector.
#
def simbaExtractSingleData(samiJson):
    samiTimeStamp = samiJson['cts']
    timeStamp = samiJson['ts'] 
    sensorType = samiJson['data']['metadata']['sensorType']
    if sensorType == 'ppg':
        sensorType = 'PPG'
    if sensorType == 'BloodPressure':
        sensorType = 'BP'
    origin = samiJson['data']['metadata']['origin']
    sampleRate = samiJson['data']['metadata']['sampleRate']
    channels = samiJson['data']['metadata']['channels']    
    values = samiJson['data']['content'][sensorType]
    key = simbaKeyWords(origin)
    timeStamps = []
    samiTimeStamps = []

    if len(values) > 1:
        if channels == 1:
            numberValues = len(values)
            for k in range(0, numberValues):
                timeStamps.append(timeStamp + (k)*1000.0/sampleRate)
                samiTimeStamps.append(samiTimeStamp)
        elif channels >1:
            numberValues = len(values)/channels
            for k in range(0, numberValues):
                timeStamps.append(timeStamp + k*1000.0/sampleRate)
                samiTimeStamps.append(samiTimeStamp)
    elif len(values) ==1:
        timeStamps = [timeStamp]
        samiTimeStamps = [samiTimeStamp]


    rcDatum = {}    
    rcDatum['timeStamps']  = timeStamps
    rcDatum['values']     = values
    rcDatum['sampleRate'] = sampleRate
    rcDatum['key']        = key
    rcDatum['channels']   = channels
    rcDatum['samiTimeStamps'] = samiTimeStamps
    
    return rcDatum
 
#
# parseSimba demuxes the cases of multiple channels into 
# arrays.
#        
def parseSimba(allData):      
    new_dict = {}
    
    for datum in allData:
        timeStamps = datum['timeStamps']
        values = datum['values']
        key = datum['key']
        channels = datum['channels']
        samiTimeStamps = datum['samiTimeStamps']
        if channels == 1:
            if new_dict.has_key(key):
                new_dict[key][0].extend(timeStamps)
                new_dict[key][1].extend(values)
                new_dict[key][2].extend(samiTimeStamps)
            else:
                new_dict[key] = (timeStamps, values, samiTimeStamps)
        elif channels > 1:
            for k in range(0, channels):
                new_key = key + str(k)
                new_values = values[k::channels]
                if (new_dict.has_key(new_key)) == False:
                    new_dict[new_key] = ([],[],[])
                new_dict[new_key][0].extend(timeStamps)
                new_dict[new_key][1].extend(new_values)
                new_dict[new_key][2].extend(samiTimeStamps)
                
    return new_dict