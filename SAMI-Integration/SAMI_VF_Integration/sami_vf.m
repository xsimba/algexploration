% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%% Research Console integrated with Validation Framework %%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main Features:
% 1 - Retrieves data from SAMI @ https://api.samihub.com/ 
% 2 - Visualizes plots of signals [ecg, ppg, acc] 
% 3 - Tests BD on ECG/PPG
% dev: PCasale : August 6th @ SSIC

clear all

addpath('DataAccessLayer');
%addpath('Matlab');
addpath('API');

acq = 'Ana' ; % 'Ram' | 'Ana';


if strcmpi(acq,'Ram')
    sc.dToken   = 'fbc35eac9f1d4850b03112801189b7dd';
    sc.did      = '771cf8655c3249ada22ea7c405e32eec';
    sc.uid      = '34823184b0fc4f4b8b584db655ca44a0';
    times.sDate = '1358020800000';
    times.eDate = '1406228400000';
elseif strcmpi(acq,'Ana')
    sc.dToken   = '1def59e56d1d4a06acf85e3cd926d2cb';
    sc.did      = 'c1717a85481b4f2aa0a1b231a190594';
    sc.uid      = '712dc68dea5044d9b9461c16e369fa62';
    endDate =  1406932020000;%
    offset = 36*60; % 36 minutes of data
    times.sDate = num2str(endDate - offset*1000);
    times.eDate = num2str(endDate);
end

% gets data

data = getData(sc,times);

% run BD test
% Cfg.Fs = 128 ;
% output = doTest( 'BDBasic','ecg',Cfg );
Cfg = 'ECG';
output = doTest( 'BD','ecg',Cfg );

% compute metrics
metrics = compareBandFeatures(output,'ecg');

% computes statistics on the signal
computed_stats = computeSignalStats('ecg','histogram');

% computes statistics on the algorithm result
bd_output_stats = computeAlgStats(output,'histogram');
% computes statistics on the interbeat time from BD output
ibt_stats = computeAlgStats(output,'interbeat-histogram');

% plots board metrics
metrics_plotnames = plotMetrics(metrics);

% plots histograms for all the channels in the list
list_of_signals = {'ecg','ppg0','accx'};
allsig_hist_plotnames = plotChannelsHistograms(list_of_signals);

% plot statistics of BD outputs
params = struct();
params.nbins = 50 ; 
params.xlabel = 'BD Output';
params.ylabel = 'Count';
params.output_directory = 'plots';
out_hist_plotnames = plotTestResults(output,'histogram',params);

% plot statistics of BD outputs
params = struct();
params.nbins = 15 ; 
params.xlabel = 'Interbeat Time';
params.ylabel = 'Count';
params.output_directory = 'plots';
out_ibthist_plotnames = plotTestResults(output,'histogram-interbeat',params);


% plot statistics of BD outputs
params = struct();
params.breakpoints = [0,.3,1,5];
params.element_names = {'< .3 secs','< 1 secs','< 5 secs','> 5 secs'};
params.title = 'Percentage of Beats within the boundary';
params.output_directory = 'plots';
pie_plotnames = plotTestResults(output,'piechart',params);