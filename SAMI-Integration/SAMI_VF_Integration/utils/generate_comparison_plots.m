function [plotname_tpr,plotname_fpr,plotname_ppv] = generate_comparison_plots( test_results )
    
    plot_styles = {'-','--',':','-.'};
    
    % find something smarter to implement this!
    all_signals = {};
    all_datasets = {};
    
    algorithms_to_test = fieldnames(test_results);
    for algIdx = 1:numel(algorithms_to_test)
        all_signals = [all_signals fieldnames(test_results.(algorithms_to_test{algIdx}))'];
        signals_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}));
        for sigIdx = 1:numel(signals_to_test)
            all_datasets = [all_datasets fieldnames(test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}))'];
        end
    end  
    
    unique_signals = unique(all_signals);
    unique_datasets = unique(all_datasets);
    plotnames = cell(1,3);

    % plot true_positive_rates
    plotname_tpr = {};
    for sigIdx = 1:numel(unique_signals)
        for dsIdx = 1:numel(unique_datasets)
            plotname = ['Comparison_True_Positive_Rate-',unique_signals{sigIdx},'-',unique_datasets{dsIdx}];
            figure('units','normalized','outerposition',[0 0 1 1])
            hold on;
            for_legend = {};
            for algIdx = 1:numel(algorithms_to_test)
                if isfield(test_results.(algorithms_to_test{algIdx}),unique_signals{sigIdx})
                    if isfield(test_results.(algorithms_to_test{algIdx}).(unique_signals{sigIdx}),unique_datasets{dsIdx})
                        metrics = test_results.(algorithms_to_test{algIdx}).(unique_signals{sigIdx}).(unique_datasets{dsIdx}).annotation_based_metrics_timetol ;
                        if metrics.validity
                            plot(metrics.time_tolerance,metrics.true_positive_rate,'LineWidth',1.8,'LineStyle',plot_styles{algIdx});
                            for_legend = [for_legend algorithms_to_test{algIdx}]; 
                        end
                    end
                end
            end
            if ~isempty(for_legend)
                legend(for_legend,'Location','SouthWest');
                xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
                ylim([0 1.01])
                title([unique_signals{sigIdx},'-',unique_datasets{dsIdx}])
                ylabel('True Positive Rate');
                xlabel('Time Tolerance')
                saveas(gcf,plotname,'fig');
                saveas(gcf,plotname,'png');
                close all
                plotname_tpr{sigIdx,dsIdx} = plotname ;
            else
                close all
                plotname_tpr{sigIdx,dsIdx} = 'None' ;
            end
        end
    end

    
    % plot False_Negative_Rate
    plotname_fpr = {};
    for sigIdx = 1:numel(unique_signals)
        for dsIdx = 1:numel(unique_datasets)
            plotname = ['Comparison_False_Negative_Rate',unique_signals{sigIdx},'-',unique_datasets{dsIdx}];
            figure('units','normalized','outerposition',[0 0 1 1])
            hold on;
            for_legend = {};
            for algIdx = 1:numel(algorithms_to_test)
                if isfield(test_results.(algorithms_to_test{algIdx}),unique_signals{sigIdx})
                    if isfield(test_results.(algorithms_to_test{algIdx}).(unique_signals{sigIdx}),unique_datasets{dsIdx})
                        metrics = test_results.(algorithms_to_test{algIdx}).(unique_signals{sigIdx}).(unique_datasets{dsIdx}).annotation_based_metrics_timetol ;
                        if metrics.validity
                            plot(metrics.time_tolerance,metrics.false_negative_rate,'LineWidth',1.8,'LineStyle',plot_styles{algIdx});
                            for_legend = [for_legend algorithms_to_test{algIdx}]; 
                        end
                    end
                end
            end
            if ~isempty(for_legend)
                legend(for_legend,'Location','SouthWest');
                xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
                ylim([0 1.01])
                title([unique_signals{sigIdx},'-',unique_datasets{dsIdx}])
                ylabel('False Negative Rate');
                xlabel('Time Tolerance')
                saveas(gcf,plotname,'fig');
                saveas(gcf,plotname,'png');
                close all
                plotname_fpr{sigIdx,dsIdx} = plotname ;
             else
                close all
                plotname_fpr{sigIdx,dsIdx} = 'None' ;
            end
        end
    end
        
    % plot Positive Predictive Value
    plotname_ppv = {};
    for sigIdx = 1:numel(unique_signals)
        for dsIdx = 1:numel(unique_datasets)
            plotname = ['Comparison_Positive_Predictive_Value',unique_signals{sigIdx},'-',unique_datasets{dsIdx}];
            figure('units','normalized','outerposition',[0 0 1 1])
            hold on;
            for_legend = {};
            for algIdx = 1:numel(algorithms_to_test)
                if isfield(test_results.(algorithms_to_test{algIdx}),unique_signals{sigIdx})
                    if isfield(test_results.(algorithms_to_test{algIdx}).(unique_signals{sigIdx}),unique_datasets{dsIdx})
                        metrics = test_results.(algorithms_to_test{algIdx}).(unique_signals{sigIdx}).(unique_datasets{dsIdx}).annotation_based_metrics_timetol ;
                        if metrics.validity
                            plot(metrics.time_tolerance,metrics.positive_predictive_value,'LineWidth',1.8,'LineStyle',plot_styles{algIdx});
                            for_legend = [for_legend algorithms_to_test{algIdx}]; 
                        end
                    end
                end
            end
            if ~isempty(for_legend)
                legend(for_legend,'Location','SouthWest');
                xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
                ylim([0 1.01])
                title([unique_signals{sigIdx},'-',unique_datasets{dsIdx}])
                ylabel('Positive Predictive Value');
                xlabel('Time Tolerance')
                saveas(gcf,plotname,'fig');
                saveas(gcf,plotname,'png');
                close all
                plotname_ppv{sigIdx,dsIdx} = plotname ;
            else
                close all
                plotname_ppv{sigIdx,dsIdx} = 'None' ;
            end
        end
    end
end

