function test_results = compute_metrics(test_results)

    algorithms_to_test = fieldnames(test_results);

    for algIdx = 1:numel(algorithms_to_test)
        signals_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}));
    
        for sigIdx = 1:numel(signals_to_test)
            datasets_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}));
        
            for dsIdx = 1:numel(datasets_to_test)
                
                this_test_results = test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx});
                
                % annotation free metrics
                annotation_free_metrics = compute_annotation_free_metrics(this_test_results.detections);
                test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_free_metrics = annotation_free_metrics;
                
            end
        end
    end  
end