function plotnames = generate_signals_histograms(list_of_signals,data_directory)
        

    output_directory ='plots';
    
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,data_directory,'*.mat'));    
           
    plotnames.signals = cell(numel(test_files),length(list_of_signals));
    plotnames.timestamps = cell(numel(test_files),length(list_of_signals));
    
    % looks for the signals to test
    for testIdx = 1:numel(test_files)
        
        load(fullfile(pwd,data_directory,test_files(testIdx).name));
        
        for sigIdx =1:numel(list_of_signals)

            switch list_of_signals{sigIdx}
                
                case 'ecg'
                    try
                        this_signal = data.ECG(2,:);
                        this_timestamps  = data.ECG(1,:);
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'ppg0'
                    try
                        this_signal = data.PPG_10(2,:);
                        this_timestamps  = data.PPG_10(1,:);
                    catch err
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            this_signal = NaN;
                        else
                            disp(err.identifier);
                        end
                    end

                case 'ppg1'                            
                    try
                        this_signal = data.PPG_11(2,:);
                        this_timestamps  = data.PPG_11(1,:);
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'ppg2' 
                    try
                        this_signal = data.PPG_20(2,:);
                        this_timestamps  = data.PPG_20(1,:);                             
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'ppg3'                            
                    try
                        this_signal = data.PPG_21(2,:);
                        this_timestamps  = data.PPG_21(1,:);                              
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'accx'                            
                    try
                        this_signal = data.Accelerometer0(2,:);
                        this_timestamps = data.Accelerometer0(1,:);                            
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'accy'                            
                    try
                        this_signal = data.Accelerometer1(2,:);
                        this_timestamps = data.Accelerometer1(1,:);                     
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'accz'                            
                    try
                        this_signal = data.Accelerometer2(2,:);
                        this_timestamps = data.Accelerometer2(1,:);   
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end
                    
            end
            
            % correct timestamps to seconds 
            this_timestamps = (this_timestamps - this_timestamps(1))/1000;
            % timestamps histogram
            params = struct();
            params.name_suffix = [list_of_signals{sigIdx},'-',num2str(testIdx),'-timestamps'];
            params.xlabel = 'Time [secs]';
            params.ylabel = '';
            params.nbins = 50;
            params.plotname = 'Timestamps_Histogram_';
            params.output_directory = output_directory ; 
            % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
            plotnames.timestamps{testIdx,sigIdx} = generate_histogram(this_timestamps,params);

            % signals histogram
            params = struct();
            params.name_suffix = [list_of_signals{sigIdx},'-',num2str(testIdx),'-signal'];
            params.xlabel = 'Signal Values';
            params.ylabel = '';
            params.nbins = 50;
            params.plotname = 'Signal_Histogram_';
            params.output_directory = output_directory ; 
            % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
            plotnames.signals{testIdx,sigIdx} = generate_histogram(this_signal,params); 
            
        end
    end

end

