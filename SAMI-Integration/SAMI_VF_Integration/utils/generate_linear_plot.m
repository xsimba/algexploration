function plotname = generate_linear_plot( data,params )
    
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(data,data,'+b');grid on;
    xlabel(params.xlabel);
    ylabel(params.ylabel);
    plotname = [params.plotname,params.name_suffix];
    saveas(gcf,plotname,'fig');
    saveas(gcf,plotname,'png');
    close all
end

