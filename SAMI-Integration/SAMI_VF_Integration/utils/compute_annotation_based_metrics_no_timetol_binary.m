function annotation_based_metrics = compute_annotation_based_metrics_no_timetol_binary( all_detections,all_annotations )
    
    annotation_based_metrics.validity = 1 ;
    
    num_tests = numel(all_annotations);
    
    for fileIdx = 1:num_tests 
       try 
            stats = confusionmatStats(all_annotations{fileIdx},all_detections{fileIdx}); 
            annotation_based_metrics.stats{fileIdx} = stats;
       catch err
            if strcmpi(err.identifier,'stats:confusionmat:GRowNumMismatch')
               if isnan(all_annotations{fileIdx})
                    annotation_based_metrics.stats{fileIdx} = NaN;
                    annotation_based_metrics.validity = 0 ;
               end
            else
                keyboard()
            end
       end
    end

end

