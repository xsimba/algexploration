function filename = write_report( test_results )

    filename = ['Test_Report_',datestr(fix(clock),30)];
    mkdir(fullfile(pwd,filename))
    cd(fullfile(pwd,filename));
    addpath(fullfile('..','utils'));
    
    fin = fopen([filename,'.html'],'w');
    fprintf(fin,'<!DOCTYPE html>');
    fprintf(fin,'<html>');
    fprintf(fin,'<body>');
  
    
    signals_to_show = {...
    'ecg',...
    'ppg_red',...
    'ppg_blue',...
    'ppg_green_center',...
    'ppg_green_side',...
    'accx',...
    'accy',...
    'accz',...
    };   
    raw_signals_plotnames = generate_signals_histograms(signals_to_show);
    

    for idx = 1:size(raw_signals_plotnames.timestamps,2)
        for jdx = 1:size(raw_signals_plotnames.timestamps,1)
            fprintf(fin,['<h1> Test file ',num2str(jdx),'-',signals_to_show{idx},'</h1>']);
            fprintf(fin,['<img src="',raw_signals_plotnames.timestamps{jdx,idx},'.png" alt="',raw_signals_plotnames.timestamps{jdx,idx},'" width="542" height="342">']);
            fprintf(fin,['<img src="',raw_signals_plotnames.signals{jdx,idx},'.png" alt="',raw_signals_plotnames.signals{jdx,idx},'" width="542" height="342">']);
        end
    end

    
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
    % plots here results from algorithms testing %
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %

    algorithms_to_test = fieldnames(test_results);
    for algIdx = 1:numel(algorithms_to_test)
        signals_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}));
        for sigIdx = 1:numel(signals_to_test)
            datasets_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}));
            
            
                fprintf(fin,['<h1> Results ',algorithms_to_test{algIdx},...
                    'on ',signals_to_test{sigIdx},'</h1>' ]);
                
                
            for dsIdx = 1:numel(datasets_to_test)
           
           
                af_metrics = test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_free_metrics;
                
                % interbeat time histogram
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.xlabel = 'Interbeat time [secs]';
                params.ylabel = 'Interbeat time';
                params.nbins = 0:10;
                params.xlims = [-1 11];
                params.plotname = 'Iterbeattime_Histogram_';
                plotname = generate_histogram(af_metrics.interbeat_time,params);
                disp(plotname)
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);

                % pie chart
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.breakpoints = [0,.3,1,5];
                params.element_names = {'< .3 secs','< 1 secs','< 5 secs','> 5 secs'};
                params.title = 'Percentage of Beats within the boundary';
                params.plotname = 'Iterbeattime_Pie_';
                plotname = generate_pie_chart(af_metrics.interbeat_time, params);
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);

                % box plot
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.xlabel = 'Test Files';
                params.ylabel = 'Interbeat time [secs]';
                params.plotname = 'Iterbeattime_Boxplot_';
                plotname = generate_boxplots(af_metrics.interbeat_time,af_metrics.interbeat_groups,params);
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);

                % linear plots
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.xlabel = 'Interbeat time [secs]';
                params.ylabel = 'Interbeat time [secs]';
                params.plotname = 'Iterbeattime_Plot_';
                plotname = generate_linear_plot(af_metrics.interbeat_time,params);
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);
                
            end
        end
    end  
    
    fprintf(fin,'</body>');
    fprintf(fin,'</html>');
    fclose(fin);

    rmpath(fullfile('..','utils'));
    cd('..')

end

