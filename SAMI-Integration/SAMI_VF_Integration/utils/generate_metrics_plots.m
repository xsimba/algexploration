function plotnames = generate_metrics_plots( metrics,params )

        % True Positive Rate vs Time Tolerance

        plotname = [params{1}.plotname,params{1}.name_suffix];
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.true_positive_rate,'LineWidth',1.5);grid on;
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel(params{1}.ylabel);
        xlabel(params{1}.xlabel)
        saveas(gcf,plotname,'fig');
        saveas(gcf,plotname,'png');
        close all
        plotnames{1} = plotname ;

        % False Negative Rate vs Time Tolerance
        plotname = [params{2}.plotname,params{2}.name_suffix];
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.false_negative_rate,'LineWidth',1.5);grid on;
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel(params{2}.ylabel);
        xlabel(params{2}.xlabel);
        saveas(gcf,plotname,'fig');
        saveas(gcf,plotname,'png');
        close all
        plotnames{2} = plotname ;

        % Positive Predictivity Value vs Time Tolerance
        plotname = [params{3}.plotname,params{3}.name_suffix];
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.positive_predictive_value,'LineWidth',1.5);grid on;
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel(params{3}.ylabel);
        xlabel(params{3}.xlabel);
        saveas(gcf,plotname,'fig');
        saveas(gcf,plotname,'png');
        close all
        plotnames{3} = plotname ;
        
        % F-Measure vs Time Tolerance
        plotname = [params{4}.plotname,params{4}.name_suffix];
        figure('units','normalized','outerposition',[0 0 1 1])
        plot(metrics.time_tolerance,metrics.f_measure,'LineWidth',1.5);grid on ; 
        xlim([min(metrics.time_tolerance)-0.1,max(metrics.time_tolerance)]);
        ylim([0 1.01])
        ylabel(params{4}.ylabel);
        xlabel(params{4}.xlabel);
        saveas(gcf,plotname,'fig');
        saveas(gcf,plotname,'png');
        close all
        plotnames{4} = plotname ;
end

