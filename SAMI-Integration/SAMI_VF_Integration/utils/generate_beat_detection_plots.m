function [results,plotnames] = generate_beat_detection_plots(BD_algorithm, Cfg)
    
    algorithms_directory = 'Algorithms';
    addpath(fullfile(pwd,algorithms_directory,BD_algorithm));
    
    % looks for test files in the dataset directory 
    % there should be only one 
    test_files = dir(fullfile(pwd,'DataBases','SAMI','*.mat'));    
    load(fullfile(pwd,'DataBases','SAMI',test_files(1).name));

    switch Cfg.SypType

        case 'ecg'
            try
                this_signal = data.ECG(2,:);
                this_timestamps  = data.ECG(1,:);
                this_annotations_timestamps = data.ECGHeartBeat(2,:);
                catch err
                disp([this_signals_to_test{sigIdx},' signal not found']);
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    this_signal = NaN;
                end
            end

        case 'ppg0'
            try
                this_signal = data.PPG_10(2,:);
                this_timestamps  = data.PPG_10(1,:);
                this_annotations_timestamps = data.HeartBeat(1,:);   
            catch err
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    disp([this_signals_to_test{sigIdx},' signal not found']);
                    this_signal = NaN;
                else
                    disp(err.identifier);
                end
            end
            
        case 'ppg1'
            try
                this_signal = data.PPG_11(2,:);
                this_timestamps  = data.PPG_11(1,:);
                this_annotations_timestamps = data.HeartBeat(1,:);   
            catch err
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    disp([this_signals_to_test{sigIdx},' signal not found']);
                    this_signal = NaN;
                else
                    disp(err.identifier);
                end
            end
            
        case 'ppg2'
            try
                this_signal = data.PPG_20(2,:);
                this_timestamps  = data.PPG_20(1,:);
                this_annotations_timestamps = data.HeartBeat(1,:);   
            catch err
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    disp([this_signals_to_test{sigIdx},' signal not found']);
                    this_signal = NaN;
                else
                    disp(err.identifier);
                end
            end
            
        case 'ppg3'
            try
                this_signal = data.PPG_21(2,:);
                this_timestamps  = data.PPG_21(1,:);
                this_annotations_timestamps = data.HeartBeat(1,:);   
            catch err
                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                    disp([this_signals_to_test{sigIdx},' signal not found']);
                    this_signal = NaN;
                else
                    disp(err.identifier);
                end
            end  
    end

    disp('hola')
    % correct timestamps to seconds 
    this_timestamps = (this_timestamps - this_timestamps(1))/1000;
    %this_timestamps = this_timestamps/1000;
    
    %%%% there is a problem with the annotations %%%%%%
    this_annotations_timestamps = (this_annotations_timestamps - this_timestamps(1))/1000;
    this_annotations_timestamps = this_annotations_timestamps(this_annotations_timestamps>0);
    this_annotations_value = this_signal(round(128*this_annotations_timestamps));
    this_detections_timestamps = feval(BD_algorithm,this_signal,algorithm_configuration{1});
    this_detections_values = this_signal(round(128*this_detections_timestamps));       

    figure('units','normalized','outerposition',[0 0 1 1]);hold on
    plot(this_annotations_timestamps,this_annotations_value,'or');
    plot(this_detections_timestamps,this_detections_values,'xg');
    legend({'Detection on Simband','Detection on SAMI'})
    plot(this_timestamps,this_signal);hold on;

    ylabel(list_of_signals{sigIdx});

    plotname = ['Beat Detection ',list_of_signals{sigIdx}];

    saveas(gcf,plotname,'fig');
    saveas(gcf,plotname,'png');

    close all

    rmpath(fullfile(pwd,algorithms_directory,BD_algorithm));
end

