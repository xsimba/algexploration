function test_results = do_test( algorithms_to_test,signals_to_test,input_parameters )
    
    datasets_to_test = {'SAMI'};  
    
    test_results = struct();
    
    % addpath 
    dataset_directory = 'DataBases';
    algorithms_directory = 'Algorithms';
        
    for algIdx = 1:numel(algorithms_to_test)
        
        addpath(fullfile(pwd,algorithms_directory,algorithms_to_test{algIdx}));
        this_datasets_to_test = datasets_to_test{1};
        
        for dsIdx = 1:numel(this_datasets_to_test)
           disp(['Testing ',algorithms_to_test{algIdx},'... ']);
           % looks for test files in the dataset directory 
           test_files = dir(fullfile(pwd,dataset_directory,this_datasets_to_test,'*.mat'));    
           
           % defines 
           this_dataset_detections = cell(1,numel(test_files));
           
           % looks for the signals to test
           this_signals_to_test = signals_to_test{algIdx}; 
           for sigIdx =1:numel(this_signals_to_test)
                for testIdx = 1:numel(test_files)
                    % load test file = variable name is "data"
                    

                    load(fullfile(pwd,dataset_directory,this_datasets_to_test,test_files(testIdx).name));
                    
                    this_signals_to_test = signals_to_test{algIdx};

                switch this_signals_to_test{sigIdx}
                
                    case 'ecg'
                        try
                            this_signal = data.ECG(2,:);
                            this_timestamps  = data.ECG(1,:);
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'ppg0'
                        try
                            this_signal = data.PPG_10(2,:);
                            this_timestamps  = data.PPG_10(1,:);
                        catch err
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                disp([this_signals_to_test{sigIdx},' signal not found']);
                                this_signal = NaN;
                            else
                                disp(err.identifier);
                            end
                        end

                    case 'ppg1'                            
                        try
                        this_signal = data.PPG_11(2,:);
                        this_timestamps  = data.PPG_11(1,:);
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                        end

                    case 'ppg2' 
                        try
                        this_signal = data.PPG_20(2,:);
                        this_timestamps  = data.PPG_20(1,:);                             
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                        end

                    case 'ppg3'                            
                        try
                            this_signal = data.PPG_21(2,:);
                            this_timestamps  = data.PPG_21(1,:);                              
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'accx'                            
                        try
                            this_signal = data.Accelerometer0(2,:);
                            this_timestamps = data.Accelerometer0(1,:);                            
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'accy'                            
                        try
                            this_signal = data.Accelerometer1(2,:);
                            this_timestamps = data.Accelerometer1(1,:);                     
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end

                    case 'accz'                            
                        try
                            this_signal = data.Accelerometer2(2,:);
                            this_timestamps = data.Accelerometer2(1,:);   
                        catch err
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                this_signal = NaN;
                            end
                        end
                            
                  end
                    
                    % runs the function here
                    detections_timestamps = feval(algorithms_to_test{algIdx},this_signal,input_parameters{algIdx});
                    
                    % stores raw_signal, timestamps and results
                    this_dataset_detections{testIdx} = detections_timestamps ;
                end
                
                test_results.(algorithms_to_test{algIdx}).(this_signals_to_test{sigIdx}).(this_datasets_to_test).detections = this_dataset_detections;
                
           end
        end
        rmpath(fullfile(pwd,algorithms_directory,algorithms_to_test{algIdx}));
    end
    % compute metrics
    disp('Done Testing .... Computing Metrics')
    test_results = compute_metrics( test_results );
    
end

