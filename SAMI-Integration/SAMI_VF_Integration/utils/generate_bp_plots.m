function plotnames = generate_beat_detection_plots(list_of_signals,data_directory)
        
    
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,data_directory,'*.mat'));    
           
    plotnames.signals = cell(numel(test_files),length(list_of_signals));
    plotnames.timestamps = cell(numel(test_files),length(list_of_signals));
    
    % looks for the signals to test
    for testIdx = 1:numel(test_files)
        load(fullfile(pwd,data_directory,test_files(testIdx).name));
        
        for sigIdx =1:numel(list_of_signals)

            switch list_of_signals{sigIdx}
                
                case 'ecg'
                    try
                        this_signal = data.ECG(2,:);
                        this_timestamps  = data.ECG(1,:);
                        this_annotatinos = data.ECG_heart_beat;
                    catch err
                        disp([this_signals_to_test{sigIdx},' signal not found']);
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            this_signal = NaN;
                        end
                    end

                case 'ppg0'
                    try
                        this_signal = data.PPG_10(2,:);
                        this_timestamps  = data.PPG_10(1,:);
                    catch err
                        if strcmpi(err.identifier,'MATLAB:nonExistentField')
                            disp([this_signals_to_test{sigIdx},' signal not found']);
                            this_signal = NaN;
                        else
                            disp(err.identifier);
                        end
                    end
                    
            end
        end
    end
    
end

