function metrics = compute_annotation_based_metrics_RMSE( all_detections,all_annotations )
    
    
    num_tests = numel(all_annotations);
    
    metrics.validity = 1;
    metrics.rmse = cell(1,num_tests);
    
    for fileIdx = 1:num_tests 
        try
            metrics.rmse{fileIdx} = sqrt(mean(all_detections{fileIdx}-all_annotations{fileIdx}).^2);
        catch err
            if isnan(all_annotations{fileIdx})
                metrics.rmse{fileIdx} = NaN;
                metrics.validity = 0 ;
            end
        end
    end

end

