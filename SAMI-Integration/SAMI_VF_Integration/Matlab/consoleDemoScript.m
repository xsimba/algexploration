%
% Research Console Test Script
%
% A. Khalak, 7/18/2014
 
%
% plug an auth token in here, can get this from the devices tab on 
% https://api.samihub.com/ 
%

clear all

acq = 'Ram' % | 'Ana';


if strcmpi(acq,'Ram')
    deviceToken = 'fbc35eac9f1d4850b03112801189b7dd';
    deviceID    = '771cf8655c3249ada22ea7c405e32eec';
    userID      = '34823184b0fc4f4b8b584db655ca44a0';
    startDate = '1358020800000';
    endDate = '1406228400000';
elseif strcmpi(acq,'Ana')
    deviceToken = '1def59e56d1d4a06acf85e3cd926d2cb';
    deviceID    = 'c1717a85481b4f2aa0a1b231a190594';
    userID      = '712dc68dea5044d9b9461c16e369fa62';
    endDate =  1406932020000;%
    offset = 36*60; % 36 minutes of data
    startDate = num2str(endDate - offset*1000);
    endDate = num2str(endDate);
end

tstart = tic;
data = getSimbandData(userID, deviceID, deviceToken, startDate, endDate);
tElapsed = toc(tstart);


if strcmpi(acq,'Ram')
    ss_ecg = findSections(data.ECG(2,:),data.ECG(1,:));
    ss_ppg_green_center = findSections(data.ppg_green_center,data.ppg_green_centerTime);
    ss_ppg_green_side = findSections(data.ppg_green_side,data.ppg_green_sideTime);
    ss_ppg_blue = findSections(data.ppg_blue,data.ppg_blueTime);
    ss_ppg_red = findSections(data.ppg_red,data.ppg_redTime);
elseif strcmpi(acq,'Ana')
    ss_ecg = findSections(data.ecg,data.ecgTime);
    ss_ppg_green_center = findSections(data.PPG_10(2,:),data.PPG_10(1,:));
    ss_ppg_green_side = findSections(data.PPG_11(2,:),data.PPG_11(1,:));
    ss_ppg_blue = findSections(data.PPG_20(2,:),data.PPG_20(1,:));
    ss_ppg_red = findSections(data.PPG_21(2,:),data.PPG_21(1,:));
end

