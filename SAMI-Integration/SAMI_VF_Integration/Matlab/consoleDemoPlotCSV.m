function consoleDemoPlotCSV(data);

t1 = data.time;
ss_ppg1_1 = data.PPG_10;
ss_ppg1_2 = data.PPG_11;
ss_ppg2_1 = data.PPG_20;
ss_ppg2_2 = data.PPG_21;



figure(2)
clf
ax1 = subplot(2,2,1);
plot(t1,ss_ppg1_1,'g')
title('PPG1.1')

ax2 = subplot(2,2,2);
plot(t1,ss_ppg1_2,'g')
title('PPG1.2')

ax3 = subplot(2,2,3);
plot(t1,ss_ppg2_1,'r')
title('PPG2.1')

ax4 = subplot(2,2,4);
plot(t1,ss_ppg2_2,'b')
title('PPG2.2')

linkaxes([ax1 ax2 ax3 ax4], 'x');

end

