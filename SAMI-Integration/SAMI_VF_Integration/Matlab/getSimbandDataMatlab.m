%
% get Simband data from Ram's demo
%
% Read simband data from SAMI using urlread2 and parse_json
%
% N. Davuluri, Samsung SSI, 7 July 2014
%

%
% add paths
% 
addpath('urlread2');
addpath('parseJson');
%
% plug an auth token in here, can get this from the devices tab on 
% https://api.samihub.com/ 
%
token = 'fbc35eac9f1d4850b03112801189b7dd';
sdid = '771cf8655c3249ada22ea7c405e32eec';
userID = 'f82abfdfbd594853b8ca21e2eb96932d';
startDate = '1325376000000';
dateObj = cdfepoch(now);
myDateNum = todatenum(dateObj);
endDateNum = int64(myDateNum - repmat(datenum('1970-1-1 00:00:00'),size(myDateNum))).*repmat(24*3600.0,size(myDateNum)) * 1000;
endDate = int2str(endDateNum);



% Create Authorization Request Header
str = ['bearer ', token];
header = http_createHeader('Authorization',str);

%
% initialize ecg dataset and timestamp
%
ecg=[];
ecgRaw = [];
ecgVisual = [];
ppg3Visual = [];
ppg1Visual = [];
ppg0Visual = [];
ppg2Visual = [];
ppg2 = [];
ppg1=[];
accelerometer = [];
heartBeat = [];
ecgHeartBeat = [];
pat = [];
hr = [];
ppgFilt1 = [];
ppgFilt2 = [];
ppgPre1 = [];
ppgPre2 = [];
hrv= [];
bp = [];
ecgFiltered = [];
offset = '';

urlString = 'https://portal.samihub.com/v1.1/historical/normalized/messages?uid=';
offsetString = '&offset=';
startDateString = '&startDate=';
endDateString = '&endDate=';
countString = '&count=1000';
url = [urlString userID startDateString startDate endDateString endDate countString];
[output,extras]= urlread2(url, 'GET', [],header);
data = parse_json(output);
sizeData = data.size;
nextParameter = data.next;
offset = nextParameter;

for i = 1:sizeData
    temp = data.data{i}.data.content;
    if isfield(temp, 'ECG');
        ecg = [ecg cell2mat(temp.ECG)];
    end
    if isfield(temp, 'ECGRaw');
        ecgRaw = [ecgRaw cell2mat(temp.ECGRaw)];
    end
    if isfield(temp, 'ECG_VISUAL');
        ecgVisual = [ecgVisual cell2mat(temp.ECG_VISUAL)];
    end
    if isfield(temp, 'PPG_3_VISUAL');
        ppg3Visual = [ppg3Visual cell2mat(temp.PPG_3_VISUAL)];
    end
    if isfield(temp, 'PPG_1_VISUAL');
        ppg1Visual = [ppg1Visual cell2mat(temp.PPG_1_VISUAL)];
    end
    if isfield(temp, 'PPG_0_VISUAL');
        ppg0Visual = [ppg0Visual cell2mat(temp.PPG_0_VISUAL)];
    end
    if isfield(temp, 'PPG_2_VISUAL');
        ppg2Visual = [ppg2Visual cell2mat(temp.PPG_2_VISUAL)];
    end
    if isfield(temp, 'PPG_2');
        ppg2 = [ppg2 cell2mat(temp.PPG_2)];
    end
    if isfield(temp, 'PPG_1');
        ppg1 = [ppg1 cell2mat(temp.PPG_1)];
    end
    if isfield(temp, 'Accelerometer');
        accelerometer = [accelerometer cell2mat(temp.Accelerometer)];
    end
    if isfield(temp, 'HeartBeat');
        heartBeat = [heartBeat cell2mat(temp.HeartBeat)];
    end
    if isfield(temp, 'ECGHeartBeat');
        ecgHeartBeat = [ecgHeartBeat cell2mat(temp.ECGHeartBeat)];
    end
    if isfield(temp, 'PAT');
        pat = [pat cell2mat(temp.PAT)];
    end
    if isfield(temp, 'HR');
        hr = [hr cell2mat(temp.HR)];
    end
    if isfield(temp, 'PPGFilt_1');
        ppgFilt1 = [ppgFilt1 cell2mat(temp.PPGFilt_1)];
    end
    if isfield(temp, 'PPGFilt_2');
        ppgFilt2 = [ppgFilt2 cell2mat(temp.PPGFilt_2)];
    end
    if isfield(temp, 'PPGPre_1');
        ppgPre1 = [ppgPre1 cell2mat(temp.PPGPre_1)];
    end
    if isfield(temp, 'PPGPre_2');
        ppgPre2 = [ppgPre2 cell2mat(temp.PPGPre_2)];
    end
    if isfield(temp, 'HRV');
        hrv = [hrv cell2mat(temp.HRV)];
    end
    if isfield(temp, 'BP');
        bp = [bp cell2mat(temp.BP)];
    end
end
    
        
ts = [];
count = 0;
while isfield(data,'next');
    
    url = [urlString userID startDateString startDate endDateString endDate offsetString offset countString];
    [output,extras] = urlread2(url,'GET',[],header);
    data = parse_json(output);
    offset = data.next;
    sizeData = data.size;
    count = count + 1
    %
    % march through offsets and pull/parse out data
    %
    for i=1:sizeData % Check ECG data
        temp=data.data{i}.data.content;
        if isfield(temp,'ECG');
            ecg = [ecg cell2mat(temp.ECG)];
            %ecg=[ecg cell2mat(temp.ecg)];            
        end
        if isfield(temp,'ECGRaw');
            ecgRaw = [ecgRaw cell2mat(temp.ECGRaw)];
        end
        if isfield(temp,'ECG_VISUAL');
            ecgVisual = [ecgVisual cell2mat(temp.ECG_VISUAL)];
        end
        if isfield(temp,'PPG_3_VISUAL');
            ppg3Visual = [ppg3Visual cell2mat(temp.PPG_3_VISUAL)];
        end
        if isfield(temp, 'PPG_1_VISUAL');
            ppg1Visual = [ppg1Visual cell2mat(temp.PPG_1_VISUAL)];
        end
        if isfield(temp, 'PPG_0_VISUAL');
            ppg0Visual = [ppg0Visual cell2mat(temp.PPG_0_VISUAL)];
        end
        if isfield(temp, 'PPG_2_VISUAL');
            ppg2Visual = [ppg2Visual cell2mat(temp.PPG_2_VISUAL)];
        end
        if isfield(temp, 'PPG_2');
            ppg2 = [ppg2 cell2mat(temp.PPG_2)];
        end
        if isfield(temp, 'PPG_1');
            ppg1 = [ppg1 cell2mat(temp.PPG_1)];
        end
        if isfield(temp, 'Accelerometer');
            accelerometer = [accelerometer cell2mat(temp.Accelerometer)];
        end
        if isfield(temp, 'HeartBeat');
            heartBeat = [heartBeat cell2mat(temp.HeartBeat)];
        end
        if isfield(temp, 'ECGHeartBeat');
            ecgHeartBeat = [ecgHeartBeat cell2mat(temp.ECGHeartBeat)];
        end
        if isfield(temp, 'PAT');
            pat = [pat cell2mat(temp.PAT)];
        end
        if isfield(temp, 'HR');
            hr = [hr cell2mat(temp.HR)];
        end
        if isfield(temp, 'PPGFilt_1');
            ppgFilt1 = [ppgFilt1 cell2mat(temp.PPGFilt_1)];
        end
        if isfield(temp, 'PPGFilt_2');
            ppgFilt2 = [ppgFilt2 cell2mat(temp.PPGFilt_2)];
        end
        if isfield(temp, 'PPGPre_1');
            ppgPre1 = [ppgPre1 cell2mat(temp.PPGPre_1)];
        end
        if isfield(temp, 'PPGPre_2');
            ppgPre2 = [ppgPre2 cell2mat(temp.PPGPre_2)];
        end
        if isfield(temp, 'HRV');
            hrv = [hrv cell2mat(temp.HRV)];
        end
        if isfield(temp, 'BP');
            bp = [bp cell2mat(temp.BP)];
        end
        if isfield(temp, 'ECGFiltered');
            ecgFiltered = [ecgFiltered cell2mat(temp.ECGFiltered)];
        end
    end
end
