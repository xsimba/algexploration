function [psdx_ref] = setReferenceSignal(reference_signal)
%computes fft and power spectrum of the reference signal specified by the
%user
%In order to plot fft and power spectrum use the following code


%f = ecgFreq/2*linspace(0,1,NFFT/2+1);
%figure
%subplot(2,1,1)
%plot(f,2*abs(Y_ref(1:NFFT/2+1))) 
%title('Single-Sided Amplitude Spectrum of reference signal')
%xlabel('Frequency (Hz)')
%ylabel('|Reference|')
%freq = 0:ecgFreq/reference_length:ecgFreq/2;
%subplot(2,1,2)
%plot(freq,10*log10(psdx_ref)); grid on;
%title('Reference Signal Periodogram Using FFT');
%xlabel('Frequency (Hz)'); ylabel('Power/Frequency(dB/Hz)');

    reference_length = length(reference_signal);
    ecgFreq = 128;
    %Reference fft
    NFFT = 2^nextpow2(reference_length);
    Y_ref = fft(reference_signal,NFFT)/reference_length;
    %Reference power spectrum
    y_spectrum = Y_ref(1:reference_length/2+1);
    psdx_ref = (1/(ecgFreq*reference_length)).*abs(y_spectrum).^2;
    psdx_ref(2:end-1) = 2*psdx_ref(2:end-1);
end
