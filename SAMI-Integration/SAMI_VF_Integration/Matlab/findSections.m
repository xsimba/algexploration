% Function to analyze Simba data and break into valid sections
function [sectionStruct] = findSections(values, timestamps)

    % todo: bring in sampFreq from SAMI
    sampFreq = 128;

    deltaTime = diff(timestamps) - (1000/sampFreq)*(60*1000);

    ins = find(deltaTime > 0);
    sectionStruct = struct([]);
    thisStruct = struct([]);
    old_index = 1;
    for i = 1:length(ins)
        new_index = ins(1,i);
        thisStruct = struct('section',values(old_index:new_index),'time',timestamps(old_index:new_index),'samiPointer',[timestamps(old_index) timestamps(new_index)]);
        sectionStruct = [sectionStruct; thisStruct];  
        old_index = new_index+1;
    end
    
    thisStruct = struct('section',values(old_index:end),'time',timestamps(old_index:end),'samiPointer',[timestamps(old_index) timestamps(end)]);
    sectionStruct = [sectionStruct; thisStruct];
    
end