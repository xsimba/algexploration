function consoleDemoPlot(data)

ss_ppg_green_center = findSections(data.ppg_green_center(2,:), data.ppg_green_center(1,:));
ss_ppg_green_side = findSections(data.ppg_green_side(2,:), data.ppg_green_side(1,:));
ss_ppg_blue = findSections(data.ppg_blue(2,:), data.ppg_blue(1,:));
ss_ppg_red = findSections(data.ppg_red(2,:), data.ppg_red(1,:));

ss_ppg_10 = findSections(data.PPG_10(2,:), data.PPG_10(1,:));
ss_ppg_11 = findSections(data.PPG_11(2,:), data.PPG_11(1,:));
ss_ppg_20 = findSections(data.PPG_20(2,:), data.PPG_20(1,:));
ss_ppg_21 = findSections(data.PPG_21(2,:), data.PPG_21(1,:));

%Plot by sections

for sectNum = 1:length(ss_ppg_green_center),
    size_secs = (length(ss_ppg_green_center(sectNum).section)-1)/128;
    t1_green_center = 0:(1/128):size_secs;
    size_secs = (length(ss_ppg_10(sectNum).section)-1)/128;
    t2_10 = 0:(1/128):size_secs;
    
    size_secs = (length(ss_ppg_green_side(sectNum).section)-1)/128;
    t1_green_side = 0:(1/128):size_secs;
    size_secs = (length(ss_ppg_11(sectNum).section)-1)/128;
    t2_11 = 0:(1/128):size_secs;
    
    size_secs = (length(ss_ppg_blue(sectNum).section)-1)/128;
    t1_blue = 0:(1/128):size_secs;
    size_secs = (length(ss_ppg_20(sectNum).section)-1)/128;
    t2_20 = 0:(1/128):size_secs;
    
    size_secs = (length(ss_ppg_red(sectNum).section)-1)/128;
    t1_red = 0:(1/128):size_secs;
    size_secs = (length(ss_ppg_21(sectNum).section)-1)/128;
    t2_21 = 0:(1/128):size_secs;
    
    figure (1)
    clf
    ax1 = subplot(2,2,1);
    plot(t2_10,ss_ppg_10(sectNum).section,'k','LineWidth',2);hold on
    plot(t1_green_center,ss_ppg_green_center(sectNum).section,'g')
    title(['PPG Green Center, Section', num2str(sectNum)])
    
    ax2 = subplot(2,2,2);
    plot(t2_11,ss_ppg_11(sectNum).section,'k','LineWidth',2);hold on
    plot(t1_green_side,ss_ppg_green_side(sectNum).section,'g')
    title(['PPG Green side, Section ', num2str(sectNum)])

    ax3 = subplot(2,2,3);
    plot(t2_20,ss_ppg_20(sectNum).section,'k','LineWidth',2);hold on
    plot(t1_blue,ss_ppg_blue(sectNum).section,'b')
    title(['PPG Blue, Section ', num2str(sectNum)])

    ax4 = subplot(2,2,4);
    plot(t2_21,ss_ppg_21(sectNum).section,'k','LineWidth',2);hold on
    plot(t1_red,ss_ppg_red(sectNum).section,'r')
    title(['PPG Red, Section ', num2str(sectNum)])

    linkaxes([ax1 ax2 ax3 ax4], 'x');
    
    pause;
end 