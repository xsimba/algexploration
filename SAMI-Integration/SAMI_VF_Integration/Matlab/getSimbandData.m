function data = getSimbandData(uid, did, dtoken, sdate, edate)

%   
%
%getSimbandData(uid, did, dtoken, sdate, edate)
%
%Input: user id, device id, device token, start date and end date
%Retrieves data from SAMI and saves data in .mat file

fullfile('Databases','SAMI','712dcc171714069298600001406932020000')
    if ~(exist(fullfile('DataBases','SAMI',[uid(1:5), did(1:5), sdate, edate]),'file')==2)

        python_command = '/Users/pcasale/Library/Enthought/Canopy_64bit/User/bin/python ';
        python_code = fullfile('.','DataAccessLayer','getSamiData.py');
        s1 = ' --uid ' ;
        s2 = ' --did ';
        s3 = ' --dtoken ';
        s4 = ' --sdate ';
        s5 = ' --edate ';
        string = [python_command python_code s1 uid s2 did s3 dtoken s4 sdate s5 edate];
        system (string);
        disp(string)
        data = load('MatlabTransfer.mat');
        save(fullfile('DataBases','SAMI',[uid(1:5), did(1:5), sdate, edate]),'data');
        delete('MatlabTransfer.mat')
    else
        disp('File Exists');
    end
end

%todo: come up with a caching mechanism to automatically know if we are
%looking for data that is already locally available.