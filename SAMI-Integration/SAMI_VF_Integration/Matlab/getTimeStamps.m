function [startDate, endDate] = getTimeStamps(lookback)

    if ~exist('lookback'),
        lookback = 60
    end

    dateNumTime = now;
    endTime = (dateNumTime-repmat(datenum('1970-1-1 00:00:00'), ...
        size(dateNumTime))).*repmat(24*3600.0,size(dateNumTime));
    endTime = endTime + 7*60*60;
    endDate = int2str(endTime*1000);
    %30 mins in milliseconds
    sinceCurrentTime = lookback*60*1000; %what does 30 mins mean in datenum format (0.0209)
    startTime = endTime*1000 - sinceCurrentTime;
    startDate = int2str(startTime);
end
    