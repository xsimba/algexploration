%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : BDunitTest.m
% Content   : Matlab script for unit test of BD (wrapper for beat detection algorithm) 
% Version   : GIT 1.1 (modified data access below)
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

close all
clear all
clc

%
% point to rest recordings data
%
data_path = '\\105.140.2.7\share\SimbandDataArea\Rest Recordings - Reference Simba Data\SRC Data + annotations';
load([data_path, filesep, 'Participant_8_wb1_lon.mat']);

% ECG test
[BeatsTimestamps_ECG,Debug_ECG] = BD(ECG, 'ECG');
  
% Debug plots
figure; hold on;
plot(Debug_ECG.InputSamples, 'k');
plot(Debug_ECG.FindPeaks_InputSamples, 'b');
plot(Debug_ECG.FindPeaks_threshold, 'r');
plot(Debug_ECG.FindPeaks_max, 'm');
plot(Debug_ECG.FindPeaks_holdoffcounter/10, '*k');
plot(Debug_ECG.FindPeaks_maxindx/10, 'ob');

plot(Debug_ECG.FindPeaks_InputSamples./(Debug_ECG.FindPeaks_edge ==  1), '^b');
plot(Debug_ECG.FindPeaks_InputSamples./(Debug_ECG.FindPeaks_edge == -1), 'vb');

legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('ECG debug');

figure;hold on;
plot((0:length(Debug_ECG.InputSamples)-1)/Debug_ECG.Fs,Debug_ECG.InputSamples,'b');
BeatAmp = interp1((0:length(Debug_ECG.InputSamples)-1)/Debug_ECG.Fs,Debug_ECG.InputSamples,BeatsTimestamps_ECG/Debug_ECG.FreqTS);
plot(BeatsTimestamps_ECG/Debug_ECG.FreqTS,BeatAmp,'or');
title('ECG beat detection');


% PPG test

% PPG input samples should represent pressure (-ve of light intensity) so a sign flip may be necessary
%PPG2G = -PPG2G;

[BeatsTimestamps_PPG2G,Debug_PPG2G] = BD(PPG2G, 'PPG');

% Debug plots
figure; hold on;
plot(Debug_PPG2G.InputSamples-mean(Debug_PPG2G.InputSamples), 'k');
plot(Debug_PPG2G.FindPeaks_InputSamples, 'b');
plot(Debug_PPG2G.FindPeaks_threshold, 'r');
plot(Debug_PPG2G.FindPeaks_max, 'm');
plot(Debug_PPG2G.FindPeaks_holdoffcounter/10, '*k');
plot(Debug_PPG2G.FindPeaks_maxindx/10, 'ob');

plot(Debug_PPG2G.FindPeaks_InputSamples./(Debug_PPG2G.FindPeaks_edge ==  1), '^b');
plot(Debug_PPG2G.FindPeaks_InputSamples./(Debug_PPG2G.FindPeaks_edge == -1), 'vb');

legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('PPG debug');

figure;hold on;
plot((0:length(Debug_PPG2G.InputSamples)-1)/Debug_PPG2G.Fs,Debug_PPG2G.InputSamples,'b');
BeatAmp = interp1((0:length(Debug_PPG2G.InputSamples)-1)/Debug_PPG2G.Fs,Debug_PPG2G.InputSamples,BeatsTimestamps_PPG2G/Debug_PPG2G.FreqTS);
plot(BeatsTimestamps_PPG2G/Debug_PPG2G.FreqTS,BeatAmp,'or');
title('PPG up stroke detection');

BeatsTimestamps_PPG2G;