% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : BD.m
%  Content    : Wrapper for FindBeats.m
%  Package    : SIMBA.Validation
%  Created by : P. Casale (pierluigi.casale@imec-nl.nl)
%  Date       : 26-04-2014
%  Modification and Version History: 
%  | Developer | Version |    Date   | 
%  |  pcasale  |   1.0   | 08-05-2014|
%  |  pcasale  |   1.1   | 14-05-2014|
%  |  ayoung   |   1.2   | 21-05-2014|
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [BeatsTimestamps,Debug] = BD( InputStream, SigType )
% Beat Detection
%   Inputs:
%      InputStream     :  Signal
%      SigType         : 'ECG' | 'PPG'
%   Output:
%      BeatsTimestamps : timestamps of detected beats
%   Usage Example:
%      SigType = 'ECG'
%      BeatsTimestamps = BD( InputStream, SigType )
    
    dims = size(InputStream);
    if dims(1)>dims(2)
        InputStream = InputStream';
    end
    
    % General
    Fs                            = 128 ;                                  % Sampling Frequency
    FreqTS                        = 32768;                                 % Timestamp clock frequency
    BlockSize                     = round(Fs*0.5);                         % 500 ms blocks
    Params.ts2samRatio            = FreqTS/Fs;                             % Ratio of timestamp to sample frequency
    
    if strcmpi(SigType,'ECG')
        SigTypeN                      = 0 ;
        Params.attack                 = 1 - exp(-1000/(1.8055*Fs));        % 1.8055 ms
        Params.decay                  = 1 - exp(-1000/(466.34*Fs));        % 466.34 ms
        Params.gain                   = 0.55 ;                             % Scaling for peak tracking to threshold
        Params.StdScale               = 0.75 ;                             % Scaling for standard deviation to offset
        Params.OffUpdateFblk          = round(Fs*   2.50/BlockSize);       % 2.50 s  Offset update fast number of blocks given duration
        Params.OffUpdateFast          = 1 - exp(-1/(0.50*Fs/BlockSize));   % 0.50 s  Offset update (fast, during first 5 sample blocks)
        Params.OffUpdateSlow          = 1 - exp(-1/(5.00*Fs/BlockSize));   % 5.00 s  Offset update (slow, after first 5 sample blocks)
        Params.holdoffSamples         = round(100.0*Fs/1000);              % Hold-off duration 100.0 ms
        Params.GroupDelay             = 13;                                % (symmetric) filter delay, 13 = (27-1)/2
        Params.CWT_FIR                = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv');  % 0: Load ECG  CWT coefficients
        
    elseif strcmpi(SigType,'PPG')
        SigTypeN                      = 1 ;
        Params.attack                 = 1 - exp(-1000/(1.7000*Fs));        % 1.7000 ms
        Params.decay                  = 1 - exp(-1000/(450.00*Fs));        % 450.00 ms
        Params.gain                   = 0.60;                              % Scaling for peak tracking to threshold
        Params.StdScale               = 0.70;                              % Scaling for standard deviation to offset
        Params.OffUpdateFblk          = round(Fs*   2.50/BlockSize);       % 2.50 s  Offset update fast number of blocks given duration
        Params.OffUpdateFast          = 1 - exp(-1/(0.50*Fs/BlockSize));   % 0.50 s  Offset update (fast, during first 5 sample blocks)
        Params.OffUpdateSlow          = 1 - exp(-1/(5.00*Fs/BlockSize));   % 5.00 s  Offset update (slow, after first 5 sample blocks)
        Params.holdoffSamples         = round(450.0*Fs/1000);              % 200 Hold-off duration 200.0 ms
        Params.GroupDelay             = 61;                                % Filter delay
        Params.CWT_FIR                = importdata('Wavelets/Coeffs/PPGwavelet8_128HzDiffLPF.csv');  % 1: Load PPG  CWT coefficients
    end

    Nblocks = floor(length(InputStream)/BlockSize);

    State = []; 
    BeatsTimestamps =[];
    
    % Debug
    FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
    FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
    FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
    FindPeaks_max             = zeros(1,BlockSize*Nblocks);
    FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
    FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
    FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);
    
    % Filter and replace input data (test)
%     Fpass = 15;  Apass = 0.1; % Passband Frequency (Hz) and ripple (dB)
%     Fstop = 20;  Astop = 60;  % Stopband Frequency (Hz) and attenuation
%     h = fdesign.lowpass('fp,fst,ap,ast', Fpass, Fstop, Apass, Astop, Fs);
%     Hd = design(h, 'equiripple', 'MinOrder', 'any', 'StopbandShape', 'flat');
%     InputStream = filter(Hd, InputStream);

    for Blk = 0:Nblocks-1
        Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
        InputSamples = InputStream(Blk*BlockSize+1:Blk*BlockSize+BlockSize); 
        [~, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigTypeN);
        BeatsTimestamps = [BeatsTimestamps BeatTimestamps]; 
        
        % Concatenate debug data
        FindPeaks_InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
        FindPeaks_cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
        FindPeaks_threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
        FindPeaks_max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
        FindPeaks_edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
        FindPeaks_holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
        FindPeaks_maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
    end
    
    Debug.InputSamples = InputStream;
    Debug.FindPeaks_InputSamples = FindPeaks_InputSamples;
    Debug.FindPeaks_cmp=FindPeaks_cmp;
    Debug.FindPeaks_threshold=FindPeaks_threshold;
    Debug.FindPeaks_max=FindPeaks_max;
    Debug.FindPeaks_edge=FindPeaks_edge;
    Debug.FindPeaks_holdoffcounter =FindPeaks_holdoffcounter;
    Debug.FindPeaks_maxindx = FindPeaks_maxindx;
    Debug.FreqTS = FreqTS;
    Debug.Fs = Fs;
end

