function [d0, dl] = DiffWavelet(xd, xl, pr, fs)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : DiffWavelet.m
% Content   : Matlab function for wavelet generation based on single asymmetric pulse parameters with differentiation
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

%  xd: Width of single PPG pulse (s)
%  xl: Start to peak interval (s)
%  pr: Sharpness, raise basic function to power of pr
%  fs: sample frequency (Hz)
%  d0: Wavelet coefficients (normalised)
%  dl: Estimated filter delay (samples)

% Example use:
% [d0, dl] = DiffWavelet(0.550, 0.125, 1.000, 100);

nc = floor(xd*fs-1);
xm = -pi+2*pi*xl/xd;
%dl = nc - round(nc*xl/xd); % Estimated filter delay
dl = nc - nc*xl/xd; % Estimated filter delay
k = -sin(xm)/(1+cos(xm));

%xx = pi*(nc-2*(0:nc-1)-1)/(nc+1); % Map -1.0:nc     to +pi:-pi
xx = pi*(nc-1-2*(0:nc-1))/nc;      % Map -0.5:nc-0.5 to +pi:-pi as we have to find zero mean (+/-0.5 samples)

% Cull near zero coefficients, search for sample time offset for zero mean, repeat with new limits if necessary.
NZdetLim = 1e-6;
Zfound = 0;
while(Zfound == 0)
    % Cull near zero coefficients
    x = xx;
    d0 = -pr*exp(-k*pr*x).*((1+cos(x)).^(pr-1)).*(sin(x)+k+k*cos(x));
    d0 = d0/max(abs(d0)); % Normalise
    nz = (abs(d0)<NZdetLim); % Near zero detection
    zi = find(diff(nz)==-1,1); % Detect transition from 1 to 0
    
    % Adjust sampling time offset to obtain zero mean
    disp(['Adjusting sampling time offset to obtain zero mean, using limit = ',num2str(NZdetLim)]);
    sd0 = zeros(100001,1);
    for n=-50000:50000
        u = n/100000;
        uu = -u*pi*2/nc; % x = pi*(nc-1-2*((0:nc-1)+u))/nc;
        x = xx+uu;
        
        d0 = -pr*exp(-k*pr*x).*((1+cos(x)).^(pr-1)).*(sin(x)+k+k*cos(x));
        d0 = d0.*(abs(x)<pi);
        
        if (~isempty(zi)), d0(1:zi) = 0; end % Set culled coefficients to zero
        
        sd0(n+50001) = sum(d0);
    end
    
    xs = (-50000:50000)/100000;
    sf = diff(sign(sd0));
    zz = find(abs(sf)==1); % Find any exact zeros
    while(~isempty(zz))
        if (zz(2)-zz(1)~=1), warning('Sanity check failed, zz(2)-zz(1)~=1'); end
        sf(zz(1))=2;
        sf(zz(2))=0;
        zz = find(abs(sf)==1); % search again for next round
    end
    
    zc = find(sf~=0); % Find sign flips
    figure;hold on;plot(xs,sd0);plot(xs(zc+1),sd0(zc+1),'ro');grid on;title(['Zero mean search, limit = ',num2str(NZdetLim)]);xlabel('Sample offset');
    
    % Find flip closest to zero
    [~,idx] = min(abs(zc-50001));
    idx = zc(idx)+1; % zc points to sample before nearest zero crossing
    if (~isempty(idx)), Zfound = 1;
    else                NZdetLim = NZdetLim * 0.1; % Reduce limit by factor of 10
    end
end % while

u = (idx-50001)/100000;
x = pi*(nc-1-2*((0:nc-1)+u))/nc;
d0 = -pr*exp(-k*pr*x).*((1+cos(x)).^(pr-1)).*(sin(x)+k+k*cos(x)).*(abs(x)<pi); % Final coeff calculation

if (~isempty(zi)) % Now really cull those coefficients to zero and compensate dl and nc
    d0 = d0(zi+1:end);
    dl = dl - zi;
    nc = nc - zi;
end

d0 = d0/max(abs(d0)); % Normalise

disp(['xd = ',num2str(xd),', xl = ',num2str(xl),', pr = ',num2str(pr),', fs = ',num2str(fs),' Hz']);
disp(['Mean value = ',num2str(mean(d0),8),' at sample offset = ',num2str(u,8)]);
rev = fliplr(d0); % Time reversed (correct direction)
pls = cumsum(rev); pls = pls/max(abs(pls)); % Summation and normalisation
figure;hold on;grid on;plot(rev,'-bo');plot(pls,'-ro');title(['Pulse and derivative: xd = ',num2str(xd),', xl = ',num2str(xl),', pr = ',num2str(pr)]);
xlabel('Samples');ylabel('Amplitude (normalised)');legend('Reversed derivative','Summation');

flg = fft([d0 zeros(1,16384-nc)]);
mlg = max(abs(flg));
lflg = 20*log10(abs(flg(1:8193))/mlg);
figure;plot((0:8192)*fs/16384,max(lflg,-180));grid on;xlabel('Frequency (Hz)');ylabel('Magnitude (dB)');title('High resolution spectrum');

end

