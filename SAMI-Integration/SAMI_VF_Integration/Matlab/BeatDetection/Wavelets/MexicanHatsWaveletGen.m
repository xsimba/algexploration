function ym = MexicanHatsWaveletGen(Fstd, Fs, Ncoef, Shape, MinVal)
%--------------------------------------------------------------------------------
% Project   : (legacy) ECG/BAN
% Filename  : MexicanHatsWaveletGen.m
% Content   : Matlab function for generation of mexican hat or first derivative wavelets for beat detection 
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% Mexican hats coefficient generation
% Fstd:   Standard frequency for wavelet (not necessarily the peak response)
% Fs:     Sample frequency (Hz)
% Ncoef:  Number of coefficients
% Minval: Minimum value of wavelet waveform
% Shape:  0 Variable width Mexican hat (min. value used), double exponential (symmetric)
%         1 Variable width Mexican hat (min. value used), polynomial exponential (symmetric)
%         2 Standard Mexican hat (symmetric)
%         3 First derivative of a Gaussian (equal positive and negative excursions, anti-symmetric)

% Example use:
% ym = MexicanHatsWaveletGen(14.3, 128, 28, 3, 0.0);

    a = Fs*sqrt(10)/(4*pi*Fstd); % Wavelet scaling, a = Fs * sqrt(10) / (4 * pi * Fstd)
    x = -(Ncoef-1)/2:1:(Ncoef-1)/2; % Time series, for odd and even number of coefficients
    xx = (x/a).^2; % Time series scaled and squared (as is commonly used)
    
    if (Shape == 0) % Variable width mexican hat lobes
        if ((MinVal > -0.04) || (MinVal < -0.44)), error('MinVal must be between -0.44 and -0.04 !'); end; % Get b from interpolation of LUT
        b = interp1(-0.04:-0.02:-0.44, [0.04066 0.06198 0.08433 0.10795 0.13313 0.16018 0.18950 0.22154 0.25689 0.29630 0.34077 0.39165 0.45083 0.52110 0.60670 0.71453 0.85685 1.05801 1.37618 2.00518 4.66095], MinVal);
        lb = log((b+1)/b);
        bb = b^2*lb/(2*b+1);
        ym = exp(-xx*bb).*((b+1)*exp(-xx*lb)-b); % Variable width mexican hat (MinVal = -0.44 for mex hat)
    elseif (Shape == 1) % Variable width polyexp lobes
        if ((MinVal > -0.05) || (MinVal < -0.5)), error('MinVal must be between -0.5 and -0.05 !'); end;
        if (MinVal > -0.255), warning('MinVal values > -0.255 cause an extra sub-lobe to appear.'); end;
        d = -1/9 -0.2489827261*MinVal;
        ym = (d*xx.^3 - 7*d*xx.^2 + (6*d-1)*xx + 1).*exp(-xx/2); % Variable width polyexp (MinVal = -0.446 for mex hat)
    elseif (Shape == 2), ym =   (1-xx).*exp(-xx/2); % Mexican hat
    else                 ym = (-x/a^2).*exp(-xx/2); % First derivative
    end
    
    % DC removal and normalisation
    taper = 20; % Percentage of coefficients to use for taper (use half at start and half at end)
    if (taper > 0)
        Tcoef = max(round(Ncoef*taper/200),4); % Number of coefficients to use at start and at end, minimum 4.
        Tfun  = (1-cos((1:Tcoef)*pi/(Tcoef+1)))/2;
        Ttmpl = ones(1,Ncoef);                 % Unity template
        Ttmpl(1:Tcoef) = Tfun;                 % Apply taper to start
        Ttmpl(end-Tcoef+1:end) = fliplr(Tfun); % Apply taper to end
        TtmplMean = mean(Ttmpl); % Mean of tapered template
        ym = ym.*Ttmpl; % Apply template to wavelet
        TcoefMean = mean(ym); % Mean of tapered coefficients
        ym = ym - Ttmpl*TcoefMean/TtmplMean; % Subtract template from coef so that mean values cancel
    else
        ym = ym - mean(ym); % Remove average value
    end
    
    % Normalisation
    ym = ym/max(abs(ym));
end