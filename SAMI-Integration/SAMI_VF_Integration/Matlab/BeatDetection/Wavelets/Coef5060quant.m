function Coef = Coef5060quant(Fs, ym, Quant, StopFreq, Supp50, Supp60, NotchWidth, plts)
%--------------------------------------------------------------------------------
% Project   : (legacy) ECG/BAN
% Filename  : Coef5060quant.m
% Content   : Matlab function for coefficient manipulation for frequency suppression and quantisation 
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% Wavelet coefficient processor for 50 and/or 60 Hz suppression and quantisation
% Author: Alex Young, IMEC-NL
% Start date: 3 SEP 2010

% Var         Range           Type               Description
% Fs          1:50000         Float              Sampling frequency in Hz
% ym          -               Float vector       Input wavelet coefficients
% StopFreq    10 to Fs/2      Float              Start of stop band frequency (Hz)
% Quant       0 or 7:8388607  Unsigned integer   0 = Double float, otherwise maximum positive integer representation of coefficients
% Supp50      0 or 1          Boolean            0 = Don't suppress 50Hz, 1 = manipulate zero to suppress 50Hz component
% Supp60      0 or 1          Boolean            0 = Don't suppress 60Hz, 1 = manipulate zero to suppress 60Hz component
% NotchWidth  0.5 to 5.0      Float              Width of notches for 50 and 60 Hz at fundamental
% plts:       0 or 1          Boolean            0 = no plots, 1 = plot intermediate results

% Output      Range           Type               Description
% Coef        -1.0 to 1.0     Float              For Quant  = 0, normalised wavelet coefficient set
% Coef        -Quant:Quant    Signed integer     For Quant ~= 0, scaled and quantised wavelet coefficient set


%% Input verification
if ((Fs         < 1   ) || (Fs          > 50000 )),         error('Sample frequency out of range, expecting value between 1Hz and 50kHz'); end;
if ((StopFreq   < 10.0) || (StopFreq    > Fs/2)),           error('StopFreq out of range, expecting value between 10Hz and Fs/2');         end;
if ((NotchWidth < 0.5 ) || (NotchWidth  > 5.0)),            error('MinVal out of range, expecting value between -1.0 and -0.0');           end;
if ((Quant  ~= 0) && ((Quant  < 7) || (Quant > 8388607))),  error('Quant out of range, expecting value between 7 and 8388607 or 0');       end;

% Method: Identify maximum weighted component in frequency domain and subtract in time domain.

Ncoef = length(ym);

flg = fft([ym zeros(1,16384-Ncoef)]);
nlg = flg(1:8193)/max(abs(flg)); % Normalise and cull negative frequencies
alg = abs(nlg);

if (plts == 1)
    xx=(-(Ncoef-1)/2:(Ncoef-1)/2)/Fs;
    figure;plot(xx*1000,ym);grid on;title('Wavelet coefficients');xlabel('Time (ms)');ylabel('Amplitude');
    figure;plot((0:8192)*Fs/16384,max(20*log10(alg),-120));grid on;xlabel('Frequency (Hz)');ylabel('Magnitude (dB)');title('High resolution spectrum (unmodified)');
end

% rc = (1-cos((1:Ncoef)*2*pi/(Ncoef+1)))/2;   % Raised cosine (include first and last points). 2pi mapped from -1 to Ncoef+1
rc = ones(1,length(Ncoef)); % Unity
TtmplMean = mean(rc); % Mean of RC template

AttenMains = 20.0; % Attenuate 50 and/or 60 Hz by this extra amount.
MinStopGain = -20.0; % Starting value of minimum stop band gain

idxFrS = floor(StopFreq*16384/Fs) + 1; % Index for start of stop band frequency

% 50 and 60 Hz
% Find index for 50 Hz +/- NotchWidth/2 and 60 Hz +/- NotchWidth/2 and harmonics
spgain = ones(1,8193); % Spectral gain
for n = 1:floor(Fs/(2*50))
    idxFrA = floor(n*(50-NotchWidth/2)*16384/Fs) + 1; % Indexes for 50 Hz notch
    idxFrB = ceil(n*(50+NotchWidth/2)*16384/Fs) + 1;
    idxFrC = floor(n*(60-NotchWidth/2)*16384/Fs) + 1; % Indexes for 60 Hz notch
    idxFrD = ceil(n*(60+NotchWidth/2)*16384/Fs) + 1;
    
    Gain = max((10^(AttenMains/20))/n,1); % Weight according to 1/n with minimum value of 1.0
    
    if ((Supp50 == 1)&&(idxFrA<8194)), spgain(idxFrA:min(idxFrB,8193)) = Gain; end
    if ((Supp60 == 1)&&(idxFrC<8194)), spgain(idxFrC:min(idxFrD,8193)) = Gain; end
end

if (plts == 1), figure;plot((0:8192)*Fs/16384,spgain);title('Spectral gain'); ylim([-0.1 10^(AttenMains/20)+0.1]); end

yym = ym;
yymbest = ym;
if (plts == 1), figure; end
for iter = 1:10000
    [~,idxm] = max(alg(idxFrS:end).*spgain(idxFrS:end)); % Find maximum value in stop band including gain
    idx = idxFrS+idxm-1;
    
    f = (idx-1)*Fs/16384; % Maximum detected at frequency
    ca = flg(idx);        % Complex amplitude
    
    x = (0:Ncoef-1)*2*pi*f/Fs;
    y = (cos(x)*real(ca) - sin(x)*imag(ca)).*rc*2/Ncoef;
    yym = yym - y*0.33; % Subtract frequency (33%)
    TcoefMean = mean(yym); % Mean of tapered coefficients
    yym = yym - rc*TcoefMean/TtmplMean; % Subtract template from coef so that mean values cancel
    
    flg = fft([yym zeros(1,16384-Ncoef)]);
    nlg = flg(1:8193)/max(abs(flg)); % Normalise and cull negative frequencies
    alg = abs(nlg);
    
    llg = 20*log10(alg);
    StopBandGain  = max(llg(idxFrS:end)); % Stop band max response
    if (StopBandGain < MinStopGain)
        MinStopGain = StopBandGain; % Update minimum stop gain
        yymbest = yym;              % Keep best result
    end
    
    if (plts == 1)
        subplot(2,2,3);
        plot((0:8192)*Fs/16384,max(llg,-160));hold on;grid on;xlabel('Frequency (Hz)');ylabel('Magnitude (dB)');
        title(['Response: stop band rejection: ', num2str(StopBandGain,4), ' dB']);
        line([StopFreq,Fs/2],[StopBandGain,  StopBandGain], 'Color','r');
        ylim([-160 1]); xlim([-1 Fs/2]);
        hold off;
        drawnow;
    end
end

yym = yymbest;

if (plts == 1) % Update plot with best result
    flg = fft([yym zeros(1,16384-Ncoef)]);
    nlg = flg(1:8193)/max(abs(flg)); % Normalise and cull negative frequencies
    alg = abs(nlg);
    subplot(2,2,3);
    llg = 20*log10(alg);
    plot((0:8192)*Fs/16384,max(llg,-160));hold on;grid on;xlabel('Frequency (Hz)');ylabel('Magnitude (dB)');
    title(['Response: best stop band rejection: ', num2str(StopBandGain,4), ' dB']);
    line([49.5,Fs/2],[StopBandGain,  StopBandGain], 'Color','r');
    ylim([-160 1]); xlim([-1 Fs/2]);
end

% Normalise
ys = yym/max(abs(yym));

% Coefficient analysis
MaxOutSat = sum(abs(ys)); % Maximum output when input is saturated to sign of coefficients (but then input is distorted)
MaxOutAut = sum(ys.^2);   % Maximum output when input is identical to coefficients (ideal waveform at full scale)
disp(['Absolute maximum filter response to saturated input: ',num2str(MaxOutSat,6),' (',num2str(20*log10(MaxOutSat),4),'dB)']);
disp(['Absolute maximum filter response to coefficient input: ',num2str(MaxOutAut,6),' (',num2str(20*log10(MaxOutAut),4),'dB)']);

%% Quantisation
z = zeros(1,Quant);
if (Quant > 0),
    Quant = floor(Quant) + 0.4999; % Adjust quantisation value to highest posible
    for h = 1:Quant % Scan scaling values from Quant to Quant*0.99 in steps of -0.01 so that DC frequency response is zero
        sc = Quant - (h-1)/100;
        yq = round(ys.*sc);
        zz = abs(sum(yq));
        z(h) = zz;
        if (zz == 0), break; end;
    end;
    if (zz ~= 0), % In case zero DC gain scaling not found
        [zz, h] = min(z); % Find minimum
        sc = Quant - (h-1)/100;
        yq = round(ys.*sc);
        DCgain = zz/Quant;
        disp(['Unable to find scaling to cause DC gain to be zero, best value is ', num2str(20*log10(DCgain)), 'dB']);
    end;
    
    Seq = 1:Ncoef;
    SeqNonZero = Seq(abs(yq) > 0);
    StartCoef = min(SeqNonZero);
    if (StartCoef > 1),
%         yq = yq(StartCoef:Ncoef-StartCoef+1); % Discard leading and trailing zero coefficients.
%         disp(['Leading/trailing zeros have been detected & suppressed. Number of coeffs. reduced from ', num2str(Ncoef), ' to ', num2str(Ncoef-StartCoef*2+2)]);
        % Replaced with message as otherwise subtraction (in plot) does not work
        disp(['Leading/trailing zeros have been detected. Number of coeffs. can be reduced from ', num2str(Ncoef), ' to ', num2str(Ncoef-StartCoef*2+2)]);
    end;
    
else
    yq = ys; % No quantisation
end;

% ym: raw wavelet
% ys: raw wavelet with suppression of 50Hz and/or 60Hz
% yq: raw wavelet with suppression of 50Hz and/or 60Hz with quantisation

FFTlen = 2^14;
dBmin = -160; % dB
Fym = fft(ym,FFTlen);                    Fys = fft(ys,FFTlen);                    Fyq = fft(yq,FFTlen);
Mym = abs(Fym(1:FFTlen/2+1));            Mys = abs(Fys(1:FFTlen/2+1));            Myq = abs(Fyq(1:FFTlen/2+1));
Lym = max(20*log10(Mym/max(Mym)),dBmin); Lys = max(20*log10(Mys/max(Mys)),dBmin); Lyq = max(20*log10(Myq/max(Myq)),dBmin);
ff = (0:FFTlen/2)*Fs/FFTlen; % Frequency axis

if (plts == 1)
    subplot(2,2,1); hold on;
                                         plot(ff,Lym,'r','linewidth',2);
    if ((Supp50 == 1) || (Supp60 == 1)), plot(ff,Lys,'g','linewidth',2); end
                                         plot(ff,Lyq,'b','linewidth',2);
    grid on; xlim([-1, Fs/2]); ylim([dBmin 1]);
    title('Frequency response'); xlabel('Frequency (Hz)');ylabel('Normalised response (dB)');
    if (Supp50 == 1),
        if (Supp60 == 1), legend('Wavelet', '50Hz & 60Hz notches', 'Quantised');
        else              legend('Wavelet', '50Hz notch', 'Quantised');
        end
    else
        if (Supp60 == 1), legend('Wavelet', '60Hz notch', 'Quantised');
        else              legend('Wavelet',               'Quantised');
        end
    end
    
    subplot(2,2,2);
    plot(0.999*yq/max(abs(yq)),'k');
    hold on; plot(0.999*ym/max(abs(ym)),'b');
    grid on; plot(100*(yq/max(abs(yq))-ym/max(abs(ym))),'r');
    title('Impulse response'); xlabel('Samples');ylabel('Normalised amplitude');
    legend('Quantised', 'Raw wavelet', '100*difference');
    xlim([0 Ncoef]);
end;

Coef = yq;
end
