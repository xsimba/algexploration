function plotname = plotTestResults( feature,plotType,params )

    switch plotType
        case 'histogram'
            params.plotname = 'histogram';
            plotname = generate_histogram( feature,params );
        case 'histogram-interbeat'
            nf = diff(feature);
            params.plotname = 'histogram-interbeat';
            plotname = generate_histogram( nf,params );
        case 'piechart'
            params.plotname = 'piechart';
            plotname = generate_pie_chart( feature,params );
        case 'boxplot'
            % %%%%% NOTE THAT THIS NEEDS GROUPS FOR GENERATING THE PLOTS
            params.plotname = 'boxplot';
            plotname = generate_boxplots( feature,params );
    end


end

