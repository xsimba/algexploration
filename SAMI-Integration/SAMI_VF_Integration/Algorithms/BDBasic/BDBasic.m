function PeaksTimestamps = BDBasic( InputStream,Parameters )

    % detrend the signal
    detrended_stream = zscore(detrend(InputStream));
    % find positive peaks 
    [~,peaks_samples_location] = findpeaks(detrended_stream,'MINPEAKHEIGHT',0.5);  
    % gets timestamps
    PeaksTimestamps = peaks_samples_location/Parameters.Fs;
    
end

