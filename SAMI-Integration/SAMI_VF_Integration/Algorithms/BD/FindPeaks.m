function [FoundPeaks, PeakIndici, State] = FindPeaks(InputSamples, BlockSize, State, Params)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : FindPeaks.m
% Content   : Matlab function for peak detection with C code as comments
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% State           Structure of state variables: input and output, initialised on first call
% (SigType         Signal type, determines wavelet and processing options. 0:ECG, 1:PPG, 2:BioZ) maybe required

% FROM PeakDetect.cpp (5 MAY 2014)
% bool PeakDetect_t::FindPeaks(
%         float* pInputSamples,                   // Input: Array of samples
%         size_t BlockSize,                       // Input: Number of samples
%         SimpleList_t<int32_t>* pPeakIndici )    // Output: Detected peaks (value(s) can range from -holdoffSamples+1 to +BlockSize-2)
% {
%     pPeakIndici->Clear();             // For each call of BeatDetector, we start with an empty list
PeakIndici = [];

%Debug
if (1)
    State.Debug_FindPeaks_InputSamples    = InputSamples;
    State.Debug_FindPeaks_cmp             = zeros(1,BlockSize);
    State.Debug_FindPeaks_threshold       = zeros(1,BlockSize);
    State.Debug_FindPeaks_max             = zeros(1,BlockSize);
    State.Debug_FindPeaks_edge            = zeros(1,BlockSize);
    State.Debug_FindPeaks_holdoffcounter  = zeros(1,BlockSize);
    State.Debug_FindPeaks_maxindx         = zeros(1,BlockSize);
end

% 
%     for (uint32_t i=0; i<BlockSize; i++)
for i = 0:BlockSize-1
    j = i + 1; % Matlab indexing!!
%     {
%         //printf("%d ---------\n", i );
%         if(pInputSamples[i] > state.cmp)	// If the current output is larger than the current comparison value -> Attack mode
%         {
%             //printf("%d > Attack mode      (sample = %f, cmp = %f)   ", i, pInputSamples[i], state.cmp );
%             state.v = pInputSamples[i] - state.cmp;
%             state.av = params.attack * state.v;
%             state.cmp = state.cmp + state.av;
%             //printf("   v=%f  av=%f  cmp=%f\n", state.v, state.av, state.cmp );
%         }
%         else    // If the current output is smaller than the current comparison value -> Decay mode
%         {
%             //printf("%d > Decay mode      (sample = %f, cmp = %f)   ", i, pInputSamples[i], state.cmp );
%             state.v = pInputSamples[i] - state.cmp;
%             state.dv = params.decay * state.v;
%             state.cmp = state.cmp + state.dv;
%             //printf("   v=%f  av=%f  cmp=%f\n", state.v, state.av, state.cmp );
%         }
    if (InputSamples(j) > State.cmp), State.v = InputSamples(j) - State.cmp; State.av = Params.attack * State.v; State.cmp = State.cmp + State.av;
    else                              State.v = InputSamples(j) - State.cmp; State.dv = Params.decay  * State.v; State.cmp = State.cmp + State.dv;
    end
% 
%         float threshold = (params.gain * state.cmp) + params.offset;  // T(n) = Gain * cmp + Offset
    threshold = (Params.gain * State.cmp) + State.offset; % T(n) = Gain * cmp + Offset
% 
%         if(pInputSamples[i] > threshold && state.holdoffcounter == 0)	// If we cross the threshold and we are not counting yet -> We have a rising edge
%         {
%             state.holdoffcounter = params.holdoffSamples;     // Set the window in which we will look for the peak
%             state.risingedge = true;
%             state.fallingedge = false;
%             //printf("%d > Rising started at: %d      (sample = %f, thld = %f)\n", i, i, pInputSamples[i], threshold );
%         }
%         else    // If we are lower than the threshold or we are already counting
%         {
%             state.risingedge = false; // No rising edge
%             state.fallingedge = (state.holdoffcounter == 1); // If we reach the end of the counting period -> Falling edge
% 
%             state.holdoffcounter--;
%             if(state.holdoffcounter<0)
%             {
%                 state.holdoffcounter = 0;
%             }
%         }
    if ((InputSamples(j) > threshold) && (State.holdoffcounter == 0)) % If we cross the threshold and we are not counting yet -> We have a rising edge
        State.holdoffcounter = Params.holdoffSamples; % Set the window in which we will look for the peak
        State.risingedge = true;
        State.fallingedge = false;
    else                                                              % If we are lower than the threshold or we are already counting
        State.risingedge = false; % No rising edge
        State.fallingedge = (State.holdoffcounter == 1); % If we reach the end of the counting period -> Falling edge
        State.holdoffcounter = max(State.holdoffcounter - 1,0);
    end
% 
%         if(state.holdoffcounter != 0)	// If we are counting
%         {
%             if(pInputSamples[i] > state.max) // If the current value is larger than the current max of the window -> Update the maximum and the index
%             {
%                 state.max = pInputSamples[i];
%                 state.maxindx = state.holdoffcounter;
%                 //printf("%d > New max at %d:  %f\n", i, i, pInputSamples[i] );
%             }
%         }
%         else
%         {
%             if(state.fallingedge)		// When we reach a falling edge, we determine the index where the maximum was
%             {
%                 int32_t peak_idx = (i - state.maxindx);
% 
%                 // Add the detected peak to the list
%                 assert_dbg( !pPeakIndici->IsFull() );       // Not yet full?
%                 pPeakIndici->Add( peak_idx );
%                 //printf("%d > Add peak to list:  %d  (%d - %d)\n", i, peak_idx, i, state.maxindx );
%             }
%             state.max = 0.0f;
%         }
    if (State.holdoffcounter > 0) % If we are counting
        if (InputSamples(j) > State.max), State.max = InputSamples(j); State.maxindx = State.holdoffcounter; end
    else
        if (State.fallingedge) % When we reach a falling edge, we determine the index where the maximum was
            peak_idx = (i - State.maxindx);
            PeakIndici = [PeakIndici peak_idx]; %#ok<AGROW>
        end
        State.max = 0;
    end
%     }

% Debug
if (1)
    State.Debug_FindPeaks_cmp(j)            = State.cmp;
    State.Debug_FindPeaks_threshold(j)      = threshold;
    State.Debug_FindPeaks_max(j)            = State.max;
    State.Debug_FindPeaks_edge(j)           = State.risingedge - State.fallingedge; % 0:no edge, 1:rising, -1:falling
    State.Debug_FindPeaks_holdoffcounter(j) = State.holdoffcounter;
    State.Debug_FindPeaks_maxindx(j)        = State.maxindx;
    
    if ((State.risingedge == 1) && (State.fallingedge == 1)), disp('Rising edge and falling edge true at same time!'); end
end

end % for i = 0:BlockSize-1
% 
%     return !pPeakIndici->IsEmpty();	// Return true or false, depending of whether the list is empty or not
FoundPeaks = ~isempty(PeakIndici);
% }
end % Of primary function
% 


 
