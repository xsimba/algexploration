function [InterpolatedIndexList] = QuadInterpolatePeaks(InputSamples, PrevSamples, PrevBlockSize, PeakIndexList)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : QuadInterpolatePeaks.m
% Content   : Matlab function for quadratic peak detection with C code as comments
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples            Block of input samples
% PrevSamples             Block of input samples from previous block
% PrevBlockSize           Number of input samples in previous block
% PeakIndexList           List of detected peaks
% InterpolatedIndexList   List of corrected peaks

% FROM PeakDetect.cpp (5 MAY 2014)
% void QuadInterpolatePeaks(
%         float *pInputSamples,                           // Input:  Current block of sample
%         float *pPrevSamples,                            // Input:  Previous block of samples
%         size_t PrevBlockSize,                           // Input:  Size of previous block of samples
%         SimpleList_t<int32_t> *pPeakIndexList,          // Input:  List of detected peaks
%         SimpleList_t<float> *pInterpolatedIndexList )   // Output: List of corrected peaks
% {
%     pInterpolatedIndexList->Clear();        // For each call of BeatDetector, we start with an empty list
AllSamples = [PrevSamples InputSamples]; % Concatenate previous and current input samples so that no boundary processing is required
% 
%     size_t NumElems = pPeakIndexList->GetNumElems();
%     for (size_t i=0; i<NumElems; i++)       // Iterate all elements of the input list 'pPeakIndexList'
%     {
%         // Get the next peak index value
%         int32_t PeakIndex = (*pPeakIndexList)[i];
%         //printf("%d > PeakIndex:  %d\n", i, PeakIndex );
%         assert_dbg( PrevBlockSize + PeakIndex-1 > 0 );
% 
%         // Fetch the 3 sample values around this peak index
%         float PrevY, PeakY, NextY;
%         if (PeakIndex > 0)
%         {
%             // All 3 samples are in the current block
%             PrevY = pInputSamples[PeakIndex-1];
%             PeakY = pInputSamples[PeakIndex];
%             NextY = pInputSamples[PeakIndex+1];
%         }
%         else if (PeakIndex < -1)
%         {
%             // All 3 samples are in the previous block
%             assert_dbg( pPrevSamples != NULL );
%             PrevY = pPrevSamples[PrevBlockSize+PeakIndex-1];
%             PeakY = pPrevSamples[PrevBlockSize+PeakIndex];
%             NextY = pPrevSamples[PrevBlockSize+PeakIndex+1];
%         }
%         else
%         {
% // We are at the block boundary between the previous block and the current block
% 
%             // Normally we would simply assert here to check 'pPrevSamples', e.g.:
%             //        assert_dbg( pPrevSamples != NULL );
%             // But at startup, it can happen that detection is triggered at index 0.
%             // This is probably due to the offset being set to 0.
%             if (pPrevSamples == NULL)
%                 break;      // !!! BREAK THE LOOP !!!  Go for the next iteration!
% 
%             // Fetch 'PrevY' at -1 (relative)
%             {
%                 const int32_t index = PeakIndex - 1;
%                 if (index >= 0) {
%                     PrevY = pInputSamples[index];
%                 } else if((int32_t)PrevBlockSize+index >= 0) {
%                     assert_dbg( pPrevSamples != NULL );
%                     PrevY = pPrevSamples[PrevBlockSize+index];
%                 } else {
%                     PrevY = pInputSamples[0];
%                 }
%             }
% 
%             // Fetch 'PeakY' at 0 (relative)
%             {
%                 const int32_t index = PeakIndex;
%                 if (index >= 0) {
%                     PeakY = pInputSamples[index];
%                 } else {
%                     assert_dbg( pPrevSamples != NULL );
%                     PeakY = pPrevSamples[PrevBlockSize+index];
%                 }
%             }
% 
%             // Fetch 'NextY' at +1 (relative)
%             {
%                 const int32_t index = PeakIndex + 1;
%                 if (index >= 0) {
%                     NextY = pInputSamples[index];
%                 } else {
%                     assert_dbg( pPrevSamples != NULL );
%                     NextY = pPrevSamples[PrevBlockSize+index];
%                 }
%             }
%         }
PrevY = AllSamples(PeakIndexList+PrevBlockSize+1-1);
PeakY = AllSamples(PeakIndexList+PrevBlockSize+1+0);
NextY = AllSamples(PeakIndexList+PrevBlockSize+1+1);

a = (NextY + PrevY - 2*PeakY)*0.5;
b = (NextY - PrevY)*0.5;
f = ((abs(a) < abs(b)) | (a == 0)); % Flag bad values

a(f) = 1;
b(f) = 0;

Correction = -b./(2*a);

InterpolatedIndexList = PeakIndexList + Correction;
%         // Interpolate to determine the fractionary correction to the integer index
%         #if 0 // Older variant
%             float Correction = QuadPkDet( PrevY, PeakY, NextY );
%         #else
%             float Correction;
%             bool Valid = QuadPkDet( PrevY, PeakY, NextY, &Correction );     // Peak interpolation
%             (void) Valid;       // Choose to ignore the 'valid' indication (Correction will be 0.0 in case the interpolation failed)
%         #endif
% 
%         pInterpolatedIndexList->Add( (float) PeakIndex + Correction );
%     }
% }

end

