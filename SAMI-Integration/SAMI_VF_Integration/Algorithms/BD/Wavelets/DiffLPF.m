function [d0, dl] = DiffLPF(Fpass, Fstop, Fs)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : DiffLPF.m
% Content   : Matlab function for wavelet generation based on LPF with differentiation
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

%  Fpass: Pass band stop frequency (Hz)
%  Fstop: Stop band start frequency (Hz)
%  fs: sample frequency (Hz)
%  d0: Wavelet coefficients (normalised)
%  dl: Estimated filter delay (samples)

% Example use:
% [d0, dl] = DiffLPF(7.0, 10.0, 128);


% Define filter, make filter have fractional delay by adjusting stop band attenuation
% Fpass = 7;   % Passband Frequency
% Fstop = 10;   % Stopband Frequency
Apass = 0.1;  % Passband Ripple (dB)
Astop = 61;   % Stopband Attenuation (dB)

h = fdesign.lowpass('fp,fst,ap,ast', Fpass, Fstop, Apass, Astop, Fs);
Hd = design(h, 'equiripple', 'MinOrder', 'any', 'StopbandShape', 'flat');
q = grpdelay(Hd);
q = q(1);

% Make wavelet combining LPF and diff...
d0 = [Hd.Numerator 0]/2 - [0 Hd.Numerator]/2; % Convolve with filter [1/2 -1/2] for diff. function
d0 = fliplr(d0)/max(abs(d0)); % Normalisation and reverse order (as we need coefficients that apply to increasing time for the C impl.)

fw = abs(fft([d0 zeros(1, 16384-length(d0))]));
fw = fw(1:8193); fw = max(20*log10(fw/max(fw)),-80); % dB scale
figure;plot((0:8192)*Fs/16384,fw);grid on;title('Wavelet frequency response');xlabel('Frequency (Hz)');
figure;plot((0:length(d0)-1)*1000/Fs,d0);grid on;title('Wavelet coefficients');xlabel('Time (ms)');

dl = q+0.5; % Add delay from derivative

end
