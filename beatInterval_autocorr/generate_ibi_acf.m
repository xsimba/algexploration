% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : generate_ibi_acf.m
%  Content    : Heart rate estimator based on autocorrelation
%  Package    : SIMBA.Validation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 16-09-2014
%  Modification and Version History: 
%  | Developer | Version |    Date   | 
%  | Yelei Li  |   1.0   | 16-09-2014|
% --------------------------------------------------------------------------------

function ibi_acf = generate_ibi_acf( InputStream, FIR_option, Fs )

    dims = size(InputStream);
    if dims(1)>dims(2)
        InputStream = InputStream';
    end
    
    % General
    if (~exist('Fs')),
        Fs                            = 128 ;                                  % Sampling Frequency
    end
    FreqTS                        = 32768;                                 % Timestamp clock frequency
    BlockSize                     = round(Fs*0.5);                         % 500 ms blocks
    Params.ts2samRatio            = FreqTS/Fs;                             % Ratio of timestamp to sample frequency

if FIR_option == 0
    
else
   Params.CWT_FIR         = importdata('Wavelets/Coeffs/PPGwavelet10_128HzDiffLPF.csv');  % 1: Load PPG  CWT coefficients 12 Hz passband, 2nd order numerical differentiation
% Params.CWT_FIR         = importdata('Wavelets/Coeffs/BioZwavelet1_1024HzSupp.csv');

end        

    Nblocks = floor(length(InputStream)/BlockSize);
    State = []; 
    
    for Blk = 0:Nblocks-1
        
        Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
        InputSamples = InputStream(Blk*BlockSize+1:Blk*BlockSize+BlockSize); 
             
        [State] = find_beatInterval(InputSamples, BlockSize, Timestamp, State, Params, FIR_option);

        ibi_acf(Blk+1) = State.location_zc / Fs;

    end
    

end

