function [State] = find_beatInterval(InputSamples, BlockSize, Timestamp, State, Params, FIR_option)
% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : auto_HR.m
%  Content    : Heart rate estimator based on autocorrelation
%  Package    : SIMBA.Validation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 17-09-2014
%  Modification and Version History: 
%  | Developer | Version |    Date   | 
%  | Yelei Li  |   1.0   | 17-09-2014|
% --------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% Timestamp       Timestamp of first sample in block (32768 Hz ticks)
% FoundBeats      Boolean flag to indicate if any beats were found in current block
% BeatTimestamps  List of timestamps of beats
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters
% SigType         Signal type, determines wavelet and processing options. 0:ECG, 1:PPG, 2:BioZ

if (isempty(State)) % Populate state: CWT state and from PeakDetect.cpp
   if FIR_option~=0 
        State.cwt = ones(length(Params.CWT_FIR)-1,1)*InputSamples(1); % Initial state of CWT, constant level of first sample     
    %     % Initialise filter
        [~,State.cwt] = filter(fliplr(Params.CWT_FIR),1,ones(length(Params.CWT_FIR),1)*InputSamples(1)); % Perform filter (reverse order for Matlab to match C impl.)
   end
   
    State.HistoryCwtOutput = zeros(1,BlockSize*5); % History of CWT output including current: 5 blocks
end

% For each call to FindBeats, we start with an empty list
% BeatTimestamps = [];

if FIR_option~=0
    % Apply filters: CWT and Abs / Zero Clamp
    [CWTsamples,State.cwt] = filter(fliplr(Params.CWT_FIR),1,InputSamples,State.cwt); % Perform filter (reverse order for Matlab to match C impl.)

    aCWTsamples = CWTsamples.*(CWTsamples>0);  % 1: PPG use clamp function

else 
     CWTsamples = InputSamples;
    aCWTsamples = InputSamples;
end

% CWT history update
%         Previous history         input
% +-----+-----+-----+-----+-----+ +-----+
% |     :                       | |     |
% +-----+-----+-----+-----+-----+ +-----+
%                    \               |
%                     v              v
%         +-----+-----+-----+-----+-----+
%         |                       :     | New history
%         +-----+-----+-----+-----+-----+
%                                 ^
%                             timestamp

State.HistoryCwtOutput(1:end-BlockSize) = State.HistoryCwtOutput(1+BlockSize:end);
State.HistoryCwtOutput(end-BlockSize+1:end) = CWTsamples;


State.location_zc = [];
% figure;[Rxy]=acf(State.HistoryCwtOutput',100);figure(10);plot(State.HistoryCwtOutput);pause; close all;
[Rx, Rx_1]=acf(State.HistoryCwtOutput',150);
[temp_max, location_max]= max(Rx_1);
temp_zc = find(Rx_1(1:end-1)>0 & Rx_1(2:end)<0);
temp_zc2 =find (temp_zc >location_max);

if ~isempty(temp_zc2)
    State.location_zc = temp_zc(temp_zc2(1));
end

if isempty(State.location_zc) | State.location_zc<35
    State.location_zc = -1;
end


