clear all;
close all;

ratio_threshold = 50;
t_simulation    = 100*3600;   %total simulation time (sec)

E0       = 3996;
E_margin = .1*E0;
s_bg     = 5.5537/1000;
s_ppg    = 8.9317/1000;
s_base   = 430/1000;
t_ppg    = 90;
t_base   = 30;
E_b      = E0-E_margin;

%K1: period between PPG samples, from lights-on to lights-on
%K2: period between Simbase wakeups                                        

M0       = 30;
M_margin = 3;
T        = 50*3600;
M_bg     = 20/(1024*8);
M_ppg    = 192/(1024*8);
M_b      = M0-M_margin;

E(1)     = E_b;    % Initial battery energy [J]
M(1)     = M_b;    % Initial memory [kbytes]
flag_M   = 0;
% flag_ppg = 0;
[duty_cycle] = duty_cycle(T, s_ppg, s_base, s_bg, M_b, M_ppg, M_bg, E_b, t_base)
flag_ppg = ppg_sampling_generator(t_simulation,duty_cycle,t_ppg,T);


for i = 2:(t_simulation);
   
    if E(i-1) <= 0
       E(i) = 0; % stop working when reach energy safety margin 
    else
        if M(i-1) <= 0
           flag_M = t_base;  % simebase wakeup
           M(i)   = M_b;
           E(i)   = E(i-1)-s_bg-heavi_side(flag_M)*s_base-flag_ppg(i)*s_ppg;
        else
           E(i)   = E(i-1)-s_bg-heavi_side(flag_M)*s_base-flag_ppg(i)*s_ppg;
           M(i)   = M(i-1)-M_bg-flag_ppg(i)*M_ppg; 
        end
    end
%         max_ratio_k1_tppg(i_T) = (T(i_T)*s_ppg*M_b + T(i_T)*s_base*t_base*M_ppg)/(E_b*M_b-T(i_T)*s_bg*M_b-T(i_T)*s_base*t_base*M_bg);
   
    flag_M = (flag_M-1);
    
end


plot((1:t_simulation)/3600,E)
figure;plot(M)