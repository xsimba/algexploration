function [duty_cycle]=duty_cycle(T, s_ppg, s_base, s_bg, M_b, M_ppg, M_bg, E_b, t_base)

    for i_T =1:length(T)

        max_ratio_k1_tppg(i_T) = (T(i_T)*s_ppg*M_b + T(i_T)*s_base*t_base*M_ppg)/(E_b*M_b-T(i_T)*s_bg*M_b-T(i_T)*s_base*t_base*M_bg);
        %               max_k2(i_energy, i_T)            = (T(i_T)*min(s_ppg)*M_b+T(i_T)*min(s_base)*t_base*M_ppg)/(E_b(i_energy)*M_ppg-T(i_T)*min(s_bg)*M_ppg+T(i_T)*min(s_ppg)*M_bg);

        if max_ratio_k1_tppg(i_T)>=50 | max_ratio_k1_tppg(i_T)<=0
        max_ratio_k1_tppg( i_T) = nan;
        %                   max_k2(i_energy, i_T)            = nan;
        end
    end

duty_cycle        = 1./max_ratio_k1_tppg;

end