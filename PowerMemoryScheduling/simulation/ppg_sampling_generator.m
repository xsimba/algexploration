function flag_ppg = ppg_sampling_generator(t_simulation,duty_cycle,t_ppg,T)
%  % regular duty cycle 
%  t_cycle = floor (t_ppg/duty_cycle); % PPG sampling interval from lights-on to lights-on
%  
%  n = ceil(t_simulation/t_cycle);
%  
%  temp_flag = [zeros(1,(t_cycle-t_ppg)) ones(1,t_ppg)];
%  
%  flag_ppg  = repmat(temp_flag,1,n);
%  flag_ppg  = flag_ppg(1:t_simulation);
 
 
 %random ppg sampling
 n_interval   = floor(T/t_ppg);
 index_sample = randperm(n_interval);
 n_cycle      = floor (n_interval*duty_cycle); % PPG sampling interval from lights-on to lights-on
 
 temp_index   = sort(index_sample(1:n_cycle));
 
 flag_ppg     = zeros(1,t_simulation);

 for i = 1:n_cycle
     flag_ppg((temp_index(i)-1)*t_ppg+1:temp_index(i)*t_ppg)     = 1;
 end
 
end