\documentclass[letter]{article} 
\usepackage[left=3cm,top=2cm,right=3cm]{geometry}
\usepackage{mathrsfs}
%\usepackage{fontspec}
%\setmainfont{Cambria}
%\newfontface\titlefont
%[Contextuals={WordInitial,WordFinal}]
%    {Jellyka Delicious Cake}
%    {Neuton Cursive}

\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
\fancyhead[LO,LE]{\large asif.khalak@samsung.com}
\fancyhead[RO,RE]{\thepage}
\fancyfoot[CO,CE]{Samsung Confidential}

\fancypagestyle{firststyle}
{
   \fancyhf{}
   \fancyfoot[C]{Samsung Confidential}
   \fancyfoot[R]{\thepage}
}

\usepackage{amsmath}
\usepackage{multirow}
\newcommand{\erf}{\mathop{\mathrm{erf}}} 
\newcommand{\erfc}{\mathop{\mathrm{erfc}}} 
\newenvironment{myEnumerate}{
  \begin{enumerate}
    \setlength{\itemsep}{1pt}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0pt}
}{
  \end{enumerate}
}

\bibliographystyle{plain}

\begin{document}

\title{Scheduling algorithms for heartbeat collection in a power and memory constrained embedded system\\ \textit{DRAFT}}
\author{Asif Khalak\\
mHealth Team, Samsung Strategy and Innovation Center\\
Menlo Park, CA\\
asif.khalak@ssi.samsung.com}
\date{20 May 2014}
\maketitle

\thispagestyle{firststyle}

\begin{abstract}
Power and memory constraints in an embedded system makes these a primary consideration in developing stable and effective algorithms for data collection.  The goals for the Simband beta release in collection mode are to run for 24 hours, and operate within the memory footprint of the M4 embedded processor, which is limited to 96kbytes.  The memory available for data storage is about 25kbytes-30kbytes, and the data must be periodically transferred to the Simbase for transfer off the device.   
\end{abstract}


\section{Optimal duty cycle scheduling}

Starting with an initial battery charge of $E_0$ in collection mode, there are two overlapping scheduling cycles: (a) the scheduling of PPG collection, separated by a period of $K_1$, and (b) the scheduling of simband wakeup and transfer separated by a period of $K_2$.  Assuming that the device remains in collection mode, the energy drain from the battery follows the following formula:

\[
\textrm{Avail. Energy at t} = \textrm{Initial Energy} - \textrm{Safety Margin} - t * \textrm{Bkgd. Power} - t* \textrm{PPG Power} - t * \textrm{Wakeup Power}
\]

\noindent or, in more detail, 

\begin{equation}
E(t) = E_0 - \epsilon_0 - S_{bg} t - \left\lfloor \frac{t}{K_1}\right\rfloor S_{ppg} t_{ppg} - \left\lfloor \frac{t}{K_2} \right\rfloor S_{base} t_{base}
\end{equation}

\noindent where $\lfloor \rfloor$ indicates the {\tt floor()} function and the following definitions apply.

\bigskip

\begin{tabular}{|c|r|} \hline
Variable & Definition \\ \hline
$E_0$ & Initial state of battery energy, [J] \\
$\epsilon$ & Energy safety margin. [J] \\
$t$     & Time variable, [sec] \\
$S_{bg}$ & Power drain from background processes with lights off, including Bio-Z processing, [W]\\\
$S_{ppg}$ & Avg. Power drain during PPG sampling, [W] \\
$S_{base}$ & Avg. Power drain during periodic Simbase wakeup, [W] \\
$t_{ppg}$ & duration of PPG sample periods, [s] \\
$t_{base}$ & duration of Simbase wakeup periods, [s] \\
$K_1$    & period between PPG samples, from lights-on to lights-on, [s] \\
$K_2$  & period between Simbase wakeups, [s] \\ \hline
\end{tabular}

\bigskip

To satisfy the design objectives, we can plug in 24 hours in for $T$, and solve for the duty cycle parameters $K_1$ and $K_2$.  It is an underconstrained problem so power considerations alone are not sufficient to determine the relative frequency of PPG sampling to Simbase wakeups.  The constraint on memory, however, determines the requirements for Simbase wakeups.  The memory can also be treated as a conserved quantity that behaves as follows following a Simbase wakeup.

\[
\textrm{Free Memory at t} = \textrm{Buffer Size} - \textrm{Safety Margin} - t * \textrm{Bio-Z storage} - t* \textrm{PPG Storage}
\]

\noindent or, in more detail, 


\begin{equation}
M(t) = M_0 - \mu_0 - m_{bg} t - M_{ppg} \left\lfloor \frac{t}{K_1} \right\rfloor  
\end{equation}

\noindent where the following definitions apply.

\bigskip

\begin{tabular}{|c|r|} \hline
Variable & Definition \\ \hline
$M_0$ & Memory buffer-size, presumed free following a simbase wakeup and flush event, [kbytes] \\
$\mu_0$ & Safety margin, [kbytes] \\
$t$     & Time variable, [s] \\
$m_{bg}$ & Memory reqd. from lights off processes, including Bio-Z processing, [kbytes/s] \\
$M_{ppg}$ & Storage for PPG sampling for a single period ($\approx30s$), [kbytes] \\
$K_1$    & period between PPG samples, from lights-on to lights-on, [s] \\ \hline
\end{tabular}

\bigskip
\noindent

As an approximation -- which is limited to a fraction of the last duty cycle -- we can ignore the $\lfloor \rfloor$ operator and compute the period as if the power and memory charges are distributed continuously.  Thus, to solve for $K_1$ and $K_2$, use the following procedure as detailed in the appendix: 

\begin{enumerate}
\item Set $t=K_2$ in equation (2), and solve for $K_1$ in terms of $K_2$.
\item Substitute this relationship into equation (1) and solve for $K_2$ in terms of the other parameters.
\item Back-substitute to find $K_1$.
\end{enumerate}

Adding in a safety margins of $\epsilon_0$ and $\mu_0$ to the battery charge and the memory budget, the following formulas for the periods are as follows (see Appendix):

\begin{align}
K_1 & =  \frac{T S_{ppg} t_{ppg} M_b + T S_{base} t_{base} M_{ppg}}{E_b M_b - T S_{bg} M_b  - T S_{base} t_{base} m_{bg} } \\
K_2 & = \frac{T S_{ppg} t_{ppg} M_b  +   T S_{base} t_{base} M_{ppg}}{E_b M_{ppg} - T S_{bg} M_{ppg} + T S_{ppg} t_{ppg} m_{bg}}
\end{align}

\noindent where $M_b = M_0-\mu_0$ and $E_b=E_0-\epsilon_0$.

Implicit in the above, is the idea that more sampling is better and that given the battery life, $T$, we should sample as much as possible.

\subsection{Interpretation of Optimal Duty Cycle}

The formulas for the optimal duty cycle periods, $K_1$ and $K_2$, can be rearranged for interpreted as follows:

\begin{align*}
K_1 & =  \frac{ S_{ppg} t_{ppg} M_b +  S_{base} t_{base} M_{ppg}}{\frac{E_b M_b}{T} - S_{bg} M_b  - S_{base} t_{base} m_{bg} }  \\
& = \frac{\textrm{PPG Energy}*\textrm{Tot.Memory Buffer} + \textrm{Base wakeup Energy}*\textrm{PPG Memory}}
{\textrm{Supply Power} * \textrm{Tot.Memory} - \textrm{Bkgd. Power} * \textrm{Tot.Memory} - \textrm{Base wake Energy} * \textrm{Bkgd.MemoryFill}}
\end{align*}

\begin{align*}
K_2 & = \frac{S_{ppg} t_{ppg} M_b  +   S_{base} t_{base} M_{ppg}}{\frac{E_b M_{ppg}}{T} + S_{ppg} t_{ppg} m_{bg} - S_{bg} M_{ppg}  } \\
&= \frac{\textrm{PPG Energy}*\textrm{Tot.Memory Buffer} + \textrm{Base wakeup Energy}*\textrm{PPG Memory}}
{\textrm{Supply Power} * \textrm{PPG Memory} + \textrm{PPG Energy} * \textrm{Bkgd.MemoryFill} - \textrm{Bkgd.Power} * \textrm{PPG Memory}}
\end{align*}

\noindent Note that in the above the "PPG Energy" and "PPG Memory" are quoted for a single $\approx30s$ sample period, and that the Background memory is a fill rate in $[kbytes/s]$.


\section{Forms of Uncertainty and Management Strategies}
In a certain world, then the formulas derived in the previous section are optimal.  However, the following forms of uncertainty may be present in the system and should be addressed:

\begin{enumerate}
\item Random consumption of energy from spot mode activities.  The above analysis presumes that the device is only in collection mode.  However, user spot mode activity will tend to reduce the power in uncontrolled ways due to user activity.
\item Random consumption of memory, including temporary overflows.  Generally, the embedded design process should eliminate these, but it would be good to guard against this.
\item Changes in PPG quality with time as a function of PPG calibration parameters, which may impact duty-cycle parameters (e.g. $M_{ppg}$, $S_{ppg}$, $t_{ppg}$).
\item Changes in PPG quality with time as a function of acceleration and physiological parameters including activity mode (e.g. sleep, exercise, etc.).
\item Changes in Bio-Z quality with time as a function of Bio-Z parameters.  
\item Changes in Bio-Z quality with time as a function of acceleration and physiologial parameters, including activity mode (e.g. sleep, exercise, etc.).
\end{enumerate}  
  
The strategy can be summarized as follows:

\begin{itemize}
\item Continuously estimate the power and memory consumption and trigger guard 'wakeup-flush-return' for low memory and 'wakeup-flush-sleep' for low power.  This handles the first two items in the above uncertainty list.  For tuning purposes, the safety buffers $\epsilon_0$ and $\mu_0$ should be set to avoid triggering the 'wakeup-flush-return' guard under intended operation.
\item Don't address items 5 and 6 of uncertainty in the scheduling, and allow the data quality of the Bio-Z.
\item Address uncertainty item 3, related to PPG quality by triggering a re-calibration.  The re-calibration can either happen during a wakeup or immediately during collection (TBD).  This re-calibration will consume power and will require memory, which may have an impact on the scheduling/duty cycle, as discussed below.  For instance, it may not be possible to perform an immediate recalibration too late in the cycle before a wake-up if the memory is not sufficient to handle it.
\item Uncertainty item 4, related to using PPG quality predictions, will cause a modification in the duty cycles per activity mode, or possibly in skipping PPG cycles that are unlikely to produce good data.  This has not yet been worked out.
\end{itemize}

\section{PPG Sensor Calibration}
\verb+This is still to be filled in+

\section{Predictive scheduling modifications based on activity mode}
\verb+This is still to be filled in+

\appendix

\section{Optimal Duty Cycle Derivation}

\subsection{Relation between $K_1$ and $K_2$ from memory constraint}
Starting from the memory constraint, 
\[
M(t) = M_0 - \mu_0 - m_{bg} t - M_{ppg} \left\lfloor \frac{t}{K_1} \right\rfloor  
\]

\noindent Set $M(K_2)=0$, such that the buffer is drained completely just before the scheduled simbase wakeup and flush.  Thus,

\[
0 = M_0 - \mu_0 - m_{bg} K_2 - M_{ppg} \left\lfloor \frac{K_2}{K_1} \right\rfloor  
\]  

\noindent and then,

\[
M_0 - \mu_0 =  m_{bg} K_2 + M_{ppg} \left\lfloor \frac{K_2}{K_1} \right\rfloor
\]

\noindent Dropping the $\lfloor \rfloor$ operators, we get

\[
M_0 - \mu_0 =  m_{bg} K_2 + M_{ppg} \frac{K_2}{K_1}
\] 

and solving for $K_1$ gives the following

\[
K_1 = \frac{M_{ppg} K_2}{M_0 - \mu_0 - m_{bg} K_2} 
\]

\subsection{Solving for $K_2$}

Start from continuous approximation to the power relation (1),

\[
E(t) = E_0 - \epsilon_0 - S_{bg} t - \frac{t}{K_1} S_{ppg} t_{ppg} - \frac{t}{K_2} S_{base} t_{base}
\]

\noindent Here, we set the energy at the battery lifetime, $T$, to zero, so $E(T)=0$,

\[
0 = E_0 - \epsilon_0 - S_{bg} T - \frac{T}{K_1} S_{ppg} t_{ppg} - \frac{T}{K_2} S_{base} t_{base}
\]

\noindent Now substitute for $K_1$, as follows:

\[
0 = E_0 - \epsilon_0 - S_{bg} T - \frac{M_0 - \mu_0 - m_{bg} K_2}{M_{ppg} K_2} S_{ppg} t_{ppg} T - \frac{1}{K_2} S_{base} t_{base} T
\]

Multiplying both sides by $K_2$, gives

\[
0 = E_0 K_2 - \epsilon_0 K_2 - S_{bg} T K_2 - \left(\frac{M_0}{M_{ppg}} - \frac{\mu_0}{M_{ppg}} - \frac{m_{bg} K_2}{M_{ppg}} \right) S_{ppg} t_{ppg} T - S_{base} t_{base} T
\]

Collect terms, and simplify as follows

\begin{align*}
& \left(M_0 - \mu_0\right) \frac{S_{ppg} t_{ppg}}{M_{ppg}} T +  S_{base} t_{base} T = \left(E_0 - \epsilon_0 - S_{bg} T + \frac{m_{bg}}{M_{ppg}}  S_{ppg} t_{ppg} T \right) K_2 \\ \\
K_2 = & \frac{\left(M_0 - \mu_0\right) \frac{S_{ppg} t_{ppg}}{M_{ppg}} T +  S_{base} t_{base} T}{E_0 - \epsilon_0 - S_{bg} T + \frac{m_{bg}}{M_{ppg}}  S_{ppg} t_{ppg} T } \\ \\
K_2 = & \frac{\left(M_0 - \mu_0\right) S_{ppg} t_{ppg} T +  S_{base} M_{ppg} t_{base} T}{E_0 M_{ppg} - \epsilon_0 M_{ppg} - S_{bg} M_{ppg} T + m_{bg}  S_{ppg} t_{ppg} T}
\end{align*}

Defining, $M_b = M_0-\mu_0$ and $E_b = E_0-\epsilon_0$, this reduces to

\[
K_2 = \frac{T S_{ppg} t_{ppg} M_b  +   T S_{base} t_{base} M_{ppg}}{E_b M_{ppg} - T S_{bg} M_{ppg} + T S_{ppg} t_{ppg} m_{bg}}
\]

\subsection{Explicit expression for $K_1$}

First, solve for $K_2$ in terms of $K_1$ from the previous result as follows:


\begin{align*}
K_1 & = \frac{M_{ppg} K_2}{M_0 - \mu_0 - m_{bg} K_2} \\
K_1 \left( M_0 - \mu_0 - m_{bg} K_2\right) & = M_{ppg} K_2  \\
K_1 M_0 - \mu_0 K_1 & = m_{bg} K_1 K_2 + M_{ppg} K_2 \\
K_1 M_0 - \mu_0 K_1 & = K_2 \left( m_{bg} K_1 + M_{ppg} \right) \\
K_2  = \frac{K_1 M_0 - \mu_0 K_1}{m_{bg} K_1 + M_{ppg}}
\end{align*}


Subsituting this result into the power relationship (1) at the end of the battery lifetime, $T$, so $E(T)=0$,

\begin{align*}
0 & = E_0 - \epsilon_0 - S_{bg} T - \frac{T}{K_1} S_{ppg} t_{ppg} - \frac{T}{K_2} S_{base} t_{base} \\
0 & = E_0 - \epsilon_0 - S_{bg} T - \frac{T}{K_1} S_{ppg} t_{ppg} - T S_{base} t_{base} \frac{m_{bg} K_1 + M_{ppg}}{K_1 M_0 - \mu_0 K_1} \\
0 & = E_0 - \epsilon_0 - S_{bg} T - \frac{T}{K_1} S_{ppg} t_{ppg} - T S_{base} t_{base} \left( \frac{m_{bg}}{M_0 - \mu_0}  + \frac{M_{ppg}}{K_1 \left(M_0 - \mu_0\right)}\right) \\
0 & = E_0 - \epsilon_0 - S_{bg} T  - T S_{base} t_{base}\frac{m_{bg}}{M_0 - \mu_0} - \frac{T}{K_1} S_{ppg} t_{ppg} - T S_{base} t_{base}\frac{M_{ppg}}{K_1 \left(M_0 - \mu_0\right)}
\end{align*}

Which reduces to
\[
 \frac{T}{K_1} S_{ppg} t_{ppg} + T S_{base} t_{base}\frac{M_{ppg}}{K_1 \left(M_0 - \mu_0\right)} = E_0 - \epsilon_0 - S_{bg} T  - T S_{base} t_{base}\frac{m_{bg}}{M_0 - \mu_0}
\]

Or,

\begin{align*}
K_1 =  \frac{T S_{ppg} t_{ppg} + T S_{base} t_{base}\frac{M_{ppg}}{\left(M_0 - \mu_0\right)}}{E_0 - \epsilon_0 - S_{bg} T  - T S_{base} t_{base}\frac{m_{bg}}{M_0 - \mu_0}} \\
K_1 =  \frac{T S_{ppg} t_{ppg} \left(M_0 - \mu_0\right) + T S_{base} t_{base} M_{ppg}}{\left(E_0 - \epsilon_0 - S_{bg} T\right) \left(M_0-\mu_0\right)  - T S_{base} t_{base} m_{bg} }
\end{align*}

And substituting the $M_b$ and $E_b$ variables, this simplifies to

\[
K_1  =  \frac{T S_{ppg} t_{ppg} M_b + T S_{base} t_{base} M_{ppg}}{E_b M_b - T S_{bg} M_b  - T S_{base} t_{base} m_{bg} } \\
\]


\end{document}
