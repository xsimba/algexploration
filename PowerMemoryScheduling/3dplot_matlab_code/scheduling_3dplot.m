clear all;
close all;

ratio_threshold = 50;

s_bg     = 47.43243243/1000;
s_ppg    = 54.18918919/1000;
s_base   = 570/1000; %331.95
t_ppg    = 90;
t_base   = 45;

%K1: period between PPG samples, from lights-on to lights-on
%K2: period between Simbase wakeups

M0       = 30;
M_margin = 3;
T        = 24*3600;
M_bg     = 20/(1024*8);
M_ppg    = 192/(1024*8);
M_b      = M0-M_margin;
t_ppg    = (25:90);


E0       = [3996];
E_margin = .1*E0;
E_b      = E0-E_margin;

% case T v.s. Ratio_ppg with varied power drain ranges.
s_bg   = [3.7962 5.5537 7.3112]/1000;
s_ppg  = [8.7664 8.9317 9.697]/1000;
s_bg   = [3.7962 15 47.4]/1000;
s_ppg  = [8.7664 54.2 54.2]/1000;

s_base = [300 430 570]/1000;
T        = 1:60:160*3600;

for i_energy=1:length(E0)
    for i_T =1:length(T)
%         for i=1:length(t_ppg)
    %         k1(i) = (T*s_ppg*t_ppg(i)*M_b+T*s_base*t_base*M_ppg*t_ppg(i))/(E_b*M_b-T*s_bg*M_b-T*s_base*t_base*M_bg);
              max_ratio_k1_tppg(i_energy, i_T) = (T(i_T)*min(s_ppg)*M_b + T(i_T)*min(s_base)*t_base*M_ppg)/(E_b(i_energy)*M_b-T(i_T)*min(s_bg)*M_b-T(i_T)*min(s_base)*t_base*M_bg);
              max_k2(i_energy, i_T)            = (T(i_T)*min(s_ppg)*M_b+T(i_T)*min(s_base)*t_base*M_ppg)/(E_b(i_energy)*M_ppg-T(i_T)*min(s_bg)*M_ppg+T(i_T)*min(s_ppg)*M_bg);
              
              if max_ratio_k1_tppg(i_energy, i_T)>=ratio_threshold | max_ratio_k1_tppg(i_energy, i_T)<=0
                  max_ratio_k1_tppg(i_energy, i_T) = nan;
                  max_k2(i_energy, i_T)            = nan;
              end
              
              min_ratio_k1_tppg(i_energy, i_T) = (T(i_T)*max(s_ppg)*M_b + T(i_T)*max(s_base)*t_base*M_ppg)/(E_b(i_energy)*M_b-T(i_T)*max(s_bg)*M_b-T(i_T)*max(s_base)*t_base*M_bg);
              min_k2(i_energy, i_T)            = (T(i_T)*max(s_ppg)*M_b+T(i_T)*max(s_base)*t_base*M_ppg)/(E_b(i_energy)*M_ppg-T(i_T)*max(s_bg)*M_ppg+T(i_T)*max(s_ppg)*M_bg);
              if min_ratio_k1_tppg(i_energy, i_T)>=ratio_threshold | min_ratio_k1_tppg(i_energy, i_T)<=0
                  min_ratio_k1_tppg(i_energy, i_T) = nan;
                  min_k2(i_energy, i_T)            = nan;
              end
              
              avg_ratio_k1_tppg(i_energy, i_T) = (T(i_T)*s_ppg(2)*M_b + T(i_T)*s_base(2)*t_base*M_ppg)/(E_b(i_energy)*M_b-T(i_T)*s_bg(2)*M_b-T(i_T)*s_base(2)*t_base*M_bg);
              avg_k2(i_energy, i_T)            = (T(i_T)*s_ppg(2)*M_b+T(i_T)*s_base(2)*t_base*M_ppg)/(E_b(i_energy)*M_ppg-T(i_T)*s_bg(2)*M_ppg+T(i_T)*s_ppg(2)*M_bg);
              if avg_ratio_k1_tppg(i_energy, i_T)>=ratio_threshold | avg_ratio_k1_tppg(i_energy, i_T)<=0
                  avg_ratio_k1_tppg(i_energy, i_T) = nan;
                  avg_k2(i_energy, i_T)            = nan;
              end
    %         k2 = (k1(i)*M_b)/(M_bg*k1(i)+M_ppg*t_ppg(i));
%               k2(i_energy, i_T)            = (T(i_T)*s_ppg*M_b+T(i_T)*s_base*t_base*M_ppg)/(E_b(i_energy)*M_ppg-T(i_T)*s_bg*M_ppg+T(i_T)*s_ppg*M_bg);
%         end
    end
end

figure(1);
clf;

figure(2);
clf;

figure(3);
clf;


for i = 1:length(E0),
    figure(1);
    plt = plot(T/3600,1./min_ratio_k1_tppg(i, :));
    hold on;
    plt2 = plot(T/3600,1./max_ratio_k1_tppg(i, :),'r');
    plt3 = plot(T/3600,1./avg_ratio_k1_tppg(i, :),'g');
    xlabel('Battery Life [h]')
    ylabel('Duty Cycle Fraction')
    set(gca, 'YLim', [0 1]);
    set(gca, 'XLim', [0 100]);
    if (i / 2 == floor(i/2)),
        set(plt, 'LineStyle', '--');
        set(plt2, 'LineStyle', '--');
        set(plt3, 'LineStyle', '--');
    else
        set(plt, 'LineWidth',2);
        set(plt2, 'LineWidth', 2);
        set(plt3, 'LineStyle', '--');
    end
    legend('Lower__bound','Upper__bound','Avg')
    
    
    figure(2);
    plt = plot(T/3600,min_k2(i, :)/3600);
    hold on; 
    plt2  = plot(T/3600,max_k2(i, :)/3600,'r');
    plt3  = plot(T/3600,avg_k2(i, :)/3600,'g');
    xlabel('Battery Life [h]');
    ylabel('Simbase Wakeup Interval [h]');
    set(gca, 'YLim', [0 2.5]);
    set(gca, 'XLim', [0 100]);
     
    if (i / 2 == floor(i/2)),
        set(plt, 'LineStyle', '--');
        set(plt2, 'LineStyle', '--');
        set(plt3, 'LineStyle', '--');
    else
        set(plt, 'LineWidth',2);
        set(plt2, 'LineWidth', 2);
        set(plt3, 'LineStyle', '--');
    end
    legend('Lower__bound','Upper__bound','Avg')

    figure(3);
    plt = plot3(T/3600,1./min_ratio_k1_tppg(i, :),min_k2(i, :)/3600);
    hold on;
    plt2 = plot3(T/3600,1./max_ratio_k1_tppg(i, :),max_k2(i, :)/3600,'r');
    plt3 = plot3(T/3600,1./avg_ratio_k1_tppg(i, :),avg_k2(i, :)/3600,'g');
    xlabel('Battery Life [h]')
    ylabel('Duty Cycle Fraction')
    zlabel('Simbase Wakeup Interval [h]');
    set(gca, 'YLim', [0 1]);
    set(gca, 'XLim', [0 100]);
    set(gca, 'ZLim', [0 2.5]);
    if (i / 2 == floor(i/2)),
        set(plt, 'LineStyle', '--');
        set(plt2, 'LineStyle', '--');
        set(plt3, 'LineStyle', '--');
    else
        set(plt, 'LineWidth',2);
        set(plt2, 'LineWidth', 2);
        set(plt3, 'LineStyle', '--');
    end
%     legend('Lower__bound','Upper__bound','Avg')


    figure(4);
    plt = plot(1./max_ratio_k1_tppg(i, :),max_k2(i, :)/3600);
    hold on;plt = plot(1./avg_ratio_k1_tppg(i, :),avg_k2(i, :)/3600,'g');
    hold on;plt = plot(1./min_ratio_k1_tppg(i, :),min_k2(i, :)/3600,'r');
    set(gca,'Xlim',[0,1])
end



