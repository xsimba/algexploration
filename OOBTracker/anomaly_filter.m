function [index, output] = anomaly_filter(input, Wsize, minC, maxC, alpha)
%
%
%
if ~exist('minC', 'var'),
    minC = 20;
    maxC = 80;
    alpha = 0.4;
end

counter = 1;
for k = 1:(length(input)-Wsize),
    clip = input(k:k+Wsize);
    med = median (clip);
    clipRange = prctile(clip, [minC maxC]);
    if (input(k) > clipRange(1) && input(k) < clipRange(2)),
        index(counter) = k;
        if (counter == 1);
            output(counter) = input(k);
        else
            output(counter) = alpha*input(k) + (1-alpha)*output(counter-1);
        end
        counter = counter + 1;
    end
end


