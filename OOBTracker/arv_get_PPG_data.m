function [fs, timestamp_arr, ppg_arr, ppg_channels_str_arr] = arv_get_PPG_data(data)

    fs = 128; %Simband sampling rate
    timestamp_arr = data.timestamps;

    ppg_arr(1,:) = data.physiosignal.ppg.a.signal;
    ppg_arr(2,:) = data.physiosignal.ppg.b.signal;
    ppg_arr(3,:) = data.physiosignal.ppg.c.signal;
    ppg_arr(4,:) = data.physiosignal.ppg.d.signal;
    ppg_arr(5,:) = data.physiosignal.ppg.e.signal;
    ppg_arr(6,:) = data.physiosignal.ppg.f.signal;
    ppg_arr(7,:) = data.physiosignal.ppg.g.signal;
    ppg_arr(8,:) = data.physiosignal.ppg.h.signal;
    
    ppg_channels_str_arr = [];
         
    for k = 1:8
        ppg_channels_str_arr{k} = sprintf('%d', k-1);
    end

end