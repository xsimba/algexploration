function [SBPest, DBPest] = continuous_bp_tracker_1 (PAT, HR, ref, psys, pdia)
% tracker algo
%   Start with a reference (BP, PAT, HR) and track from there.
%

SBPest = ref.SBP + psys.alpha * log ((ref.PTT) ./ (PAT - ref.PET)) + ...
    psys.beta * (HR - ref.HR) / ref.HR;

DBPest = ref.DBP + pdia.alpha * log ((ref.PTT) ./ (PAT - ref.PET)) + ...
    pdia.beta * (HR - ref.HR) / ref.HR;

