%Check to see if 'data' exist in memory, Simband data version 5
if (~exist('data', 'var')),
    error ('data not loaded (V5)');
elseif (~isfield(data, 'physiosignal')),
    error ('data not loaded (V5)');
end    


%general parameters (for filtering PPG signals and beat detection)
general_params = [];

general_params.lowFreq = 0.5;
general_params.highFreq = 5;
general_params.avg_win_sec = 10;

general_params.min_heart_rate = 40;
general_params.max_heart_rate  = 200;
general_params.filter_lower_than_mean_values  = false;
general_params.chunk_size_min = 1;

general_params.segmentation_ch_index = 5;

general_params.correct_baseline_wandering = true;
general_params.show_corrected_wandering_baseline = false;
general_params.show_beat_segmentation = false;

multichannel_params = [];
multichannel_params.processed_channels_arr = [];
multichannel_params.num_peaks = 2;


%selected channels to be fuzed
multichannel_params.processed_channels_arr = [1 1 1 1 1 1 1 1];
    

    
%get data
[fs, timestamp_arr, ppg_arr, ppg_channels_str_arr] = arv_get_PPG_data(data);


general_params.channels_str_arr = ppg_channels_str_arr;

general_params.fs = fs;
%-------------FILTER CHANNELS------------------------------------------------------------------

%filter data
%notice: the detault filtering is by using an FIR filter; however if this filter was not already predefined (calculation time is a little bit long),
%a Butterworth is used (IIR)
[filtered_ppg_arr] =  filter_PPGs(ppg_arr,  general_params.fs, general_params.lowFreq, general_params.highFreq, general_params.avg_win_sec);
%correct for group delay 
filtered_timestamp_arr = timestamp_arr(1:size(filtered_ppg_arr, 2));
%----------------------------------------------------------------------------------------------------------------
%% 

%core routine - models the AC component of the PPGs
[multichannel_signal_arr, baseline_signals_arr, model_params_arr, model_graphic_data]  = ...
                                                    fit_multi_PPG_signal_to_sum_gaussians(filtered_timestamp_arr, filtered_ppg_arr,...
                                                    general_params, multichannel_params);
                                                
results_data = [];
results_data.timestamp_arr = filtered_timestamp_arr;
results_data.ppg_arr = ppg_arr;
results_data.filtered_ppg_arr = filtered_ppg_arr;


results_data.general_params = general_params;
results_data.multichannel_params = multichannel_params;

results_data.multichannel_signal_arr = multichannel_signal_arr;
results_data.baseline_signals_arr = baseline_signals_arr;

results_data.model_params_arr = model_params_arr;
results_data.model_graphic_data = model_graphic_data;
                                                
                                                %% 
                                                
[multichannel_signal_arr2, baseline_signals_arr2, model_params_arr2, model_graphic_data2]  = ...
                                                    fit_each_PPG_signal_to_sum_gaussians(filtered_timestamp_arr, filtered_ppg_arr,...
                                                    general_params, multichannel_params);
                                                
result_data2 = [];
result_data2.timestamp_arr = filtered_timestamp_arr;
result_data2.ppg_arr = ppg_arr;
result_data2.filtered_ppg_arr = filtered_ppg_arr;


result_data2.general_params = general_params;
result_data2.multichannel_params = multichannel_params;

result_data2.multichannel_signal_arr = multichannel_signal_arr2;
result_data2.baseline_signals_arr = baseline_signals_arr2;

result_data2.model_params_arr = model_params_arr2;
result_data2.model_graphic_data = model_graphic_data2;

%--------------------Show modeling results-----------------------------------------
%% 

show_peakFit(model_graphic_data, general_params.channels_str_arr);    


%--------------------Save modeling results-----------------------------------------

%% 









%     results_filename = fullfile(dir_path, protocol_dir_arr{file_index}, ...
%         'model_2peaks', sprintf('%s_2peaks.mat', filename_arr{file_index}));
%
%     save(results_filename, 'results_data');