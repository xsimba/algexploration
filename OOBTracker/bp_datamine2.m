%
% finapres vs simband offset
%

dataJsonString = '{"startDate" :1433284500000,"endDate" :1433286720000,"sdids" : "1624c0064e5a446799e2cb0e5b10b78b","uids" : "32c571fee8d44b19bc0229610a5fcf28"}';
p = readSamiClipboard(dataJsonString);

if(~exist('data', 'var')),
    data = readSamiPortalData(p, 0, 'aligned');
end

if(~exist('dataDebug', 'var')),
    dataDebug = readSamiPortalData(p, 0, 'not aligned');
end

%%


fin_offset = 295.1; % approx in seconds
annot_marks = [0 182 465 520 780 825 1020]; % Lindsay's annotations
annot_marks_corr = [0 185 460 522 783 823 1024]; % corrected annotations
annot_offset = 957;

rest1 = [0 annot_offset];
hgrip_range = annot_offset+annot_marks_corr(1:2);
rest2 = annot_offset+annot_marks_corr(2:3);
squat_range = annot_offset+annot_marks_corr(3:4);
rest3 = annot_offset+annot_marks_corr(4:5);
ice_range = annot_offset+annot_marks_corr(5:6);
rest4 = [annot_offset+annot_marks_corr(6) max(data.timestamps)];


working_trange = [500 2300];

% run SSICv0.1 model

% define reference.  Need to align fina and simband indicies.
% reference @ 60s before the hand grip

% trim crap off the end of the HR
data.heartRate.lbhr.timestamps = data.heartRate.lbhr.timestamps(1:2150);
data.heartRate.lbhr.signal = data.heartRate.lbhr.signal(1:2150);

refTime = annot_offset - 60;
[~,pat_indx] = min(abs(data.pat.timestamps-data.pat.timestamps(1)-refTime));
[~,fin_indx] = min(abs(finapres_bp(:,1)+fin_offset-refTime));

ref.SBP  = finapres_bp(fin_indx,2);
ref.DBP  = finapres_bp(fin_indx,3);
ref.PAT = data.pat.signal(pat_indx)*1000;
ref.HR  = finapres_bp(fin_indx,5) ;
ref.PET = 150;
ref.PTT = ref.PAT - ref.PET;

%
% parameters
%
psys.alpha = 50;
psys.beta = 10;

pdia.alpha = 10;
pdia.beta = 0;

%
% prepare inputs for basic_tracker
%
PAT = data.pat.signal * 1000; % in ms
% currently using the LBHR as HR proxy.
HR  = interp1(data.heartRate.lbhr.timestamps, data.heartRate.lbhr.signal, timePat);
timePat = data.pat.timestamps;
timePat = timePat - timePat(1);

[SBPest, DBPest] = continuous_bp_tracker_1 (PAT, HR, ref, psys, pdia);

% extract pwa features
upstroke = data.physiosignal.ppg.e.beats.timestamps;
features_raw = data.physiosignal.ppg.e.beats.info.signal;
features = reshape(features_raw, 8, length(features_raw)/8);

foot = features(1,1:length(upstroke));
peak = features(3,1:length(upstroke));
secpeak = features(2,1:length(upstroke));

upstrokeAmp = interp1(data.unixTimeStamps/1000, ...
    data.physiosignal.ppg.e.signal, ...
    upstroke);

footAmp = interp1(data.unixTimeStamps/1000, ...
    data.physiosignal.ppg.e.signal, ...
    upstroke+foot);
peakAmp = interp1(data.unixTimeStamps/1000, ...
    data.physiosignal.ppg.e.signal, ...
    upstroke+peak);
secpeakAmp = interp1(data.unixTimeStamps/1000, ...
    data.physiosignal.ppg.e.signal, ...
    upstroke+secpeak);

acraw = peakAmp - footAmp;
reflTime_raw = secpeak-peak;

%
% these presume that the gaussian fitting has been run and the
% result is contained in the variable results_data2.
%
if exist('results_data2', 'var'),
    time = results_data2.timestamp_arr;
    beatInd = results_data2.model_params_arr.beats_begin_index_arr;
    
    mu = results_data2.model_params_arr.all_mu_arr;
    sigma = results_data2.model_params_arr.all_sigma_arr;
end

%%

%
% compute indicies for signals and beat_features
%

allActions = {...
    'plot offset graph', ... %1
    'plot raw signal record', ... %2
    'BP tracker comparison', ... %3
    'ppg DC', ... %4
    'pwa AC', ... %5
    'pwa AC/DC', ... %6
    'pat', ... %7
    'prim-sec peak', ... %8
    'plot gaussian fit means', ... % 9
    'plot gaussian fit scaled means (mu2-mu1)/sigmaRat', ... %10
    'plot gaussian fit scaled means (mu2-mu1)/log(sigmaRat)', ... %11
    'plot gaussian fit scaled means vs. pat vs. ci', ... %12
    'plot gaussian fit scaled means vs. pat', ... %13
    };
actions = allActions{12};

switch actions,
    case {'plot offset graph'},
        %%
        figure(1);
        clf
        ax1= subplot(211);
        plot(dataDebug.physiosignal.ppg.e.timestamps, dataDebug.physiosignal.ppg.e.signal, '.', 'MarkerSize', 10);
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], 1e5*[-3 0], 'k--');
        end
        
        set(gca, 'XLim', working_trange);
        ylabel('Simband PPG Ch4');
        
        ax2 = subplot(212);
        range = 1:382090;
        plot(finapres_raw(range,1)+fin_offset, finapres_raw(range,2))
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [-100 300], 'k--');
        end
        set(gca, 'XLim', working_trange);
        set(gca, 'YLim', [-40 300]);
        ylabel('Finapres Finger');
        xlabel('time [s] -- with offset');
        linkaxes([ax1 ax2], 'x');
        set(gca, 'XLim', working_trange);
        figure(2);
        clf;
        tlbhr = data.heartRate.lbhr.timestamps-data.heartRate.lbhr.timestamps(1);
        ax1 = subplot(211);
        plot(tlbhr, data.heartRate.lbhr.signal);
        hold on;
        plot(finapres_bp(:,1)+fin_offset, finapres_bp(:,5), 'r');
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [40 120], 'k--');
        end
        set(gca, 'XLim', working_trange);
        legend({'Simband LBHR','Finapres HR'});
        ax2= subplot(212);
        finHrInterp = interp1(finapres_bp(:,1)+fin_offset, finapres_bp(:,5), data.heartRate.lbhr.timestamps-data.heartRate.lbhr.timestamps(1));
        plot(tlbhr, data.heartRate.lbhr.signal-finHrInterp, 'r.');
        set(gca, 'XLim', working_trange);
        linkaxes([ax1 ax2], 'x');
        
    case {'plot raw signal record'},
        %%
        figure(1);
        clf
        ax1= subplot(211);
        plot(finapres_bp(:,1)+fin_offset, finapres_bp(:,2) - ...
            finapres_bp(:,3));
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks(k)*[1 1], [30 100], 'k--');
        end
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [30 100], 'r--');
        end
        
        xlabel('time [s]');
        ylabel('SBP-DBP [mmHg]');
        set(gca, 'XLim', working_trange);
        set(gca, 'YLim', [30 100]);
        
        ax2= subplot(212);
        plot(data.accel.variance.timestamps-data.accel.variance.timestamps(1), ...
            data.accel.variance.signal);
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks(k)*[1 1], [0 0.2], 'k--');
        end
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 0.2], 'r--');
        end
        xlabel('time [s]');
        ylabel('accel variance [g]');
        set(gca, 'XLim', working_trange);
        linkaxes([ax1 ax2], 'x');
        
    case {'BP tracker comparison'},
        %%
        figure(1)
        clf
        subplot(211);
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        
        subplot(212);
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,3), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), DBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 130]);
        xlabel('time [s]');
        ylabel('Diastolic BP [mmHg]')
        
    case 'ppg DC';
        %%
        figure(1);  % systolic
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        subplot(212);
        plt(2) = gca;
        plot(data.timestamps, data.physiosignal.ppg.e.signal, '-', 'MarkerSize', 10);
        hold on;
        plot(data.physiosignal.ppg.e.beats.timestamps - data.unixTimeStamps(1)/1000, ...
            upstrokeAmp, 'x', 'MarkerSize', 6);
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], 1e5*[-3 0], 'k--');
        end
        xlabel('time [s]');
        ylabel('PPG Signal')
        
        figure(2);  % all features annotated
        clf
        subplot(211);
        plt(3) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        subplot(212);
        plt(4) = gca;
        plot(data.timestamps, data.physiosignal.ppg.e.signal, '-', 'MarkerSize', 10);
        hold on;
        plot(upstroke - data.unixTimeStamps(1)/1000, ...
            upstrokeAmp, 'x', 'MarkerSize', 6);
        plot(upstroke + foot - data.unixTimeStamps(1)/1000, ...
            footAmp, 'o', 'MarkerSize', 6);
        plot(upstroke + peak - data.unixTimeStamps(1)/1000, ...
            peakAmp, '+', 'MarkerSize', 6);
        plot(upstroke + secpeak - data.unixTimeStamps(1)/1000, ...
            secpeakAmp, 'v', 'MarkerSize', 6);
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], 1e5*[-3 0], 'k--');
        end
        xlabel('time [s]');
        ylabel('PPG Signal')
        
        linkaxes(plt, 'x');
        
        
    case 'pwa AC';
        %%
        figure(1);  % systolic
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        
        subplot(212);
        plt(2) = gca;
        plot(data.physiosignal.ppg.e.beats.timestamps - data.unixTimeStamps(1)/1000, ...
            acraw, 'x', 'MarkerSize', 6);
        %        for k = 1:length(annot_marks),
        %            plot(annot_offset+annot_marks_corr(k)*[1 1], [-300 0], 'k--');
        %        end
        xlabel('time [s]');
        ylabel('PPG Beat AC Signal')
        
        linkaxes(plt, 'x');
        
    case 'pat';
        %%
        figure(1);  % systolic
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        
        subplot(212);
        plt(2) = gca;
        plot(timePat, PAT/1000, 'x', 'MarkerSize', 6);
        %        for k = 1:length(annot_marks),
        %            plot(annot_offset+annot_marks_corr(k)*[1 1], [-300 0], 'k--');
        %        end
        xlabel('time [s]');
        ylabel('PAT [s]')
        
        linkaxes(plt, 'x');
        
        
    case 'prim-sec peak';
        %%
        figure(1);  % refltime
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        subplot(212);
        plt(2) = gca;
        plot(upstroke - data.unixTimeStamps(1)/1000, ...
            reflTime_raw, 'x', 'MarkerSize', 6);
        %         for k = 1:length(annot_marks),
        %             plot(annot_offset+annot_marks_corr(k)*[1 1], [-1 1], 'k--');
        %         end
        xlabel('time [s]');
        ylabel('PPG Pri/Sec Peak delay')
        
        linkaxes(plt, 'x');
        
    case {'plot gaussian fit means'};
        %%
        ch = 3;
        peak_delay = mu(ch,:,2) - mu(ch,:,1);
        
        %filter out anomalous data points
        pd_ind = find(peak_delay>0 & peak_delay<0.5);
        t = time(beatInd(pd_ind));
        
        
        figure;
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        %        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        hold off;
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        subplot(212);
        plt(2) = gca;
        
        plot(t,peak_delay(pd_ind),'bx');
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 0.6], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [0 0.6]);
        xlabel('time [s]');
        ylabel('PPG Pri/Sec Peak delay')
        
        linkaxes(plt, 'x');
        
    case {'plot gaussian fit scaled means (mu2-mu1)/sigmaRat'};
        %%
        ch = 8;
        feature1 = (mu(ch,:,2) - mu(ch,:,1)) ./ (sigma(ch,:,1)./sigma(ch,:,2));
        feature2 = (mu(ch,:,2) - mu(ch,:,1)) ./ ...
            abs(log(sigma(ch,:,1)) - log(sigma(ch,:,2)));
        
        %filter out anomalous data points
        pd_ind = find(peak_delay>0 & peak_delay<0.5);
        t = time(beatInd(pd_ind));
        
        
        figure (1);
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        %        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        hold off;
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        subplot(212);
        plt(2) = gca;
        
        plot(t,feature1(pd_ind),'bx');
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 10], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [0 10]);
        xlabel('time [s]');
        ylabel('(mu_1 - mu_2) / sigmaRatio')
        
        linkaxes(plt, 'x');
        
    case {'plot gaussian fit scaled means (mu2-mu1)/log(sigmaRat)'};
        ch = 8;
        feature2 = (mu(ch,:,2) - mu(ch,:,1)) ./ ...
            log(sigma(ch,:,1)) - log(sigma(ch,:,2));
        
        %filter out anomalous data points
        pd_ind = find(peak_delay>0 & peak_delay<0.5);
        t = time(beatInd(pd_ind));
        
        figure(1)
        clf
        subplot(211);
        plt(1) = gca;
        plot(fin_offset+finapres_bp(:,1), finapres_bp(:,2), 'r.');
        hold on;
        %        plot(data.pat.timestamps-data.pat.timestamps(1), SBPest, 'b.');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [70 220], 'k--');
        end
        hold off;
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [70 220]);
        %        legend({'finapres', 'Simband SSIC0.1b'});
        xlabel('time [s]');
        ylabel('Systolic BP [mmHg]')
        subplot(212);
        plt(2) = gca;
        
        plot(t,feature2(pd_ind),'bx');
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 5], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [0 5]);
        xlabel('time [s]');
        ylabel('(mu_1 - mu_2) / log(sigmaRatio)')
        
        linkaxes(plt, 'x');

    case {'plot gaussian fit scaled means vs. pat vs. ci'}; %12
        %%
        ch = 8;
        feature1 = (mu(ch,:,2) - mu(ch,:,1)) ./ (sigma(ch,:,1)./sigma(ch,:,2));
        [index1, filtFeature1] = anomaly_filter(feature1(pd_ind),30, 20, 80, 0.35);
        [index0, filtPAT] = anomaly_filter(PAT/1000,30, 20, 80, 0.35);
       
        subplot(311);
        plt(1) = gca;
        plot(timePat, PAT/1000, 'x', 'MarkerSize', 6);
        %        for k = 1:length(annot_marks),
        %            plot(annot_offset+annot_marks_corr(k)*[1 1], [-300 0], 'k--');
        %        end
        hold on;
        plot(timePat(index0),filtPAT,'r--');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 5], 'k--');
        end
        set(gca, 'YLim', [0.15 0.4]);
        set(gca, 'XLim', [500 2300]);
        xlabel('time [s]');
        ylabel('PAT [s]')
        
        subplot(312);
        plt(2) = gca;
        
        plot(t,feature1(pd_ind),'bx');
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 5], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [0 1]);
        xlabel('time [s]');
        ylabel('(mu_1 - mu_2) / sigmaRatio')
        
        plot(t(index1),filtFeature1,'r--');
        
        
        subplot(313);
        plt(3) = gca;
%        ci = data.physiosignal.ppg.e.confidenceraw;
        ci = data.physiosignal.ppg.h.confidenceraw;
        plot(ci.timestamps-ci.timestamps(1),ci.signal,'b.');
        hold on;
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 5], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [0 5]);
        xlabel('time [s]');
        ylabel('PPG CI Raw')

        
        linkaxes(plt, 'x');
        
        figure;
        clf
        plot(timePat(index0),filtPAT,'b--');
        hold on;
        plot(t(index1),filtFeature1,'r--');
        for k = 1:length(annot_marks),
            plot(annot_offset+annot_marks_corr(k)*[1 1], [0 5], 'k--');
        end
        set(gca, 'XLim', [500 2300]);
        set(gca, 'YLim', [0 0.7]);
        xlabel('time [s]');
        ylabel('PPG CI Raw')
        legend({'PAT', 'delta mu / sigmaRat'});
        

    case {'plot gaussian fit scaled means vs. pat'}; %13
        %%
        ch = 5;
        feature1 = (mu(ch,:,2) - mu(ch,:,1)) ./ (sigma(ch,:,1)./sigma(ch,:,2));
        selfeature = feature1(pd_ind);
        selfeature2 = interp1(t, selfeature, timePat);
        notNan = (~isnan(selfeature2) & ~isnan(PAT) & selfeature2>0 & selfeature2 < 1);
        
        correlation = corrcoef(selfeature2(notNan), PAT(notNan));
        
        figure;
        clf;
        plot(selfeature2(notNan), PAT(notNan)/1000, 'x', 'MarkerSize', 6);
        axis([0 1 0.15 0.4]);
        xlabel('gaussian fit delay');
        ylabel('pat')
        
        
end

