sigmaVec = 1:50;
CIRawvec = 1:0.1:5;

CIraw = repmat(CIRawvec(:), 1, length(sigmaVec));
sigma = repmat(sigmaVec(:)', length(CIRawvec), 1);

HRCI_raw = ((4/3) * CIraw -(1/3)) .* (-2*((1./(1+exp(-sigma/12))) - 1));
HRCI = min(max(round(HRCI_raw), 1), 5);
%HRCI = -1 * (2*(CIraw - 1) .*((1./(1+exp(-sigma/10))) -1)) + 1;
%y = -1 * (2* CIRange *((1./(1+exp(-x/20))) -0.5) - CIRange) + 1;
%y = (CIMax + 1) - (2*CIRange*((1./(1+exp(-x/100))) -0.5) + 1);
%y = 7.5 - (5 ./ (1+exp(-x/100)));
figure;
surf(sigmaVec, CIRawvec,  HRCI), xlabel('/sigma'), ylabel('PPG CI'), zlabel('BB BiosemHR CI')


