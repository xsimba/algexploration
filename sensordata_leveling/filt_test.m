threshold = 0.25;

BeatTime_Rem = [];
BeatAmp_Rem = [];
lastBeat = BeatTimePpgDetrend(1);
lastAmp  = BeatAmpPpgDetrend(1);
for i = 2:length(BeatTimePpgDetrend),
if (BeatTimePpgDetrend(i) - lastBeat) > threshold*FreqTS,
  BeatTime_Rem = [BeatTime_Rem(:)' lastBeat];
  BeatAmp_Rem = [BeatAmp_Rem(:)' lastAmp];
  lastBeat = BeatTimePpgDetrend(i);
  lastAmp = BeatAmpPpgDetrend(i);
  end
end

[removals, rind] = setdiff(BeatTimePpgDetrend, BeatTime_Rem);
removalsAmp = BeatAmpPpgDetrend(rind);

figure(5)
clf
subplot(211);
hist(diff(BeatTime_Rem/FreqTS), [0.2:0.2:6])
title('detrend, with double beat filter');
subplot(212);
hist(diff(BeatTimePpgDetrend/FreqTS), [0.2:0.2:6])
title('detrend, no double beat filter');

figure(6)
clf
Nsamp = length(SampPpgDetrend);
plot((0:Nsamp-1)/Fs, SampPpgDetrend, 'b');
hold on;
plot(BeatTime_Rem/FreqTS, BeatAmp_Rem, 'rx');
plot(removals/FreqTS, removalsAmp, 'go');

figure(7)
clf
subplot(311)
plot (sort(diff(BeatTime_Rem), 'descend')/FreqTS, (1:length(BeatTime_Rem)-1)/length(BeatTime_Rem))
hold on
plot (sort(diff(BeatTimePpgDetrend), 'descend')/FreqTS, (1:length(BeatTimePpgDetrend)-1)/length(BeatTimePpgDetrend), 'g')
ylabel('1-CDF')
xlabel ('inter pulse time, t [s]')

subplot(312)
plot (sort(diff(BeatTime_Rem), 'descend')/FreqTS, (1:length(BeatTime_Rem)-1))
hold on
plot (sort(diff(BeatTimePpgDetrend), 'descend')/FreqTS, (1:length(BeatTimePpgDetrend)-1), 'g')
ylabel('# detections less than t')
xlabel ('inter pulse time, t [s]')

subplot(313)
plot (60*FreqTS./sort(diff(BeatTime_Rem), 'descend'), (1:length(BeatTime_Rem)-1)/length(BeatTime_Rem))
hold on
plot (60*FreqTS ./ sort(diff(BeatTimePpgDetrend), 'descend'), (1:length(BeatTimePpgDetrend)-1)/length(BeatTimePpgDetrend), 'g')
ylabel('1-CDF')
xlabel ('Heart Rate [bpm]')
set(gca, 'XLim', [0 180])

