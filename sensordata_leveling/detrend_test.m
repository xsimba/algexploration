%
% test detrending on PPG
%
PPGsit = load('C:\Users\asif.khalak\Documents\sensordata\senstrace_horz_lhand_resting_fv0_13_2_sv_0_95_0\PPG_1.tsv');
PPGstand = load('C:\Users\asif.khalak\Documents\sensordata\senstrace_horz_lhand_standing_fv0_13_2_sv_0_95_0\PPG_1.tsv');

time = PPGsit(:,1);
PPGred = PPGsit(:,2);
PPGgreen = PPGsit(:,3);

plot(time, PPGgreen);

%
% robust coarse detrending
%
windowhlen = 100; % 2 sec
order = 10; % 5th percentile
runningfloor = zeros(size(time));

for i = windowhlen:(length(PPGgreen)-windowhlen),
    lookahead = PPGgreen(i-windowhlen+1:i+windowhlen);
    runningfloor(i) = orderstat(lookahead, order);
end

detrend_coarse = PPGgreen - runningfloor;
detrend_coarse(1:windowhlen) = zeros(length(windowhlen));
detrend_coarse(end-windowhlen:end) = zeros(length(windowhlen));


%
% fine detrending should be done w/ a smoothing spline, or similar.
%
windowhlen_2 = 30; % 0.6 sec
order_2 = 2; % 2d percentile
runningfloor_2 = zeros(size(time));
 
for i = windowhlen+windowhlen_2:(length(PPGgreen)-windowhlen_2-windowhlen),
    lookwindow = detrend_coarse(i-windowhlen_2+1:i+windowhlen_2);
    runningfloor_2(i) = orderstat(lookwindow, order);
end

detrend_fine                     = detrend_coarse - runningfloor_2;
detrend_fine(1:windowhlen)       = zeros(length(windowhlen));
detrend_fine(end-windowhlen:end) = zeros(length(windowhlen));
 
%
% plotting
%
ind = (windowhlen+1+windowhlen_2):(length(time)-windowhlen-windowhlen_2-1);

figure(1); clf;
ax1= subplot(311);
plot(time(ind),PPGgreen(ind));
ylabel('PPG green [raw]')
ax2= subplot(312);
plot(time(ind), detrend_coarse(ind));
ylabel('coarse detrend')
ax3 = subplot(313);
plot(time(ind),runningfloor(ind));
ylabel('coarse trend')
linkaxes([ax1 ax2 ax3], 'x');
xlabel('time [s]');

figure(2); clf;
ax1= subplot(311);
plot(time(ind),PPGgreen(ind));
ylabel('PPG green [raw]')
ax2= subplot(312);
plot(time(ind), detrend_fine(ind));
ylabel('fine detrend')
ax3 = subplot(313);
plot(time(ind),runningfloor(ind)+runningfloor_2(ind));
ylabel('fine trend')
linkaxes([ax1 ax2 ax3], 'x');
xlabel('time [s]');



    