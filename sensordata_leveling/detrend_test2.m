%
% test detrending on PPG
%
PPGsit = load('C:\Users\asif.khalak\Documents\sensordata\senstrace_horz_lhand_resting_fv0_13_2_sv_0_95_0\PPG_1.tsv');
PPGstand = load('C:\Users\asif.khalak\Documents\sensordata\senstrace_horz_lhand_standing_fv0_13_2_sv_0_95_0\PPG_1.tsv');

sit=1;
if (sit==0),
    time = PPGstand(:,1);
    PPGred = PPGstand(:,2);
    PPGgreen = PPGstand(:,3);
else
    time = PPGsit(:,1);
    PPGred = PPGsit(:,2);
    PPGgreen = PPGsit(:,3);
end

dataSeries = PPGred;

%
% two step detrending (coarse + fine) in a single pass
%
windowhlen = 100; % 2 sec
order = 10; % 5th percentile
runningfloor = zeros(size(time));

windowhlen_2 = 30; % 0.6 sec.  needs to be smaller than 1/2 windowhlen
order_2 = 2; % 2d percentile
runningfloor_2 = zeros(size(time));

detrend_coarse = zeros(size(time));
detrend_fine   = zeros(size(time));

for i = windowhlen:(length(dataSeries)-windowhlen),
    filtwindow1           = dataSeries(i-windowhlen+1:i+windowhlen);
    runningfloor(i)       = orderstat(filtwindow1, order);
    detrend_coarse(i)     = dataSeries(i) - runningfloor(i);
    if i > (windowhlen),
        filtwindow2       = detrend_coarse(i-2*windowhlen_2:i);
        runningfloor_2(i-windowhlen_2) = orderstat(filtwindow2, order_2);
        detrend_fine(i-windowhlen_2)   = detrend_coarse(i-windowhlen_2) - ...
            runningfloor_2(i-windowhlen_2);
    end
end


%
% plotting
%
ind = (windowhlen+1+windowhlen_2):(length(time)-windowhlen-windowhlen_2-1);

figure(1); clf;
ax1= subplot(311);
plot(time(ind), dataSeries(ind));
ylabel('PPG green [raw]')
ax2= subplot(312);
plot(time(ind), detrend_coarse(ind));
ylabel('coarse detrend')
ax3 = subplot(313);
plot(time(ind),runningfloor(ind));
ylabel('coarse trend')
linkaxes([ax1 ax2 ax3], 'x');
xlabel('time [s]');



figure(2); clf;
ax1= subplot(311);
plot(time(ind), dataSeries(ind));
ylabel('PPG red [raw]')
ax2= subplot(312);
plot(time(ind), detrend_fine(ind));
ylabel('fine detrend')
ax3 = subplot(313);
plot(time(ind),runningfloor(ind)+runningfloor_2(ind));
ylabel('fine trend')
linkaxes([ax1 ax2 ax3], 'x');
xlabel('time [s]');


%h = firpm(15, [0 0.1 0.3 1], [0 1 0 0], 'differentiator');
%
%figure(3); clf
%ax1= subplot(311);
%plot(time(ind), DataSeries(ind));
%ylabel('PPG green [raw]')
%ax2= subplot(312);
%plot(time(ind),filter(h, 1, PPGgreen(ind)));
%ylabel('diff PPG raw')
%ax3= subplot(313);
%plot(time(ind),filter(h, 1, detrend_fine(ind)));
%ylabel('diff fine detrend')
%linkaxes([ax1 ax2 ax3], 'x');
%xlabel('time [s]');



    