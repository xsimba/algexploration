function [AccOut] = AccGainOffset(acc, MinX, MaxX, MinY, MaxY, MinZ, MaxZ, PlotFlag)

% First time running use inputs: acc, -1, 1, -1, 1, -1, 1
% Then determine max and min values for each axis from plot.

x = acc.x.signal;
y = acc.y.signal;
z = acc.z.signal;

if (PlotFlag)
    figure;hold on;plot(x,'b');plot(y,'r');plot(z,'g');grid on;
    title('Input accelerometer signals');legend('x','y','z');
    
    % Filter data                        Fpass, Fstop, Apass, Astop, Fs
    h = fdesign.lowpass('fp,fst,ap,ast', 4.000, 5.000, 0.100, 60.00, 128);
    Hd = design(h, 'equiripple'); gd = grpdelay(Hd); gd = ceil(gd(1));
    ax = [x(1)*ones(1,2*gd) x x(end)*ones(1,gd)]; fx = filter(Hd,ax); fx = fx(gd*2+1:end-gd);
    ay = [y(1)*ones(1,2*gd) y y(end)*ones(1,gd)]; fy = filter(Hd,ay); fy = fy(gd*2+1:end-gd);
    az = [z(1)*ones(1,2*gd) z z(end)*ones(1,gd)]; fz = filter(Hd,az); fz = fz(gd*2+1:end-gd);
    fm = sqrt(fx.^2+fy.^2+fz.^2);
    
    figure;hold on;plot(fx,'b');plot(fy,'r');plot(fz,'g');plot(fm,'k');grid on;
    title('Filtered input accelerometer signals');legend('x','y','z','mag');
end

AccOut.x.signal = (x*2-(MinX+MaxX))/(MaxX-MinX);
AccOut.y.signal = (y*2-(MinY+MaxY))/(MaxY-MinY);
AccOut.z.signal = (z*2-(MinZ+MaxZ))/(MaxZ-MinZ);
AccOut.m.signal = sqrt(AccOut.x.signal.^2 + AccOut.y.signal.^2 + AccOut.z.signal.^2);

if (PlotFlag)
    figure;hold on;plot(AccOut.x.signal,'b');plot(AccOut.y.signal,'r');plot(AccOut.z.signal,'g');plot(AccOut.m.signal,'k');grid on;
    title('Calibrated accelerometer signals');legend('x','y','z','mag');
    
    % Filter calibrated data
    ax = [AccOut.x.signal(1)*ones(1,2*gd) AccOut.x.signal AccOut.x.signal(end)*ones(1,gd)]; fx = filter(Hd,ax); fx = fx(gd*2+1:end-gd);
    ay = [AccOut.y.signal(1)*ones(1,2*gd) AccOut.y.signal AccOut.y.signal(end)*ones(1,gd)]; fy = filter(Hd,ay); fy = fy(gd*2+1:end-gd);
    az = [AccOut.z.signal(1)*ones(1,2*gd) AccOut.z.signal AccOut.z.signal(end)*ones(1,gd)]; fz = filter(Hd,az); fz = fz(gd*2+1:end-gd);
    am = [AccOut.m.signal(1)*ones(1,2*gd) AccOut.m.signal AccOut.m.signal(end)*ones(1,gd)]; fm = filter(Hd,am); fm = fm(gd*2+1:end-gd);
    
    figure;hold on;plot(fx,'b');plot(fy,'r');plot(fz,'g');plot(fm,'k');grid on;
    title('Filtered calibrated accelerometer signals');legend('x','y','z','mag');
end

end

