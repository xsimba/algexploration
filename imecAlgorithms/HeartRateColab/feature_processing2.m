% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : freq_decision.m
%  Content    : freature processing for HR estimation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 21-10-2014
%  Modification and Version History:
%  | Developer | Version |    Date   |
%  | Yelei Li  |   2.0   | 21-10-2014|
% --------------------------------------------------------------------------------

function [CI_freq] = feature_processing2(PPG_freq, ACC_freq, activity_flag, blast_radius)
% PPG_freq       Frequency peak FFT bin number for PPG (signal)
% ACC_freq       Frequency peak FFT bin number for ACC (motion)
% activity_flag  Flag to indicate activity, 0:no activity, 1:activity
% blast_radius   Range of detection of corresponding frequencies
% CI_freq        Output flag indicating peak rejection, 0:rejected, 1:keep

CI_freq = ones(length(PPG_freq),1);

if activity_flag ~= 0
    [~,diff_freq] = intersect(PPG_freq,ACC_freq);
    if ~isempty (diff_freq)
        CI_freq(diff_freq) = 0; % Remove PPG local peaks which overlap with motion artifacts
    end
    if blast_radius>0,
        for k = 1:blast_radius,
            [~,diff_freq] = intersect(PPG_freq,ACC_freq+k);
            if ~isempty (diff_freq)
                CI_freq(diff_freq) = 0; % Remove PPG local peaks which overlap with motion artifacts
            end
            [~,diff_freq] = intersect(PPG_freq,ACC_freq-k);
            if ~isempty (diff_freq)
                CI_freq(diff_freq) = 0; % Remove PPG local peaks which overlap with motion artifacts
            end
        end
    end
end

end
