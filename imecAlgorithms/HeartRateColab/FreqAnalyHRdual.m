% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : FreqAnalyHR.m
%  Content    : Frequency analysis for heart rate estimation taking dual inputs for signal and motion
%  Package    :
%  Created by : Alex Young (alex.young@imec-nl.nl)
%  Start date : 08-09-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function [iHRs, rHRs, aHRs, iHRm, rHRm, aHRm, Npks, Npkm] = FreqAnalyHRdual(CsigSeg, CmotSeg, Params)
% CsigSeg      Circular signal containing either PPG, ECG or BioZ like waveforms representing cardiac activity for 1 sample buffer
% CmotSeg      Circular signal containing either ACC or PPG signals representing motion activity
% Params       Parameter structure
% iHRs         Signal: Heart rate value (bpm)
% rHRs         Signal: Raw (not enhanced) heart rate frequency bin number (0 = DC)
% aHRs         Signal: Amplitude of heart rate frequency component
% iHRm         Motion: Heart rate value (bpm)
% rHRm         Motion: Raw (not enhanced) heart rate frequency bin number (0 = DC)
% aHRm         Motion: Amplitude of heart rate frequency component
% Npks         Signal: Number of frequency peaks detected
% Npkm         Motion: Number of frequency peaks detected

iHRs = zeros(Params.MxPk,1); % Instantaneous signal HR, interpolated
rHRs = zeros(Params.MxPk,1); % Instantaneous signal HR, raw
aHRs = zeros(Params.MxPk,1); % Instantaneous signal HR, amplitude
iHRm = zeros(Params.MxPk,1); % Instantaneous motion HR, interpolated
rHRm = zeros(Params.MxPk,1); % Instantaneous motion HR, raw
aHRm = zeros(Params.MxPk,1); % Instantaneous motion HR, amplitude

fts = fft(CsigSeg)*2/Params.CsegLen; % Perform FFT
ftm = fft(CmotSeg)*2/Params.CsegLen;

fas = abs(fts(1:Params.CsegLen/2)); % Magnitude and cull to half length
fam = abs(ftm(1:Params.CsegLen/2));

FidxLo = floor(Params.FrMin*Params.CsegLen/Params.Fs); % Lowest frequency of interest
FidxHi = ceil(Params.FrMax*Params.CsegLen/Params.Fs); % Highest frequency of interest

% Limit frequency band just outside limits
fas(1:FidxLo-1) = 0;   % Remove baseline (1 below low limit)
fam(1:FidxLo-1) = 0;
fas(FidxHi+1:end) = 0; % Remove components above harmonics of max heart rate (1 above high limit)
fam(FidxHi+1:end) = 0;

% Gather statistics from spectrum over frequency region of interest
k3 = 2.0;

ftsstd = std(fas);
ftsmax = max(fas);
ftsmea = mean(fas);
ftssum = sum(fas(fas>ftsstd*k3)); % Only used for text output

ftmstd = std(fam);
ftmmax = max(fam);
ftmmea = mean(fam);
ftmsum = sum(fam(fam>ftmstd*k3)); % Only used for text output

% Threshold calculation
ThSig = Params.ThSigNoiseFloor + ftsmea*Params.ThSigMean + ftsstd*Params.ThSigStd;
ThMot = Params.ThMotNoiseFloor + ftmmea*Params.ThMotMean + ftmstd*Params.ThMotStd;

if (Params.debugFA == 1)
    disp(['Signal: Th = ',num2str(ThSig,3),'  Std = ',num2str(ftsstd,3),'  Max = ',num2str(ftsmax,3),'  Mean = ',num2str(ftsmea,3),'  PkSum = ',num2str(ftssum,3)]);
    disp(['Motion: Th = ',num2str(ThMot,3),'  Std = ',num2str(ftmstd,3),'  Max = ',num2str(ftmmax,3),'  Mean = ',num2str(ftmmea,3),'  PkSum = ',num2str(ftmsum,3)]);
    fx = (0:FidxHi)*60*Params.Fs/Params.CsegLen; % Frequency axis (bpm)
    figure;hold on;grid on;
    [ap, ~, ~] = plotyy(fx,fas(1:FidxHi+1),fx,fam(1:FidxHi+1));
    set(get(ap(1),'Ylabel'),'String','Signal')
    set(get(ap(2),'Ylabel'),'String','Motion (g)')
    xlabel('Frequency (bpm)');title('Spectra of signal and motion');
end

Npks = 0;
Npkm = 0;
for n = FidxLo+1:FidxHi-1 % Search frequencies between limits only (will use frequency bins one less and one more than n)
    if ((fas(n-1) <= fas(n))&&(fas(n) > fas(n+1))) % Detect peak in signal, where x(n-1) <= x(n) > x(n+1)
        yy = sqrt(fas(n)^2 + 0.8467*(fas(n-1)+fas(n+1))^2); % Amplitude enhancement (much better estimate than raw peak value)
        if (yy > ThSig) % Amplitude > Threshold?
            r1 = (fas(n+1)-fas(n-1))/fas(n); % Frequency enhancement
            xx = sqrt(abs(r1)).*sign(r1)*0.6020;
            fe = (n-1+xx)*Params.Fs/Params.CsegLen; % Conversion to frequency
            % Adjust amplitude based on frequency, if above Knee frequency, reduce amplitude as Knee frequency/estimated frequency
            if (fe > Params.FadjKneeSig), yy = yy*Params.FadjKneeSig/fe; end
            if (Npks < Params.MxPk) % Collect frequency and amplitude data until max number of peaks is reached
                aHRs(Npks+1) = yy;
                iHRs(Npks+1) = 60*fe; % Conversion to instantaneous HR
                rHRs(Npks+1) = n-1;   % FFT bin
                Npks = Npks+1;        % Bump counter
            end
        end
    end
    if ((fam(n-1) <= fam(n))&&(fam(n) > fam(n+1))) % Detect peak in motion
        yy = sqrt(fam(n)^2 + 0.8467*(fam(n-1)+fam(n+1))^2); % Amplitude enhancement
        if (yy > ThMot) % Amplitude > Threshold?
            r1 = (fam(n+1)-fam(n-1))/fam(n); % Frequency enhancement
            xx = sqrt(abs(r1)).*sign(r1)*0.6020;
            fe = (n-1+xx)*Params.Fs/Params.CsegLen; % Conversion to frequency            
            % Adjust amplitude based on frequency, if above Knee frequency, reduce amplitude as Knee frequency/raw frequency
            if (fe > Params.FadjKneeMot), yy = yy*Params.FadjKneeMot/fe; end
            if (Npkm < Params.MxPk) % Collect frequency and amplitude data until max number of peaks is reached
                aHRm(Npkm+1) = yy;
                iHRm(Npkm+1) = 60*fe; % Conversion to instantaneous HR
                rHRm(Npkm+1) = n-1;   % FFT bin
                Npkm = Npkm+1;        % Bump counter
            end
        end
    end
end
[~,i] = sort(aHRs(1:Npks),'descend'); % Signal: Sort amplitudes (highest with lowest indexes)
aHRs(1:Npks) = aHRs(i);
iHRs(1:Npks) = iHRs(i);
rHRs(1:Npks) = rHRs(i);
[~,i] = sort(aHRm(1:Npkm),'descend'); % Motion: Sort amplitudes (highest with lowest indexes)
aHRm(1:Npkm) = aHRm(i);
iHRm(1:Npkm) = iHRm(i);
rHRm(1:Npkm) = rHRm(i);

end

