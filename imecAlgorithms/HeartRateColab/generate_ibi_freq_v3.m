function [CI_freq, ibi_freq, amp_freq, timestep,index_seg_end, num_pks,action_flag] = generate_ibi_freq_v3(InputStream_PPG, InputStream_ACC, Action_ACC, Fs)

% General
if (~exist('Fs','var')), Params.Fs = 128; % Sampling Frequency
else                     Params.Fs = Fs;
end

Params.Len  = length(InputStream_PPG);
% Heart rate estimation by frequency analysis
Params.Troi  = 7.5;   % Region of interest duration (s)
Params.Tmar  = 0.5;   % Margin duration (s)
Params.Tupd  = 0.5;   % Update resolution (s)
Params.ThFA  = 0.10;  % 10% threshold to accept a frequency peak (normalised to 1 for highest peak)
Params.MxPk  = 20;    % Maximum number of correlation peaks.
Params.HRmin = 27.0;  % Minimum bpm
Params.HRmax = 240.0; % Maximum bpm
Params.Harm  = 2;     % Maximum harmonic

Params.ThSigNoiseFloor = 0.00; % Threshold noise floor component for signal
Params.ThSigMean       = 1.00; % Threshold mean component for signal
Params.ThSigStd        = 0.50; % Threshold std component for signal
Params.ThMotNoiseFloor = 0.005 ; % Threshold noise floor component for motion
Params.ThMotMean       = 0.75; % Threshold mean component for motion
Params.ThMotStd        = 1.50; % Threshold std component for motion

Params.FadjKneeSig =   2.5; % Signal knee frequency to adjust amplitude
Params.FadjKneeMot = 100.0; % Not required for motion, set to high value

Params.FrMin = Params.HRmin/60.0;
Params.FrMax = Params.HRmax*Params.Harm/60.0;

Params.SegROI  = round(Params.Fs*Params.Troi/2)*2; % Segment region of interest (even)
Params.SegMar  = round(Params.Fs*Params.Tmar/2)*2; % Segment margin (even)
Params.SegLen  = Params.SegROI+2*Params.SegMar;    % Segment length: |-Mar-|-----ROI-----|-Mar-|
Params.SegSkp  = round(Params.Fs*Params.Tupd);     % Segment skip
Params.CsegLen = Params.SegROI+Params.SegMar;      % Circular segment length: |-----ROI-----|\Mar\|

Params.xfseg = 0.5 - 0.5*cos(pi*(0:Params.SegMar-1)/Params.SegMar); % Cross fade function for margins (0 to 0.999)
Params.Nseg = floor((Params.Len-Params.SegLen)/Params.SegSkp) + 1; % UsableLen = SegLen + (n-1)*SegOvl

ibi_freq_raw = NaN(Params.MxPk,Params.Nseg); % Raw ridge data for HR - instantaneous raw HR value (bpm)
ibi_freq     = NaN(Params.MxPk,Params.Nseg); % Ridge data for HR - instantaneous HR value (bpm)
amp_freq     = NaN(Params.MxPk,Params.Nseg); % Ridge data for HR - instantaneous HR amplitude (normalised to 0 lag)
CI_freq      = NaN(Params.MxPk,Params.Nseg); % peak masking Indicator
index_seg_end = zeros(1,Params.Nseg);        %
num_pks       = zeros(1,Params.Nseg);
action_flag   = zeros(1,Params.Nseg);


for n = 0:Params.Nseg-1
    Seg_PPG  = InputStream_PPG((1:Params.SegLen)+n*Params.SegSkp); % Extract cardiac signal segment
    Seg_ACC  = InputStream_ACC((1:Params.SegLen)+n*Params.SegSkp);
    Seg_actionACC  = Action_ACC((1:Params.SegLen)+n*Params.SegSkp);
    
    Cseg_PPG = [Seg_PPG(Params.SegMar+1:Params.SegMar+Params.SegROI) Seg_PPG(1:Params.SegMar).*Params.xfseg+Seg_PPG(Params.SegMar+Params.SegROI+1:end).*(1-Params.xfseg)]; % Linear to circular conversion
    Cseg_ACC = [Seg_ACC(Params.SegMar+1:Params.SegMar+Params.SegROI) Seg_ACC(1:Params.SegMar).*Params.xfseg+Seg_ACC(Params.SegMar+Params.SegROI+1:end).*(1-Params.xfseg)];
    
    index_seg_end(n+1) = Params.SegLen + n*Params.SegSkp;  % Add filter and FFT delays
    action_flag(n+1)   = action_detector(Seg_actionACC);
    
    Params.debugFA = (n == round(450.5/Params.Tupd)); % Set debug time (s) or negative to disable
    
    %iHRs, rHRs, aHRs, iHRm, rHRm, aHRm, Npks,         Npkm
    [iPPG, rPPG, aPPG, iACC, rACC, aACC, num_pks(n+1), num_pkm] = FreqAnalyHRdual(Cseg_PPG, Cseg_ACC, Params);
    % rXXX is the FFT bin number corresponding to frequency, referenced to 0 for DC
    % iXXX is the enhanced heart rate frequency (bpm)
    % aXXX is the amplitude, output is sorted by decreasing amplitude for all outputs
    
    if (0) % Select masking method
        blast_radius_bin = 1;   % In units of FFT frequency bins (integer)
        CI_freq(:,n+1) = feature_processing2(rPPG, rACC, action_flag(n+1), blast_radius_bin);
    else
        blast_radius_bpm = 7.5; % Precise comparison in bpm
        CI_freq(:,n+1) = feature_processing_bpm(iPPG, iACC, num_pks(n+1), num_pkm, action_flag(n+1), blast_radius_bpm);
    end
    
    if Params.debugFA
        disp('Debug: from Params.debugFA set');
        disp('rACC          rPPG         iPPG          aPPG            CI_freq');
        q = zeros(Params.MxPk,7);
        q(:,1) = rACC;
        q(:,2) = rPPG;
        q(:,3) = iACC;
        q(:,4) = iPPG;
        q(:,5) = aPPG;
        q(:,6) = aACC;
        q(:,7) = CI_freq(:,n+1);
        disp(num2str(q,5));
        disp(['action_flag = ',num2str(action_flag(n+1)),'  num_pks = ',num2str(num_pks(n+1),2),'  num_pkm = ',num2str(num_pkm,2)]);
    end
    
    ibi_freq_raw(:,n+1) = rPPG*Params.Fs/Params.CsegLen; % Convert FFT bin to frequency (Hz)
    ibi_freq(:,n+1)     = iPPG/60;                       % Convert  heart rate (bpm) to frequency (Hz)
    amp_freq(:,n+1)     = aPPG;
end

timestep = Params.Tupd;
end

