%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <AVGBDet2>
% Content   : Calculate average beat and estimate SNR of ECG and PPG data 
% Version   : GIT 2
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
% Signal denoising by mean pattern substraction.
% Adapted from:
% Inaki RL - IMEC (Eindhoven)
% by E.C. Wentink . .--
% %  Modification and Version History: 
%  | Developer  | Version |    Date    |     Changes      |
%  | Inaki RL   | 0.1     | 05/2009 | first version    |
%  | EC Wentink | 0.2     | 16/12/2013 | works for ECG some bug fixes    |
%  | EC Wentink | 0.3     | 18/02/2015 | works for ECG and PPG some bug fixes    |
% 
% Description:
%   Clean a signal by removing a repetitive pattern. The pattern is
%   calculated by alignment of several patterns to fidutial times and
%   computing the mean value.
%
% Inputs:
%   HRD = data in
%   qrs = the detected r peaks in the data
%   Fs = sample frequency
%   tt = the percentage of the window to take before the detected beat
%
%  Outputs:
%  SNR = the estimation SNR
%  noise = the noise
%  d = first attempt at template matching to the average waveform
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [SNR,STDqrs,d]=AVGBDet2(HRD,RP,Fs,minHR,tt)
%% subtract the mean of the data in the window
HRD=HRD-mean(HRD);
%% get rid of negative values from peak detector    
noqrs = sort(find(RP<=0),'descend');
RP(noqrs)=[];

%% 
RP=floor(RP(:)'*Fs); % from time to samples
% RR = RP(2:end)-RP(1:end-1); % calculate RR-interval (if you want to make window adaptive)
windavg=minHR*Fs; %the window size, based on the "minimum" HR,can also be adaptive by using the min RR-interval

%% Make data long enough
HRD_t = [ones(windavg,1)*HRD(1); HRD; ones(windavg,1)*HRD(end)]; % make data long enough)
RP1=floor(RP+windavg); 
RPcomp=zeros(length(RP),windavg); % make array
% tt=0.3; % set the time in seconds where to start the window before the R-peak
%% start the cutting of the individual beats (partly before R-peak and the rest after)
for i=1:1:(length(RP1))
    if i ==1
        mi = floor((RP1(1)-round(windavg*tt))+1);
        ma = (RP1(1)-round(windavg*tt))+windavg;
        if mi<1
           RPcomp_temp=HRD_t(1:ma);
           RPcomp(1,:)= [zeros(1,(abs(mi)+1)) RPcomp_temp'];
           maxpeak(i)=HRD_t(ceil(RP1(i))); % determine the height of the peaks    
        else
           RPcomp(1,:)=HRD_t(mi:ma); % get each beat by looking at the 0.3* the window before the peak and 0.7 after the peak
           maxpeak(i)=HRD_t(ceil(RP1(i))); % determine the height of the peaks    
        end
    else
        stap = floor((RP1(i)-round(windavg*tt))+1);
        stop = floor((RP1(i)-round(windavg*tt))+windavg);
        RPcomp(i,:)=HRD_t(stap: stop)-mean(HRD_t(stap: stop)); % get each beat by looking at the 0.3* the window before the peak and 0.7 after the peak
        maxpeak(i)=HRD_t(ceil(RP1(i))); % determine the height of the peaks
    end
end
meanqrs=mean(RPcomp); % mean of all the beats
STDqrs=std(RPcomp); % std of all the beats
qrsp=mean(maxpeak); %the mean value of all the peaks
maxav=meanqrs(round(windavg*tt)); % assuming the peak of all the beats is at 0.3* the window (thats were you put is) this will give the max/top of the average beat
avgbeat=ones(1,(length(HRD))+(windavg*3))*mean(HRD);


%% Make new signal with only the "average beat" aligned with the (peaks of the) beats in the original data
for i=1:length(RP)
     stap2 = floor(RP1(i)-round(windavg*tt)+1);
     stop2 = floor(RP1(i)-round(windavg*tt)+windavg);
     avgbeat(stap2:stop2)=meanqrs/maxav*mean(qrsp); % remake the signal with "average" beats only
end
avgbeat=avgbeat(windavg+1:ceil(windavg+length(HRD)));
%% calculate the noise
noise=HRD'-avgbeat;
%% calculate the signal
signal=HRD-noise'; % Calculate signal by subtracting artifacts from signal
%% calculate the SNR
SNR = (rms(signal)/rms(noise))^2;


%% check meanqrs with template
load('wave2.mat')
wave2(1:20)=[];
d = dtw_c(meanqrs(30:end)',wave2'-mean(wave2));

%%    Plot the data if needed
if (0)
figure(105);
subplot(2,2,1); plot(HRD);text(20,10,num2str(SNR),'FontSize',14);title('Original')
subplot(2,2,2); [~,~,h2] = plotyy([0:1:length(meanqrs)-1],RPcomp',[0:1:length(meanqrs)-1],meanqrs);text(10,10,num2str(d),'FontSize',14);title('Original');title('Individual beats') ;set(h2,'LineWidth',3);
subplot(2,2,3); plot(signal);title('Signal')
subplot(2,2,4); plot(noise);title('Noise');
end
end



