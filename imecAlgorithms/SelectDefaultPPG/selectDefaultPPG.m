function data = selectDefaultPPG( data )

    ppg_channels = {'a','b','c','d','e','f','g','h'};
    
    mode_all_channels = zeros(numel(ppg_channels,1));
    freq_all_channels = zeros(numel(ppg_channels,1));
    
    for cIdx = 1:numel(ppg_channels)
        
        if isfield(data.ppg,ppg_channels{cIdx})
            [mode_all_channels(cIdx),freq_all_channels(cIdx)] = mode(data.ppg.(ppg_channels{cIdx}).CI_raw);
        end
        
    end
    
    final_decision = mode_all_channels.*freq_all_channels ; 
    
    [~,maxIdx] = max(final_decision);
    
    all_maxs = find(final_decision==final_decision(maxIdx));
    
    if all_maxs>1
        maxIdx = all_maxs(end);
    end
    
    data.defaultPPG = ppg_channels{maxIdx};

    
end


