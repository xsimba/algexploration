
function [output] = CalcPAT(input)
%% Check pat
% close all;

for e = 1:1:length(input.ecg.beats)-1
    RR_t(e)= input.ecg.beats(e+1)-input.ecg.beats(e);
    if  RR_t(e) < 2  &&  RR_t(e) > 0.25 % min 30bpm & max 240 bpm-> RR-int should be in between
        CI_ecgHB(e) = 1;
    else 
        CI_ecgHB(e) = 0;
    end
    
      Bppg1_t = find(input.ppg.a.beats< input.ecg.beats(e+1) & input.ppg.a.beats> input.ecg.beats(e));
      Bppg2_t = find(input.ppg.b.beats< input.ecg.beats(e+1) & input.ppg.b.beats> input.ecg.beats(e));
      Bppg3_t = find(input.ppg.c.beats< input.ecg.beats(e+1) & input.ppg.c.beats> input.ecg.beats(e));
      Bppg4_t = find(input.ppg.d.beats< input.ecg.beats(e+1) & input.ppg.d.beats> input.ecg.beats(e));
if  isfield(input.ppg ,'e')
      Bppg5_t = find(input.ppg.e.beats< input.ecg.beats(e+1) & input.ppg.e.beats> input.ecg.beats(e));
      Bppg6_t = find(input.ppg.f.beats< input.ecg.beats(e+1) & input.ppg.f.beats> input.ecg.beats(e));
      end
      t_check=1;
      [pat1(e), pat1i(e), CI_pat1(e)] = PAT_checker(Bppg1_t,input.ppg.a.beats,input.ecg.beats,e,t_check);
      [pat2(e), pat2i(e), CI_pat2(e)] = PAT_checker(Bppg2_t,input.ppg.b.beats,input.ecg.beats,e,t_check);
      [pat3(e), pat3i(e), CI_pat3(e)] = PAT_checker(Bppg3_t,input.ppg.c.beats,input.ecg.beats,e,t_check);
      [pat4(e), pat4i(e), CI_pat4(e)] = PAT_checker(Bppg4_t,input.ppg.d.beats,input.ecg.beats,e,t_check);
if  isfield(input.ppg ,'e')
      [pat5(e), pat5i(e), CI_pat5(e)] = PAT_checker(Bppg5_t,input.ppg.e.beats,input.ecg.beats,e,t_check);
      [pat6(e), pat6i(e), CI_pat6(e)] = PAT_checker(Bppg6_t,input.ppg.f.beats,input.ecg.beats,e,t_check);
      end
end     
     
clear Bppg1_t Bppg2_t Bppg3_t Bppg4_t
    

CI_pat1= CI_ecgHB.*CI_pat1;pat1 = pat1.*CI_pat1;
CI_pat2= CI_ecgHB.*CI_pat2;pat2 = pat2.*CI_pat2;
CI_pat3= CI_ecgHB.*CI_pat3;pat3 = pat3.*CI_pat3;
CI_pat4= CI_ecgHB.*CI_pat4;pat4 = pat4.*CI_pat4;
if  isfield(input.ppg ,'e')
CI_pat5= CI_ecgHB.*CI_pat5;pat5 = pat5.*CI_pat5;
CI_pat6= CI_ecgHB.*CI_pat6;pat6 = pat6.*CI_pat6;
end

RR_ti = RR_t.*CI_pat1;
figure;plot(RR_t);hold on;plot(RR_ti,'r')

[RR_pat1,pat1,pat1_2, pat1_3,CI_pat2_1,HR1] = RR2PAT2RR(pat1,RR_ti);
[RR_pat2,pat2,pat2_2, pat2_3,CI_pat2_2,HR2] = RR2PAT2RR(pat2,RR_ti);
[RR_pat3,pat3,pat3_2, pat3_3,CI_pat2_3,HR3] = RR2PAT2RR(pat3,RR_ti);
[RR_pat4,pat4,pat4_2, pat4_3,CI_pat2_4,HR4] = RR2PAT2RR(pat4,RR_ti);
if  isfield(input.ppg ,'e')
[RR_pat5,pat5,pat5_2, pat5_3,CI_pat2_5,HR5] = RR2PAT2RR(pat5,RR_ti);
[RR_pat6,pat6,pat6_2, pat6_3,CI_pat2_6,HR6] = RR2PAT2RR(pat6,RR_ti);
end

figure;plot(HR1);
% HR2
% HR3
% HR4


CI_pat_T =[CI_pat2_1;CI_pat2_2;CI_pat2_3;CI_pat2_4];
HR_T = [HR1;HR2;HR3;HR4];
pat_T =[pat1_2;pat2_2;pat3_2;pat4_2];
if  isfield(input.ppg ,'e')
CI_pat_T =[CI_pat2_1;CI_pat2_2;CI_pat2_3;CI_pat2_4;CI_pat2_5;CI_pat2_6];
HR_T = [HR1;HR2;HR3;HR4;HR5;HR6];
pat_T =[pat1_2;pat2_2;pat3_2;pat4_2;pat5_2;pat6_2];
end
[CI_ch] = PATandCI2Chan(CI_pat_T);

thepat = pat_T(CI_ch(1),:) ;
thepat=thepat(thepat>0);
theHR =  HR_T(CI_ch(1),:);
theHR= theHR(theHR>0);

if max(thepat)>0
thepat=thepat(1,2);
HR_pat = round(theHR(1,2));
else
    thepat = 0;
end
% 
figure(13);%

if  isfield(input.ppg ,'e')
D(1)=subplot(9,1,1);plot(input.time.t,input.ppg.a.signal-median(input.ppg.a.signal));hold on;plot(input.ppg.a.beats,input.ppg.a.DB.BeatAmp-median(input.ppg.a.signal),'ro');title('Beats ppg 1 Green')
D(2)=subplot(9,1,2);plot(input.time.t,input.ppg.b.signal-median(input.ppg.b.signal));hold on;plot(input.ppg.b.beats,input.ppg.b.DB.BeatAmp-median(input.ppg.b.signal),'ro');title('Beats ppg 2 Blue')
D(3)=subplot(9,1,3);plot(input.time.t,input.ppg.c.signal-median(input.ppg.c.signal));hold on;plot(input.ppg.c.beats,input.ppg.c.DB.BeatAmp-median(input.ppg.c.signal),'ro');title('Beats ppg 3 IR')
D(4)=subplot(9,1,4);plot(input.time.t,input.ppg.d.signal-median(input.ppg.d.signal));hold on;plot(input.ppg.d.beats,input.ppg.d.DB.BeatAmp-median(input.ppg.d.signal),'ro');title('Beats ppg 4 Red')
D(5)=subplot(9,1,5);plot(input.time.t,input.ppg.e.signal-median(input.ppg.e.signal));hold on;plot(input.ppg.e.beats,input.ppg.e.DB.BeatAmp-median(input.ppg.e.signal),'ro');title('Beats ppg 5 Green')
D(6)=subplot(9,1,6);plot(input.time.t,input.ppg.f.signal-median(input.ppg.f.signal));hold on;plot(input.ppg.f.beats,input.ppg.f.DB.BeatAmp-median(input.ppg.f.signal),'ro');title('Beats ppg 6 Green')
D(7)=subplot(9,1,7);plot(input.time.t,input.ecg.signal);hold on;plot(input.ecg.beats(:),input.ecg.DB.BeatAmp,'ro');title('Beats ecg')
D(8)=subplot(9,1,8);plot(pat1i(15:end),pat1_2','b+');hold on;plot(pat2i(15:end),pat2_2','r+');plot(pat3i(15:end),pat3_2','m+');plot(pat4i(15:end),pat4_2','c+');plot(pat5i(15:end),pat5_2','g+');hold on;plot(pat6i(15:end),pat6_2','k+');ylabel('pat');%legend('pat ppg1','pat ppg2','pat ppg3','pat ppg4','pat ppg5','pat ppg6')
D(9)=subplot(9,1,9);plot(pat1i(15:end),CI_pat2_1','b+');hold on;plot(pat2i(15:end),CI_pat2_2','b+');plot(pat3i(15:end),CI_pat2_3','m+');plot(pat4i(15:end),CI_pat2_4','c+');plot(pat5i(15:end),CI_pat2_5','g+');plot(pat6i(15:end),CI_pat2_6','k+');linkaxes(D,'x');ylabel('CI');legend('ppg1','ppg2','ppg3','ppg4','ppg5','ppg6');text(1,2, ['Pick channel: ' num2str(CI_ch) ' , the pat is: ' num2str(thepat) ' and the HR is: ' num2str(HR_pat) ],'FontSize',14);
else
D(1)=subplot(7,1,1);plot(input.time.t,input.ppg.a.signal-median(input.ppg.a.signal));hold on;plot(input.ppg.a.beats,input.ppg.a.DB.BeatAmp-median(input.ppg.a.signal),'ro');title('Beats ppg 1 Green')
D(2)=subplot(7,1,2);plot(input.time.t,input.ppg.b.signal-median(input.ppg.b.signal));hold on;plot(input.ppg.b.beats,input.ppg.b.DB.BeatAmp-median(input.ppg.b.signal),'ro');title('Beats ppg 2 Blue')
D(3)=subplot(7,1,3);plot(input.time.t,input.ppg.c.signal-median(input.ppg.c.signal));hold on;plot(input.ppg.c.beats,input.ppg.c.DB.BeatAmp-median(input.ppg.c.signal),'ro');title('Beats ppg 3 IR')
D(4)=subplot(7,1,4);plot(input.time.t,input.ppg.d.signal-median(input.ppg.d.signal));hold on;plot(input.ppg.d.beats,input.ppg.d.DB.BeatAmp-median(input.ppg.d.signal),'ro');title('Beats ppg 4 Red')
D(5)=subplot(7,1,5);plot(input.time.t,input.ecg.signal);hold on;plot(input.ecg.beats,input.ecg.DB.BeatAmp,'ro');title('Beats ecg')
D(6)=subplot(7,1,6);plot(pat1i(15:end),pat1_2','b+');hold on;plot(pat2i(15:end),pat2_2','r+');plot(pat3i(15:end),pat3_2','m+');plot(pat4i(15:end),pat4_2','c+');ylabel('pat');
D(7)=subplot(7,1,7);plot(pat1i(15:end),CI_pat2_1','b+');hold on;plot(pat2i(15:end),CI_pat2_2','r+');plot(pat3i(15:end),CI_pat2_3','m+');plot(pat4i(15:end),CI_pat2_4','c+');linkaxes(D,'x');ylabel('CI');legend('ppg1','ppg2','ppg3','ppg4');text(1,2, ['Pick channel: ' num2str(CI_ch) ' , the pat is: ' num2str(thepat) ' and the HR is: ' num2str(HR_pat) ],'FontSize',14);
end


disp(['choose channel:  ' num2str(CI_ch) ' , the pat is: ' num2str(thepat) ' and the HR is: ' num2str(HR_pat) ])

output=input;
output.pat.pat = thepat;
output.pat.Channel = CI_ch;
output.pat.HR = HR_pat;
output.ppg.a.pat = pat1_2;
output.ppg.a.CI_pat = CI_pat2_1;
output.ppg.b.pat = pat2_2;
output.ppg.b.CI_pat = CI_pat2_2;
output.ppg.c.pat = pat3_2;
output.ppg.c.CI_pat = CI_pat2_3;
output.ppg.d.pat = pat4_2;
output.ppg.d.CI_pat = CI_pat2_4;
if  isfield(input.ppg ,'e')
    output.ppg.e.pat = pat5_2;
output.ppg.e.CI_pat = CI_pat2_5;
output.ppg.f.pat = pat6_2;
output.ppg.f.CI_pat = CI_pat2_6;
end