% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <PAT_checker>
% Content   : Script for checking the existance of (good) pat
% Version   : GIT 2
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% first checks if a ppg beat is found. if no pat is nan else pat is
% calculated
% than check is a pat is found if it is between the boundary conditions of
% 150ms<pat<350 ms. if yest CI_temp  is one else it is zero.
% by E.C. Wentink 30/10/2014 . .--
% %  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%%%%%%%%%%%%%%%%%%%%%%
function  [pat, pati, CI_pat, patRR] = PAT_checker(Bppg_t,e,BTS_ppg,BTS_ecg,ppg_CI_beat,min_CI_ppg)
          if size(Bppg_t,2) == 0
             pat(1:3)=nan;
             pati(1:4) =nan;
          elseif  ppg_CI_beat(Bppg_t)>=min_CI_ppg
             patRR(1) = BTS_ppg(1,Bppg_t)-BTS_ecg(e);
             patRR(2) = BTS_ecg(e);
             pat(1) =  BTS_ppg(1,Bppg_t)-BTS_ecg(e); % the pat
             pat(2) =  BTS_ppg(2,Bppg_t)-BTS_ppg(4,Bppg_t); % the PP to Foot
             if isnan(BTS_ppg(4,Bppg_t)) %???????????????????????? should check BTS_ppg(4,Bppg_t+1)
                pat(3) =nan;
             else
                 if Bppg_t < length(BTS_ppg)-2
                pat(3) = BTS_ppg(4,Bppg_t+1)-BTS_ecg(e);%the Foot to PP    
                 else
                     pat(3)=nan;
                 end
             end
             pati(1)= BTS_ppg(1,Bppg_t); % index upstroke
             pati(2)= BTS_ppg(2,Bppg_t); % index foot
             if Bppg_t < length(BTS_ppg)-2
                pati(3)= BTS_ppg(4,Bppg_t+1); % index primary peak belonging to foot and upstroke
             else
                pati(3)=nan; %???????????????????????? should be pati(3) = nan
             end
             pati(4)= BTS_ppg(4,Bppg_t); % previous primary peak
          else  
             pat(1:3)=nan;
             pati(1:4) =nan;
             patRR(1:2) = nan;
         end
      
      
%% CI pat %%%%%%
% check if the calculated pat is within the physiological limits of
% 150-400ms else the CI becomes 0 (the Pat remains)
      if pat(1)<0.4 && pat(1)>0.15
         CI_pat = 1;       
      else
         CI_pat = 0;
         pat(1:3)=nan;
         pati(1:4) =nan;
         patRR(1:2) = nan; %???????????????????????? remark: this invalidates 2 RR intervals when doing diff(patRR(...))
         %patRR should not be NaN, otherwise HR is calculated wrongly
      end

