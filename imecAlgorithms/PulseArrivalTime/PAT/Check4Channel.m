% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Check4Channel>
% Content   : Script for checking the best channel for PAT selection
% Version   : GIT 2
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Simple script, checks for hte max in the CI of the PAT channels and
% selects the best based on that for further use in BP calculation.
% by E.C. Wentink 30/10/2014 . .--
% %  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%%%%%%%%%%%%%%%%%%%%%%

function [output] = Check4Channel(input)
output = input;

%parameters of all ppg channels in a row
CIs = [input.ppg.a.pat.CI_pat,input.ppg.b.pat.CI_pat,input.ppg.c.pat.CI_pat,input.ppg.d.pat.CI_pat];
Pats= [input.ppg.a.pat.pat2.signal,input.ppg.b.pat.pat2.signal,input.ppg.c.pat.pat2.signal,input.ppg.d.pat.pat2.signal];
Patstds = [input.ppg.a.pat.pat2std,input.ppg.b.pat.pat2std,input.ppg.c.pat.pat2std,input.ppg.d.pat.pat2std];
HR = [input.ppg.a.pat.HR2,input.ppg.b.pat.HR2,input.ppg.c.pat.HR2,input.ppg.d.pat.HR2];
HRstd = [input.ppg.a.pat.HR2std,input.ppg.b.pat.HR2std,input.ppg.c.pat.HR2std,input.ppg.d.pat.HR2std];

%parameters of all ppg channels in a row
if isfield(input.ppg ,'e')
CIs = [input.ppg.a.pat.CI_pat,input.ppg.b.pat.CI_pat,input.ppg.c.pat.CI_pat,input.ppg.d.pat.CI_pat,input.ppg.e.pat.CI_pat,input.ppg.f.pat.CI_pat];
Pats= [input.ppg.a.pat.pat2.signal,input.ppg.b.pat.pat2.signal,input.ppg.c.pat.pat2.signal,input.ppg.d.pat.pat2.signal,input.ppg.e.pat.pat2.signal,input.ppg.f.pat.pat2.signal];
Patstds = [input.ppg.a.pat.pat2std,input.ppg.b.pat.pat2std,input.ppg.c.pat.pat2std,input.ppg.d.pat.pat2std,input.ppg.e.pat.pat2std,input.ppg.f.pat.pat2std];
HR = [input.ppg.a.pat.HR2,input.ppg.b.pat.HR2,input.ppg.c.pat.HR2,input.ppg.d.pat.HR2,input.ppg.e.pat.HR2,input.ppg.f.pat.HR2];
HRstd = [input.ppg.a.pat.HR2std,input.ppg.b.pat.HR2std,input.ppg.c.pat.HR2std,input.ppg.d.pat.HR2std,input.ppg.e.pat.HR2std,input.ppg.f.pat.HR2std];
end

%parameters of all ppg channels in a row (always the case from now on...)
if isfield(input.ppg ,'g')
CIs = [input.ppg.a.pat.CI_pat,input.ppg.b.pat.CI_pat,input.ppg.c.pat.CI_pat,input.ppg.d.pat.CI_pat,input.ppg.e.pat.CI_pat,input.ppg.f.pat.CI_pat,input.ppg.g.pat.CI_pat,input.ppg.h.pat.CI_pat];
Pats= [input.ppg.a.pat.pat2.signal,input.ppg.b.pat.pat2.signal,input.ppg.c.pat.pat2.signal,input.ppg.d.pat.pat2.signal,input.ppg.e.pat.pat2.signal,input.ppg.f.pat.pat2.signal,input.ppg.g.pat.pat2.signal,input.ppg.h.pat.pat2.signal];
Patstds = [input.ppg.a.pat.pat2std,input.ppg.b.pat.pat2std,input.ppg.c.pat.pat2std,input.ppg.d.pat.pat2std,input.ppg.e.pat.pat2std,input.ppg.f.pat.pat2std,input.ppg.g.pat.pat2std,input.ppg.h.pat.pat2std];
HR = [input.ppg.a.pat.HR2,input.ppg.b.pat.HR2,input.ppg.c.pat.HR2,input.ppg.d.pat.HR2,input.ppg.e.pat.HR2,input.ppg.f.pat.HR2,input.ppg.g.pat.HR2,input.ppg.h.pat.HR2];
HRstd = [input.ppg.a.pat.HR2std,input.ppg.b.pat.HR2std,input.ppg.c.pat.HR2std,input.ppg.d.pat.HR2std,input.ppg.e.pat.HR2std,input.ppg.f.pat.HR2std,input.ppg.g.pat.HR2std,input.ppg.h.pat.HR2std];
pat_timestamps = [input.ppg.a.pat.pat2.timestamps,input.ppg.b.pat.pat2.timestamps,input.ppg.c.pat.pat2.timestamps,input.ppg.d.pat.pat2.timestamps,input.ppg.e.pat.pat2.timestamps,input.ppg.f.pat.pat2.timestamps,input.ppg.g.pat.pat2.timestamps,input.ppg.h.pat.pat2.timestamps];
end

%search for first highest CI
checka = find(CIs==max(CIs),1);

%assign parameters found at first highest CI to data struct
output.pat.pat.signal = Pats(1,checka)';
output.pat.pat.timestamps = pat_timestamps(1,checka)';
output.pat.HR = HR(1,checka)';
output.pat.patSD = Patstds(1,checka)';
output.pat.HRSD = HRstd(1,checka)';
output.pat.CI_pat = max(CIs);


%check in which of the appended ppg channels the maximum CI is detected
%currently disabled

% if checka <= length(input.ppg.a.pat.CI_pat)
%    output.pat.channel = 'a' ;
% elseif checka <= 2*length(input.ppg.a.pat.CI_pat) && checka > length(input.ppg.a.pat.CI_pat)
%    output.pat.channel = 'b' ;
% elseif checka <= 3*length(input.ppg.a.pat.CI_pat) && checka > 2*length(input.ppg.a.pat.CI_pat)
%    output.pat.channel = 'c' ;
% elseif checka <= 4*length(input.ppg.a.pat.CI_pat) && checka > 3*length(input.ppg.a.pat.CI_pat)
%    output.pat.channel = 'd' ;
% elseif checka <= 5*length(input.ppg.a.pat.CI_pat) && checka > 4*length(input.ppg.a.pat.CI_pat)
%   output.pat.channel = 'e' ;
% elseif checka <= 6*length(input.ppg.a.pat.CI_pat) && checka > 5*length(input.ppg.a.pat.CI_pat)
%    output.pat.channel = 'f' ;
% elseif checka <= 7*length(input.ppg.a.pat.CI_pat) && checka > 6*length(input.ppg.a.pat.CI_pat)    
%    output.pat.channel = 'g' ;
% else 
%    output.pat.channel = 'h' ;
% end

%Choose PPG channel based on ppg selection in the band (this selection should be implemented in Matlab as well!)
%Only the choice during spot check is relevant (there might be a different channel chosen before/after spot check)!

spotcheck_on = input.spot_check.timestamps(input.spot_check.signal == 1);
ppg_during_spotcheck = round(mean(input.ppg_selection.signal(input.ppg_selection.timestamps > spotcheck_on(1) & input.ppg_selection.timestamps < spotcheck_on(end))));

if ppg_during_spotcheck == 0
    output.pat.channel = 'a' ;
elseif ppg_during_spotcheck == 1
    output.pat.channel = 'b' ;
elseif ppg_during_spotcheck == 2
    output.pat.channel = 'c' ;
elseif ppg_during_spotcheck == 3
    output.pat.channel = 'd' ;
elseif ppg_during_spotcheck == 4
    output.pat.channel = 'e' ;
elseif ppg_during_spotcheck == 5
    output.pat.channel = 'f' ;
elseif ppg_during_spotcheck == 6
    output.pat.channel = 'g' ;
elseif ppg_during_spotcheck == 7
    output.pat.channel = 'h' ;
end

output.pat.pat1 = eval(['output.ppg.' output.pat.channel '.pat.pat2(1,:);']);
output.pat.HR1 = eval(['output.ppg.' output.pat.channel '.pat.HR2(1,:);']);
output.pat.HRSD1 = eval(['output.ppg.' output.pat.channel '.pat.HR2std(1,:);']);
output.pat.patSD1 = eval(['output.ppg.' output.pat.channel '.pat.pat2std(1,:);']);
output.pat.CI_pat = eval(['output.ppg.' output.pat.channel '.pat.CI_pat(1,:);']);

max(CIs);





