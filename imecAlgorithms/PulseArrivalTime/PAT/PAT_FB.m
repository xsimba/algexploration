% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <PAT>
% Content   : Main script for PAT calculation
% Version   : 3.6
% Author    : F. Beutel (fabian.beutel@imec-nl.nl)
%  Modification and Version History:
%  | Developer | Version |    Date    |     Changes      |
%  | F. Beutel |   3.0   |  03/12/14  | Start of PAT algo refactoring |
%  | F. Beutel |   3.2   |  14/12/14  | Bugfixes, exclusion of debug plots, compatibility with old data, documentation (to be completed) etc. |
%                                     | For now omplete functionality assumed --> Function must be tested with different low/high-quality data |
%  | F. Beutel |   3.3   |  03/02/15  | Set SpotCheck to only terminate at 3 instead of 2 OR 3 |
%  | F. Beutel |   3.4   |  11/02/15  | Set SpotCheck to reset at intermediate 2 |
%  | F. Beutel |   3.5   |  12/02/15  | Refactor flag orientation to make algo agnostic to high or low HRs|
%  | F. Beutel |   3.6   |  11/03/15  | PATsd not allowed to be 0 (single PAT in Spotcheck), Variability compared to last valid PAT insted of last (possibly invalid) PAT |
%  | F. Beutel | 0.3.6   |  12/03/15  | Same as 3.6: Introduction of new version tracking |
%  | F. Beutel | 0.4.0   |  17/03/15  | Incorporation of upstroke gradient and primary peak-to-foot amplitude |

% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% This function computes a PAT and HR average, incl their SDs per Spot Check.
% These quantities serve as input for the following BP computation algorithm.
% Depending on the quality of signals and performance of previous algorithms,
% it outputs either an average of all valid PATs in the current Spot Check
% OR ideally the average of a 10sec window of high quality PATs (i.e. strictly
% consecutive PATs within defined boundaries and with relatively constant variabilty).

% Input:
    % - 'input':
    %   data struct from previous algos, itemization of relevant fields
    %   -- LeadOn/LeadOff (i.e. 'input.ecg_lead')
    %   -- Spot Check (i.e. 'input.spot_check')
    %   -- ECG R peaks (i.e. 'input.ecg.bd.rpeak')
    %   -- PPG upstrokes (i.e. 'input.ppg.(chan).bd.upstroke)
    % - 'chan':
    %   ppg channel

% Output:
    % - 'pat_raw':
    %   All the single detected PATs
    % - 'pat_avg':
    %   Average of all valid PATs in Spot Check 
    % - 'pat_sd':
    %   Standard deviation of all valid PATs in Spot Check
    % - 'pat_for_BP':
    %   Single averaged PAT (EITHER the last value in 'pat_avg' after 20sec OR the average of 10sec of high quality PATs)
    % - 'pat_sd_for_BP':
    %   Single SD of PATs (EITHER the last value in 'pat_avg' after 20sec OR the SD of 10sec of high quality PATs)
    
    % - 'HR_raw':
    %   All the single computed HRs
    % - 'HR_avg':
    %   Average of all valid HRs in Spot Check
    % - 'HR_sd':
    %   Standard deviation of all valid HRs in Spot Check
    % - HR_for_BP (HR):
    %   Single averaged HR corresponding to pat_for_BP
    % - HR_sd_for_BP (HR):
    %   Single SD of HRs corresponding to pat_for_BP
    
    % - All parameters for upstroke (PPGs) gradient 
    % - All parameters for primary peak (PP) to foot (FT) amplitude
    
    % - 'pat_raw_CI':
    %   -- outputted per potential PAT (i.e. per R peak) to indicate the quality of PAT outputted for BP computation    
    %   -- depends on error flags from HR and PAT computation
    %   -- either 5 if avg of high quality PATs is calculated or maximal 4 depending on percentage of valid PATs in Spot Check
  
    % - 'pat_valid_avg_CI':
    %   -- outputted per potential PAT containing the CI computed at the last valid PAT. 
    %   -- is the CI associated with the average of all valid PATs in Spot Check
    
    % Output is made available per PPG channel and also in root of the data struct for last execution of this function 
    

function [output] = PAT(input, chan)

chan = chan(end);

% main input parameters
ecg_r = input.ecg.bd.rpeak; %timestamps ECG R peaks
ppg_s = eval(['input.ppg.' chan '.bd.upstroke']); %timestamps PPGs upstrokes
%ppg_s = eval(['input.ppg.' chan '.bd.foot']); %timestamps PPGs upstrokes
ppg_upstgrad = eval(['input.ppg.' chan '.bd.upstgrad'])
ppg_pp_ft_amp = eval(['input.ppg.' chan '.bd.ppftamp'])

%fixed input parameters for timing
length_pat_hq_interval =10;%seconds
                 
%fixed input parameters for thresholds

%absoute thresholds determining principal validity
PAT_low_th = 0.15; %(.1 as lower threshold for foot)   
PAT_high_th = 0.45;

%variability thresholds 
PATV_th = 0.035; %seconds, i.e. 35ms
HRV_th = 35; %bpm

%dummies for prospective check of CI necessary to calculate PAT, e.g. minimum required CI (threshold)
min_CI_ppg = 0;
min_CI_ecg =0;

%CI of PPG and ECG beats
ppg_CI_beats = ones(1, length(ecg_r)); %eval(['input.ppg.' chan '.CI_beat']);
ecg_CI_beats = ones(1, length(ecg_r)); %input.ecg.CI_beat;

%% Allocate memory for matrices

output = input;

% PAT
output.pat.pat_raw.signal = zeros(1, length(ecg_r)-1)*nan; % pat_raw signal
output.pat.pat_raw.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.pat_raw.error_flag.signal = zeros(1, length(ecg_r)-1)*nan; %error flags for raw signal

output.pat.pat_raw_CI.signal = zeros(1, length(ecg_r)-1)*nan; % pat_raw_CI signal
output.pat.pat_raw_CI.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.pat_valid_avg_CI.signal = zeros(1, length(ecg_r)-1)*nan; % pat_valid_avg_CI signal
output.pat.pat_valid_avg_CI.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.pat_avg.signal = zeros(1, length(ecg_r)-1)*nan; % pat_avg signal
output.pat.pat_avg.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.pat_sd.signal = zeros(1, length(ecg_r)-1)*nan; % pat_sd signal
output.pat.pat_sd.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.ppg_upstgrad.signal = zeros(1, length(ecg_r)-1)*nan; % ppg_upstgrad signal
output.pat.ppg_upstgrad.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.ppg_pp_ft_amp.signal = zeros(1, length(ecg_r)-1)*nan; % ppg_pp_ft_amp signal
output.pat.ppg_pp_ft_amp.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

%PAT/HR HQ

output.pat.timelapsed_pat_hq_interval.signal = zeros(1, length(ecg_r)-1)*nan; %time interval for high quality pats
output.pat.timelapsed_pat_hq_interval.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.pat_hq_avg.signal = zeros(1, length(ecg_r)-1)*nan; %high quality pat avg (10s avg of precendent valid pats)
output.pat.pat_hq_avg.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.pat_hq_sd.signal = zeros(1, length(ecg_r)-1)*nan; %high quality pat sd (10s sd of precendent valid pats)
output.pat.pat_hq_sd.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.HR_hq_avg.signal = zeros(1, length(ecg_r)-1)*nan; %high quality pat avg (10s avg of precendent valid pats)
output.pat.HR_hq_avg.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.HR_hq_sd.signal = zeros(1, length(ecg_r)-1)*nan; %high quality pat sd (10s sd of precendent valid pats)
output.pat.HR_hq_sd.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

%PPG upstgrad
output.pat.ppg_upstgrad_hq.signal = zeros(1, length(ecg_r)-1)*nan; % ppg_upstgrad signal
output.pat.ppg_upstgrad_hq.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

%PPG pp_ft_amp
output.pat.ppg_pp_ft_amp_hq.signal = zeros(1, length(ecg_r)-1)*nan; % ppg_pp_ft_amp signal
output.pat.ppg_pp_ft_amp_hq.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

%HR
output.pat.HR_raw.signal = zeros(1, length(ecg_r)-1)*nan; % HR_raw signal
output.pat.HR_raw.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.HR_raw.error_flag.signal = zeros(1, length(ecg_r)-1)*nan;  %error flags for raw signal

output.pat.HR_avg.signal = zeros(1, length(ecg_r)-1)*nan; % HR_avg signal
output.pat.HR_avg.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

output.pat.HR_sd.signal = zeros(1, length(ecg_r)-1)*nan; % HR_avg signal
output.pat.HR_sd.timestamps = zeros(1, length(ecg_r)-1)*nan; % ...and the corresponding timestamps

%PAT for BP

output.pat.pat_avg_for_BP.signal = []; % pat_for_BP (single value per spot_check, either normal avg or hq)
output.pat.pat_avg_for_BP.timestamps = []; % ...and the corresponding timestamps

output.pat.pat_sd_for_BP.signal = []; % pat_for_BP (single value per spot_check, either normal avg or hq)
output.pat.pat_sd_for_BP.timestamps = []; % ...and the corresponding timestamps

%PPG upstgrad for BP
output.pat.ppg_upstgrad_avg_for_BP.signal = []; 
output.pat.ppg_upstgrad_avg_for_BP.timestamps = [];

output.pat.ppg_upstgrad_sd_for_BP.signal = []; 
output.pat.ppg_upstgrad_sd_for_BP.timestamps = [];

%PPG pp_ft_amp for BP
output.pat.ppg_pp_ft_amp_avg_for_BP.signal = []; 
output.pat.ppg_pp_ft_amp_avg_for_BP.timestamps = [];

output.pat.ppg_pp_ft_amp_sd_for_BP.signal = []; 
output.pat.ppg_pp_ft_amp_sd_for_BP.timestamps = [];

%HR for BP

output.pat.HR_avg_for_BP.signal = []; % HR_for_BP (single value per spot_check)
output.pat.HR_avg_for_BP.timestamps = []; % ...and the corresponding timestamps

output.pat.HR_sd_for_BP.signal = []; % HR_for_BP (single value per spot_check)
output.pat.HR_sd_for_BP.timestamps = []; % ...and the corresponding timestamps

%CI for BP

output.pat.pat_CI_for_BP.signal = []; % pat_CI_for_BP (single value per spot_check)
output.pat.pat_CI_for_BP.timestamps = []; % ...and the corresponding timestamps

%% Check whether 'spot_check' signal is available and valid (otherwise define alternative flag)

%check if more than 10 elements are present (should be even ~20 in normal case)
if isfield(input, 'spot_check') && numel(input.spot_check.signal) > 10
    spot_check_valid = 1;
    
    %check whether spot check contains valid start and end sequences
    valid_start_locations = []; %vector containing valid start sequence in spot check
    for i = 1:numel(input.spot_check.signal) - 1
        if (input.spot_check.signal(i) == 2) && (input.spot_check.signal(i+1) == 1)
            valid_start_locations = [valid_start_locations, i];
        end
    end
    
    valid_stop_locations = []; %vector containing valid start sequence in spot check
    for i = 1:numel(input.spot_check.signal) - 1
        if (input.spot_check.signal(i) == 1) && (input.spot_check.signal(i+1) == 3) %|| (input.spot_check.signal(i+1) == 1)) --> 2 not regarded as valid anymore!
            valid_stop_locations = [valid_stop_locations, i];
        end
    end
    
    %make sure that spotcheck signal begins in idle state (2) and not during an active spotcheck (1), i.e. there should be no stop location before first start location
    %if this is the case: fill spotcheck signal with idle state (2) until first stop location and then delete it
    if ~isempty(valid_start_locations) && ~isempty(valid_stop_locations)
        if valid_stop_locations(1) < valid_start_locations(1)
            for k = 1: valid_stop_locations(1)+1
                input.spot_check.signal(k) = 2
            end
            valid_stop_locations(1) = [];
            disp('First "spot_check" is identified as invalid and will be ignored!')
        end
    end
    
    if isempty(valid_start_locations) || isempty(valid_stop_locations) || diff([valid_start_locations(1), valid_stop_locations(1)]) < 10 || input.spot_check.timestamps(valid_start_locations(1)) < ecg_r(2) || input.spot_check.timestamps(valid_stop_locations(end)) > ecg_r(end)
        
        spot_check_valid = 0;
    end
else
    spot_check_valid = 0;
end

%...if not, create field in data struct for complete length of recording (necessary for BP model v1 training data)
if spot_check_valid
    %input.spot_check.signal(end-4) = 3; %uncommment to shorten spotcheck for alignment with band 
    flag = input.spot_check;
    disp('Signal stream "spot_check" valid')
% elseif isfield(input, 'ecg_lead')
%     flag = input.ecg_lead;
else
    disp('Signal stream "spot_check" invalid --> Recording will be regarded as single Spotcheck')
    input.spot_check.signal = [NaN, 2, ones(1, length(ecg_r)-4), 3, NaN];
    input.spot_check.timestamps = ecg_r;
    flag = input.spot_check;
end

%% Compute PATs

%in general case that SpotCheck onset preceeds first detected ECG peak --> add ECG peak before first
%SpotCheck sample

if ecg_r(2) < input.spot_check.timestamps(1)
   ecg_r = [ecg_r(1), input.spot_check.timestamps(1)-1, ecg_r(2:end)];
end

%initialize flag
current_state = [];
previous_state = [];

for e=3:1:length(ecg_r)-1 %start from '3' to check past state in first iteration (normally 2, but first ecg_r is always NaN!), end at '-1' to compute parameters between current and succeeding R peak
    
    %Old flag orientation
        %look back from current ECG peak to get last flag signal
        %current_lead_state = flag.signal(find(flag.timestamps <= ecg_r(e), 2, 'last')); %take last 2 so for low HRs spot check termination is not missed (as flag signal is updated with 1Hz, HRs < 1Hz could miss latest flag change)
        %previous_lead_state = flag.signal(find(flag.timestamps <= ecg_r(e-1), 2, 'last')); %take last 2 so for low HRs spot check interruption is not missed
    
    %look for flag signal in between current and last ECG r-peak
    current_flag_between_peaks = flag.signal(find((flag.timestamps <= ecg_r(e)) &  flag.timestamps >= ecg_r(e-1)));

    %if flag signal (probably more than one sample in low HRs) was found
    %assign value(s) as latest state
    if ~isempty(current_flag_between_peaks)
        current_state = current_flag_between_peaks;
    end
    
    %start/interruption spotcheck
    if ismember(2, previous_state) & ismember(1, current_state)
        flag_state = 21;
    %termination spotcheck
    elseif ismember(1, previous_state) & ismember(3, current_state) & ~ismember(3, previous_state) %3 may not as well occur in previous state! --> situation where last available/detected spotcheck flag is 3
        flag_state = 13;
    %ongoing spotcheck
    elseif ismember(1, previous_state) & ismember(1, current_state) & ~(any(ismember([2 3], previous_state))) & ~(any(ismember([2 3], current_state)))
        flag_state = 11;
    else 
    %idle spotcheck    
        flag_state = 22;
    end    
 
    %only assign previous_state the former value of current_state if current_state changed as well
    if ~isempty(current_flag_between_peaks)
        previous_state = current_state;
    end
    
    %Check flag state and compute PATs
    
    if flag_state == 21 || flag_state == 11 ; %either start/interruption OR ongoing spotcheck
        
        if flag_state == 21; %reset parameters
            
            RR_ints_current_spotcheck = []; %vector containing all RR intervals for current spotcheck
            HRs_raw_current_spotcheck = []; %vector containing all HRs for current spotcheck
            pats_raw_current_spotcheck = []; %vector containing all pats for current spotcheck
            ppg_upstgrad_current_spotcheck = [];
            ppg_pp_ft_amp_current_spotcheck = [];            
            
            pats_hq_current_spotcheck = []; %vector containing succeding pats for 10s window for averaging
            pats_hq_current_spotcheck_timestamps = []; %...and correspondent timestamps
            
            ppg_upstgrad_hq_current_spotcheck = [];
            ppg_upstgrad_hq_current_spotcheck_timestamps = [];
            
            ppg_pp_ft_amp_hq_current_spotcheck = [];
            ppg_pp_ft_amp_hq_current_spotcheck_timestamps = [];            
            
            HRs_hq_current_spotcheck = []; %vector containing succeding HRs for 10s window for averaging
            HRs_hq_current_spotcheck_timestamps = []; %...and correspondent timestamps
            
            pat_valid_avg_CI_current_spotcheck = [NaN]; %vector containing the CIs associated with the latest computed average of valid PATs in Spot Check
            
            current_pat_hq_avg = NaN; %variable containing the high quality pat avgs (10s avg of precendent valid pats)
            current_pat_hq_sd = NaN; %variable containing the high quality pat stds (10s std of precendent valid pats)
            current_ppg_upstgrad_hq_avg = NaN;
            current_ppg_upstgrad_hq_sd = NaN;
            
            current_ppg_pp_ft_amp_hq_avg = NaN;
            current_ppg_pp_ft_amp_hq_sd = NaN;            
            
            current_HR_hq_avg = NaN; %variable containing the high quality HR avgs (10s avg of precendent valid pats)
            current_HR_hq_sd = NaN; %variable containing the high quality HR stds (10s std of precendent valid pats)
            
            error_flags_HR_current_spotcheck = [];
            error_flag_HR = 0; %error flag to indicate a fail in HR check
            %0: No error
            %1: -
            %2: Exceedance variability threshold
            %3: CI of input signals not sufficient
            %4: not between boundary values (30/220 bpm)
            error_flags_pat_current_spotcheck = [];
            error_flag_pat = 0; %error flag to indicate a fail in any of the checks for PAT
            %0: No error
            %1: No upstroke in between two consecutive R peaks
            %2: Exceedance variability threshold
            %3: CI of input signals not sufficient
            %4: not between boundary values (150/450ms)
            timelapsed_pat_hq_interval = NaN; %interval for high quality pats
            previous_pat = 0; %required in some cases where no pat was detected, default 0 if first pat would be one of those cases
            
            starttime_spotcheck = ecg_r(e); %start time to indicate the start of the 20 sec.
            timelapsed_spotcheck = 0;
            
        else %proceed with ongoing spotcheck
            
            timelapsed_spotcheck = ecg_r(e) - starttime_spotcheck;%the time lapsed since the start of spotcheck
            
        end
    
        % check ECG CI
        if ecg_CI_beats(e) >= min_CI_ecg %currently dummy such that all CI's pass
            
            %% compute RR interval for HR
            
            current_RR_int = ecg_r(e+1) - ecg_r(e);
            RR_ints_current_spotcheck = [RR_ints_current_spotcheck, current_RR_int];
            current_HR = 60./current_RR_int;
            
            if (current_HR > 30) & (current_HR < 220) %principal validity
                
                if isempty(HRs_raw_current_spotcheck)
                    
                    HRs_raw_current_spotcheck = [HRs_raw_current_spotcheck, current_HR];
                    error_flag_HR = 0; %assume first HR to be correct
                    
                elseif ((previous_HR - HRV_th) <= current_HR) & (current_HR <= (previous_HR + HRV_th)) %smaller than previous HR +/- empirically determined, highest physiologically valid HRV in dataset
                    %check for ECG misdetections using HRV threshold
                    HRs_raw_current_spotcheck = [HRs_raw_current_spotcheck, current_HR];
                    error_flag_HR = 0;
                    
                elseif ((previous_HR - HRV_th) > current_HR) | (current_HR > (previous_HR + HRV_th))
                    
                    error_flag_HR = 2; %exceedance variability threshold
                    
                end
                
            else
                
                error_flag_HR = 4; % not between boundary values
                
            end
            
            %% compute PAT interval
            
            % Find first PPGs between two consecutive ECG R-peaks
            current_ppg_s = ppg_s(find(ppg_s > ecg_r(e) & ppg_s < ecg_r(e+1), 1));
            current_ppg_upstgrad = ppg_upstgrad(find(ppg_s > ecg_r(e) & ppg_s < ecg_r(e+1), 1));
            current_ppg_pp_ft_amp = ppg_pp_ft_amp(find(ppg_s > ecg_r(e) & ppg_s < ecg_r(e+1), 1)); 
            
            if isempty(current_ppg_upstgrad)
                current_ppg_upstgrad = NaN;
            end
            if isempty(current_ppg_pp_ft_amp)
                current_ppg_pp_ft_amp = NaN;
            end            
            
            % Calculate PAT and check principal validity (i.e. if PAT is < 400ms and > 150ms)
            current_pat = current_ppg_s - ecg_r(e);
            
            %only continue if a PAT could be computed, i.e. if there was a PPGs between two consecutive ECG R-peaks
            if ~isempty(current_pat)
                
                if (current_pat > PAT_low_th) & (current_pat < PAT_high_th)  %principal validity 
                    
                    %check for PAT misdetections using PATV threshold
                    if isempty(pats_raw_current_spotcheck)
                        
                        pats_raw_current_spotcheck = [pats_raw_current_spotcheck, current_pat];
                        ppg_upstgrad_current_spotcheck = [ppg_upstgrad_current_spotcheck, current_ppg_upstgrad];
                        ppg_pp_ft_amp_current_spotcheck = [ppg_pp_ft_amp_current_spotcheck, current_ppg_pp_ft_amp];
                        error_flag_pat = 0; %assume first PAT to be correct
                        
                    elseif ((previous_pat - PATV_th) <= current_pat) & (current_pat <= (previous_pat + PATV_th)) %smaller than previous PAT +/- empirically determined, highest physiologically valid PATV in dataset
                        
                        pats_raw_current_spotcheck = [pats_raw_current_spotcheck, current_pat];
                        ppg_upstgrad_current_spotcheck = [ppg_upstgrad_current_spotcheck, current_ppg_upstgrad];
                        ppg_pp_ft_amp_current_spotcheck = [ppg_pp_ft_amp_current_spotcheck, current_ppg_pp_ft_amp];
                        error_flag_pat = 0;
                        
                    elseif ((previous_pat - PATV_th) > current_pat) | (current_pat > (previous_pat + PATV_th))
                        
                        error_flag_pat = 2; %exceedance variability threshold
                        
                    end
                    
                else
                    
                    error_flag_pat = 4; % not between boundary values)
                    
                end
                
            else
                
                current_pat = previous_pat; % use previous PAT (as NaN or similar cannot serve as threshold for next iteration) %set as NaN 
                error_flag_pat = 1; % no principally valid PAT (no PPGs between two consecutive ECG R-peaks)
                
            end
            
        else
            % if ECG CI not good enough all outputs remains NaNs
            current_pat = previous_pat; % use previous PAT (as NaN or similar cannot serve as threshold for next iteration) %set as NaN
            error_flag_HR = 3; % CI of input signals not sufficient
            error_flag_pat = 3; % CI of input signals not sufficient
        end
        
        %save error flags for current HR and pat (relevant for calculating CI)
        error_flags_HR_current_spotcheck = [error_flags_HR_current_spotcheck, error_flag_HR];
        error_flags_pat_current_spotcheck = [error_flags_pat_current_spotcheck, error_flag_pat];
        
        %save ratio of correct pats for CI computation
        ratio_correct_pats = sum(error_flags_pat_current_spotcheck==0) / numel(error_flags_pat_current_spotcheck); %#valid PATs / #PATs expected (not considering misdetections of ECG peaks!)
        
        %% Check for high quality PATs/HRs and assign CI
        
        if (error_flag_HR == 0) & (error_flag_pat == 0) & isnan(timelapsed_pat_hq_interval); 
            
            pats_hq_current_spotcheck = [pats_hq_current_spotcheck, current_pat]; %vector of succeeding hq pats in current spotcheck
            pats_hq_current_spotcheck_timestamps = [pats_hq_current_spotcheck_timestamps, ecg_r(e)];
            
            ppg_upstgrad_hq_current_spotcheck = [ppg_upstgrad_hq_current_spotcheck, current_ppg_upstgrad];
            ppg_upstgrad_hq_current_spotcheck_timestamps = [ppg_upstgrad_hq_current_spotcheck_timestamps, ecg_r(e)];
            
            ppg_pp_ft_amp_hq_current_spotcheck = [ppg_pp_ft_amp_hq_current_spotcheck, current_ppg_pp_ft_amp];
            ppg_pp_ft_amp_hq_current_spotcheck_timestamps = [ppg_pp_ft_amp_hq_current_spotcheck_timestamps, ecg_r(e)];            
            
            HRs_hq_current_spotcheck = [HRs_hq_current_spotcheck, current_HR]; %vector of succeeding hq HR in current spotcheck
            HRs_hq_current_spotcheck_timestamps = [HRs_hq_current_spotcheck_timestamps, ecg_r(e)];
            
            starttime_pat_hq_interval = ecg_r(e); %start time to indicate the start of the 10 sec. of high quality pats
            timelapsed_pat_hq_interval = 0;
            current_pat_raw_CI = round(ratio_correct_pats*3)+1; %ratio of correct pats scaled from 0-1 to 1-4
            pat_valid_avg_CI_current_spotcheck = [pat_valid_avg_CI_current_spotcheck, current_pat_raw_CI];
            
        elseif (error_flag_HR == 0) & (error_flag_pat == 0) & ~isnan(timelapsed_pat_hq_interval);
            
            pats_hq_current_spotcheck = [pats_hq_current_spotcheck, current_pat];
            pats_hq_current_spotcheck_timestamps = [pats_hq_current_spotcheck_timestamps, ecg_r(e)];
            
            ppg_upstgrad_hq_current_spotcheck = [ppg_upstgrad_hq_current_spotcheck, current_ppg_upstgrad];
            ppg_upstgrad_hq_current_spotcheck_timestamps = [ppg_upstgrad_hq_current_spotcheck_timestamps, ecg_r(e)];

            ppg_pp_ft_amp_hq_current_spotcheck = [ppg_pp_ft_amp_hq_current_spotcheck, current_ppg_pp_ft_amp];
            ppg_pp_ft_amp_hq_current_spotcheck_timestamps = [ppg_pp_ft_amp_hq_current_spotcheck_timestamps, ecg_r(e)];            
            
            HRs_hq_current_spotcheck = [HRs_hq_current_spotcheck, current_HR]; %vector of succeeding hq HR in current spotcheck
            HRs_hq_current_spotcheck_timestamps = [HRs_hq_current_spotcheck_timestamps, ecg_r(e)];
            
            timelapsed_pat_hq_interval = ecg_r(e) - starttime_pat_hq_interval; %the time lapsed since the start of the high quality pat interval
            
            if timelapsed_pat_hq_interval >= length_pat_hq_interval;
                
                %truncate vector of high quality pats to max length of pat_hq interval
                if ~isempty(pats_hq_current_spotcheck_timestamps)
                    greater_than_pat_hq_interval = (pats_hq_current_spotcheck_timestamps < pats_hq_current_spotcheck_timestamps(end) - length_pat_hq_interval);
                    
                    pats_hq_current_spotcheck(greater_than_pat_hq_interval) = [];
                    pats_hq_current_spotcheck_timestamps(greater_than_pat_hq_interval) = [];
                    
                    ppg_upstgrad_hq_current_spotcheck(greater_than_pat_hq_interval) = [];
                    ppg_upstgrad_hq_current_spotcheck_timestamps(greater_than_pat_hq_interval) = [];
                    
                    ppg_pp_ft_amp_hq_current_spotcheck(greater_than_pat_hq_interval) = [];
                    ppg_pp_ft_amp_hq_current_spotcheck_timestamps(greater_than_pat_hq_interval) = [];                    
                    
                    HRs_hq_current_spotcheck(greater_than_pat_hq_interval) = [];
                    HRs_hq_current_spotcheck_timestamps(greater_than_pat_hq_interval) = [];
                end
                
                %compute mean and sd for the remaining last 10 seconds of high quality pats and HRs
                current_pat_hq_avg =  nanmean(pats_hq_current_spotcheck);%either take the only one that occurs in spotcheck or always the latest that was calculated
                current_pat_hq_sd =  nanstd(pats_hq_current_spotcheck);%
                
                current_ppg_upstgrad_hq_avg = nanmean(ppg_upstgrad_hq_current_spotcheck);
                current_ppg_upstgrad_hq_sd = nanstd(ppg_upstgrad_hq_current_spotcheck);
                
                current_ppg_pp_ft_amp_hq_avg = nanmean(ppg_pp_ft_amp_hq_current_spotcheck);
                current_ppg_pp_ft_amp_hq_sd = nanstd(ppg_pp_ft_amp_hq_current_spotcheck);                
                
                current_HR_hq_avg =  nanmean(HRs_hq_current_spotcheck);%either take the only one that occurs in spotcheck or always the latest that was calculated
                current_HR_hq_sd =  nanstd(HRs_hq_current_spotcheck);%
                
                current_pat_raw_CI = 5;
                pat_valid_avg_CI_current_spotcheck = [pat_valid_avg_CI_current_spotcheck, current_pat_raw_CI];
                
            else
                
                current_pat_raw_CI = round(ratio_correct_pats*3)+1; %ratio of correct pats scaled from 0-1 to 1-4
                pat_valid_avg_CI_current_spotcheck = [pat_valid_avg_CI_current_spotcheck, current_pat_raw_CI];
                
            end
            
        elseif (error_flag_HR ~= 0) | (error_flag_pat ~= 0) %for non-high quality pats, i.e. %if  CI not sufficient OR HRVth exceeded OR no upstroke between ECG peaks OR pat not in boundary conditions OR PATVth exceeded
            
            pats_hq_current_spotcheck = [];
            pats_hq_current_spotcheck_timestamps = [];
            
            ppg_upstgrad_hq_current_spotcheck = [];
            ppg_upstgrad_hq_current_spotcheck_timestamps = [];
            
            ppg_pp_ft_amp_hq_current_spotcheck = [];
            ppg_pp_ft_amp_hq_current_spotcheck_timestamps = [];            
            
            HRs_hq_current_spotcheck = [];
            HRs_hq_current_spotcheck_timestamps = [];
            
            timelapsed_pat_hq_interval = NaN;
            current_pat_raw_CI = round(ratio_correct_pats*3)+1; %ratio of correct pats scaled from 0-1 to 1-4
            
        end
        
        
        %% Assign values to output vectors for HR and PAT
        
        %HR
        output.pat.HR_raw.signal(e) = current_HR; %including potential outliers due to ECG misdetections!
        output.pat.HR_raw.error_flag.signal(e) = error_flag_HR;
        output.pat.HR_raw.timestamps(e) = ecg_r(e);
        
        output.pat.HR_avg.signal(e) = nanmean(HRs_raw_current_spotcheck); %average only includes valid ECG R peaks
        output.pat.HR_avg.timestamps(e) = ecg_r(e);
        
        output.pat.HR_sd.signal(e) = nanstd(HRs_raw_current_spotcheck);
        output.pat.HR_sd.timestamps(e) = ecg_r(e);
        
        output.pat.HR_hq_avg.signal(e) = current_HR_hq_avg;
        output.pat.HR_hq_avg.timestamps(e) = ecg_r(e);
        
        output.pat.HR_hq_std.signal(e) = current_HR_hq_sd;
        output.pat.HR_hq_std.timestamps(e) = ecg_r(e);
        
        %PAT
        output.pat.pat_raw.signal(e) = current_pat; %including potential outliers!
        output.pat.pat_raw.error_flag.signal(e) = error_flag_pat;
        output.pat.pat_raw.timestamps(e) = ecg_r(e);
        
        output.pat.pat_avg.signal(e) = nanmean(pats_raw_current_spotcheck); %average only includes valid PATs
        output.pat.pat_avg.timestamps(e) = ecg_r(e);
        
        output.pat.pat_sd.signal(e) = nanstd(pats_raw_current_spotcheck);
        output.pat.pat_sd.timestamps(e) = ecg_r(e);
        
        %PPG gradient
        output.pat.ppg_upstgrad.signal(e) = current_ppg_upstgrad;
        output.pat.ppg_upstgrad.timestamps(e) = ecg_r(e);
        
        output.pat.ppg_upstgrad_avg.signal(e) = nanmean(ppg_upstgrad_current_spotcheck);
        output.pat.ppg_upstgrad_avg.timestamps(e) = ecg_r(e);        
        
        output.pat.ppg_upstgrad_sd.signal(e) = nanstd(ppg_upstgrad_current_spotcheck);
        output.pat.ppg_upstgrad_sd.timestamps(e) = ecg_r(e);           
        
        %PPG pp-ft amplitude
        output.pat.ppg_pp_ft_amp.signal(e) = current_ppg_pp_ft_amp;
        output.pat.ppg_pp_ft_amp.timestamps(e) = ecg_r(e);
        
        output.pat.ppg_pp_ft_amp_avg.signal(e) = nanmean(ppg_pp_ft_amp_current_spotcheck);
        output.pat.ppg_pp_ft_amp_avg.timestamps(e) = ecg_r(e);        
        
        output.pat.ppg_pp_ft_amp_sd.signal(e) = nanstd(ppg_pp_ft_amp_current_spotcheck);
        output.pat.ppg_pp_ft_amp_sd.timestamps(e) = ecg_r(e);          
        
        %PAT CI
        
        output.pat.pat_raw_CI.signal(e) = current_pat_raw_CI; %CI for every single pat
        output.pat.pat_raw_CI.timestamps(e) = ecg_r(e);
        
        output.pat.pat_valid_avg_CI.signal(e) = pat_valid_avg_CI_current_spotcheck(end); %CI for latest average of valid PATs
        output.pat.pat_valid_avg_CI.timestamps(e) = ecg_r(e);
        
        %HQ PAT
        
        output.pat.timelapsed_pat_hq_interval.signal(e) = timelapsed_pat_hq_interval;
        output.pat.timelapsed_pat_hq_interval.timestamps(e) = ecg_r(e);
        
        output.pat.pat_hq_avg.signal(e) = current_pat_hq_avg;
        output.pat.pat_hq_avg.timestamps(e) = ecg_r(e);
        
        output.pat.pat_hq_std.signal(e) = current_pat_hq_sd;
        output.pat.pat_hq_std.timestamps(e) = ecg_r(e);
        
        %HQ PPG gradient
        output.pat.ppg_upstgrad_hq_avg.signal(e) = current_ppg_upstgrad_hq_avg;
        output.pat.ppg_upstgrad_hq_avg.timestamps(e) = ecg_r(e);
               
        output.pat.ppg_upstgrad_hq_std.signal(e) = current_ppg_upstgrad_hq_sd;
        output.pat.ppg_upstgrad_hq_std.timestamps(e) = ecg_r(e);
        
        %HQ PPG pp-ft amplitude
        output.pat.ppg_pp_ft_amp_hq_avg.signal(e) = current_ppg_pp_ft_amp_hq_avg;
        output.pat.ppg_pp_ft_amp_hq_avg.timestamps(e) = ecg_r(e);
               
        output.pat.ppg_pp_ft_amp_hq_std.signal(e) = current_ppg_pp_ft_amp_hq_sd;
        output.pat.ppg_pp_ft_amp_hq_std.timestamps(e) = ecg_r(e);        
        
        %% Set previous HR and pat for next iteration for next iteration
        previous_HR = current_HR;
        if error_flag_pat ~= 4; %only assign previous PAT when value is principally valid, in this case 
            previous_pat = current_pat;
        end
        
    elseif flag_state == 13 %termination
        
        % Compute PAT output for BP at end of spotcheck
        if ~isnan(current_pat_hq_avg) %hq pats were detected
            
            output.pat.pat_avg_for_BP.signal = [output.pat.pat_avg_for_BP.signal; current_pat_hq_avg];
            output.pat.pat_avg_for_BP.timestamps = [output.pat.pat_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.pat_sd_for_BP.signal = [output.pat.pat_sd_for_BP.signal; current_pat_hq_sd];
            output.pat.pat_sd_for_BP.timestamps = [output.pat.pat_sd_for_BP.timestamps; ecg_r(e)];
            
            output.pat.ppg_upstgrad_avg_for_BP.signal = [output.pat.ppg_upstgrad_avg_for_BP.signal; current_ppg_upstgrad_hq_avg];
            output.pat.ppg_upstgrad_avg_for_BP.timestamps = [output.pat.ppg_upstgrad_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.ppg_upstgrad_sd_for_BP.signal = [output.pat.ppg_upstgrad_sd_for_BP.signal; current_ppg_upstgrad_hq_sd];
            output.pat.ppg_upstgrad_sd_for_BP.timestamps = [output.pat.ppg_upstgrad_sd_for_BP.timestamps; ecg_r(e)];   
            
            output.pat.ppg_pp_ft_amp_avg_for_BP.signal = [output.pat.ppg_pp_ft_amp_avg_for_BP.signal; current_ppg_pp_ft_amp_hq_avg];
            output.pat.ppg_pp_ft_amp_avg_for_BP.timestamps = [output.pat.ppg_pp_ft_amp_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.ppg_pp_ft_amp_sd_for_BP.signal = [output.pat.ppg_pp_ft_amp_sd_for_BP.signal; current_ppg_pp_ft_amp_hq_sd];
            output.pat.ppg_pp_ft_amp_sd_for_BP.timestamps = [output.pat.ppg_pp_ft_amp_sd_for_BP.timestamps; ecg_r(e)];             
            
            %CI in case that hq pats are available (always 5)
            output.pat.pat_CI_for_BP.signal = [output.pat.pat_CI_for_BP.signal; 5];
            output.pat.pat_CI_for_BP.timestamps = [output.pat.pat_CI_for_BP.timestamps; ecg_r(e)];
            
        else %no hq pats detected
            
            output.pat.pat_avg_for_BP.signal = [output.pat.pat_avg_for_BP.signal; nanmean(pats_raw_current_spotcheck)];
            output.pat.pat_avg_for_BP.timestamps = [output.pat.pat_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.pat_sd_for_BP.signal = [output.pat.pat_sd_for_BP.signal; nanstd(pats_raw_current_spotcheck)];
            output.pat.pat_sd_for_BP.timestamps = [output.pat.pat_sd_for_BP.timestamps; ecg_r(e)];
            
            output.pat.ppg_upstgrad_avg_for_BP.signal = [output.pat.ppg_upstgrad_avg_for_BP.signal; nanmean(ppg_upstgrad_current_spotcheck)];
            output.pat.ppg_upstgrad_avg_for_BP.timestamps = [output.pat.ppg_upstgrad_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.ppg_upstgrad_sd_for_BP.signal = [output.pat.ppg_upstgrad_sd_for_BP.signal; nanstd(ppg_upstgrad_current_spotcheck)];
            output.pat.ppg_upstgrad_sd_for_BP.timestamps = [output.pat.ppg_upstgrad_sd_for_BP.timestamps; ecg_r(e)];            
            
            output.pat.ppg_pp_ft_amp_avg_for_BP.signal = [output.pat.ppg_pp_ft_amp_avg_for_BP.signal; nanmean(ppg_pp_ft_amp_current_spotcheck)];
            output.pat.ppg_pp_ft_amp_avg_for_BP.timestamps = [output.pat.ppg_pp_ft_amp_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.ppg_pp_ft_amp_sd_for_BP.signal = [output.pat.ppg_pp_ft_amp_sd_for_BP.signal; nanstd(ppg_pp_ft_amp_current_spotcheck)];
            output.pat.ppg_pp_ft_amp_sd_for_BP.timestamps = [output.pat.ppg_pp_ft_amp_sd_for_BP.timestamps; ecg_r(e)];              
            
            %CI in case that no hq pats are available 
            % - Either from last valid average or from last computed raw PAT
            % - In case of average, the CI is not based on the total number of theroretically available PATs as there might be invalid PATs ahead with CI=1. 
            %   Instead, it will always output the CI corresponding to the avg of valid PATs, and thus not just the last computed raw CI.
            
            if ~isnan(pat_valid_avg_CI_current_spotcheck(end))
               output.pat.pat_CI_for_BP.signal = [output.pat.pat_CI_for_BP.signal; pat_valid_avg_CI_current_spotcheck(end)];
               output.pat.pat_CI_for_BP.timestamps = [output.pat.pat_CI_for_BP.timestamps; ecg_r(e)];
               
            else 
                
               output.pat.pat_CI_for_BP.signal = [output.pat.pat_CI_for_BP.signal; current_pat_raw_CI];
               output.pat.pat_CI_for_BP.timestamps = [output.pat.pat_CI_for_BP.timestamps; ecg_r(e)];
               
            end
            
            %Check that PAT std is not 0 (which is indicative for only a single PAT detected)
            %If it is, change last appended CI for BP to 1
            if nanstd(pats_raw_current_spotcheck) == 0
                output.pat.pat_CI_for_BP.signal(end) = 1;
                % output.pat.pat_CI_for_BP.timestamps stays the same
            end
                        
        end
        
        if ~isnan(current_HR_hq_avg) %hq intervals detected
            
            output.pat.HR_avg_for_BP.signal = [output.pat.HR_avg_for_BP.signal; current_HR_hq_avg];
            output.pat.HR_avg_for_BP.timestamps = [output.pat.HR_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.HR_sd_for_BP.signal  = [output.pat.HR_sd_for_BP.signal; current_HR_hq_sd];
            output.pat.HR_sd_for_BP.timestamps  = [output.pat.HR_sd_for_BP.timestamps; ecg_r(e)];
            
        else %no hq intervals detected
            
            output.pat.HR_avg_for_BP.signal = [output.pat.HR_avg_for_BP.signal; nanmean(HRs_raw_current_spotcheck)];
            output.pat.HR_avg_for_BP.timestamps = [output.pat.HR_avg_for_BP.timestamps; ecg_r(e)];
            
            output.pat.HR_sd_for_BP.signal = [output.pat.HR_sd_for_BP.signal; nanstd(HRs_raw_current_spotcheck)];
            output.pat.HR_sd_for_BP.timestamps = [output.pat.HR_sd_for_BP.timestamps; ecg_r(e)];
            
        end
    end
end

%% Make sure that output variables to be passed to BP computation are not empty

if isempty(output.pat.pat_avg_for_BP.signal)

%PAT for BP

output.pat.pat_avg_for_BP.signal = NaN; % pat_for_BP (single value per spot_check, either normal avg or hq)
output.pat.pat_avg_for_BP.timestamps = NaN; % ...and the corresponding timestamps

output.pat.pat_sd_for_BP.signal = NaN; % pat_for_BP (single value per spot_check, either normal avg or hq)
output.pat.pat_sd_for_BP.timestamps = NaN; % ...and the corresponding timestamps

%HR for BP

output.pat.HR_avg_for_BP.signal = NaN; % HR_for_BP (single value per spot_check)
output.pat.HR_avg_for_BP.timestamps = NaN; % ...and the corresponding timestamps

output.pat.HR_sd_for_BP.signal = NaN; % HR_for_BP (single value per spot_check)
output.pat.HR_sd_for_BP.timestamps = NaN; % ...and the corresponding timestamps

%CI for BP

output.pat.pat_CI_for_BP.signal = NaN; % pat_CI_for_BP (single value per spot_check)
output.pat.pat_CI_for_BP.timestamps = NaN; % ...and the corresponding timestamps

end

%% PPG channel selection (currently hardwired to channel 'e'!)

if isfield(input, 'ppg_selection') & isfield(input, 'spot_check')
    
    %Choose PPG channel based on ppg selection in the band
    %Only the choice during spot check is relevant (there might be a different channel chosen before/after spot check)!
    spotcheck_on = input.spot_check.timestamps(input.spot_check.signal == 1);
    ppg_during_spotcheck = round(mean(input.ppg_selection.signal(input.ppg_selection.timestamps > spotcheck_on(1) & input.ppg_selection.timestamps < spotcheck_on(end))));
    
    if ppg_during_spotcheck == 0
        output.pat.channel = 'a' ;
    elseif ppg_during_spotcheck == 1
        output.pat.channel = 'b' ;
    elseif ppg_during_spotcheck == 2
        output.pat.channel = 'c' ;
    elseif ppg_during_spotcheck == 3
        output.pat.channel = 'd' ;
    elseif ppg_during_spotcheck == 4
        output.pat.channel = 'e' ;
    elseif ppg_during_spotcheck == 5
        output.pat.channel = 'f' ;
    elseif ppg_during_spotcheck == 6
        output.pat.channel = 'g' ;
    elseif ppg_during_spotcheck == 7
        output.pat.channel = 'h' ;
    end
    
end

%currently hardwired
output.pat.channel = 'e' ;

%% Prepare output
if isfield(input,'spot_check')
    output.spot_check=input.spot_check; %e.g. if 'spot_check' field was added in PAT function
end

% make the output available per channel (while current calculation is always in pat field)
eval(['output.ppg.' chan '.pat = output.pat;']);

