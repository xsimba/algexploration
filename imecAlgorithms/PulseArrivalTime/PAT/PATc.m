% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <PAT>
% Content   : Main script for PAT calculation
% Version   : GIT 2
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
%  Modification and Version History: 
%  | Developer | Version |    Date    |     Changes      |
%  | F. Beutel |   1.1   |  04/11/14  | Add corresponding timestamps to all PAT vectors |
%  | F. Beutel |   1.5   |  09/11/14  | Start complete refactoring of PAT code |
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%

% This script calculates and outputs the PATs per 20 sec. average. If a
% window of 10 sec is available where there are no missing beats and all
% pats are within the set boundaries, it will output the 10 sec avarage.
% inputs:
% Lead on / off per (ecg) beat
% ECG beats
% PPG beats
% output:
% pat.pat = 10 sec window avarage of pat (if available else nan) 1st row on
% upstroke, rest is features
% pat.patstd = same but std in stead of mean
% pat.pat2 = 30 sec window average of pat and if available 10sec window
% averages (CI=5) and else nan
% pat.pat2std = same but std in stead of mean
% pat.HR = HR average over the 10 sec window
% pat.HRstd = HR std over the 10 second window
% pat.HR2 = HR avarage over the 30 sec. window
% pat. HR2std = HR std over 30 sec window.
% pat.CI_pat = CI over all 1 if pat is bad / non existent, 1-4 if 30 sec window avarage, 5 if 10 sec window avarage.  

%%%%%%%%%%%%%%%%%%%%%%
function [output] = PATc(input,chan)
% main input parameters
BTS_ppg = eval(['input.ppg.' chan '.beats']);
BTS_ecg = input.ecg.beats;

%fixed input parameters for timing and CI
    mintime2check =10;%seconds
    maxtime2check =20;%seconds 
        
    %dummies for prospective check of CI necessary to calculate PAT
    %minimum required CI (threshold)
    min_CI_ppg = 0;
    min_CI_ecg =0;

%actual CI of ecg and ppg 
ppg_CI_beat = eval(['input.ppg.' chan '.CI_beat']);  
ecg_CI_beat = input.ecg.CI_beat;

% Allocate memory for matrices
output = input;
output.pat.pat.signal=zeros(3,length(input.ecg.beats)-1)*nan; % the pats from the 10 sec window
output.pat.pat.timestamps=zeros(3,length(input.ecg.beats)-1)*nan; % ...and the corresponding timestamps

output.pat.patstd=zeros(3,length(input.ecg.beats)-1)*nan; % the pats from the 10 sec window
output.pat.pat2std=zeros(3,length(input.ecg.beats)-1)*nan; % the pats from the 10 sec window

output.pat.pat1.signal=zeros(3,length(input.ecg.beats)-1)*nan; % all the pats
output.pat.pat1.timestamps=zeros(3,length(input.ecg.beats)-1)*nan; % ...and the corresponding timestamps

output.pat.pat2.signal=zeros(3,length(input.ecg.beats)-1)*nan; % the pats from the 30 sec window incl ones form 10 sec window
output.pat.pat2.timestamps=zeros(3,length(input.ecg.beats)-1)*nan; %...and the corresponding timestamps

patRR=zeros(2,length(input.ecg.beats)-1)*nan; % the pats from the 30 sec window
output.pat.HR=zeros(1,length(input.ecg.beats)-1)*nan; % the pats from the 30 sec window incl ones form 10 sec window
output.pat.HRstd=zeros(1,length(input.ecg.beats)-1)*nan; % the pats from the 30 sec window incl ones form 10 sec window
output.pat.HR2=zeros(1,length(input.ecg.beats)-1)*nan; % the pats from the 30 sec window incl ones form 10 sec window
output.pat.HR2std=zeros(1,length(input.ecg.beats)-1)*nan; % the pats from the 30 sec window incl ones form 10 sec window
output.pat.CI_pat=zeros(1,length(input.ecg.beats)-1)*nan; % CI_pat is NaN, only if state is 'lead on' it becomes active as PAT must theoretically be calculated 

CI_pat_t2(1)=0; %no idea yet, to be identified

for e=2:1:length(input.ecg.beats)-1            
%% Check if ECG CI is >= 2/3 and if lolo ==1 
% ECG should not be >1 if lolo (lead on /off =0) therefore checking for CI
% ECGbeats > 3 should be enough. But there is no point in checking for PATs

%check if spot_check is available 
%(if not ecg_lead serves as flag for start of PAT computation)
if isfield(input, 'spot_check')
    flag = input.spot_check;
else
    flag = input.ecg_lead; 
end

current_lead_state = flag.signal(find(flag.timestamps <= input.ecg.beats(e),1,'last'));%updated with 2Hz
previous_lead_state = flag.signal(find(flag.timestamps <= input.ecg.beats(e-1),1,'last'));%updated with 2Hz

    if current_lead_state ==1 % there should be "lead on"
        %% detect flag transition (lead off to lead on, respectively spot check off to on)
        %then start timer
        if isempty(previous_lead_state) || previous_lead_state == 2
            starttime = input.ecg.beats(e); %start time to indicate the start of the (30) sec.
            startindex = e;
            timelapsed=0;
        else 
             timelapsed = input.ecg.beats(e) - starttime;%the time lapsed since the start to indicate how far along the 30 sec. you are
        end

        %% check ECG CI
        if ecg_CI_beat(e) >= min_CI_ecg %currently dummy such that all CI's pass
           % check if there are PPG beats in between 2 ECG beats:
           % find the first PPG beat detections in between two consequetive ECG R-peaks
            Bppg_t = find(BTS_ppg(1,:)< BTS_ecg(e+1) & BTS_ppg(1,:)> BTS_ecg(e),1);
          
           %% check the first detected PPG beat in the ECG interval if any and calc the pat and check if it is < 350ms and > 150ms
           %  Check how many PPG beat detections are within the interval and
           %  calculate the correct PAT from it 
           [pat(:,e), pati(:,e), CI_pat(e), patRR(:,e)] = PAT_checker(Bppg_t, e, BTS_ppg(:,:), BTS_ecg, ppg_CI_beat, min_CI_ppg);
               output.pat.pat1.timestamps(:,e) = patRR(2,e);
               output.pat.pat1.signal(:,e) = pat(:,e);
            
            %% calculate output parameters (averages and std's)
            
            idx_maxtime_sec_ago = find(((output.ecg.beats(1:e))-((output.ecg.beats(e))-maxtime2check)) > 0, 1, 'first'); %find the index of the ecg.beats 30 sec back
            
            output.pat.pat2.timestamps(:,e) = output.ecg.beats(e);
            output.pat.pat2.signal(1,e) =  nanmean(output.pat.pat1.signal(1,idx_maxtime_sec_ago:e)); % take the mean over all pats in the last 30 sec.
%             output.pat.pat2.signal(2,e) =  nanmean(output.pat.pat1.signal(2,thsecind1:e)); % take the mean over all pats in the last 30 sec.
%             output.pat.pat2.signal(3,e) =  nanmean(output.pat.pat1.signal(3,thsecind1:e)); % take the mean over all pats in the last 30 sec.
            output.pat.pat2std(1,e) =  nanstd(output.pat.pat1.signal(1,idx_maxtime_sec_ago:e)); % take the mean over all pats in the last 30 sec.
%             output.pat.pat2std(2,e) =  nanstd(output.pat.pat1.signal(2,thsecind1:e)); % take the mean over all pats in the last 30 sec.
%             output.pat.pat2std(3,e) =  nanstd(output.pat.pat1.signal(3,thsecind1:e)); % take the mean over all pats in the last 30 sec.
            output.pat.HR2(e) = 60./nanmean(diff(patRR(2,idx_maxtime_sec_ago:e))); % take the mean HR over 30 sec
            output.pat.HR2std(e) = nanstd(60./diff(patRR(2,idx_maxtime_sec_ago:e))); % take the std of the HR over 30 sec.
            nrnonpat(1,e) = length(find(isnan(output.pat.pat1.signal(1,idx_maxtime_sec_ago:e)))); % calc the nr of "missed" pats in the last 30 sec. 
            %?????
            indnonpat(1,e) =(e-idx_maxtime_sec_ago) + 1; % #of pats that should have been there (ask Sander for '+1'?!) 
            %?????
            output.pat.CI_pat(1,e) = ((indnonpat(1,e) - nrnonpat(1,e))/indnonpat(1,e))*4; % calculate the CI depending on the nr of found pats in the 30 sec window (max CI is 4)

           %if minimum time is not reached CI_pat is 0 (instad of NaN)
           if timelapsed < mintime2check
               output.pat.CI_pat(1,e) = 0;
           end
            
           %% if the max time passed is not yet 30 sec. than check for a 10 sec good window
           if timelapsed < maxtime2check %if the max amount of time is not passed, look for the 10 sec of good pats      
               
                [pat(:,e),CI_pat,CI_pat_t2,patstd(:,e),HR(:,e),HRstd(:,e),output] = TimecheckPAT(output, CI_pat, e, CI_pat_t2, mintime2check);
                output.pat.pat.timestamps(:,e) = output.ecg.beats(e);
                output.pat.pat.signal(:,e) = pat(:,e);
                output.pat.patstd(:,e) = patstd(:,e); %
                output.pat.HR(:,e) = HR(:,e); %
                output.pat.HRstd(:,e) = HRstd(:,e); %
                output.pat.pati = pati(:,e);             
                
            else       %if the max time has passed, shift the index by 1;
                % in this way all the data gets anlysed, also if the spot
                % check is longer than the 30 sec aimed for.
                starttime = starttime+1;
                startindex=startindex+1;                        
            end
        else
            % if quality ECG not good enough output remains nan, and CI
            % will
            % be 1
            output.CI_pat(e)=1;
            timelapsed=0;
        end  
    else
    %if there's no 'lead on' or 'spot check on': 
    %pat should remain NaN (as defined in preallocation)
    %CI_pat "    "      "         "            "
    end
end
%% make the output available per channel (while current calculation is always in pat field)
eval(['output.ppg.' chan '.pat = output.pat;']);
%assign all CI_pat's <=1 the value 1 (to be identified why...)
eval(['output.ppg.' chan '.pat.CI_pat(output.ppg.' chan '.pat.CI_pat<=1)=1;']);
