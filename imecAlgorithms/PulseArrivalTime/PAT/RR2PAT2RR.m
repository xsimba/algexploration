function [RR_pat, pat, pat_2, pat_3,CI_pat2,HR] = RR2pat2RR(pat,RR_ti)

% Get the pat RR interval-if the RR distance too large-> remove. but only
% if the pat itself is not 0
for a = 2:1:length(pat)-1
    if pat(a)~=0 && pat(a+1)~=0 && abs(pat(a)-pat(a+1))<0.2 || pat(a)~=0 && pat(a-1)~=0 && abs(pat(a)-pat(a-1))<0.2
       RR_pat_t(a) =  pat(a)-pat(a+1);
    elseif pat(a)~=0 && pat(a+1)==0
        RR_pat_t(a) = pat(a);
    else %&& pat(a+1)~=0 && abs(pat(a)-pat(a+1))<0.2
       RR_pat_t(a) =  0;
    end
end
RR_pat=RR_pat_t;
RR_pat(abs(RR_pat)>0)=1;
RR_pat(end+1)=0;
pat =RR_pat.*pat;


%
% RR_pat =pat(1:end-1)-pat(2:end);
% RR_pat(abs(RR_pat)>0.2)=0;
% RR_pat(abs(RR_pat)>0)=1;
%% get rid of all "lonely" pat's
pat_temp=pat;
pat_temp(pat_temp>0)=1;
for a= 2:1:length(pat_temp)-1
    if pat_temp(a)>0 && pat_temp(a+1)>0 && pat_temp(a-1)>0
       pat_3(a) = pat(a);
    else
       pat_3(a) =0;
    end
end
pat_3(abs(pat_3)>0)=1;
% pat_3=pat_3(1:end);
pat_3(end+1)=0;
pat=pat.*pat_3;

%% get the mean over 10 pat's
for a= 1:1:length(pat_temp)-14
    Pat_temp = pat_temp((a:a+14));
    if sum(Pat_temp) == 15
        pat_2(a) = mean(pat(a:a+14));
        CI_pat2(a) = 5;
        HR(a) = 60./mean(RR_ti(a:a+14));
    else
        pat_2(a) = 0;
        CI_pat2(a) = 1;
        HR(a) = 0;
    end
end

pat_temp=pat;
pat_temp(pat_temp>0)=1;
% % if there are no 10 pat in a row
b=1;
for a=1:1:length(pat_temp)-50
if  sum(pat_2)==0;
    tei = find(pat(a:a+50>0));
    if length(tei)>15
    pat_2(b) = mean(pat(a:a+50));
    CI_pat2(b) = (length(tei)/10);
    
    else
    pat_2(b) = mean(pat(a:a+50));
    CI_pat2(b) = 1;
    end
end
b=b+1;
end    





% pat_temp=pat;
% pat_temp(pat_temp>0)=1;
% % if there are no 10 pat in a row
% if sum(pat_2(1:round(0.5*end)))==0;
%    if  sum(pat_temp(1:round(0.5*end)))< 20
%        pat_2(round(0.4*length(pat))) = 0;
%        CI_pat
%    else
%        tr = pat(pat(1:round(0.5*end))>0);
%        pat_2(round(0.4*end)) = mean(tr(1:20));
%    end
% end
% clear tr;
% % if there are no 10 pat in a row
% if sum(pat_2(round(0.5*end):end))==0;
%    if  sum(pat_temp(round(0.5*end):end))< 20
%        pat_2(round(0.9*length(pat))) = 0;
%    else
%        Paty =pat(round(0.5*length(pat)):end);
%        tr = Paty((Paty>0.1));% - (round(0.5*length(pat))-1));
%        pat_2(round(0.9*end)) = mean(tr(1:20));
%    end
% end
% 




