% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <TimecheckPAT>
% Content   : Script for determination of pat quality and if 10 sec window
%             avarage is possible
% Version   : GIT 2
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% input: all ok pats, CI_temp, the min time to check the pats (ie is ther a
% 10 sec window with all good pats in a row?
% output
% pats of 10 sec window if available
% CI of 5 at those beat detections where 10sec window is taken
% by E.C. Wentink 30/10/2014 . .--
% %  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%%%%%%%%%%%%%%%%%%%%%%

function [pat,CI_pat,CI_pat_t2,patstd,HR,HRstd,output] = TimecheckPAT(output, CI_pat, e, CI_pat_t2, mintime2check) 
pat1 = output.pat.pat1.signal;
HR =nan; HRstd =nan;pat=[nan;nan;nan];patstd=[nan;nan;nan];
% Check if one Pat next to the other is not bigger /  smaller than the variablity threshold
pat_var_threshold = 0.05;
if e>1
    if abs(pat1(1,e)-(pat1(1,e-1))) < pat_var_threshold
        CI_pat_t(e) =1;
    else
        CI_pat_t(e) =0;
    end
else
    CI_pat_t(e) = 0;
end
CI_pat(e) = CI_pat(e) * CI_pat_t(e);

% Check for missing beats in the ECG
% ie. if the current rr-interval is bigger/smaller than the mean rr-interval +/- 2*std of the interval

CI_pat_t2(1:5)=0;
if e>=5 % there must be at least 5 ecg beats detected (guesstimate to have sufficient intervals for calculating std)
   if CI_pat(e-1)>0 && CI_pat(e)>0 %previous and current CI must be > 0
        if sum(CI_pat(e-4:e)) == 5 % there then must be at least 5 pats detected (if CI =0 there is no pat, if CI=1 there is a pat, so in this section tot nr of pat=5)
            % 5 pats in 10 seconds corresponds to the smallest HR (ish..) of 0.5Hz
          %check if ...
          if sum(CI_pat_t2)<5 && e<9
             CI_pat_t2(e-4:e)=1; % you can only check if the amount of beats is >=5, want to assume them to be 0 at first, but if this holds than also previous ones should be ok
          else
            CI_pat_t2(e)=1; % if e>9 then only the current pat is ok and current CI_pat_t2 gets value 1 assigned
          end
            rr = diff(output.ecg.beats(e-4:e)); %RR interval of ECG in this section
            mdf = mean(rr); %mean of RR-nterval
            sdf=std(rr);%std of RR-nterval
            df =  find(rr<mdf-sdf*2 | rr>mdf+sdf*2); % check if the rr is bigger /  smaller than threshold
            % check if there is an RR interval that deviates too much from the rest (ie is there a mis-detection or missed beat? -> than skip)
            if  size(df,2)==0
                CI_pat_t2(e)=1;
%                 disp('no missing beat');
                if sum(CI_pat_t2(1:e))>5
                   r= find(CI_pat_t2>0);
                   x= output.ecg.beats(1,r(1)); 
                   y= output.ecg.beats(1,r(end));
                   if y-x>=mintime2check % there should be 10 sec of data and within that 10 sec all good pats and no missing beats
                      % varables only on the 10 sec window
                      pat(1,:) = nanmean(pat1(1,r(1):r(end))); % mean pat on 10sec window
                      patstd(1,:) = nanstd(pat1(1,r(1):r(end))); % std of pat on 10 sec window
                      pat(2,:) = nanmean(pat1(2,r(1):r(end))); % same for features
                      patstd(2,:) = nanstd(pat1(2,r(1):r(end)));
                      pat(3,:) = nanmean(pat1(3,r(1):r(end)));
                      patstd(3,:) = nanstd(pat1(3,r(1):r(end)));
                      HR = 60./nanmean(diff(output.ecg.beats(1,r(1):r(end)))); % the HR on the 10 sec window
                      HRstd= nanstd(60./diff(output.ecg.beats(1,r(1):r(end)))); % the HR std on the 10 sec window
                      % variables on the 10 sec window and on the 30 sec window
                      output.pat.pat2.signal(1,e) = nanmean(pat1(1,r(1):r(end))); % same as above but added to the 30 sec window calculations
                      output.pat.pat2std(1,e) = nanstd(pat1(1,r(1):r(end)));
                      output.pat.pat2.signal(2,e) = nanmean(pat1(2,r(1):r(end)));
                      output.pat.pat2std(2,e) = nanstd(pat1(2,r(1):r(end)));
                      output.pat.pat2.signal(3,e) = nanmean(pat1(3,r(1):r(end)));
                      output.pat.pat2std(3,e) = nanstd(pat1(3,r(1):r(end)));
                      output.pat.HR2(e) = 60./nanmean(diff(output.ecg.beats(1,r(1):r(end))));
                      output.pat.HR2std(e)= nanstd(60./diff(output.ecg.beats(1,r(1):r(end))));
                      % the CI on both the 10 and 30 sec window
                      output.pat.CI_pat(1,e) = 5; % the CI on the pats for this 10 sec window average is 5;

                   end
                end              
            else
                CI_pat_t2(1:e) = 0;%if there are missed beats CI= 0
                HR =nan; HRstd =nan;pat=[nan;nan;nan];patstd=[nan;nan;nan];% make all varables nan when not up to standard
                disp('missing beat')
            end
        else % there are not 5 pat in this section
            CI_pat_t2(e) = 0;
            HR =nan; HRstd =nan;pat=[nan;nan;nan];patstd=[nan;nan;nan];%
        end
   else
     CI_pat_t2(1:e) = 0;
      HR =nan;HRstd=nan;pat=[nan;nan;nan];patstd=[nan;nan;nan];%
   end
else
  CI_pat_t2(1:e) = 0;
 HR =nan;HRstd=nan;pat=[nan;nan;nan];patstd=[nan;nan;nan];%
end
%     