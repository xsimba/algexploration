% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : BP.m
%  Content    : Blood Pressure Computation
%  Package    : SIMBA.Validation
%  Created by : F. Beutel (fabian.beutel@imec-nl.nl)
%  Date       : XXXXXXX
%  Modification and Version History: 
%  | Developer    | Version |    Date    |     Changes      |
%  |  fbeutel     |   1.0   | 16-09-2014 |                  |
%  |  EC Wentink  |   2.0   | 23-09-2014 | no subtraction and plotting, but only multiplying by -1 |
%
% This script assumes an incoming light intensity (I) signal and inverts it to PPG
%
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function [data] = PPGinv(data)
   if isfield(data ,'lit') % So you don't flip twice
      return;
   else
    data.lit =data.ppg; % make from "ppg naming light intensity, as that is what it is
    data.ppg.a.signal=data.lit.a.signal*-1; % flip the signal by multiplying by -1 than also trackable through the system if it is flipped or not.
    data.ppg.b.signal=data.lit.b.signal*-1;
    data.ppg.c.signal=data.lit.c.signal*-1;
    data.ppg.d.signal=data.lit.d.signal*-1;

    if isfield(data.ppg ,'e')
    data.ppg.e.signal=data.lit.e.signal*-1;
    data.ppg.f.signal=data.lit.f.signal*-1;
    end    
    if isfield(data.ppg ,'g')
    data.ppg.g.signal=data.lit.g.signal*-1;
    data.ppg.h.signal=data.lit.h.signal*-1;
    end 

   end
end

