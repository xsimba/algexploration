function [beginTimeSC,endTimeSC,spot_check_OK] = checkValidity_SC(spot_check,maxDurationSC,minDurationSC);
% input:    spot_check signal
%           maximal spot check duration (in seconds);
%           minimum spot check duration (in seconds);

spot_check_OK = 0;


timeSC = spot_check.timestamps;
signalSC = spot_check.signal;

% find end flag spot check
endSC = find(signalSC==3)-1;
if isempty(endSC) 
    if any(diff(spot_check.signal)) % old spotcheck format (from 1 to 2 instead of 3)
        endSC = find(diff(spot_check.signal)==1);
    else % data may contain OK spot check but missing end flag
        endSC = length(signalSC);
    end
end

beginSC = NaN*zeros(size(endSC));
for spotC = 1:length(endSC)
    % identify start of spot check (last time it was 2)
    % spot check start just after
    tempSC = find(signalSC(1:endSC(spotC))==1,1,'last');
    bSC = find(signalSC(1:tempSC)==2,1,'last')+1;
    if isempty(bSC) % first (and only) spot check might not have begin flag
        if (spotC==1) && ((timeSC(endSC(spotC)) - timeSC(1)) > minDurationSC) && ...
           ((timeSC(endSC(spotC)) - timeSC(1)) < maxDurationSC)
                
            beginSC(spotC) = 1;
        else
            beginSC(spotC)  =   NaN;
            endSC(spotC)    =   NaN;
        end
    else
        beginSC(spotC) = bSC;
    end
    
    if (~isnan(beginSC(spotC)) && ~isnan(endSC(spotC))) && ... % should be a number to checks below
        all(signalSC(beginSC(spotC):endSC(spotC))==1) && ...  % during spot check flag should be 1
            ((timeSC(endSC(spotC))-timeSC(beginSC(spotC)))> minDurationSC) &&...
            ((timeSC(endSC(spotC))-timeSC(beginSC(spotC)))< maxDurationSC) % spot check should have certain length
        
        spot_check_OK = 1;       
    else
        % (probably) not a good spot check
        % remove spot check
        beginSC(spotC)  =   NaN;
        endSC(spotC)    =   NaN;
      
    end
end

beginTimeSC = timeSC(beginSC(~isnan(beginSC)));
endTimeSC   = timeSC(endSC(~isnan(endSC)));

if spot_check_OK==0;
    %whole measurement treated as spot_check
    beginTimeSC = timeSC(1);
    endTimeSC = timeSC(end);
    
end
