% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <PPG_HR_Extraction>
% Content   : PPG HR Extraction algorithm
% Version   : 0.0.0
% Author    : E. Hermeling (evelien.hermeling@imec-nl.nl)
%  Modification and Version History:
%  | Developer    | Version |    Date    |     Changes      |
%  | E. Hermeling | 0.0.0   |  06/22/15  |

%
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% This function is similar to FeatureExtraction.m
% This function computes  ppg-HR raw, average, and spot check (including sd and CI).
%
%
% A feature is defined as a combination of 1 or more previously determined
% time/amplitude points (calculated in the beat detection)
% In contrast to FeatureExtraction.m HR is aligned with respect to upstroke in the PPG
% ====> number of values in one spotcheck differences from FeatureExtraction.m
%

% Input:
% - 'input':
%   data struct from previous algos
%   -- PPG beat detection (i.e. 'input.ppg.(chan).bd.upstroke)
%   -- spot_check.signal,  either from simband or extracted afterFeatureExtraction.m
% - 'chan':
%   ppg channel
%
% Output: - HR
%                 + timestamps and for each feature:
%                 + signal
%                 + sd
%                 + CI
%                 + runningAverage over valid values
%                 + runningAverage_SD (standard deviation over valid values)
%                 + runningAverage_CI (percentage of valid values)
%       - Spotcheck
%                 + last values of runningAverage;



function input = PPG_HR_Extraction(input, chan)

debug_flag = 0;
max_duration_spot_check = 40; % (in seconds)
min_duration_spot_check = 15; % (in seconds)

HR_limit(1) = 30; % lower limit
HR_limit(2) = 220; % upper limit
varLimit = 35; %

% get data
chan = chan(end);
ppg.bd = input.ppg.(chan).bd;
upstrokes = ppg.bd.upstroke;
nrbeats = length(upstrokes)-1;

CIppg = input.ppg.(chan).mat_CIraw.signal;
CIppg_timestamps = input.ppg.(chan).mat_CIraw.timestamps;

CIbd_ppg = input.ppg.(chan).bd.ci.level;
CIbd_ppg_timestamps = input.ppg.(chan).bd.ci.timestamps;

%spot_check = input.spot_check;
curSpotcheck = 0;
curSpotcheckValid = 0;

%idxStart_goodQ = 0;
startTimeSpotcheck = NaN;
startIdxSpotcheck = 0;
% Allocate memory for matrices
% of variables of each feature calculate for each beat


HRppg.signal = nan(1,nrbeats); % raw signal
HRppg.CI = nan(1,nrbeats);
HRppg.valid = nan(1,nrbeats);
HRppg.runningAverage = nan(1,nrbeats); % avg signal
HRppg.runningAverage_sd = nan(1,nrbeats); % sd signal
HRppg.runningAverage_CI = nan(1,nrbeats); % percentage accepted beats
HRppg.timestamps = nan(1,nrbeats); % ...and the corresponding timestamps

previousHR = nan(1,1);


good_feature.min_CIraw_ppg = 0;
good_feature.min_CIbd_ppg = 0;


for beat=2:nrbeats ;
   curTime = upstrokes(beat);
    %idxSpotcheck = find(input.spot_check.timestamps < curTime,1,'last');
    idxSpotcheck = find(input.spot_check.timestamps > curTime & input.spot_check.timestamps < upstrokes(beat+1));
    if  ~isempty(idxSpotcheck)
    
        if max(input.spot_check.signal(idxSpotcheck)) == 2
            if debug_flag == 1; disp(['flag = 2 ',num2str(curTime)]); end
            % spot check not yet started (or prematurely ended)
            startIdxSpotcheck = 0;
            curSpotcheckValid = 0;
            startTimeSpotcheck = nan(1,1);
             
        elseif (startIdxSpotcheck == 0) && max((input.spot_check.signal(idxSpotcheck))) == 1 % flag of start spot_check
            if debug_flag == 1; disp(['first time flag = 1 ',num2str(curTime)]); end
            curSpotcheckValid = 0;
            startTimeSpotcheck  = curTime;
            startIdxSpotcheck = beat;
                 
        elseif max(input.spot_check.signal(idxSpotcheck)) == 3 % end flag of spot_check
            if debug_flag == 1;  disp(['flag = 3 ',num2str(curTime)]); end
			previousHR = NaN;
            % check validity of spot_check that just ended
            if startIdxSpotcheck > 0 ...
                    && ((curTime - startTimeSpotcheck) > min_duration_spot_check )...         % longer than min spot check duration
                    && ((curTime - startTimeSpotcheck) < max_duration_spot_check )... % shorter than max spot check duration
                    && all(input.spot_check.signal(startIdxSpotcheck:idxSpotcheck-1)==1);   % between begin and end flag spotcheck signal should be 1
                curSpotcheck = curSpotcheck + 1;
                curSpotcheckValid = 1;
            end
            
        end
    end
    
    
    HRppg.timestamps(beat) = curTime;
    
    idx_CIppg = find(CIppg_timestamps >= curTime,1);
    idx_CIbd_ppg = find(CIbd_ppg_timestamps >= curTime,1);
     
    currentHR  = 60./(upstrokes(beat+1)-upstrokes(beat));
    
    
    
    %% check validity of  new feature
    %CI 0: no value
    %CI 1: invalid CI raw (ppg/ecg/both depending on feature)
    %CI 2: invalid CI BD (from ppg)
    %CI 3: not within absolute limits
    %CI 4: not wihin variability limits
    %CI 5: OK
    
    if ~isempty(currentHR) && ~isnan(currentHR)
        % raw contains all values (including invalid)
        HRppg.signal(beat) = currentHR;
        

        % if CI of signal (ppg/ecg) should be used, check if within boundary
        if  (CIppg(idx_CIppg) < good_feature.min_CIraw_ppg)
            % CIecg or CIppg of insufficient quality
            HRppg.CI(beat) = 1;
            
        else
            % check if CI of BD ppg is valid
            if  (CIbd_ppg(idx_CIbd_ppg) < good_feature.min_CIbd_ppg)
                % CI BD of ecg and/or CI BD of ppg of insufficient quality
                HRppg.CI(beat) = 2;
            else
            % if limits for this feature are declared
            % check of raw signal is within these boundary
                if (currentHR < HR_limit(1)) || (currentHR > HR_limit(2))
                    % not between boundary values
                    HRppg.CI(beat) = 3;
                else
                    % either no limits described for this variable
                    % or currentValue within the limits
                
                
                    if  ~isnan(previousHR) && (abs(currentHR-previousHR) > varLimit)
                        % variablility threshold exceeded
                        HRppg.CI(beat) = 4;
                    else
                        % either no variation limits given
                        %   or no history (previousValue(f) NaN)
                        %   or within variation limit
                    
                        % ===> quality OK
                        HRppg.CI(beat) = 5;
                        HRppg.valid(beat) = currentHR;
                    end
                
                    previousHR = currentHR;
                end
            end
        end
    else
        % no value could be calculated
        % one or more nans in variables
        HRppg.signal(beat) = previousHR;
        HRppg.CI(beat) = 0;
        
    end
    
    % get average and sd of all PRECEDING valid values in CURRENT spotcheck
    if startIdxSpotcheck > 0
        validValues = ~isnan(HRppg.valid(startIdxSpotcheck:beat));
        
        HRppg.runningAverage(beat) = nanmean(HRppg.valid(startIdxSpotcheck:end));
        HRppg.runningAverage_sd(beat) = nanstd(HRppg.valid(startIdxSpotcheck:end));
        HRppg.runningAverage_CI(beat) =  (sum(validValues)/numel(validValues)*3+1);
    end
    
    if curSpotcheckValid == 1; % only done once for every spotcheck just after end-flag and only when spotcheck is valid
        if debug_flag == 1; disp(['valid spotcheck # ',num2str(curSpotcheck)]); end
        
        input.ppg.(chan).spotcheck.HRppg.signal(curSpotcheck) = HRppg.runningAverage(beat);
        input.ppg.(chan).spotcheck.HRppg.sd(curSpotcheck) = HRppg.runningAverage_sd(beat);
        input.ppg.(chan).spotcheck.HRppg.CI(curSpotcheck) = HRppg.runningAverage_CI(beat);
        
        curSpotcheckValid = 0;
        startIdxSpotcheck = 0;
    end
    
    
end

input.ppg.(chan).HRppg = HRppg;
