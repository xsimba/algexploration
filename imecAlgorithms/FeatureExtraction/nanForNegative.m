function y = nanForNegative(x)

if x>0
    y=x;
else
    y=NaN;
end

