% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <featureExtraction>
% Content   : Main script for feature extraction (replaced PAT)
% Version   : 0.0.0
% Author    : E. Hermeling (evelien.hermeling@imec-nl.nl)
%  Modification and Version History:
%  | Developer    | Version |    Date    |     Changes      |
%  | E. Hermeling | 0.0.0   |  06/02/15  | Generalization PAT.m to extract other features and get high quality feature
%  | E. Hermeling | 0.0.1   |  06/17/15  | Some bug fixes and including spotcheck CIs
%  | E. Hermeling | 0.0.2   |  06/22/15  | Added comments
%  | F. Beutel    | 0.1.0   |  06/30/15  | Validated and thresholds set for firsst C implementation |
%  | F. Beutel    | 0.1.1   |  07/02/15  | 
%  | E. Hermeling | 0.1.2   |  07/09/15  | Bugs fixes
%  | F. Beutel    | 0.1.3   |  08/04/15  | Based PAT foot computation on output of TDE instead of BD |
%
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% This function computes a PAT and HR and other features, raw, average,
% high quality and spot check value (including sd and CI).
%
% A feature is defined as a combination of 1 or more previously determined
% time/amplitude points (calculated in the beat detection)
% all features are aligned with respect to the rpeak in the ECG
%
% Input:
% - 'input':
%   data struct from previous algos
%   -- ECG beat detection (i.e. 'input.ecg.bd.rpeak')
%   -- PPG beat detection (i.e. 'input.ppg.(chan).bd.upstroke)
%   -- preferably spot_check.signal, otherwise complete measurement will be
%   treated as one spotcheck
% - 'chan':
%   ppg channel

% Output:
%       - 'features', e.g. PAT for BP or PWV features
%          + timestamps and for each feature:
%                           + signal         
%                           + sd
%                           + CI
%                           + valid values (CI> threshold)
%                           + runningAverage over valid values
%                           + runningAverage_SD (standard deviation over valid values)
%                           + runningAverage_CI (percentage of valid values)
%       - 'highquality'      : high quality average, see powerpoint presentation for details
%          + timestamps and for each feature:
%                           + signal (HQ-value,)
%                           + sd (standard deviation over HQ values)
%                           + CI  (see power point presentation!)
%       - 'spotcheck'       last value of highquality signal
%          + timestamps and for each feature: (see powerpoint)
%                           + signal (average over HQ values)
%                           + sd (standard deviation over HQ values)
%                           + CI 
% NOTE CIraw, i.e. CIecg and CIppg are treated as features


function input = featureExtraction(input, chan)

max_duration_spot_check = 40; % (in seconds)
min_duration_spot_check = 15; % (in seconds)
%input can still specify signal type(e.g. ppg), but only channel is handled(e.g. e)
chan = chan(end);

%%%  part not for real-time implementation  %%%
% use to be able to gather all available data, although some fields
% might be empty or not valid

debug_flag = 1;

if ~isfield(input,'spot_check')
    
    startSC = find(input.ecg.bd.rpeak>0,1,'first');
    endSC = find(input.ecg.bd.rpeak>0,1,'last');
    
    input.spot_check.timestamps = input.ecg.bd.rpeak(startSC):input.ecg.bd.rpeak(endSC);
    input.spot_check.signal= ones(size(input.spot_check.timestamps));
end



[beginTimeSC,endTimeSC,spotcheck_OK] = checkValidity_SC(input.spot_check,max_duration_spot_check,min_duration_spot_check);
if debug_flag ==1; disp(['spotcheck_OK = ',num2str(spotcheck_OK)]);end
        
if ~isfield(input.ppg.(chan),'mat_CIraw')
    % no CI raw: always accept (CI>>min_CI)
    input.ppg.(chan).mat_CIraw.timestamps = input.timestamps(1):1:input.timestamps(end);
    input.ppg.(chan).mat_CIraw.signal = 99*ones(size(input.ppg.(chan).mat_CIraw.timestamps));
end

if ~isfield(input.ecg,'mat_CIraw')
    % no CI raw: always accept (CI>>min_CI)
    input.ecg.mat_CIraw.timestamps = input.timestamps(1):1:input.timestamps(end);
    input.ecg.mat_CIraw.signal = 99*ones(size(input.ecg.mat_CIraw.timestamps));
end

if ~isfield(input.ppg.(chan).bd,'ci')
    % no CI BD: always accept (CI>>min_CI)
    input.ppg.(chan).bd.ci.timestamps = input.timestamps(1):1:input.timestamps(end);
    input.ppg.(chan).bd.ci.level = 99*ones(size(input.ppg.(chan).mat_CIraw.timestamps));
end

if ~isfield(input.ecg.bd,'ci')
    % no CI BD: always accept (CI>>min_CI)
    input.ecg.CI_beat.timestamps = input.timestamps(1):1:input.timestamps(end);
    input.ecg.CI_beat.signal = 99*ones(size(input.ppg.(chan).mat_CIraw.timestamps));
end

%%% end part not for real-time implementation %%%


%% START FEATURE DEFINITIONS %%
% define below all "new features" that need to be processed
% idea is that the processing steps are very similar for each new variable
% and only one routine can be used to calculate them.
%
% IMPORTANT: all features are time-aligned to R peak of ECG!
% new features is either CI raw of PPG or ECG and CI_BD (SHOULD BE DEFINED FIRST!!)
% or
% new features is a direct copy of variables from BD (either PPG or ECG)
% or
% new features is calculated as function of one or more variables from BD (either PPG or ECG)
% 
% IMPORTANT: in BD some characteristics are calculated from the previous
% beat (as you cannot see the future). To correctly align these features
% for each variable the prevBeat need to be defined!
% prevBeat = 1 => variable is calculate from previous beat so we should use NEXT beat
% => to calculate HR difference timedifference between current and next rpeak is used
%       in this case also prevBeat=1 (for one of the two variables)

newFeature(1).name = 'CIecg';
newFeature(1).nrVars = 1;
newFeature(1).function = @(v) v;
newFeature(1).var1 = 'CIecg';
newFeature(1).prevBeat1 = 0; %variable from previous beat

newFeature(2).name = 'CIppg';
newFeature(2).nrVars = 1;
newFeature(2).function = @(v) v;
newFeature(2).var1 = 'CIppg';
newFeature(2).prevBeat1 = 0; %variable from previous beat

newFeature(3).name = 'CIbd_ecg';
newFeature(3).nrVars = 1;
newFeature(3).function = @(v) v;
newFeature(3).var1 = 'CIbd_ecg';
newFeature(3).prevBeat1 = 0; %variable from previous beat

newFeature(4).name = 'CIbd_ppg';
newFeature(4).nrVars = 1;
newFeature(4).function = @(v) v;
newFeature(4).var1 = 'CIbd_ppg';
newFeature(4).prevBeat1 = 0; %variable from previous beat

newFeature(5).name ='HR';
newFeature(5).nrVars = 2;
newFeature(5).function = @(v) 60./(v(1)-v(2));
newFeature(5).var1 ='ecg.bd.rpeak'; % var1 - var2
newFeature(5).prevBeat1 = 1; %variable from next beat
newFeature(5).var2 ='ecg.bd.rpeak';
newFeature(5).prevBeat2 = 0; %variable from previous beat?
newFeature(5).limit(1) = 30; % lower limit
newFeature(5).limit(2) = 220; % upper limit
newFeature(5).varLimit = 35; %

newFeature(6).name ='pat';
newFeature(6).nrVars = 2;
newFeature(6).function = @(v) (v(1)-v(2));
newFeature(6).var1 ='ppg.bd.upstroke'; % var1 - var2
newFeature(6).prevBeat1 = 0; %variable from previous beat?
newFeature(6).var2 ='ecg.bd.rpeak';
newFeature(6).prevBeat2 = 0; %variable from previous beat?
newFeature(6).limit(1) = 0.15; % (in seconds)
newFeature(6).limit(2) = 0.45; % (in seconds)
newFeature(6).varLimit  = 0.035; % (in seconds)

newFeature(7).name = 'ppg_pp_ft_amp';
newFeature(7).nrVars = 2;
newFeature(7).function = @(v) v(2) - v(1);
newFeature(7).var1 = 'ppg.filtsig.ftamp';
newFeature(7).prevBeat1 = 0; %variable from previous beat?
newFeature(7).var2 = 'ppg.filtsig.ppamp';
newFeature(7).prevBeat2 = 1; %variable from previous beat?

newFeature(8).name = 'risetime';
newFeature(8).nrVars = 1;
newFeature(8).function = @(v) v;
newFeature(8).var1 = 'ppg.bd.risetime';
newFeature(8).prevBeat1 = 1; %variable from previous beat?
%newFeature(8).limit(1) = 0.10   ; % (in seconds)
%newFeature(8).limit(2) = 0.20; % (in seconds)
%newFeature(8).varLimit  = 0.01; % (in seconds)

newFeature(9).name = 'decaytime';
newFeature(9).nrVars = 1;
newFeature(9).function = @(v) v;
newFeature(9).var1 = 'ppg.bd.decaytime';
newFeature(9).prevBeat1 = 1; %variable from previous beat?

newFeature(10).name = 'RCtime';
newFeature(10).nrVars = 1;
newFeature(10).function = @(v) v;
newFeature(10).var1 = 'ppg.bd.RCtime';
newFeature(10).prevBeat1 = 1; %variable from previous beat?

newFeature(11).name ='rise_decay_ratio';
newFeature(11).nrVars = 2;
newFeature(11).function = @(v) v(1)/v(2);
newFeature(11).var1 ='ppg.bd.risetime'; % var1/var2
newFeature(11).prevBeat1 = 1; %variable from previous beat?
newFeature(11).var2 ='ppg.bd.decaytime';
newFeature(11).prevBeat2 = 1; %variable from previous beat?

newFeature(12).name = 'ppg_upstgrad';
newFeature(12).nrVars = 1;
newFeature(12).function = @(v) v;
newFeature(12).var1 = 'ppg.bd.upstgrad';
newFeature(12).prevBeat1 = 0; %variable from previous beat?

newFeature(13).name ='pat_foot';
newFeature(13).nrVars = 2;
newFeature(13).function = @(v) (v(1)-v(2));
%newFeature(13).var1 ='ppg.bd.foot'; % var1 - var2
newFeature(13).var1 ='tde.beatsTDE.timestamps'; % var1 - var2
newFeature(13).prevBeat1 = 0; %variable from previous beat?
newFeature(13).var2 ='ecg.bd.rpeak';
newFeature(13).prevBeat2 = 0; %variable from previous beat?
newFeature(13).limit(1) = 0.1; % (in seconds)
newFeature(13).limit(2) = 0.4; % (in seconds)
newFeature(13).varLimit  = 0.035; % (in seconds)

newFeature(14).name ='foot_amp';
newFeature(14).nrVars = 1;
newFeature(14).function = @(v) v;
newFeature(14).var1 ='ppg.filtsig.ftamp'; % var1 - var2
newFeature(14).prevBeat1 = 0; %variable from previous beat?

newFeature(15).name ='pp_amp';
newFeature(15).nrVars = 1;
newFeature(15).function = @(v) v;
newFeature(15).var1 ='ppg.filtsig.ppamp'; % var1 - var2
newFeature(15).prevBeat1 = 1; %variable from previous beat?

newFeature(16).name ='ejection_time';
newFeature(16).nrVars = 2;
newFeature(16).function = @(v) (v(1)-v(2));
newFeature(16).var1 ='ppg.bd.dicrnot'; % var1 - var2
newFeature(16).prevBeat1 = 1; %variable from previous beat?
newFeature(16).var2 ='ppg.bd.foot';
newFeature(16).prevBeat2 = 0; %variable from previous beat?

newFeature(17).name ='pp_sp_int';
newFeature(17).nrVars = 2;
newFeature(17).function = @(v) nanForNegative(v(1)-v(2));
newFeature(17).var1 ='ppg.bd.secpeak'; % var1 - var2
newFeature(17).prevBeat1 = 1; %variable from previous beat?
newFeature(17).var2 ='ppg.bd.pripeak';
newFeature(17).prevBeat2 = 1; %variable from previous beat?

good_feature.min_CIraw_ecg = 2;
good_feature.min_CIraw_ppg = 2;
good_feature.min_CIbd_ecg = 0;
good_feature.min_CIbd_ppg = 0;

%conditions for good quality: HR and PAT CI = 5
good_quality(1).featureName = 'HR';
good_quality(1).minCI = 5;
good_quality(2).featureName = 'pat';
good_quality(2).minCI = 5;
%good_quality(3).featureName = 'risetime';
%good_quality(3).minCI = 5;


%fixed input parameters for timing
duration_hq_interval = 10;%seconds
min_good_beats = 10; % if no hq present

% get data
ecg_r = input.ecg.bd.rpeak;
nrbeats = length(ecg_r)-1;

previousValue = NaN(size(newFeature));

ppg.bd = input.ppg.(chan).bd;
ppg.filtsig = input.ppg.(chan).filtsig;
if isfield(input.ppg.(chan), 'beatsTDE')
    tde.beatsTDE = input.ppg.(chan).beatsTDE; 
else
    tde.beatsTDE.timestamps = NaN;
end
    
ecg = input.ecg; %#ok<NASGU>

%CI of PPG and ECG beats
CIecg = input.ecg.mat_CIraw.signal; %#ok<NASGU>
CIecg_timestamps = input.ecg.mat_CIraw.timestamps;

CIppg = input.ppg.(chan).mat_CIraw.signal; %#ok<NASGU>
CIppg_timestamps = input.ppg.(chan).mat_CIraw.timestamps;

CIbd_ppg = input.ppg.(chan).bd.ci.level; %#ok<NASGU>
CIbd_ppg_timestamps = input.ppg.(chan).bd.ci.timestamps;

CIbd_ecg = input.ecg.bd.ci.level; %#ok<NASGU>
CIbd_ecg_timestamps = input.ecg.bd.ci.timestamps;


curSpotcheck = 0;
curSpotcheckValid = 0;

idxStart_goodQ = 0;
startTimeSpotcheck = NaN;
startIdxSpotcheck = 0;

% Allocate memory for matrices
% of variables of each feature calculate for each beat
%disp('should check whether CI idx (from timestamps) is OKE, maybe use closest?: line 332')

for f=1:length(newFeature);
    features.(newFeature(f).name).signal = nan(1,nrbeats); % raw signal
    features.(newFeature(f).name).CI = nan(1,nrbeats);
    features.(newFeature(f).name).valid = nan(1,nrbeats);
    
    features.(newFeature(f).name).runningAverage = nan(1,nrbeats); % avg signal
    features.(newFeature(f).name).runningAverage_sd = nan(1,nrbeats); % sd signal
    features.(newFeature(f).name).runningAverage_CI = zeros(1,nrbeats); % average CI
    
    highquality.(newFeature(f).name).signal= nan(1,nrbeats);
    highquality.(newFeature(f).name).sd = nan(1,nrbeats);
                   
    % if spotcheck not valid, spotcheck will have 1 NaN per feature
    spotcheck.(newFeature(f).name).signal(1) = NaN;
    spotcheck.(newFeature(f).name).sd(1) = NaN;
    spotcheck.timestamps(1) = NaN;
    spotcheck.CI(1) = NaN;
    
    
end
features.timestamps = nan(1,nrbeats); % ...and the corresponding timestamps
highquality.nElapsedBeats  = zeros(1,nrbeats);
highquality.goodquality = boolean(zeros(1,nrbeats));
highquality.CI =  nan(1,nrbeats);
highquality.timestamps = nan(1,nrbeats);

beginTimeSC(end+1) = NaN; % to allow search of beginTimeSC after last spotcheck (end before end of file)
endTimeSC(end+1) = NaN;


for beat=2:nrbeats ;
    
    curTime = ecg_r(beat);   
    
    features.timestamps(beat) = curTime;

    % indices for different signal types
    idx_ppg = find(ppg.bd.upstroke >= curTime & ppg.bd.upstroke < ecg_r(beat+1),1);
    idx_tde = find(tde.beatsTDE.timestamps >= curTime & tde.beatsTDE.timestamps < ecg_r(beat+1),1);
    idx_CIppg = find(CIppg_timestamps >= curTime,1);
    idx_CIecg = find(CIecg_timestamps >= curTime,1);
    idx_CIbd_ecg = find(CIbd_ecg_timestamps >= curTime,1);
    idx_CIbd_ppg = find(CIbd_ppg_timestamps >= curTime,1);
    
    for f=1:length(newFeature);
        
        
        %% STEP 1) calculate new feature from BD variables
        
        % 1a) get correct indices for all variables (belonging to either ppg or ecg)                 
        var = nan(newFeature(f).nrVars,1);
        check_CIppg = 0;
        check_CIecg = 0;
        for v = 1:newFeature(f).nrVars
            
            curVarName = ['var',num2str(v)];
            prevBeatName = ['prevBeat',num2str(v)];
            
            vardata = eval(newFeature(f).(curVarName));
            if strcmp(newFeature(f).(curVarName)(1:3),'ecg') % index of ecg (is used as reference)
                idx = beat + newFeature(f).(prevBeatName);
                check_CIecg = 1; % CI of ecg only used when ecg used in feature extraction
            elseif strcmp(newFeature(f).(curVarName)(1:3),'ppg') % use corresponding index of ppg
                idx = idx_ppg + newFeature(f).(prevBeatName);
                check_CIppg = 1; % CI of ppg only used when ppg used in feature extraction
            elseif strcmp(newFeature(f).(curVarName)(1:5),'CIecg') % use corresponding index of CI ecg
                idx = idx_CIecg + newFeature(f).(prevBeatName);
            elseif strcmp(newFeature(f).(curVarName)(1:5),'CIppg') % use corresponding index of CI ppg
                idx = idx_CIppg + newFeature(f).(prevBeatName);
            elseif strcmp(newFeature(f).(curVarName)(1:6),'CIbd_e') % use corresponding index of CI BD ecg
                idx = idx_CIbd_ecg + newFeature(f).(prevBeatName);
            elseif strcmp(newFeature(f).(curVarName)(1:6),'CIbd_p') % use corresponding index of CI BD ppg
                idx = idx_CIbd_ppg + newFeature(f).(prevBeatName); 
            elseif strcmp(newFeature(f).(curVarName)(1:3),'tde') % use corresponding index of TDE
                idx = idx_tde + newFeature(f).(prevBeatName); 
            else
                keyboard
                disp('poorly defined feature')
            end
            
            if ~isempty(idx) && (idx(1) >= 1) && (idx(1) <= length(vardata));
                var(v) = vardata(idx(1));
            end
        end
        
        % 1b) and calculate new feature
        if ~any(isnan(v))
            currentValue = newFeature(f).function(var);
        else
            currentValue = nan;
        end
        
        
        %% STEP 2) check validity of  new feature
        %CI 0: no value
        %CI 1: invalid CI raw (ppg/ecg/both depending on feature)
        %CI 2: invalid CI BD (from ppg)
        %CI 3: not within absolute limits
        %CI 4: not wihin variability limits
        %CI 5: OK
        
        
        if ~isempty(currentValue) && ~isnan(currentValue)
            % raw contains all values (including invalid)
            features.(newFeature(f).name).signal(beat) = currentValue;
            

            
            % check if CI of raw signal (ppg/ecg) is valid
            if (check_CIecg && (features.CIecg.signal(beat) < good_feature.min_CIraw_ecg))...
                    || (check_CIppg && (features.CIppg.signal(beat) < good_feature.min_CIraw_ppg))
                % CI raw of ecg and/or CI raw of ppg of insufficient quality
                features.(newFeature(f).name).CI(beat) = 1;
                

            else
                 % check if CI of BD (ppg/ecg) is valid
                if (check_CIecg && (features.CIbd_ecg.signal(beat) < good_feature.min_CIbd_ecg))...
                    || (check_CIppg && (features.CIbd_ppg.signal(beat) < good_feature.min_CIbd_ppg))
                % CI BD of ecg and/or CI BD of ppg of insufficient quality
                features.(newFeature(f).name).CI(beat) = 2;
                    
                else
                    
                    % if limits for this feature are declared
                    % check of raw signal is within these boundary
                    if (~isempty(newFeature(f).limit)) && ...
                            ((currentValue < newFeature(f).limit(1)) ||...
                            (currentValue > newFeature(f).limit(2)) )
                        % not between boundary values
                        features.(newFeature(f).name).CI(beat) = 3;
                    else
                        % either no limits described for this variable
                        % or currentValue within the limits
                        
                        
                        if (~isempty(newFeature(f).varLimit) && ~isnan(previousValue(f)) &&...
                                abs(currentValue-previousValue(f))> newFeature(f).varLimit)
                            % variablility threshold exceeded
                            features.(newFeature(f).name).CI(beat) = 4;
                        else
                            % either no variation limits given
                            %   or no history (previousValue(f) NaN)
                            %   or within variation limit
                            
                            % ===> quality OK
                            features.(newFeature(f).name).CI(beat) = 5;
                            features.(newFeature(f).name).valid(beat) = currentValue;
                        end
                        
                        previousValue(f) = currentValue;
                    end
                end
            end
        else
            % no value could be calculated
            % one or more nans in variables
            features.(newFeature(f).name).signal(beat) = previousValue(f);
            features.(newFeature(f).name).CI(beat) = 0;
            
        end
        
        % get average and sd of all PRECEDING valid values in CURRENT spotcheck
        if startIdxSpotcheck > 0
            validValues = ~isnan(features.(newFeature(f).name).valid(startIdxSpotcheck:beat));
            
            features.(newFeature(f).name).runningAverage(beat) = nanmean(features.(newFeature(f).name).valid(startIdxSpotcheck:end));
            features.(newFeature(f).name).runningAverage_sd(beat) = nanstd(features.(newFeature(f).name).valid(startIdxSpotcheck:end));
            features.(newFeature(f).name).runningAverage_CI(beat) =  (sum(validValues)/numel(validValues)*3+1);
        end
        
        
        
    end
    
    upstroke_afterNextRpeak = ppg.bd.upstroke(find(ppg.bd.upstroke > ecg_r(beat+1),1));
    if  curSpotcheckValid == 0 && startIdxSpotcheck == 0 && curTime >= beginTimeSC(curSpotcheck+1)
        % spotcheck started
        startIdxSpotcheck = beat;
        idxStart_goodQ = 0;
        curSpotcheck = curSpotcheck + 1;
        if debug_flag
            disp(['start spotcheck, time =',num2str(curTime),' beat number = ',num2str(beat)])
        end
        
    elseif curSpotcheckValid == 0 && startIdxSpotcheck > 0 && ecg_r(beat+1) >= endTimeSC(curSpotcheck) &&...
            (isempty(upstroke_afterNextRpeak) || upstroke_afterNextRpeak >= endTimeSC(curSpotcheck))
        % spotcheck ended
        previousValue = NaN(size(newFeature));
        curSpotcheckValid = 1;
        if debug_flag
            disp(['end spotcheck, time =',num2str(curTime),' beat number = ',num2str(beat)])
        end
    end
    
    
    
    if startIdxSpotcheck > 0
        
        
        % HIGH QUALITY (techniqually this could in a different algorithm.
        goodQ = zeros(length(good_quality),1);
        poorQ_CI =nan(length(good_quality),1);
        for f = 1:length(good_quality)
            % feature CI should be at least of minimal value
            goodQ(f) = features.(good_quality(f).featureName).CI(beat)>=good_quality(f).minCI;
            poorQ_CI(f) = (features.(good_quality(f).featureName).runningAverage_CI(beat)-1)/3;
        end
        
        % all good quality features reach CI limit
        highquality.goodquality(beat) =  all(goodQ);
        
        idxHQ=0;
        if highquality.goodquality(beat)==1
            highquality.nElapsedBeats(beat)  = highquality.nElapsedBeats (beat-1)+1;
            if idxStart_goodQ == 0 % first beat of good quality
                idxStart_goodQ = beat;
            elseif (curTime - features.timestamps(idxStart_goodQ)) > duration_hq_interval
                % a high quality interval is present, good quality longer than duartion_hq_interval                
                idxHQ = find((curTime -features.timestamps(idxStart_goodQ:beat)-duration_hq_interval)>0,1,'last');
                idxHQ = idxHQ + idxStart_goodQ - 1;
            end
        else
            idxStart_goodQ = 0;
            idxHQ = 0;
        end
        
        for f = 1:length(newFeature);
            
            if idxHQ>0
                % currently a high quality interval
                highquality.(newFeature(f).name).signal(beat) = nanmean(features.(newFeature(f).name).signal(idxHQ:beat));
                highquality.timestamps(beat) = curTime;
                highquality.(newFeature(f).name).sd(beat) = nanstd(features.(newFeature(f).name).signal(idxHQ:beat));
                highquality.CI(beat) = 5;
                
            elseif highquality.CI(beat-1) == 5
                % currently no high quality, but previously a hq int in spotcheck
             
                highquality.(newFeature(f).name).signal(beat) = highquality.(newFeature(f).name).signal(beat-1);
                highquality.timestamps(beat) = highquality.timestamps(beat-1);
                highquality.(newFeature(f).name).sd(beat) = highquality.(newFeature(f).name).sd(beat-1);
                highquality.CI(beat) = 5;
            elseif sum(highquality.goodquality(startIdxSpotcheck:beat))> min_good_beats
                % no high quality interval, but 10 good quality beat
                % (spread over spot check)
        
                highquality.(newFeature(f).name).signal(beat) = nanmean(features.(newFeature(f).name).signal(highquality.goodquality));
                highquality.timestamps(beat) =  curTime;
                highquality.(newFeature(f).name).sd(beat) = nanstd(features.(newFeature(f).name).signal(highquality.goodquality));
                highquality.CI(beat) = round((sum(highquality.goodquality(startIdxSpotcheck:beat))./numel(highquality.goodquality(startIdxSpotcheck:beat)))*2)+2;
            else
                % no high quality or 10 good quality beats
                highquality.(newFeature(f).name).signal(beat) = features.(newFeature(f).name).runningAverage(beat);
                highquality.timestamps(beat) = curTime;
                highquality.(newFeature(f).name).sd(beat) = features.(newFeature(f).name).runningAverage_sd(beat);
                highquality.CI(beat) = mean(poorQ_CI)+1;
                                
            end
            
            if curSpotcheckValid == 1; % only done once for every spotcheck just after end-flag and only when spotcheck is valid

                spotcheck.(newFeature(f).name).signal(curSpotcheck) = highquality.(newFeature(f).name).signal(beat);
                spotcheck.(newFeature(f).name).sd(curSpotcheck) = highquality.(newFeature(f).name).sd(beat);
                spotcheck.timestamps(curSpotcheck)  = curTime;  %highquality.timestamps(beat);
                spotcheck.CI(curSpotcheck)  = highquality.CI(beat);

                if f==length(newFeature)
                    if debug_flag==1;  disp(['valid spotcheck # ',num2str(curSpotcheck)]); end;
                    curSpotcheckValid = 0;
                    startIdxSpotcheck = 0;
                end
                
            end
        end
    end
end


input.ppg.(chan).features = features;
input.ppg.(chan).highquality = highquality;
input.ppg.(chan).spotcheck = spotcheck;
