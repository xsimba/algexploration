% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <featureExtraction_noSpotcheck_noHQ>
% Content   : Main script for feature extraction (replaced PAT)
% Version   : 0.0.0
% Author    : E. Hermeling (evelien.hermeling@imec-nl.nl)
%  Modification and Version History:
%  | Developer    | Version |    Date    |     Changes      |
%  | E. Hermeling | 0.0.0   |  06/02/15  | Generalization PAT.m to extract other features and get high quality feature
%  | E. Hermeling | 0.0.1   |  06/17/15  | Some bug fixes and including spotcheck CIs
%  | E. Hermeling | 0.0.2   |  06/22/15  | Added comments
%
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% This function computes a PAT and HR and other features, raw, average,
% high quality and spot check value (including sd and CI).
%
% A feature is defined as a combination of 1 or more previously determined
% time/amplitude points (calculated in the beat detection)
% all features are aligned with respect to the rpeak in the ECG
%
% Input:
% - 'input':
%   data struct from previous algos
%   -- ECG beat detection (i.e. 'input.ecg.bd.rpeak')
%   -- PPG beat detection (i.e. 'input.ppg.(chan).bd.upstroke)
% - 'chan':
%   ppg channel

% Output:
%       - 'features', e.g. PAT for BP or PWV features
%          + timestamps and for each feature:
%                           + signal         
%                           + runningAverage (mean over xx beats)
%                           + runningMedian (median over xx beats)
%                           + runningSD (sd over xx beats);

function input = FeatureExtraction_noSpotcheck_noHQ(input, chan)

newFeature(1).name ='HR';
newFeature(1).nrVars = 2;
newFeature(1).function = @(v) 60./(v(1)-v(2));
newFeature(1).var1 ='ecg.bd.rpeak';
newFeature(1).prevBeat1 = 1; %variable from next beat
newFeature(1).var2 ='ecg.bd.rpeak';
newFeature(1).prevBeat2 = 0; %variable from previous beat?
newFeature(1).limit(1) = 30; % lower limit
newFeature(1).limit(2) = 220; % upper limit
newFeature(1).varLimit = 35; %

newFeature(2).name ='pat';
newFeature(2).nrVars = 2;
newFeature(2).function = @(v) (v(1)-v(2));
newFeature(2).var1 ='ppg.bd.upstroke'; 
newFeature(2).prevBeat1 = 0; %variable from previous beat?
newFeature(2).var2 ='ecg.bd.rpeak';
newFeature(2).prevBeat2 = 0; %variable from previous beat?
newFeature(2).limit(1) = 0.15; % (in seconds)
newFeature(2).limit(2) = 0.45; % (in seconds)
newFeature(2).varLimit  = 0.035; % (in seconds)

newFeature(3).name ='ejection_time';
newFeature(3).nrVars = 2;
newFeature(3).function = @(v) nanForNegative(v(1)-v(2));
newFeature(3).var1 ='ppg.bd.dicrnot'; 
newFeature(3).prevBeat1 = 1; %variable from previous beat?
newFeature(3).var2 ='ppg.bd.foot';
newFeature(3).prevBeat2 = 0; %variable from previous beat?

newFeature(4).name ='pp_sp_int';
newFeature(4).nrVars = 2;
newFeature(4).function = @(v) nanForNegative(v(1)-v(2));
newFeature(4).var1 ='ppg.bd.secpeak'; 
newFeature(4).prevBeat1 = 1; %variable from previous beat?
newFeature(4).var2 ='ppg.bd.pripeak';
newFeature(4).prevBeat2 = 1; %variable from previous beat?

newFeature(5).name ='pat_foot';
newFeature(5).nrVars = 2;
newFeature(5).function = @(v) (v(1)-v(2));
newFeature(5).var1 ='ppg.bd.foot'; 
newFeature(5).prevBeat1 = 0; %variable from previous beat?
newFeature(5).var2 ='ecg.bd.rpeak';
newFeature(5).prevBeat2 = 0; %variable from previous beat?
%newFeature(5).limit(1) = 0.1; % (in seconds)
%newFeature(5).limit(2) = 0.4; % (in seconds)
%newFeature(5).varLimit  = 0.035; % (in seconds)

newFeature(6).name = 'augmentation_index';
newFeature(6).nrVars = 3;
newFeature(6).function = @(v) ( v(1) - v(2) )/ ( v(1) - v(3) );
newFeature(6).var1 ='ppg.filtsig.ppamp'; 
newFeature(6).prevBeat1 = 1; %variable from previous beat?
newFeature(6).var2 ='ppg.filtsig.spamp';
newFeature(6).prevBeat2 = 1; %variable from previous beat?
newFeature(6).var3 ='ppg.filtsig.ftamp';
newFeature(6).prevBeat3 = 0; %variable from previous beat?


newFeature(7).name = 'pp_ft_amp';
newFeature(7).nrVars = 2;
newFeature(7).function = @(v) v(2) - v(1);
newFeature(7).var1 = 'ppg.filtsig.ftamp';
newFeature(7).prevBeat1 = 0; %variable from previous beat?
newFeature(7).var2 = 'ppg.filtsig.ppamp';
newFeature(7).prevBeat2 = 1; %variable from previous beat?

newFeature(8).name = 'risetime';
newFeature(8).nrVars = 1;
newFeature(8).function = @(v) v;
newFeature(8).var1 = 'ppg.bd.risetime';
newFeature(8).prevBeat1 = 1; %variable from previous beat?

newFeature(9).name = 'decaytime';
newFeature(9).nrVars = 1;
newFeature(9).function = @(v) v;
newFeature(9).var1 = 'ppg.bd.decaytime';
newFeature(9).prevBeat1 = 1; %variable from previous beat?

newFeature(10).name = 'RCtime';
newFeature(10).nrVars = 1;
newFeature(10).function = @(v) v;
newFeature(10).var1 = 'ppg.bd.RCtime';
newFeature(10).prevBeat1 = 1; %variable from previous beat?

newFeature(11).name ='rise_decay_ratio';
newFeature(11).nrVars = 2;
newFeature(11).function = @(v) v(1)/v(2);
newFeature(11).var1 ='ppg.bd.risetime'; % var1/var2
newFeature(11).prevBeat1 = 1; %variable from previous beat?
newFeature(11).var2 ='ppg.bd.decaytime';
newFeature(11).prevBeat2 = 1; %variable from previous beat?

newFeature(12).name = 'upstgrad';
newFeature(12).nrVars = 1;
newFeature(12).function = @(v) v;
newFeature(12).var1 = 'ppg.bd.upstgrad';
newFeature(12).prevBeat1 = 0; %variable from previous beat?

newFeature(13).name = 'relative_upstroke_gradient';
newFeature(13).nrVars = 3;
newFeature(13).function = @(v) v(1)/( v(2)-v(3) );
newFeature(13).var1 ='ppg.bd.upstgrad'; % var1/var2
newFeature(13).prevBeat1 = 0; %variable from previous beat?
newFeature(13).var2 ='ppg.filtsig.ftamp';
newFeature(13).prevBeat2 = 0; %variable from previous beat?
newFeature(13).var3 ='ppg.filtsig.ppamp';
newFeature(13).prevBeat3 = 1; %variable from previous beat?

newFeature(14).name = 'DC_foot';
newFeature(14).nrVars = 1;
newFeature(14).function = @(v) v;
newFeature(14).var1 = 'ppg.filtsig.ftamp';
newFeature(14).prevBeat1 = 0; %variable from previous beat?

newFeature(15).name = 'DC_peak';
newFeature(15).nrVars = 1;
newFeature(15).function = @(v) v;
newFeature(15).var1 = 'ppg.filtsig.ppamp';
newFeature(15).prevBeat1 = 1; %variable from previous beat?


% get data
ecg_r = input.ecg.bd.rpeak;
nrbeats = length(ecg_r)-1;


chan = chan(end);
ppg.bd = input.ppg.(chan).bd;
ppg.filtsig = input.ppg.(chan).filtsig;
ecg = input.ecg;

numberOfBeats2Average = 5;

% Allocate memory for matrices
% of variables of each feature calculate for each beat
for f=1:length(newFeature);
    features.(newFeature(f).name).signal = nan(1,nrbeats); % raw signal  
    features.(newFeature(f).name).runningAverage = nan(1,nrbeats); % avg signal
    features.(newFeature(f).name).runningMedian = nan(1,nrbeats); % median signal 
    features.(newFeature(f).name).runningSD = nan(1,nrbeats); % sd signal
    features.(newFeature(f).name).valid = nan(1,nrbeats);
end

previousValue = nan(size(newFeature));
features.timestamps = nan(1,nrbeats); % ...and the corresponding timestamps
for beat=2:nrbeats ;
   
    curTime = ecg_r(beat); 
    features.timestamps(beat) = curTime;
    
    for f=1:length(newFeature);
        
        
        %% STEP 1) calculate new feature from BD variables
        
        % 1a) get correct indices for all variables (belonging to either ppg or ecg)
        
        idx_ppg = find(ppg.bd.upstroke >= curTime & ppg.bd.upstroke < ecg_r(beat+1),1);
                
        var = nan(newFeature(f).nrVars,1);
        for v = 1:newFeature(f).nrVars
            
            curVarName = ['var',num2str(v)];
            prevBeatName = ['prevBeat',num2str(v)];
            
            vardata = eval(newFeature(f).(curVarName));
            if strcmp(newFeature(f).(curVarName)(1:3),'ecg') % index of ecg (is used as reference)
                idx = beat + newFeature(f).(prevBeatName);
   
            elseif strcmp(newFeature(f).(curVarName)(1:3),'ppg') % use corresponding index of ppg
                idx = idx_ppg + newFeature(f).(prevBeatName);
            else
                disp('unknown feature')
            end
            
            if ~isempty(idx) && (idx(1) >= 1) && (idx(1) <= length(vardata));
                var(v) = vardata(idx(1));
            end
        end
        
        % 1b) and calculate new feature
        if ~any(isnan(v))
            currentValue = newFeature(f).function(var);
        else
            currentValue = nan;
        end
        
        features.(newFeature(f).name).signal(beat) = currentValue;
        if (~isempty(newFeature(f).limit)) && ...
                ((currentValue < newFeature(f).limit(1)) ||...
                (currentValue > newFeature(f).limit(2)) )
            % in default featureExtraction, the CI is asigned here;
        else
            % either no limits described for this variable
            % or currentValue within the limits
            
            
            if (~isempty(newFeature(f).varLimit) && ~isnan(previousValue(f)) &&...
                    abs(currentValue-previousValue(f))> newFeature(f).varLimit)
                % variablility threshold exceeded
                % in default featureExtraction, the CI is asigned here;
            else
                % either no variation limits given
                %   or no history (previousValue(f) NaN)
                %   or within variation limit
                
                % ===> quality OK
                
                features.(newFeature(f).name).valid(beat) = currentValue;
            end
            
            previousValue(f) = currentValue;
            
        end
        
       % get average and sd of numberBeats2Average PRECEDING values
        startBeat = max([1,beat-numberOfBeats2Average]); 
        
        features.(newFeature(f).name).runningAverage(beat) =  nanmean(features.(newFeature(f).name).signal(startBeat:beat));
        features.(newFeature(f).name).runningMedian(beat) = nanmedian(features.(newFeature(f).name).signal(startBeat:beat));
        features.(newFeature(f).name).runningSD(beat) =  nanstd(features.(newFeature(f).name).signal(startBeat:beat));
        
        %features.(newFeature(f).name).runningAverage(beat) =  nanmean(features.(newFeature(f).name).valid(startBeat:beat));
        %features.(newFeature(f).name).runningMedian(beat) = nanmedian(features.(newFeature(f).name).valid(startBeat:beat));
        %features.(newFeature(f).name).runningSD(beat) =  nanstd(features.(newFeature(f).name).valid(startBeat:beat));

    end
end

input.ppg.(chan).features = features;