% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : processUCSF4.m
%  Content    : Wrapper for loading Simband and spreadsheet data, execution
%  of all algorithms and generating overviews and error plots of the data
%  Version    : 1.0
%  Package    : SIMBA
%  Created by : F.Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 05-08-2015
%  Modification and Version History:
%  | Developer | Version |    Date    |     Changes      |
%  | F.Beutel  |   1.0   |  05-08-15  |                  |

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc; warning off;

%make sure that data containing SRC aligned UCSF4 files is declared as
%current external database in RCSetup.m

N_subjects = 92; %92
start_subject = 1;
bad_subjects = [19, 29, 30, 67, 84, 89]; 
%Explanation:
    %19 band did not output values for all 6 spotchecks
    %29 same 19
    %30 Subscript indices must either be real positive integers or logicals.
    % Error in PulseWaveAnalysis (line 127)
    %             [xf1 xf2 ~] = QuadraticZeroPeakDet(sa(zcindf-1), sa(zcindf), sa(zcindf+1));
    %67 same as 30
    %84 is completely empty
    %89 same as 30
good_subjects = [2, 7, 9, 11, 12, 13, 14, 16, 17, 18, 22, 23, 24, 25, 27, 28, 33, 34, 35, 36, 39, 42, 44, 47, 48, 51, 52, 54, 58, 60, 61, 62, 64, 65, 71, 75, 77, 78, 81, 82, 85, 86, 88, 91, 92]; 
%selected_subjects = [2, 7, 9, 11, 12, 13, 14, 16, 17, 18, 22];

database = 'simband_data_UCSF4';

%spreadsheet_data
spreadsheet_data.spreadsheet_ndata = xlsread(['simband_manual_data_time_adjusted.xlsx']);
[~,spreadsheet_data.spreadsheet_txtdata] = xlsread(['simband_manual_data_time_adjusted.xlsx']);

%key spreadsheet ndata (column-wise)
%1:rID
%2:gender
%3:age
%4:height
%5:weight
%27:trial_nr
%30:DBPmeas
%31:SBPmeas
%32:SimbandID
%33:DBPest,Simband
%34:SBPest,Simband

%define new matrix containing only the relevant quantities above
spreadsheet_selection = spreadsheet_data.spreadsheet_ndata(:, [1, 27, 2, 3, 4, 5, 31, 30, 32, 34, 33]);
%1:rID
%2:trial_nr
%3:gender
%4:age
%5:height
%6:weight
%7:SBPmeas
%8:DBPmeas
%9:SimbandID
%10:SBPest,Simband
%11:DBPest,Simband

%simband_data
all_files_database = what(database);
mat_files_database = all_files_database.mat;
database_folder = all_files_database.path;

%define spreadsheet that only contains valid subjects in which number
%of trials matches number of spotchecks recorded
spreadsheet_valid_subjects = [];

%for i = start_subject:N_subjects %to identify good subjects
for i = 1:length(good_subjects) %good_subjects %when good subjects are already identified
    rID = good_subjects(i); %selected_subjects
    
    %rID = i; %to identify good subject
    rID_string = ['rID-' num2str(rID) '_dID']; %recording/subject_id    
    
    if ~ismember(rID, bad_subjects)        
        
        disp(['Processing ' rID_string])
        
        %get selected spreadsheet info only for current subject
        spreadsheet_selection_subject = spreadsheet_selection(spreadsheet_selection(:,1) == rID, :);
        
        %find all files of rID
        rID_file_indices = [];
        for j = 1:numel(mat_files_database)
            search_results = findstr(mat_files_database{j}, rID_string);
            if ~isempty(search_results)
                rID_file_indices = [rID_file_indices, j];
            end
        end
        
        rID_mat_files = mat_files_database(rID_file_indices)
        
        %for more than 1 file per subject, check whether files are listed in correct sequence as indicated by
        %spreadsheet document
        if numel(rID_mat_files) ~= 1 && ~isempty(rID_mat_files)
            %define vector containing the filenumbers
            file_numbers = [];
            for j = 1:numel(rID_mat_files)
                %parse string to extract the filename
                [~, first_remain] = strtok(rID_mat_files{j},'-');
                [~, second_remain] = strtok(first_remain,'-');
                [~, third_remain] = strtok(second_remain,'-');
                [target_sequence, fourth_remain] = strtok(third_remain,'_');
                %append filenumber to vector
                file_numbers = [file_numbers, abs(str2num(target_sequence))];
            end
            %sort identified filenumbers and mat files of subject accordingly
            [sorted_file_numbers, sorted_indices_file_numbers] = sort(file_numbers);
            disp('Files sorted to:')
            rID_mat_files = rID_mat_files(sorted_indices_file_numbers, 1)
        end
        
        
        %check whether any recordings for subject exist
        if isempty(rID_mat_files)
            spotchecks_subject = NaN;
            timestamps = NaN;
            SBP_meas = NaN;
            DBP_meas = NaN;                        
            SBP_est_abs = NaN;
            DBP_est_abs = NaN;
            SBP_est_track_PWV = NaN;
            DBP_est_track_PWV = NaN;
            SBP_est_track_PAT = NaN;
            DBP_est_track_PAT = NaN;  
            SBP_cal_track_PWV = NaN;
            DBP_cal_track_PWV = NaN;
            SBP_cal_track_PAT = NaN;
            DBP_cal_track_PAT = NaN;             
            PAT = NaN;
            PWV = NaN;
            HR = NaN;
            PAT_CI = NaN;
            PPG_CI_raw = NaN;
            ECG_CI_raw = NaN;
            PPG_risetime_avg = NaN;
            PPG_risetime_sd = NaN;
            PPG_decaytime_avg = NaN;
            PP_SP_int_avg = NaN;
            calibration_spotchecks = NaN;
            delta_refSC_SBP_meas = NaN;
            delta_refSC_DBP_meas = NaN;
            delta_refSC_SBP_est_track_PWV = NaN;
            delta_refSC_DBP_est_track_PWV = NaN;
            delta_refSC_SBP_cal_track_PWV = NaN;
            delta_refSC_DBP_cal_track_PWV = NaN;
            delta_refSC_SBP_est_track_PAT = NaN;
            delta_refSC_DBP_est_track_PAT = NaN;
            delta_refSC_SBP_cal_track_PAT = NaN;
            delta_refSC_DBP_cal_track_PAT = NaN;             
            
        else
            %iterate over files per subject, executeAllAlgorithms and save
            %spotchecks and BP outcomes
            
            %define vectors to be filled per subject
            spotchecks_subject = [];
            timestamps = [];
            SBP_meas = [];
            DBP_meas = [];            
            SBP_est_abs = [];
            DBP_est_abs = [];
            SBP_est_track_PWV = [];
            DBP_est_track_PWV = [];
            SBP_est_track_PAT = [];
            DBP_est_track_PAT = [];
            SBP_cal_track_PWV = [];
            DBP_cal_track_PWV = [];
            SBP_cal_track_PAT = [];
            DBP_cal_track_PAT = [];            
            PAT = [];
            PWV = [];
            HR = [];
            PAT_CI = [];
            PPG_CI_raw = [];
            ECG_CI_raw = [];
            PPG_risetime_avg = [];
            PPG_risetime_sd = [];
            PPG_decaytime_avg = [];
            PP_SP_int_avg = [];
            calibration_spotchecks = [];
            delta_refSC_SBP_meas = [];
            delta_refSC_DBP_meas = [];
            delta_refSC_SBP_est_track_PWV = [];
            delta_refSC_DBP_est_track_PWV = [];
            delta_refSC_SBP_cal_track_PWV = [];
            delta_refSC_DBP_cal_track_PWV = [];
            delta_refSC_SBP_est_track_PAT = [];
            delta_refSC_DBP_est_track_PAT = [];
            delta_refSC_SBP_cal_track_PAT = [];
            delta_refSC_DBP_cal_track_PAT = [];           
            
            for j = 1:numel(rID_mat_files)
                
                load(rID_mat_files{j})
                %modification for UCSF4 data
                data=aligned_d;
                data.timestamps = (data.timestamps-data.timestamps(1))/1000; %not 1024!
                
                %add reference BP to data struct --> calibration-based tracker
                %section of spreadsheet from current subject only                              
                data.bp.sbpref = spreadsheet_selection_subject(:,7)';
                data.bp.dbpref = spreadsheet_selection_subject(:,8)';
                
                %execution of all algorithms
                    %ATTENTION:
                    %If the spotchecks are in more than 1 file but BP references from metadata are complete beforehand:
                    %1) save the number of spotchecks from previous file (see last block in this for-loop)
                    %2) use reference BP from the subsequent spotcheck(i.e. the first from the new file)                    
                    flip = 0;
                    if j == 1
                        calibration_spotcheck = 1;
                        disp([' '])
                        disp(['Running algorithm chain for ' rID_mat_files{j}])
                        [data]=executeAllAlgorithms(data, flip, calibration_spotcheck);
                        %get ECG CI (not yet in AllAlgorithms)
                        [data] = ECGCIrawv5(data);
                    else
                        calibration_spotcheck = spotchecks_previous_file + 1;
                        disp([' '])
                        disp(['Running algorithm chain for ' rID_mat_files{j}])
                        [data]=executeAllAlgorithms(data, flip, calibration_spotcheck);
                        %get ECG CI (not yet in AllAlgorithms)
                        [data] = ECGCIrawv5(data);
                    end 
       
                %append all variables from file to vectors
                timestamps = [timestamps, data.ppg.e.bp.sbp.timestamps];
                SBP_meas = [SBP_meas, data.bp.sbpref];
                DBP_meas = [DBP_meas, data.bp.dbpref];
                SBP_est_abs = [SBP_est_abs, data.ppg.e.bp.sbp.signal];
                DBP_est_abs = [DBP_est_abs, data.ppg.e.bp.dbp.signal];
                SBP_est_track_PWV = [SBP_est_track_PWV, data.ppg.e.bp.tracker.file.estimation_based.via_pwv.sbp.signal];
                DBP_est_track_PWV = [DBP_est_track_PWV, data.ppg.e.bp.tracker.file.estimation_based.via_pwv.dbp.signal];
                SBP_est_track_PAT = [SBP_est_track_PAT, data.ppg.e.bp.tracker.file.estimation_based.via_pat.sbp.signal];
                DBP_est_track_PAT = [DBP_est_track_PAT, data.ppg.e.bp.tracker.file.estimation_based.via_pat.dbp.signal];
                SBP_cal_track_PWV = [SBP_cal_track_PWV, data.ppg.e.bp.tracker.file.calibration_based.via_pwv.sbp.signal];
                DBP_cal_track_PWV = [DBP_cal_track_PWV, data.ppg.e.bp.tracker.file.calibration_based.via_pwv.dbp.signal];
                SBP_cal_track_PAT = [SBP_cal_track_PAT, data.ppg.e.bp.tracker.file.calibration_based.via_pat.sbp.signal];
                DBP_cal_track_PAT = [DBP_cal_track_PAT, data.ppg.e.bp.tracker.file.calibration_based.via_pat.dbp.signal];                
                PAT = [PAT, data.ppg.e.pat.pat_avg_for_BP.signal'];
                PWV = [PWV, ones(1,length(data.ppg.e.pat.pat_avg_for_BP.signal)).*data.ssb.height.signal(1)./data.ppg.e.pat.pat_avg_for_BP.signal']; %(1) for height as this sometimes differs from the number of detected spotchecks
                HR = [HR, data.ppg.e.pat.HR_avg_for_BP.signal'];
                PAT_CI = [PAT_CI, data.ppg.e.pat.pat_CI_for_BP.signal'];
                PPG_CI_raw = [PPG_CI_raw, data.ppg.e.pat.ppg_raw_CI_avg_for_BP.signal'];
                ECG_CI_raw = [ECG_CI_raw, NaN*ones(1,length(data.ppg.e.bp.sbp.signal))]; %data.ecg.mat_CIraw.signal'
                PPG_risetime_avg = [PPG_risetime_avg, data.pat.ppg_risetime_avg_for_BP.signal'];
                PPG_risetime_sd = [PPG_risetime_sd, data.pat.ppg_risetime_sd_for_BP.signal'];
                PPG_decaytime_avg = [PPG_decaytime_avg, data.ppg.e.pat.ppg_decaytime_avg_for_BP.signal'];
                PP_SP_int_avg = [PP_SP_int_avg, data.ppg.e.pat.pp_sp_int_avg_for_BP.signal'];
                
                %Plot all signals and save figure
%                 debugplot_title = rID_mat_files{j};
%                 plotDebugInfoPATandBP(data, 'ppg.e', debugplot_title(1:end-4))
%                 savefig(debugplot_title(1:end-4)) 

            %save spotchecks from previous file to know which reference to use in next file
            spotchecks_previous_file = numel(SBP_est_abs);
            calibration_spotchecks = [calibration_spotchecks, calibration_spotcheck];
            
            %save relative changes of measured and estimated BP w.r.t. reference spotchecks 
            %ATTENTION: Currently, this is the same as the absolute error after calibration!!!
            
            %since reference BPs are complete beforehand (from spreadsheet), it needs to be
            %checked per iteration over files whether they are distributed over more than 1 file
            if numel(calibration_spotchecks) == 1 
                delta_refSC_SBP_meas = data.bp.sbpref(find(data.bp.sbpref) >= calibration_spotcheck) - data.bp.sbpref(calibration_spotcheck);
                delta_refSC_DBP_meas = data.bp.dbpref(find(data.bp.dbpref) >= calibration_spotcheck) - data.bp.dbpref(calibration_spotcheck);
            elseif numel(calibration_spotchecks) > 1 %if more than 1 file, refSC changes and hence succeeding relative changes
                
                delta_refSC_SBP_meas = data.bp.sbpref;
                delta_refSC_DBP_meas = data.bp.sbpref;
                
                for i=1:length(calibration_spotchecks)
                    
                    if i==length(calibration_spotchecks) %for last calibration spotcheck
                        
                        delta_refSC_SBP_meas(calibration_spotchecks(i):end) = delta_refSC_SBP_meas(calibration_spotchecks(i):end) -  data.bp.sbpref(calibration_spotchecks(i));
                        delta_refSC_DBP_meas(calibration_spotchecks(i):end) = delta_refSC_DBP_meas(calibration_spotchecks(i):end) -  data.bp.dbpref(calibration_spotchecks(i));
                        
                    else %for any other calibration spotcheck
                        
                        delta_refSC_SBP_meas(calibration_spotchecks(i):calibration_spotchecks(i+1)-1) = delta_refSC_SBP_meas(calibration_spotchecks(i):calibration_spotchecks(i+1)-1) - data.bp.sbpref(calibration_spotchecks(i));
                        delta_refSC_DBP_meas(calibration_spotchecks(i):calibration_spotchecks(i+1)-1) = delta_refSC_DBP_meas(calibration_spotchecks(i):calibration_spotchecks(i+1)-1) - data.bp.dbpref(calibration_spotchecks(i));
                        
                    end
                    
                end
                
            end
            
            %PWV based
            delta_refSC_SBP_est_track_PWV = [delta_refSC_SBP_est_track_PWV, SBP_est_track_PWV(find(SBP_est_track_PWV) >= calibration_spotcheck) - SBP_est_track_PWV(calibration_spotcheck)];
            delta_refSC_DBP_est_track_PWV = [delta_refSC_DBP_est_track_PWV, DBP_est_track_PWV(find(DBP_est_track_PWV) >= calibration_spotcheck) - DBP_est_track_PWV(calibration_spotcheck)];
            delta_refSC_SBP_cal_track_PWV = [delta_refSC_SBP_cal_track_PWV, SBP_cal_track_PWV(find(SBP_cal_track_PWV) >= calibration_spotcheck) - SBP_cal_track_PWV(calibration_spotcheck)];
            delta_refSC_DBP_cal_track_PWV = [delta_refSC_DBP_cal_track_PWV, DBP_cal_track_PWV(find(DBP_cal_track_PWV) >= calibration_spotcheck) - DBP_cal_track_PWV(calibration_spotcheck)];            
            %PAT based
            delta_refSC_SBP_est_track_PAT = [delta_refSC_SBP_est_track_PAT, SBP_est_track_PAT(find(SBP_est_track_PAT) >= calibration_spotcheck) - SBP_est_track_PAT(calibration_spotcheck)];
            delta_refSC_DBP_est_track_PAT = [delta_refSC_DBP_est_track_PAT, DBP_est_track_PAT(find(DBP_est_track_PAT) >= calibration_spotcheck) - DBP_est_track_PAT(calibration_spotcheck)];
            delta_refSC_SBP_cal_track_PAT = [delta_refSC_SBP_cal_track_PAT, SBP_cal_track_PAT(find(SBP_cal_track_PAT) >= calibration_spotcheck) - SBP_cal_track_PAT(calibration_spotcheck)];
            delta_refSC_DBP_cal_track_PAT = [delta_refSC_DBP_cal_track_PAT, DBP_cal_track_PAT(find(DBP_cal_track_PAT) >= calibration_spotcheck) - DBP_cal_track_PAT(calibration_spotcheck)];
            
            
            end
            %compute total number of spotchecks per subject
            spotchecks_subject = numel(SBP_est_abs);
            
            %number of spotchecks in last file
            spotchecks_subject_last_file = numel(data.ppg.e.bp.sbp.signal);
        end
        
        %append arrays to struct per subject
        outcome.SBP_meas = data.bp.sbpref;
        outcome.DBP_meas = data.bp.dbpref;
        outcome.spotchecks = spotchecks_subject;
        outcome.timestamps = timestamps;
        outcome.calibration_spotchecks = calibration_spotchecks;
        outcome.SBP_est_abs = SBP_est_abs;
        outcome.DBP_est_abs = DBP_est_abs;
        outcome.SBP_est_track_PWV = SBP_est_track_PWV;
        outcome.DBP_est_track_PWV = DBP_est_track_PWV;
        outcome.SBP_est_track_PAT = SBP_est_track_PAT;
        outcome.DBP_est_track_PAT = DBP_est_track_PAT;
        outcome.SBP_cal_track_PWV = SBP_cal_track_PWV;
        outcome.DBP_cal_track_PWV = DBP_cal_track_PWV;
        outcome.SBP_cal_track_PAT = SBP_cal_track_PAT;
        outcome.DBP_cal_track_PAT = DBP_cal_track_PAT;        
        outcome.PAT = PAT;
        outcome.PWV = PWV;
        outcome.HR = HR;
        outcome.PAT_CI = PAT_CI;
        outcome.PPG_CI_raw = PPG_CI_raw;
        outcome.ECG_CI_raw = ECG_CI_raw;
        outcome.PPG_risetime_avg = PPG_risetime_avg;
        outcome.PPG_risetime_sd = PPG_risetime_sd;
        outcome.PPG_risetime_sd_all_spotchecks = nanstd(PPG_risetime_avg);
        outcome.PPG_decaytime_avg = PPG_decaytime_avg;
        outcome.PP_SP_int_avg = PP_SP_int_avg; 
        %absolute errors
            %PAT_based
                outcome.error_SBP_est_track_PAT = outcome.SBP_est_track_PAT'-spreadsheet_selection_subject(:,7);
                outcome.error_SBP_est_track_PAT_abs = abs(outcome.error_SBP_est_track_PAT);
                outcome.error_SBP_est_track_PAT_mean_abs = nanmean(outcome.error_SBP_est_track_PAT_abs);
                outcome.error_SBP_est_track_PAT_mean_abs_rest = nanmean(outcome.error_SBP_est_track_PAT_abs(2:3));
                outcome.error_SBP_est_track_PAT_mean_abs_rest_exerc = nanmean(outcome.error_SBP_est_track_PAT_abs([2:3, end-2:end]));

                outcome.error_DBP_est_track_PAT = outcome.DBP_est_track_PAT'-spreadsheet_selection_subject(:,8);
                outcome.error_DBP_est_track_PAT_abs = abs(outcome.error_DBP_est_track_PAT);
                outcome.error_DBP_est_track_PAT_mean_abs = nanmean(outcome.error_DBP_est_track_PAT_abs);
                outcome.error_DBP_est_track_PAT_mean_abs_rest = nanmean(outcome.error_DBP_est_track_PAT_abs(2:3));
                outcome.error_DBP_est_track_PAT_mean_abs_rest_exerc = nanmean(outcome.error_DBP_est_track_PAT_abs([2:3, end-2:end]));

                outcome.error_SBP_cal_track_PAT = outcome.SBP_cal_track_PAT'-spreadsheet_selection_subject(:,7);
                outcome.error_SBP_cal_track_PAT_abs = abs(outcome.error_SBP_cal_track_PAT);
                outcome.error_SBP_cal_track_PAT_mean_abs = nanmean(outcome.error_SBP_cal_track_PAT_abs);
                outcome.error_SBP_cal_track_PAT_mean_abs_rest = nanmean(outcome.error_SBP_cal_track_PAT_abs(2:3));        
                outcome.error_SBP_cal_track_PAT_mean_abs_rest_exerc = nanmean(outcome.error_SBP_cal_track_PAT_abs([2:3, end-2:end]));

                outcome.error_DBP_cal_track_PAT = outcome.DBP_cal_track_PAT'-spreadsheet_selection_subject(:,8);
                outcome.error_DBP_cal_track_PAT_abs = abs(outcome.error_DBP_cal_track_PAT); 
                outcome.error_DBP_cal_track_PAT_mean_abs = nanmean(outcome.error_DBP_cal_track_PAT_abs);
                outcome.error_DBP_cal_track_PAT_mean_abs_rest = nanmean(outcome.error_DBP_cal_track_PAT_abs(2:3));
                outcome.error_DBP_cal_track_PAT_mean_abs_rest_exerc = nanmean(outcome.error_DBP_cal_track_PAT_abs([2:3, end-2:end]));        
            %PWV_based
                outcome.error_SBP_est_track_PWV = outcome.SBP_est_track_PWV'-spreadsheet_selection_subject(:,7);
                outcome.error_SBP_est_track_PWV_abs = abs(outcome.error_SBP_est_track_PWV);
                outcome.error_SBP_est_track_PWV_mean_abs = nanmean(outcome.error_SBP_est_track_PWV_abs);
                outcome.error_SBP_est_track_PWV_mean_abs_rest = nanmean(outcome.error_SBP_est_track_PWV_abs(2:3));
                outcome.error_SBP_est_track_PWV_mean_abs_rest_exerc = nanmean(outcome.error_SBP_est_track_PWV_abs([2:3, end-2:end]));

                outcome.error_DBP_est_track_PWV = outcome.DBP_est_track_PWV'-spreadsheet_selection_subject(:,8);
                outcome.error_DBP_est_track_PWV_abs = abs(outcome.error_DBP_est_track_PWV);
                outcome.error_DBP_est_track_PWV_mean_abs = nanmean(outcome.error_DBP_est_track_PWV_abs);
                outcome.error_DBP_est_track_PWV_mean_abs_rest = nanmean(outcome.error_DBP_est_track_PWV_abs(2:3));
                outcome.error_DBP_est_track_PWV_mean_abs_rest_exerc = nanmean(outcome.error_DBP_est_track_PWV_abs([2:3, end-2:end]));

                outcome.error_SBP_cal_track_PWV = outcome.SBP_cal_track_PWV'-spreadsheet_selection_subject(:,7);
                outcome.error_SBP_cal_track_PWV_abs = abs(outcome.error_SBP_cal_track_PWV);
                outcome.error_SBP_cal_track_PWV_mean_abs = nanmean(outcome.error_SBP_cal_track_PWV_abs);
                outcome.error_SBP_cal_track_PWV_mean_abs_rest = nanmean(outcome.error_SBP_cal_track_PWV_abs(2:3));        
                outcome.error_SBP_cal_track_PWV_mean_abs_rest_exerc = nanmean(outcome.error_SBP_cal_track_PWV_abs([2:3, end-2:end]));

                outcome.error_DBP_cal_track_PWV = outcome.DBP_cal_track_PWV'-spreadsheet_selection_subject(:,8);
                outcome.error_DBP_cal_track_PWV_abs = abs(outcome.error_DBP_cal_track_PWV); 
                outcome.error_DBP_cal_track_PWV_mean_abs = nanmean(outcome.error_DBP_cal_track_PWV_abs);
                outcome.error_DBP_cal_track_PWV_mean_abs_rest = nanmean(outcome.error_DBP_cal_track_PWV_abs(2:3));
                outcome.error_DBP_cal_track_PWV_mean_abs_rest_exerc = nanmean(outcome.error_DBP_cal_track_PWV_abs([2:3, end-2:end]));
        %relative errors
           
            %diff errors (not completely correct --> other than in tracker relative change is computed wrt previous measurement/estimation and not wrt refSC! However, when tracker is deployed this metric gives an indication on the computed relative change)
                           

                %PAT based
                    outcome.diff_error_SBP_est_track_PAT = [NaN; diff(outcome.SBP_est_track_PAT)'-diff(outcome.SBP_meas)'];
                    outcome.diff_error_SBP_est_track_PAT_abs = abs(outcome.diff_error_SBP_est_track_PAT);
                    outcome.diff_error_SBP_est_track_PAT_mean_abs = nanmean(outcome.diff_error_SBP_est_track_PAT_abs);
                    outcome.diff_error_SBP_est_track_PAT_mean_abs_rest = nanmean(outcome.diff_error_SBP_est_track_PAT_abs(2:3));
                    outcome.diff_error_SBP_est_track_PAT_mean_abs_rest_exerc = nanmean(outcome.diff_error_SBP_est_track_PAT_abs([2:3, end-2:end]));

                    outcome.diff_error_DBP_est_track_PAT = [NaN; diff(outcome.DBP_est_track_PAT)'-diff(outcome.DBP_meas)'];
                    outcome.diff_error_DBP_est_track_PAT_abs = abs(outcome.diff_error_DBP_est_track_PAT);
                    outcome.diff_error_DBP_est_track_PAT_mean_abs = nanmean(outcome.diff_error_DBP_est_track_PAT_abs);
                    outcome.diff_error_DBP_est_track_PAT_mean_abs_rest = nanmean(outcome.diff_error_DBP_est_track_PAT_abs(2:3));
                    outcome.diff_error_DBP_est_track_PAT_mean_abs_rest_exerc = nanmean(outcome.diff_error_DBP_est_track_PAT_abs([2:3, end-2:end]));

                    outcome.diff_error_SBP_cal_track_PAT = [NaN; diff(outcome.SBP_cal_track_PAT)'-diff(outcome.SBP_meas)'];
                    outcome.diff_error_SBP_cal_track_PAT_abs = abs(outcome.diff_error_SBP_cal_track_PAT);
                    outcome.diff_error_SBP_cal_track_PAT_mean_abs = nanmean(outcome.diff_error_SBP_cal_track_PAT_abs);
                    outcome.diff_error_SBP_cal_track_PAT_mean_abs_rest = nanmean(outcome.diff_error_SBP_cal_track_PAT_abs(2:3));        
                    outcome.diff_error_SBP_cal_track_PAT_mean_abs_rest_exerc = nanmean(outcome.diff_error_SBP_cal_track_PAT_abs([2:3, end-2:end]));

                    outcome.diff_error_DBP_cal_track_PAT = [NaN; diff(outcome.DBP_cal_track_PAT)'-diff(outcome.DBP_meas)'];
                    outcome.diff_error_DBP_cal_track_PAT_abs = abs(outcome.diff_error_DBP_cal_track_PAT); 
                    outcome.diff_error_DBP_cal_track_PAT_mean_abs = nanmean(outcome.diff_error_DBP_cal_track_PAT_abs);
                    outcome.diff_error_DBP_cal_track_PAT_mean_abs_rest = nanmean(outcome.diff_error_DBP_cal_track_PAT_abs(2:3));
                    outcome.diff_error_DBP_cal_track_PAT_mean_abs_rest_exerc = nanmean(outcome.diff_error_DBP_cal_track_PAT_abs([2:3, end-2:end]));        
                %PWV_based
                    outcome.diff_error_SBP_est_track_PWV = [NaN; diff(outcome.SBP_est_track_PWV)'-diff(outcome.SBP_meas)'];
                    outcome.diff_error_SBP_est_track_PWV_abs = abs(outcome.diff_error_SBP_est_track_PWV);
                    outcome.diff_error_SBP_est_track_PWV_mean_abs = nanmean(outcome.diff_error_SBP_est_track_PWV_abs);
                    outcome.diff_error_SBP_est_track_PWV_mean_abs_rest = nanmean(outcome.diff_error_SBP_est_track_PWV_abs(2:3));
                    outcome.diff_error_SBP_est_track_PWV_mean_abs_rest_exerc = nanmean(outcome.diff_error_SBP_est_track_PWV_abs([2:3, end-2:end]));

                    outcome.diff_error_DBP_est_track_PWV = [NaN; diff(outcome.DBP_est_track_PWV)'-diff(outcome.DBP_meas)'];
                    outcome.diff_error_DBP_est_track_PWV_abs = abs(outcome.diff_error_DBP_est_track_PWV);
                    outcome.diff_error_DBP_est_track_PWV_mean_abs = nanmean(outcome.diff_error_DBP_est_track_PWV_abs);
                    outcome.diff_error_DBP_est_track_PWV_mean_abs_rest = nanmean(outcome.diff_error_DBP_est_track_PWV_abs(2:3));
                    outcome.diff_error_DBP_est_track_PWV_mean_abs_rest_exerc = nanmean(outcome.diff_error_DBP_est_track_PWV_abs([2:3, end-2:end]));
                    
                    outcome.diff_error_SBP_cal_track_PWV = [NaN; diff(outcome.SBP_cal_track_PWV)'-diff(outcome.SBP_meas)'];
                    outcome.diff_error_SBP_cal_track_PWV_abs = abs(outcome.diff_error_SBP_cal_track_PWV);
                    outcome.diff_error_SBP_cal_track_PWV_mean_abs = nanmean(outcome.diff_error_SBP_cal_track_PWV_abs);
                    outcome.diff_error_SBP_cal_track_PWV_mean_abs_rest = nanmean(outcome.diff_error_SBP_cal_track_PWV_abs(2:3));        
                    outcome.diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc = nanmean(outcome.diff_error_SBP_cal_track_PWV_abs([2:3, end-2:end]));

                    outcome.diff_error_DBP_cal_track_PWV = [NaN; diff(outcome.DBP_cal_track_PWV)'-diff(outcome.DBP_meas)'];
                    outcome.diff_error_DBP_cal_track_PWV_abs = abs(outcome.diff_error_DBP_cal_track_PWV); 
                    outcome.diff_error_DBP_cal_track_PWV_mean_abs = nanmean(outcome.diff_error_DBP_cal_track_PWV_abs);
                    outcome.diff_error_DBP_cal_track_PWV_mean_abs_rest = nanmean(outcome.diff_error_DBP_cal_track_PWV_abs(2:3));
                    outcome.diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc = nanmean(outcome.diff_error_DBP_cal_track_PWV_abs([2:3, end-2:end]));
                
            %delta errors (essentially correct --> in line with tracker computations relative change wrt refSC)
            
                %PAT based
                    outcome.delta_error_SBP_est_track_PAT = delta_refSC_SBP_est_track_PAT'-delta_refSC_SBP_meas';
                    outcome.delta_error_SBP_est_track_PAT_abs = abs(outcome.delta_error_SBP_est_track_PAT);
                    outcome.delta_error_SBP_est_track_PAT_mean_abs = nanmean(outcome.delta_error_SBP_est_track_PAT_abs);
                    outcome.delta_error_SBP_est_track_PAT_mean_abs_rest = nanmean(outcome.delta_error_SBP_est_track_PAT_abs(2:3));
                    outcome.delta_error_SBP_est_track_PAT_mean_abs_rest_exerc = nanmean(outcome.delta_error_SBP_est_track_PAT_abs([2:3, end-2:end]));

                    outcome.delta_error_DBP_est_track_PAT = delta_refSC_DBP_est_track_PAT'-delta_refSC_DBP_meas';
                    outcome.delta_error_DBP_est_track_PAT_abs = abs(outcome.delta_error_DBP_est_track_PAT);
                    outcome.delta_error_DBP_est_track_PAT_mean_abs = nanmean(outcome.delta_error_DBP_est_track_PAT_abs);
                    outcome.delta_error_DBP_est_track_PAT_mean_abs_rest = nanmean(outcome.delta_error_DBP_est_track_PAT_abs(2:3));
                    outcome.delta_error_DBP_est_track_PAT_mean_abs_rest_exerc = nanmean(outcome.delta_error_DBP_est_track_PAT_abs([2:3, end-2:end]));

                    outcome.delta_error_SBP_cal_track_PAT = delta_refSC_SBP_cal_track_PAT'-delta_refSC_SBP_meas';
                    outcome.delta_error_SBP_cal_track_PAT_abs = abs(outcome.delta_error_SBP_cal_track_PAT);
                    outcome.delta_error_SBP_cal_track_PAT_mean_abs = nanmean(outcome.delta_error_SBP_cal_track_PAT_abs);
                    outcome.delta_error_SBP_cal_track_PAT_mean_abs_rest = nanmean(outcome.delta_error_SBP_cal_track_PAT_abs(2:3));        
                    outcome.delta_error_SBP_cal_track_PAT_mean_abs_rest_exerc = nanmean(outcome.delta_error_SBP_cal_track_PAT_abs([2:3, end-2:end]));

                    outcome.delta_error_DBP_cal_track_PAT = delta_refSC_DBP_cal_track_PAT'-delta_refSC_DBP_meas';
                    outcome.delta_error_DBP_cal_track_PAT_abs = abs(outcome.delta_error_DBP_cal_track_PAT); 
                    outcome.delta_error_DBP_cal_track_PAT_mean_abs = nanmean(outcome.delta_error_DBP_cal_track_PAT_abs);
                    outcome.delta_error_DBP_cal_track_PAT_mean_abs_rest = nanmean(outcome.delta_error_DBP_cal_track_PAT_abs(2:3));
                    outcome.delta_error_DBP_cal_track_PAT_mean_abs_rest_exerc = nanmean(outcome.delta_error_DBP_cal_track_PAT_abs([2:3, end-2:end]));        
                %PWV_based
                    outcome.delta_error_SBP_est_track_PWV = delta_refSC_SBP_est_track_PWV'-delta_refSC_SBP_meas';
                    outcome.delta_error_SBP_est_track_PWV_abs = abs(outcome.delta_error_SBP_est_track_PWV);
                    outcome.delta_error_SBP_est_track_PWV_mean_abs = nanmean(outcome.delta_error_SBP_est_track_PWV_abs);
                    outcome.delta_error_SBP_est_track_PWV_mean_abs_rest = nanmean(outcome.delta_error_SBP_est_track_PWV_abs(2:3));
                    outcome.delta_error_SBP_est_track_PWV_mean_abs_rest_exerc = nanmean(outcome.delta_error_SBP_est_track_PWV_abs([2:3, end-2:end]));

                    outcome.delta_error_DBP_est_track_PWV = delta_refSC_DBP_est_track_PWV'-delta_refSC_DBP_meas';
                    outcome.delta_error_DBP_est_track_PWV_abs = abs(outcome.delta_error_DBP_est_track_PWV);
                    outcome.delta_error_DBP_est_track_PWV_mean_abs = nanmean(outcome.delta_error_DBP_est_track_PWV_abs);
                    outcome.delta_error_DBP_est_track_PWV_mean_abs_rest = nanmean(outcome.delta_error_DBP_est_track_PWV_abs(2:3));
                    outcome.delta_error_DBP_est_track_PWV_mean_abs_rest_exerc = nanmean(outcome.delta_error_DBP_est_track_PWV_abs([2:3, end-2:end]));

                    outcome.delta_error_SBP_cal_track_PWV = delta_refSC_SBP_cal_track_PWV'-delta_refSC_SBP_meas';
                    outcome.delta_error_SBP_cal_track_PWV_abs = abs(outcome.delta_error_SBP_cal_track_PWV);
                    outcome.delta_error_SBP_cal_track_PWV_mean_abs = nanmean(outcome.delta_error_SBP_cal_track_PWV_abs);
                    outcome.delta_error_SBP_cal_track_PWV_mean_abs_rest = nanmean(outcome.delta_error_SBP_cal_track_PWV_abs(2:3));        
                    outcome.delta_error_SBP_cal_track_PWV_mean_abs_rest_exerc = nanmean(outcome.delta_error_SBP_cal_track_PWV_abs([2:3, end-2:end]));

                    outcome.delta_error_DBP_cal_track_PWV = delta_refSC_DBP_cal_track_PWV'-delta_refSC_DBP_meas';
                    outcome.delta_error_DBP_cal_track_PWV_abs = abs(outcome.delta_error_DBP_cal_track_PWV); 
                    outcome.delta_error_DBP_cal_track_PWV_mean_abs = nanmean(outcome.delta_error_DBP_cal_track_PWV_abs);
                    outcome.delta_error_DBP_cal_track_PWV_mean_abs_rest = nanmean(outcome.delta_error_DBP_cal_track_PWV_abs(2:3));
                    outcome.delta_error_DBP_cal_track_PWV_mean_abs_rest_exerc = nanmean(outcome.delta_error_DBP_cal_track_PWV_abs([2:3, end-2:end]));                
                    
                    
        %compare total number of spotchecks to number of trials from spreadsheet (in order to define good subjects!)
        if size(spreadsheet_selection_subject, 1) == spotchecks_subject
            
            
            %append calculated quantities to spreadsheet (only PWV based errors included)
            spreadsheet_selection_subject = [spreadsheet_selection_subject, ...
                outcome.SBP_est_abs', ...
                outcome.DBP_est_abs',  ...
                outcome.SBP_est_track_PWV', ...
                outcome.DBP_est_track_PWV', ...
                outcome.SBP_est_track_PAT', ...
                outcome.DBP_est_track_PAT', ...
                outcome.SBP_cal_track_PWV', ...
                outcome.DBP_cal_track_PWV', ...
                outcome.SBP_cal_track_PAT', ...
                outcome.DBP_cal_track_PAT', ...
                outcome.PAT', ...
                outcome.HR', ...
                outcome.PAT_CI', ...
                outcome.PPG_CI_raw', ...
                outcome.ECG_CI_raw', ...
                outcome.PPG_risetime_avg', ...
                outcome.PPG_risetime_sd', ...
                outcome.PPG_risetime_sd_all_spotchecks*ones(length(outcome.SBP_est_abs),1), ...
                numel(rID_mat_files)*ones(length(outcome.SBP_est_abs),1), ...
                spotchecks_subject*ones(length(outcome.SBP_est_abs),1), ...
                spotchecks_subject_last_file*ones(length(outcome.SBP_est_abs),1), ...
                numel(calibration_spotchecks)*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_SBP_est_track_PWV, ...
                outcome.error_SBP_est_track_PWV_abs, ...
                outcome.error_SBP_est_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_SBP_est_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_SBP_est_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_DBP_est_track_PWV, ...
                outcome.error_DBP_est_track_PWV_abs, ...
                outcome.error_DBP_est_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_DBP_est_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_DBP_est_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_SBP_cal_track_PWV, ...
                outcome.error_SBP_cal_track_PWV_abs, ...
                outcome.error_SBP_cal_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_SBP_cal_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_SBP_cal_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_DBP_cal_track_PWV, ...
                outcome.error_DBP_cal_track_PWV_abs, ...
                outcome.error_DBP_cal_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_DBP_cal_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.error_DBP_cal_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_SBP_est_track_PWV, ...
                outcome.diff_error_SBP_est_track_PWV_abs, ...
                outcome.diff_error_SBP_est_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_SBP_est_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_SBP_est_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_DBP_est_track_PWV, ...
                outcome.diff_error_DBP_est_track_PWV_abs, ...
                outcome.diff_error_DBP_est_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_DBP_est_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_DBP_est_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_SBP_cal_track_PWV, ...
                outcome.diff_error_SBP_cal_track_PWV_abs, ...
                outcome.diff_error_SBP_cal_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_SBP_cal_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_DBP_cal_track_PWV, ...
                outcome.diff_error_DBP_cal_track_PWV_abs, ...
                outcome.diff_error_DBP_cal_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_DBP_cal_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_SBP_est_track_PWV, ...
                outcome.delta_error_SBP_est_track_PWV_abs, ...
                outcome.delta_error_SBP_est_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_SBP_est_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_SBP_est_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_DBP_est_track_PWV, ...
                outcome.delta_error_DBP_est_track_PWV_abs, ...
                outcome.delta_error_DBP_est_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_DBP_est_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_DBP_est_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_SBP_cal_track_PWV, ...
                outcome.delta_error_SBP_cal_track_PWV_abs, ...
                outcome.delta_error_SBP_cal_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_SBP_cal_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_SBP_cal_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_DBP_cal_track_PWV, ...
                outcome.delta_error_DBP_cal_track_PWV_abs, ...
                outcome.delta_error_DBP_cal_track_PWV_mean_abs*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_DBP_cal_track_PWV_mean_abs_rest*ones(length(outcome.SBP_est_abs),1), ...
                outcome.delta_error_DBP_cal_track_PWV_mean_abs_rest_exerc*ones(length(outcome.SBP_est_abs),1)];
            
            
            %columns
            %1:rID
            %2:trial_nr
            %3:gender
            %4:age
            %5:height
            %6:weight
            %7:SBP measured
            %8:DBP measured
            %9:SimbandID
            %10:SBPest,Simband
            %11:DBPest,Simband
            %12:SBPest,absolute BP
            %13:DBPest,absolute BP
            %14:SBPest,track,PWV
            %15:DBPest,track,PWV
            %16:SBPest,track,PAT
            %17:DBPest,track,PAT
            %18:SBPcal,track,PWV
            %19:DBPcal,track,PWV
            %20:SBPcal,track,PAT
            %21:DBPcal,track,PAT
            %22:PAT
            %23:HR
            %24:PAT_CI
            %25:PPG_CI_raw
            %26:ECG_CI_raw
            %27:risetime_avg
            %28:risetime_sd
            %29:risetime_sd_all_spotchecks
            %30:number of files for subject
            %31:number of spotchecks for subject
            %32:number of spotchecks in last file
            %33:number of calibration spotchecks (= number of files)
            %(only PWV errors added)
            %absolute errors
            %34:error_SBP_est_track_PWV
            %35:error_SBP_est_track_PWV_abs
            %36:error_SBP_est_track_PWV_mean_abs
            %37:error_SBP_est_track_PWV_mean_abs_rest
            %38:error_SBP_est_track_PWV_mean_abs_rest_exerc
            %39:error_DBP_est_track_PWV
            %40:error_DBP_est_track_PWV_abs
            %41:error_DBP_est_track_PWV_mean_abs
            %42:error_DBP_est_track_PWV_mean_abs_rest
            %43:error_DBP_est_track_PWV_mean_abs_rest_exerc
            %44:error_SBP_cal_track_PWV
            %45:error_SBP_cal_track_PWV_abs
            %46:error_SBP_cal_track_PWV_mean_abs
            %47:error_SBP_cal_track_PWV_mean_abs_rest
            %48:error_SBP_cal_track_PWV_mean_abs_rest_exerc
            %49:error_DBP_cal_track_PWV
            %50:error_DBP_cal_track_PWV_abs
            %51:error_DBP_cal_track_PWV_mean_abs
            %52:error_DBP_cal_track_PWV_mean_abs_rest
            %53:error_DBP_cal_track_PWV_mean_abs_rest_exerc
            %relative errors
            %54:diff_error_SBP_est_track_PWV
            %55:diff_error_SBP_est_track_PWV_abs
            %56:diff_error_SBP_est_track_PWV_mean_abs
            %57:diff_error_SBP_est_track_PWV_mean_abs_rest
            %58:diff_error_SBP_est_track_PWV_mean_abs_rest_exerc
            %59:diff_error_DBP_est_track_PWV
            %60:diff_error_DBP_est_track_PWV_abs
            %61:diff_error_DBP_est_track_PWV_mean_abs
            %62:diff_error_DBP_est_track_PWV_mean_abs_rest
            %63:diff_error_DBP_est_track_PWV_mean_abs_rest_exerc
            %64:diff_error_SBP_cal_track_PWV
            %65:diff_error_SBP_cal_track_PWV_abs
            %66:diff_error_SBP_cal_track_PWV_mean_abs
            %67:diff_error_SBP_cal_track_PWV_mean_abs_rest
            %68:diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc
            %69:diff_error_DBP_cal_track_PWV
            %70:diff_error_DBP_cal_track_PWV_abs
            %71:diff_error_DBP_cal_track_PWV_mean_abs
            %72:diff_error_DBP_cal_track_PWV_mean_abs_rest
            %73:diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc
            %74:delta_error_SBP_est_track_PWV
            %75:delta_error_SBP_est_track_PWV_abs
            %76:delta_error_SBP_est_track_PWV_mean_abs
            %77:delta_error_SBP_est_track_PWV_mean_abs_rest
            %78:delta_error_SBP_est_track_PWV_mean_abs_rest_exerc
            %79:delta_error_DBP_est_track_PWV
            %80:delta_error_DBP_est_track_PWV_abs
            %81:delta_error_DBP_est_track_PWV_mean_abs
            %82:delta_error_DBP_est_track_PWV_mean_abs_rest
            %83:delta_error_DBP_est_track_PWV_mean_abs_rest_exerc
            %84:delta_error_SBP_cal_track_PWV
            %85:delta_error_SBP_cal_track_PWV_abs
            %86:delta_error_SBP_cal_track_PWV_mean_abs
            %87:delta_error_SBP_cal_track_PWV_mean_abs_rest
            %88:delta_error_SBP_cal_track_PWV_mean_abs_rest_exerc
            %89:delta_error_DBP_cal_track_PWV
            %90:delta_error_DBP_cal_track_PWV_abs
            %91:delta_error_DBP_cal_track_PWV_mean_abs
            %92:delta_error_DBP_cal_track_PWV_mean_abs_rest
            %93:delta_error_DBP_cal_track_PWV_mean_abs_rest_exerc            
            
            %in turn, append to spreadsheet of valid subjects
            spreadsheet_valid_subjects = [spreadsheet_valid_subjects; spreadsheet_selection_subject];
            
            %Save variable with outcomes per subject
            save([database_folder '\' rID_string '_outcome.mat'], 'outcome');
            
            %% Plotting
            
            %Plot tracking trajectory vs absolute BP
            figure, hold on;
            %tracking trajectories
            
            %SBP
                subplot(4,4, [1 2 5 6]), hold on
                plot(data.bp.sbpref,'r'); %reference BP
                plot(outcome.SBP_est_abs', 'm:o'); %SBP_est_abs
                plot(outcome.SBP_est_track_PWV', 'b-o'); %est PWV based tracker SBP
                plot(outcome.SBP_est_track_PAT','b--o'); %est PAT based tracker SBP
                plot(outcome.SBP_cal_track_PWV', 'g-o'); %cal PWV based tracker SBP
                plot(outcome.SBP_cal_track_PAT','g--o'); %cal PAT based tracker SBP
                plot(calibration_spotchecks, outcome.SBP_cal_track_PWV(calibration_spotchecks)', 'o', 'MarkerFaceColor','g'); %cal PWV based tracker SBP
                plot(calibration_spotchecks, outcome.SBP_cal_track_PAT(calibration_spotchecks)','o',  'MarkerFaceColor','g'); %cal PAT based tracker SBP
                %dashed line to separate files
                plot([spotchecks_subject-spotchecks_subject_last_file+.5  spotchecks_subject-spotchecks_subject_last_file+.5], [min(data.bp.sbpref) max(data.bp.sbpref)], 'k--');
                legend('SBP_{cuff}', 'SBP_{absolute}', 'SBP_{tracker, est, PWV}', 'SBP_{tracker, est, PAT}', 'SBP_{tracker, cal, PWV}', 'SBP_{tracker, cal, PAT}')
                xlabel('Spotcheck #')
                ylabel('SBP [mmHg]')
                
                %absolute errors boxplot
                subplot(4,4, [3]), hold on
                title('Absolute Tracking Errors SBP');
                boxplot([outcome.error_SBP_est_track_PWV_abs, outcome.error_SBP_est_track_PAT_abs, outcome.error_SBP_cal_track_PWV_abs, outcome.error_SBP_cal_track_PAT_abs],{'est, PWV','est, PAT','cal, PWV','cal, PAT'});
                scatter(1, outcome.error_SBP_est_track_PWV_mean_abs, 'r', 'fill');
                scatter(3, outcome.error_SBP_cal_track_PWV_mean_abs, 'r', 'fill');
                %MAD in plot
                text(1+0.2, outcome.error_SBP_est_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.error_SBP_est_track_PWV_mean_abs)])
                text(2+0.2, outcome.error_SBP_cal_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.error_SBP_cal_track_PWV_mean_abs)])
                % %of estimations below threshold
                %text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
                %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
                hline(7)
                ylabel('Absolute Deviation [mmHg]')
                                
                %relative errors boxplot
                subplot(4,4, [7]), hold on
                title('Relative Tracking Errors SBP');
                boxplot([outcome.diff_error_SBP_est_track_PWV_abs, outcome.diff_error_SBP_est_track_PAT_abs, outcome.diff_error_SBP_cal_track_PWV_abs, outcome.diff_error_SBP_cal_track_PAT_abs],{'est, PWV','est, PAT','cal, PWV','cal, PAT'});
                scatter(1, outcome.diff_error_SBP_est_track_PWV_mean_abs, 'r', 'fill');
                scatter(3, outcome.diff_error_SBP_cal_track_PWV_mean_abs, 'r', 'fill');
                %MAD in plot
                text(1+0.2, outcome.diff_error_SBP_est_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.diff_error_SBP_est_track_PWV_mean_abs)])
                text(2+0.2, outcome.diff_error_SBP_cal_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.diff_error_SBP_cal_track_PWV_mean_abs)])
                % %of estimations below threshold
                %text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
                %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
                hline(7)
                ylabel('Absolute Deviation [mmHg]')
            
            %DBP
                subplot(4,4, [9 10 13 14]), hold on
                plot(data.bp.dbpref,'r'); %reference BP
                plot(outcome.DBP_est_abs', 'm:o'); %DBP_est_abs
                plot(outcome.DBP_est_track_PWV', 'b-o'); %est PWV based tracker DBP
                plot(outcome.DBP_est_track_PAT','b--o'); %est PAT based tracker DBP
                plot(outcome.DBP_cal_track_PWV', 'g-o'); %cal PWV based tracker DBP
                plot(outcome.DBP_cal_track_PAT','g--o'); %cal PAT based tracker DBP
                plot(calibration_spotchecks, outcome.DBP_cal_track_PWV(calibration_spotchecks)', 'o', 'MarkerFaceColor','g'); %cal PWV based tracker DBP
                plot(calibration_spotchecks, outcome.DBP_cal_track_PAT(calibration_spotchecks)','o',  'MarkerFaceColor','g'); %cal PAT based tracker DBP
                %dashed line to separate files
                plot([spotchecks_subject-spotchecks_subject_last_file+.5  spotchecks_subject-spotchecks_subject_last_file+.5], [min(data.bp.dbpref) max(data.bp.dbpref)], 'k--');
                legend('DBP_{cuff}', 'DBP_{absolute}', 'DBP_{tracker, est, PWV}', 'DBP_{tracker, est, PAT}', 'DBP_{tracker, cal, PWV}', 'DBP_{tracker, cal, PAT}')
                xlabel('Spotcheck #')
                ylabel('DBP [mmHg]')
                
                %absolute errors boxplot
                subplot(4,4, [11]), hold on
                title('Absolute Tracking Errors DBP');
                boxplot([outcome.error_DBP_est_track_PWV_abs, outcome.error_DBP_est_track_PAT_abs, outcome.error_DBP_cal_track_PWV_abs, outcome.error_DBP_cal_track_PAT_abs],{'est, PWV','est, PAT','cal, PWV','cal, PAT'});
                scatter(1, outcome.error_DBP_est_track_PWV_mean_abs, 'r', 'fill');
                scatter(3, outcome.error_DBP_cal_track_PWV_mean_abs, 'r', 'fill');
                %MAD in plot
                text(1+0.2, outcome.error_DBP_est_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.error_DBP_est_track_PWV_mean_abs)])
                text(2+0.2, outcome.error_DBP_cal_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.error_DBP_cal_track_PWV_mean_abs)])
                % %of estimations below threshold
                %text(1+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
                %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
                hline(7)
                ylabel('Absolute Deviation [mmHg]')
                                
                %relative errors boxplot
                subplot(4,4, [15]), hold on
                title('Relative Tracking Errors DBP');
                boxplot([outcome.diff_error_DBP_est_track_PWV_abs, outcome.diff_error_DBP_est_track_PAT_abs, outcome.diff_error_DBP_cal_track_PWV_abs, outcome.diff_error_DBP_cal_track_PAT_abs],{'est, PWV','est, PAT','cal, PWV','cal, PAT'});
                scatter(1, outcome.diff_error_DBP_est_track_PWV_mean_abs, 'r', 'fill');
                scatter(3, outcome.diff_error_DBP_cal_track_PWV_mean_abs, 'r', 'fill');
                %MAD in plot
                text(1+0.2, outcome.diff_error_DBP_est_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.diff_error_DBP_est_track_PWV_mean_abs)])
                text(2+0.2, outcome.diff_error_DBP_cal_track_PWV_mean_abs, ['MAD:', char(10), num2str(outcome.diff_error_DBP_cal_track_PWV_mean_abs)])
                % %of estimations below threshold
                %text(1+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
                %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
                hline(7)
                ylabel('Absolute Deviation [mmHg]')
            
            %signals and CIs
            subplot(4,4, [4]), hold on, plot(outcome.PAT), plot(outcome.PPG_risetime_avg), plot(outcome.PPG_decaytime_avg); legend('PAT', 'Risetime', 'Decaytime'), xlabel('Spotcheck #'), ylabel('[s]');
            subplot(4,4, [8]), plot(outcome.HR), xlabel('Spotcheck #'), ylabel('HR [bpm]');
            subplot(4,4, [12]), hold on, plot(outcome.PAT_CI), plot(outcome.PPG_CI_raw), legend('PAT CI', 'PPG CI'), xlabel('Spotcheck #'), ylabel('CI'), axis([0 numel(outcome.PAT_CI) 1 5]);
            subplot(4,4, [16]), plot(outcome.PP_SP_int_avg), xlabel('Spotcheck #'), ylabel('PP-SP int [s]');
            
            %save
            savefig([rID_string '_BP_tracker'])
            
        end
        
        %output spreadsheet of valid subjects to console
        spreadsheet_valid_subjects

    else
        disp(['Bad subject: ' rID_string])
    end    
    
end

%save output spreadsheet of valid subject
%save([database_folder '\spreadsheet_valid_subjects.mat'], 'spreadsheet_valid_subjects');

    
%% Additional analysis absolute BP
    
close all

load('spreadsheet_valid_subjects')

%limit only to sitting 
sitting_booleans = (spreadsheet_valid_subjects(:,2) == 1) | (spreadsheet_valid_subjects(:,2) == 2) | (spreadsheet_valid_subjects(:,2) == 3);
spreadsheet_valid_subjects_sitting = spreadsheet_valid_subjects(sitting_booleans,:);


SBPmeas = spreadsheet_valid_subjects_sitting(:, 7);
DBPmeas = spreadsheet_valid_subjects_sitting(:, 8);

SBPest_simband = spreadsheet_valid_subjects_sitting(:, 10);
DBPest_simband = spreadsheet_valid_subjects_sitting(:, 11);

SBPest_algo = round(spreadsheet_valid_subjects_sitting(:, 12));
DBPest_algo = round(spreadsheet_valid_subjects_sitting(:, 13));

errors_SBP_simband = SBPest_simband - SBPmeas;
errors_SBP_algo = SBPest_algo - SBPmeas; % + overestimations / - underestimations

mean_error_SBP_simband = nanmean(errors_SBP_simband);
mean_error_SBP_algo = nanmean(errors_SBP_algo);


%Plot Histograms
xmin = 80;
xmax = 140;
ymin = 0;
ymax = 40;

figure, hold on, 
subplot(1,3,1), hist(SBPmeas), title('SBP meas'), axis([xmin, xmax, ymin, ymax])
subplot(1,3,2), hist(SBPest_simband), title('SBP est Simband'), axis([xmin, xmax, ymin, ymax])
subplot(1,3,3), hist(SBPest_algo), title('SBP est Algo'), axis([xmin, xmax, ymin, ymax])

%Error Plot
figure, hold on,
plot(SBPmeas, errors_SBP_simband, 'ro')
plot(SBPmeas, errors_SBP_algo, 'bo')
xlabel('BP Meas')
ylabel('Error (est-meas)')
legend('Error Simband UCSF4','Error Algo')

%Bland Altman Plots

% BA plot paramters for SBP
gnames = {};%{territories, states}; % names of groups in data {dimension 1 and 2}
label = {'SBP_{meas}','SBP_{est}','mmHg'}; % Names of data sets
Corr_info = {'n','SSE','r2','eq'}; % stats to display of Corr_elation scatter plot
BAinfo = {'SD'}; % stats to display on Bland-ALtman plot
limits = 'Tight'; % how to set the axes limits
symbols = 's'; % symbols for the data sets (default)
colors = '';% colors for different data sets
markercolor =  'r'; %color for single set marker
tit = '';

fig_BA_SBP_simband = figure('Name','Correlation and Bland-Altman Plots'); clf; hold on;

corr_title = 'Correlation Plot Simband UCSF4';
BA_title = 'Bland-Altman Plot Simband UCSF4';
[cr, fig, statsStruct] = BlandAltman(subplot(2,1,1),SBPmeas, SBPest_simband,label,tit,gnames,Corr_info,BAinfo,limits,colors,symbols,markercolor,corr_title,BA_title);

corr_title = 'Correlation Plot Algo 0.7.3';
BA_title = 'Bland-Altman Plot Algo 0.7.3';
[cr, fig, statsStruct] = BlandAltman(subplot(2,1,2),SBPmeas, SBPest_algo,label,tit,gnames,Corr_info,BAinfo,limits,colors,symbols,markercolor,corr_title,BA_title);

%% Additional analysis BP Tracker
    
close all

load('spreadsheet_valid_subjects')

%one line per subject for already averaged metrics (e.g. mean abs error of all/rest/rest_exerc)

%find unique rIDs and output corresponding idxs
[unique_rID, unique_rID_idx] = unique(spreadsheet_valid_subjects(:,1));
%use idxs to get one row per subjec
spreadsheet_valid_subjects_one_row = spreadsheet_valid_subjects(unique_rID_idx,:);

%list mean absolute errors (per subject for all/rest/rest_exerc tracking spotchecks)
%SBP
mean_errors_all_SBP = spreadsheet_valid_subjects_one_row(:,46);
mean_errors_rest_SBP = spreadsheet_valid_subjects_one_row(:,47);
mean_errors_rest_exerc_SBP = spreadsheet_valid_subjects_one_row(:,48);
%DBP
mean_errors_all_DBP = spreadsheet_valid_subjects_one_row(:,51);
mean_errors_rest_DBP = spreadsheet_valid_subjects_one_row(:,52);
mean_errors_rest_exerc_DBP = spreadsheet_valid_subjects_one_row(:,53);
%compute overall mean errors
%SBP
overall_mean_error_all_SBP = mean(mean_errors_all_SBP)
overall_mean_error_rest_SBP = mean(mean_errors_rest_SBP)
overall_mean_error_rest_exerc_SBP = mean(mean_errors_rest_exerc_SBP)
%DBP
overall_mean_error_all_DBP = mean(mean_errors_all_DBP)
overall_mean_error_rest_DBP = mean(mean_errors_rest_DBP)
overall_mean_error_rest_exerc_DBP = mean(mean_errors_rest_exerc_DBP)


%compute average PAT_CI and PPG_CIper subject
avg_PAT_CI = [];
avg_PPG_CI = [];
for i=1:numel(unique_rID)
    current_spreadsheet_valid_subject = spreadsheet_valid_subjects(spreadsheet_valid_subjects(:,1) == unique_rID(i), :);  
    avg_PAT_CI = [avg_PAT_CI; mean(current_spreadsheet_valid_subject(:,24))];
    avg_PPG_CI = [avg_PPG_CI; mean(current_spreadsheet_valid_subject(:,25))];  
end
%also consider risetime (only available as avg for all spotchecks)
sd_risetime_all = spreadsheet_valid_subjects_one_row(:,29);

%visually correlate mean errors with CI (all/rest/res_exerc)

figure, hold on;
%SBP
subplot(2,3,1), hold on;
scatter(avg_PAT_CI, mean_errors_all_SBP, 'b')
scatter(avg_PPG_CI, mean_errors_all_SBP, 'r')
title('all')
subplot(2,3,2), hold on;
scatter(avg_PAT_CI, mean_errors_rest_SBP, 'b')
scatter(avg_PPG_CI, mean_errors_rest_SBP, 'r')
title('rest')
% subplot(2,3,3), hold on;
% scatter(avg_PAT_CI, mean_errors_rest_exerc_SBP, 'b')
% scatter(avg_PPG_CI, mean_errors_rest_exerc_SBP, 'r')
% title('rest & exercise')
subplot(2,3,3), hold on;
scatter(sd_risetime_all, mean_errors_all_SBP, 'g')
title('all')

%DBP
subplot(2,3,4), hold on;
scatter(avg_PAT_CI, mean_errors_all_DBP, 'b')
scatter(avg_PPG_CI, mean_errors_all_DBP, 'r')
title('all')
subplot(2,3,5), hold on;
scatter(avg_PAT_CI, mean_errors_rest_DBP, 'b')
scatter(avg_PPG_CI, mean_errors_rest_DBP, 'r')
title('rest')
% subplot(2,4,7), hold on;
% scatter(avg_PAT_CI, mean_errors_rest_exerc_DBP, 'b')
% scatter(avg_PPG_CI, mean_errors_rest_exerc_DBP, 'r')
% title('rest & exercise')
subplot(2,3,6), hold on;
scatter(sd_risetime_all, mean_errors_all_DBP, 'g')
title('all')

%list mean absolute errors (per subject for all/rest/rest_exerc tracking spotchecks), with avg CI's > 3 
criterion_fullfilled_idxs = (avg_PAT_CI>=3 & avg_PPG_CI>=3);
%SBP
mean_errors_all_SBP_CI_fulfilled = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs,46);
mean_errors_rest_SBP_CI_fulfilled = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs,47);
mean_errors_rest_exerc_SBP_CI_fulfilled = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs,48);
%DBP
mean_errors_all_DBP_CI_fulfilled = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs,51);
mean_errors_rest_DBP_CI_fulfilled = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs,52);
mean_errors_rest_exerc_DBP_CI_fulfilled = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs,53);
%compute overall mean errors, with avg CI's > 3 
%SBP
overall_mean_error_all_SBP_CI_fulfilled = mean(mean_errors_all_SBP_CI_fulfilled)
overall_mean_error_rest_SBP_CI_fulfilled = mean(mean_errors_rest_SBP_CI_fulfilled)
overall_mean_error_rest_exerc_SBP_CI_fulfilled = mean(mean_errors_rest_exerc_SBP_CI_fulfilled)
%DBP
overall_mean_error_all_DBP_CI_fulfilled = mean(mean_errors_all_DBP_CI_fulfilled)
overall_mean_error_rest_DBP_CI_fulfilled = mean(mean_errors_rest_DBP_CI_fulfilled)
overall_mean_error_rest_exerc_DBP_CI_fulfilled = mean(mean_errors_rest_exerc_DBP_CI_fulfilled)

%which subjects fulfilled CI but did not achieve error lower than 7?
%SBP
outliers_all_SBP = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs & spreadsheet_valid_subjects_one_row(:,46) > 7, 1)
outliers_rest_SBP = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs & spreadsheet_valid_subjects_one_row(:,47) > 7, 1)
outliers_rest_exerc_SBP = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs & spreadsheet_valid_subjects_one_row(:,48) > 7, 1)
%DBP
outliers_all_DBP = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs & spreadsheet_valid_subjects_one_row(:,51) > 7, 1)
outliers_rest_DBP = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs & spreadsheet_valid_subjects_one_row(:,52) > 7, 1)
outliers_rest_exerc_DBP = spreadsheet_valid_subjects_one_row(criterion_fullfilled_idxs & spreadsheet_valid_subjects_one_row(:,53) > 7, 1)


N_all = 92;% N_subjects; %all subjects in UCSF4 = 92;

N_data_metadata_match = size(spreadsheet_valid_subjects_one_row,1); %+6 where algo chain crashed for different reasons

N_criteria_fulfilled = sum(criterion_fullfilled_idxs);

% Boxplots mean errors subjects & overall mean errors
%row1: no CI criteria applied
%row2: CI criteriat applied

figure, hold on;
%No criteria
    %all
    subplot(2,2,1), hold on;
    boxplot([mean_errors_all_SBP, mean_errors_all_DBP],{'SBP', 'DBP'});
    scatter(1, overall_mean_error_all_SBP, 'r', 'fill');
    scatter(2, overall_mean_error_all_DBP, 'r', 'fill');  
    %N in plot
    text(1.5, max(mean_errors_all_SBP)-3, ['N=', num2str(numel(mean_errors_all_SBP))])
    %MAD in plot
    text(1+0.2, overall_mean_error_all_SBP, ['MAD:', char(10), num2str(overall_mean_error_all_SBP)])
    text(2+0.2, overall_mean_error_all_DBP, ['MAD:', char(10), num2str(overall_mean_error_all_DBP)])
    %of estimations below threshold
    perc_in_thresh_mean_errors_all_SBP = sum(mean_errors_all_SBP <= 7)./numel(mean_errors_all_SBP);
    perc_in_thresh_mean_errors_all_DBP = sum(mean_errors_all_DBP <= 7)./numel(mean_errors_all_DBP);
    text(1+0.2, overall_mean_error_all_SBP-4, ['(', num2str(perc_in_thresh_mean_errors_all_SBP), '%)'])
    text(2+0.2, overall_mean_error_all_DBP-4, ['(', num2str(perc_in_thresh_mean_errors_all_DBP), '%)'])
    %threshold
    hline(7);
    ylabel('Mean Absolute Deviation [mmHg]')
    title('All interventions, No criteria applied')
    %rest_exerc
%     subplot(2,2,2), hold on;
%     boxplot([mean_errors_rest_exerc_SBP, mean_errors_rest_exerc_DBP],{'SBP', 'DBP'});
%     scatter(1, overall_mean_error_rest_exerc_SBP, 'r', 'fill');
%     scatter(2, overall_mean_error_rest_exerc_DBP, 'r', 'fill');
%     hline(7);
%     title('rest&exerc, no criteria')
    %rest
    subplot(2,2,2), hold on;
    boxplot([mean_errors_rest_SBP, mean_errors_rest_DBP],{'SBP', 'DBP'});
    scatter(1, overall_mean_error_rest_SBP, 'r', 'fill');
    scatter(2, overall_mean_error_rest_DBP, 'r', 'fill');
    %N in plot
    text(1.5, max(mean_errors_rest_SBP)-3, ['N=', num2str(numel(mean_errors_rest_SBP))])    
    %MAD in plot
    text(1+0.2, overall_mean_error_rest_SBP, ['MAD:', char(10), num2str(overall_mean_error_rest_SBP)])
    text(2+0.2, overall_mean_error_rest_DBP, ['MAD:', char(10), num2str(overall_mean_error_rest_DBP)]) 
    %of estimations below threshold
    perc_in_thresh_mean_errors_rest_SBP = sum(mean_errors_rest_SBP <= 7)./numel(mean_errors_rest_SBP);
    perc_in_thresh_mean_errors_rest_DBP = sum(mean_errors_rest_DBP <= 7)./numel(mean_errors_rest_DBP);
    text(1+0.2, overall_mean_error_rest_SBP-5, ['(', num2str(perc_in_thresh_mean_errors_rest_SBP), '%)'])
    text(2+0.2, overall_mean_error_rest_DBP-5, ['(', num2str(perc_in_thresh_mean_errors_rest_DBP), '%)'])
    %threshold    
    hline(7);
    ylabel('Mean Absolute Deviation [mmHg]')
    title('Rest only, No criteria applied')
%CI criteria
    %all
    subplot(2,2,3), hold on;
    boxplot([mean_errors_all_SBP_CI_fulfilled, mean_errors_all_DBP_CI_fulfilled],{'SBP', 'DBP'});
    scatter(1, overall_mean_error_all_SBP_CI_fulfilled, 'r', 'fill');
    scatter(2, overall_mean_error_all_DBP_CI_fulfilled, 'r', 'fill');
    %N in plot
    text(1.5, max(mean_errors_all_SBP_CI_fulfilled)-1, ['N=', num2str(numel(mean_errors_all_SBP_CI_fulfilled))])     
    %MAD in plot
    text(1+0.2, overall_mean_error_all_SBP_CI_fulfilled, ['MAD:', char(10), num2str(overall_mean_error_all_SBP_CI_fulfilled)])
    text(2+0.2, overall_mean_error_all_DBP_CI_fulfilled, ['MAD:', char(10), num2str(overall_mean_error_all_DBP_CI_fulfilled)])     
    %of estimations below threshold
    perc_in_thresh_mean_errors_all_SBP_CI_fulfilled = sum(mean_errors_all_SBP_CI_fulfilled <= 7)./numel(mean_errors_all_SBP_CI_fulfilled);
    perc_in_thresh_mean_errors_all_DBP_CI_fulfilled = sum(mean_errors_all_DBP_CI_fulfilled <= 7)./numel(mean_errors_all_DBP_CI_fulfilled);
    text(1+0.2, overall_mean_error_all_SBP-3, ['(', num2str(perc_in_thresh_mean_errors_all_SBP_CI_fulfilled), '%)'])
    text(2+0.2, overall_mean_error_all_DBP-2, ['(', num2str(perc_in_thresh_mean_errors_all_DBP_CI_fulfilled), '%)'])
    %threshold   
    hline(7);
    ylabel('Mean Absolute Deviation [mmHg]')
    title('All interventions, CI criteria applied')
    %rest_exerc
%     subplot(2,2,5), hold on;
%     boxplot([mean_errors_rest_exerc_SBP_CI_fulfilled, mean_errors_rest_exerc_DBP_CI_fulfilled],{'SBP', 'DBP'});
%     scatter(1, overall_mean_error_rest_exerc_SBP_CI_fulfilled, 'r', 'fill');
%     scatter(2, overall_mean_error_rest_exerc_DBP_CI_fulfilled, 'r', 'fill');
%     hline(7);
%     title('rest&exerc, CI criteria')
    %rest
    subplot(2,2,4), hold on;
    boxplot([mean_errors_rest_SBP_CI_fulfilled, mean_errors_rest_DBP_CI_fulfilled],{'SBP', 'DBP'});
    scatter(1, overall_mean_error_rest_SBP_CI_fulfilled, 'r', 'fill');
    scatter(2, overall_mean_error_rest_DBP_CI_fulfilled, 'r', 'fill');
    %N in plot
    text(1.5, max(mean_errors_rest_SBP_CI_fulfilled)-1, ['N=', num2str(numel(mean_errors_rest_SBP_CI_fulfilled))])     
    %MAD in plot
    text(1+0.2, overall_mean_error_rest_SBP_CI_fulfilled, ['MAD:', char(10), num2str(overall_mean_error_rest_SBP_CI_fulfilled)])
    text(2+0.2, overall_mean_error_rest_DBP_CI_fulfilled, ['MAD:', char(10), num2str(overall_mean_error_rest_DBP_CI_fulfilled)])    
    %of estimations below threshold
    perc_in_thresh_mean_errors_rest_SBP_CI_fulfilled = sum(mean_errors_rest_SBP_CI_fulfilled <= 7)./numel(mean_errors_rest_SBP_CI_fulfilled);
    perc_in_thresh_mean_errors_rest_DBP_CI_fulfilled = sum(mean_errors_rest_DBP_CI_fulfilled <= 7)./numel(mean_errors_rest_DBP_CI_fulfilled);
    text(1+0.2, overall_mean_error_rest_SBP-4, ['(', num2str(perc_in_thresh_mean_errors_rest_SBP_CI_fulfilled), '%)'])
    text(2+0.2, overall_mean_error_rest_DBP-3, ['(', num2str(perc_in_thresh_mean_errors_rest_DBP_CI_fulfilled), '%)'])
    %threshold       
    hline(7);
    ylabel('Mean Absolute Deviation [mmHg]')
    title('Rest only, CI criteria applied')    


