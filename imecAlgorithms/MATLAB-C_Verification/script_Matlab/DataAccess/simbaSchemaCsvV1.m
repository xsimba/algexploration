function schema = simbaSchemaCsvV1

schema = {...
'HRV:0',  'HRV', 'Rate';
'HR:0',  'HR', 'Rate';
'PAT:0',  'PAT', 'Feature';
'ECGHeartBeat:0',  'ECGHeartBeat', 'Feature';
'HeartBeat:0',  'HeartBeat', 'Feature';
'ECGRaw:0',  'ECGRaw', 'Signal';
'ppg/blue:0',  'ppg_blue', 'SrcSignal';
'ppg/red:0',  'ppg_red', 'SrcSignal';
'ppg/green/center:0',  'ppg_green_center', 'SrcSignal';
'ppg/green/side:0',  'ppg_green_side', 'SrcSignal';
'ECG:0',  'ECG', 'SrcSignal';
'PPG_2:0',  'PPG_20', 'SrcSignal';
'PPG_2:1',  'PPG_21', 'SrcSignal';
'PPG_1:0',  'PPG_10', 'SrcSignal';
'PPG_1:1',  'PPG_11', 'SrcSignal';
'Accelerometer:0',  'Accelerometer1', 'SrcSignal';
'Accelerometer:1',  'Accelerometer2', 'SrcSignal';
'Accelerometer:2',  'Accelerometer3', 'SrcSignal';
'PPG_3_VISUAL:0',  'PPG_3_VISUAL', 'SrcSignal';
'PPG_1_VISUAL:0',  'PPG_1_VISUAL', 'SrcSignal';
'PPG_2_VISUAL:0',  'PPG_2_VISUAL', 'SrcSignal';
'PPG_0_VISUAL:0',  'PPG_0_VISUAL', 'SrcSignal';
'ECG_VISUAL:0',  'ECG_VISUAL', 'SrcSignal';
%
%
%
%
%
%
%
%
'ppg/green/side:visual:0',  'ppg_green_side_visual', 'SrcSignal';
'ppg/green/center:visual:0',  'ppg_green_center_visual', 'SrcSignal';
'ppg/blue:visual:0',  'ppg_blue_visual', 'SrcSignal';
'ppg/red:visual:0',  'ppg_red_visual', 'SrcSignal';
%
%
'BioZ:0',  'BioZ', 'SrcSignal';
'BioZRaw:0',  'BioZRaw', 'Signal';
'ppg/ir1:visual:0',  'ppg_ir1_visual', 'SrcSignal';
%
%
'ppg/ir2:visual:0',  'ppg_ir2', 'SrcSignal';
'PPG_4_VISUAL:0',  'PPG_4_VISUAL', 'SrcSignal';
'PPG_5_VISUAL:0',  'PPG_5_VISUAL', 'SrcSignal';
'PPG_3:0',  'PPG_30', 'SrcSignal';
'PPG_3:1',  'PPG_31', 'SrcSignal';
'ECGLeadOn:0',  'ecg_lead_on', 'Discrete';
    };
end