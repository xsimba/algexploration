function c = loadSessionData(fname)
%
% loads a SAMI session data file with credentials and timestamps.
%

[junk,junk,credentials] = xlsread(fname, 'Devices');
[junk,junk,sessions] = xlsread(fname, 'Sessions');

c = {};

for i = 2:size(sessions,1);
	s = sessions(i,:);	
	tempc.sessionID = s{1};
	tempc.time.startTime = sprintf('%.0f',s{3});
	tempc.time.endTime   = sprintf('%.0f',s{4});
	deviceNick = s{2};
    deviceInd  = strmatch(deviceNick, credentials(:,1));
    if deviceInd == [],
    	error(['Sessions database, ', fname,' is corrupted']);
    end
    tempc.cred.did = credentials{deviceInd,2};
    tempc.cred.uid = credentials{deviceInd,3};
    tempc.cred.tok = credentials{deviceInd,4};
    c{i-1} = tempc;
end
