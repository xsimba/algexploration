function [data] =csv2mat2_23092014(foldername, file)
%% Import data from text file.
%% Initialize variables.
% filename = '\\winnl\brown\Public\bp protocol\Sub2\sub2\stand2.csv';

filename = [foldername file '.csv'];

delimiter = ',';

%% Read columns of data as strings:
% For more information, see the TEXTSCAN documentation.
formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);


%% Close the text file.
fclose(fileID);

%% Convert the contents of columns containing numeric strings to numbers.
% Replace non-numeric strings with NaN.
raw = repmat({''},length(dataArray{1}),length(dataArray)-1);
for col=1:length(dataArray)-1
    raw(1:length(dataArray{col}),col) = dataArray{col};
end
numericData = NaN(size(dataArray{1},1),size(dataArray,2));

for col=[1,2,3,4,5,6,7,8,9,10,11,12,13]
    % Converts strings in the input cell array to numbers. Replaced non-numeric
    % strings with NaN.
    rawData = dataArray{col};
    for row=1:size(rawData, 1);
        % Create a regular expression to detect and remove non-numeric prefixes and
        % suffixes.
        regexstr = '(?<prefix>.*?)(?<numbers>([-]*(\d+[\,]*)+[\.]{0,1}\d*[eEdD]{0,1}[-+]*\d*[i]{0,1})|([-]*(\d+[\,]*)*[\.]{1,1}\d+[eEdD]{0,1}[-+]*\d*[i]{0,1}))(?<suffix>.*)';
        try
            result = regexp(rawData{row}, regexstr, 'names');
            numbers = result.numbers;
            
            % Detected commas in non-thousand locations.
            invalidThousandsSeparator = false;
            if any(numbers==',');
                thousandsRegExp = '^\d+?(\,\d{3})*\.{0,1}\d*$';
                if isempty(regexp(thousandsRegExp, ',', 'once'));
                    numbers = NaN;
                    invalidThousandsSeparator = true;
                end
            end
            % Convert numeric strings to numbers.
            if ~invalidThousandsSeparator;
                numbers = textscan(strrep(numbers, ',', ''), '%f');
                numericData(row, col) = numbers{1};
                raw{row, col} = numbers{1};
            end
        catch me
        end
    end
end


%% Replace non-numeric cells with NaN
R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),raw); % Find non-numeric cells
raw(R) = {NaN}; % Replace non-numeric cells

%% Create output variable
datatemp = cell2mat(raw);
datatemp(1,:)=[];
tempecg=find(isnan(datatemp(:,5)));
datatemp(tempecg,:)=[];
tempppg0=find(isnan(datatemp(:,6)));
datatemp(tempppg0,:)=[];
tempppg1=find(isnan(datatemp(:,8)));
datatemp(tempppg1,:)=[];
tempppg2=find(isnan(datatemp(:,10)));
datatemp(tempppg2,:)=[];
tempppg3=find(isnan(datatemp(:,12)));
datatemp(tempppg3,:)=[];

data.timestamp(1,:)    = datatemp(:,1);
data.acc.x.signal(1,:) = datatemp(:,2);
data.acc.y.signal(1,:) = datatemp(:,3);
data.acc.z.signal(1,:) = datatemp(:,4);
data.ecg.signal(1,:)   = datatemp(:,5);
data.ppg.a.signal(1,:) = datatemp(:,6);
data.ppg.b.signal(1,:) = datatemp(:,7);
data.ppg.c.signal(1,:) = datatemp(:,8);
data.ppg.d.signal(1,:) = datatemp(:,9);
data.ppg.e.signal(1,:) = datatemp(:,10);
data.ppg.f.signal(1,:) = datatemp(:,11);
data.ppg.g.signal(1,:) = datatemp(:,12);
data.ppg.h.signal(1,:) = datatemp(:,13);

% data.timestamp(1,:)    = datatemp(:,1);
% data.acc.x.signal(1,:) = datatemp(:,2);
% data.acc.y.signal(1,:) = datatemp(:,3);
% data.acc.z.signal(1,:) = datatemp(:,4);
% % data.ecg.signal(1,:)   = datatemp(:,5);
% data.ppg.a.signal(1,:) = datatemp(:,5);
% data.ppg.b.signal(1,:) = datatemp(:,6);
% data.ppg.c.signal(1,:) = datatemp(:,7);
% data.ppg.d.signal(1,:) = datatemp(:,8);
% data.ppg.e.signal(1,:) = datatemp(:,9);
% data.ppg.f.signal(1,:) = datatemp(:,10);
% data.ppg.g.signal(1,:) = datatemp(:,11);
% data.ppg.h.signal(1,:) = datatemp(:,12);


data.fs=128;
data.acc.All = [data.acc.x.signal;data.acc.y.signal;data.acc.z.signal];
save([foldername file],'data')
%% Clear temporary variables
clearvars filename delimiter formatSpec fileID datatemp dataArray ans raw col numericData rawData row regexstr result numbers invalidThousandsSeparator thousandsRegExp me R;