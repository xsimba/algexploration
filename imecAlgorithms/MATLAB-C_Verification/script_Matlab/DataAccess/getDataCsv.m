function data = getDataCsv(filename, startTimeStamp)
%
% getDataCsv reads in Csv data into the Research Console format, as 
% specified in the Rosetta.xlsx spreadsheet.  
%
% A. Khalak, 8/13/2014
%
schema = simbaSchemaCsvV1;

if (~exist('startTimeStamp', 'var')),
    startTimeStamp = 0;
end

%
% assert that file exists
%
fid = fopen(filename, 'r');
assert(fid > 0, 'Cannot read file %s ', filename)

%
% Steps
% 1) Look at the header, determine # fields, indentify in schema, etc.
% 2) Slurp in the data 
% 3) Determine the timeGrid for the SrcSignal fields 
% 4) Assign the values of the SrcSignal fields as 1xN vectors, and 
%    2xM_i for the rest, where N is the timeGrid length, and 2xM_i 
%    corresponds to the timestamp and 
%

header_line = fgetl(fid);

% Count number of columns
ncols = sum(header_line == ',') + 1;
column_names = textscan( header_line, '%s', 'delimiter', ',' );
column_names = column_names{1};
assert(numel(column_names) == ncols);

% figure out column matlab name and type from schema
formatStr = '%f';
fieldMatlab{1} = 'timeGrid';
fieldType{1} = 'time';
for i = 2:ncols,
    nameIdx = strcmp(column_names{i}, schema(:,1));
    assert(sum(nameIdx)==1, 'CSV header not supported by schema version');
    fieldIdx = find(nameIdx);
    fieldMatlab{i} = schema{fieldIdx, 2};
    fieldType{i}   = schema{fieldIdx, 3};
    formatStr = [formatStr,'%f'];
end
formatStr = [formatStr,'%[^\n\r]'];

% slurp in the data
frewind(fid);
dataArray = textscan(fid, formatStr, 'Delimiter', ',', ...
    'EmptyValue' ,NaN,'HeaderLines' ,1, 'ReturnOnError', false);
fclose(fid);

% determine the timeGrid for the SrcSignal fields
timeAlignedCheck = strcmp('SrcSignal', fieldType);
timeAlignedCols = find(timeAlignedCheck);
timeAlignedCheck(1) = 1; % assign time as time-aligned for next step
nonTimeAlignedCols = find(~timeAlignedCheck);

% go through all of the timeAlignedColumns and only take data when
% all fields are present
idx = timeAlignedCols(1);
timeGridIndx = ~isnan(dataArray{idx});
for j = 2:length(timeAlignedCols),
    jdx = timeAlignedCols(j);
    timeGridIndx = timeGridIndx & ~isnan(dataArray{jdx});
end
data.timeGrid = dataArray{1}(timeGridIndx)';

% place the time aligned columns into the data output
for i = 1:length(timeAlignedCols),
    idx = timeAlignedCols(i);
    data.(fieldMatlab{idx}) = dataArray{idx}(timeGridIndx)';
end

for i = 1:length(nonTimeAlignedCols),
    idx = nonTimeAlignedCols(i);
    timeGridIndx = ~isnan(dataArray{:,idx});
    data.(fieldMatlab{idx}) = [dataArray{1}(timeGridIndx)'; ...
                               dataArray{idx}(timeGridIndx)'];
end

end

