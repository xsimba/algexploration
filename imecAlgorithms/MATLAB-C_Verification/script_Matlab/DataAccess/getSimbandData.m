function data = getSimbandData(uid, did, dtoken, sdate, edate)

%
%
%getSimbandData(uid, did, dtoken, sdate, edate)
%
%Input: user id, device id, device token, start date and end date
%Retrieves data from SAMI and saves data in .mat file

    s1 = 'python ..\DataAccessLayer\getSamiData.py --uid ';
    s2 = ' --did ';
    s3 = ' --dtoken ';
    s4 = ' --sdate ';
    s5 = ' --edate ';
    string = [s1 uid s2 did s3 dtoken s4 sdate s5 edate];
    system (string);
    data = load('MatlabTransfer.mat');
end

%todo: come up with a caching mechanism to automatically know if we are
%looking for data that is already locally available.