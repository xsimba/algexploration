function [ input_dir,output_dir,alg_dir ] = set_path( )

    addpath(fullfile('.','DataAccess'));
    addpath(fullfile('.','DataMining'));
    addpath(fullfile('.','ValidationFramework'));
    % addpath(fullfile('.','Metrics'));
    addpath(fullfile('.','ViewReporting'));

    % modify path of input files
    input_dir = fullfile('..','input');
    addpath(input_dir);
    % modify here path for output files
    output_dir = fullfile('..','output_Matlab');
    addpath(output_dir);
    % modify here path for algorithm directory
    alg_dir = fullfile('..','..');
    addpath(alg_dir);

end

