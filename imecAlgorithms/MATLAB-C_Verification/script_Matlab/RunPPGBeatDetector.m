% read csv
clear 

[ input_dir,output_dir,alg_dir ] = set_path( );
data = loadData(input_dir);

addpath(genpath(fullfile(alg_dir,'BeatDetection')));
data = doTest('BD',data,'ppg.a'); 
 
csvwrite(fullfile(output_dir,'00-result-PPG_Beats.csv'),data.ppg.a.beats');
disp('Done...');
