%
% Research Console Test Script
%
% A. Khalak, 8/14/2014

setRCpath;

%   
% load session database
%
c = loadSessionData('./Databases/MySessions.xlsx');
[sc, time] = setSessionData(c, 'test003');

% get data from SAMI
data = getDataSAMI(sc,time);
% get raw data from SAMI
data_raw = getDataSamiRaw(sc,time);

% test one algorithm on ECG
output_ecg_basic = doTest('BDBasic',data,'ECG');


% test one algorithm on ECG
output_ppg = doTest('BD_PPG_v2',data,'ppg_blue');

% test one algorithm on raw data (there are problem for the multiple rows
% in the matrix ). This does not work for the moment
% output_ecg_raw = doTest('BDBasic','ECG','raw');

% test multiple algorithms on ECG
la = {'BDBasic','BD_ECG_v2','BD_ECG_v3'} ;
ls = {'ECG','ECG','ECG'} ; 
lo_ecg = performAllTests(la,ls,data);

% test same algorithm on different PPG channels
la = {'BD_PPG_v2','BD_PPG_v2','BD_PPG_v2','BD_PPG_v2'} ;
ls = {'PPG_10','PPG_11','PPG_20','PPG_21'} ; 
lo_ppg = performAllTests(la,ls);

% compare results of the ecg beat detection with the band detected beats
m_ecg = compareBandFeatures(output_ecg_basic,data,'ECGHeartBeat');

% compare results of the ppg beat detection with the band detected beats
m_ppg = compareBandFeatures(output_ppg,data,'HeartBeat');

% compute statistics on the algorithm output
params.nbins = 20;
params.stats = 'histogram';
algStats = computeAlgStats(output_ppg,params);

% computes statistics on the Accelerometer and BioZ signals
accStats = computeSignalStats(data,'Accelerometer1',params);
biozStats = computeSignalStats(data,'BioZ',params);

% plots metrics 
output_directory = './plots';
metrics_plotnames = plotMetrics(m_ecg,output_directory);
% plots signal stats
ls = {'ECG','PPG_10','PPG_11','PPG_20','PPG_21','Accelerometer1','BioZ'} ; 
sign_hist_plotnames = plotChannelsHistograms(data,ls,output_directory);

% plots signal stats on raw data
ls = {'ECG','PPG_10','PPG_11','PPG_20','PPG_21','Accelerometer1','BioZ'} ; 
raw_sign_hist_plotnames = plotChannelsHistograms(data_raw,ls,output_directory);

% write report ppt
report_filename = write_report_ppt('./Report',metrics_plotnames,raw_sign_hist_plotnames,sign_hist_plotnames);