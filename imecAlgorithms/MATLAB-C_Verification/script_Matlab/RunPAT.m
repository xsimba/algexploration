% read csv
clear 

[ input_dir,output_dir,alg_dir ] = set_path( );
data = loadData(input_dir);
addpath(genpath(fullfile(alg_dir,'BeatDetection')));
addpath(genpath(fullfile(alg_dir,'ConfidenceIndicator','CI','CI_I')));
addpath(genpath(fullfile(alg_dir,'ConfidenceIndicator','CI','CI_II')));
addpath(genpath(fullfile(alg_dir,'PulseArrivalTime','PAT')));

data = doTest('BD',data,'ecg'); 

ppg_channels = {'a','b','c','d','e','f','g','h'};
for cIdx = 1:numel(ppg_channels) 

    data = doTest('BD',data,['ppg.',ppg_channels{cIdx}]); % Matlab

end

% test CI Raw ECG
data = doTest('CI_raw',data, 'ecg'); % Matlab
 
% test CI Raw PPG
for cIdx = 1:numel(ppg_channels) 

    data = doTest('CI_raw',data,['ppg.',ppg_channels{cIdx}]); 

end

data = doTest('CI_II',data, 'ecg'); % Matlab

% test CI Beats PPG
for cIdx = 1:numel(ppg_channels) 

    data = doTest('CI_II',data,['ppg.',ppg_channels{cIdx}]);

end
    
data = doTest('PAT',data,'ppg.a');
data = doTest('PAT',data,'ppg.b');
data = doTest('PAT',data,'ppg.c');
data = doTest('PAT',data,'ppg.d');
data = doTest('PAT',data,'ppg.e');
data = doTest('PAT',data,'ppg.f');
data = doTest('PAT',data,'ppg.g');
data = doTest('PAT',data,'ppg.h');

data = Check4Channel(data);

pat= [data.pat.pat1' data.pat.patSD1' data.pat.HR1' data.pat.HRSD1']';

csvwrite(fullfile(output_dir,'00-result-PAT.csv'),pat(:));

disp('Done...');