function browsePpgDataBySection(data)

ss_ppg_green_center = findSections(data.ppg_green_center(2,:), data.ppg_green_center(1,:));
ss_ppg_green_side = findSections(data.ppg_green_side(2,:), data.ppg_green_side(1,:));
ss_ppg_blue = findSections(data.ppg_blue(2,:), data.ppg_blue(1,:));
ss_ppg_red = findSections(data.ppg_red(2,:), data.ppg_red(1,:));

%Plot by sections

for sectNum = 1:length(ss_ppg_green_center),
    size_secs = (length(ss_ppg_green_center(sectNum).values)-1)/128;
    t1 = 0:(1/128):size_secs;
    
    size_secs = (length(ss_ppg_green_side(sectNum).values)-1)/128;
    t2 = 0:(1/128):size_secs;
    
    size_secs = (length(ss_ppg_blue(sectNum).values)-1)/128;
    t3 = 0:(1/128):size_secs;
    
    size_secs = (length(ss_ppg_red(sectNum).values)-1)/128;
    t4 = 0:(1/128):size_secs;
    figure (1)
    clf
    ax1 = subplot(2,2,1);
    plot(t1,ss_ppg_green_center(sectNum).values,'g')
    title(['PPG Green Center, Section', num2str(sectNum)])
    
    ax2 = subplot(2,2,2);
    plot(t2,ss_ppg_green_side(sectNum).values,'g')
    title(['PPG Green side, Section ', num2str(sectNum)])

    ax3 = subplot(2,2,3);
    plot(t3,ss_ppg_blue(sectNum).values,'b')
    title(['PPG Blue, Section ', num2str(sectNum)])

    ax4 = subplot(2,2,4);
    plot(t4,ss_ppg_red(sectNum).values,'r')
    title(['PPG Red, Section ', num2str(sectNum)])

    linkaxes([ax1 ax2 ax3 ax4], 'x');
    
    pause;
end

    

