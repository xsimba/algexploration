function  [clean_signal] = identifySignal(section, psdx_ref)
% section on which signals are to be identified
% ref_ps - power spectrum density of reference signal
% mseThres - threshold for mean-squared error
    warning off
    offset = 10;
    i = 10;
    clean_signal = [];
    data = section;
    count = 1;
    mse_thres = 20000; 
    ecgFreq = 128;
    while i < length(data)
        end_index = 2*length(psdx_ref)-1 + (i-1);
        if(end_index > length(data))            
            i = length(data);
            continue;
        end
        
        sample = data(i:end_index);
        sample_length = length(sample);
        NFFT = 2^nextpow2(sample_length);
        Y_sample = fft(sample,NFFT)/sample_length;
        y_spectrum = Y_sample(1:sample_length/2+1);
        psdx_sample = (1/(ecgFreq*sample_length)).*abs(y_spectrum).^2;
        psdx_sample(2:end-1) = 2*psdx_sample(2:end-1);
        if (length(psdx_ref) == length(psdx_sample))
            error = psdx_ref - psdx_sample;
            mse(count) = sum(error.^2);
            if sum(error.^2) <= mse_thres
               clean_signal = [clean_signal i];
            end
        end
        
        count = count + 1;
        i = i + offset;
    end
end
