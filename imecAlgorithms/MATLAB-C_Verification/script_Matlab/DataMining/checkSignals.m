function okCode = checkSignals( data )

    % check raw data
    figure('units','normalized','outerposition',[0 0 1 1])
    ax1 = subplot(3,4,1);
    plot(data.ecg.signal);ylabel('ECG');
    
    ax2 = subplot(3,4,2);
    plot(data.acc.x.signal);ylabel('ACC.X');
    
    ax3 = subplot(3,4,3);
    plot(data.acc.y.signal);ylabel('ACC.Y');
    
    ax4 = subplot(3,4,4);
    plot(data.acc.z.signal);ylabel('ACC.Z');
    
    ax5 = subplot(3,4,5);
    plot(data.ppg.a.signal);ylabel('PPG.A');
    
    ax6 = subplot(3,4,6);
    plot(data.ppg.b.signal);ylabel('PPG.B');
    
    ax7 = subplot(3,4,7);
    plot(data.ppg.c.signal);ylabel('PPG.C');
    
    ax8 = subplot(3,4,8);
    plot(data.ppg.d.signal);ylabel('PPG.D');
    
    ax9 = subplot(3,4,9);
    plot(data.ppg.e.signal);ylabel('PPG.E');
    
    ax10 = subplot(3,4,10);
    plot(data.ppg.f.signal);ylabel('PPG.F');
    
    ax11 = subplot(3,4,11);
    plot(data.ppg.g.signal);ylabel('PPG.G');
    
    ax12 = subplot(3,4,12);
    plot(data.ppg.f.signal);ylabel('PPG.H');
    
    linkaxes([ax1,ax2,ax3,ax4,ax5,ax6,ax7,ax8,ax9,ax10,ax11,ax11,ax12],'x');
    
    okCode = 1;
end

