function browsePpgData(data)

%
% assign the streams
%
t1   = data.PPG_10(1,:);
ppg1 = data.PPG_10(2,:);
t2   = data.PPG_11(1,:);
ppg2 = data.PPG_11(2,:);
t3   = data.PPG_20(1,:);
ppg3 = data.PPG_20(2,:);
t4   = data.PPG_21(1,:);
ppg4 = data.PPG_21(2,:);

tacc = data.Accelerometer0(1,:);
accx = data.Accelerometer0(2,:);
accy = data.Accelerometer1(2,:);
accz = data.Accelerometer2(2,:);


%
% plot the PPG data
%
figure (1)
clf
ax1 = subplot(2,2,1);
plot(t1,ppg1,'g')
ylabel('PPG 1')

ax2 = subplot(2,2,2);
plot(t2,ppg2,'g')
ylabel('PPG 2')

ax3 = subplot(2,2,3);
plot(t3,ppg3,'b')
ylabel('PPG 3')

ax4 = subplot(2,2,4);
plot(t4,ppg4,'r')
ylabel('PPG 4')

%
% plot the Accel data
%
figure(2)
clf
ax5 = subplot(3,1,1);
plot(tacc, accx, '.');
ylabel('AccX');
ax6 = subplot(3,1,2);
plot(tacc, accy, '.');
ylabel('AccY');
ax7 = subplot(3,1,3);
plot(tacc, accz, '.');
ylabel('AccZ');
xlabel('time');

linkaxes([ax1 ax2 ax3 ax4 ax5 ax6 ax7], 'x');
