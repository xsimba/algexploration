% read csv
clear 

[ input_dir,output_dir,alg_dir ] = set_path( );
data = loadData(input_dir);
addpath(genpath(fullfile(alg_dir,'BeatDetection')));
addpath(genpath(fullfile(alg_dir,'ConfidenceIndicator','CI','CI_I')));
   
data = doTest('BD',data,'ecg'); 

ppg_channels = {'a','b','c','d','e','f','g','h'};
for cIdx = 1:numel(ppg_channels) 

    data = doTest('BD',data,['ppg.',ppg_channels{cIdx}]); % Matlab

end

% test CI Raw ECG
data = doTest('CI_raw',data, 'ecg'); % Matlab
 
csvwrite(fullfile(output_dir,'00-result-ECGConfidenceIndicator.csv'),data.ecg.CI_raw');
 
csvwrite(fullfile(output_dir,'00-result-ECGConfidenceIndicator_TS.csv'),data.ecg.CI_times');

disp('Done...');