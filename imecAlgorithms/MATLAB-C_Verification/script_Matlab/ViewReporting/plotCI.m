function okCode = plotCI( data,signal )
    
        okCode = 0 ;  
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953;];
       
        if strcmpi(signal,'ecg')
            
            figure('units','normalized','outerposition',[0 0 1 1])
            
            if isfield(data.ecg ,'CI_raw') && ~isfield(data.ecg ,'CI_raw_c')

                s = data.ecg.signal;
                ci_m = data.ecg.CI_raw ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI')

                title(signal)
                
            
            elseif ~isfield(data.ecg ,'CI_raw') && isfield(data.ecg ,'CI_raw_c')

                s = data.ecg.signal;
                ci_c = data.ecg.CI_raw_c ;
                ci_cts = data.ecg.CI_raw_cts ;
                ci_c = [ones(ci_cts(1),1); ci_c];

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_c(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                
                ylabel('C CI');
                title(signal)
            
            elseif isfield(data.ecg ,'CI_raw') && isfield(data.ecg ,'CI_raw_c')

                s = data.ecg.signal;
                ci_m = data.ecg.CI_raw ;
                ci_c = data.ecg.CI_raw_c ;
                ci_cts = data.ecg.CI_raw_cts ;
                ci_c = [ones(ci_cts(1),1); ci_c];

                ax1 = subplot(211);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI')
                
                ax2 = subplot(212);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_c(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI')
                
                title(ax1,signal)
                title(ax2,signal)
                linkaxes([ax1,ax2],'xy');
                
            end
            
        elseif strcmpi(signal(1:3),'ppg')
            
            figure('units','normalized','outerposition',[0 0 1 1])
            channel = lower(signal(end));
            if isfield(data.ppg.(channel) ,'CI_raw') && isfield(data.ppg.(channel) ,'CI_raw_c')

                s = data.ppg.(channel).signal;
                ci_m = data.ppg.(channel).CI_raw ;
                ci_c = data.ppg.(channel).CI_raw_c ;
                ci_cts = data.ppg.(channel).CI_raw_cts ;
                ci_c = [ones(ci_cts(1),1); ci_c];
                ci_m(ci_m<=0) = 1;
                ci_c(ci_c<=0) = 1;
                
                ax1 = subplot(211);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
   
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI')

                ax2 = subplot(212); 
                
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on; 
                
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_c(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                
                ylabel('C CI');
                
                title(ax1,signal)
                title(ax2,signal)
                linkaxes([ax1,ax2],'xy');
                           
            elseif isfield(data.ppg.(channel) ,'CI_raw') && ~isfield(data.ppg.(channel) ,'CI_raw_c')

                s = data.ppg.(channel).signal;
                ci_m = data.ppg.(channel).CI_raw ;
                
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on; 
                
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI')
                
                title(signal)

            elseif ~isfield(data.ppg.(channel) ,'CI_raw') && isfield(data.ppg.(channel) ,'CI_raw_c')

                s = data.ppg.(channel).signal;
                ci_c = data.ppg.(channel).CI_raw_c ;
                ci_cts = data.ppg.(channel).CI_raw_cts ;
                ci_c = [ones(ci_cts(1),1); ci_c];
                
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on; 
                
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_c(k),:),'LineWidth',1.7);hold on;
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI')
                
                title(signal)
                
            end
        end
        
        okCode = 1 ;
end

