function okCode = plotBeats( data,signal,channel )
    
        okCode = 0 ; 
        
        if strcmpi(signal,'ecg')
            figure('units','normalized','outerposition',[0 0 1 1])
            if isfield(data.(signal),'beats_c') && isfield(data.(signal) ,'beats')

                s = data.(signal).signal;
                m_bts = round(data.fs*data.(signal).beats(:)) ;
                m_bts(isnan(m_bts)) = [];
                m_bts(m_bts<=0) = [];
                c_bts = round(data.fs*data.(signal).beats_c(:)) ;
                c_bts = c_bts(5:end);
                c_bts(c_bts<=0) = [];
                ax1 = subplot(211); plot(s);hold on;plot(m_bts,s(m_bts),'or');
                ylabel('Matlab Beats')
                ax2 = subplot(212); plot(s);hold on;plot(c_bts,s(c_bts),'or');
                ylabel('C Beats')
                title(ax1,signal)
                title(ax2,signal)
                linkaxes([ax1,ax2],'xy');

            elseif isfield(data.(signal) ,'beats_c')

                s = data.(signal).signal;
                c_bts = round(data.fs*data.(signal).beats_c(:)) ;
                c_bts = c_bts(5:end);
                c_bts(c_bts<=0) = [];
                plot(s);hold on;plot(c_bts,s(c_bts),'or');
                ylabel('C Beats');
                title(signal)

            elseif isfield(data.(signal),'beats')

                s = data.(signal).signal;
                m_bts = round(data.fs*data.(signal).beats(:)) ;
                m_bts(isnan(m_bts)) = [];
                m_bts(m_bts<=0) = [];
                plot(s);hold on;plot(m_bts,s(m_bts),'or');
                ylabel('Matlab Beats');
                title(signal)

            end
        elseif strcmpi(signal(1:3),'ppg')
            channel = signal(end);
            signal = signal(1:3);
            figure('units','normalized','outerposition',[0 0 1 1])
            if isfield(data.(signal).(channel) ,'beats') && isfield(data.(signal).(channel) ,'beats_c')

                s = data.(signal).(channel).signal;
                m_bts = round(data.fs*data.(signal).(channel).beats) ;
                m_bts(m_bts<=0) = NaN;
                m_bts_1 = m_bts(1,:);
                m_bts_1(isnan(m_bts_1)) = [];
                m_bts_2 = m_bts(2,:);
                m_bts_2(isnan(m_bts_2)) = [];
                m_bts_4 = m_bts(4,:);
                m_bts_4(isnan(m_bts_4)) = [];
                c_bts = round(data.fs*data.(signal).(channel).beats_c) ;
                c_bts = c_bts(:,5:end);
                c_bts(c_bts<=0) = NaN;
                c_bts_1 = c_bts(1,:);
                c_bts_1(isnan(c_bts_1)) = [];
                c_bts_2 = c_bts(2,:);
                c_bts_2(isnan(c_bts_2)) = [];
                c_bts_4 = c_bts(4,:);
                c_bts_4(isnan(c_bts_4)) = [];
                ax1 = subplot(211); plot(s);hold on;plot(m_bts_1,s(m_bts_1),'or');plot(m_bts_2,s(m_bts_2),'+r');plot(m_bts_4,s(m_bts_4),'*r');
                ylabel('Matlab Beats')
                ax2 = subplot(212); plot(s);hold on;plot(c_bts_1,s(c_bts_1),'or');plot(c_bts_2,s(c_bts_2),'+r');plot(c_bts_4,s(c_bts_4),'*r');
                ylabel('C Beats')
                title(ax1,[signal,'.',channel])
                title(ax2,[signal,'.',channel])
                linkaxes([ax1,ax2],'xy');

            elseif isfield(data.(signal).(channel) ,'beats_c')

                s = data.(signal).(channel).signal;
                c_bts = round(data.fs*data.(signal).(channel).beats_c(:)) ;
                c_bts = c_bts(5:end);
                c_bts(c_bts<=0) = [];
                plot(s);hold on;plot(c_bts,s(c_bts),'or');
                title([signal,'.',channel])
                ylabel('C Beats');

            elseif isfield(data.(signal).(channel),'beats')

                s = data.(signal).(channel).signal;
                m_bts = round(data.fs*data.(signal).(channel).beats(:)) ;
                m_bts(isnan(m_bts)) = [];
                m_bts(m_bts<=0) = [];
                plot(s);hold on;plot(m_bts,s(m_bts),'or');
                title([signal,'.',channel])
                ylabel('Matlab Beats');
                
            end
        end
        
        okCode = 1 ;
end

