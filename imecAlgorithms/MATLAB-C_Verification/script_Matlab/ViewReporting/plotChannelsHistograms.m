function plotnames = plotChannelsHistograms(data,list_of_signals,output_directory)

    plotnames = {};
        
        for sigIdx =1:numel(list_of_signals)
            temp = data.(list_of_signals{sigIdx});
            if size(temp,1) == 1
                % signals histogram
                params = struct();
                params.name_suffix = [list_of_signals{sigIdx},'-signal'];
                params.xlabel = 'Signal Values';
                params.ylabel = '';
                params.nbins = 50;
                params.plotname = fullfile(output_directory,['Signal_Histogram_',list_of_signals{sigIdx}]);
                params.output_directory = output_directory;
                % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
                plotnames = [plotnames generate_histogram(temp,params)];
            elseif size(temp,1) == 3
                % signals histogram
                params = struct();
                params.name_suffix = [list_of_signals{sigIdx},'-signal'];
                params.xlabel = 'Signal Values';
                params.ylabel = '';
                params.nbins = 50;
                params.plotname = fullfile(output_directory,['Signal_Histogram_',list_of_signals{sigIdx}]);
                params.output_directory = 'plots';
                % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
                plotnames = [ plotnames generate_histogram(temp(2,:),params)];
                this_timestamps = temp(1,:);
                % correct timestamps to seconds 
                this_timestamps = (this_timestamps - this_timestamps(1))/1000;
                % timestamps histogram
                params = struct();
                params.name_suffix = [list_of_signals{sigIdx},'-timestamps'];
                params.xlabel = 'Time [secs]';
                params.ylabel = '';
                params.nbins = 50;
                params.plotname = fullfile(output_directory,['Timestamps_Histogram_',list_of_signals{sigIdx}]);
                params.output_directory = output_directory;
                % %%%%%%%%%%% FIX THIS %%%%%%%%%%% %
                plotnames = [plotnames generate_histogram(this_timestamps,params)];
            end
        end

end

