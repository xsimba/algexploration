% read csv
pat_data = csvread('..\input\00-in.csv');
pat_data = reshape(pat_data,7,int32(size(pat_data,1)/7));

data.ecg.beats = ecg_data;
data.ppg.a.beats = pat_data';
data.ppg.b.beats = pat_data';
data.ppg.c.beats = pat_data';
data.ppg.d.beats = pat_data';
data.ppg.e.beats = pat_data';
data.ppg.f.beats = pat_data';

 load('.\BP\full_bp_model.mat')
   
 dbp = predict(full_dbp_model,pat_data);
        
 sbp = predict(full_sbp_model,[pat_data dbp]);
 
 csvwrite(fullfile('..','output_Matlab','00-out.csv'),output(:));