clear all; 

setRCpath ; 
        
    % load data
    % interventions are in {'static','variation','consistency'}
    % posture is in {'sitting','standing'}
    % measurement is in {1,2,3}
    data = getLocalSimbandData(21,'static','standing',3);
    %checkSignals(data);
    
    % Test BD ECG 
    % Matlab 
    data = doTest('BD',data,'ecg'); 
    % C
    data = doTestC('BD',data,'ecg');
    % visualize
    plotBeats(data,'ecg');
    
    % Test BD PPG
    ppg_channels = {'a','b','c','d','e','f','g','h'};
    for cIdx = 1:numel(ppg_channels) 
        
        data = doTest('BD',data,['ppg.',ppg_channels{cIdx}]); % Matlab
        data = doTestC('BD',data,['ppg.',ppg_channels{cIdx}]); % C
       
        % visualize
        plotBeats(data,['ppg.',ppg_channels{cIdx}]);
        input('')
        close all
    end
    
    
    % test CI Raw ECG
    data = doTest('CI_raw',data, 'ecg'); % Matlab
    data = doTestC('CI_raw',data,'ecg');  % C 
    % visualize
    plotCI(data,'ecg');

    % test CI Raw PPG
    for cIdx = 1:numel(ppg_channels) 
        
        data = doTest('CI_raw',data,['ppg.',ppg_channels{cIdx}]); % Matlab
        data = doTestC('CI_raw',data,['ppg.',ppg_channels{cIdx}]); % C
       
        % visualize
        plotCI(data,['ppg.',ppg_channels{cIdx}]);
        input('')
        close all
    end
    
    % test CI Beats ECG
    data = doTest('CI_II',data, 'ecg'); % Matlab
    data = doTestC('CI_II',data,'ecg');  % C 
    % visualize
    plotCIBeats(data,'ecg');

    % test CI Beats PPG
    for cIdx = 1:numel(ppg_channels) 
        
        data = doTest('CI_II',data,['ppg.',ppg_channels{cIdx}]); % Matlab
        data = doTestC('CI_II',data,['ppg.',ppg_channels{cIdx}]); % C
       
        % visualize
        plotCIBeats(data,['ppg.',ppg_channels{cIdx}]);
        input('')
        close all
    end

    data = doTest('PAT',data,'ppg.a');

   
   disp('hola')