% read csv
clear 

[ input_dir,output_dir,alg_dir ] = set_path( );
data = loadData(input_dir);

addpath(genpath(fullfile(alg_dir,'BeatDetection')));
data = doTest('BD',data,'ecg'); 
 
csvwrite(fullfile(output_dir,'00-result-ECG_Beats.csv'),data.ecg.beats');
disp('Done...');
