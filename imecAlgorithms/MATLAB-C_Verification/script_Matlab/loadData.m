function data = loadData(input_dir)

    data.timestamps = csvread(fullfile(input_dir,'00-in-TIMESTAMPS.csv'));
    data.ecg.signal = csvread(fullfile(input_dir,'00-in-ECG.csv'));
    data.ppg.a.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.b.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.c.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.d.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.e.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.f.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.g.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.ppg.h.signal = csvread(fullfile(input_dir,'00-in-PPG.csv'));
    data.acc.x.signal = csvread(fullfile(input_dir,'00-in-ACCX.csv'));
    data.acc.y.signal = csvread(fullfile(input_dir,'00-in-ACCY.csv'));
    data.acc.z.signal = csvread(fullfile(input_dir,'00-in-ACCZ.csv'));
    temp = csvread(fullfile(input_dir,'00-in-ACCALL.csv'));
    data.acc.All = reshape(temp,4,size(temp,1)/4);
    
end

