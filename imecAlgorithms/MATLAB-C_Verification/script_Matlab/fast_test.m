clear all; 

setRCpath ; 
        
    % load data
    % interventions are in {'static','variation','consistency'}
    % posture is in {'sitting','standing'}
    % measurement is in {1,2,3}
    data = getLocalSimbandData(21,'static','standing',3);
    %checkSignals(data);
    
    % Test BD ECG 
    % Matlab 
    data = doTest('BD',data,'ecg'); 
    % C
    data = doTestC('BD',data,'ecg');
    
    % Test BD PPG
    ppg_channels = {'a','b','c','d','e','f','g','h'};
    for cIdx = 1:numel(ppg_channels) 
        
        data = doTest('BD',data,['ppg.',ppg_channels{cIdx}]); % Matlab
        data = doTestC('BD',data,['ppg.',ppg_channels{cIdx}]); % C

    end
    
    
    % test CI Raw ECG
    data = doTest('CI_raw',data, 'ecg'); % Matlab
    data = doTestC('CI_raw',data,'ecg');  % C 

    % test CI Raw PPG
    for cIdx = 1:numel(ppg_channels) 
        
        data = doTest('CI_raw',data,['ppg.',ppg_channels{cIdx}]); % Matlab
        data = doTestC('CI_raw',data,['ppg.',ppg_channels{cIdx}]); % C

    end
    
    % test CI Beats ECG
    data = doTest('CI_II',data, 'ecg'); % Matlab
    data = doTestC('CI_II',data,'ecg');  % C 


    % test CI Beats PPG
    for cIdx = 1:numel(ppg_channels) 
        
        data = doTest('CI_II',data,['ppg.',ppg_channels{cIdx}]); % Matlab
        data = doTestC('CI_II',data,['ppg.',ppg_channels{cIdx}]); % C

    end

    data = doTest('PAT',data,'ppg.a');
    data = doTestC('PAT',data,'ppg.a');
   
   