function output = doTestC( algorithm,data,parameters )

    try
        % %%%%%%%%%%%%%% BEAT DETECTOR ECG %%%%%%%%%%%%%%%%%%%%%%%%%%
        if strcmpi(algorithm,'BD')&& strcmpi(parameters,'ECG')
            
            command  = 'Algorithms_C\RunECGBeatDetector.exe -i Algorithms_C\data';
            saveCsv('Algorithms_C\data\00-in-ECG.csv',data.ecg.signal);
            system(command);
            temp = csvread('Algorithms_C\data\00-result-ECG_Beats.csv');
            output = data ; 
            output.ecg.beats_c = temp ;
            
        % %%%%%%%%%%%%%% BEAT DETECTOR PPG %%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif  strcmpi(algorithm,'BD')&& strcmpi(parameters(1:3),'PPG')
            
            command  = 'Algorithms_C\RunPPGBeatDetector.exe -i Algorithms_C\data';
            saveCsv('Algorithms_C\data\00-in-PPG.csv',data.(parameters(1:3)).(parameters(end)).signal);
            system(command);
            temp = csvread('Algorithms_C\data\00-result-PPG_Beats.csv'); 
            temp = reshape(temp,4,length(temp)/4);
            output = data;
            output.(parameters(1:3)).(parameters(end)).beats_c = temp ;
            
        % %%%%%%%%%%%%%% PAT %%%%%%%%%%%%%%%%%%%%%%%%%%            
        elseif  strcmpi(algorithm,'PAT')
            warning('PAT is using beats computed from matlab..this will not reflect the full chain in C')
            % checks if the BD-ECG Results are there
            % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
%             if (exist('Algorithms_C\data\00-result-ECG_Beats.csv','file')==2) 
%                 % if results are there, copy and changes the name
%                 copyfile('Algorithms_C\data\00-result-ECG_Beats.csv','Algorithms_C\data\00-in-ECG_Beats.csv');
%             else
%                 error('Error: ECG Beats not found. Compute Beats')
%             end
%             
%             % checks if the BD-PPG Results are there
%             % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
%             if (exist('Algorithms_C\data\00-result-PPG_Beats.csv','file')==2) 
%                 % if results are there, copy and changes the name
%                 copyfile('Algorithms_C\data\00-result-PPG_Beats.csv','Algorithms_C\data\00-in-PPG_Beats.csv');
%             else
%                 error('Error: PPG Beats not found. Compute Beats')
%             end
            saveCsv('Algorithms_C\data\00-in-ECG_Beats.csv',data.ecg.beats);
            saveCsv('Algorithms_C\data\00-in-PPG_Beats.csv',data.ppg.(parameters(end)).beats);
            command  = 'Algorithms_C\RunPAT.exe -i Algorithms_C\data';
            system(command);
            temp = csvread('Algorithms_C\data\00-result-PAT.csv');
            pat_c = reshape(temp,4,size(temp,1)/4);
            pat_c_ci = csvread('Algorithms_C\data\00-result-PAT_CI.csv')';
            pat_c_ts = csvread('Algorithms_C\data\00-result-PAT_TS.csv')';
            output = data ;
            output.ppg.(parameters(end)).pat_c = pat_c;
            output.ppg.(parameters(end)).pat_c_ci = pat_c_ci;
            output.ppg.(parameters(end)).pat_c_ts = pat_c_ts;
            
        % %%%%%%%%%%%%%% CI_I ECG %%%%%%%%%%%%%%%%%%%%%%%%%%            
        elseif  strcmpi(algorithm,'CI_Raw')&& strcmpi(parameters,'ECG')
            % checks if the BD-ECG Results are there
            % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
            saveCsv('Algorithms_C\data\00-in-ECG.csv',data.ecg.signal');
            saveCsv('Algorithms_C\data\00-in-ACCX.csv',data.acc.All(1,:));
            saveCsv('Algorithms_C\data\00-in-ACCY.csv',data.acc.All(2,:));
            saveCsv('Algorithms_C\data\00-in-ACCZ.csv',data.acc.All(3,:));

            command  = 'Algorithms_C\RunECGConfidenceIndicator.exe --input-dir Algorithms_C\data';
            system(command);
            ci = csvread('Algorithms_C\data\00-result-ECG_CI.csv');
            ci_ts = csvread('Algorithms_C\data\00-result-ECG_CI_TS.csv');
            output = data ;
            output.ecg.CI_raw_c = ci;
            output.ecg.CI_raw_cts = ci_ts;
            
        % %%%%%%%%%%%%%% CI_I PPG %%%%%%%%%%%%%%%%%%%%%%%%%%            
        elseif  strcmpi(algorithm,'CI_Raw')&& strcmpi(parameters(1:3),'PPG')
            % checks if the BD-ECG Results are there
            % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
            saveCsv('Algorithms_C\data\00-in-PPG.csv',data.(lower(parameters(1:3))).(lower(parameters(end))).signal');
            saveCsv('Algorithms_C\data\00-in-ACCX.csv',data.acc.All(1,:));
            saveCsv('Algorithms_C\data\00-in-ACCY.csv',data.acc.All(2,:));
            saveCsv('Algorithms_C\data\00-in-ACCZ.csv',data.acc.All(3,:));

            command  = 'Algorithms_C\RunPPGConfidenceIndicator.exe --input-dir Algorithms_C\data';
            system(command);
            output = csvread('Algorithms_C\data\00-result-PPG_CI.csv');   
            ci = csvread('Algorithms_C\data\00-result-PPG_CI.csv');
            ci_ts = csvread('Algorithms_C\data\00-result-PPG_CI_TS.csv');
            output = data ;
            output.(lower(parameters(1:3))).(lower(parameters(end))).CI_raw_c = ci;
            output.(lower(parameters(1:3))).(lower(parameters(end))).CI_raw_cts = ci_ts;
            
             
        % %%%%%%%%%%%%%% CI_II ECG %%%%%%%%%%%%%%%%%%%%%%%%%%            
        elseif  strcmpi(algorithm,'CI_II')&& strcmpi(parameters(1:3),'ECG')
            % checks if the BD-ECG Results are there
            % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
            saveCsv('Algorithms_C\data\00-in-ECGBD.csv',data.(lower(parameters(1:3))).beats_c');
            saveCsv('Algorithms_C\data\00-in-LOLO.csv',ones(length(data.(lower(parameters(1:3))).beats_c),1));
            saveCsv('Algorithms_C\data\00-in-LOLO_TS.csv',1:length(data.(lower(parameters(1:3))).beats_c)');

            command  = 'Algorithms_C\RunECGConfidenceIndicatorBeats.exe --input-dir Algorithms_C\data';
            system(command);
            cib = csvread('Algorithms_C\data\00-result-ECG_CIBD.csv');   
            ci_ts = csvread('Algorithms_C\data\00-result-ECG_CIBD_TS.csv');   
            output = data ;
            output.ecg.CI_beat_c = cib;
            output.ecg.CI_beats_cts = ci_ts;
            
        % %%%%%%%%%%%%%% CI_II PPG %%%%%%%%%%%%%%%%%%%%%%%%%%            
        elseif  strcmpi(algorithm,'CI_II')&& strcmpi(parameters(1:3),'PPG')
            % checks if the BD-ECG Results are there
            % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
            saveCsv('Algorithms_C\data\00-in-PPGBD.csv',data.(lower(parameters(1:3))).(lower(parameters(end))).beats_c');
            saveCsv('Algorithms_C\data\00-in-PPG_CI.csv',data.(lower(parameters(1:3))).(lower(parameters(end))).CI_raw_c);
            saveCsv('Algorithms_C\data\00-in-PPG_CI_TS.csv',data.(lower(parameters(1:3))).(lower(parameters(end))).CI_raw_cts);

            command  = 'Algorithms_C\RunPPGConfidenceIndicatorBeats.exe --input-dir Algorithms_C\data';
            system(command);
            cib = csvread('Algorithms_C\data\00-result-PPG_CIBD.csv')';   
            ci_ts = csvread('Algorithms_C\data\00-result-PPG_CIBD_TS.csv')';   
            output = data ;
            output.ppg.(lower(parameters(end))).CI_beat_c = cib;
            output.ppg.(lower(parameters(end))).CI_beats_cts = ci_ts;
            
        % %%%%%%%%%%%%%% BLOOD PRESSURE %%%%%%%%%%%%%%%%%%%%%%%%%%
        elseif  strcmpi(algorithm,'BloodPressure')
            % checks if the BD-ECG Results are there
            % TODO: ADD CHECK FOR VARARGIN IF FILE DOES NOT EXISTS
            if (exist('Algorithms_C\data\00-result-PAT.csv','file')==2) 
                % if results are there, copy and changes the name
                temp = csvread('Algorithms_C\data\00-result-PAT.csv');
                temp = reshape(temp,4,size(temp,1)/4)';
                temp = [temp repmat([data.ssb.age, data.ssb.weight, data.ssb.height, data.ssb.posture],size(temp,1),1)];
                temp = temp'; % do something smarter here when the structure will be decided
                temp = temp(:);
                saveCsv('Algorithms_C\data\00-in.csv',temp);
                command  = 'Algorithms_C\RunBloodPressure.exe -i Algorithms_C\data';
                system(command);
                bp = csvread('Algorithms_C\data\00-result.csv');
                bp = reshape(bp,2,int32(0.5*size(bp,1)))';
                output = data ; 
                output.bp.sbp = bp(:,1);
                output.bp.dbp = bp(:,2);
            else
                error('Error: PAT not found. Compute PAT')
            end
            
        end
    catch err
        disp(['Error ',err.identifier,'.',err.message])
        output = data ;
    end
    
    close all
end

