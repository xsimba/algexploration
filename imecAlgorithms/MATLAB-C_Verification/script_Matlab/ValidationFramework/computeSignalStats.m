function computed_stats = computeSignalStats(data,channel,params)
%computeSignalStats generates statistics on the signal
% types of statistics histgram
    
     if ~isfield(params,'stats') || strcmpi(params.stats,'')
        err = MException('ParamsChk:NoStatsDefined', ...
        'No statistics defined for function ComputeSignalStats');
        throw(err);
    else
        switch params.stats
            case 'histogram'
                data = data.(channel);
        end
    end
    
    if ~isfield(params,'nbins') || params.nbins==0
        warning('Number of bins not defined. Set to 30');
        params.nbins = 30;
    end

    min_val = min(data);
    max_val = max(data);
    binrange = [min_val:(max_val-min_val)/params.nbins:max_val];
    [bincount] = histc(data,binrange);
    computed_stats.bincount = bincount;
    computed_stats.binrange = binrange;
            
 

end

