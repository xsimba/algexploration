function computed_stats = computeSignalStats(channel,params,varargin)
%computeSignalStats generates statistics on the signal
% types of statistics histgram

  
    % addpath 
    dataset_directory = fullfile('.','DataBases');        
    
    % code to deal with raw data before SRC
    % %%%% bugprone code: CHECK IT FOR SMARTER SOLUTION %%%% %%
    if nargin==2
        file_ends_with = '*.mat';
        data_starts_with='';
    elseif nargin==3
        file_ends_with = '*raw.mat';
        data_starts_with='raw_';
    end
    
    % loads data
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,file_ends_with));    
           
    load(fullfile(pwd,dataset_directory,test_files(1).name));
    input = eval([data_starts_with,'data']);
    

    
     if ~isfield(params,'stats') || strcmpi(params.stats,'')
        err = MException('ParamsChk:NoStatsDefined', ...
        'No statistics defined for function ComputeSignalStats');
        throw(err);
    else
        switch params.stats
            case 'histogram'
                data = input.(channel);
        end
    end
    
    if ~isfield(params,'nbins') || params.nbins==0
        warning('Number of bins not defined. Set to 30');
        params.nbins = 30;
    end

    min_val = min(data);
    max_val = max(data);
    binrange = [min_val:(max_val-min_val)/params.nbins:max_val];
    [bincount] = histc(data,binrange);
    computed_stats.bincount = bincount;
    computed_stats.binrange = binrange;
            
 

end

