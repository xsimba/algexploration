% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : compute_metrics.m
%  Content    : Test Framework for Simba Algorithgms
%  Version   : 0.1
%  Created by: P. Casale (pierluigi.casale@imec-nl.nl)
%  Date      : 24-04-2014
%  Modification and Version History: | Developper | Version | Date | 
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function metrics = compute_metrics(detections,expected,tol)
% This functions calculates the metrics of the input lists.
%
% INPUT:
%  - ExpectedBeatTimes   - List of expected beat-times
%  - DetectedBeatTimes   - List of detected beat-times
%  - TimeAccuracy     - Tolerance between expected and detected time
%
% OUTPUT:
%  - Metrics             - Struct with fields:
%     o NrTruePositives  - Number of True Positives (TP)
%     o NrFalsePositives - Number of False Positive (FP)
%     o NrFalseNegatives - Number of False Negatives (FN)
%     o NrBeats          - Number of expected beats
%     o Se               - Sensitivity
%     o Pp               - Positive Predictivity
%     o De               - Detection error
%     o tp_indx          - Index of the true positives in the list of expected beat-times
%     o fp_indx          - Index of the false positives in the list of detected beat-times
%     o fn_indx          - Index of the false negatives in the list of expected beat-times
%
% Example:
% LOE              = [756, 756.256, 1000];                                               % List of expected
% LOD              = [710, 750, 750, 756.300, 756.301, 1000, 1250.5, 1250.5, 1250.5];    % List of detected
% Cfg.TimeAccuracy = 0.1;                                                                % (s) Tolerance
% Metrics          = BeatDetector(@calculate_metrics,LOE, LOD, Cfg)
% Metrics = 
% 
%      NrTruePositives: 2
%     NrFalsePositives: 7
%     NrFalseNegatives: 1
%              NrBeats: 3
%                   Se: 66.6666666666667
%                   Pp: 22.2222222222222
%                   De: 2.66666666666667
%              tp_indx: [3x1 logical]
%              fp_indx: [9x1 logical]
%              fn_indx: [3x1 logical]

    metrics = struct('NrTruePositives', [], ...
                     'NrFalsePositives', [], ...
                     'NrFalseNegatives', [], ...
                     'NrBeats', [], ...
                     'Se', [], ...
                     'Pp', [], ...
                     'De', [], ...
                     'tp_indx', [], ...
                     'fp_indx', [], ...
                     'fn_indx', []);

    
    % Determine all true positives and all false negatives
    % tp: exists in  EXP &&  DET
    % fp: exists in !EXP &&  DET
    % fn: exists in  EXP && !DET
    tp_indx = false(size(expected));
    fn_indx = false(size(expected));
    fp_indx = false(size(detections));
    NrRemoved = 0;
    
    for k=1:length(expected)
        candidates = abs(expected(k)-detections)<tol;
        if sum(candidates)>=1
            L                           = find(candidates);
            fp_indx(L(2:end)+NrRemoved) = true;                                 % Set all multiple entries within tolerance to FP
            tp_indx(k)                  = true;
        else
            fn_indx(k) = true;
        end
        detections(candidates) = [];
        NrRemoved          = NrRemoved + sum(candidates);
    end
    
    % Find the index of the left-over entries in detqrs. When DetectedBeatTimes
    % consists of multiple entries, the index of the first index will be taken.
    for k=1:length(detections)
        indx = detections == detections(k);
        fp_indx(indx) = true;                                               % Set all detections which where not TP as FP
    end
    fp = sum(fp_indx);
    tp = sum(tp_indx);
    fn = sum(fn_indx);

    metrics.NrTruePositives  = tp;
    metrics.NrFalsePositives = fp;
    metrics.NrFalseNegatives = fn;
    metrics.NrBeats          = length(expected);
    metrics.Se               = tp/(tp+fn)*100;                           % Sensitivity
    metrics.Pp               = tp/(tp+fp)*100;                           % Positive Predictivity
    metrics.De               = (fp+fn)/(tp+fn);                          % Detection error
    metrics.tp_indx          = tp_indx;
    metrics.fp_indx          = fp_indx;
    metrics.fn_indx          = fn_indx;
    
end