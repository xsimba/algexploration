function output = doTest( algorithm,data,parameters )
%doTest runs algorithm on signal

    % runs the function here
    try

        if strcmpi(algorithm,'BD') && strcmpi(parameters(1:3),'ecg') 
            [o1,o2] = feval(algorithm,[],data.(parameters(1:3)).signal,parameters);
            output = data;
            output.(parameters).beats_ts = o1;
            output.(parameters).DB = o2;
            output.(parameters).beats = o1/o2.FreqTS;

        elseif strcmpi(algorithm,'BD') && strcmpi(parameters(1:3),'ppg')
            [o1,o2] = feval(algorithm,[],data.(parameters(1:3)).(parameters(end)).signal,parameters(1:3));
            output = data;
            output.(parameters(1:3)).(parameters(end)).beats_ts = o1;   
            output.(parameters(1:3)).(parameters(end)).DB = o2;
            output.(parameters(1:3)).(parameters(end)).beats = o1/o2.FreqTS;

        elseif strcmpi(algorithm(1:2),'CI') 
            if strcmpi(parameters,'ecg')
                output = feval(algorithm,data,'ecg');
            else
                output = feval(algorithm,data,lower(parameters(end)));
            end          
            
        elseif strcmpi(algorithm,'BloodPressure')
            output = feval(algorithm,data,parameters);
            
        elseif strcmpi(algorithm,'PAT')
            output = PATc(data,lower(parameters(end)));
            
        else 
            output = feval(algorithm,data);
        end

    catch err
        disp([algorithm,'.',parameters,':',err.message])
        disp('No Result written in the data structure...')
        output = data ;
    end
    
    %rmpath(fullfile(pwd,algorithms_directory));
    close all
end

