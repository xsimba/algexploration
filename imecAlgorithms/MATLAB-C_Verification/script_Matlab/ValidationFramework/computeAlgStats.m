function computed_stats = computeAlgStats(algOutput,params)
%computeAlgStats generates statistics on the output of the algorithm
    
    if ~isfield(params,'stats') || strcmpi(params.stats,'')
        err = MException('ParamsChk:NoStatsDefined', ...
        'No statistics defined for function ComputeAlgStats');
        throw(err);
    else
        switch params.stats
            case 'histogram'
                data = algOutput;
            case 'interbeat-histogram'
                data = diff(algOutput);
        end
    end
    
    if ~isfield(params,'nbins') || params.nbins==0
        warning('Number of bins not defined. Set to 30');
        params.nbins = 30;
    end

    min_val = min(data);
    max_val = max(data);
    binrange = [min_val:(max_val-min_val)/params.nbins:max_val];
    [bincount] = histc(data,binrange);
    computed_stats.bincount = bincount;
    computed_stats.binrange = binrange;

end

