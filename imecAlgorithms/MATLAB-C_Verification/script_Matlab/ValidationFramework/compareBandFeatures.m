function metrics = compareBandFeatures( computedOutput,data,signalReference,time_tolerance )
%compareBandFeatures compares the output of doTest with simband features
%   Usage Example: 
%       output = doTest(alg,signal,Cfg);
%       metrics = compareBandFeatures( output,signal )
%           where metrics is a struct with
%           metrics.truePositiveRate
%           metrics.tpositivePredictiveValue
%           metrics.falseNegativeRate
 
    
    metrics = compute_metrics(computedOutput,data.(signalReference),time_tolerance);
    

end

