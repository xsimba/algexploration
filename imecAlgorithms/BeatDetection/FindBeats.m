function [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : FindBeats.m
% Content   : Matlab function for ECG beat detection
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% Timestamp       Timestamp of first sample in block (32768 Hz ticks)
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters
% FoundBeats      Boolean flag to indicate if any beats were found in current block
% BeatTimestamps  List of timestamps of beats

% Sanity check
if (Params.SigTypeN ~= 0), error('Calling FindBeats for PPG or BioZ!'); end

if (isempty(State)) % Populate state: CWT state and from PeakDetect.cpp
    State.cwt = ones(length(Params.CWT_FIR)-1,1)*InputSamples(1); % Initial state of CWT, constant level of first sample
    
    % Initialise filter
    [~,State.cwt] = filter(fliplr(Params.CWT_FIR),1,ones(length(Params.CWT_FIR),1)*InputSamples(1)); % Perform filter (reverse order for Matlab to match C impl.)
    
    State.cmp = 0;
    State.v = 0;
    State.av = 0;
    State.dv = 0;
    State.max = 0;
    State.maxindx = 0;
    State.fallingedge = 0; % false
    State.risingedge = 0; % false
    State.holdoffcounter = 0;
    State.PrevInputSamples = zeros(1,BlockSize);
    State.PrevBlockSize = BlockSize;
    State.Count = 0;
    State.offset = 0;
end

% For each call to FindBeats, we start with an empty list
BeatTimestamps = [];

% Apply filters: CWT and Abs / Zero Clamp
[CWTsamples,State.cwt] = filter(fliplr(Params.CWT_FIR),1,InputSamples,State.cwt); % Perform filter (reverse order for Matlab to match C impl.)
State.Debug_CWToutSamples = CWTsamples; % Store CWT output for debugging
aCWTsamples = abs(CWTsamples); % ECG use abs function

% Automatically update the offset parameter for the peak detector to the optimal value using standard deviation
ss = std(aCWTsamples)*Params.StdScale;
if (State.Count < Params.OffUpdateFblk), State.offset = State.offset*(1-Params.OffUpdateFast) + ss*Params.OffUpdateFast; % Fast update for first OffUpdateFblk sample blocks
else                                     State.offset = State.offset*(1-Params.OffUpdateSlow) + ss*Params.OffUpdateSlow; % Slow update for rest
end

% Apply the peak detection algorithm
[FoundPeaks, PeakIndexList, State] = FindPeaks(aCWTsamples, BlockSize, State, Params);

if (FoundPeaks)
    %InterpolatedIndexList = QuadInterpolatePeaks(aCWTsamples, State.PrevInputSamples, State.PrevBlockSize, PeakIndexList); % Refine peak location(s)
    InterpolatedIndexList = QuadInterpolatePeaks(CWTsamples, State.PrevInputSamples, State.PrevBlockSize, PeakIndexList); % Refine peak location(s) - use non-abs values
    
    % PeakIndexList, InterpolatedIndexList and GroupDelay are in units of samples relative to the current block start
    % Timestamp refers to the first sample of the block, ts2samRatio converts from sample to timestamp units
    BeatTimestamps = Timestamp + (InterpolatedIndexList - Params.GroupDelay) * Params.ts2samRatio;
end

%State.PrevInputSamples = aCWTsamples; % Current becomes previous block
State.PrevInputSamples = CWTsamples; % Current becomes previous block - use non-abs values

State.PrevBlockSize = BlockSize;
State.Count = min(State.Count + 1,65534); % Limit to fit in 16 bit unsigned integer (to prevent wrapping around to zero)
FoundBeats = FoundPeaks;
end
