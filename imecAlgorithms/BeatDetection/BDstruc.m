% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : BDstruc.m
%  Content    : Wrapper for beat detector using structures as input and output containers (can be the same structure)
%  Package    : SIMBA.Validation
%  Created by : A. Young (alex.young@imec-nl.nl)
%  Date       : 20-11-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [data] = BDstruc(data, SigID, varargin)
% Beat Detection
%   Inputs:
%      data                                    :  Structure: timestamps in units of SECONDS
%        .ppg.{a..g}.{signal, timestamps, fs}  :  PPG input
%        .bio.{i|q}.{signal, timestamps, fs}   :  BioZ input
%        .ecg.{signal, timestamps, fs}         :  ECG input
%    OR (alternative input format)
%      data                                    :  Structure: timestamps in units of SECONDS
%        .timestamps                           :  Time stamps for each sample in units of SECONDS
%        .ppg.{a..g}.{signal, fs}              :  PPG input
%        .bio.{i|q}.{signal, fs}               :  BioZ input
%        .ecg.{signal, fs}                     :  ECG input
%    NOTE: using any format, the timestamps field does not need to exist, if missing, a substitute will be used starting from zero.
%      SigType                                 :  String : 'ecg' | 'ppg.x' | 'bio.y', x = a to h, y = i or q
%      varargin                                :  Integer | empty : Filter pass band frequency (Hz) for PPG or BioZ only, no effect for ECG
%
%   Output:
%      data                                    :  Structure appended to:
%        .ppg.{a..g}.bd.{element, debug}       :  PPG output (upstroke and foot times of current cycle, pripeak and secpeak times of previous cycle, debug information)
%        .bio.{i|q}.bd.{element, debug}        :  BioZ output (upstroke and foot times of current cycle, pripeak and secpeak times of previous cycle, debug information)
%        .ecg.bd.{rpeak, debug}                                    :  ECG output (r-peak time, debug information)
%      (Time values decrease from upstroke to primary peak.)
%
%   Usage Examples:
%      data = BDstruc(data, 'ecg');     % ECG input
%      data = BDstruc(data, 'ppg.g');   % PPG input g
%      data = BDstruc(data, 'bio.i');   % BioZ input i

% pwa    element    Description
% (1,:)  .upstroke  Upstroke timestamp (current cycle)
% (2,:)  .foot      Foot timestamp (current cycle)
% (3,:)  .secpeak   Secondary peak timestamp (previous cycle)
% (4,:)  .pripeak   Primary peak timestamp (previous cycle)
% (5,:)  .dicrnot   Dicrotic notch timestamp (previous cycle)
% (6,:)  .spftamp   Foot to secondary peak amplitude
% (7,:)  .dnftamp   Foot to dicrotic notch amplitude
% (8,:)  .ppftamp   Foot to primarty peak amplitude
% (9,:)  .upstgrad  Gradient at upstroke
% (10,:) .decaytime Decay time (previous cycle)
% (11,:) .risetime  Rise time (previous cycle)
% (12,:) .RCtime    RC time (exp fit, previous cycle)

% Format and checks
LenSigID = length(SigID);
if ((LenSigID < 3)||(LenSigID > 5)), error('SigID must contain between 3 and 5 characters only: ecg, ppg.{a..g} or bio.{i|q}.'); end

Char = char(SigID);
SigType = cellstr(Char(1:3)); % First 3 characters

LenExtraVar = length(varargin);
if (LenExtraVar > 1), error('BDstruc: Only 2 or 3 input arguments accepted!'); end

% General and defaults
FreqTS          = 32768; % Timestamp clock frequency
Fs              = 128;   % When Fs field is absent, use this default value
InputTimestamps = [];    % When timestamp field is absent

% Signal type specific processing for input
if     strcmpi(SigType,'ecg')
    SigTypeN  = 0; % 0:ECG, 1:PPG, 2:BioZ
    SigInfo   = data.ecg;
    
elseif strcmpi(SigType,'ppg')
    SigTypeN  = 1; % 0:ECG, 1:PPG, 2:BioZ
    ChChar    = Char(5);
    if (~isfield(data.ppg,ChChar)), error(['Field data.ppg.',ChChar,' does not exist.']); end
    SigInfo   = data.ppg.(ChChar);
    
elseif strcmpi(SigType,'bio')
    SigTypeN  = 2; % 0:ECG, 1:PPG, 2:BioZ
    ChChar    = Char(5);
    if (~isfield(data.bio,ChChar)), error(['Field data.bio.',ChChar,' does not exist.']); end
    SigInfo   = data.bio.(ChChar);
    
else
    error('Unknown SigType, expecting ecg, ppg.{a..h} or bio.{i|q}');
end

% Extract fields
                                    InputStream     = SigInfo.signal;
if (isfield(SigInfo,'timestamps')), InputTimestamps = SigInfo.timestamps*FreqTS; end % If field exists, convert to timestamp frequency units, otherwise test for alternative format...
if (isfield(data,'timestamps')),    InputTimestamps = data.timestamps*FreqTS;    end % If field exists, convert to timestamp frequency units, otherwise use default empty vector.
if (isfield(SigInfo,'fs')),         Fs              = SigInfo.fs;                end % If field exists, update sample frequency, otherwise use default.

% Call beat detection algorithm
if (LenExtraVar == 0)
    [BeatsTimestamps,Debug] = BD(InputTimestamps, InputStream, Fs, SigType);
else
    [BeatsTimestamps,Debug] = BD(InputTimestamps, InputStream, Fs, SigType, varargin{1});
end

% Signal type specific processing for output
if     (SigTypeN == 0) % ECG
    data.ecg.bd.rpeak             = BeatsTimestamps(1,:)/FreqTS; % Convert from TS to time
    data.ecg.bd.debug             = Debug;
    
elseif (SigTypeN == 1) % PPG
    data.ppg.(ChChar).bd.upstroke  = BeatsTimestamps(1,:)/FreqTS; % Convert from TS to time
    data.ppg.(ChChar).bd.foot      = BeatsTimestamps(2,:)/FreqTS;
    data.ppg.(ChChar).bd.secpeak   = BeatsTimestamps(3,:)/FreqTS;
    data.ppg.(ChChar).bd.pripeak   = BeatsTimestamps(4,:)/FreqTS;
    data.ppg.(ChChar).bd.dicrnot   = BeatsTimestamps(5,:)/FreqTS;
    data.ppg.(ChChar).bd.spftamp   = BeatsTimestamps(6,:);
    data.ppg.(ChChar).bd.dnftamp   = BeatsTimestamps(7,:);
    data.ppg.(ChChar).bd.ppftamp   = BeatsTimestamps(8,:);
    data.ppg.(ChChar).bd.upstgrad  = BeatsTimestamps(9,:);
    data.ppg.(ChChar).bd.decaytime = BeatsTimestamps(10,:)/FreqTS;
    data.ppg.(ChChar).bd.risetime  = BeatsTimestamps(11,:)/FreqTS;
    data.ppg.(ChChar).bd.RCtime    = BeatsTimestamps(12,:)/FreqTS;
    data.ppg.(ChChar).bd.debug     = Debug;
    
elseif (SigTypeN == 2) % BioZ
    data.bio.(ChChar).bd.upstroke  = BeatsTimestamps(1,:)/FreqTS; % Convert from TS to time
    data.bio.(ChChar).bd.foot      = BeatsTimestamps(2,:)/FreqTS;
    data.bio.(ChChar).bd.secpeak   = BeatsTimestamps(3,:)/FreqTS;
    data.bio.(ChChar).bd.pripeak   = BeatsTimestamps(4,:)/FreqTS;
    data.bio.(ChChar).bd.dicrnot   = BeatsTimestamps(5,:)/FreqTS;
    data.bio.(ChChar).bd.spftamp   = BeatsTimestamps(6,:);
    data.bio.(ChChar).bd.dnftamp   = BeatsTimestamps(7,:);
    data.bio.(ChChar).bd.ppftamp   = BeatsTimestamps(8,:);
    data.bio.(ChChar).bd.upstgrad  = BeatsTimestamps(9,:);
    data.bio.(ChChar).bd.decaytime = BeatsTimestamps(10,:)/FreqTS;
    data.bio.(ChChar).bd.risetime  = BeatsTimestamps(11,:)/FreqTS;
    data.bio.(ChChar).bd.RCtime    = BeatsTimestamps(12,:)/FreqTS;
    data.bio.(ChChar).bd.debug     = Debug;
    
else
    error('Unknown SigType, expecting ecg, ppg or bio');
end


% Debugging plots (use here or use as examples elsewhere)

% Select general data
if     (SigTypeN == 0), gendata = data.ecg.bd;          % ECG
elseif (SigTypeN == 1), gendata = data.ppg.(ChChar).bd; % PPG
else                    gendata = data.bio.(ChChar).bd; % BioZ
end

if (0) % Enable debug plot - show filtered input and CWT output
    % LPF design
    if (SigTypeN > 0) % PPG/BioZ features
        Fpass = 8.0; % Pass band (Hz), this should be the same as selected for BD!
        h = fdesign.lowpass('fp,fst,ap,ast', Fpass, Fpass+3.0, 0.1, 64.0, gendata.debug.Fs); % Fstop = Fpass+3.0Hz, Apass = 0.1dB, Astop = 64dB
        Hd = design(h, 'equiripple', 'MinOrder', 'any', 'StopbandShape', 'linear','StopBandDecay',25); % 25 dB decay over stopband (compensate for differentiation)
        DCresp = sum(Hd.Numerator); % Response at DC, overall filter should have a response of 1.000 at DC.
        CoefLen = length(Hd.Numerator);
        LPFgrpdel = grpdelay(Hd); LPFgrpdel = LPFgrpdel(1);
        InputLPF = filter(Hd.Numerator/DCresp,1,[ones(1,CoefLen)*gendata.debug.InputSamples(1) gendata.debug.InputSamples ones(1,CoefLen)*gendata.debug.InputSamples(end)]); % Prevent boundary conditions
        InputLPF = InputLPF(CoefLen+1:end-CoefLen); % Crop flanks
    end
    
    InMean = nanmean2(gendata.debug.InputSamples);
    figure; hold on;
    plot(gendata.debug.TimeInputSamples,gendata.debug.InputSamples - InMean,'b'); % Zero mean input samples
    plot(gendata.debug.TimeInputSamples(1:length(gendata.debug.FindPeaks_CWToutSamples)) - gendata.debug.GroupDelay/gendata.debug.Fs,gendata.debug.FindPeaks_CWToutSamples,'k'); % CWT output
    if (SigTypeN > 0) % PPG/BioZ features
        plot(gendata.debug.TimeInputSamples - LPFgrpdel/gendata.debug.Fs,InputLPF - InMean,'m'); % Simulated LPF output
        plot(gendata.upstroke,gendata.debug.usamp - InMean,'or');
        plot(gendata.foot,    gendata.debug.ftamp - InMean,'vr');
        plot(gendata.pripeak, gendata.debug.ppamp - InMean,'^r');
        plot(gendata.secpeak, gendata.debug.spamp - InMean,'^k');
        plot(gendata.dicrnot, gendata.debug.dnamp - InMean,'vk');
        legend('Input','CWTout','LPFsim','UpStroke','Foot','PriPk','SecPk','DicrNo');
    else
        plot(gendata.rpeak,gendata.debug.usamp - InMean,'or');
        legend('Input','CWTout','Rpeak');
    end
    title('Simple beat detection debug output'); xlabel('Absolute time (s)');
    grid on;
end

if (0) % Enable simple debug plot - samples
    figure; hold on;
    plot(gendata.debug.TimeInputSamples*gendata.debug.Fs,gendata.debug.InputSamples,'b');
    if (SigTypeN > 0) % PPG/BioZ features
        plot(gendata.upstroke*gendata.debug.Fs,gendata.debug.usamp,'or');
        plot(gendata.foot*gendata.debug.Fs,gendata.debug.ftamp,'vr');
        plot(gendata.pripeak*gendata.debug.Fs,gendata.debug.ppamp,'^r');
        plot(gendata.secpeak*gendata.debug.Fs,gendata.debug.spamp,'^k');
        plot(gendata.dicrnot*gendata.debug.Fs,gendata.debug.dnamp,'vk');
        legend('Input','UpStroke','Foot','PriPk','SecPk','DicrNo');
    else
        plot(gendata.rpeak*gendata.debug.Fs,gendata.debug.usamp,'or');
    end
    title('Simple beat detection debug output'); xlabel('Absolute time (samples)');
    grid on;
end

if (0) % Enable general debug plot
    LenFP = length(gendata.debug.FindPeaks_InputSamples);
    xx = (gendata.debug.TimeInputSamples(1:LenFP) - gendata.debug.Time0)*gendata.debug.Fs;
    figure; hold on;
    plot((gendata.debug.TimeInputSamples - gendata.debug.Time0)*gendata.debug.Fs + gendata.debug.GroupDelay , gendata.debug.InputSamples-nanmean2(gendata.debug.InputSamples), 'k'); % Shift input to align with filter delay
    if (SigTypeN > 0) % PPG/BioZ features
        plot(gendata.upstroke*gendata.debug.Fs + gendata.debug.GroupDelay - gendata.debug.Time0*gendata.debug.Fs,gendata.debug.usamp-nanmean2(gendata.debug.InputSamples),'or'); % and beat location markers.
    else
        plot(gendata.rpeak*gendata.debug.Fs + gendata.debug.GroupDelay - gendata.debug.Time0*gendata.debug.Fs,gendata.debug.usamp-nanmean2(gendata.debug.InputSamples),'or');
    end
    plot(xx, gendata.debug.FindPeaks_InputSamples, 'b');
    plot(xx, gendata.debug.FindPeaks_threshold, 'r--');
    plot(xx, gendata.debug.FindPeaks_cmp, 'r');
    plot(xx, gendata.debug.FindPeaks_max, 'm');
    plot(xx, gendata.debug.FindPeaks_InputSamples./(gendata.debug.FindPeaks_edge ==  1), '^b');
    plot(xx, gendata.debug.FindPeaks_InputSamples./(gendata.debug.FindPeaks_edge == -1), 'vb');
    % plot(xx, gendata.debug.FindPeaks_holdoffcounter/10, '*k');
    % plot(xx, gendata.debug.FindPeaks_maxindx/10, 'ob');
    
    legend('Input','Beat','CLcwt','Thres','Cmp','Max','Trig','Timeout');
    title('Beat detection debug output'); xlabel('Relative time (samples)');
    grid on;
end

if (0) % Enable PWA debug plot
    if (SigTypeN > 0) % PPG/BioZ features
        figure;hold on;grid on;
        plot(gendata.secpeak,gendata.spftamp,'b');
        plot(gendata.dicrnot,gendata.dnftamp,'r');
        plot(gendata.pripeak,gendata.ppftamp,'g');
        title('PPG feature amplitudes');legend('Foot to SecPk','Foot to DicrNo','Foot to PriPk');xlabel('Absolute time (s)');
        
        figure;grid on;
        plot(gendata.upstroke,gendata.upstgrad,'b');
        title('PPG feature US gradient');xlabel('Absolute time (s)');
        
        figure;hold on;grid on;
        plot(gendata.upstroke,1000*gendata.decaytime,'b');
        plot(gendata.upstroke,1000*gendata.risetime,'r');
        plot(gendata.upstroke,1000*gendata.RCtime,'g');
        title('PPG relative timing features');legend('Decay time','Rise time','RC time');xlabel('Absolute time (s)');ylabel('Time (ms)');        
    end
end

end

% Get over stat license problem
function m = nanmean2(v)
m = mean(v(~isnan(v)));
end

