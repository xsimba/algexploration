function Filt4to13Hz(InputSamples)
% Remove low frequency components from InputSamples
% Limit bandwidth of resulting signal using 10 different filters, each having the same group delay.
% Plot different bandwidth signals with a vertical offset to see the morphology changes.

Fpass = [ 4.000  5.000  6.000  7.000  8.000  9.000 10.000 11.000 12.000 13.000]; % Pass bands (Hz)
Fstop = [ 7.100  8.050  9.000 10.000 11.000 12.000 13.000 14.000 15.000 16.000]; % Stop bands (Hz)
Apass = [ 0.100  0.100  0.100  0.100  0.090  0.085  0.078  0.076  0.074  0.073]; % Pass band ripple (dB)
Astop = [65.460 65.070 64.000 64.000 63.000 63.000 62.000 62.000 62.000 62.000]; % Stop band attenuation (dB)

Fs      = 128; % Sample frequency (Hz)
CropSam = 300; % Number of samples to crop from end of filtered data to eliminate discontinuity effects.

gd = zeros(10,1);
Coef = zeros(10,127);

InputSamples = reshape(InputSamples,[],1); % Make into a coloumn vector

% LPF design
for ff = 1:10
    h = fdesign.lowpass('fp,fst,ap,ast', Fpass(ff), Fstop(ff), Apass(ff), Astop(ff), Fs);
    Hd = design(h, 'equiripple', 'MinOrder', 'any', 'StopbandShape', 'linear','StopBandDecay',25); % 25 dB decay over stopband (compensate for differentiation)
    Coef(ff,:) = Hd.Numerator;
    q = grpdelay(Hd);
    gd(ff) = q(1);
    disp(['Filter ',num2str(Fpass(ff),2),' Hz group delay  = ',num2str(gd(ff)),' samples.']);
end


% HPF design
h = fdesign.highpass('fst,fp,ast,ap', 0.250, 0.500, 50.00, 0.100, Fs); % Pass band 0.5 Hz (30 bpm)
HdSOS = design(h, 'ellip','MatchExactly', 'both');


% High and low pass filter and plot data
InputHPF = filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,InputSamples); % Remove low frequency components (HPF)
sd = std(InputHPF); % Get approximate scale of data
vstep = sd/10; % Vertical step between filter outputs for better visualisation

units = [1 2 5]; % Choose appropriate step size (1, 2 or 5 * 10^n)
q = log10(vstep./units);
[~,i] = min(abs(q-round(q)));
vhstep = units(i)*10^round(q(i));
disp(['HPF and LPF step size = ',num2str(vstep),' -> ',num2str(vhstep)]);

% Define frequencies to be plotted (separate plots), 14 = no LPF
fr = 4:14; % All frequencies
%fr = [7 8 14]; % Specific frequencies
%fr = [4 7 8 11 14]; % Specific frequencies
%fr = [7 8]; % Specific frequencies

% Define frequencies to be plotted (sub plots), 14 = no LPF
%frs = 4:14; % All frequencies
%frs = [7 8 14]; % Specific frequencies
%frs = [4 7 8 11 14]; % Specific frequencies
frs = [7 8]; % Specific frequencies

xx = (0:length(InputHPF)-CropSam-1)/Fs;
n = 0;
leg = {}; % Empty legend (cell array)
figure;hold all;
for ff = fr
    if (ff == 14), InputFilt = [zeros(gd(1),1); InputHPF(1:end-gd(1))]; leg = [leg 'None']; %#ok<AGROW>
    else           InputFilt = filter(Coef(ff-3,:),1,InputHPF);         leg = [leg [num2str(ff),'Hz']]; %#ok<AGROW> % Coef(1:10,:) for frequencies 4 to 13 Hz
    end
    plot(xx,InputFilt(1:end-CropSam)+n*vhstep);
    n = n + 1;
end
legend(leg);
title('HPF and LPF data'); xlabel('Time (s)');
pp1 = gcf;


% Differentiate and low pass filter data and plot
InputDiff = filter([1 0 -1],1,[InputHPF(1); InputHPF(1); InputHPF]); % Differentiate
InputDiff = InputDiff(3:end); % Crop to eliminate first 3 samples (include 1 sample delay from diff function)
sd = std(InputDiff); % Get approximate scale of data
vstep = sd/5; % Vertical step between filter outputs for better visualisation

units = [1 2 5]; % Choose appropriate step size (1, 2 or 5 * 10^n)
q = log10(vstep./units);
[~,i] = min(abs(q-round(q)));
vdstep = units(i)*10^round(q(i));
disp(['Differentiated and LPF step size = ',num2str(vstep),' -> ',num2str(vdstep)]);

xx = (0:length(InputDiff)-CropSam-1)/Fs;
n = 0;
leg = {}; % Empty legend (cell array)
figure;hold all;
for ff = fr
    if (ff == 14), InputFilt = [zeros(gd(1),1); InputDiff(1:end-gd(1))]; leg = [leg 'None']; %#ok<AGROW>
    else           InputFilt = filter(Coef(ff-3,:),1,InputDiff);         leg = [leg [num2str(ff),'Hz']]; %#ok<AGROW>
    end
    plot(xx,InputFilt(1:end-CropSam)+n*vdstep);
    n = n + 1;
end
grid on; legend(leg); title('HPF, differentiated and LPF'); xlabel('Time (s)');
pp2 = gcf;

linkaxes([gca(pp1) gca(pp2)],'x');


% Subplots
vhstep = 0; % Override step settings if desired
vdstep = 0;
figure;
sp1 = subplot(2,1,1);
xx = (0:length(InputHPF)-CropSam-1)/Fs;
n = 0;
leg = {}; % Empty legend (cell array)
hold all;
for ff = frs
    if (ff == 14), InputFilt = [zeros(gd(1),1); InputHPF(1:end-gd(1))]; leg = [leg 'None']; %#ok<AGROW>
    else           InputFilt = filter(Coef(ff-3,:),1,InputHPF);         leg = [leg [num2str(ff),'Hz']]; %#ok<AGROW> % Coef(1:10,:) for frequencies 4 to 13 Hz
    end
    plot(xx,InputFilt(1:end-CropSam)+n*vhstep);
    n = n + 1;
end
grid on; legend(leg); ylabel('HPF and LPF');

sp2 = subplot(2,1,2);
xx = (0:length(InputDiff)-CropSam-1)/Fs;
n = 0;
leg = {}; % Empty legend (cell array)
hold all;
for ff = frs
    if (ff == 14), InputFilt = [zeros(gd(1),1); InputDiff(1:end-gd(1))]; leg = [leg 'None']; %#ok<AGROW>
    else           InputFilt = filter(Coef(ff-3,:),1,InputDiff);         leg = [leg [num2str(ff),'Hz']]; %#ok<AGROW>
    end
    plot(xx,InputFilt(1:end-CropSam)+n*vdstep);
    n = n + 1;
end
grid on; legend(leg); ylabel('HPF, differentiated and LPF'); xlabel('Time (s)');

linkaxes([sp1 sp2],'x');


