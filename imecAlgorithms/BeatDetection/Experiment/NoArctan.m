% Test script to show elimination of arctan function for frequency estimation
% Alex Young 29 JAN 2015

clc
clear all
close all

% Test 1
fs = 128; % Sample frequency
Tdur = 10.0; % Test duration
t = (0:fs*Tdur-1)/fs; % Time series
fr = 1.200; % Test frequency (Hz)
p = 1.0 + 2*pi*fr*t; % Initial phase and frequency to phase conversion

g = sin(p); % Quadrature data
f = cos(p);

figure;hold all;
plot(t,g,'b');plot(t,f,'r');
legend('g(t)','f(t)');

% ff = diff(arctan(g(x),f(x)),x) = (g'(x)*f(x) - g(x)*f'(x)) / (f(x)^2 + g(x)^2)
% Numerical differentiation approximation: g'(x) -> (g(n+1)-g(n-1))/2
% ff = (((g(n+1)-g(n-1))*f(n) - g(n)*(f(n+1)-f(n-1))) / (2*(f(n)^2 + g(n)^2))
u = ((g(3:end)-g(1:end-2)).*f(2:end-1)-g(2:end-1).*(f(3:end)-f(1:end-2)))./(2*(f(2:end-1).^2+g(2:end-1).^2));
ff = u*fs/(2*pi);
figure;plot(t(2:end-1),ff);xlabel('Time (s)');ylabel('Frequency (Hz)');
mf = mean(ff);
disp(['Test frequency = ',num2str(fr),', estimated frequency = ',num2str(mf)]);


% Test 2
fs = 128; % Sample frequency
Tdur = 100.0; % Test duration
t = (0:fs*Tdur-1)/fs; % Time series
fr1 = 0.500; % Test frequency start (Hz)
fr2 = 4.500; % Test frequency end (Hz)
fr = fr1 + (fr2-fr1)*t/Tdur; % Frequency sweep

% Frequency sweep: fr = fr1 + (fr2-fr1)*t/10
% Phase = integral of fr*2*pi -> fr1*2*pi*t + (fr2-fr1)*t^2*2*pi/20

p = 2.3 + fr1*2*pi*t + (fr2-fr1)*t.^2*2*pi/(2*Tdur); % Initial phase and frequency to phase conversion

g = sin(p); % Quadrature data
f = cos(p);

figure;hold on;
plot(t,g,'b');plot(t,f,'r');
legend('g(t)','f(t)');

% ff = diff(arctan(g(x),f(x)),x) = (g'(x)*f(x) - g(x)*f'(x)) / (f(x)^2 + g(x)^2)
% Numerical differentiation approximation: g'(x) -> (g(n+1)-g(n-1))/2
% ff = (((g(n+1)-g(n-1))*f(n) - g(n)*(f(n+1)-f(n-1))) / (2*(f(n)^2 + g(n)^2))
u = ((g(3:end)-g(1:end-2)).*f(2:end-1)-g(2:end-1).*(f(3:end)-f(1:end-2)))./(2*(f(2:end-1).^2+g(2:end-1).^2));
ff = u*fs/(2*pi);
figure;grid on;plot(t(2:end-1),ff);xlabel('Time (s)');ylabel('Frequency (Hz)');

figure;hold on;grid on;
plot(t,fr,'k');plot(t(2:end-1),ff,'b');plot(t(2:end-1),10*(fr(2:end-1)-ff),'r');
legend('Sweep','Estimate','10x err');xlabel('Time (s)');ylabel('Frequency (Hz)');

figure;hold on;grid on;
plot(t,fr*60,'k');plot(t(2:end-1),ff*60,'b');plot(t(2:end-1),10*(fr(2:end-1)-ff)*60,'r');
legend('Sweep','Estimate','10x err');xlabel('Time (s)');ylabel('Frequency (bpm)');
