% --------------------------------------------------------------------------------
%  Project     : SIMBA
%  Filename    : PPGView.m
%  Content     : This script initialises the global variables to support feature plots for ECG and PPG signals.
%  Originator  : RL Inaki            07-2008
%  Modified by : Bernard Grundlehner 12-2008
%                A.M. Tautan         03-2014
%  Copyright  : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

%                1    2     3        4   5                     6    7        8                     9             10               11               12
function PPGView(ppg, dppg, ppg_Tus, fs, UncertaintyRegionPPG, ecg, ecg_Trp, UncertaintyRegionECG, feature_foot, feature_pripeak, feature_dicrnot, feature_secpeak)

default_fs = 128;
if ((nargin < 4) || (isempty(fs))), fs = default_fs;           end
if (nargin < 5),                    UncertaintyRegionPPG = []; end
if (nargin < 7),                    ecg = []; ecg_Trp = [];    end
if (nargin < 8),                    UncertaintyRegionECG = []; end
if (nargin < 9),                    feature_foot = [];         end
if (nargin < 10),                   feature_pripeak = [];      end
if (nargin < 11),                   feature_dicrnot = [];      end
if (nargin < 12),                   feature_secpeak = [];      end

TimeSpan = 5;  % Time duration for each waveform plot (there are 3)
StartTime = 0; % Start time

h0 = figure('NumberTitle','off',...
      'Visible','on',...
      'Color',[0.7 0.7 0.7],...
      'PaperPosition',[5 10 576 432],...
      'PaperUnits','points', ...
      'Position',[10 80 1750 950],...
      'MenuBar','none');

globvar = struct('ppg',ppg,'dppg',dppg,'feature',ppg_Tus,'fs',fs,'tini',StartTime,...
    'UncertaintyRegionPPG', UncertaintyRegionPPG,...
    'ecg',ecg,'qrs', ecg_Trp, 'UncertaintyRegionECG', UncertaintyRegionECG, ...
    'feature_foot',feature_foot,'feature_pripeak',feature_pripeak,'feature_dicrnot',feature_dicrnot,'feature_secpeak',feature_secpeak,...
    'rememberUncertainty', 0,'time_resolution',TimeSpan,...
    'display_dppg',0,'display_ecg',0,'display_all',0);

set(gcf,'Userdata',globvar);

EditPPG2;