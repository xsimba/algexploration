% Analysis of annotation

close all
clear all
clc

% Cell array: File location and name
% DataLoc = {'\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_d_Alex.mat'   ;... % File 1
%            '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_d_Fabian.mat' ;... % File 2
%            '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_d_Eva.mat'    ;... % File 3
%            '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_d_Roberto.mat';... % File 4
%            '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_d_Evelien.mat';... % File 5
%            '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_d_BD.mat'     };   % File 6
       
DataLoc = {'\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_e_Fabian.mat' ;... % File 1
           '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_e_Eva.mat'    ;... % File 2
           '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_e_Roberto.mat';... % File 3
           '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_e_Evelien.mat';... % File 4
           '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation_e_BD.mat'     };   % File 5

Nfiles = size(DataLoc,1);

%% Part 1: parsing of each annotation file

% todo: handle uncertainty regions

% Time limits
FTtoUSmaxTime = 0.150; % 0.100; % Maximum limit for foot         to upstroke       time
UStoPPmaxTime = 0.250; % 0.500; % Maximum limit for upstroke     to primary peak   time
UStoDNmaxTime = 0.500; % 1.000; % Maximum limit for upstroke     to dicrotic notch time
PPtoDNmaxTime = 0.350; % 0.500; % Maximum limit for primary peak to dicrotic notch time
UStoSPmaxTime = 0.750; % 1.200; % Maximum limit for upstroke     to secondary peak time
PPtoSPmaxTime = 0.600; % 0.750; % Maximum limit for primary peak to secondary peak time

dbmess = false; % Debug messages
ShortFileName = cell(1, Nfiles);
hp = zeros(1,Nfiles); % Plot handles
figure;
for ff = 1:Nfiles
    FileName = char(DataLoc(ff,1));
    
    disp(['Processing file: ' FileName]);
    
    Idx = find(FileName=='\',1,'last'); % Search for '\' as it will cause a warning if used in a text expression
    if isempty(Idx), ShortName = FileName(1:end-4); % Strip off '.mat' from end
    else             ShortName = FileName(Idx+1:end-4); % Strip off up to '\' and '.mat' from end
    end
    ShortFileName{ff} = ShortName(max(length(ShortName)-15,1):end); % Short file name, 15 Characters maximum
    
    load(FileName);    % Load file (creates structure 'globvar')

    LenPPG = length(globvar.ppg);
    ts = (0:LenPPG-1)/globvar.fs;
    
    ScaleFactor = 0.5 * rms(globvar.ppg)/rms(globvar.dppg); % Scaling for dPPG
    
    % Plot signal and timing points
    hp(ff) = subplot(Nfiles,1,ff); hold on;
    plot(ts, globvar.ppg,'b');
    plot(ts, globvar.dppg*ScaleFactor,'k');
    plot(globvar.feature,         interp1(ts, globvar.ppg, globvar.feature,        'pchip', 0), 'or'); % Extrapolate using fixed zero value
    plot(globvar.feature_foot,    interp1(ts, globvar.ppg, globvar.feature_foot,   'pchip', 0), 'vk');
    plot(globvar.feature_pripeak, interp1(ts, globvar.ppg, globvar.feature_pripeak,'pchip', 0), '^k');
    plot(globvar.feature_dicrnot, interp1(ts, globvar.ppg, globvar.feature_dicrnot,'pchip', 0), 'vb');
    plot(globvar.feature_secpeak, interp1(ts, globvar.ppg, globvar.feature_secpeak,'pchip', 0), '^b');
    if (ff == 1) % Only for first subplot
        legend('PPG','dPPG','UpStroke','Foot','PriPeak','DicrNot','SecPeak');
        title('Annotation data');
    end
    ylabel(ShortFileName(ff),'Interpreter','none'); % Formatting characters will not be acted upon
    if (ff == Nfiles) % Only for last subplot
        xlabel('Relative time (s)');
    end
    grid on;
    
    % Group features together, based on upstroke
    % Valid sequence: F/US/PP/DN/SP, any feature(s) can be missing
    
    % Upstroke
    FeatUS = globvar.feature;
    FeatUS = FeatUS(~isnan(FeatUS)); % Remove NaNs
    ur = interp1(ts, globvar.UncertaintyVectorPPG,FeatUS,'nearest',0); % Uncertainty for each feature time
    FeatUS = FeatUS(~ur); % Remove Uncertainty region(s)
    LenUS1 = length(FeatUS);
    anno(ff).upstroke = FeatUS; %#ok<SAGROW>
    
    % Foot
    FeatFT = globvar.feature_foot;
    FeatFT = FeatFT(~isnan(FeatFT)); % Remove NaNs
    ur = interp1(ts, globvar.UncertaintyVectorPPG,FeatFT,'nearest',0); % Uncertainty for each feature time
    FeatFT = FeatFT(~ur); % Remove Uncertainty region(s)
    anno(ff).foot = NaN(1, LenUS1); %#ok<SAGROW> % Preallocate output with NaNs
    for n = 1:LenUS1
        q = max(FeatUS(n) - FeatFT, 0); % Time between foot and upstroke
        q(q==0) = NaN; % Set zero values to NaN
        [val, idx] = min(q);
        if (~isempty(idx))
            if (val > FTtoUSmaxTime)
                if dbmess, disp(['Nearest foot at ',num2str(FeatFT(idx)),' s for up stroke at ',num2str(FeatUS(n)),' s is too far away (> ',num2str(FTtoUSmaxTime*1000),' ms).']); end
            else
                anno(ff).foot(n) = FeatFT(idx); %#ok<SAGROW>
                FeatFT(idx) = NaN; % Flag as assigned
            end
        else
            if dbmess, disp(['No associated foot for up stroke at ',num2str(FeatUS(n)),' seconds.']); end
        end
    end
    NotAssigned = sum(~isnan(FeatFT));
    disp(['Total number foot unassigned: ',num2str(NotAssigned)]);
    disp(['Total number of US with unassigned foot: ',num2str(sum(isnan(anno(ff).foot)))]);
    
    % pripeak
    FeatPP = globvar.feature_pripeak;
    FeatPP = FeatPP(~isnan(FeatPP)); % Remove NaNs
    ur = interp1(ts, globvar.UncertaintyVectorPPG,FeatPP,'nearest',0); % Uncertainty for each feature time
    FeatPP = FeatPP(~ur); % Remove Uncertainty region(s)
    anno(ff).pripeak = NaN(1, LenUS1); %#ok<SAGROW> % Preallocate output with NaNs
    for n = 1:LenUS1
        q = max(FeatPP - FeatUS(n), 0); % Time between upstroke and primary peak
        q(q==0) = NaN; % Set zero values to NaN
        [val, idx] = min(q);
        if (~isempty(idx))
            if (val > UStoPPmaxTime)
                if dbmess, disp(['Nearest primary peak at ',num2str(FeatPP(idx)),' s for up stroke at ',num2str(FeatUS(n)),' s is too far away (> ',num2str(UStoPPmaxTime*1000),' ms).']); end
            else
                anno(ff).pripeak(n) = FeatPP(idx); %#ok<SAGROW>
                FeatPP(idx) = NaN; % Flag as assigned
            end
        else
            if dbmess, disp(['No associated primary peak for up stroke at ',num2str(FeatUS(n)),' seconds.']); end
        end
    end
    NotAssigned = sum(~isnan(FeatPP));
    disp(['Total number primary peak unassigned: ',num2str(NotAssigned)]);
    disp(['Total number of US with unassigned primary peak: ',num2str(sum(isnan(anno(ff).pripeak)))]);
    
    % dicrnot
    FeatDN = globvar.feature_dicrnot;
    FeatDN = FeatDN(~isnan(FeatDN)); % Remove NaNs
    ur = interp1(ts, globvar.UncertaintyVectorPPG,FeatDN,'nearest',0); % Uncertainty for each feature time
    FeatDN = FeatDN(~ur); % Remove Uncertainty region(s)
    anno(ff).dicrnot = NaN(1, LenUS1); %#ok<SAGROW> % Preallocate output with NaNs
    for n = 1:LenUS1
        q = max(FeatDN - FeatUS(n), 0); % Time between upstroke and dicrotic notch
        q(q==0) = NaN; % Set zero values to NaN
        [val, idx] = min(q);
        if (~isempty(idx))
            if (val > UStoDNmaxTime)
                if dbmess, disp(['Nearest dicrotic notch at ',num2str(FeatDN(idx)),' s for up stroke at ',num2str(FeatUS(n)),' s is too far away (> ',num2str(UStoDNmaxTime*1000),' ms).']); end
            else
                PPtoDNtime = FeatDN(idx) - anno(ff).pripeak;
                if (PPtoDNtime < 0)
                    if dbmess, disp(['Nearest dicrotic notch at ',num2str(FeatDN(idx)),' s is before primary peak at ',num2str(anno(ff).pripeak),' s']); end
                else
                    if (PPtoDNtime > PPtoDNmaxTime)
                        if dbmess, disp(['Nearest dicrotic notch at ',num2str(FeatDN(idx)),' s for primary peak at ',num2str(anno(ff).pripeak),' s is too far away (> ',num2str(PPtoDNmaxTime*1000),' ms).']); end
                    else
                        anno(ff).dicrnot(n) = FeatDN(idx);
                        FeatDN(idx) = NaN; % Flag as assigned
                    end
                end
            end
        else
            if dbmess, disp(['No associated dicrotic notch for up stroke at ',num2str(FeatUS(n)),' seconds.']); end
        end
    end
    NotAssigned = sum(~isnan(FeatDN));
    disp(['Total number dicrotic notch unassigned: ',num2str(NotAssigned)]);
    disp(['Total number of US with unassigned dicrotic notch: ',num2str(sum(isnan(anno(ff).dicrnot)))]);
    
    % secpeak
    FeatSP = globvar.feature_secpeak;
    FeatSP = FeatSP(~isnan(FeatSP)); % Remove NaNs
    ur = interp1(ts, globvar.UncertaintyVectorPPG,FeatSP,'nearest',0); % Uncertainty for each feature time
    FeatSP = FeatSP(~ur); % Remove Uncertainty region(s)
    anno(ff).secpeak = NaN(1, LenUS1); %#ok<SAGROW> % Preallocate output with NaNs
    for n = 1:LenUS1
        q = max(FeatSP - FeatUS(n), 0); % Time between upstroke and secondary peak
        q(q==0) = NaN; % Set zero values to NaN
        [val, idx] = min(q);
        if (~isempty(idx))
            if (val > UStoSPmaxTime)
                if dbmess, disp(['Nearest secondary peak at ',num2str(FeatSP(idx)),' s for up stroke at ',num2str(FeatUS(n)),' s is too far away (> ',num2str(UStoDNmaxTime*1000),' ms).']); end
            else
                PPtoSPtime = FeatSP(idx) - anno(ff).pripeak;
                DNtoSPtime = FeatSP(idx) - anno(ff).dicrnot;
                if (DNtoSPtime <0)
                    if dbmess, disp(['Nearest secondary peak at ',num2str(FeatSP(idx)),' s is before dicrotic notch at ',num2str(anno(ff).dicrnot),' s']); end
                else
                    if (PPtoSPtime < 0)
                        if dbmess, disp(['Nearest secondary peak at ',num2str(FeatSP(idx)),' s is before primary peak at ',num2str(anno(ff).pripeak),' s']); end
                    else
                        if (PPtoSPtime > PPtoSPmaxTime)
                            if dbmess, disp(['Nearest secondary peak at ',num2str(FeatSP(idx)),' s for primary peak at ',num2str(anno(ff).pripeak),' s is too far away (> ',num2str(PPtoSPmaxTime*1000),' ms).']); end
                        else
                            anno(ff).secpeak(n) = FeatSP(idx); %#ok<SAGROW>
                            FeatSP(idx) = NaN; % Flag as assigned
                        end
                    end
                end
            end
        else
            if dbmess, disp(['No associated secondary peak for up stroke at ',num2str(FeatUS(n)),' seconds.']); end
        end
    end
    NotAssigned = sum(~isnan(FeatSP));
    disp(['Total number secondary peak unassigned: ',num2str(NotAssigned)]);
    disp(['Total number of US with unassigned secondary peak: ',num2str(sum(isnan(anno(ff).secpeak)))]);
    
    disp(' ');
end

linkaxes(hp,'xy');

%% Part 2 comparisson of annotations

UStoUSmaxTime = 0.040; % Maximum allowable time between upstrokes in different files

% In the first instance we will compare 2 files
f1 = 1;
f2 = 2;

% Find corresponding upstrokes
LenUS1 = length(anno(f1).upstroke);
LenUS2 = length(anno(f2).upstroke);

AllocUS1 = zeros(1,LenUS1);
AllocUS2 = zeros(1,LenUS2);

for n = 1:LenUS1
    [val, idx] = min(abs(anno(f2).upstroke - anno(f1).upstroke(n))); % Look for minimum differences
    if (val <= UStoUSmaxTime)
        AllocUS1(n) = idx; % Link US1 to US2
        AllocUS2(idx) = n; % Link US2 to US1
    end
end

% Metrics
% 1. Missing US and time deviation of matches
% 2. Missing foot and time deviation of matches
% 3. Missing primary peak and time deviation of matches
% 4. Missing dicrotic notch and time deviation of matches
% 5. Missing secondary peak and time deviation of matches

% upstroke
US1missingRatio = sum(AllocUS1==0)/LenUS1;
US2missingRatio = sum(AllocUS2==0)/LenUS2;
us1 = anno(f1).upstroke(AllocUS1>0);
us2 = anno(f2).upstroke(AllocUS1);
dus = us1-us2;
us_std = std(dus);
us_mean = mean(dus);
[us_max_val, idx_max] = max(abs(dus));
us_max_time = us1(idx_max);
disp(['Missing upstrokes - file 1: ',num2str(round(US1missingRatio*1000)/10),'%  file 2: ',num2str(round(US2missingRatio*1000)/10),...
     '%  Differences - std: ',num2str(round(us_std*10000)/10),' ms  mean: ',num2str(round(us_mean*10000)/10),' ms  max: ',...
     num2str(round(us_max_val*10000)/10),' ms at time ',num2str(round(us_max_time*1000)/1000),' s.']);

% foot
ft1 = anno(f1).foot(AllocUS1>0);
ft2 = anno(f2).foot(AllocUS1);
FT1missingRatio = sum(isnan(ft1))/length(ft1);
FT2missingRatio = sum(isnan(ft2))/length(ft2);
dft = ft1-ft2;
dftn = dft(~isnan(dft)); % Version without NaNs
ft_std = std(dftn);
ft_mean = mean(dftn);
[ft_max_val, idx_max] = max(abs(dft));
ft_max_time = ft1(idx_max);
disp(['Missing foot - file 1: ',num2str(round(FT1missingRatio*1000)/10),'%  file 2: ',num2str(round(FT2missingRatio*1000)/10),...
     '%  Differences - std: ',num2str(round(ft_std*10000)/10),' ms  mean: ',num2str(round(ft_mean*10000)/10),' ms  max: ',...
     num2str(round(ft_max_val*10000)/10),' ms at time ',num2str(round(ft_max_time*1000)/1000),' s.']);

% primary peak
pp1 = anno(f1).pripeak(AllocUS1>0);
pp2 = anno(f2).pripeak(AllocUS1);
PP1missingRatio = sum(isnan(pp1))/length(pp1);
PP2missingRatio = sum(isnan(pp2))/length(pp2);
dpp = pp1-pp2;
dppn = dpp(~isnan(dpp)); % Version without NaNs
pp_std = std(dppn);
pp_mean = mean(dppn);
[pp_max_val, idx_max] = max(abs(dpp));
pp_max_time = pp1(idx_max);
disp(['Missing primary peak - file 1: ',num2str(round(PP1missingRatio*1000)/10),'%  file 2: ',num2str(round(PP2missingRatio*1000)/10),...
     '%  Differences - std: ',num2str(round(pp_std*10000)/10),' ms  mean: ',num2str(round(pp_mean*10000)/10),' ms  max: ',...
     num2str(round(pp_max_val*10000)/10),' ms at time ',num2str(round(pp_max_time*1000)/1000),' s.']);

% dicrotic notch
dn1 = anno(f1).dicrnot(AllocUS1>0);
dn2 = anno(f2).dicrnot(AllocUS1);
DN1missingRatio = sum(isnan(dn1))/length(dn1);
DN2missingRatio = sum(isnan(dn2))/length(dn2);
ddn = dn1-dn2;
ddnn = ddn(~isnan(ddn)); % Version without NaNs
dn_std = std(ddnn);
dn_mean = mean(ddnn);
[dn_max_val, idx_max] = max(abs(ddn));
dn_max_time = dn1(idx_max);
disp(['Missing dicrotic notch - file 1: ',num2str(round(DN1missingRatio*1000)/10),'%  file 2: ',num2str(round(DN2missingRatio*1000)/10),...
     '%  Differences - std: ',num2str(round(dn_std*10000)/10),' ms  mean: ',num2str(round(dn_mean*10000)/10),' ms  max: ',...
     num2str(round(dn_max_val*10000)/10),' ms at time ',num2str(round(dn_max_time*1000)/1000),' s.']);

% secondary peak
sp1 = anno(f1).secpeak(AllocUS1>0);
sp2 = anno(f2).secpeak(AllocUS1);
SP1missingRatio = sum(isnan(sp1))/length(sp1);
SP2missingRatio = sum(isnan(sp2))/length(sp2);
dsp = sp1-sp2;
dspn = dsp(~isnan(dsp)); % Version without NaNs
sp_std = std(dspn);
sp_mean = mean(dspn);
[sp_max_val, idx_max] = max(abs(dsp));
sp_max_time = sp1(idx_max);
disp(['Missing secondary peak - file 1: ',num2str(round(SP1missingRatio*1000)/10),'%  file 2: ',num2str(round(SP2missingRatio*1000)/10),...
     '%  Differences - std: ',num2str(round(sp_std*10000)/10),' ms  mean: ',num2str(round(sp_mean*10000)/10),' ms  max: ',...
     num2str(round(sp_max_val*10000)/10),' ms at time ',num2str(round(sp_max_time*1000)/1000),' s.']);


%% Output processing
% If a feature is within FeatMaxTime then the feature times will be averaged, otherwise the feature will not be included.
FeatMaxTime = 0.015; % 15 ms

% Use globvar from last file, unchanged: ppg, dppg, fs, tini, ecg, qrs, rememberUncertainty, time_resolution, display_dppg, display_ecg, display_all, UncertaintyRegionECG, UncertaintyVectorECG

% Clear PPG Uncertainty regions as these have been used to mask features
globvar.UncertaintyRegionPPG = [];
globvar.UncertaintyVectorPPG = zeros(1, LenPPG);

globvar.feature         = [];
globvar.feature_foot    = [];
globvar.feature_pripeak = [];
globvar.feature_dicrnot = [];
globvar.feature_secpeak = [];

% Culling and consistency
disp(' ');
disp(['Annotation consistency check for files ', char(ShortFileName(f1)),' and ',char(ShortFileName(f2))]);
cus = 0;
for n = 1:length(dus)
    if (dus(n) <= FeatMaxTime)
        cus = cus + 1;
        globvar.feature(cus) = (us1(n)+us2(n))/2; % Average US time
        globvar.feature_foot(cus) = NaN;
        globvar.feature_pripeak(cus) = NaN;
        globvar.feature_dicrnot(cus) = NaN;
        globvar.feature_secpeak(cus) = NaN;
        if (dft(n) <= FeatMaxTime)
            globvar.feature_foot(cus) = (ft1(n)+ft2(n))/2; % Average FT time
        else
            if (~isnan(ft1(n)) || ~isnan(ft2(n))), disp(['Foot deleted at ',num2str(ft1(n)),'/',num2str(ft2(n)),' s for upstroke at ', num2str(us1(n)), ' s as annotators disagree.']); end
        end
        if (dpp(n) <= FeatMaxTime)
            globvar.feature_pripeak(cus) = (pp1(n)+pp2(n))/2; % Average PP time
        else
            if (~isnan(pp1(n)) || ~isnan(pp2(n))), disp(['Primary peak deleted at ',num2str(pp1(n)),'/',num2str(pp2(n)),' s for upstroke at ', num2str(us1(n)), ' s as annotators disagree.']); end
        end
        if (ddn(n) <= FeatMaxTime)
            globvar.feature_dicrnot(cus) = (dn1(n)+dn2(n))/2; % Average DN time
        else
            if (~isnan(dn1(n)) || ~isnan(dn2(n))), disp(['Dicrotic notch deleted at ',num2str(dn1(n)),'/',num2str(dn2(n)),' s for upstroke at ', num2str(us1(n)), ' s as annotators disagree.']); end
        end
        if (dsp(n) <= FeatMaxTime)
            globvar.feature_secpeak(cus) = (sp1(n)+sp2(n))/2; % Average SP time
        else
            if (~isnan(sp1(n)) || ~isnan(sp2(n))), disp(['Secondary peak deleted at ',num2str(sp1(n)),'/',num2str(sp2(n)),' s for upstroke at ', num2str(us1(n)), ' s as annotators disagree.']); end
        end
    else
        disp(['Upstroke deleted at ', num2str(us1(n)),'/',num2str(us2(n)), ' s as annotators disagree.']);
    end
end

ExpFileName = inputdlg('Please enter file name for output (.mat will be appended)');
if ~isempty(char(ExpFileName)) % When either cancel or no text entered, don't save
    ExpFileName = [char(ExpFileName) '.mat']; % Convert to string and append .mat
    save(ExpFileName, 'globvar');
    disp(['Output saved in ' ExpFileName]);
else
    disp('Save output file cancelled.');
end

