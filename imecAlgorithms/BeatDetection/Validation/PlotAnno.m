function PlotAnno(globvar, Ytext)
%PlotAnno Plot single annotation output
% globvar is expected to contain the following fields:
%   .ppg .dppg .fs .feature .feature_foot .feature_pripeak .feature_dicrnot .feature_secpeak
% Ytext is a text string for the Y axis.

% Example use:
%   load('test_out_e.mat'); % Get globvar
%   PlotAnno(globvar, 'File: test_out_e.mat');

LenPPG = length(globvar.ppg);
ts = (0:LenPPG-1)/globvar.fs;

ScaleFactor = 0.5 * rms(globvar.ppg)/rms(globvar.dppg); % Scaling for dPPG

% Plot signal and timing points
figure; hold on;
plot(ts, globvar.ppg,'b');
plot(ts, globvar.dppg*ScaleFactor,'k');
plot(globvar.feature,         interp1(ts, globvar.ppg, globvar.feature,        'pchip', 0), 'or'); % Extrapolate using fixed zero value
plot(globvar.feature_foot,    interp1(ts, globvar.ppg, globvar.feature_foot,   'pchip', 0), 'vk');
plot(globvar.feature_pripeak, interp1(ts, globvar.ppg, globvar.feature_pripeak,'pchip', 0), '^k');
plot(globvar.feature_dicrnot, interp1(ts, globvar.ppg, globvar.feature_dicrnot,'pchip', 0), 'vb');
plot(globvar.feature_secpeak, interp1(ts, globvar.ppg, globvar.feature_secpeak,'pchip', 0), '^b');
legend('PPG','dPPG','UpStroke','Foot','PriPeak','DicrNot','SecPeak');
title('Annotation data');
ylabel(Ytext,'Interpreter','none'); % Formatting characters will not be acted upon
xlabel('Relative time (s)');
grid on;

end
