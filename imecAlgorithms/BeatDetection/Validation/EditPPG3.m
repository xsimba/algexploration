% --------------------------------------------------------------------------------
%  Project     : SIMBA
%  Filename    : EditPPG3.m
%  Content     : Draws the HR, waveforms and features for PPG and ECG in the main window
%  Originator  : RL Inaki            07-2008
%  Modified by : Bernard Grundlehner 12-2008
%                A.M. Tautan         03-2014
%                A. Young            06-2015
%  Copyright  : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function EditPPG3

%% Initiatilization
globvar = get(gcf,'UserData');
ppg = globvar.ppg;
dppg = globvar.dppg;
display_dppg = globvar.display_dppg;
display_ecg = globvar.display_ecg;
display_all = globvar.display_all;

feature         = globvar.feature;
feature_foot    = globvar.feature_foot;
feature_pripeak = globvar.feature_pripeak;
feature_dicrnot = globvar.feature_dicrnot;
feature_secpeak = globvar.feature_secpeak;

time_resolution = globvar.time_resolution;

qrs = globvar.qrs;
ecg = globvar.ecg;

fs = globvar.fs;
tini = globvar.tini; % Start time specified in PPGView.m

UncertaintyRegionPPG = globvar.UncertaintyRegionPPG;
UncertaintyRegionECG = globvar.UncertaintyRegionECG;
if ~isfield(globvar, 'UncertaintyVectorPPG'), globvar.UncertaintyVectorPPG = CreateUncertaintyVector(UncertaintyRegionPPG, fs, length(ppg)); end
if ~isfield(globvar, 'UncertaintyVectorECG'), globvar.UncertaintyVectorECG = CreateUncertaintyVector(UncertaintyRegionECG, fs, length(ecg)); end

clf;
tfeature = 0;

set(gcf, 'UserData', globvar);
warning('off', 'MATLAB:interp1:NaNinY');

%% Enable Buttons
if tini <= 1/globvar.fs, enPB = 'off';
else                     enPB = 'on';
end;

if (length(ppg)/fs)-3*time_resolution <= tini, enNB = 'off';
else                                           enNB = 'on';
end;

%% Main Program
if length(ppg) >= 60*fs
    
    % HR plot for ECG and PPG
    h0 = axes('Parent',gcf, ...
        'Box','on', ...
        'CameraUpVector',[0 1 0], ...
        'Color',[1 1 1], ...
        'Position',[0.03 1-(2*.17) 0.96 0.27], ...
        'Tag','Axes1', ...
        'FontSize',7,...
        'XGrid','on', ...
        'YGrid','on');
    
    [hr_ecg, t] = HeartRate(globvar.qrs, globvar.UncertaintyRegionECG);
    h_hr_ecg = plotUncertain(t, hr_ecg,  globvar.UncertaintyRegionECG, {'LineWidth', 2,'Color',[0.7,0.7,0.7]}, tini);
        
    [hr_ppg, t] = HeartRate(globvar.feature, globvar.UncertaintyRegionPPG);
    h_hr_ppg = plotUncertain(t, hr_ppg,  globvar.UncertaintyRegionPPG, {'LineWidth', 2}, tini);
    
    ylim([25 250]); % Restrict to reasonable HR range
    ylabel('Heart rate (bpm)');
    legend('ECG','Region','PPG','Region');
    set(h_hr_ecg, 'DisplayName', 'ECG Heart Rate');
    set(h_hr_ppg, 'DisplayName', 'PPG Heart Rate');
    
    % Signal plots
    for i = 4:6,
        tfin = tini + time_resolution - 1/fs;
        dfin = round(tfin*fs);
        dini = max(1, round(tini*fs));
        dat = zeros(time_resolution*fs, 1);
        
        if display_dppg, diff_dat = zeros(time_resolution*fs, 1); end
        if display_ecg,  dat_ecg  = zeros(time_resolution*fs, 1); end
        
        if (dfin > 0)
            if length(ppg) > dfin
                dat(end-(dfin-dini):end) = ppg(dini:dfin);
                if display_dppg, diff_dat(end-(dfin-dini):end) = dppg(dini:dfin); end
                if display_ecg,   dat_ecg(end-(dfin-dini):end) =  ecg(dini:dfin); end
            elseif dini < length(ppg)
            dat = [ppg(dini:end); zeros(dfin - length(ppg), 1)];
                if display_dppg, diff_dat = [dppg(dini:end); zeros(dfin - length(dppg), 1)]; end
                if display_ecg,   dat_ecg = [ ecg(dini:end); zeros(dfin - length(ecg),  1)]; end
            end
        end
        
        UncertaintyVectorPPG = globvar.UncertaintyVectorPPG;
        tUncertain_ppg = UncertaintyVectorPPG(max(1, dini):min(end,dfin));
        
        if display_ecg
            UncertaintyVectorECG = globvar.UncertaintyVectorECG;
            tUncertain_ecg = UncertaintyVectorECG(max(1, dini):min(end,dfin));
        end
        
        t = (fs*tini:round(fs*tfin))/fs;
        
        % feature (upstroke)
        indfeature = find(feature>=tini & feature<tfin);
        if (indfeature>0), sfeature = feature(indfeature(1):indfeature(end));
        else               sfeature = [];
        end
        
        % feature foot
        indfeature = find(feature_foot>=tini & feature_foot<tfin);
        if (indfeature>0), sfeature_foot = feature_foot(indfeature(1):indfeature(end));
        else               sfeature_foot = [];
        end
        
        % feature pripeak
        indfeature = find(feature_pripeak>=tini & feature_pripeak<tfin);
        if (indfeature>0), sfeature_pripeak = feature_pripeak(indfeature(1):indfeature(end));
        else               sfeature_pripeak = [];
        end
        
        % feature dicrnot
        indfeature = find(feature_dicrnot>=tini & feature_dicrnot<tfin);
        if (indfeature>0), sfeature_dicrnot = feature_dicrnot(indfeature(1):indfeature(end));
        else               sfeature_dicrnot = [];
        end
        
        % feature secpeak
        indfeature = find(feature_secpeak>=tini & feature_secpeak<tfin);
        if (indfeature>0), sfeature_secpeak = feature_secpeak(indfeature(1):indfeature(end));
        else               sfeature_secpeak = [];
        end
        
        
        
        % feature (upstroke)
        if ~isempty(sfeature)
            if sfeature(1)<tini
                k1 = find(sfeature<tini);
                sfeature = sfeature(k1(end)+1:end);
            end
            if sfeature(end)>tfin
                k2 = find(sfeature>tfin);
                sfeature = sfeature(1:k2(1)-1);
            end
        end
        
        % feature foot
        if ~isempty(sfeature_foot)
            if sfeature_foot(1)<tini
                k1 = find(sfeature_foot<tini);
                sfeature_foot = sfeature_foot(k1(end)+1:end);
            end
            
            if sfeature_foot(end)>tfin
                k2 = find(sfeature_foot>tfin);
                sfeature_foot = sfeature_foot(1:k2(1)-1);
            end
        end
        
        % feature pripeak
        if ~isempty(sfeature_pripeak)
            if sfeature_pripeak(1)<tini
                k1 = find(sfeature_pripeak<tini);
                sfeature_pripeak = sfeature_pripeak(k1(length(k1))+1:end);
            end
            if sfeature_pripeak(end)>tfin
                k2 = find(sfeature_pripeak>tfin);
                sfeature_pripeak = sfeature_pripeak(1:k2(1)-1);
            end
        end
        
        % feature dicrnot
        if ~isempty(sfeature_dicrnot)
            if sfeature_dicrnot(1)<tini
                k1 = find(sfeature_dicrnot<tini);
                sfeature_dicrnot = sfeature_dicrnot(k1(length(k1))+1:end);
            end
            if sfeature_dicrnot(end)>tfin
                k2 = find(sfeature_dicrnot>tfin);
                sfeature_dicrnot = sfeature_dicrnot(1:k2(1)-1);
            end
        end
        
        % feature secpeak
        if ~isempty(sfeature_secpeak)
            if sfeature_secpeak(1)<tini
                k1 = find(sfeature_secpeak<tini);
                sfeature_secpeak = sfeature_secpeak(k1(length(k1))+1:end);
            end
            if sfeature_secpeak(end)>tfin
                k2 = find(sfeature_secpeak>tfin);
                sfeature_secpeak = sfeature_secpeak(1:k2(1)-1);
            end
        end
        
        h1 = axes('Parent',gcf, ...
            'Box','on', ...
            'CameraUpVector',[0 1 0], ...
            'Color',[1 1 1], ...
            'Position',[0.03 1-((i-1)*.18) 0.96 0.15], ...
            'Tag','Axes1', ...
            'FontSize',7,...
            'XGrid','on', ...
            'YGrid','on');

        hold on;
        
        h = plot(t,dat,'LineWidth',2); % ppg
        set(h, 'DisplayName', 'ppg')
        
        if display_dppg
            scale_diff_dat = min([max([dat; 0]) -min([dat; 0])])/max(abs(diff_dat));
            hd2 = plot(t,scale_diff_dat*diff_dat,'Color','k','LineWidth',1.5);
            set(hd2, 'DisplayName', 'dppg');
        end
        
        if display_ecg
            scale_ecg_dat = min([max([dat; 0]) -min([dat; 0])])/max(abs(dat_ecg));
            sc_dat_ecg = scale_ecg_dat*dat_ecg;
            hd3 = plot(t,sc_dat_ecg,'Color',[0.5 0.5 0.5],'LineWidth',1.5);
            set(hd3, 'DisplayName', 'ecg');
        end
        
        tmp = find(tUncertain_ppg);
        if ~isempty(tmp)
        tmp2 = find(diff(tmp)>1);
            StartOnlyFlag = 0;
            if isempty(tmp2)
                xu = t(tmp); % Plot coordinates
                yu = dat(tmp)';
                if (length(tmp) == 1), StartOnlyFlag = 1; end
            else
                xu = t(tmp(1:tmp2(1)));  % Plot coordinates
                yu = dat(tmp(1:tmp2(1)))';
            for n = 2:length(tmp2)
                    xu = [xu NaN t(tmp(tmp2(n-1)+1:tmp2(n)))]; %#ok<AGROW> % Augment plot coordinates (use Nan as a separator)
                    yu = [yu NaN dat(tmp(tmp2(n-1)+1:tmp2(n)))']; %#ok<AGROW>
                end
                if isempty(n), n = 1; end % When length(tmp2) < 2
                xu = [xu NaN t(tmp(tmp2(n)+1:end))];  %#ok<AGROW> % Augment plot coordinates (use Nan as a separator)
                yu = [yu NaN dat(tmp(tmp2(n)+1:end))']; %#ok<AGROW>
                if (tmp2(n)+1 == length(tmp)), StartOnlyFlag = 1; end
            end
            if (StartOnlyFlag == 0), hd4 = plot(xu, yu, 'm', 'linewidth',3);
            else                     hd4 = plot(xu, yu, 'om');
            end
            set(hd4, 'DisplayName', 'UncerPPG');
        end
        
        if display_ecg
            tmp = find(tUncertain_ecg);
            if ~isempty(tmp)
            tmp2 = find(diff(tmp)>1);
                if isempty(tmp2)
                    xu = t(tmp);  % Plot coordinates
                    yu = sc_dat_ecg(tmp);
                else
                    xu = t(tmp(1:tmp2(1)));   % Plot coordinates
                    yu = sc_dat_ecg(tmp(1:tmp2(1)));
                for n = 2:length(tmp2)
                        xu = [xu NaN t(tmp(tmp2(n-1)+1:tmp2(n)))];  %#ok<AGROW> % Augment plot coordinates (use Nan as a separator)
                        yu = [yu NaN sc_dat_ecg(tmp(tmp2(n-1)+1:tmp2(n)))]; %#ok<AGROW>
                    end
                    if isempty(n), n = 1; end % When length(tmp2) < 2
                    xu = [xu NaN t(tmp(tmp2(n)+1:end))]; %#ok<AGROW>
                    yu = [yu NaN sc_dat_ecg(tmp(tmp2(n)+1:end))]; %#ok<AGROW>
                end
                hd5 = plot(xu, yu, 'k');
                set(hd5, 'DisplayName', 'UncerECG');
            end
        end
        
        % feature (upstroke)
        if ~isempty(sfeature)
            hfu = stem(sfeature,ones([1,length(sfeature)])*min(dat),'b.');
            set(hfu, 'DisplayName', 'upstroke', 'BaseValue', max(dat));
        end
  
        % feature foot
        if ~isempty(sfeature_foot)
            h3 = stem(sfeature_foot,ones([1,length(sfeature_foot)])*min(dat),'m.');
            set(h3, 'DisplayName', 'foot', 'BaseValue', max(dat));
        end
        
        % feature pripeak
        if ~isempty(sfeature_pripeak)
            h4 = stem(sfeature_pripeak,ones([1,length(sfeature_pripeak)])*min(dat),'g.');
            set(h4, 'DisplayName', 'pripeak', 'BaseValue', max(dat));
        end        
        
        % feature dicrnot
        if ~isempty(sfeature_dicrnot)
            h5 = stem(sfeature_dicrnot,ones([1,length(sfeature_dicrnot)])*min(dat),'c.');
            set(h5, 'DisplayName', 'dicrnot', 'BaseValue', max(dat));
        end        
        
        % feature secpeak
        if ~isempty(sfeature_secpeak)
            h6 = stem(sfeature_secpeak,ones([1,length(sfeature_secpeak)])*min(dat),'.--','color',[1 0.5 0]); % Orange, make dashed in case dicrotic notch is underneath.
            set(h6, 'DisplayName', 'secpeak', 'BaseValue', max(dat));
        end        
        
        if (i == 5), legend('show'); end  % Legend just for middle part
   
        hold off;
        %set(h1,'XTick',t(1):1:t(end));                % Don't include end time
        set(h1,'XTick',tini:1:tini + time_resolution); % Include end time
        grid on;

        axmin = min(dat)*1.02; % 2% margins
        axmax = max(dat)*1.02;
        if (axmin == axmax), axmax = axmax + 1; end
        %axis([t(1) t(end) axmin axmax]);                % Don't include end time
        axis([tini tini + time_resolution axmin axmax]); % Include end time
        
        tini = tfin + 1/fs;
        tfeature = tfeature + length(sfeature)*2;
        
        ylabel(['Signals part ',num2str(i-3)]);
        
    end % for i = 4:6
end

dcmObject = datacursormode;
try
    set(get(get(dcmObject, 'uicontextmenu'), 'children'), 'visible', 'off')
catch
end

% Make context dependent

set(dcmObject, 'enable', 'on', 'Updatefcn', @enablebuttons)
cmenu = uicontextmenu;
item1 = uimenu(cmenu, 'Label', 'Remove Feature', 'Callback', @removebeat);
item2 = uimenu(cmenu, 'Label', 'Add Upstroke', 'Callback', @addbeat);
item3 = uimenu(cmenu, 'Label', 'Add Foot', 'Callback', @addfoot);
item4 = uimenu(cmenu, 'Label', 'Add PriPeak', 'Callback', @addpripeak);
item5 = uimenu(cmenu, 'Label', 'Add DicrNot', 'Callback', @adddicrnot);
item6 = uimenu(cmenu, 'Label', 'Add SecPeak', 'Callback', @addsecpeak);
if length(UncertaintyRegionPPG) > 1 && sum(diff(UncertaintyRegionPPG')==0) > 0
    item8 = uimenu(cmenu, 'Label', 'End of period with noise or lost packets', 'Callback', {@StartUncertainPeriod,2});
else
    item8 = uimenu(cmenu, 'Label', 'Start/removal of period with noise or lost packets', 'Callback', {@StartUncertainPeriod,1});
end
item9 = uimenu(cmenu, 'Label', 'Jump To...', 'Callback', @JumpTo);

set(dcmObject, 'uicontextmenu', cmenu)

setappdata(gcf, 'dcmObject', dcmObject)
set(findobj(gcf,'type','figure'), 'toolbar','figure');

%% Buttons and Beats per minute

Hm_1=uicontrol(gcf,'Style','push',...
    'Enable',enNB,...
    'Position',[900 12 150 50],...
    'String','Next',...
    'FontSize',20,...
    'CallBack',['globvar=get(gcf,''UserData'');','globvar.tini=globvar.tini+(3*globvar.time_resolution);','set(gcf,''Userdata'',globvar);','EditPPG3;']);

Hm_2=uicontrol(gcf,'Style','push',...
    'Enable',enPB,...
    'Position',[730 12 150 50],...
    'String','Previous',...
    'FontSize',20,...
    'CallBack',['globvar=get(gcf,''UserData'');','globvar.tini=globvar.tini-(3*globvar.time_resolution);','set(gcf,''Userdata'',globvar);','EditPPG3;']);

Hm_3=uicontrol(gcf,'Style','push',...
    'Enable','off',...
    'Position',[560 12 150 50],...
    'String','Remove',...
    'FontSize',20,...
    'Tag', 'pbRemBeat',...
    'CallBack',@removebeat);

Hm_4=uicontrol(gcf,'Style','push',...
    'Enable','off',...
    'Position',[390 12 150 50],...
    'String','Add beat',...
    'FontSize',20,...
    'Tag', 'pbAddBeat',...
    'CallBack',@addbeat);

Hm_5=uicontrol(gcf,'Style','push',...
    'Enable','on',...
    'Position',[220 12 150 50],...
    'String','Export',...
    'FontSize',20,...
    'CallBack',@export);



Hm_7=uicontrol(gcf,'Style','Radio',...
    'Enable','on',...
    'Tag','Derivative',...
    'Value',display_dppg,...
    'Position',[1070 12 150 50],...
    'String','Derivative',...
    'FontSize',14,...
    'CallBack',@derivative);

Hm_8=uicontrol(gcf,'Style','Radio',...
    'Enable','on',...
    'Tag','Display_ECG',...
    'Value',display_ecg,...
    'Position',[1480 12 150 50],...
    'String','Display ECG',...
    'FontSize',14,...
    'CallBack',@disp_ecg);

Hm_9=uicontrol(gcf,'Style','Radio',...
    'Enable','on',...
    'Tag','Features',...
    'Value',display_all,...
    'Position',[1260 12 200 50],...
    'String','Features',...
    'FontSize',14,...
    'CallBack',['globvar=get(gcf,''UserData'');','globvar.display_all=0;','set(gcf,''Userdata'',globvar);','EditPPG2;']);


itime = datestr(datenum(num2str(round(tini/60)), 'MM'), 'HH:MM');

setappdata(gcf,'htime',uicontrol(gcf,'style','edit',...
    'units','points',...
    'position',[25 12 65 26],...
    'BackgroundColor',[1 1 1],...
    'string',datestr(itime,'HH:MM'),...
    'Enable','on',...
    'FontSize',20,...
    'CallBack',@jumptime));

thtime=uicontrol('style', 'text','units','points',...
    'BackgroundColor',[0.1 0.7 1],'position',[25 38 65 18],'string','Time (HH:MM)','FontSize',13);

ibeat=uicontrol('style', 'edit','units','points','position',[100 12 40 26], ...
    'BackgroundColor',[1 1 1],'string',tfeature,'Enable','off','FontSize',20);

tibeat=uicontrol('style', 'text','units','points',...
    'BackgroundColor',[0.1 0.7 1],'position',[100 38 40 18],'string','BPM','FontSize',13);

%%
function outtxt = enablebuttons(obj,event_obj)
dcm = getappdata(gcf, 'dcmObject');
c_info = get(event_obj);
DispName = get(c_info.Target, 'DisplayName');
handles = guihandles(gcf);
if strcmp(DispName, 'ppg')
    set(handles.pbRemBeat, 'enable', 'off')
    set(handles.pbAddBeat, 'enable', 'on')
    outtxt = {'ppg'};
elseif strcmp(DispName, 'upstroke')
    set(handles.pbRemBeat, 'enable', 'on')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'upstroke'};
elseif strcmp(DispName, 'foot')
    set(handles.pbRemBeat, 'enable', 'on')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'foot'};
elseif strcmp(DispName, 'pripeak')
    set(handles.pbRemBeat, 'enable', 'on')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'pripeak'};
elseif strcmp(DispName, 'dicrnot')
    set(handles.pbRemBeat, 'enable', 'on')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'dicrnot'};
elseif strcmp(DispName, 'secpeak')
    set(handles.pbRemBeat, 'enable', 'on')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'secpeak'};
elseif strcmp(DispName, 'dppg')
    set(handles.pbRemBeat, 'enable', 'off')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'derivative'};
elseif strcmp(DispName, 'UncerPPG')
    set(handles.pbRemBeat, 'enable', 'off')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'UncertaintyPPG'};
elseif strcmp(DispName, 'UncerECG')
    set(handles.pbRemBeat, 'enable', 'off')
    set(handles.pbAddBeat, 'enable', 'off')
    outtxt = {'UncertaintyECG'};
else
    set(handles.pbRemBeat, 'enable', 'off')
    set(handles.pbAddBeat, 'enable', 'off')
    %outtxt = {['--- (' DispName ')']};
    outtxt = DispName;
end

%%
function addbeat(obj,event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
if strcmp(cur_txt, 'ppg') || strcmp(cur_txt, 'dppg')
    globvar = get(gcf,'UserData');
    feature = globvar.feature;
    time = c_info.Position(1);
    idx = find(feature<time,1,'last') + 1;
    feature_new = [feature(1:idx-1) time feature(idx:end)];
    globvar.feature = feature_new;
    set(gcf, 'UserData', globvar);
    EditPPG3
end

function addfoot(obj,event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
if strcmp(cur_txt, 'ppg') || strcmp(cur_txt, 'dppg')
    globvar = get(gcf,'UserData');
    feature = globvar.feature_foot;
    time = c_info.Position(1);
    idx = find(feature<time,1,'last') + 1;
    feature_new = [feature(1:idx-1) time feature(idx:end)];
    globvar.feature_foot = feature_new;
    set(gcf, 'UserData', globvar);
    EditPPG3
end

function addpripeak(obj,event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
if strcmp(cur_txt, 'ppg') || strcmp(cur_txt, 'dppg')
    globvar = get(gcf,'UserData');
    feature = globvar.feature_pripeak;
    time = c_info.Position(1);
    idx = find(feature<time,1,'last') + 1;
    feature_new = [feature(1:idx-1) time feature(idx:end)];
    globvar.feature_pripeak = feature_new;
    set(gcf, 'UserData', globvar);
    EditPPG3
end

function adddicrnot(obj,event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
if strcmp(cur_txt, 'ppg') || strcmp(cur_txt, 'dppg')
    globvar = get(gcf,'UserData');
    feature = globvar.feature_dicrnot;
    time = c_info.Position(1);
    idx = find(feature<time,1,'last') + 1;
    feature_new = [feature(1:idx-1) time feature(idx:end)];
    globvar.feature_dicrnot = feature_new;
    set(gcf, 'UserData', globvar);
    EditPPG3
end

function addsecpeak(obj,event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
if strcmp(cur_txt, 'ppg') || strcmp(cur_txt, 'dppg')
    globvar = get(gcf,'UserData');
    feature = globvar.feature_secpeak;
    time = c_info.Position(1);
    idx = find(feature<time,1,'last') + 1;
    feature_new = [feature(1:idx-1) time feature(idx:end)];
    globvar.feature_secpeak = feature_new;
    set(gcf, 'UserData', globvar);
    EditPPG3
end

%%
function StartUncertainPeriod(obj,event_obj,startstop)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
globvar = get(gcf,'UserData');
if strcmp(cur_txt, 'ppg') || strcmp(cur_txt, 'dppg')|| strcmp(cur_txt, 'UncerPPG')
    UncertaintyRegion = globvar.UncertaintyRegionPPG;
    len = length(globvar.ppg);
    DataType = 1; % PPG
elseif strcmp(cur_txt, 'ecg')
    UncertaintyRegion = globvar.UncertaintyRegionECG;
    len = length(globvar.ecg);
    DataType = 2; % ECG
else
    DataType = 0; % Ignore input
end

if (DataType > 0)
    time = c_info.Position(1);
    if startstop == 1
        UncertaintyVector = CreateUncertaintyVector(UncertaintyRegion, globvar.fs, len);
        if UncertaintyVector(round(time*globvar.fs))
            % then we want to *remove* the uncertainty period!
            Idx = find(UncertaintyRegion(:, 1) < time);
            Idx = Idx(end);
            if size(UncertaintyRegion, 1) == 1
                UncertaintyRegion = [];
            elseif Idx < size(UncertaintyRegion, 1)
                UncertaintyRegion = [UncertaintyRegion(1:Idx-1, :); UncertaintyRegion(Idx+1:end,:)];
            else
                UncertaintyRegion = UncertaintyRegion(1:end-1,:);
            end
        elseif length(UncertaintyRegion) > 1 && (UncertaintyRegion(end, 1) == UncertaintyRegion(end, 2))
            msgbox('Please define the end of the previous uncertainty region first', 'Command cannot be processed')
            return
        else
            UncertaintyRegion(end+1, 1) = time;
            UncertaintyRegion(end, 2) = UncertaintyRegion(end, 1);
        end
    else
        UncertaintyRegion(find(diff(UncertaintyRegion')==0), 2) = time;
    end
    if ~isempty(UncertaintyRegion) &&  UncertaintyRegion(end, 1) ~= UncertaintyRegion(end, 2)
        UncertaintyRegion = sortrows(UncertaintyRegion);
    end
    if (DataType == 1) % PPG
        globvar.UncertaintyRegionPPG = UncertaintyRegion;
        globvar.UncertaintyVectorPPG = CreateUncertaintyVector(UncertaintyRegion, globvar.fs, length(globvar.ppg));
        globvar.UncertaintyRegionPPG = CreateUncertaintyRegion(globvar.UncertaintyVectorPPG, globvar.fs);
    else
        globvar.UncertaintyRegionECG = UncertaintyRegion;
        globvar.UncertaintyVectorECG = CreateUncertaintyVector(UncertaintyRegion, globvar.fs, length(globvar.ecg));
        globvar.UncertaintyRegionECG = CreateUncertaintyRegion(globvar.UncertaintyVectorECG, globvar.fs);
    end
    set(gcf, 'UserData', globvar);
    % assignin('base', 'UncertaintyPeriods', globvar.UncertaintyRegion)
    EditPPG3
end
%%
%function UncertaintyVector = CreateUncertaintyVector(UncertaintyRegion, globvar)
function UncertaintyVector = CreateUncertaintyVector(UncertaintyRegion, fs, len)
%fs = globvar.fs;
%K = length(globvar.ppg);
%UncertaintyVector = zeros(1, K);
UncertaintyVector = zeros(1, len);
for n = 1:size(UncertaintyRegion, 1)
    UncertaintyVector(round(UncertaintyRegion(n, 1)*fs) : round(UncertaintyRegion(n, 2)*fs)) = 1;
end
function UncertaintyRegion = CreateUncertaintyRegion(UncertaintyVector, fs)
UncertaintyVector(1) = 0;
UncertaintyVector(end) = 0;
K = find(diff(UncertaintyVector));
L = length(K);
if L == 0
    UncertaintyRegion = [];
else
    for n = 1:2:L
        UncertaintyRegion((n+1)/2,1) = (K(n)+1)/fs; %#ok<AGROW>
        UncertaintyRegion((n+1)/2,2) = (K(n+1))/fs; %#ok<AGROW>
    end
end

%%
function JumpTo(obj,event_obj,startstop)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
globvar = get(gcf,'UserData');
time_resolution = globvar.time_resolution;
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor

if (strcmp(cur_txt, 'ECG Heart Rate') || strcmp(cur_txt, 'PPG Heart Rate') || strcmp(cur_txt, 'Region')) % HR figure
    Time = c_info.Position(1);
else
    Time = inputdlg('Please enter time in seconds to jump to ');
    if isempty(Time)
        Time = c_info.Position(1);
    else
        Time = str2num(Time{1}); % Convert to number
    end
end

maxtini = floor((length(globvar.ppg)/globvar.fs) - 3*time_resolution); % Maximum value of tini and snap to nearest second towards zero
tini = round(Time - time_resolution); % Snap to nearest second
tini = min(max(0, tini), maxtini); % Apply limits
globvar.tini = tini;
globvar.rememberUncertainty = false;
set(gcf, 'UserData', globvar);
EditPPG2

% if ~strcmp(cur_txt, 'Heart Rate')
%     Time = inputdlg('Please enter time in seconds:', 'Jump To...');
%     if ~isempty(Time)
%         %globvar=get(gcf,'UserData');
%         maxtime = (length(globvar.ppg)/globvar.fs)-60;
%         Time = str2num(Time{1});
%         globvar.tini = max(1/globvar.fs, min(maxtime, Time - 3*time_resolution));
%         globvar.rememberUncertainty = false;
%         set(gcf, 'UserData', globvar);
%         EditPPG2
%     end
% else
%     Time = round(c_info.Position(1)); % Snap to nearest second
%     maxtime=(length(globvar.ppg)/globvar.fs)-60;
%     globvar.tini = max(-3*time_resolution, min(maxtime, (Time - (4.5*time_resolution))));
%     globvar.rememberUncertainty = false;
%     set(gcf, 'UserData', globvar);
%     EditPPG2
% end

%%
function jumptime(obj,event_obj)
globvar=get(gcf,'UserData');
jtime=get(getappdata(gcf,'htime'),'string');
jtime=(datenum(jtime,'HH:MM'));
jtime_hhmm(1) = str2num(datestr(jtime, 'HH'));
jtime_hhmm(2) = str2num(datestr(jtime, 'MM'));
jtime = jtime_hhmm(1)*60*60+jtime_hhmm(2)*60;
maxtime=(length(globvar.ppg)/globvar.fs)-60;
if jtime < maxtime
    globvar.tini=max(1/globvar.fs, jtime-60);
else
    globvar.tini=maxtime;
end
set(gcf, 'UserData', globvar);
EditPPG3
%%
function estimatebeat(obj,event_obj,type,number)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
if ~strcmp(get(c_info.Target, 'DisplayName'), 'Heart Rate')
    globvar=get(gcf,'UserData');
    feature = globvar.feature;
    beattime = c_info.Position(1);
    if type == 1
        tmp = find(feature<beattime);
        MeanRRInterval = mean(diff(feature(tmp(end-4):tmp(end))));
        beattime = feature(tmp(end)) + MeanRRInterval;
    elseif type == 2
        tmp = find(feature>beattime);
        MeanRRInterval = mean(diff(feature(tmp(1):tmp(5))));
        beattime = feature(tmp(1)) - MeanRRInterval;
    elseif type == 3
        tmp = find(feature>beattime);
        MeanRRInterval = (diff(feature(tmp(1)-1:tmp(1))))/2;
        tmp = find(feature<beattime);
        beattime = feature(tmp(end)) + MeanRRInterval;
    elseif type == 4
        %     tmp = find(feature<beattime);
        %     MeanRRInterval = mean(diff(feature(tmp(end)-4:tmp(end))));
        %     for i=1:number
        %         beattime(i) = feature(tmp(end)) + i*MeanRRInterval;
        %     end
        
    end
    
    beatsample = round(beattime*globvar.fs);
    % now, we find the absolute maximum within a few samples around the
    % selected point on the dppg
    % dppg = globvar.dppg;
    % for i=1:number
    %     [maxdummy, maxidx] = max(dppg(beatsample(i)-10:beatsample(i)+10));
    %     [mindummy, minidx] = min(dppg(beatsample(i)-10:beatsample(i)+10));
    %     if ~(maxidx==1) && ~(maxidx == 11) && ((maxdummy > -mindummy) || (minidx==1) || (minidx==11))%max is not at the boundary
    %         beattime(i) = (beatsample(i)-10+maxidx-1)/globvar.fs;
    %     else
    %         beattime(i) = (beatsample(i)-10+minidx-1)/globvar.fs;
    %     end
    % end
    tmp = find(feature<beattime);
    beatidx = length(tmp)+1;
    featurenew = [feature(1:beatidx-1) beattime feature(beatidx+3:end)];
    globvar.feature = featurenew;
    set(gcf, 'UserData', globvar);
    EditPPG3
end
%%
function removebeat(obj,event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
cur_time = c_info.Position(1); % Time at cursor
cur_txt = get(c_info.Target, 'DisplayName'); % Text at cursor
globvar = get(gcf,'UserData');
if strcmp(cur_txt, 'upstroke')
    feature = globvar.feature;
    [~, idx] = min(abs(feature-cur_time));
    feature_new = [feature(1:idx-1) feature(idx+1:end)];
    globvar.feature = feature_new;
elseif strcmp(cur_txt, 'foot')
    feature = globvar.feature_foot;
    [~, idx] = min(abs(feature-cur_time));
    feature_new = [feature(1:idx-1) feature(idx+1:end)];
    globvar.feature_foot = feature_new;
elseif strcmp(cur_txt, 'pripeak')
    feature = globvar.feature_pripeak;
    [~, idx] = min(abs(feature-cur_time));
    feature_new = [feature(1:idx-1) feature(idx+1:end)];
    globvar.feature_pripeak = feature_new;
elseif strcmp(cur_txt, 'dicrnot')
    feature = globvar.feature_dicrnot;
    [~, idx] = min(abs(feature-cur_time));
    feature_new = [feature(1:idx-1) feature(idx+1:end)];
    globvar.feature_dicrnot = feature_new;
elseif strcmp(cur_txt, 'secpeak')
    feature = globvar.feature_secpeak;
    [~, idx] = min(abs(feature-cur_time));
    feature_new = [feature(1:idx-1) feature(idx+1:end)];
    globvar.feature_secpeak = feature_new;
elseif strcmp(cur_txt, 'UncerPPG')
    % Remove this PPG uncertainty region
    % Find region
    % Delete entries
    % Update global
elseif strcmp(cur_txt, 'UncerECG')
    % Remove this ECG uncertainty region
    % Find region
    % Delete entries
    % Update global
end
set(gcf, 'UserData', globvar);
EditPPG3


%%
function export(obj, event_obj)
globvar = get(gcf,'UserData');

ExpFileName = inputdlg('Please enter file name (.mat will be appended)');
ExpFileName = [char(ExpFileName) '.mat']; % Convert to string and append .mat
save(ExpFileName, 'globvar');

% feature = globvar.feature;
% feature_foot    = globvar.feature_foot;
% feature_pripeak = globvar.feature_pripeak;
% feature_dicrnot = globvar.feature_dicrnot;
% feature_secpeak = globvar.feature_secpeak;
% UncertaintyRegion = globvar.UncertaintyRegion;
% 
% varname = inputdlg('Please enter a subject''s name','Export to Workspace');
% if ~isempty(varname)
%     if iscell(varname)
%         featurename        = ['feature_upstroke_' varname{1}];
%         featurefootname    = ['feature_foot_' varname{1}];
%         featurepripeakname = ['feature_pripeak_' varname{1}];
%         featuredicrnotname = ['feature_dicrnot_' varname{1}];
%         featuresecpeakname = ['feature_secpeak_' varname{1}];
%         uncname = ['UncertaintyPeriods_' varname{1}];
%         
%     else
%         featurename        = ['feature_' varname];
%         featurefootname    = ['feature_foot_' varname];
%         featurepripeakname = ['feature_pripeak_' varname];
%         featuredicrnotname = ['feature_dicrnot_' varname];
%         featuresecpeakname = ['feature_secpeak_' varname];
%         uncname = ['UncertaintyPeriods_' varname];
%     end
%     
%     
%     % feature (upstroke)
%     flag = 1;
%     if evalin('base', ['exist(''' featurename ''')'])
%         flag = 0;
%         answer = questdlg([featurename ' already exists, are you sure to overwrite?'], 'Confirmation required', 'Yes', 'No', 'Yes');
%         if strcmp(answer, 'Yes'), flag = 1; end
%     end
%     if flag
%         assignin('base', featurename , feature)
%         disp([featurename ' stored'])
%     end
%     
%     % feature foot
%     flag = 1;
%     if evalin('base', ['exist(''' featurefootname ''')'])
%         flag = 0;
%         answer = questdlg([featurefootname ' already exists, are you sure to overwrite?'], 'Confirmation required', 'Yes', 'No', 'Yes');
%         if strcmp(answer, 'Yes'), flag = 1; end
%     end
%     if flag
%         assignin('base', featurefootname , feature_foot)
%         disp([featurefootname ' stored'])
%     end
%     
%     % feature pripeak
%     flag = 1;
%     if evalin('base', ['exist(''' featurepripeakname ''')'])
%         flag = 0;
%         answer = questdlg([featurepripeakname ' already exists, are you sure to overwrite?'], 'Confirmation required', 'Yes', 'No', 'Yes');
%         if strcmp(answer, 'Yes'), flag = 1; end
%     end
%     if flag
%         assignin('base', featurepripeakname , feature_pripeak)
%         disp([featurepripeakname ' stored'])
%     end
%     
%     % feature dicrnot
%     flag = 1;
%     if evalin('base', ['exist(''' featuredicrnotname ''')'])
%         flag = 0;
%         answer = questdlg([featuredicrnotname ' already exists, are you sure to overwrite?'], 'Confirmation required', 'Yes', 'No', 'Yes');
%         if strcmp(answer, 'Yes'), flag = 1; end
%     end
%     if flag
%         assignin('base', featuredicrnotname , feature_dicrnot)
%         disp([featuredicrnotname ' stored'])
%     end
%     
%     % feature secpeak
%     flag = 1;
%     if evalin('base', ['exist(''' featuresecpeakname ''')'])
%         flag = 0;
%         answer = questdlg([featuresecpeakname ' already exists, are you sure to overwrite?'], 'Confirmation required', 'Yes', 'No', 'Yes');
%         if strcmp(answer, 'Yes'), flag = 1; end
%     end
%     if flag
%         assignin('base', featuresecpeakname , feature_secpeak)
%         disp([featuresecpeakname ' stored'])
%     end
%     
%     % UncertaintyRegion
%     flag = 1;
%     if evalin('base', ['exist(''' uncname ''')'])
%         flag = 0;
%         answer = questdlg([uncname ' already exists, are you sure to overwrite?'], 'Confirmation required', 'Yes', 'No', 'Yes');
%         if strcmp(answer, 'Yes'), flag = 1; end
%     end
%     if flag
%         assignin('base', uncname , UncertaintyRegion)
%         disp([uncname ' stored'])
%     end
% end

%%
function [hrRS, t] = HeartRate(feature_tot, UncertaintyPeriods)

%HEARTRATE  computation of heart rate, taking into account uncertainty
%periods
%    [hr, t] = HeartRate(feature_tot, UncertaintyPeriods)
%
% (c) 2009, by Bernard Grundlehner, IMEC-NL.
%

if nargin == 2 && ~isempty(UncertaintyPeriods)
    feature = feature_tot(find(feature_tot < UncertaintyPeriods(1, 1)));
    [hrRS, t] = computeHR(feature);
    
    for n = 2:size(UncertaintyPeriods, 1)
        feature = feature_tot(find(feature_tot > UncertaintyPeriods(n-1,2) & feature_tot < UncertaintyPeriods(n,1)));
        [hrRStmp, ttmp] = computeHR(feature);
        hrRS = [hrRS, hrRStmp(1:end)];
        t = [t, ttmp(1:end)];
    end
    feature = feature_tot(find(feature_tot > UncertaintyPeriods(end,2)));
    [hrRStmp, ttmp] = computeHR(feature);
    hrRS = [hrRS, hrRStmp(1:end)];
    t = [t, ttmp(1:end)];
else
    [hrRS, t] = computeHR(feature_tot);
end

%%
function [hrRS, tRS] = computeHR(feature)

if length(feature) > 2
    t = 0.5*(feature(1:end-1)+feature(2:end));
    hr = 60./diff(feature);
    tRS = t(1):0.25:t(end);
    hrRS = interp1(t, hr, tRS, 'linear'); %this defines the hr at a 4 Hz timescale
else
    hrRS = [];
    tRS = [];
end

%%
function varargout = plotUncertain(t, x, UncertaintyPeriods, para, tini)
globvar = get(gcf,'UserData');
time_resolution = globvar.time_resolution;
if isempty(UncertaintyPeriods)
    if (nargin < 4), h = plot(t, x);
    else             h = plot(t, x, para{:});
    end
    t_i = t; x_i = x;
else
    t_i = t(1):t(2)-t(1):t(end);
    x_i = interp1(t, x, t_i);
    for n = 1:size(UncertaintyPeriods, 1)
        vec = find(t_i < UncertaintyPeriods(n,2) & t_i > UncertaintyPeriods(n, 1));
        x_i(vec) = NaN; %dirty trick to force plotting 'empty spaces' into one figure
    end
    if (nargin < 4), h = plot(t_i, x_i);
    else             h = plot(t_i, x_i, para{:});
    end
end
hold on
tfin = tini + 3*time_resolution;
plot(t_i(t_i > tini & t_i < tfin), x_i(t_i > tini & t_i < tfin), 'r')
if nargout == 1
    varargout = {h};
end

%%
function derivative (obj, event_obj)
globvar=get(gcf,'UserData');
select = get(obj,'Value');
if select, globvar.display_dppg = 1;
else       globvar.display_dppg = 0;
end
set(gcf, 'UserData', globvar);
EditPPG3;

%%
function disp_ecg (obj, event_obj)
globvar=get(gcf,'UserData');
select = get(obj,'Value');
if select, globvar.display_ecg = 1;
else       globvar.display_ecg = 0;
end
set(gcf, 'UserData', globvar);
EditPPG3;

%%
function reduce(obj, event_obj)
c_info = getCursorInfo(getappdata(gcf, 'dcmObject'));
if strcmp(get(c_info.Target, 'DisplayName'), 'feature')
    globvar=get(gcf,'UserData');
    feature = globvar.feature;
    beattime = c_info.Position(1);
    
    % compute the interval
    tmp = find(feature<beattime);
    MeanBeatInterval = mean(diff(feature(tmp(end-9):tmp(end))));
    interval = 0.85*MeanBeatInterval;
    
    tmp = find(feature>=beattime & feature<(beattime+5));
    
    index = 1;
    while index<length(tmp)-2
        a1 = feature(tmp(index));
        a2 = feature(tmp(index+1));
        d = feature(tmp(index+1))-feature(tmp(index));
        
        if d<interval
            feature = [feature(1:tmp(index)) feature(tmp(index+2):end)] ;
            tmp =[tmp(1:index) tmp(index+2:end)];
        else
            index=index+1;
        end
    end
    
    globvar.feature = feature;
    set(gcf, 'UserData', globvar);
end

EditPPG3;



