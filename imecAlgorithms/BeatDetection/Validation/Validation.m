
close all;
clear all;
clc;

CallBD = true; % Set this flag if beat detection is to be called

% Cell array: File location and name                                                                                                           channel
DataLoc = {'\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation.mat',                                'd';... % Red
           '\\winnl\simbadc\WinNT\Applications\simband annotation data\First_annotation\data_for_annotation.mat',                                     'e';... % Green
           '\\winnl\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model\SP0171\SP0171_S_SI_1.mat', 'e';...
           '\\winnl\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model\SP0171\SP0171_S_SI_2.mat', 'e';...
           '\\winnl\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model\SP0171\SP0171_S_SI_3.mat', 'e';...
           '\\winnl\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model\SP0100\SP0100_S_SI_1.mat', 'e';...
           '\\winnl\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model\SP0100\SP0100_S_SI_2.mat', 'e';...
           '\\winnl\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model\SP0100\SP0100_S_SI_3.mat', 'e'};

Nfiles = size(DataLoc,1);

Fs = 128;
Fsi = 1000; % Interpolator sample frequency (Hz)

% Filter design
hBR = fdesign.highpass('fst,fp,ast,ap', 0.30, 0.45, 50, 0.05, Fs); % PPG baseline removal: Stopband 0 to 0.30Hz, passband from 0.45Hz (27 bpm)
HdSOSbr = design(hBR, 'ellip', 'MatchExactly', 'both', 'SOSScaleNorm', 'Linf');
% fvtool(HdSOSbr, 'Fs',Fs); % Only if you really want to see it..

hHF = fdesign.lowpass('fp,fst,ap,ast', 10, 11, 0.05, 50, Fs); % PPG HF noise reduction: Passband 0 to 8Hz, stopband from 11Hz
HdSOShf = design(hHF, 'ellip', 'MatchExactly', 'both', 'SOSScaleNorm', 'Linf');
% fvtool(HdSOShf, 'Fs',Fs); % Only if you really want to see it..

hECG = fdesign.bandpass('fst1,fp1,fp2,fst2,ast1,ap,ast2', 0.60, 0.70, 30.00, 30.10, 50, 0.1, 50, Fs); % ECG BPF: Stopband 0 to 0.60Hz, passband 0.70 to 30.00Hz, stopband from 30.10Hz
HdSOSecg = design(hECG, 'ellip', 'MatchExactly', 'both', 'SOSScaleNorm', 'Linf');
% fvtool(HdSOSecg, 'Fs',Fs); % Only if you really want to see it..

for ff = 1%1:Nfiles
    FileName = char(DataLoc(ff,1));
    
    Idx = find(FileName=='\',1,'last'); % Search for '\' as it will cause a warning if used in a text expression
    if isempty(Idx), ShortFileName = FileName;
    else             ShortFileName = FileName(Idx+1:end);
    end
    ShortFileName = ShortFileName(max(length(ShortFileName)-20,1):end); % Short file name, 20 Characters maximum
    
    load(FileName);    % Load file (creates structure 'data')
    ChChar = char(DataLoc(ff,2)); % Channel select
    
    AbsStartTime = data.timestamps(1);
    ts  = data.timestamps - AbsStartTime; % Make time relative as this is assumed in the main annotation scripts.
    data.timestamps = ts; % Use these new timestamps
    sig = data.ppg.(ChChar).signal;
    disp(['File start time is ',num2str(AbsStartTime),' seconds. Relative time will be used from this point.']);
    
    figure;
    plot(ts, sig);
    title(['Beat detection input, file ' ShortFileName],'Interpreter','none'); % Formatting characters will not be acted upon
    xlabel('Relative time (s)');
    grid on;
    
    % Interpolation
    tsi = (ceil(ts(1)*Fsi):floor(ts(end)*Fsi))/Fsi; % Timestamps for interpolated signal
    
    % Apply filtering (off line only)
    sigBR = filtfilt(HdSOSbr.sosMatrix,HdSOSbr.ScaleValues,sig);   % Baseline removal
    sigHF = filtfilt(HdSOShf.sosMatrix,HdSOShf.ScaleValues,sigBR); % HF noise removal
    
    % Beat detection
    if CallBD
        cd ../
        data = BDstruc(data, ['ppg.' ChChar]);
        data = BDstruc(data,  'ecg');
        cd Validation
    end
    
    bddata = data.ppg.(ChChar).bd;
    
    % CWT output and timestamps
    sigCW = bddata.debug.FindPeaks_CWToutSamples;
    sigCW(end-ceil(bddata.debug.GroupDelay):end) = 0; % Zero filter tail as it would dominate plot
    tsCW  = bddata.debug.TimeInputSamples(1:length(bddata.debug.FindPeaks_CWToutSamples)) - bddata.debug.GroupDelay/bddata.debug.Fs;
    
    % Interpolate to fine time grid: signals
    sigBRi = interp1(ts,   sigBR, tsi, 'PCHIP'); % Baseline removal
    sigHFi = interp1(ts,   sigHF, tsi, 'PCHIP'); % HF noise removal
    sigCWi = interp1(tsCW, sigCW, tsi, 'PCHIP'); % CWT output
    
    % Interpolate to fine time grid: amplitudes at timing points
    sigCWus = interp1(tsCW, sigCW, bddata.upstroke, 'PCHIP'); % Up-stroke
    sigCWft = interp1(tsCW, sigCW, bddata.foot,     'PCHIP'); % Foot
    sigCWsp = interp1(tsCW, sigCW, bddata.secpeak,  'PCHIP'); % Secondary Peak
    sigCWpp = interp1(tsCW, sigCW, bddata.pripeak,  'PCHIP'); % Primary Peak
    sigCWdn = interp1(tsCW, sigCW, bddata.dicrnot,  'PCHIP'); % Dicrotic Notch
    
    figure; hold on;
    plot(tsi, sigBRi, 'b');
    plot(tsi, sigHFi, 'r');
    plot(tsi, sigCWi, 'k');
    plot(bddata.upstroke, sigCWus,'or');
    plot(bddata.foot,     sigCWft,'vr');
    plot(bddata.pripeak,  sigCWpp,'^r');
    plot(bddata.secpeak,  sigCWsp,'^k');
    plot(bddata.dicrnot,  sigCWdn,'vk');
    legend('BL rem','BL + HF rem','CWTout','UpStroke','Foot','PriPk','SecPk','DicrNo');
    title(['Beat detection debug output, file: ' ShortFileName],'Interpreter','none');
    xlabel('Relative time (s)');
    grid on;
    
    Tupstroke = bddata.upstroke(~isnan(bddata.upstroke));
    Trpeak    = data.ecg.bd.rpeak(~isnan(data.ecg.bd.rpeak));
    ecgFil    = filtfilt(HdSOSecg.sosMatrix,HdSOSecg.ScaleValues,data.ecg.signal); % ECG filtering
    ecgi      = interp1(ts, ecgFil, tsi, 'PCHIP'); % Interpolate ECG
    
    %       ppg,     dppg,    ppg_Tus,   fs,  UncertaintyPPG, ecg,  ecg_Trp, UncertaintyECG, feature_foot, feature_peak,   feature_dicrnot, feature_secpeak
    PPGView(sigHFi', sigCWi', Tupstroke, Fsi, [],             ecgi, Trpeak,  [],             bddata.foot,  bddata.pripeak, bddata.dicrnot,  bddata.secpeak);
    set(gcf,'Name',['Annotation for file: ' ShortFileName]);
end
