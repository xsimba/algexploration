function [InterpolatedIndexList] = QuadInterpolatePeaks(InputSamples, PrevSamples, PrevBlockSize, PeakIndexList)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : QuadInterpolatePeaks.m
% Content   : Matlab function for quadratic peak detection with C code as comments
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples            Block of input samples
% PrevSamples             Block of input samples from previous block
% PrevBlockSize           Number of input samples in previous block
% PeakIndexList           List of detected peaks
% InterpolatedIndexList   List of corrected peaks

AllSamples = [PrevSamples InputSamples]; % Concatenate previous and current input samples so that no boundary processing is required

PrevY = AllSamples(PeakIndexList+PrevBlockSize+1-1);
PeakY = AllSamples(PeakIndexList+PrevBlockSize+1+0);
NextY = AllSamples(PeakIndexList+PrevBlockSize+1+1);

% Given the quadratic y = a*x^2 + b*x + c
% When x = -1, PrevY = a - b + c
% When x =  0, PeakY =         c
% When x = +1, NextY = a + b + c
% So a = (NextY + PrevY - 2*PeakY)/2 and b = (NextY - PrevY)/2
% Gradient is zero when y' = 0 = 2*a*x + b, solving for x = -b/(2*a)
a = (NextY + PrevY - 2*PeakY)*0.5;
b = (NextY - PrevY)*0.5;
f = ((abs(a) < abs(b)) | (a == 0)); % Flag bad values

a(f) = 1;
b(f) = 0;

Correction = -b./(2*a);

InterpolatedIndexList = PeakIndexList + Correction;

end

