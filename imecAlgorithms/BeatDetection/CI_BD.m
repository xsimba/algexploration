% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : CI_BD.m
%  Content    : Confidence indicator for beat detection
%  Package    : SIMBA
%  Created by : A. Young (alex.young@imec-nl.nl)
%  Date       : 23-MAR-2015
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [data] = CI_BD(data, SigID)
% Confidence indicator for beat detection
%   Inputs:
%      data                         :  Structure: timestamps in units of SECONDS
%        .ppg.{a..g}.bd.{features}  :  PPG input from BDstruc
%        .bio.{i|q}.bd.{features}   :  BioZ input from BD
%        .ecg.bd.{rpeak}            :  ECG input from BD
%      SigType                      :  String : 'ecg' | 'ppg.x' | 'bio.y', x = a to h, y = i or q
%
%   Output:
%      data                                          :  Structure appended to:
%        .ppg.{a..g}.bd.ci.{level, timestamp, comment} :  PPG output
%        .bio.{i|q}.bd.ci.{level, timestamp, comment}  :  BioZ output
%        .ecg.bd.ci.{level, timestamp, comment}        :  ECG output
%
%  level     : 1:Bad or no signal
%            : 2:Upstroke/R-peak detection AND plausible beat-to-beat timing
%            : 3:Upstroke and foot detection AND plausible beat-to-beat timing
%            : 4:Upstroke, foot, primary peak, dicrotic notch, secondary peak / R-peak detection AND plausible beat-to-beat timing AND low variance
%
%  timestamps : Issued every update interval and is a multiple of that interval
%
%  comment   : Text comment to justify level attribution
%
%   Usage Examples, after calling BDstruc:
%      data = CI_BD(data, 'ecg');     % ECG input
%      data = CI_BD(data, 'ppg.g');   % PPG input g
%      data = CI_BD(data, 'bio.i');   % BioZ input i

% To do:
% Comment field

% Format and checks
LenSigID = length(SigID);
if ((LenSigID < 3)||(LenSigID > 5)), error('SigID must contain between 3 and 5 characters only: ecg, ppg.{a..g} or bio.{i|q}.'); end

Char = char(SigID);
SigType = cellstr(Char(1:3)); % First 3 characters

% Parameters
Tupdate  =  1.0; % Update interval (seconds)
Thist    =  5.0; % History duration (seconds)
Ttimeout =  2.5; % Timeout duration (seconds)
BeatTimingMin  = 0.200;  % 200 ms
BeatTimingMax  = 2.250;  % 26.67 bpm
BeatTimimngVar = 0.0050; % Ratio of var to mean

% Signal type specific processing for input
if     strcmpi(SigType,'ecg')
    SigTypeN  = 0; % 0:ECG, 1:PPG, 2:BioZ
    SigInfo   = data.ecg;
    
elseif strcmpi(SigType,'ppg')
    SigTypeN  = 1; % 0:ECG, 1:PPG, 2:BioZ
    ChChar    = Char(5);
    if (~isfield(data.ppg,ChChar)), error(['Field data.ppg.',ChChar,' does not exist.']); end
    SigInfo   = data.ppg.(ChChar);
    
elseif strcmpi(SigType,'bio')
    SigTypeN  = 2; % 0:ECG, 1:PPG, 2:BioZ
    ChChar    = Char(5);
    if (~isfield(data.bio,ChChar)), error(['Field data.bio.',ChChar,' does not exist.']); end
    SigInfo   = data.ppg.(ChChar);
    
else
    error('Unknown SigType, expecting ecg, ppg.{a..h} or bio.{i|q}');
end

if (~isfield(SigInfo,'bd')), error('No beat detection output found'); end

% Select pulse source
if (SigTypeN == 0), Tpulse = SigInfo.bd.rpeak;    % ECG: R-peak
else                Tpulse = SigInfo.bd.upstroke; % PPG/BioZ: upstroke
end

% Remove all NaNs from Pulse times
ValidIdx = ~isnan(Tpulse); % Valid indexes, use for all PWA features
Tpulse = Tpulse(ValidIdx);
if (SigTypeN > 0) % PPG/BioZ
    Tusfoot  = Tpulse-SigInfo.bd.foot(ValidIdx);    % Relative time form US to foot
    Tuspripk = Tpulse-SigInfo.bd.pripeak(ValidIdx); % Relative time form US to primary peak
    Tusdicno = Tpulse-SigInfo.bd.dicrnot(ValidIdx); % Relative time form US to dicrotic notch
    Tussecpk = Tpulse-SigInfo.bd.secpeak(ValidIdx); % Relative time form US to secondary peak
    Appft    = SigInfo.bd.ppftamp(ValidIdx);
    Adnft    = SigInfo.bd.dnftamp(ValidIdx);
    Aspft    = SigInfo.bd.spftamp(ValidIdx);
    Gus      = SigInfo.bd.upstgrad(ValidIdx);
    Presence_F_PP = ~(isnan(SigInfo.bd.foot(ValidIdx))|isnan(SigInfo.bd.pripeak(ValidIdx))); % Presence of US, foot & primary peak
    Presence_F_PP_DN_SP = ~((~Presence_F_PP)|isnan(SigInfo.bd.dicrnot(ValidIdx))|isnan(SigInfo.bd.secpeak(ValidIdx))); % Presence of previous + dicrotic notch & secondary peak
end

% Debug plots
if (0)
    MeanPulseTime = (Tpulse(1:end-1)+Tpulse(2:end))/2;
    figure;plot(MeanPulseTime,1000*diff(Tpulse),'-b.');xlabel('Time (s)');ylabel('Beat interval (ms)');title('Debug: pulse timing');grid on;
    figure;plot(MeanPulseTime,60./diff(Tpulse),'-b.');xlabel('Time (s)');ylabel('Instantaneous HR (bpm)');title('Debug: pulse rate');grid on;
    
    if (SigTypeN > 0) % PPG/BioZ
        figure;hold on;
        plot(Tpulse,Tusfoot*1000,'k');
        plot(Tpulse,Tuspripk*1000,'b');
        plot(Tpulse,Tusdicno*1000,'m');
        plot(Tpulse,Tussecpk*1000,'r');
        legend('Foot','PriPk','DicNo','SecPk');xlabel('Time (s)');ylabel('Relative time (ms)');title('Debug: feature timing');grid on;
        
        figure;hold on;
        plot(Tpulse,Appft,'b');
        plot(Tpulse,Adnft,'m');
        plot(Tpulse,Aspft,'r');
        legend('PriPk','DicNo','SecPk');xlabel('Time (s)');ylabel('Amplitude');title('Debug: feature amplitudes');grid on;
        
        figure;
        plot(Tpulse,Gus,'b');
        xlabel('Time (s)');ylabel('Gradient');title('Debug: Upstroke gradient');grid on;
    end
end

% Codes
% CI=1: bad or no signal (timeout after e.g. 2.5 seconds)
% CI=2: Upstroke / R-peak detection AND plausible beat-to-beat timing
% CI=3: Upstroke, foot and primary peak detection AND plausible beat-to-beat timing
% CI=4: Upstroke, foot, primary peak, dicrotic notch, secondary peak / R-peak detection AND plausible beat-to-beat timing AND low variance


if ~isempty(Tpulse) %can be empty in case of a flatline
    Tstart = ceil((Tpulse(1)+Thist)/Tupdate)*Tupdate; % Start time, from when there is enough history
    Tend   = floor(Tpulse(end)/Tupdate)*Tupdate; % End time, to last update time when there is still data
    if (Tend >= Tstart) % Check there will be any output
        CItimestamps = Tstart:Tupdate:Tend;
        CIcode = ones(1,length(CItimestamps)); % 1 by default
        for n = 1:length(CItimestamps)
            IdxStart = find(Tpulse>(CItimestamps(n)-Thist),1,'first');
            IdxEnd   = find(Tpulse>(CItimestamps(n)),1,'first') - 1; % Find first time past timestamps then go back one
            if (IdxEnd > IdxStart) % Make sure we have at least 2 values for diff
                b2btiming = diff(Tpulse(IdxStart:IdxEnd));
                if ((min(b2btiming) >= BeatTimingMin)&&(max(b2btiming) <= BeatTimingMax))
                    CIcode(n) = 2; % Only pulse and b2b timimg
                    if (SigTypeN > 0) % PPG/BioZ
                        if (min(Presence_F_PP(IdxStart:IdxEnd)) == 1)
                            CIcode(n) = 3; % With foot and primary peak
                            RatioVM = var(b2btiming)/mean(b2btiming);
                            if ((min(Presence_F_PP_DN_SP(IdxStart:IdxEnd)) == 1)&&(RatioVM < BeatTimimngVar))
                                CIcode(n) = 4; % With dicrotic notch, secondary peak and variance check
                            end
                        end
                    else
                        RatioVM = var(b2btiming)/mean(b2btiming);
                        if (RatioVM < BeatTimimngVar)
                            CIcode(n) = 4; % With variance check
                        end
                    end
                end
            end
        end
    else
        CItimestamps = Tstart;
        CIcode = 0; % 0 to indicate no valid data
    end
else
    CItimestamps = 0;
    CIcode = 0; % 0 to indicate no valid data
end

if (0)
    figure;plot(CItimestamps, CIcode);title('CI code output');xlabel('Time (s)');ylabel('Ci code');
end

% Signal type specific processing for output
if     (SigTypeN == 0) % ECG
    data.ecg.bd.ci.level     = CIcode;
    data.ecg.bd.ci.timestamps = CItimestamps;
    
elseif (SigTypeN == 1) % PPG
    data.ppg.(ChChar).bd.ci.level     = CIcode;
    data.ppg.(ChChar).bd.ci.timestamps = CItimestamps;
    
elseif (SigTypeN == 2) % BioZ
    data.bio.(ChChar).bd.ci.level     = CIcode;
    data.bio.(ChChar).bd.ci.timestamps = CItimestamps;
    
else
    error('Unknown SigType, expecting ecg, ppg or bio');
end


