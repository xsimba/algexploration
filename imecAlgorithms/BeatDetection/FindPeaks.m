function [FoundPeaks, PeakIndici, State] = FindPeaks(InputSamples, BlockSize, State, Params)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : FindPeaks.m
% Content   : Matlab function for peak detection
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters

PeakIndici = []; % For each call of BeatDetector, we start with an empty list

%Debug
if (1)
    State.Debug_FindPeaks_InputSamples    = InputSamples;
    State.Debug_FindPeaks_cmp             = zeros(1,BlockSize);
    State.Debug_FindPeaks_threshold       = zeros(1,BlockSize);
    State.Debug_FindPeaks_max             = zeros(1,BlockSize);
    State.Debug_FindPeaks_edge            = zeros(1,BlockSize);
    State.Debug_FindPeaks_holdoffcounter  = zeros(1,BlockSize);
    State.Debug_FindPeaks_maxindx         = zeros(1,BlockSize);
end

if (State.Count == 0) % First block only
    State.Last2inSam = [InputSamples(1) InputSamples(1)];
    State.ZeroDet = true;
end 

InSamPk = [State.Last2inSam InputSamples]; % Append last 2 samples from previous block (for peak detection)
State.Last2inSam = InputSamples(end-1:end); % Remember last 2 samples at end of block to append to next

% Sample loop
for i = 0:BlockSize-1
    j = i + 1; % Just for Matlab indexing!! Feel free to optimise away when implementing, in fact I insist!
    
    threshold = (Params.gain * State.cmp) + State.offset; % Th = Gain * cmp + Offset
    
    % State machine
    if ((InputSamples(j) > threshold) && (State.holdoffcounter == 0) && (State.ZeroDet == true))
        % When we cross the threshold and we are not counting yet and a zero has been detected -> We have a rising edge
        State.risingedge = true;
        State.fallingedge = false;
        
        if (Params.SigTypeN > 0), State.ZeroDet = false; end % For PPG/BioZ clear zero detect flag when triggered
        
        State.holdoffcounter = Params.holdoffSamples; % Set the window in which we will look for the peak

        State.max = 0; % Initialise maximum detector as we just crossed the threshold
        State.maxindx = 0;
    else
        % When we are lower than the threshold or we are already counting
        State.risingedge = false; % No rising edge

        if (State.holdoffcounter == 1) % If we reach the end of the counting period, did we detect a peak...
            State.fallingedge = true; % Set falling edge (timer timeout)
            State.holdoffcounter = 0;
            
            if (State.max > 0) % Make sure we really detected a peak
                peak_idx = (i - State.maxindx - 1); % -1 to compensate for using 1 sample delay
                PeakIndici = [PeakIndici peak_idx]; %#ok<AGROW>
            end
        else
            State.fallingedge = false;
            State.holdoffcounter = max(State.holdoffcounter - 1,0);
            
            if (State.holdoffcounter > 0) % If we are counting
                % Take local points around a possible peak at j+1: j, j+1 and j+2
                if ((InSamPk(j) <= InSamPk(j+1)) && (InSamPk(j+1) > InSamPk(j+2)) && (InSamPk(j+1) > State.max)) % Real peak detection
                    State.max = InSamPk(j+1);
                    State.maxindx = State.holdoffcounter;
                end
            end
        end
    end
    
    if (InputSamples(j) == 0), State.ZeroDet = true; end % Detect zero level and update detector if true
    
    % Update comparisson and threshold values
    if (InputSamples(j) > State.cmp), State.v = InputSamples(j) - State.cmp; State.av = Params.attack * State.v; State.cmp = State.cmp + State.av; % Current samp >  cmp: attack mode
    else                              State.v = InputSamples(j) - State.cmp; State.dv = Params.decay  * State.v; State.cmp = State.cmp + State.dv; % Current samp <= cmp: decay mode
    end
    
    % Debug
    if (1)
        State.Debug_FindPeaks_cmp(j)            = State.cmp;
        State.Debug_FindPeaks_threshold(j)      = threshold;
        State.Debug_FindPeaks_max(j)            = State.max;
        State.Debug_FindPeaks_edge(j)           = State.risingedge - State.fallingedge; % 0:no edge, 1:rising, -1:falling
        State.Debug_FindPeaks_holdoffcounter(j) = State.holdoffcounter;
        State.Debug_FindPeaks_maxindx(j)        = State.maxindx;
        
        if ((State.risingedge == 1) && (State.fallingedge == 1)), disp('Rising edge and falling edge true at same time!'); end
    end
    
end % for i = 0:BlockSize-1

FoundPeaks = ~isempty(PeakIndici); % Return true or false, depending of whether the list is empty or not

end % Of function
