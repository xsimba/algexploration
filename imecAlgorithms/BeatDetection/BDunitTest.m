%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : BDunitTest.m
% Content   : Matlab script for unit test of BD (wrapper for beat detection algorithm) 
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

close all
clear all
clc

load('Participant_8_wb1_lon.mat');

% ECG test
[BeatsTimestamps_ECG,Debug_ECG] = BD([], ECG, 128, 'ECG'); % Sample frequency = 128 Hz
  
% Debug plots
figure; hold on;
plot(Debug_ECG.InputSamples, 'k');
plot(Debug_ECG.FindPeaks_InputSamples, 'b');
plot(Debug_ECG.FindPeaks_threshold, 'r');
plot(Debug_ECG.FindPeaks_max, 'm');
plot(Debug_ECG.FindPeaks_holdoffcounter/10, '*k');
plot(Debug_ECG.FindPeaks_maxindx/10, 'ob');

plot(Debug_ECG.FindPeaks_InputSamples./(Debug_ECG.FindPeaks_edge ==  1), '^b');
plot(Debug_ECG.FindPeaks_InputSamples./(Debug_ECG.FindPeaks_edge == -1), 'vb');

legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('ECG debug');

figure;hold on;
plot((0:length(Debug_ECG.InputSamples)-1)/Debug_ECG.Fs,Debug_ECG.InputSamples,'b');
plot(BeatsTimestamps_ECG/Debug_ECG.FreqTS,Debug_ECG.usamp,'or');
title('ECG beat detection');


% PPG test

% PPG input samples should represent pressure (-ve of light intensity) so a sign flip may be necessary
%PPG2G = -PPG2G;

PPG2G = PPG2G(1:78910); % Cut out saturation at end (data specific)

[BeatsTimestamps_PPG2G,Debug_PPG2G] = BD([], PPG2G, 128, 'PPG'); % Sample frequency = 128 Hz
%[BeatsTimestamps_PPG2G,Debug_PPG2G] = BD([], PPG2G, 128, 'PPG', 4); % Use 4Hz filter bandwidth

% Debug plots
figure; hold on;
plot(Debug_PPG2G.InputSamples-mean(Debug_PPG2G.InputSamples), 'k');
%plot(Debug_PPG2G.FindPeaks_InputSamples, 'b');
plot(Debug_PPG2G.FindPeaks_CWToutSamples, 'b');
plot(Debug_PPG2G.FindPeaks_threshold, 'r');
plot(Debug_PPG2G.FindPeaks_max, 'm');
plot(Debug_PPG2G.FindPeaks_holdoffcounter/10, '*k');
plot(Debug_PPG2G.FindPeaks_maxindx/10, 'ob');
plot(Debug_PPG2G.FindPeaks_InputSamples./(Debug_PPG2G.FindPeaks_edge ==  1), '^b');
plot(Debug_PPG2G.FindPeaks_InputSamples./(Debug_PPG2G.FindPeaks_edge == -1), 'vb');
legend('Input','CWTout','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('PPG debug');

figure;hold on;
plot((0:length(Debug_PPG2G.InputSamples)-1)/Debug_PPG2G.Fs,Debug_PPG2G.InputSamples,'b');
plot(BeatsTimestamps_PPG2G(1,:)/Debug_PPG2G.FreqTS,Debug_PPG2G.usamp,'or');
plot(BeatsTimestamps_PPG2G(2,:)/Debug_PPG2G.FreqTS,Debug_PPG2G.ftamp,'vb');
plot(BeatsTimestamps_PPG2G(3,:)/Debug_PPG2G.FreqTS,Debug_PPG2G.spamp,'^b');
plot(BeatsTimestamps_PPG2G(4,:)/Debug_PPG2G.FreqTS,Debug_PPG2G.ppamp,'^r');
plot(BeatsTimestamps_PPG2G(5,:)/Debug_PPG2G.FreqTS,Debug_PPG2G.dnamp,'vr');
legend('Input','UpStroke','Foot','SecPk','PriPk','DicNo')
title('PPG beat detection');xlabel('Time (s)');

figure;hold on;grid on;
plot(BeatsTimestamps_PPG2G(3,:)/Debug_PPG2G.FreqTS,BeatsTimestamps_PPG2G(6,:),'b');
plot(BeatsTimestamps_PPG2G(5,:)/Debug_PPG2G.FreqTS,BeatsTimestamps_PPG2G(7,:),'r');
plot(BeatsTimestamps_PPG2G(4,:)/Debug_PPG2G.FreqTS,BeatsTimestamps_PPG2G(8,:),'g');
title('PPG feature amplitudes');legend('Foot to SecPk','Foot to DicrNo','Foot to PriPk');xlabel('Time (s)');

figure;grid on;
plot(BeatsTimestamps_PPG2G(1,:)/Debug_PPG2G.FreqTS,BeatsTimestamps_PPG2G(9,:),'b');
title('PPG feature US gradient');xlabel('Time (s)');

% Frequency analysis of amplitude data
ftppamp  = BeatsTimestamps_PPG2G(8,3:end); % Primary peak
ftppampt = BeatsTimestamps_PPG2G(4,3:end)/Debug_PPG2G.FreqTS;
PltTxt   = 'Foot-PriPeak';

% ftppamp = BeatsTimestamps_PPG2G(6,1:end); % Secondary peak
% ftppampt = BeatsTimestamps_PPG2G(3,1:end)/Debug_PPG2G.FreqTS;
% PltTxt   = 'Foot-SecPeak';

% ftppamp = BeatsTimestamps_PPG2G(7,1:end); % Dicrotic notch
% ftppampt = BeatsTimestamps_PPG2G(5,1:end)/Debug_PPG2G.FreqTS;
% PltTxt   = 'Foot-DicrNot';

% NaN handling
nd = find(isnan(ftppamp)==1);
st = 1;
en = length(ftppamp);
if (~isempty(nd))
    for n = 1:length(nd)
        if (st==nd(n)), st = nd(n)+1; % Move start until not a NaN
        else            break;
        end
    end
    for n = length(nd):-1:1
        if (en==nd(n)), en = nd(n)-1; % Move end until not a NaN
        else            break;
        end
    end
    ftppamp  = ftppamp(st:en); % Crop
    ftppampt = ftppampt(st:en);
    nd = find(isnan(ftppamp)==1);
    if (~isempty(nd))
        for n = 1:length(nd)
            st = nd(n) - 1; % Start index (always valid as we are moving forward..)
            en = nd(n) + 1; % Tentative end index
            for nn = n+1:length(nd)
                if (en==nd(nn)), en = nd(nn) + 1;
                else             break;
                end
            end
            ftppamp(st+1)  = ftppamp(st)  + (ftppamp(en) -ftppamp(st) )/(en-st); % Interpolation
            ftppampt(st+1) = ftppampt(st) + (ftppampt(en)-ftppampt(st))/(en-st);
        end
    end
end

LenFFT = 2^(ceil(log2(length(ftppamp)))+2); % At least 4 times the number of input points
ftppampi = interp1(ftppampt,ftppamp,ftppampt(1)+(0:LenFFT-1)*(ftppampt(end)-ftppampt(1))/(LenFFT-1),'spline'); % Interpolate to put on regular time grid
ftppampi = ftppampi - mean(ftppampi);
ftppampf = fft(ftppampi.*(1-cos(2*pi*(0:LenFFT-1)/LenFFT))); % Hanning window
ftppampf = abs(ftppampf(1:LenFFT/4+1));
tf = ftppampt(end)-ftppampt(1);
figure;plot((0:LenFFT/4)/tf,ftppampf,'b');title([PltTxt,' amplitude spectrum']);xlabel('Frequency (Hz)');grid on;

figure;plot(60*(0:LenFFT/4)/tf,max(20*log10(ftppampf/max(ftppampf)),-60),'b');title([PltTxt,' amplitude spectrum']);xlabel('Frequency (bpm)');ylabel('Magnitude (dB)');grid on;

n