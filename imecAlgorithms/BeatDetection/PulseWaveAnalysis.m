function [FoundBeats, pwa, State] = PulseWaveAnalysis(InputSamples, BlockSize, Timestamp, State, Params)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : PulseWaveAnalysis.m
% Content   : Matlab function for pulse wave analysis (for PPG and BioZ)
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% Timestamp       Timestamp of first sample in block (32768 Hz ticks)
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters
% FoundBeats      Boolean flag to indicate if any beats were found in current block
% pwa
% (1,:) : Upstroke timestamp (current cycle)
% (2,:) : Foot timestamp (current cycle)
% (3,:) : Secondary peak timestamp(previous cycle)
% (4,:) : Primary peak timestamp (previous cycle)
% (5,:) : Dicrotic notch timestamp (previous cycle)
% (6,:) : Foot to secondary peak amplitude
% (7,:) : Foot to dicrotic notch amplitude
% (8,:) : Foot to primarty peak amplitude
% (9,:) : Gradient at upstroke
% (10,:): Decay time (previous cycle)
% (11,:): Rise time  (previous cycle)
% (12,:): RC-time (previous cycle)

% Sanity check
if (Params.SigTypeN == 0), error('Trying to call PulseWaveAnalysis for ECG!'); end

% Option for
%DecayTimeEarlyTh = true;  % Early threshold crossing
DecayTimeEarlyTh = false; % Late threshold crossing

if (isempty(State)) % Populate state variables
    State.cwt = ones(length(Params.CWT_FIR)-1,1)*InputSamples(1); % Initial state of CWT, constant level of first sample
    
    % Initialise filter
    [~,State.cwt] = filter(fliplr(Params.CWT_FIR),1,ones(length(Params.CWT_FIR),1)*InputSamples(1)); % Perform filter (reverse order for Matlab to match C impl.)
    
    State.cmp = 0;
    State.v = 0;
    State.av = 0;
    State.dv = 0;
    State.max = 0;
    State.maxindx = 0;
    State.fallingedge = 0; % false
    State.risingedge = 0; % false
    State.holdoffcounter = 0;
    State.HistoryCwtOutput = zeros(1,BlockSize*6); % History of CWT output including current: 6 blocks
    State.Count = 0;
    State.offset = 0;
end

% For each call to PulseWaveAnalysis, we start with an empty list
pwa = [];

% Apply filters: CWT and Abs / Zero Clamp
[CWTsamples,State.cwt] = filter(fliplr(Params.CWT_FIR),1,InputSamples,State.cwt); % Perform filter (reverse order for Matlab to match C impl.)
State.Debug_CWToutSamples = CWTsamples; % Store CWT output for debugging
aCWTsamples = CWTsamples.*(CWTsamples>0); % Use clamp function


% CWT history update
%         Previous history         input
% +-----+-----+-----+-----+-----+ +-----+
% |     :                       | |     |
% +-----+-----+-----+-----+-----+ +-----+
%                    \               |
%                     v              v
%         +-----+-----+-----+-----+-----+
%         |                       :     | New history
%         +-----+-----+-----+-----+-----+
%                                 ^
%                             timestamp
State.HistoryCwtOutput(1:end-BlockSize) = State.HistoryCwtOutput(1+BlockSize:end);
State.HistoryCwtOutput(end-BlockSize+1:end) = CWTsamples;

% figure;plot(State.HistoryCwtOutput);

% Automatically update the offset parameter for the peak detector to the optimal value using standard deviation
ss = std(aCWTsamples)*Params.StdScale;
if (State.Count < Params.OffUpdateFblk), State.offset = State.offset*(1-Params.OffUpdateFast) + ss*Params.OffUpdateFast; % Fast update for first OffUpdateFblk sample blocks
else                                     State.offset = State.offset*(1-Params.OffUpdateSlow) + ss*Params.OffUpdateSlow; % Slow update for rest
end

% Apply the peak detection algorithm
[FoundPeaks, PeakIndexList, State] = FindPeaks(aCWTsamples, BlockSize, State, Params);

if (FoundPeaks)
    LenHistCwt = length(State.HistoryCwtOutput);
    InterpolatedIndexList = QuadInterpolatePeaks(CWTsamples, State.HistoryCwtOutput(end-BlockSize*2+1:end-BlockSize), BlockSize, PeakIndexList); % Refine peak location(s) - use non clamped values
    
    pwa = NaN(12,length(PeakIndexList));
    
    % 1. Find upstroke
    % 2. Get gradient of upstroke
    % 3. Foot search
    % 4. Check if previous upstroke exists
    % 5. Find primary peak
    % 6. Calculate integration approximation
    % 7. Get primary peak and foot amplitudes
    % 8. Find secondary peak (or inflection point)
    % 9. Find dicrotic notch
    % 10. Calculate rise time: find minimum int value before previous upstroke, find maximum int value after previous upstroke but before curent
    % 11. Calculate decay time: find minimum int value after maximum int value but before curent upstroke
    % 12. Calculate RC time: find threshold time
    
    % 1. Find upstroke
    % PeakIndexList, InterpolatedIndexList and GroupDelay are in units of samples relative to the current block start
    % Timestamp refers to the first sample of the block, ts2samRatio converts from sample to timestamp units
    pwa(1,:) = Timestamp + (InterpolatedIndexList - Params.GroupDelay) * Params.ts2samRatio;
    
    % PPG/BioZ feature extraction
    for n = 1:length(PeakIndexList)
        
        % 2. Get gradient of upstroke
        cind = PeakIndexList(n) + LenHistCwt - BlockSize + 1; % Index of peak in history buffer
        pwa(9,n) = QuadraticInterpolation(State.HistoryCwtOutput(cind-1), State.HistoryCwtOutput(cind), State.HistoryCwtOutput(cind+1), InterpolatedIndexList(n)-PeakIndexList(n));
        
        % 3. Foot search: Search for the latest zero crossing before the current upstroke (peak in derivative)
        sa = State.HistoryCwtOutput(1:PeakIndexList(n)+LenHistCwt-BlockSize+1);
        q = (sa(2:end)>0).*(sa(1:end-1)<=0); % Preceeding point <= 0 and next point > 0 so an increasing gradient through zero
        zcindf = find(q==1,1,'last');
        FootFound = false;
        if isempty(zcindf)
            %disp('Foot search: Zero crossing not found where one was expected!');
        else
            [xf1, xf2] = QuadraticLevelCrossDet(sa(zcindf-1), sa(zcindf), sa(zcindf+1),0);
            if     (xf1 >= 0), xf = xf1; FootFound = true;
            elseif (xf2 >= 0), xf = xf2; FootFound = true;
            else   disp('Foot quadratic: Zero crossing not found where one was expected (out of range/no root)!');
            end
            if (FootFound), pwa(2,n) = Timestamp + (zcindf + xf - LenHistCwt+BlockSize-1 - Params.GroupDelay) * Params.ts2samRatio; % Foot timestamp
            end
        end
        
        % 4. Check if previous upstroke exists
        if isfield(State,'LastUpTS')
            % BeatTimestamps = Timestamp + (InterpolatedIndexList - Params.GroupDelay) * Params.ts2samRatio
            % So InterpolatedIndexList = (BeatTimestamps - Timestamp)/Params.ts2samRatio + Params.GroupDelay
            % In the history buffer the index is referenced to LenHistCwt - BlockSize + 1 so
            IdxCur = round((pwa(1,n) - Timestamp)/Params.ts2samRatio + Params.GroupDelay + LenHistCwt - BlockSize + 1); % Current US index
            IdxPre = round((State.LastUpTS - Timestamp)/Params.ts2samRatio + Params.GroupDelay + LenHistCwt - BlockSize + 1); % Previous US index
            if (IdxPre > 1) % Check that previous US is in range
                
                % 5. Find primary peak
                sa = State.HistoryCwtOutput(IdxPre:IdxCur);
                q = (sa(2:end)<0).*(sa(1:end-1)>=0); % Preceeding point >= 0 and next point < 0 so a decreasing gradient through zero
                q(1) = 0; % Force first point to zero as it is on the upstroke
                zcindp = find(q==1,1,'first');
                PriPkFound = false;
                if isempty(zcindp)
                    %disp('Primary peak search: Zero crossing not found where one was expected!');
                else
                    if (zcindp < IdxCur-IdxPre)
                        [xp1, xp2] = QuadraticLevelCrossDet(sa(zcindp-1), sa(zcindp), sa(zcindp+1),0);
                        if     (xp1 >= 0), xp = xp1; PriPkFound = true;
                        elseif (xp2 >= 0), xp = xp2; PriPkFound = true;
                        else   disp('Primary peak quadratic: Zero crossing not found where one was expected (out of range/no root)!');
                        end
                        if (PriPkFound), pwa(4,n) = Timestamp + (IdxPre - 2 + zcindp + xp - LenHistCwt + BlockSize - Params.GroupDelay) * Params.ts2samRatio; % PriPk timestamp
                        end
                    end
                end
                
                if (PriPkFound && FootFound)
                    % 6. Calculate integration approximation
                    int = cumsum(sa); % Approximation to integration
                    if ((State.Count > 49) && (State.Count < -55)) % Debug: make negative to disable plot
                        figure;hold on;grid on;plot(State.HistoryCwtOutput,'b');plot(IdxCur,State.HistoryCwtOutput(IdxCur),'or');plot(IdxPre,State.HistoryCwtOutput(IdxPre),'og');
                        plot(IdxPre:IdxCur,int*max(abs(State.HistoryCwtOutput))/max(abs(int)),'r');legend('Cwt hist.','Latest US','Previous US','Integral');xlabel('Time (samples)');
                        figure;plot(int,'.-');grid on;title('Integral');xlabel('Time (samples)');
                    end
                    
                    % 7. Get primary peak and foot amplitudes
                    % Difference between primary peak and foot times (in samples)
                    PriPkIdx = round(zcindp + xp); % Index of primary peak in int
                    FootIdx  = round(zcindf + xf - IdxPre); % Index of foot in int
                    
                    PriPk2FootSamp = FootIdx - PriPkIdx; % Number of samples between primary peak and foot
                    
                    PriPkAmp = int(PriPkIdx); % Snap to nearest samples
                    FootAmp  = int(FootIdx);
                    
                    pwa(8,n) = PriPkAmp - FootAmp; % Foot to primary peak amplitude
                    
                    SamWindowSecPk = round(PriPk2FootSamp*Params.SecPkTimeLim);      % Number of samples to search for secondary peak
                    SrchEnd = PriPkIdx+SamWindowSecPk;              
                    
                    AmpThreshSecPk = FootAmp + (PriPkAmp-FootAmp)*Params.SecPkAmpTh; % Amplitude threshold to search for secondary peak
                    
                    % 8. Find secondary peak (or inflection point)
                    q = (sa(PriPkIdx+2:SrchEnd)<0).*(sa(PriPkIdx+1:SrchEnd-1)>=0); % Preceeding point >= 0 and next point < 0 so a decreasing gradient through zero
                    zcinds = find(q==1,1,'first');
                    if isempty(zcinds)
                        % Search for inflection point (of int) instead, this is a maximum in sa but still negative
                        q = (sa(PriPkIdx+1-1:SrchEnd-1-1) <= sa(PriPkIdx+1:SrchEnd-1)).*(sa(PriPkIdx+1:SrchEnd-1) > sa(PriPkIdx+1+1:SrchEnd-1+1)); % Peak detection
                        q = q.*(sa(PriPkIdx+1:SrchEnd-1) <= 0); % Negative check
                        zcindi = find(q==1,1,'first');
                        if ~isempty(zcindi)
                            xi = QuadraticPeakDet(sa(PriPkIdx+zcindi-1), sa(PriPkIdx+zcindi), sa(PriPkIdx+zcindi+1));
                            if  (~isnan(xi))
                                SecPkIdx = round(PriPkIdx + zcindi + xi);
                                SecPkAmp = int(SecPkIdx); % Snap to nearest sample
                                if (SecPkAmp >= AmpThreshSecPk)
                                    pwa(3,n) = Timestamp + (IdxPre + PriPkIdx - 2 + zcindi + xi - LenHistCwt + BlockSize - Params.GroupDelay) * Params.ts2samRatio; % SecPk timestamp
                                    pwa(5,n) = pwa(3,n); % Dicrotic notch timestamp
                                    pwa(6,n) = SecPkAmp - FootAmp; % Foot to secondary peak amplitude
                                    pwa(7,n) = pwa(6,n); % Foot to dicrotic notch amplitude
                                end
                            else
                                disp(['Inflection point: maximum of derivatave not found where one was expected! sa = ',num2str(sa(PriPkIdx+zcindi-1)),' ',num2str(sa(PriPkIdx+zcindi)),' ',num2str(sa(PriPkIdx+zcindi+1))]);
                            end
                        end
                    else
                        % Secondary peak refinement
                        SecPkFound = false;
                        [xs1, xs2] = QuadraticLevelCrossDet(sa(PriPkIdx+zcinds-1), sa(PriPkIdx+zcinds), sa(PriPkIdx+zcinds+1),0);
                        if     (xs1 >= 0), xs = xs1; SecPkFound = true;
                        elseif (xs2 >= 0), xs = xs2; SecPkFound = true;
                        else   disp('Secondary peak quadratic: Zero crossing not found where one was expected (out of range/no root)!');
                        end
                        if (SecPkFound),
                            SecPkIdx = round(PriPkIdx + zcinds + xs);
                            SecPkAmp = int(SecPkIdx); % Snap to nearest sample
                            if (SecPkAmp >= AmpThreshSecPk),
                                pwa(3,n) = Timestamp + (IdxPre + PriPkIdx - 2 + zcinds + xs - LenHistCwt + BlockSize - Params.GroupDelay) * Params.ts2samRatio; % SecPk timestamp
                                pwa(6,n) = SecPkAmp - FootAmp; % Foot to secondary peak amplitude
                                
                                % 9. Find dicrotic notch
                                q = (sa(PriPkIdx+2:SecPkIdx)>0).*(sa(PriPkIdx+1:SecPkIdx-1)<=0); % Preceeding point <= 0 and next point > 0 so a increasing gradient through zero
                                zcindd = find(q==1,1,'last');
                                if isempty(zcindd)
                                    %disp('Dicrotic notch search: Zero crossing not found where one was expected!');
                                else
                                    DicrFound = false;
                                    [xd1, xd2] = QuadraticLevelCrossDet(sa(PriPkIdx+zcindd-1), sa(PriPkIdx+zcindd), sa(PriPkIdx+zcindd+1),0);
                                    if     (xd1 >= 0), xd = xd1; DicrFound = true;
                                    elseif (xd2 >= 0), xd = xd2; DicrFound = true;
                                    else   disp('Dicrotic notch quadratic: Zero crossing not found where one was expected (out of range/no root)!');
                                    end
                                    if (DicrFound),
                                        DicrNotIdx = round(PriPkIdx + zcindd + xd);
                                        DicrNotAmp = int(DicrNotIdx); % Snap to nearest sample
                                        pwa(5,n) = Timestamp + (IdxPre + PriPkIdx - 2 + zcindd + xd - LenHistCwt + BlockSize - Params.GroupDelay) * Params.ts2samRatio; % DicrNot timestamp
                                        pwa(7,n) = DicrNotAmp - FootAmp; % Foot to dicrotic notch amplitude
                                    end
                                end
                            end
                        end
                    end
                    
                    % 10. Calculate rise time: find minimum int value before previous upstroke, find maximum int value after previous upstroke but before curent
                    IdxSrc = IdxPre - Params.RiseSearchSam; % Index of start of search region before previous US
                    if (IdxSrc>1) % Check that search region is included in buffer
                        intpre = cumsum(State.HistoryCwtOutput(IdxSrc:IdxPre-1)); % Approximation to integration for section before int
                        intpre = intpre - intpre(end);
                        
                        [MinValPre, MinIdxPre] = min(intpre);
                        [MaxValPre, MaxIdxPre] = max(int);
                        
                        if ((MinIdxPre>1) && (MinIdxPre<length(intpre)) && (MaxIdxPre>1) && (MaxIdxPre<length(int))) % Check boundaries
                            RiseTime_MaxThAmp = Params.RiseDecayMaxTh*(MaxValPre-MinValPre) + MinValPre;
                            RiseTime_MinThAmp = Params.RiseDecayMinTh*(MaxValPre-MinValPre) + MinValPre;
                            
                            intpre = [intpre int]; %#ok<AGROW> % Concatonate vectors
                            RiseTime_MinThIdx = find(intpre(MinIdxPre:end) >= RiseTime_MinThAmp, 1, 'first') + MinIdxPre - 1;
                            RiseTime_MaxThIdx = find(intpre(MinIdxPre:end) >= RiseTime_MaxThAmp, 1, 'first') + MinIdxPre - 1;
                            
                            RiseTimeMinFound = false;
                            [x1, x2] = QuadraticLevelCrossDet(intpre(RiseTime_MinThIdx-1), intpre(RiseTime_MinThIdx), intpre(RiseTime_MinThIdx+1), RiseTime_MinThAmp);
                            if     (x1 <= 0), xrl = x1; RiseTimeMinFound = true;
                            elseif (x2 <= 0), xrl = x2; RiseTimeMinFound = true;
                            else   disp('Rise time min quadratic: Level crossing not found where one was expected (out of range/no root)!');
                            end
                            
                            RiseTimeMaxFound = false;
                            [x1, x2] = QuadraticLevelCrossDet(intpre(RiseTime_MaxThIdx-1), intpre(RiseTime_MaxThIdx), intpre(RiseTime_MaxThIdx+1), RiseTime_MaxThAmp);
                            if     (x1 <= 0), xrh = x1; RiseTimeMaxFound = true;
                            elseif (x2 <= 0), xrh = x2; RiseTimeMaxFound = true;
                            else   disp('Rise time max quadratic: Level crossing not found where one was expected (out of range/no root)!');
                            end
                            
                            if (RiseTimeMinFound && RiseTimeMaxFound)
                                RiseTimeMin = RiseTime_MinThIdx + xrl;
                                RiseTimeMax = RiseTime_MaxThIdx + xrh;
                                pwa(11,n) = (RiseTimeMax - RiseTimeMin) * Params.ts2samRatio; % Rise time (previous cycle)
                            end
                            
                            %figure;plot(intpre);
                        end
                        
                        % 11. Calculate decay time: find minimum int value after maximum int value but before curent upstroke
                        [MinValCur, MinIdxCurR] = min(int(MaxIdxPre:end));
                        MinIdxCur = MinIdxCurR+MaxIdxPre-1; % Adjust index
                        if ((MinIdxCurR>1) && (MinIdxCur<length(int)) && (MaxIdxPre>1) && (MaxIdxPre<length(int))) % Check boundaries
                            DecayTime_MaxThAmp = Params.RiseDecayMaxTh*(MaxValPre-MinValCur) + MinValCur;
                            DecayTime_MinThAmp = Params.RiseDecayMinTh*(MaxValPre-MinValCur) + MinValCur;
                            
                            DecayTime_MinThIdx = find(int(MaxIdxPre:end) <= DecayTime_MinThAmp, 1, 'first') + MaxIdxPre - 1;
                            
                            if (DecayTimeEarlyTh), DecayTime_MaxThIdx = find(int(MaxIdxPre:end) <= DecayTime_MaxThAmp, 1, 'first') + MaxIdxPre - 1; % Earliest descent through threshold
                            else                   DecayTime_MaxThIdx = find(int(MaxIdxPre:DecayTime_MinThIdx) >= DecayTime_MaxThAmp, 1, 'last') + MaxIdxPre - 1; % Latest descent through threshold
                            end
                            
                            DecayTimeMinFound = false;
                            [x1, x2] = QuadraticLevelCrossDet(int(DecayTime_MinThIdx-1), int(DecayTime_MinThIdx), int(DecayTime_MinThIdx+1), DecayTime_MinThAmp);
                            if     (x1 <= 0), xdl = x1; DecayTimeMinFound = true;
                            elseif (x2 <= 0), xdl = x2; DecayTimeMinFound = true;
                            else   disp('Decay time min quadratic: Level crossing not found where one was expected (out of range/no root)!');
                            end
                            
                            DecayTimeMaxFound = false;
                            [x1, x2] = QuadraticLevelCrossDet(int(DecayTime_MaxThIdx-1), int(DecayTime_MaxThIdx), int(DecayTime_MaxThIdx+1), DecayTime_MaxThAmp);
                            % if     (x1 <= 0), xdh = x1; DecayTimeMaxFound = true; % Earliest
                            % elseif (x2 <= 0), xdh = x2; DecayTimeMaxFound = true;
                            % if     (x1 >= 0), xdh = x1; DecayTimeMaxFound = true; % Latest
                            % elseif (x2 >= 0), xdh = x2; DecayTimeMaxFound = true;
                            if     (~isnan(x1)), xdh = x1; DecayTimeMaxFound = true;
                            elseif (~isnan(x2)), xdh = x2; DecayTimeMaxFound = true;
                            else   disp('Decay time max quadratic: Level crossing not found where one was expected (out of range/no root)!');
                            end
                            
                            if (DecayTimeMinFound && DecayTimeMaxFound)
                                DecayTimeMin = DecayTime_MinThIdx + xdl;
                                DecayTimeMax = DecayTime_MaxThIdx + xdh;
                                pwa(10,n) = (DecayTimeMin - DecayTimeMax) * Params.ts2samRatio; % Decay time (previous cycle)
                            end
                            
                            % 12. Calculate RC time: find threshold time
                            RCtime_MaxThAmp = Params.RCmaxTh*(MaxValPre-MinValCur) + MinValCur;
                            RCtime_MaxThIdx = find(int(MaxIdxPre:end) <= RCtime_MaxThAmp, 1, 'first') + MaxIdxPre - 1;
                            
                            RCsignal = (int(RCtime_MaxThIdx:DecayTime_MinThIdx)-MinValCur)/(MaxValPre-MinValCur);
                            log_RCsignal = log(RCsignal);
                            
                            xx = [ones(size(log_RCsignal));linspace(0,(length(log_RCsignal)-1)*Params.ts2samRatio,length(log_RCsignal))];
                            b = log_RCsignal/xx;
                            pwa(12,n) = -1/b(2); % RC-time (previous cycle)
                        end
                    end
                end % PriPkFound && FootFound
            end % Previous US is in range
        end
        State.LastUpTS = pwa(1,n); % Remember last up-stroke that was found
    end
end

State.Count = min(State.Count + 1,65534); % Limit to fit in 16 bit unsigned integer (to prevent wrapping around to zero)
FoundBeats = FoundPeaks;
end

% Quadratic curve fitting for zero crossing detection
function [x1, x2] = QuadraticLevelCrossDet(PrevY, CurrY, NextY, Level)
% Given the quadratic y = a*x^2 + b*x + c = Level and 3 Y values:
% When x = -1, PrevY = a - b + c
% When x =  0, CurrY =         c
% When x = +1, NextY = a + b + c
% Solving:
a = (NextY + PrevY - 2*CurrY)*0.5;
b = (NextY - PrevY)*0.5;
c = CurrY;

x1 = NaN; % First zero crossing solution
x2 = NaN; % Second zero crossing solution

% Level crossing detection: y = a*x^2 + b*x + c - Level = 0
% x = (-b +/- sqrt(b^2-4*a*(c-Level)))/(2*a)
ss = b^2-4*a*(c-Level);
if (ss >= 0)
    sq = sqrt(ss);
    v = abs(a)*2;
    
    u = -b + sq; % Test first solution x1, against limits of -1 and +1 (and implicitly reject a = 0)
    if (abs(u) < v), x1 = u/(2*a); end
    
    u = -b - sq; % Test second solution x2, against limits of -1 and +1 (and implicitly reject a = 0)
    if (abs(u) < v), x2 = u/(2*a); end
end
end

% Quadratic curve fitting for peak detection
function xp = QuadraticPeakDet(PrevY, CurrY, NextY)
% Given the quadratic y = a*x^2 + b*x + c and 3 Y values:
% When x = -1, PrevY = a - b + c
% When x =  0, CurrY =         c
% When x = +1, NextY = a + b + c
% Solving:
a = (NextY + PrevY - 2*CurrY)*0.5;
b = (NextY - PrevY)*0.5;
% (c not required)

xp = NaN; % Peak detection solution

% Peak selection
% Gradient is zero when y' = 0 = 2*a*x + b, solving for x = -b/(2*a)
% Test xp against limits of -0.5 and +0.5 (and implicitly reject a = 0)
if (abs(a) > abs(b)), xp = -b/(2*a); end

end


% Quadratic curve fitting for interpolation (X is usually a scalar but can be a vector)
function Yout = QuadraticInterpolation(PrevY, CurrY, NextY, X)
% Given the quadratic y = a*x^2 + b*x + c and 3 Y values:
% When x = -1, PrevY = a - b + c
% When x =  0, CurrY =         c
% When x = +1, NextY = a + b + c
% Solving:
a = (NextY + PrevY - 2*CurrY)*0.5;
b = (NextY - PrevY)*0.5;
c = CurrY;

if (max(abs(X)) > 0.5), warning('Interpolation expects values between -0.5 and +0.5!'); end

Yout = (a*X + b).*X + c;

end
