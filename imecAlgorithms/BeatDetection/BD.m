% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : BD.m
%  Content    : Wrapper for FindBeats.m
%  Package    : SIMBA.Validation
%  Created by : P. Casale (pierluigi.casale@imec-nl.nl)
%  Date       : 26-04-2014
%  Modification and Version History:
%  | Developer | Version |    Date   |
%  |  pcasale  |   1.0   | 08-05-2014|
%  |  pcasale  |   1.1   | 14-05-2014|
%  |  ayoung   |   1.2   | 21-05-2014|
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [BeatInfo,Debug] = BD(InputTimestamps, InputStream, Fs, SigType, varargin)
% Beat Detection
%   Inputs:
%      InputTimestamps  Row/Col vector : Timestamp for each Signal sample (in units of 1/FreqTS = 30.52 us, NOT seconds) | []
%      InputStream      Row/Col vector : Signal
%      Fs               Float           : Sample frequency (Hz)
%      SigType          String         : 'ECG' | 'PPG' | 'Bio' | 'BioZ'
%      varargin         Integer | empty : Filter pass band frequency (Hz) for PPG or BioZ only, no effect for ECG
%   Output:
%      BeatsTimestamps  ECG - 1xn matrix, PPG/BioZ - 4xn matrix : timestamps of detected beats in units of 1/FreqTS, NOT seconds!
%                       (1,:) : PPG/BioZ Upstroke or ECG R-peak timestamp (current cycle)
%                       (2,:) : PPG/BioZ Foot timestamp (current cycle)
%                       (3,:) : PPG/BioZ Secondary peak timestamp(previous cycle)
%                       (4,:) : PPG/BioZ Primary peak timestamp (previous cycle)
%                       (5,:) : PPG/BioZ Dicrotic notch timestamp (previous cycle)
%                       (6,:) : PPG/BioZ Foot to secondary peak amplitude
%                       (7,:) : PPG/BioZ Foot to dicrotic notch amplitude
%                       (8,:) : PPG/BioZ Foot to primarty peak amplitude
%                       (9,:) : PPG/BioZ Gradient at upstroke
%                       (10,:): PPG/BioZ decay time (previous cycle)
%                       (11,:): PPG/BioZ rise time  (previous cycle)
%                       (12,:): PPG/BioZ RC-time (previous cycle);
%                       Time values decrease from upstroke-foot-secpeak-dicrnotch-pripeak.
%      Debug            Structure      : debug information
%   Usage Examples:
%      [BeatsTimestamps,debug] = BD(Timestamps,                InputStream, 'ECG');  % ECG input using timestamps with units of 1/32768 s, output debug information
%      [BeatsTimestamps,~]     = BD(TimestampsInSeconds*32768, InputStream, 'Bio');  % BioZ input using timestamps in seconds
%      [BeatsTimestamps,~]     = BD([],                        InputStream, 'PPG');  % PPG input using internally generated timestamps, starting from 0

% Format and checks
dims = size(InputTimestamps);
if (min(dims)>1), error('InputTimestamps: Expecting row or coloumn vector!'); end
if dims(1)>dims(2), InputTimestamps = InputTimestamps'; end

dims = size(InputStream);
if (min(dims)>1), error('InputStream: Expecting row or coloumn vector!'); end
if dims(1)>dims(2), InputStream = InputStream'; end

if ((~isempty(InputTimestamps)) && (length(InputTimestamps) ~= length(InputStream))), error('Length of timestamps and signal are different'); end

LenExtraVar = length(varargin);
if (LenExtraVar > 1), error('BD: Only 4 or 5 input arguments accepted!'); end

if (LenExtraVar == 1)
    FiltFreq = varargin{1};
    if (~isnumeric(FiltFreq)),                                             error('BD: expecting numeric argument for filter frequency.'); end
    if ((fix(FiltFreq) ~= FiltFreq) || (FiltFreq < 4) || (FiltFreq > 13)), error('BD: expecting integer argument for filter frequency between 4 and 13 Hz.'); end
    FiltNum = FiltFreq + 9; % Frequency = 4 to 13 Hz corresponds to filter number 13 to 22
    disp(['Frequency selection: ',num2str(FiltFreq),' Hz.']);
end

% General
FreqTS                     = 32768;                             % Timestamp clock frequency
BlockSize                  = round(Fs*0.5);                     % 500 ms blocks
Params.ts2samRatio         = FreqTS/Fs;                         % Ratio of timestamp to sample frequency

if strcmpi(SigType,'ECG')
    Params.SigTypeN        = 0 ;                                % 0:ECG, 1:PPG, 2:BioZ
    Params.attack          = 1 - exp(-1000/(1.8055*Fs));        % Tatt = 1.8055 ms  Attack time constant
    Params.decay           = 1 - exp(-1000/(466.34*Fs));        % Tdec = 466.34 ms  Decay time constant
    Params.gain            = 0.65;                              % Kth  = 0.65       Scaling for peak tracking to threshold
    Params.StdScale        = 0.75 ;                             % Kstd = 0.75       Scaling for standard deviation to offset
    Params.OffUpdateFblk   = round(Fs*   2.50/BlockSize);       % Tbfu = 2.50 s     Fast update time for offset update, gives number of blocks
    Params.OffUpdateFast   = 1 - exp(-1/(0.50*Fs/BlockSize));   % Tfu  = 0.50 s     Offset update fast time constant during initial Tbfu sec.
    Params.OffUpdateSlow   = 1 - exp(-1/(5.00*Fs/BlockSize));   % Tsu  = 5.00 s     Offset update slow time constant after initial Tbfu sec.
    Params.holdoffSamples  = round(200.0*Fs/1000);              % Thof = 200.0 ms   Hold-off duration
    Params.GroupDelay      = 13.0;                              % Filter delay (get from wavelet information)
    Params.CWT_FIR         = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv'); % Load ECG CWT coefficients
    
elseif strcmpi(SigType,'PPG')
    Params.SigTypeN        = 1 ;                                % 0:ECG, 1:PPG, 2:BioZ
    Params.attack          = 1 - exp(-1000/(1.7000*Fs));        % Tatt = 1.7000 ms  Attack time constant
    Params.decay           = 1 - exp(-1000/(450.00*Fs));        % Tdec = 450.00 ms  Decay time constant
    Params.gain            = 0.55;                              % Kth  = 0.55       Scaling for peak tracking to threshold
    Params.StdScale        = 0.40;                              % Kstd = 0.40       Scaling for standard deviation to offset
    Params.OffUpdateFblk   = round(Fs*   5.00/BlockSize);       % Tbfu = 5.00 s     Fast update time for offset update, gives number of blocks
    Params.OffUpdateFast   = 1 - exp(-1/(0.50*Fs/BlockSize));   % Tfu  = 0.50 s     Offset update fast time constant during initial Tbfu sec.
    Params.OffUpdateSlow   = 1 - exp(-1/(5.00*Fs/BlockSize));   % Tsu  = 5.00 s     Offset update slow time constant after initial Tbfu sec.
    Params.holdoffSamples  = round(225.0*Fs/1000);              % Thof = 225.0 ms   Hold-off duration
    Params.SecPkAmpTh      = 50.0/100.0;                        % SPth = 50.0%      Secondary peak amplitude threshold (ratio of foot to primary peak amplitude)
    Params.SecPkTimeLim    = 85.0/100.0;                        % SPlm = 85.0%      Secondary peak time limit (ratio of foot to primary peak time)
    Params.RiseDecayMaxTh  = 70.0/100.0;                        % RDmx = 70.0%      Rise and decay time maximum threshold
    Params.RiseDecayMinTh  = 10.0/100.0;                        % RDmi = 10.0%      Rise and decay time minimum threshold
    Params.RCmaxTh         = 50.0/100.0;                        % RCth = 50.0%      Maximum threshold for RC time calculation
    Params.RiseSearchSam   = round(170.0*Fs/1000);              % RSin = 170.0 ms   Rise search interval time (from previous upstroke) in samples
    if (LenExtraVar == 0) 
        Params.GroupDelay  = 64.0;                              % Filter delay (get from wavelet information)
        Params.CWT_FIR     = importdata('Wavelets/Coeffs/PPGwavelet17_128HzDiffLPF.csv'); % Load PPG CWT coefficients 8 Hz passband, 2nd order numerical differentiation
    else
        Params.GroupDelay  = 64.0;                              % Filter delay (set 13 to 22 all have a delay of 64 samples)
        Params.CWT_FIR     = importdata(['Wavelets/Coeffs/PPGwavelet',num2str(FiltNum),'_128HzDiffLPF.csv']); % Load PPG filter coefficient set 13 to 22, 2nd order numerical differentiation
    end
    
elseif (strcmpi(SigType,'Bio') || strcmpi(SigType,'BioZ'))
    Params.SigTypeN        = 2 ;                                % 0:ECG, 1:PPG, 2:BioZ
    Params.attack          = 1 - exp(-1000/(1.7000*Fs));        % Tatt = 1.7000 ms  Attack time constant
    Params.decay           = 1 - exp(-1000/(450.00*Fs));        % Tdec = 450.00 ms  Decay time constant
    Params.gain            = 0.55;                              % Kth  = 0.55       Scaling for peak tracking to threshold
    Params.StdScale        = 0.40;                              % Kstd = 0.40       Scaling for standard deviation to offset
    Params.OffUpdateFblk   = round(Fs*   5.00/BlockSize);       % Tbfu = 5.00 s     Fast update time for offset update, gives number of blocks
    Params.OffUpdateFast   = 1 - exp(-1/(0.50*Fs/BlockSize));   % Tfu  = 0.50 s     Offset update fast time constant during initial Tbfu sec.
    Params.OffUpdateSlow   = 1 - exp(-1/(5.00*Fs/BlockSize));   % Tsu  = 5.00 s     Offset update slow time constant after initial Tbfu sec.
    Params.holdoffSamples  = round(225.0*Fs/1000);              % Thof = 225.0 ms   Hold-off duration
    Params.SecPkAmpTh      = 50.0/100.0;                        % SPth = 50.0%      Secondary peak amplitude threshold (ratio of foot to primary peak amplitude)
    Params.SecPkTimeLim    = 85.0/100.0;                        % SPlm = 85.0%      Secondary peak time limit (ratio of foot to primary peak time)
    Params.RiseDecayMaxTh  = 70.0/100.0;                        % RDmx = 70.0%      Rise and decay time maximum threshold
    Params.RiseDecayMinTh  = 10.0/100.0;                        % RDmi = 10.0%      Rise and decay time minimum threshold
    Params.RCmaxTh         = 50.0/100.0;                        % RCth = 50.0%      Maximum threshold for RC time calculation
    Params.RiseSearchSam   = round(170.0*Fs/1000);              % RSin = 170.0 ms   Rise search interval time (from previous upstroke) in samples
    if (LenExtraVar == 0) 
        Params.GroupDelay  = 64.0;                              % Filter delay (get from wavelet information)
        Params.CWT_FIR     = importdata('Wavelets/Coeffs/PPGwavelet15_128HzDiffLPF.csv'); % Load BioZ (PPG) CWT coefficients 6 Hz passband, 2nd order numerical differentiation
    else
        Params.GroupDelay  = 64.0;                              % Filter delay (set 13 to 22 all have a delay of 64 samples)
        Params.CWT_FIR     = importdata(['Wavelets/Coeffs/PPGwavelet',num2str(FiltNum),'_128HzDiffLPF.csv']); % Load BioZ (PPG) filter coefficient set 13 to 22, 2nd order numerical differentiation
    end
    
else
    error('Unknown SigType, expecting ECG, PPG, Bio or BioZ (not case sensitive)');
end

Nblocks = floor(length(InputStream)/BlockSize);

State = [];

if (Params.SigTypeN == 0), BeatInfo = NaN;      % ECG
else                       BeatInfo = NaN(12,1); % PPG/BioZ
end

% Debug
FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
FindPeaks_CWToutSamples   = zeros(1,BlockSize*Nblocks);
FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
FindPeaks_max             = zeros(1,BlockSize*Nblocks);
FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);

% Filter and replace input data (test)
%     Fpass = 15;  Apass = 0.1; % Passband Frequency (Hz) and ripple (dB)
%     Fstop = 20;  Astop = 60;  % Stopband Frequency (Hz) and attenuation
%     h = fdesign.lowpass('fp,fst,ap,ast', Fpass, Fstop, Apass, Astop, Fs);
%     Hd = design(h, 'equiripple', 'MinOrder', 'any', 'StopbandShape', 'flat');
%     InputStream = filter(Hd, InputStream);

% Block processing
for Blk = 0:Nblocks-1
    if isempty(InputTimestamps), Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
    else                         Timestamp = InputTimestamps(Blk*BlockSize+1); % Extract first timestamp of block
    end
    InputSamples = InputStream(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract block of samples
    if (Params.SigTypeN == 0), [~, BeatInfoBlock, State] =         FindBeats(InputSamples, BlockSize, Timestamp, State, Params); % ECG
    else                       [~, BeatInfoBlock, State] = PulseWaveAnalysis(InputSamples, BlockSize, Timestamp, State, Params); % PPG/BioZ
    end
    BeatInfo = [BeatInfo BeatInfoBlock];  %#ok<AGROW>
    
    % Concatenate debug data
    x = 1+Blk*BlockSize:(Blk+1)*BlockSize;
    FindPeaks_InputSamples(x)   = State.Debug_FindPeaks_InputSamples;
    FindPeaks_CWToutSamples(x)  = State.Debug_CWToutSamples;
    FindPeaks_cmp(x)            = State.Debug_FindPeaks_cmp;
    FindPeaks_threshold(x)      = State.Debug_FindPeaks_threshold;
    FindPeaks_max(x)            = State.Debug_FindPeaks_max;
    FindPeaks_edge(x)           = State.Debug_FindPeaks_edge;
    FindPeaks_holdoffcounter(x) = State.Debug_FindPeaks_holdoffcounter;
    FindPeaks_maxindx(x)        = State.Debug_FindPeaks_maxindx;
end

Debug.InputSamples             = InputStream;               % Input samples to beat detection
Debug.FindPeaks_InputSamples   = FindPeaks_InputSamples;    % Input samples to FindPeaks, after wavelet and clamp/abs processing
Debug.FindPeaks_CWToutSamples  = FindPeaks_CWToutSamples;   % CWT output samples
Debug.FindPeaks_cmp            = FindPeaks_cmp;             % Peak tracking value
Debug.FindPeaks_threshold      = FindPeaks_threshold;       % Threshold
Debug.FindPeaks_max            = FindPeaks_max;             % Maximum value between triggers (helps with peak detection)
Debug.FindPeaks_edge           = FindPeaks_edge;            % Flag to indicate timer trigger and reset
Debug.FindPeaks_holdoffcounter = FindPeaks_holdoffcounter;  % Hold off counter
Debug.FindPeaks_maxindx        = FindPeaks_maxindx;         % Index of detected peak
Debug.FreqTS                   = FreqTS;                    % Timestamp clock frequency
Debug.Fs                       = Fs;                        % Sample frequency
if isempty(InputTimestamps)
    Debug.Time0 = 0; % Start from 0
    Debug.TimeInputSamples = (0:length(InputStream)-1)/Fs;
else
    Debug.Time0 = InputTimestamps(1)/Debug.FreqTS; % Extract first timestamp and convert to time
    Debug.TimeInputSamples = InputTimestamps/Debug.FreqTS;
end
if (Params.SigTypeN > 0) % PPG/BioZ features
    Debug.usamp = interp1(Debug.TimeInputSamples, InputStream, BeatInfo(1,:)/FreqTS); % Amplitude at PPG up-stroke
    Debug.ftamp = interp1(Debug.TimeInputSamples, InputStream, BeatInfo(2,:)/FreqTS); % Amplitude at PPG foot
    Debug.spamp = interp1(Debug.TimeInputSamples, InputStream, BeatInfo(3,:)/FreqTS); % Amplitude at PPG Secondary Peak
    Debug.ppamp = interp1(Debug.TimeInputSamples, InputStream, BeatInfo(4,:)/FreqTS); % Amplitude at PPG Primary Peak
    Debug.dnamp = interp1(Debug.TimeInputSamples, InputStream, BeatInfo(5,:)/FreqTS); % Amplitude at PPG Dicrotic Notch
else
    Debug.usamp = interp1(Debug.TimeInputSamples, InputStream, BeatInfo(1,:)/FreqTS); % Amplitude at ECG R-peak
end
Debug.GroupDelay = Params.GroupDelay; % Needed for debug plots

%% Debug data plotting
if (0) % Enable simple debug plot - time
    figure; hold on;
    plot(Debug.TimeInputSamples,Debug.InputSamples,'b');
    plot(BeatInfo(1,:)/Debug.FreqTS,Debug.usamp,'or');
    if (Params.SigTypeN > 0) % PPG/BioZ features
        plot(BeatInfo(2,:)/Debug.FreqTS,Debug.ftamp,'vr');
        plot(BeatInfo(4,:)/Debug.FreqTS,Debug.ppamp,'^r');
        plot(BeatInfo(3,:)/Debug.FreqTS,Debug.spamp,'^k');
        plot(BeatInfo(5,:)/Debug.FreqTS,Debug.dnamp,'vk');
        legend('Input','UpStroke','Foot','PriPk','SecPk','DicrNot');
    end
    title('Simple beat detection debug output'); xlabel('Absolute time (s)');
    grid on;
end

if (0) % Enable simple debug plot - samples
    figure; hold on;
    plot(Debug.TimeInputSamples*Debug.Fs,Debug.InputSamples,'b');
    plot(BeatInfo(1,:)*Debug.Fs/Debug.FreqTS,Debug.usamp,'or');
    if (Params.SigTypeN > 0) % PPG/BioZ features
        plot(BeatInfo(2,:)*Debug.Fs/Debug.FreqTS,Debug.ftamp,'vr');
        plot(BeatInfo(4,:)*Debug.Fs/Debug.FreqTS,Debug.ppamp,'^r');
        plot(BeatInfo(3,:)*Debug.Fs/Debug.FreqTS,Debug.spamp,'^k');
        plot(BeatInfo(5,:)*Debug.Fs/Debug.FreqTS,Debug.dnamp,'vk');
        legend('Input','UpStroke','Foot','PriPk','SecPk','DicrNot');
    end
    title('Simple beat detection debug output'); xlabel('Absolute time (samples)');
    grid on;
end

if (0) % Enable general debug plot
    LenFP = length(Debug.FindPeaks_InputSamples);
    xx = Debug.TimeInputSamples(1:LenFP)*Debug.Fs - Debug.Time0*Debug.Fs;
    figure; hold on;
    plot(Debug.TimeInputSamples*Debug.Fs + Debug.GroupDelay - Debug.Time0*Debug.Fs, Debug.InputSamples-nanmean2(Debug.InputSamples), 'k'); % Shift input to align with filter delay
    plot(BeatInfo(1,:)*Debug.Fs/Debug.FreqTS  + Debug.GroupDelay - Debug.Time0*Debug.Fs,Debug.usamp-nanmean2(Debug.InputSamples),'or'); % and beat location markers.
    plot(xx, Debug.FindPeaks_InputSamples, 'b--');
    plot(xx, Debug.FindPeaks_CWToutSamples, 'b');
    plot(xx, Debug.FindPeaks_threshold, 'r--');
    plot(xx, Debug.FindPeaks_cmp, 'r');
    plot(xx, Debug.FindPeaks_max, 'm');
    plot(xx, Debug.FindPeaks_InputSamples./(Debug.FindPeaks_edge ==  1), '^b');
    plot(xx, Debug.FindPeaks_InputSamples./(Debug.FindPeaks_edge == -1), 'vb');
    % plot(xx, Debug.FindPeaks_holdoffcounter/10, '*k');
    % plot(xx, Debug.FindPeaks_maxindx/10, 'ob');
    
    legend('Input','Beat','CWTproc','CWTout','Thres','Cmp','Max','Trig','Timeout');
    title('Beat detection debug output'); xlabel('Relative time (samples)');
    grid on;
end

end

% Get over stat license problem
function m = nanmean2(v)
m = mean(v(~isnan(v)));
end

