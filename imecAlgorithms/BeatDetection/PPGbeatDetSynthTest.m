%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : PPGbeatDetSynthTest.m
% Content   : Matlab script for PPG beat detection test using synthesised waveforms for timing estimation of PPG features.
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

close all
clear all
clc

Fs = 128;
FreqTS = 32768; % Timestamp clock frequency
BlockSize = round(Fs*0.5); % 500 ms blocks
TestDur = 300.0; % Test duration (s)
Nsamp = ceil(Fs*TestDur);
Nblocks = floor(Nsamp/BlockSize);

% General
% Params.ts2samRatio = FreqTS/Fs; % Ratio of timestamp to sample frequency

% Test: load working reference
% load RefTest.mat
% RefAllBeatTimeStamps        = AllBeatTimeStamps;        clear AllBeatTimeStamps;
% RefFindPeaks_threshold      = FindPeaks_threshold;      clear FindPeaks_threshold;
% RefFindPeaks_max            = FindPeaks_max;            clear FindPeaks_max;
% RefFindPeaks_edge           = FindPeaks_edge;           clear FindPeaks_edge;
% RefFindPeaks_holdoffcounter = FindPeaks_holdoffcounter; clear FindPeaks_holdoffcounter;


%% PPG test
disp(' ');
disp('PPG test.');

% Heart rate parameters
HRtime = [ 0.00  100.00  200.00  300.00]; % Temporal markers, must cover 0 to TestDur time
HRrate = [29.95   29.95   90.05   90.05]; % Raw heart rate (beats/min.) before THM and respiration
HRresp = [12.00   13.00   17.00   18.00]; % Respiration rate (breaths/min.), typically 10.0 to 30.0 but lower than heart rate by a factor of 3 to 8.
HRresa = [ 1.00    1.00    1.00    1.50]*0; % Respiration amplitude HR (relative to 100 for total modulation)
HRthmf = [ 2.40    2.40    2.40    2.40]; % THM frequency (cycles/min.), typically 0.2 to 24.0 (low to high frequency) but lower than respiration
HRthma = [ 1.50    1.50    1.50    1.50]*0; % THM amplitude (relative to 100 for total modulation)

% Can add PPG amplitude variations from THM+respiration, with a phase difference

% PPG timing parameters (timing relative to upstroke)
%                foot    pp     dn     sp
PPGtimeConst = [-140.0  140.0  280.0  420.0]; % Constant timing (ms)
PPGtimeDepen = [   0.0    0.0    0.0    0.0]; % Interval dependent timing (% of interval)
PPGamplitude = [ 100.0  700.0  500.0  600.0]; %

% Calculate instantaneous heart rate and show it
Tstep = 0.001; % Time resolution (1ms)
t = 0:Tstep:TestDur;
yraw  = interp1(HRtime, HRrate, t,'PCHIP'); % Raw HR
yresR = interp1(HRtime, HRresp, t,'PCHIP'); % Respiration rate
yresA = interp1(HRtime, HRresa, t,'PCHIP'); % Respiration amplitude - HR
ythmR = interp1(HRtime, HRthmf, t,'PCHIP'); % THM rate
ythmA = interp1(HRtime, HRthma, t,'PCHIP'); % THM amplitude
phres = cumsum(yresR*2*pi*Tstep/60); % Accumulation of phase increments for respiration
phthm = cumsum(ythmR*2*pi*Tstep/60); % Accumulation of phase increments for THM
yins = yraw.*(1 + yresA.*sin(phres)/100 + ythmA.*sin(phthm)/100); % Instantaneous HR: Generate scaled sines and modulate raw rate
figure; hold on; grid on;
plot(t,yraw,'b');
plot(t,yresR,'r');
plot(t,yins,'k');
title('Synthesised heart rate over time');ylabel('Instantaneous rate (per minute)');xlabel('Relative time (s)');
legend('Raw heart','Respiration','Instantaneous');

% Render
% Get beat times
achr = cumsum(yins)*Tstep/60; % Accumulation of rate gives beat count -> timing
Nbeats = floor(achr(end));
BeatTimes = zeros(1,Nbeats);
for b = 1:Nbeats % Determine each beat position
    idx = find(achr>=b,1);
    x = (achr(idx)-b)/(achr(idx-1)-achr(idx)); % Linear interpolation y = a*x+b, x = -y0/(y0-yz) = y0/(yz-y0)
    BeatTimes(b) = (idx-1+x)*Tstep;
end
yinsb  = interp1(t, yins, BeatTimes,'PCHIP'); % Instantaneous HR for each beat

% Populate samples
usr = 10; % Up-sampling ratio
Fsu = Fs*usr;
Nsampu = Nsamp*usr;
yppgu = ones(1,Nsampu+2*Fsu)*PPGamplitude(1); % 2 seconds extra for an extra cycle @ 30 bpm, inital value of first defined amplitude
LenPPGfeat = length(PPGtimeConst);
for b = 1:Nbeats
    bt = BeatTimes(b); % Beat time, up-stroke
    ft = PPGtimeConst/1000 + PPGtimeDepen*60/(yins(b)*100); % Feature times relative to beat time
    for f = 0:LenPPGfeat-1
        if ((b>1) || (f>0))
            if (f>0)
                a1 = PPGamplitude(f);   % First to last amplitude/time points
                x1 = ft(f);
                a3 = PPGamplitude(f+1);
                x3 = ft(f+1);
            else
                a1 = PPGamplitude(end); % Previous last to next first section joining
                x1 = ft(end) - bt + BeatTimes(b-1); % Referenced to previous beat time so we must subtract the relative time between beats
                a3 = PPGamplitude(1);
                x3 = ft(1);
            end
            if ((x1<0) && (x3>0)) % Curve includes up-stroke (at x2=0)
                den = 1/((x1-x3)^3*(x1^2+4*x1*x3+x3^2)); % Quartic polynomial, y = aa*x^4 + bb*x^3 + cc*x^2 + dd*x + ee
                aa = 3*(x1+x3)*(a3-a1)*den;              % @ x=x1, y=a1, y'=0
                bb = (4*(x1^2+x1*x3+x3^2))*(a1-a3)*den;  % @ x=x2, y''=0
                cc = 0;                                  % @ x=x3, y=a3, y'=0
                dd = 12*x1^2*x3^2*(a3-a1)*den;
                ee = (a3*x1^5+x1^4*x3*a3-8*x1^3*x3^2*a3+8*x1^2*x3^3*a1-x1*x3^4*a1-x3^5*a1)*den;
            else                  % Curve does not include up-stroke
                den = 1/(x1-x3)^3;                       % Cubic polynomial, y = bb*x^3 + cc*x^2 + dd*x + ee
                aa = 0;                                  % @ x=x1, y=a1, y'=0
                bb = 2*(a3-a1)*den;                      % @ x=x3, y=a3, y'=0
                cc = 3*(a1-a3)*(x1+x3)*den;
                dd = 6*x3*(a3-a1)*x1*den;
                ee = (a3*x1^3-3*a3*x3*x1^2+3*a1*x3^2*x1-a1*x3^3)*den;
            end
            s = ceil((bt+x1)*Fsu):floor((bt+x3)*Fsu); % First to last samples
            x = s/Fsu - bt;                           % In terms of x
            y = aa*x.^4 + bb*x.^3 + cc*x.^2 + dd*x + ee;
            yppgu(s+1) = y;
        end
    end
end

figure;hold on; grid on;
plot((0:Nsampu-1)/Fsu,yppgu(1:Nsampu),'b');
plot(((0:Nsampu-1)+0.5)/Fsu,25*diff(yppgu(1:Nsampu+1)),'r');
plot(BeatTimes,interp1((0:Nsampu-1)/Fsu,yppgu(1:Nsampu),BeatTimes),'^b');
plot(BeatTimes,interp1(((0:Nsampu-1)+0.5)/Fsu,25*diff(yppgu(1:Nsampu+1)),BeatTimes),'^r');
legend('PPG','dv/dt');title('Synthesised PPG and derivitave');ylabel('Amplitude');xlabel('Relative time (s)');

Samp = yppgu(1:usr:Nsampu); % Decimate to required Fs

% Select ONE test (enable by using 1, disable by using 0)

if (0) % Test BD
    [AllBeatTimeStamps,Debug] = BD([], Samp, Fs, 'PPG'); % Usual PPG handling
    AllBeatTimeStamps = AllBeatTimeStamps/FreqTS; % Convert from TS units to time
    bt = BeatTimes(1:300); % Synthesised up-stroke times
end

if (1) % Test BDstruc with default format (using e.g. data.ppg.a.timestamps)
    data.ppg.a.signal     = Samp;
    data.ppg.a.fs         = Fs; % Comment out to test absence of fs field
    TimeOffset            = 500.0;
    data.ppg.a.timestamps = (0:length(Samp)-1)/Fs + TimeOffset;
    data = BDstruc(data, 'ppg.a');
    AllBeatTimeStamps = zeros(9,length(data.ppg.a.bd.upstroke));
    AllBeatTimeStamps(1,:) = data.ppg.a.bd.upstroke;
    AllBeatTimeStamps(2,:) = data.ppg.a.bd.foot;
    AllBeatTimeStamps(3,:) = data.ppg.a.bd.secpeak;
    AllBeatTimeStamps(4,:) = data.ppg.a.bd.pripeak;
    AllBeatTimeStamps(5,:) = data.ppg.a.bd.dicrnot;
    AllBeatTimeStamps(6,:) = data.ppg.a.bd.spftamp;
    AllBeatTimeStamps(7,:) = data.ppg.a.bd.dnftamp;
    AllBeatTimeStamps(8,:) = data.ppg.a.bd.ppftamp;
    AllBeatTimeStamps(9,:) = data.ppg.a.bd.upstgrad;
    AllBeatTimeStamps(10,:) = data.ppg.a.bd.decaytime;
    AllBeatTimeStamps(11,:) = data.ppg.a.bd.risetime;
    AllBeatTimeStamps(12,:) = data.ppg.a.bd.RCtime;
    bt = BeatTimes + TimeOffset; % Synthesised up-stroke times
end

if (0) % Test BDstruc with alternative format (using data.timestamps)
    data.ppg.b.signal = Samp;
    data.ppg.b.fs     = Fs; % Comment out to test absence of fs field
    TimeOffset        = 300.0;
    data.timestamps   = (0:length(Samp)-1)/Fs + TimeOffset;
    data = BDstruc(data, 'ppg.b');
    AllBeatTimeStamps = zeros(9,length(data.ppg.b.bd.upstroke));
    AllBeatTimeStamps(1,:) = data.ppg.b.bd.upstroke;
    AllBeatTimeStamps(2,:) = data.ppg.b.bd.foot;
    AllBeatTimeStamps(3,:) = data.ppg.b.bd.secpeak;
    AllBeatTimeStamps(4,:) = data.ppg.b.bd.pripeak;
    AllBeatTimeStamps(5,:) = data.ppg.b.bd.dicrnot;
    AllBeatTimeStamps(6,:) = data.ppg.b.bd.spftamp;
    AllBeatTimeStamps(7,:) = data.ppg.b.bd.dnftamp;
    AllBeatTimeStamps(8,:) = data.ppg.b.bd.ppftamp;
    AllBeatTimeStamps(9,:) = data.ppg.b.bd.upstgrad;
    AllBeatTimeStamps(10,:) = data.ppg.b.bd.decaytime;
    AllBeatTimeStamps(11,:) = data.ppg.b.bd.risetime;
    AllBeatTimeStamps(12,:) = data.ppg.b.bd.RCtime;
    bt = BeatTimes + TimeOffset; % Synthesised up-stroke times
end

if (0) % Test BDstruc with alternative format (using data.timestamps), simulate data loss (ignore timing error plot)
    data.ppg.c.signal = Samp;
    data.ppg.c.fs     = Fs; % Comment out to test absence of fs field
    TimeOffset        = 300.0;
    data.timestamps   = (0:length(Samp)-1)/Fs + TimeOffset;
    StartCutIdx = 20000;
    EndCutIdx   = 21023;
    data.ppg.c.signal = data.ppg.c.signal([1:StartCutIdx-1 EndCutIdx+1:end]);
    data.timestamps   = data.timestamps([1:StartCutIdx-1 EndCutIdx+1:end]);
    data = BDstruc(data, 'ppg.c');
    AllBeatTimeStamps = zeros(9,length(data.ppg.c.bd.upstroke));
    AllBeatTimeStamps(1,:) = data.ppg.c.bd.upstroke;
    AllBeatTimeStamps(2,:) = data.ppg.c.bd.foot;
    AllBeatTimeStamps(3,:) = data.ppg.c.bd.secpeak;
    AllBeatTimeStamps(4,:) = data.ppg.c.bd.pripeak;
    AllBeatTimeStamps(5,:) = data.ppg.c.bd.dicrnot;
    AllBeatTimeStamps(6,:) = data.ppg.c.bd.spftamp;
    AllBeatTimeStamps(7,:) = data.ppg.c.bd.dnftamp;
    AllBeatTimeStamps(8,:) = data.ppg.c.bd.ppftamp;
    AllBeatTimeStamps(9,:) = data.ppg.c.bd.upstgrad;
    AllBeatTimeStamps(10,:) = data.ppg.c.bd.decaytime;
    AllBeatTimeStamps(11,:) = data.ppg.c.bd.risetime;
    AllBeatTimeStamps(12,:) = data.ppg.c.bd.RCtime;
    bt = BeatTimes + TimeOffset; % Synthesised up-stroke times
end

if (0) % Test BDstruc with alternative format (using data.timestamps) and select filter frequency
    data.ppg.d.signal = Samp;
    data.ppg.d.fs     = Fs; % Comment out to test absence of fs field
    TimeOffset        = 400.0;
    data.timestamps   = (0:length(Samp)-1)/Fs + TimeOffset;
    data = BDstruc(data, 'ppg.d', 7); % 7 Hz
    AllBeatTimeStamps = zeros(9,length(data.ppg.d.bd.upstroke));
    AllBeatTimeStamps(1,:) = data.ppg.d.bd.upstroke;
    AllBeatTimeStamps(2,:) = data.ppg.d.bd.foot;
    AllBeatTimeStamps(3,:) = data.ppg.d.bd.secpeak;
    AllBeatTimeStamps(4,:) = data.ppg.d.bd.pripeak;
    AllBeatTimeStamps(5,:) = data.ppg.d.bd.dicrnot;
    AllBeatTimeStamps(6,:) = data.ppg.d.bd.spftamp;
    AllBeatTimeStamps(7,:) = data.ppg.d.bd.dnftamp;
    AllBeatTimeStamps(8,:) = data.ppg.d.bd.ppftamp;
    AllBeatTimeStamps(9,:) = data.ppg.d.bd.upstgrad;
    AllBeatTimeStamps(10,:) = data.ppg.d.bd.decaytime;
    AllBeatTimeStamps(11,:) = data.ppg.d.bd.risetime;
    AllBeatTimeStamps(12,:) = data.ppg.d.bd.RCtime;
    bt = BeatTimes + TimeOffset; % Synthesised up-stroke times
end

if (0) % Test BDstruc with no timestamps field (valid for both default and alternative formats)
    data.ppg.e.signal = Samp;
    data.ppg.e.fs     = Fs; % Comment out to test absence of fs field
    data = BDstruc(data, 'ppg.e');
    AllBeatTimeStamps = zeros(9,length(data.ppg.e.bd.upstroke));
    AllBeatTimeStamps(1,:) = data.ppg.e.bd.upstroke;
    AllBeatTimeStamps(2,:) = data.ppg.e.bd.foot;
    AllBeatTimeStamps(3,:) = data.ppg.e.bd.secpeak;
    AllBeatTimeStamps(4,:) = data.ppg.e.bd.pripeak;
    AllBeatTimeStamps(5,:) = data.ppg.e.bd.dicrnot;
    AllBeatTimeStamps(6,:) = data.ppg.e.bd.spftamp;
    AllBeatTimeStamps(7,:) = data.ppg.e.bd.dnftamp;
    AllBeatTimeStamps(8,:) = data.ppg.e.bd.ppftamp;
    AllBeatTimeStamps(9,:) = data.ppg.e.bd.upstgrad;
    AllBeatTimeStamps(10,:) = data.ppg.e.bd.decaytime;
    AllBeatTimeStamps(11,:) = data.ppg.e.bd.risetime;
    AllBeatTimeStamps(12,:) = data.ppg.e.bd.RCtime;
    bt = BeatTimes; % Synthesised up-stroke times
end

disp(' ');
figure;plot(diff(AllBeatTimeStamps(1,4:end)));title('PPG R-R intervals (first 3 points ignored)');
figure;plot(60./diff(AllBeatTimeStamps(1,4:end)));title('PPG Heart rate (first 3 points ignored)');


% Adjust to align beats
[val,idx] = min(abs(bt(15)-AllBeatTimeStamps(1,1:30)));
ia = idx - 15;
disp(['Minimum time difference = ',num2str(val*1000),' ms with adjustment of ',num2str(ia),' indexes.']);

lenBT = length(BeatTimes);
lenTS = size(AllBeatTimeStamps,2) - ia - 1; % 1 shorter than actual size as 1 or 0 is added to the index
MinLen = min(lenBT, lenTS);

bt = bt(1:MinLen);

us_mt = AllBeatTimeStamps(1,(1:MinLen)+ia);   % Up-stroke time
ft_mt = AllBeatTimeStamps(2,(1:MinLen)+ia);   % Foot time
sp_mt = AllBeatTimeStamps(3,(1:MinLen)+ia+1); % Secondary peak time (previous)
pp_mt = AllBeatTimeStamps(4,(1:MinLen)+ia+1); % Primary peak time (previous)
dn_mt = AllBeatTimeStamps(5,(1:MinLen)+ia+1); % Dicrotic notch time(previous)
sf_mt = AllBeatTimeStamps(6,(1:MinLen)+ia);   % Foot to secondary peak amplitude
df_mt = AllBeatTimeStamps(7,(1:MinLen)+ia);   % Foot to dicrotic notch amplitude
pf_mt = AllBeatTimeStamps(8,(1:MinLen)+ia);   % Foot to primarty peak amplitude
gr_mt = AllBeatTimeStamps(9,(1:MinLen)+ia);   % Gradient at upstroke
dt_mt = AllBeatTimeStamps(10,(1:MinLen)+ia);  % Decay time (previous)
rt_mt = AllBeatTimeStamps(11,(1:MinLen)+ia);  % Rise time (previous)
rc_mt = AllBeatTimeStamps(12,(1:MinLen)+ia);  % RC time (previous)

% Timing deviations (in ms)
us_er = 1000*(us_mt-bt);                 % Up stroke
ft_er = 1000*(ft_mt-bt)-PPGtimeConst(1); % Foot
pp_er = 1000*(pp_mt-bt)-PPGtimeConst(2); % Primary peak
dn_er = 1000*(dn_mt-bt)-PPGtimeConst(3); % Dicrotic notch
sp_er = 1000*(sp_mt-bt)-PPGtimeConst(4); % Secondary peak

figure;hold on;grid on;
plot(us_mt, us_er,'b');
plot(us_mt, ft_er,'r');
plot(us_mt, pp_er,'g');
plot(us_mt, dn_er,'m');
plot(us_mt, sp_er,'k');
title('PPG feature timing errors');legend('Up-stroke','Foot','Primary Pk','Dicrotic No','Secondary Pk');ylabel('Time (ms)');xlabel('Time (s)');

figure;hold on;grid on;
plot(us_mt, sf_mt,'b');
plot(us_mt, df_mt,'r');
plot(us_mt, pf_mt,'g');
title('PPG feature amplitudes');legend('Foot to SecPk','Foot to DicrNo','Foot to PriPk');xlabel('Time (s)');

figure;grid on;
plot(us_mt, gr_mt,'b');
title('PPG feature US gradient');xlabel('Time (s)');

figure;hold on;grid on;
plot(us_mt, dt_mt*1000,'b');
plot(us_mt, rt_mt*1000,'r');
plot(us_mt, rc_mt*1000,'g');
title('PPG relative timing features');legend('Decay time','Rise time','RC time');xlabel('Time (s)');ylabel('Time (ms)');

% Test of FiltAmp
if (1)
    ChChar = 'a'; % Select channel
    if (1) % Simulate data loss
        data.ppg.(ChChar).signal     = [data.ppg.(ChChar).signal(1:10000)     data.ppg.(ChChar).signal(11000:end)];
        data.ppg.(ChChar).timestamps = [data.ppg.(ChChar).timestamps(1:10000) data.ppg.(ChChar).timestamps(11000:end)];
    end
    data = FiltAmp(data, ['ppg.' ChChar], 8.0); % PPG, 8 Hz low pass
    InputSamp = data.ppg.(ChChar).signal;
    FiltSamp  = data.ppg.(ChChar).filtsig.signal;
    SampTimes = data.ppg.(ChChar).timestamps;
    figure;hold on;
    plot(SampTimes, InputSamp,'b');
    plot(SampTimes, FiltSamp,'k');
    plot(data.ppg.(ChChar).bd.upstroke, data.ppg.(ChChar).filtsig.usamp, 'or');
    plot(data.ppg.(ChChar).bd.foot,     data.ppg.(ChChar).filtsig.ftamp, 'vr');
    plot(data.ppg.(ChChar).bd.pripeak,  data.ppg.(ChChar).filtsig.ppamp, '^r');
    plot(data.ppg.(ChChar).bd.secpeak,  data.ppg.(ChChar).filtsig.spamp, '^m');
    plot(data.ppg.(ChChar).bd.dicrnot,  data.ppg.(ChChar).filtsig.dnamp, 'vm');
    legend('Input','Filt','UpStroke','Foot','PriPk','SecPk','DicrNo');
    xlabel('Time (s)');title('Filtered amplitude extraction');
end

% Reporting
ff = '%+6.2f'; % Format spec
for n = 5:10:MinLen
    disp(['HR: ',sprintf('%6.2f',yinsb(n)),' @ ',sprintf('%7.2f',BeatTimes(n)),' s.  Errors. US: ',sprintf(ff,us_er(n)),' ms  FT: ',sprintf(ff,ft_er(n)),' ms  PP: ',sprintf(ff,pp_er(n)),' ms  DN: ',sprintf(ff,dn_er(n)),' ms  SP: ',sprintf(ff,sp_er(n)),' ms']);
end
disp(' ');
disp(['Maximum absolute errors. US: ',num2str(max(abs(us_er(5:end)))),' ms  FT: ',num2str(max(abs(ft_er(5:end)))),' ms  PP: ',num2str(max(abs(pp_er(5:end)))),' ms  DN: ',num2str(max(abs(dn_er(5:end)))),' ms  SP: ',num2str(max(abs(sp_er(5:end)))),' ms']);

% Test: save reference data
% save('RefTest','AllBeatTimeStamps','FindPeaks_threshold','FindPeaks_max','FindPeaks_edge','FindPeaks_holdoffcounter');

% Test: compare to reference
% figure; hold on; plot(RefAllBeatTimeStamps(1,:),'b');   plot(AllBeatTimeStamps(1,:),'r');   title('UpStroke times');
% figure; hold on; plot(RefAllBeatTimeStamps(2,:),'b');   plot(AllBeatTimeStamps(2,:),'r');   title('Foot times');
% figure; hold on; plot(RefAllBeatTimeStamps(4,:),'b');   plot(AllBeatTimeStamps(4,:),'r');   title('PriPk times');
% figure; hold on; plot(RefFindPeaks_threshold,'b');      plot(FindPeaks_threshold,'r');      title('Threshold');
% figure; hold on; plot(RefFindPeaks_max,'b');            plot(FindPeaks_max,'r');            title('Max');
% figure; hold on; plot(RefFindPeaks_edge,'b');           plot(FindPeaks_edge,'r');           title('Edge');
% figure; hold on; plot(RefFindPeaks_holdoffcounter,'b'); plot(FindPeaks_holdoffcounter,'r'); title('HoldOffCounter');

b;
