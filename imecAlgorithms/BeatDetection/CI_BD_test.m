
close all;
clear all;
clc;

% Test 1: good example
load('Participant_8_wb1_lon.mat');
data.ppg.a.signal = PPG2G;
data.ecg.signal   = ECG;
clear PPG2G_f PPG2R ecg_uncertainty foot_green peak_green ppg2g_uncertainty qrs upstroke_green
data = BDstruc(data,'ppg.a');
data = CI_BD(data,'ppg.a');
data = BDstruc(data,'ecg');
data = CI_BD(data,'ecg');

% Test 2: 
load('\\winnl\simba\Databases\simband_data\0107-0136_original_large_scale_NL_team\SP0108\SP0108_S_SI_2.mat')
data = BDstruc(data,'ppg.e');
data = CI_BD(data,'ppg.e');
data = BDstruc(data,'ecg');
data = CI_BD(data,'ecg');
