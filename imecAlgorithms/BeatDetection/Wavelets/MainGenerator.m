%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : MainGenerator.m
% Content   : Main Matlab script for generation of wavelet coefficients for beat detection algorithms 
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% Refer to xls for specifications - fill in xls first!
% The function Coef5060quant may take some time so only enable the generators that are required to be updated!

close all
clear all
clc

%% ECG #1: Mexican hat, Fstd = 14.30 Hz, Fs = 512 Hz, Ncoef = 105 (odd for integer sample delay)
%          No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 512;
    wav = MexicanHatsWaveletGen(14.30, Fs, 105, 2, 0.00);
    Coef = Coef5060quant(Fs, wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/ECGwavelet1_512HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['ECGwavelet1_512Hz: delay = ',num2str((length(Coef)-1)/2),' samples.']);
end


%% ECG #2: Mexican hat, Fstd = 14.30 Hz, Fs = 128 Hz, Ncoef = 27 (odd for integer sample delay)
%          No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    wav = MexicanHatsWaveletGen(14.30, Fs, 27, 2, 0.00);
    Coef = Coef5060quant(Fs, wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/ECGwavelet2_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['ECGwavelet2_128Hz: delay = ',num2str((length(Coef)-1)/2),' samples.']);
end


%% PPG #1/2: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 100 Hz
%            No quantisation, StopFreq = 49.5 Hz, 50 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 100;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/PPGwavelet1_100Hz.csv', Wav, 'newline','pc', 'precision',8);
    dlmwrite('Coeffs/PPGwavelet2_100HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwaveletX_100Hz: delay = ',num2str(dl),' samples.']);
end


%% PPG #3/4: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 128 Hz
%            No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/PPGwavelet3_128Hz.csv', Wav, 'newline','pc', 'precision',8);
    dlmwrite('Coeffs/PPGwavelet4_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwaveletX_128Hz: delay = ',num2str(dl),' samples.']);
end


%% PPG #5: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 50 Hz
%          No quantisation, StopFreq = 49.5 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 50;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
%     Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/PPGwavelet5_50Hz.csv', Wav, 'newline','pc', 'precision',8);
    disp(['PPGwavelet5_50Hz: delay = ',num2str(dl),' samples.']);
end

%% PPG #6: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 128 Hz
%          No quantisation, StopFreq = 12.0 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
    Coef = Coef5060quant(Fs, [Wav 0 0 0 0], 0, 12, 1, 1, 1.0, 1); % Add a few extra coefficients so that the bandwidth reduction has something to use.
    dlmwrite('Coeffs/PPGwavelet6_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet6_128Hz: delay = ',num2str(dl+4),' samples.']);
end

%% PPG #7: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 1.000, Fs = 128 Hz
%          No quantisation, StopFreq = 10.0 Hz, 50 and 60 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 1.000, Fs);
    Coef = Coef5060quant(Fs, [Wav 0 0 0 0], 0, 10, 1, 1, 1.0, 1); % Add a few extra coefficients so that the bandwidth reduction has something to use.
    dlmwrite('Coeffs/PPGwavelet7_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet7_128Hz: delay = ',num2str(dl+4),' samples.']);
end

%% PPG #8: DiffLPF, Fpass = 7.0 Hz, Fstop = 10.0 Hz, Fs = 128 Hz, 1st order numerical differentiation
if (0)
    Fs = 128;
    [Coef, dl] = DiffLPF(7.0, 10.0, 0.1, 61.0, Fs, 1);
    dlmwrite('Coeffs/PPGwavelet8_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet8_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #9: DiffLPF, Fpass = 12.0 Hz, Fstop = 15.0 Hz, Fs = 128 Hz, 1st order numerical differentiation
if (0)
    Fs = 128;
    [Coef, dl] = DiffLPF(12.0, 15.0, 0.1, 61.0, Fs, 1);
    dlmwrite('Coeffs/PPGwavelet9_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet9_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #10: DiffLPF, Fpass = 12.0 Hz, Fstop = 15.0 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (0)
    Fs = 128;
    [Coef, dl] = DiffLPF(12.0, 15.0, 0.1, 61.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet10_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet10_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #11: DiffLPFinterp, Fpass = 12.0 Hz, Fstop = 15.0 Hz, Fs = 128*8 Hz, 2nd order numerical differentiation
if (0)
    Fs = 128*8;
    [Coef, dl] = DiffLPF(12.0, 15.0, 0.1, 61.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet11_128HzDiffLPFinterp8.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet11_128HzDiffLPFinterp8: delay = ',num2str(dl/8,8),' samples.']);
end

%% PPG #12: DiffLPF, Fpass = 6.0 Hz, Fstop = 9.0 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (0)
    Fs = 128;
    [Coef, dl] = DiffLPF(6.0, 9.0, 0.1, 61.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet12_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet12_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #13: DiffLPF, Fpass = 4.00 Hz, Fstop = 7.10 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(4.00, 7.10, 0.1, 65.46, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet13_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet13_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #14: DiffLPF, Fpass = 5.00 Hz, Fstop = 8.05 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(5.00, 8.05, 0.1, 65.07, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet14_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet14_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #15: DiffLPF, Fpass = 6.00 Hz, Fstop = 9.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(6.00, 9.00, 0.1, 64.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet15_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet15_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #16: DiffLPF, Fpass = 7.00 Hz, Fstop = 10.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(7.00, 10.00, 0.1, 64.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet16_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet16_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #17: DiffLPF, Fpass = 8.00 Hz, Fstop = 11.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(8.00, 11.00, 0.090, 63.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet17_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet17_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #18: DiffLPF, Fpass = 9.00 Hz, Fstop = 12.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(9.00, 12.00, 0.085, 63.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet18_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet18_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #19: DiffLPF, Fpass = 10.00 Hz, Fstop = 13.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(10.00, 13.00, 0.078, 62.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet19_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet19_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #20: DiffLPF, Fpass = 11.00 Hz, Fstop = 14.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(11.00, 14.00, 0.076, 62.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet20_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet20_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #21: DiffLPF, Fpass = 12.00 Hz, Fstop = 15.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(12.00, 15.00, 0.074, 62.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet21_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet21_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end

%% PPG #22: DiffLPF, Fpass = 13.00 Hz, Fstop = 16.00 Hz, Fs = 128 Hz, 2nd order numerical differentiation
if (1)
    Fs = 128;
    [Coef, dl] = DiffLPF(13.00, 16.00, 0.073, 62.0, Fs, 2);
    dlmwrite('Coeffs/PPGwavelet22_128HzDiffLPF.csv', Coef, 'newline','pc', 'precision',8);
    disp(['PPGwavelet22_128HzDiffLPF: delay = ',num2str(dl),' samples.']);
end


%% BioZ #1: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 2.500, Fs = 1024 Hz
%           No quantisation, StopFreq = 49.5 Hz, 50 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 1024;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 2.500, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/BioZwavelet1_1024HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['BioZwavelet1_1024HzSupp: delay = ',num2str(dl),' samples.']);
end

%% BioZ #2: DiffRCexp, xd = 550 ms, xl = 125 ms, pr = 2.500, Fs = 128 Hz
%           No quantisation, StopFreq = 49.5 Hz, 50 Hz suppression, 1.0 Hz notch width
if (0)
    Fs = 128;
    [Wav, dl] = DiffWavelet(0.550, 0.125, 2.500, Fs);
    Coef = Coef5060quant(Fs, Wav, 0, 49.5, 1, 1, 1.0, 1);
    dlmwrite('Coeffs/BioZwavelet2_128HzSupp.csv', Coef, 'newline','pc', 'precision',8);
    disp(['BioZwavelet2_128HzSupp: delay = ',num2str(dl),' samples.']);
end