function [d0, dl] = DiffLPF(Fpass, Fstop, Apass, Astop, Fs, order)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : DiffLPF.m
% Content   : Matlab function for wavelet generation based on LPF with differentiation
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

%  Fpass: Pass band stop frequency (Hz)
%  Fstop: Stop band start frequency (Hz)
%  fs: sample frequency (Hz)
%  order: Numerical differentiation order
%  d0: Wavelet coefficients (normalised)
%  dl: Estimated filter delay (samples)

% Example use:
% [d0, dl] = DiffLPF(7.0, 10.0, 0.1, 61.0, 128);

h = fdesign.lowpass('fp,fst,ap,ast', Fpass, Fstop, Apass, Astop, Fs);
Hd = design(h, 'equiripple', 'MinOrder', 'any', 'StopbandShape', 'linear','StopBandDecay',25); % 25 dB decay over stopband (compensate for differentiation)
q = grpdelay(Hd);
q = q(1);

% Make wavelet combining LPF and diff...
% Choose order of polynomial for numerical differentiation
if     (order == 1), kd = [ +1    -1                                         ]; dd = 0.5; 
elseif (order == 2), kd = [ +1     0    -1                                   ]; dd = 1.0;
elseif (order == 3), kd = [ -1   +27   -27      +1                           ]; dd = 1.5;
elseif (order == 4), kd = [ -1    +8     0      -8      +1                   ]; dd = 2.0;
elseif (order == 5), kd = [ +9  -125 +2250   -2250    +125    -9             ]; dd = 2.5;
elseif (order == 6), kd = [ +1    -9   +45       0     -45    +9    -1       ]; dd = 3.0;
elseif (order == 7), kd = [-75 +1029 -8575 +128625 -128625 +8575 -1029 +75   ]; dd = 3.5;
elseif (order == 8), kd = [ -3   +32  -168    +672       0  -672  +168 -32 +3]; dd = 4.0;
else                 error('order not in range, 1 to 8 expected!');
end
d0 = conv(Hd.Numerator,kd); % Convolve LPF with differentiation kernel
d0 = fliplr(d0)/max(abs(d0)); % Normalisation and reverse order (as we need coefficients that apply to increasing time for the C impl.)

LenFFT = 2^nextpow2(Fs*64); % Resolution better than 0.016 Hz
fw = fft([d0 zeros(1, LenFFT-length(d0))]);
fw = abs(fw(1:LenFFT/2+1)); fw = fw/max(fw); % Crop, magnitude and normalise
fl = max(20*log10(fw),-80); % dB scale
idx = round((Fpass-1.0)*LenFFT/Fs+1); % Index at Fpass - 1.0 Hz 
Fidx = (idx-1)*Fs/LenFFT; % Precise frequency at index
figure;plot((0:LenFFT/2)*Fs/LenFFT,fl);grid on;title('Wavelet frequency response');xlabel('Frequency (Hz)');ylabel('Normalised magnitude (dB)');

figure;grid on;hold on;line([0 Fidx/fw(idx)],[0 1],'LineWidth',5,'color',[0.8 0.8 0.8]); % Extrapolate line from (0,0)-(Fidx,fw(idx)) to (0,0)-(x,1), x = Fidx/fw(idx)
plot((0:LenFFT/2)*Fs/LenFFT,fw);title('Wavelet frequency response');xlabel('Frequency (Hz)');ylabel('Normalised magnitude');

figure;plot((0:length(d0)-1)*1000/Fs,d0,'-ob');grid on;title('Impulse response');xlabel('Time (ms)');ylabel('Amplitude');

dl = q + dd; % Add delay from derivative

%disp(dl);

end
