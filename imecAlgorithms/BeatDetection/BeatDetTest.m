%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : BeatDetTest.m
% Content   : Matlab script for test of beat detection algorithm without wrapper using synthetic waveforms
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

close all
clear all
clc

Fs = 128;
FreqTS = 32768; % Timestamp clock frequency
BlockSize = round(Fs*0.5); % 500 ms blocks
TestDur = 300.0; % Test duration (s)
Nsamp = ceil(Fs*TestDur);
Nblocks = floor(Nsamp/BlockSize);

% General
Params.ts2samRatio    = FreqTS/Fs; % Ratio of timestamp to sample frequency

%% ECG test
disp(' ');
disp('ECG test.');
BeatsPerMin = 102.37 + 61.234*3*0; % Heart rate
Params.SigTypeN = 0; % 0:ECG, 1:PPG, 2:BioZ
Params.CWT_FIR        = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv');  % 0: Load ECG  CWT coefficients
Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
Params.gain           = 0.48799; % Scaling for peak tracking to threshold
Params.StdScale       = 1.00000; % Scaling for standard deviation to offset
Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
Params.holdoffSamples = round(100.0*Fs/1000); % Hold-off duration 100.0 ms
Params.GroupDelay     = 13; % Symmetric filter delay, 13 = (27-1)/2

% Generate test data
Morph = importdata('Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv'); % Load CWT coefficients
Samp = randn(1,Nsamp)*0.01*0; % Noise amplitude
Nbeats = floor((length(Samp) - length(Morph))*BeatsPerMin/(Fs*60));

% Off-grid composition
MorphUS10 = resample([0 Morph 0], 10, 1); % 10x upsample
for n = 1:Nbeats-1
    idx = n*Fs*60/BeatsPerMin + 1;
    disp(['Beat timestamps: ',num2str((idx-1+Params.GroupDelay)*FreqTS/Fs)]);
    idxI = round(idx);
    idxF = idx - idxI;
    Samp(idxI:idxI+length(Morph)-1) = Samp(idxI:idxI+length(Morph)-1) + interp1(MorphUS10,(10:10:length(Morph)*10+1)-idxF*10,'cubic'); % Cubic interpolation
end

figure;plot((0:length(Samp)-1)/Fs,Samp); xlabel('Time (s)'); title('ECG test data');

% Debug
FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
FindPeaks_max             = zeros(1,BlockSize*Nblocks);
FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);

State = []; % Initial (empty) state
AllBeatTimeStamps =[];
for Blk = 0:Nblocks-1
    Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
    InputSamples = Samp(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract samples for this block
    [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params);
    
    % Concatenate debug data
    FindPeaks_InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
    FindPeaks_cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
    FindPeaks_threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
    FindPeaks_max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
    FindPeaks_edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
    FindPeaks_holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
    FindPeaks_maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
    
    disp(['ECG BeatTimestamps: ',num2str(BeatTimestamps)]);
    AllBeatTimeStamps = [AllBeatTimeStamps BeatTimestamps]; %#ok<AGROW>
end
disp(' ');
figure;plot(diff(AllBeatTimeStamps)/32768);title('ECG R-R intervals');
figure;plot(60*32768./diff(AllBeatTimeStamps));title('ECG Heart rate');ylim([30 240]);

figure;hold on;
plot((0:Nsamp-1)/Fs,Samp,'b');
BeatAmp = interp1((0:Nsamp-1)/Fs,Samp,AllBeatTimeStamps/FreqTS);
plot(AllBeatTimeStamps/FreqTS,BeatAmp,'or');

% Debug plots
figure; hold on;
plot(Samp, 'k');
plot(FindPeaks_InputSamples, 'b');
plot(FindPeaks_threshold, 'r');
plot(FindPeaks_max, 'm');
plot(FindPeaks_holdoffcounter/10, '*k');
plot(FindPeaks_maxindx/10, 'ob');

plot(FindPeaks_InputSamples./(FindPeaks_edge ==  1), '^b');
plot(FindPeaks_InputSamples./(FindPeaks_edge == -1), 'vb');

legend('Input','|cwt|','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('ECG debug');

% Save input and output test data
if (0)
    dlmwrite('ECGbeatDetInputSamp.csv', Samp, 'newline','pc', 'precision',8);
    dlmwrite('ECGbeatDetOutputBeatTimestamps.csv', AllBeatTimeStamps, 'newline','pc', 'precision',8);
end

%% PPG test
disp(' ');
disp('PPG test.');
BeatsPerMin = 35.678*0 + 61.234; % Heart rate
% SigType = 1; % 0:ECG, 1:PPG, 2:BioZ
% Params.CWT_FIR        = importdata('Wavelets/Coeffs/PPGwavelet8_128HzDiffLPF.csv');  % 1: Load PPG  CWT coefficients
% Params.attack         = 1 - exp(-1000/(1.7000*Fs)); % 1.7000 ms
% Params.decay          = 1 - exp(-1000/(1000.0*Fs)); % 1000.0 ms -- 37% after 1 sec.
% Params.gain           = 0.48799; % Scaling for peak tracking to threshold
% Params.StdScale       = 1.00000; % Scaling for standard deviation to offset
% Params.OffUpdateFblk  = round(Fs*   2.50/BlockSize);     % 2.50 s  Offset update fast number of blocks given duration
% Params.OffUpdateFast  = 1 - exp(-1/(0.75*Fs/BlockSize)); % 0.75 s  Offset update fast (during first OffUpdateFblk sample blocks)
% Params.OffUpdateSlow  = 1 - exp(-1/(5.00*Fs/BlockSize)); % 5.00 s  Offset update slow (after first OffUpdateFblk sample blocks)
% Params.holdoffSamples = round(200.0*Fs/1000); % Hold-off duration 200.0 ms
% Params.GroupDelay     = 61; % Asymmetric filter delay

%
Params.SigTypeN               = 1 ;                                % 0:ECG, 1:PPG, 2:BioZ
Params.attack          = 1 - exp(-1000/(1.7000*Fs));        % Tatt = 1.7000 ms  Attack time constant
Params.decay           = 1 - exp(-1000/(450.00*Fs));        % Tdec = 450.00 ms  Decay time constant
Params.gain            = 0.60;                              % Kth  = 0.60       Scaling for peak tracking to threshold
Params.StdScale        = 0.70;                              % Kstd = 0.70       Scaling for standard deviation to offset
Params.OffUpdateFblk   = round(Fs*   2.50/BlockSize);       % Tbfu = 2.50 s     Fast update time for offset update, gives number of blocks
Params.OffUpdateFast   = 1 - exp(-1/(0.50*Fs/BlockSize));   % Tfu  = 0.50 s     Offset update fast time constant during initial Tbfu sec.
Params.OffUpdateSlow   = 1 - exp(-1/(5.00*Fs/BlockSize));   % Tsu  = 5.00 s     Offset update slow time constant after initial Tbfu sec.
Params.holdoffSamples  = round(450.0*Fs/1000);              % Thof = 450.0 ms   Hold-off duration
Params.GroupDelay      = 63.0;                              % Filter delay (get from wavelet information)
Params.CWT_FIR         = importdata('Wavelets/Coeffs/PPGwavelet10_128HzDiffLPF.csv');  % 1: Load PPG  CWT coefficients 12 Hz passband, 2nd order numerical differentiation
%

% Generate test data
cwt = importdata('Wavelets/Coeffs/PPGwavelet4_128HzSupp.csv'); % Load CWT coefficients
Morph = cumsum(fliplr(cwt)); % Reverse coefficients to get positive time and integrate
Samp = 1.0 + randn(1,Nsamp)*0.03*0; % DC offset and noise amplitude
Nbeats = floor((length(Samp) - length(Morph))*BeatsPerMin/(Fs*60));

% Off-grid composition
MorphUS10 = resample([0 Morph 0], 10, 1); % 10x upsample
for n = 1:Nbeats-1
    idx = n*Fs*60/BeatsPerMin + 1;
    idxI = round(idx);
    idxF = idx - idxI;
    Samp(idxI:idxI+length(Morph)-1) = Samp(idxI:idxI+length(Morph)-1) + interp1(MorphUS10,(10:10:length(Morph)*10+1)-idxF*10,'cubic'); % Cubic interpolation
end

figure;plot((0:length(Samp)-1)/Fs,Samp); title('PPG test data');

% Debugging...
figure; hold on;
plot(0:length(Samp)-1,8*Samp,'o-b');
plot(0:length(Samp)-1,filter(fliplr(cwt),1,Samp),'o-g');
plot((0:length(Samp)-2)+0.5,50*diff(Samp),'o-m');
title('PPG data after CWT');legend('Input','RevFilt','Diff');
xlabel('Samples');

cmp = zeros(1,Nblocks); % Debug
off = cmp;

% Debug
FindPeaks_InputSamples    = zeros(1,BlockSize*Nblocks);
FindPeaks_cmp             = zeros(1,BlockSize*Nblocks);
FindPeaks_threshold       = zeros(1,BlockSize*Nblocks);
FindPeaks_max             = zeros(1,BlockSize*Nblocks);
FindPeaks_edge            = zeros(1,BlockSize*Nblocks);
FindPeaks_holdoffcounter  = zeros(1,BlockSize*Nblocks);
FindPeaks_maxindx         = zeros(1,BlockSize*Nblocks);

State = []; % Initial (empty) state
AllBeatTimeStamps =[];
for Blk = 0:Nblocks-1
    Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
    InputSamples = Samp(Blk*BlockSize+1:Blk*BlockSize+BlockSize); % Extract samples for this block
    [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params);
    
    % Concatenate debug data
    FindPeaks_InputSamples(1+Blk*BlockSize:(Blk+1)*BlockSize)   = State.Debug_FindPeaks_InputSamples;
    FindPeaks_cmp(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_cmp;
    FindPeaks_threshold(1+Blk*BlockSize:(Blk+1)*BlockSize)      = State.Debug_FindPeaks_threshold;
    FindPeaks_max(1+Blk*BlockSize:(Blk+1)*BlockSize)            = State.Debug_FindPeaks_max;
    FindPeaks_edge(1+Blk*BlockSize:(Blk+1)*BlockSize)           = State.Debug_FindPeaks_edge;
    FindPeaks_holdoffcounter(1+Blk*BlockSize:(Blk+1)*BlockSize) = State.Debug_FindPeaks_holdoffcounter;
    FindPeaks_maxindx(1+Blk*BlockSize:(Blk+1)*BlockSize)        = State.Debug_FindPeaks_maxindx;
    
    if ~isempty(BeatTimestamps), disp(['PPG BeatTimestamps: ',num2str(BeatTimestamps(1,:))]); end % Just upstrokes
    AllBeatTimeStamps = [AllBeatTimeStamps BeatTimestamps]; %#ok<AGROW>
end
disp(' ');
figure;plot(diff(AllBeatTimeStamps)/32768);title('PPG R-R intervals');
figure;plot(60*32768./diff(AllBeatTimeStamps));title('PPG Heart rate');

% Debug plots
figure; hold on;
plot(Samp, 'k');
plot(FindPeaks_InputSamples, 'b');
plot(FindPeaks_threshold, 'r');
plot(FindPeaks_max, 'm');
plot(FindPeaks_holdoffcounter/10, '*k');
plot(FindPeaks_maxindx/10, 'ob');

plot(FindPeaks_InputSamples./(FindPeaks_edge ==  1), '^b');
plot(FindPeaks_InputSamples./(FindPeaks_edge == -1), 'vb');

legend('Input','CLcwt','Thres','max','HOcnt','MaxIn');
xlabel('Samples'); title('PPG debug');

figure;hold on;
plot((0:Nsamp-1)/Fs,Samp,'b');
BeatAmp = interp1((0:Nsamp-1)/Fs,Samp,AllBeatTimeStamps/FreqTS);
plot(AllBeatTimeStamps/FreqTS,BeatAmp,'or');

% Save input and output test data
if (0)
    dlmwrite('PPGbeatDetInputSamp.csv', Samp, 'newline','pc', 'precision',8);
    dlmwrite('PPGbeatDetOutputBeatTimestamps.csv', AllBeatTimeStamps, 'newline','pc', 'precision',8);
end

Fs
