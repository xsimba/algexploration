%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : FiltAmp.m
% Content   : Matlab script for low pass filtering of the raw ECG, PPG or BioZ signal and picking off the amplitude values for the relevant features.
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

function data = FiltAmp(data, SigID, PassBandFreq)
%FiltAmp Filter raw ECG/PPG/BioZ input using LPF with desired pass band frequency and extract amplitudes at desired time points.

% To be called after BDstruc

%% Input parser
LenSigID = length(SigID);
if ((LenSigID < 3)||(LenSigID > 5)), error('SigID must contain between 3 and 5 characters only: ecg, ppg.{a..g} or bio.{i|q}.'); end

Char = char(SigID);
SigType = cellstr(Char(1:3)); % First 3 characters

% General and defaults
Fs        = 128;   % When Fs field is absent, use this default value
InputTimestamps = [];    % When timestamp field is absent

% Signal type specific processing for input
if     strcmpi(SigType,'ecg')
    SigTypeN  = 0; % 0:ECG, 1:PPG, 2:BioZ
    SigInfo   = data.ecg;
    
elseif strcmpi(SigType,'ppg')
    SigTypeN  = 1; % 0:ECG, 1:PPG, 2:BioZ
    ChChar    = Char(5);
    if (~isfield(data.ppg,ChChar)), error(['Field data.ppg.',ChChar,' does not exist.']); end
    SigInfo   = data.ppg.(ChChar);
    
elseif strcmpi(SigType,'bio')
    SigTypeN  = 2; % 0:ECG, 1:PPG, 2:BioZ
    ChChar    = Char(5);
    if (~isfield(data.bio,ChChar)), error(['Field data.bio.',ChChar,' does not exist.']); end
    SigInfo   = data.ppg.(ChChar);
    
else
    error('Unknown SigType, expecting ecg, ppg.{a..h} or bio.{i|q}');
end

% Extract fields
                                    InputStream     = SigInfo.signal;
if (isfield(SigInfo,'timestamps')), InputTimestamps = SigInfo.timestamps; end % If field exists, get timestamps, otherwise test for alternative format...
if (isfield(data,'timestamps')),    InputTimestamps = data.timestamps;    end % If field exists, get timestamps, otherwise use default empty vector.
if (isfield(SigInfo,'fs')),         Fs              = SigInfo.fs;         end % If field exists, update sample frequency, otherwise use default.


%% Filter design: special note - force DC response to be exactly 0dB, otherwise points will not seem to match original signal (with a relatively high DC offset)!
TransitionWidthHz = 3.0; % Transition width (Hz)

h = fdesign.lowpass('fp,fst,ap,ast', PassBandFreq, PassBandFreq + TransitionWidthHz, 0.1, 60, Fs);
Hd = design(h, 'equiripple', 'MinOrder', 'even', 'StopbandShape', 'linear'); % Even order guarantees an integer sample delay
q = grpdelay(Hd);
FiltGd = q(1);
FiltLen = length(Hd.Numerator);

DCresp = sum(Hd.Numerator); % Filter response at DC
Hd.Numerator = Hd.Numerator/DCresp; % Compensate response to obtain 0dB at DC.
%fvtool(Hd,'Fs',Fs); % Only if you really want to see it..

if (0) % Test: filter alignment
    Nsamp = 500;
    xx = 1 + 0.1*sin((0:Nsamp-1)*2*pi*2.6/Fs).*(1-cos((0:Nsamp-1)*2*pi/(Nsamp-1)))/2; % 2.6 Hz
    yy = filter(Hd.Numerator,1,[ones(1,FiltLen)*xx(1) xx ones(1,FiltLen)*xx(end)]); % Filter with lead in & out
    yy = yy(FiltLen+FiltGd+1-2:end-FiltGd+1); % Compensate for lead in/out and filter group delay BUT include 2 samples on both ends for interpolation
    figure;hold on;plot(0:Nsamp-1,xx,'b');plot(-2:Nsamp+1,yy,'r');grid on;title('Filter output alignment test');xlabel('Samples');
end


%% Apply filter
FiltSig = filter(Hd.Numerator,1,[ones(1,FiltLen)*InputStream(1) InputStream ones(1,FiltLen)*InputStream(end)]); % Filter with lead in & out
FiltSig = FiltSig(FiltLen+FiltGd+1-2:end-FiltGd+1); % Compensate for lead in/out and filter group delay BUT include 2 samples on both ends for interpolation

if (0) % Test: plot input and filtered signal
    LenInputStream = length(InputStream);
    figure;hold on;plot((0:LenInputStream-1)/Fs,InputStream,'b');plot((-2:LenInputStream+1)/Fs,FiltSig,'r');grid on;
    title('Raw input and filtered signals');xlabel('Time (s)');
end

% Create timestamps if they don't exist
if isempty(InputTimestamps), InputTimestamps = (0:length(InputStream)-1)/fs; end

% For each timing point, use quadratic interpolation to get the amplitude
if isfield(SigInfo,'bd') % Check that BD was called
    if (SigTypeN == 0) % ECG
        LenFeat = length(SigInfo.bd.rpeak);
        SigInfo.filtsig.rpamp = NaN(1,LenFeat);
        for feat = 1:LenFeat
            % R-peak only for ECG
            t = SigInfo.bd.rpeak(feat); 
            IdxF = find(InputTimestamps >= t,1,'first'); % Find timestamp with the first equal or greater time
            Fr = (t - InputTimestamps(IdxF))*Fs;
            if (IdxF > 0), SigInfo.filtsig.rpamp(feat) = QuadraticInterpolation(FiltSig(IdxF-1+2), FiltSig(IdxF+2), FiltSig(IdxF+1+2), Fr); end % filtered signal index offset by 2
        end
    else % PPG/BioZ
        LenFeat = length(SigInfo.bd.upstroke);
        SigInfo.filtsig.usamp = NaN(1,LenFeat);
        SigInfo.filtsig.ftamp = NaN(1,LenFeat);
        SigInfo.filtsig.ppamp = NaN(1,LenFeat);
        SigInfo.filtsig.dnamp = NaN(1,LenFeat);
        SigInfo.filtsig.spamp = NaN(1,LenFeat);
        for feat = 1:LenFeat
            % Up stroke
            t = SigInfo.bd.upstroke(feat); 
            IdxF = find(InputTimestamps >= t,1,'first'); % Find timestamp with the first equal or greater time
            Fr = (t - InputTimestamps(IdxF))*Fs;
            if (IdxF > 0), SigInfo.filtsig.usamp(feat) = QuadraticInterpolation(FiltSig(IdxF-1+2), FiltSig(IdxF+2), FiltSig(IdxF+1+2), Fr); end % filtered signal index offset by 2
            
            % Foot
            t = SigInfo.bd.foot(feat);
            IdxF = find(InputTimestamps >= t,1,'first'); % Find timestamp with the first equal or greater time
            Fr = (t - InputTimestamps(IdxF))*Fs;
            if (IdxF > 0), SigInfo.filtsig.ftamp(feat) = QuadraticInterpolation(FiltSig(IdxF-1+2), FiltSig(IdxF+2), FiltSig(IdxF+1+2), Fr); end % filtered signal index offset by 2
            
            % Primary peak
            t = SigInfo.bd.pripeak(feat);
            IdxF = find(InputTimestamps >= t,1,'first'); % Find timestamp with the first equal or greater time
            Fr = (t - InputTimestamps(IdxF))*Fs;
            if (IdxF > 0), SigInfo.filtsig.ppamp(feat) = QuadraticInterpolation(FiltSig(IdxF-1+2), FiltSig(IdxF+2), FiltSig(IdxF+1+2), Fr); end % filtered signal index offset by 2
            
            % Dicrotic notch
            t = SigInfo.bd.dicrnot(feat);
            IdxF = find(InputTimestamps >= t,1,'first'); % Find timestamp with the first equal or greater time
            Fr = (t - InputTimestamps(IdxF))*Fs;
            if (IdxF > 0), SigInfo.filtsig.dnamp(feat) = QuadraticInterpolation(FiltSig(IdxF-1+2), FiltSig(IdxF+2), FiltSig(IdxF+1+2), Fr); end % filtered signal index offset by 2
            
            % Secondary peak
            t = SigInfo.bd.secpeak(feat);
            IdxF = find(InputTimestamps >= t,1,'first'); % Find timestamp with the first equal or greater time
            Fr = (t - InputTimestamps(IdxF))*Fs;
            if (IdxF > 0), SigInfo.filtsig.spamp(feat) = QuadraticInterpolation(FiltSig(IdxF-1+2), FiltSig(IdxF+2), FiltSig(IdxF+1+2), Fr); end % filtered signal index offset by 2
        end
    end
    
    SigInfo.filtsig.signal = FiltSig(3:end-2); % Strip off the added 2 samples at start and end (will cause confusion if left in!)
    
    % Insert signal and amplitudes into their correct location
    if     (SigTypeN == 0), data.ecg          = SigInfo; % ECG
    elseif (SigTypeN == 1), data.ppg.(ChChar) = SigInfo; % PPG
    else                    data.bio.(ChChar) = SigInfo; % BioZ
    end
    
else
    warning('It does not appear that BDstruc was called as the field .bd was not found. No amplitudes were calculated.');
end

end % function


%% Sub functions

% Quadratic curve fitting for interpolation (X is usually a scalar but can be a vector)
function Yout = QuadraticInterpolation(PrevY, CurrY, NextY, X)
% Given the quadratic y = a*x^2 + b*x + c and 3 Y values:
% When x = -1, PrevY = a - b + c
% When x =  0, CurrY =         c
% When x = +1, NextY = a + b + c
% Solving:
a = (NextY + PrevY - 2*CurrY)*0.5;
b = (NextY - PrevY)*0.5;
c = CurrY;

if (max(abs(X)) > 0.5), warning('Interpolation expects values between -0.5 and +0.5!'); end

Yout = (a*X + b).*X + c;

end

