%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : <RunEvaluation2>
% Content   : Run the Signal evaluation script
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% input:
% Simba data struct
% output: annotations per second of data for each channel
% modified by E.C. Wentink  . .--
%  |  ECWentink  |   2.0   | 24-06-2014| make it work for simba data, add more parameters in window
%  |  ECWentink  |   2.2   | 02-05-2015| bug fixes, file updates
%%%%%%%%%%%%
clear all;close all;clc
folder_name = '.\Example files\Data\';
file_name = 'Metr_SP1_WRi_OO_1_Ncal';
load([ folder_name '\' file_name '.mat' ]);
if  exist('data','var')
output=data;
end
files = {'ppg.a' 'ppg.a' 'ppg.a' 'ppg.a' 'ppg.a' 'ppg.a' 'ppg.a' 'ppg.a' 'ecg'};
nr=input('which one do you want to do? (1= PPG1, 2=PPG2, 3=PPG3, 4=PPG4, 5=PPG5, 6=PPG6, 7=PPG7, 8=PPG8, 9=ECG and  0=quit)')


input.fs=128;
if nr==1
    input.PPG =output.ppg.a.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==2
    input.PPG =output.ppg.b.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==3
    input.PPG =output.ppg.c.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==4
    input.PPG =output.ppg.d.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==5
    input.PPG =output.ppg.e.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==6
    input.PPG =output.ppg.f.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==7
    input.PPG =output.ppg.g.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==8
    input.PPG =output.ppg.h.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return
elseif nr==9
    input.PPG =output.ecg.signal;
    input.Acc= [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
    filename =[ file_name '_' files{nr}];
    SignalEvaluation(filename,input,nr);
    return 
end





