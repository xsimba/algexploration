classdef SignalEvaluation
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : <SignalEvaluation>
% Content   : ECG confidence evaluation script
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl) and Torfinn Berset
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% SIGNALEVALUATION
% Program for manual evaluation of signal quality
%
% Usage: "SignalEvaluation('filename.mat')"
%
% 'filename.mat' refers to a .mat file with a signal 'x' and a sampling
% frequency 'fs'.
%
% During the evaluation, you are requested to enter a score for each
% segment, by entering a number between 1 and 5. Once a number is entered,
% you will be shown the next segment to evaluate.
%
% The final scores will
% be saved in a file named '<filename>_result.mat'. Please forward this
% file to your contact person after completing the evaluation.
%
% Thank you for your time.
%
% Author Torfinn.Berset@imec-nl.nl
% Date   25/07/2012 - 27/07/2012
% modified by E.C. Wentink  . .--
%  |  ECWentink  |   2.0   | 24-06-2014| make it work for simba data, add more parameters in window
%  |  ECWentink  |   2.2   | 02-05-2015| bug fixes

    
    properties (Access=public)
        % Changable parameters
        window = 1; % Length of window to evaluate (in seconds)\
        minval = 1;  % Minimum value allowed for evaluation
        maxval = 5;  % Maximum value allowed for evaluation
        
%         data2fft
    end
    
    properties (Access=private)
        % DO NOT MODIFY PARAMETERS BELOW             
        fig
        fs;     
        data    
        data2
        data1
        f
        a
        data2fftemp
        data2fftemp1
        data1fftemp
        data1fftemp1
        data2fft
        position = 0;
        time; 
        nr
        infile
        outfile
        nsegments = 0;        
    end
    
    
%TODO: add segment value frame
    
    methods
        function this = SignalEvaluation(filename,input,nr)
            
            % Initialization
            this.infile = filename
            this.nr=nr;
%             data = load(this.infile);
            data = input;
                        
            this.fs = data.fs;
            this.outfile = [ this.infile, '_result.mat'];
            
            this.data = data.Acc;
            this.time = (0:length(this.data)-1) ./ this.fs;
            this.nsegments = ceil( length(this.data) / (this.window*this.fs) );
                        
%             this.data2 = data.PPG2;
            this.data2 = data.PPG;
            %%
            T = 1/this.fs;   
            L = 4*this.fs;   % Length of signal
            NFFT = 2^nextpow2(L);
            this.f = this.fs/2*linspace(0,1,NFFT/2+1);
            this.data2fftemp = zeros(this.nsegments,(2*this.fs)+1);
            this.data2fftemp1 = zeros(this.nsegments,(4*this.fs));
            this.data1fftemp = zeros(this.nsegments,(2*this.fs)+1);
            this.data1fftemp1 = zeros(this.nsegments,(4*this.fs));
            % slightly filter the data21
            [bb,aa]=butter(2,[0.1/(this.fs/2) 30/(this.fs/2)],'bandpass'); %bandpass filter
            this.data2=filter(bb,aa,this.data2);
            this.data1=filter(bb,aa,this.data);
            for pp = 4:1:this.nsegments-1          
                this.data2fftemp1(pp,:) = fft(this.data2((pp-3)*this.fs:pp*this.fs),NFFT)/L;
                this.data2fftemp(pp,:) = 2*abs(this.data2fftemp1(pp,1:NFFT/2+1)); 
                %fft of the acc channels
                this.data1fftemp1(pp,:) = fft(this.data1((pp-3)*this.fs:pp*this.fs),NFFT)/L;
                this.data1fftemp(pp,:) = 2*abs(this.data1fftemp1(pp,1:NFFT/2+1)); 
            end               
%             this.data2fftemp(this.nsegments-4:end,:) = repmat(this.data2fftemp(this.nsegments-5,:),5,1);
            %%
            % Basic input testing
            assert( this.maxval > this.minval, 'Minval must be larger than Maxval!');
            assert( this.fs > 0, 'Sampling frequency must be a positive number');
            assert( this.window > 0, 'Window size must be a positive number!');
            assert( this.nsegments > 0, 'No segments to evaluate!');
            
            titlestr = sprintf('Please enter a score between %d and %d for the segments', this.minval, this.maxval );
            
            this.fig = figure(...
            'Position', [200 500 1257 391], ...                
       ... 'NumberTitle','off','Menubar','none',...
            'Name',titlestr,...
            'KeyPressFcn', @(obj,evt) this.keyboardMap(obj,evt) );
                        
            this = init(this);
        end
        
        function this = init(this)
            % Set up figure and callback to keypressfcn
            % Set up figure and callback to keypressfcn
            L = 4*this.fs;   % Length of signal
            NFFT = 2^nextpow2(L);
            this.a=1;
            sub1 = subplot(4,2,1);plot(this.f, 2*abs(this.data2fftemp(this.a,1:NFFT/2+1)));xlim([min(this.f) 10]); xlabel('PPG Frequency (Hz)');
            sub2 = subplot(4,2,2);plot(this.f, 2*abs(this.data1fftemp(this.a,1:NFFT/2+1)));xlim([min(this.f) 10]); xlabel('Acc Frequency (Hz)');
            sub3 = subplot(4,2,[3 4]);plot(this.time, this.data);  xlim([-5 this.window]);
            sub4 = subplot(4,2,[5:8]);plot(this.time, this.data2);linkaxes([sub3 sub4],'x');xlim([-2 this.window*3]);
            text(this.window,0,'evaluate selection', 'VerticalAlignment','Top','HorizontalAlignment','right',	'FontSize',14);vline([0,1])
%           plot(this.time, this.data);
            grid on;
            
            title(sprintf( 'Segment #%d of #%d', 1, this.nsegments ) );               
            
            ylabel('Amplitude');
            xlabel('Time [s]');
            
            % This can be handy for giving progress update to user
            fprintf('%d segments to analyze! Grab a coffee and let''s get started\n', this.nsegments);
            
            scorebox = annotation('textbox', [ 0.93, 0.5, 0.05, 0.1 ] );
            set(scorebox, 'FontSize', 16, 'String', 'X');
            
            % Scores are initialized to NaN
            evalStruct = struct('position', 0, 'segment', 1, 'scores', NaN(1, this.nsegments), 'scorebox', scorebox );
            
            % Current [Position Segment] is stored in UserData
            set(gcf,'UserData', evalStruct);
            
%             xlim([-5 this.window]);
        end
        
        function this = keyboardMap( this, ~, event )
            
            evalStruct = get(gcf,'UserData'); % Position is stored in UserData
            c = event.Character;
            n = str2double(c);
            
            if n >= this.minval && n <= this.maxval % Check for valid point value
                                                
                evalStruct.scores(evalStruct.segment) = n; % Add new score to end
                evalStruct.position = evalStruct.position + this.window;
                evalStruct.segment = evalStruct.segment + 1;
                
                title(sprintf( 'Please evaluate this Segment #%d of #%d', evalStruct.segment, this.nsegments ), 'FontSize',14);
                
                if this.nsegments >= evalStruct.segment
                    % Still data left                  
                    a=evalStruct.segment*this.window;
                    L = 4*this.fs;   % Length of signal
                    NFFT = 2^nextpow2(L);
                    sub1 = subplot(4,2,1);plot(this.f, 2*abs(this.data2fftemp(evalStruct.segment,1:NFFT/2+1)));xlim([min(this.f) 10]);xlabel('Frequency (Hz)', 'FontSize',12);title('FFT of 4sec PPG', 'FontSize',12)
                    sub2 = subplot(4,2,2);plot(this.f, 2*abs(this.data1fftemp(evalStruct.segment,1:NFFT/2+1)));xlim([min(this.f) 10]);xlabel('Frequency (Hz)', 'FontSize',12);title('FFT of 4sec Acc', 'FontSize',12)
                    sub3 = subplot(4,2,[3 4]);plot(this.time, this.data); title('Accelerometer data', 'FontSize',12);
                    sub4 = subplot(4,2,[5:8]);plot(this.time, this.data2);axis auto;linkaxes([sub3 sub4],'x'); title('PPG data', 'FontSize',12);
                    xlim( [((evalStruct.segment)*this.window)-2*this.window,((evalStruct.segment)*this.window)+2*this.window] );vline([a-0.5,a+0.5])
                 
                    set(gcf,'UserData', evalStruct);
                else
                    % Last segment evaluated
                    result = evalStruct.scores
                    
                    if any( isnan(result) )
                        % Not done yet!
                         nonevaluatedsegments = sprintf('%d, ', find(isnan(result)));           
                         msgstring = sprintf('Not all segments are evaluated! Missing results from the following segments: %s', nonevaluatedsegments);
                         msgbox(msgstring, 'Incomplete evaluation');
                         return;
                    end
                    
                    windowLength = this.window;
                    fs = this.fs;
                    save(this.outfile, 'result', 'windowLength', 'fs');
                    close all;
                    
                    msgstring = sprintf('All done for this one! Thank you for your time. Please continue with nr: %s', num2str(this.nr+1));
                    
                    
                    %% Plot some results
                    timevec = linspace( 0, windowLength * (length(result)-1), length(result) );
                    figure('Name', 'Final result');
                    stairs(timevec, result);
                    xlabel('Time [s]');
                    ylabel('Score [points]');
                    ylim([0 6]);
                    
                    figure('Name', 'Final result, histogram');
                    hist(result, 5);
                    ylabel('#');
                    xlabel('Score [points]');
                    xlim([1 5]);
                    
                    
                                        
                    msgbox(msgstring,'Evaluation complete');
                    return
                end
                
            end
            
            switch event.Key
                
                case 'leftarrow' % Go back                   
                    if evalStruct.segment > 1
                        evalStruct.position = evalStruct.position - this.window;
                        evalStruct.segment = evalStruct.segment - 1;
%                         this.a=this.a-1;
                    end                                                            
                case 'rightarrow' % Go forward           
                    if evalStruct.segment < this.nsegments
                        evalStruct.position = evalStruct.position + this.window;
                        evalStruct.segment = evalStruct.segment + 1;
%                         this.a=this.a+1;
                    end                                                            
                otherwise
                    % Nothing
            end
            set(gcf,'UserData', evalStruct);
%             xlim( [evalStruct.position, evalStruct.position + this.window] );
%             xlim( [evalStruct.position-this.window, evalStruct.position+1*this.window] )
            xlim( [((evalStruct.segment)*this.window)-2*this.window,((evalStruct.segment)*this.window)+2*this.window] );


            score = evalStruct.scores(evalStruct.segment);
            if isnan(score)
                scoreboxstr = 'X';
            else
                scoreboxstr = num2str(score);                
            end
                        
            set( evalStruct.scorebox, 'String', scoreboxstr );
            title(sprintf( 'Please evaluate this segment #%d of #%d', evalStruct.segment, this.nsegments ) , 'FontSize',14);
            
        end
    end    
end