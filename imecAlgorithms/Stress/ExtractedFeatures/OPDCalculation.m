% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : OPDCalculation.m
%  Content    : Compute ohmic perturbation duration
%  Inputs     : eda    - input signal
%               signal - signal type "gtec","empatica" or "simband"
%  Output     : OPD
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 12.06.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 12-06-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------


function OPD = OPDCalculation(eda, signalType)


if strcmp(signalType,'gtec')
    Fs = 128;
    smoothIndex = 1;
elseif strcmp(signalType,'empatica')
    Fs = 4;
    smoothIndex = 0;
elseif strcmp(signalType,'simband')
    Fs = 32;
    smoothIndex = 1;
else
    display('data type not supported!')
end

if smoothIndex
    edasmooth = smooth(eda, 2*Fs);
    edasmooth = edasmooth(3*Fs:end-3*Fs); %removed ringing caused by smoothing - 1st and last 7 seconds
else 
    edasmooth = smooth(eda, 2*Fs);
end

rawndiff = diff(edasmooth, 2); %compute first difference
rawndiff = rawndiff - mean(rawndiff); %normalize
if smoothIndex
    rawndiff = smooth(rawndiff, 5*Fs); %smooth the signal
    rawndiff(1:7*Fs) = 0; rawndiff(end-7*Fs:end)=0;
end
rawndiff = abs(rawndiff); %normalize difference
th = mean(rawndiff)-0.1*mean(rawndiff); %same threshold as the one used in SCRRCalculation

plotIndex = 0;
if plotIndex
    t = 1:length(edasmooth);
    figure;
    plot(t./Fs,(edasmooth-mean(edasmooth))./10000,'k'); hold on;
    plot(t(2:end-1)./Fs,rawndiff*2,'b')
    line([1 length(rawndiff)/Fs], [th th],'Color','r')
end

OPDIndex = rawndiff > th;
if plotIndex
    plot(t(OPDIndex)./Fs,(edasmooth(OPDIndex)-mean(edasmooth))./10000,'g.')
end

OPD = sum(OPDIndex == 1)/Fs; %convert sample number to seconds

