% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : SCRRCalculation.m
%  Content    : Compute number of responses from the eda signal
%  Inputs     : eda    - input signal
%               signal - signal type "gtec","empatica" or "simband"
%  Output     : SCRR
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 12.06.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 12-06-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------


function SCRR = SCRRCalculation(eda,signalType,plotIndex)

if strcmp(signalType,'gtec')
    Fs = 128;
    smoothIndex = 1;
elseif strcmp(signalType,'empatica')
    Fs = 4;
    smoothIndex = 1;
elseif strcmp(signalType,'simband')
    Fs = 32;
    smoothIndex = 1;
else
    display('data type not supported!')
end

minTimeBetweenResponses = 0.2; %arbitrarily chosen - required during peak selection to avoid noise being picked up

if smoothIndex
    edasmooth = smooth(eda, 2*Fs);
else 
    edasmooth = eda;
end
rawndiff = diff(edasmooth, 1); %compute first difference
rawndiff = rawndiff-mean(rawndiff); %normalize difference


if plotIndex
    figure; plot((eda-mean(eda))./1000); hold on;
    plot(rawndiff,'k');
end

%compute threshold for the derivative as the mean derivative signal level
th = mean(rawndiff)-0.1*mean(rawndiff);
if plotIndex
    line([1 length(eda)],[th, th],'Color','r')
end


rawndiff(rawndiff<th) = 0; %select only values above the threshold
if smoothIndex
    rawndiff = smooth(rawndiff,1*Fs); %smooth the derivative to eliminate noise
    rawndiff(1:3*Fs) = 0; rawndiff(end-3*Fs:end)=0; %remove ringing caused by smoothing - 1st and last 7 seconds are zeroed
end
if plotIndex
    plot(rawndiff,'g')
    l = legend({'EDA Raw','1st Diff','Thresh','Above Thresh'});
end

%Detect the peaks in the first derivative
counterAsc = 0; % count the ascending samples
counterDesc = 0; %count the descending samples
SCRR = 0;
SCRRLocation = [];
mpd = minTimeBetweenResponses*Fs; %mean peak distance


for ind = 1:length(rawndiff)-1
    noiseLevel = (rawndiff(ind)+rawndiff(ind+1))/2; %try to eliminate detections do to the low noise level
    if rawndiff(ind)+noiseLevel < rawndiff(ind+1)
        counterAsc = counterAsc+1;
        if counterDesc ~=0
            counterDesc = 0;
        end
    elseif rawndiff(ind) > rawndiff(ind+1)+noiseLevel
        counterDesc = counterDesc+1;
        if counterAsc ~=0
            counterAsc = 0;
            if isempty(SCRRLocation)
                SCRR = SCRR+1;
                SCRRLocation = [SCRRLocation, ind+1];
            elseif (ind+1 -SCRRLocation(end) ) > mpd
                SCRR = SCRR+1;
                SCRRLocation = [SCRRLocation, ind+1];
            end
        end
    elseif rawndiff(ind) == 0
        counterDesc = 0;
        counterAsc = 0;
    end
end

if plotIndex
    figure;
    plot(eda); hold on
    plot(SCRRLocation,eda(SCRRLocation),'r+')
end






