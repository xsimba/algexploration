% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : SCLCalculation.m
%  Content    : Compute mean SCL 
%  Inputs     : eda
%  Output     : SCL
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 27.05.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 27-05-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function meanSCL = SCLCalculation(eda)

    meanSCL = mean(eda);

end





