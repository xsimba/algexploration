% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : HRVCalculation.m
%  Content    : Compute HRV in the frequency domain
%  Output     : LF, HF, LFHF
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 11.06.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 11-06-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [LF,HF,LFHF] = HRVCalculation(feature,signalType)

if size(feature,2) == 1
    feature = feature';
end

    if strcmp(signalType,'simband')
        Fs = 128;
        feature_t = 1:1/Fs:length(feature);
        ibi = diff(feature(2:end));
       
    elseif strcmp(signalType,'empatica')
        Fs = 64;
        feature_t = 1:1/Fs:length(feature);
        ibi = feature;
        
    elseif strcmp(signalType,'gtec')
        Fs = 128;
        feature_t = 1:1/Fs:length(feature);
        ibi = diff(feature(2:end));
        
    else
        display('signalType not supported!')
    end
    
%     HR  = 60000*(1./ibi); %in bpm
%     figure; plot(HR);
    
    HR = ibi*1000;

    t = feature_t(1):1/Fs:feature_t(end);
    HR = interp1(HR,t,'linear','extrap');
    HR = HR-mean(HR);
%     figure; subplot(211); plot(HR)
    HR = smooth(HR,10*Fs);
    HR = HR(7:end-7)';
%     subplot(212); plot(HR)
    
    L = length(HR);
    nfft = 2^nextpow2(L);
    win = hanning(L)';
    win = win./sum(win);
    hrHanning = HR.*win;
    hrHanningFFT = fft(hrHanning,nfft)/L;
    
    hrHanningFFT = abs(hrHanningFFT);
    hrHanningFFT = hrHanningFFT(1:nfft/2+1);
%     hrHanningFFT (2:end-1) = 2*hrHanningFFT(2:end-1);
    hrHanningFFT  = 2*hrHanningFFT;

    
    f = Fs/2*linspace(0,1,nfft/2+1);
    VLFstart = round(0.005/(Fs/nfft)+1);
    VLFstop = round(0.04/(Fs/nfft));
    LFstart = round(0.04/(Fs/nfft)+1);
    LFstop = round(0.15/(Fs/nfft));
    HFstart = round(0.15/(Fs/nfft)+1);
    HFstop = round(0.4/(Fs/nfft));
    
    plotIndex = 0;
    if plotIndex == 1
        figure('name',signalType);
        subplot(211)
        plot((1:length(HR))./Fs,HR)
        subplot(212)
        plot(f,hrHanningFFT); hold on;
        plot(f(VLFstart:VLFstop),hrHanningFFT(VLFstart:VLFstop),'c')
        plot(f(LFstart:LFstop),hrHanningFFT(LFstart:LFstop),'r')
        plot(f(HFstart:HFstop),hrHanningFFT(HFstart:HFstop),'g')
    end
    
    VLF = sum(hrHanningFFT(VLFstart:VLFstop).^2)*10e9;
    LF = sum(hrHanningFFT(LFstart:LFstop).^2)*10e9;
    HF = sum(hrHanningFFT(HFstart:HFstop).^2)*10e9;
    LFHF = LF/HF;
