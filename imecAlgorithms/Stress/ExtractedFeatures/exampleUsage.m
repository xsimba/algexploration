% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : exampleUsage.m
%  Content    : Example use of the GSR Features Extracted for Stress
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 13.07.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc;

load('Subject8.mat');

%Ohmic perturbation duration
OPDSimband = OPDCalculation(data.simband.gsr.tonic.signal, 'simband')
OPDgtec = OPDCalculation(data.gtec.eda_finger.signal, 'gtec')
OPDempatica = OPDCalculation(data.empatica_left.eda.signal, 'empatica')

%Power in the Second Difference
SCdiff2Simband = SCdiff2Calculation(data.simband.gsr.tonic.signal,32,'simband')
SCdiff2gtec = SCdiff2Calculation(data.gtec.eda_finger.signal,128, 'gtec')
SCdiff2empatica = SCdiff2Calculation(data.empatica_left.eda.signal,4, 'empatica')

%Mean skin conductance level
SCLSimband = SCLCalculation(data.simband.gsr.tonic.signal)
SCLgtec = SCLCalculation(data.gtec.eda_finger.signal)
SCLempatica = SCLCalculation(data.empatica_left.eda.signal)

%Skin conductance response rate
SCRRSimband = SCRRCalculation(data.simband.gsr.tonic.signal, 'simband',1)
SCRRgtec = SCRRCalculation(data.gtec.eda_finger.signal, 'gtec',1)
SCRRempatica = SCRRCalculation(data.empatica_left.eda.signal, 'empatica',1)