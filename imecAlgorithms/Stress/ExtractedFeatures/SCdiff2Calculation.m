% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : SCdiff2Calculation.m
%  Content    : Compute power in the second derivative - different versions
%               for each signal type - eda finger has stronger responses -> more
%               smoothing
%  Inputs     : eda    - input signal
%               fs     - sampling frequency, 
%               signal - signal type "gtec","empatica" or "simband"
%  Output     : SCdiff2
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 27.05.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 27-05-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function SCdiff2 = SCdiff2Calculation(eda,fs,signal)

if strcmp(signal,'gtec')
    edasmooth = smooth(eda, 5 * fs);
    raw2nddiff = diff(edasmooth, 2);
    smoothed1 = smooth(raw2nddiff, 5*fs);
    smoothed2 = smooth(smoothed1, 5*fs);
    smoothed3 = smooth(smoothed2, 5*fs);
    smoothed3 = smoothed3((7 * fs):(end - (7 * fs)));
    SCdiff2 = (sum(smoothed3.^2)) / length(eda); 
elseif strcmp(signal,'empatica')
    raw2ndiff = filter([-0.5 0 0.5],1, [eda(1) eda(1) eda' eda(end) eda(end)]);
    raw2ndiff = raw2ndiff(4:end-1);
    SCdiff2 = (sum(raw2ndiff.^2)) / length(eda);
elseif strcmp(signal,'simband') 
    raw2ndiff = filter([-0.5 0 0.5],1, [eda(1) eda(1) eda eda(end) eda(end)]);
    raw2ndiff = raw2ndiff(4:end-1);
    SCdiff2 = (sum(raw2ndiff.^2)) / length(eda);
else
    display('Data structure not supported')
    return;
end




