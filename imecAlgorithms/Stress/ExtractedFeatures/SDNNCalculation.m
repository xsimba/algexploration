% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : SDNNCalculation.m
%  Content    : Compute standard deviation of interbeat intervals either
%  from computed beats (gtec,simba) or from ibi (empatica)
%  Output     : SDNN
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 27.05.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 27-05-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function SDNN = SDNNCalculation(feature,signalType)

if size(feature, 2) == 1
    feature = feature';
end

if strcmp(signalType,'simband') || strcmp(signalType,'gtec')
    ibi = diff(feature(2:end));
    SDNN = std(ibi);
elseif strcmp(signalType,'empatica')
    SDNN = std(feature);
else
    display('Data structure not supported')
    return;
end

