% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : HRCalculation.m
%  Content    : Compute heart rate from input detected beats (gtec, simba)
%  or input ibi (empatica)
%  Output     : HR
%  Created by : A.M. Tautan(alexandra.tautan@imec-nl.nl)
%  Date       : 27.05.2015
%  Modification and Version History:
%  | Developer | Version        |    Date    |     Changes      |
%  |  atautan  |   0.1          | 27-05-2015 |                  |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function HR = HRCalculation(feature,signal_type)

if size(feature, 2) == 1
    feature = feature';
end

if strcmp(signal_type,'simband') || strcmp(signal_type,'gtec')
    ibi = diff(feature(2:end));
    HR  = 60*(1/mean(ibi));
elseif strcmp(signal_type,'empatica')
    HR  = 60*(1/mean(feature));
else
    display('Data structure not supported')
    return;
end

