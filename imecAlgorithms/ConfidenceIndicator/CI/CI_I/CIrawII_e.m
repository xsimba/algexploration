%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <CI_raw>
% Content   : Main script for CI_raw calculation
% Version   : GIT 3
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History:
%  | Developer   | Version |    Date   |
%  |  ECWentink  |   0.1   | 01-08-2014|
%  |  ECWentink  |   0.2   | 01-08-2014|
%  |  ECWentink  |   0.3   | 08-08-2014|
%  |  ECWentink  |   2.0   | 08-08-2014|
%  |  ECWentink  |   2.1   | 30-09-2014|new model
%  |  ECWentink  |   2.2   | 01-10-2014|fix lit  vs ppg
%  |  ECWentink  |   3.0   | 01-10-2014|new thresholds for ppg and model for ECG for 21.1 data
%  |  ECWentink  |   3.1   | 01-10-2014|fast amp raise removal, extra thresholds low vs medium CI
%  |  ECWentink  |   4.0   | 04-03-2015| only for channel 'ppg.e', now using visual filtred data.
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input: all data, ECG/PPG and ACC data at least and the "channel" which
% you wish to calculte it on
%% output
% CI_raw: the CI_raw on raw data per second
% CI_raw_mn: the CI_raw averaged over 5 second in the past
% CI_DBraw: debug information
% CI_explan: the explanation/reasoning behind the 1-4 selection-> ie "too much motion " etc to
% determine why choise was made-> so easy for debugging
% CI_times: time in seconds!!
%%%%%%%%%%%%%%%%%%%%%%
function [output] = CIrawII_e(input,chan)

output=input;

%% software parameters
nrpulses = 16;
nravg = 2;
cor = nrpulses*nravg; % correction for DC level
% deel p-p door de huidige ACG->betere match -> pas thresholds aan.
sig = eval(['input.ppg.' chan '.signal'])/cor;
% visual filtered data needs to be there
signv = eval(['input.ppg.' chan '.visual.signal'])/cor;

t=input.timestamps;
acc = input.acc.All;

if size(sig,2) == 1
    sig = sig';
end

if isfield(input.ppg.(chan), 'ciraw')
    % Make the windows to match the band windows
    timeOffset = input.ppg.(chan).ciraw.timestamps(find(input.ppg.(chan).ciraw.timestamps >= 0, 1));
    offset = round(timeOffset * 128) + 1;
else
    timeOffset = 0;
    offset = 1;
end
if offset > 0
    sig = sig(offset:end);
    signv = signv(offset:end);
end


%% definitions of the parameters
Fs = 128;
%% windowing
wind = (1*Fs)-1; % 4sec for time domain stuff
windb = (4*Fs)-1; % 1 sec for FFT domain stuff
udr = 1*Fs; % update rate, every second

%% accelerometer data calculations

e = 1;
% size(sig)
DB_inf=zeros(ceil(size(sig,2)/udr),9);
DB_inffilt=zeros(ceil(size(sig,2)/udr),8);
e_top = ceil(min([size(acc,2), size(sig,2)-3])/udr);
CI_N4filt=zeros(e_top,1);
CI_N5filt=zeros(e_top,1);
SKfilt = zeros(e_top,1);
DCshiftfilt =zeros(e_top,1);
sigvals =zeros(e_top,5);
sigvalsfilt =zeros(e_top,5);
accvals =zeros(e_top,5);
CI_p2filt = cell(e_top,1);
t_snr = zeros(e_top,1);   

for a = 1:udr:min([size(acc,2), size(sig,2)-3])
    %% first CI will be 1 due to lack of data
    if (a)<windb
            CI_N4filt(e)=1;
            CI_N5filt(e)=1;
            DB_inf(e,1:9)=0;
            DB_inffilt(e,1:8)=0;
    else
        %% Assign data in 1 sec windows
        acc2sec = acc(4,a-wind:a)'; % 1 sec of only on the magnitude of acc
        sig2sec = sig(a-wind:a)';  % a sec the signal PPG
        signv2sec = signv(a-wind:a)'; % 1 sec visual filtered PPG

        %% calculate parameters
        DCshiftfilt(e) = max(abs(diff(signv2sec/32)));%

        %% calc the the amplitude parameters (min, max, peak peak, mean, std)
        % Vals det calculates the: min, max, Peak-peak, mean and std of the window
        sigvals(e,:) = Valsdet(sig2sec);
        sigvalsfilt(e,:) = Valsdet(signv2sec);
        accvals(e,:) = Valsdet(acc2sec);

        % the skewness
%         SKfilt(e)= skewness((signv2sec)); %with toolbox
        SKfilt(e) = ((((sum((signv2sec-mean(signv2sec)).^3)))/length(signv2sec))/(std(signv2sec))^3);

        
        % Combine the parameters
        DB_inf(e,:) = sigvals(e,4);
        DB_inffilt(e,:) = [sigvalsfilt(e,:),accvals(e,3), SKfilt(e),DCshiftfilt(e)];

        %% run it through the CI==1 detection
        [CI_N4filt(e),CI_p2filt{e}] = TFBDfilt(DB_inffilt(:,:),DB_inf(:,:),e,chan);
        %% check if it was "one" or else

        if mod(CI_N4filt(e),2)% getting the "odd" values
           CI_N5filt(e) = 1; % odd values are now CI==1
        elseif CI_N4filt(e) > 1000
            CI_N5filt(e) = 2; % odd
        else % if CI is not 1 than search for CI>1
           CI_N5filt(e) = 3;
           CI_p2filt{e} = 'Good';
        end
    end
    t_snr(e) = t(a);
    e=e+1;
end


%% the PPG results saved
output.ppg.(chan).mat_CIraw.signal= CI_N5filt'; % the CI per second
output.ppg.(chan).mat_CIraw.timestamps= t_snr' + timeOffset; % the time for the CI

output.ppg.(chan).mat_CIraw.DB.CI_explan_nr= CI_N4filt'; % the explanation in nrs

output.ppg.(chan).mat_CIraw.DB.CI_DBraw = DB_inf; % the debug info (all the parameters, those used and unused)
output.ppg.(chan).mat_CIraw.DB.CI_DBfilt = DB_inffilt; % the debug info (all the parameters, those used and unused)

output.ppg.(chan).mat_CIraw.DB.CI_explan = CI_p2filt'; % the "explanation"
output.ppg.(chan).mat_CIraw.one_sec_dc = sigvals(:,4)*cor;
output.ppg.(chan).mat_CIraw.one_sec_ac = sigvals(:,3)*cor;




