function ax = plotOneBandCI( data)
    okCode = 0 ; 
            
    ppgChannels = 'ee';%'abcdefgh';
    
    figure('units','normalized','outerposition',[0 0 1 1])
    
    for idx = 1:numel(ppgChannels)
        
        if isfield(data.ppg,ppgChannels(idx))
            ax{idx}=subplot(2,1,idx);
            idx_a=idx;
            plotThisBandCI(data,['ppg.',ppgChannels(idx)],idx_a)

        end
    end
    
    linkaxes([ax{:}], 'x');
    okCOde = 1 ; 
end


function plotThisBandCI( data,signal ,idx_a)
    
        okCode = 0 ;  
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953;];
       
        if strcmpi(signal,'ecg')
            
            if isfield(data.ecg ,'band_CIraw') 
                                                
                ci_m = data.ecg.mat_CIraw.signal ;
                ci_m_time = round(data.ecg.mat_CIraw.timestamps);
                ci_m_time = ci_m_time; 

                ci_m_time(ci_m_time<0) = [];

                
                ci_m2 = data.ecg.mat_CIraw.signal ;
                ci_m2_time = round(data.ecg.mat_CIraw.timestamps);
                ci_m2_time = ci_m2_time; 

                ci_m2_time(ci_m2_time<0) = [];
                
                
                s = data.ecg.signal;
                s_times = data.timestamps ; 
                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];



                for idx = 1:length(ci_m_time)
                    try
                        if ci_m(idx)~= 0
                            timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                            plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
                        end
                    catch err
                        if strcmpi(err.identifier,'MATLAB:badsubscript')
                            disp('end of stream');
                        else
                            keyboard()
                        end
                    end
                end 
                ylabel('CI Band');xlabel('Time [s]')
                title(signal)
                set(gca,'XTick',[1:1280:idx+1])
                set(gca,'XTickLabel',10*[0:numel([0:1280:idx])])   
                
            end
            
        elseif strcmpi(signal(1:3),'ppg')
            
            channel = lower(signal(end));
            if isfield(data.ppg.(channel) ,'band_CIraw')
                
 
                ci_m = data.ppg.(channel).mat_CIraw.signal ;
                ci_m_time = data.ppg.(channel).mat_CIraw.timestamps;
                ci_m_time = round(ci_m_time); 
                ci_m_time(ci_m_time<0) = [];
                
                
                ci_m2 = data.ppg.(channel).band_CIraw.signal ;
                ci_m2_time = data.ppg.(channel).band_CIraw.timestamps;
                ci_m2_time = round(ci_m2_time); 
                ci_m2_time(ci_m2_time<0) = [];
                
              
                
%                 s = data.ppg.(channel).signal;
                s_times = data.timestamps ; 

                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];
                


                for idx = 1:length(ci_m_time)-1
                    if idx_a ==1
                       s = data.ppg.(channel).signal;
                    else
                       s = data.ppg.(channel).visual.signal;
                    end
                        
                        
                    try
                        if ci_m(idx)~= 0
                           
                           timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                           plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;plot( s_times(timestamps),s(timestamps)+10000,'Color',col(ci_m2(idx),:),'LineWidth',1.7);%plot( s_times(timestamps),s(timestamps)+20000,'Color',col(ci_m3(idx),:),'LineWidth',1.7)


                        end
                    catch err
                        if strcmpi(err.identifier,'MATLAB:badsubscript')
                            disp('end of stream');
                        else
                            keyboard()
                        end
                    end
                end 
                ylabel('Matlab CI');xlabel('Time [s]');title('Top = band CI, Bottom = Matlab CI')
                title([upper(signal) '     Top = band CI, Bottom = Matlab CI'])

            end
        end
        
        okCode = 1 ;

end