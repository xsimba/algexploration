%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <CI_raw>
% Content   : Main script for CI_raw calculation
% Version   : GIT 3
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History:
%  | Developer   | Version |    Date   |
%  |  ECWentink  |   0.1   | 01-08-2014|
%  |  ECWentink  |   0.2   | 01-08-2014|
%  |  ECWentink  |   0.3   | 08-08-2014|
%  |  ECWentink  |   2.0   | 08-08-2014|
%  |  ECWentink  |   2.1   | 30-09-2014|new model
%  |  ECWentink  |   2.2   | 01-10-2014|fix lit  vs ppg
%  |  ECWentink  |   3.0   | 01-10-2014|new thresholds for ppg and model for ECG for 21.1 data
%  |  ECWentink  |   3.1   | 01-10-2014|fast amp raise removal, extra thresholds low vs medium CI
%  |  ECWentink  |   3.2   | 29-01-2015|cleaned up version of 3.1

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input: all data, ECG/PPG and ACC data at least and the "channel" which
% you wish to calculte it on
%% output
% CI_raw: the CI_raw on raw data per second
% CI_raw_mn: the CI_raw averaged over 5 second in the past
% CI_DBraw: debug information
% CI_explan: the explanation/reasoning behind the 1-4 selection-> ie "too much motion " etc to
% determine why choise was made-> so easy for debugging
% CI_times: time in seconds!!
%%%%%%%%%%%%%%%%%%%%%%
function [output] = CI_raw(input,chan)

if strcmp(chan,'ecg')
    sig = input.ecg.signal;
    if isfield(input,'timestamps') 
        t=input.timestamps;
    else
    t=input.ecg.timestamps;
    end
else
    sig = eval(['input.ppg.' chan '.signal']);
    if isfield(input,'timestamps') 
        t=input.timestamps;
    else
        t = eval(['input.ppg.' chan '.timestamps']);
    end
    
%     t_snr = 
end
acc = input.acc.All;

if size(sig,2) == 1
    sig = sig';
end
% if size(acc,2) == 4
%     acc = acc';
% end

%% definitions of the parameters
Fs = 128;
lolo=1;
%% windowing
wind = 4*Fs-1; % 4sec for FFT stuff
winda = 1*Fs-1; % 1 sec for time domain stuff
udr = 1*Fs; % update rate, every second

%% accelerometer data calculations
output=input;
e = 1;
take=0;
amp=110;
for a = 1:udr:min([size(acc,2), size(sig,2)])
    %% first CI will be 1 due to lack of data
    if a<wind
        CI_I(e)=1;
        if strcmp(chan,'ecg')
            DB_inf(e,1:6)=0;
        else
            DB_inf(e,1:12)=0;
        end
    else
        %% Assign data in 4 sec windows
        idx4 = (a-wind:a)-1;
        acc4sec = acc(4,idx4)'; % only on the magnitude of acc
        sig4sec = sig(idx4)';  % the signal PPG/ECG
        
        %% Assign data in 1 second windows
        idx2 = (a-winda:a)-1;
        acc2sec = acc(4,idx2)'; % 1 sec window of magnitude ACC
        acc12sec = acc(1,idx2)';% 1 sec window x-axis ACC
        acc22sec = acc(2,idx2)';% 1 sec window y-axis ACC
        acc32sec = acc(3,idx2)';% 1 sec window z-axis ACC
        sig2sec = sig(idx2)'; % 1 sec window PPG/ECG
        
        sig2sec2 = sig2sec - mean(sig2sec);% 1 sec window of PPG/ECG mean subtracted
        
        
        %% %%%%%%% Calc FFT %%%%%%%%%%%%%%
        [sigfft(e,:),fsig] = DAT2FFT2(sig4sec,Fs);
        % calculate the FFT of acc
        [accfft(e,:),facc] = DAT2FFT2(acc4sec,Fs);
        
        % frequency bin/ step size
        jump = ((128/2)/(wind/2));
        sta = round((0.5/jump))+1; % in indices the start of the 0.5 HZ frequency
        sto = round((4/jump))+1;% in indices the stop of the 4 HZ frequency
        
        %% Calc the innproduct of acc and ppg
        Csig(e) = dot(sigfft(e,:),accfft(e,:));% Take dot product
        
        %% calc the the amplitude parameters (min, max, peak peak, mean, std)
        % Vals det calculates the: min, max, Peak-peak, mean and std of the window
        sigvals(e,:) = Valsdet(sig2sec);
        
        % calc the the amplitude parameters ecg
        accvals(e,:,:) = Valsdet(acc2sec);
        accvals_1(e,:,:) = Valsdet(acc12sec);
        accvals_2(e,:,:) = Valsdet(acc22sec);
        accvals_3(e,:,:) = Valsdet(acc32sec);
        
        %% Covariance calculation of the PPG/ECG
        sigcov(e) = cov(sig2sec);

        %% Mean of the signal FFT in 0.5-4Hz window
        mep(e) = mean(sigfft(e,sta:sto));        
        %% skewness
        SK(e)=skewness(sig2sec2);

        %% end of unused
        
        
        if strcmp(chan,'ecg')
            DB_inf(e,:) = [sigvals(e,:),Csig(e)];
            [~,lolo,~] = CI_1ECG(Csig(e),sigvals(e,:),lolo);
            lolo1(e)=lolo;
            
        else
            
            % Not all the variables are used, but may be useful for MAR -> don't
            % implement al in C
            % Used: 1 (mean fft ppg), 4 (peak-peak PPG), 5 (mean PPG),6 (std ppg), 7
            % (covariance ppg), 8 cdot of ffft ppg and fft acc magnitude,
            % 10 skew, 11 max acc-x, 12 max acc-y, 13 max acc-z ( 11,12 and 13 always used together,
            % ie the max of the 3 is taken, so can be calculated differently if
            % required)
            DB_inf(e,:) = [mep(e), sigvals(e,:), sigcov(e), Csig(e) ,SK(e),accvals_1(e,3),accvals_2(e,3),accvals_3(e,3)];
            [CI_N4(e),CI_p2{e}] = Treeforbaddata(DB_inf(:,:),e);
            
            
            if mod(CI_N4(e),2)% getting the "odd" values
                CI_N5(e) = 1;
            else % low motion
                CI_N5(e) = 8;
            end
            
           
            CI_N6(e)=CI_N5(e); % make new array to fill with CI and get the last CI's between 1 and 4
           % %% checking out what is left and does not have a CI yet, first the "low motion" windows
            if CI_N6(e) ==8 % for low motion
                mn_cov(e) = DB_inf(e,4)/DB_inf(e,7); % the peak-peak divided by the covariance
                if mn_cov(e) <=0.01   &&  mn_cov(e) >0.002 && DB_inf(e,7) >= 200000 && DB_inf(e,1)>100
                    CI_N6(e) =4;
                    CI_p2{e} = 'LM,high cov, good amp/cov, good mn fft';
                elseif mn_cov(e) <=0.01   &&  mn_cov(e) >0.002 && DB_inf(e,7) >= 200000
                    CI_N6(e) =3;
                    CI_p2{e} = 'LM,high cov, good amp/cov';
                elseif mn_cov(e) <=0.01   &&  mn_cov(e) >0.002 && DB_inf(e,7) >= 15000 && DB_inf(e,7) < 200000
                    CI_N6(e) =3;
                    CI_p2{e} = 'LM,med cov, good amp/cov';
                elseif mn_cov(e) <=0.01   &&  mn_cov(e) >0.002 && DB_inf(e,7) < 15000
                    CI_N6(e) =2;
                    CI_p2{e} = 'LM,low cov, good amp/cov';
                end
                
                if mn_cov(e) <= 0.15  &&  mn_cov(e) >0.01 && DB_inf(e,7) > 4000 && abs(DB_inf(e,9)) < 0.45 && abs(DB_inf(e,9)) >0.2
                    CI_N6(e) = 4;
                    CI_p2{e} = 'LM,med cov, med amp/cov, good skew';
                elseif mn_cov(e) <= 0.15  &&  mn_cov(e) >0.01 && DB_inf(e,7) <= 4000   && abs(DB_inf(e,4)) < 0.5
                    CI_N6(e) = 3;
                    CI_p2{e} = 'LM,med cov2, med amp/cov';
                elseif mn_cov(e) <= 0.15  &&  mn_cov(e) >0.01 && DB_inf(e,7) > 2000 %&& DB_inf(e,2) > 300 && DB_inf(e,2) < 20000
                    CI_N6(e) = 3;
                    CI_p2{e} = 'LM,med cov3, med amp/cov';
                elseif mn_cov(e) <=0.15   &&  mn_cov(e) >0.01 && DB_inf(e,7) <= 2000
                    CI_N6(e) = 2;
                    CI_p2{e} = 'LM,low cov, med amp/cov';
                end
                
                if mn_cov(e) <=0.5   &&  mn_cov(e) >0.15
                    CI_N6(e) = 2;
                    CI_p2{e} = 'LM,OK amp/cov';
                elseif mn_cov(e) > 0.5
                    CI_N6(e) = 1;
                    CI_p2{e} = 'LM,bad amp/cov';
                end
                
                if mn_cov(e) <= 0.002  && DB_inf(e,7) >= 200000
                    CI_N6(e) = 4;
                    CI_p2{e} = 'LM,high cov, excellent amp/cov';
                elseif mn_cov(e) <= 0.002  && DB_inf(e,7) > 10000 && DB_inf(e,7) < 200000 && DB_inf(e,8) < 4
                    CI_N6(e) = 3;
                    CI_p2{e} = 'LM,med cov, excellent amp/cov, good cdot';
                elseif mn_cov(e) <= 0.002  && DB_inf(e,7) > 10000 && DB_inf(e,7) < 200000
                    CI_N6(e) = 2;
                    CI_p2{e} = 'LM,med cov, excellent amp/cov';
                elseif mn_cov(e) <= 0.002  && DB_inf(e,7) <= 10000
                    CI_N6(e) = 1;
                    CI_p2{e} = 'LM,low cov, excellent amp/cov';
                end
                
            end
            

            %% if any 8 or 6 left make them 2 (there should not be any)
            if  CI_N6(e) ==8 || CI_N6(e) ==6
                CI_N6(e) = 2;
                CI_p2{e} = 'last resort';
            end
            
            %% average out the CI -> becomes CI_raw_mn
            if e>5
                CI_N7(e)=round(mean(CI_N6(e-5:e))*2)/2; % average to the nearest half
            else
                CI_N7(e)=CI_N6(e); % if there are not enough samples yet do not average
            end
            
        end
    end
    e=e+1;
end
clear e
t=t';
t_snr = t(1:udr:size(sig,2));%
t_snr=t_snr';

%% The ECG model
if strcmp(chan,'ecg')
    load('modelE_8.mat')
    
    output.ecg.mat_CIraw.DB.vals = sigvals;
    output.ecg.mat_CIraw.DB.CI_DBraw = DB_inf;
    
    CI_t = predict(mdlE,DB_inf(:,1:6));
    for a=1:1:length(CI_t)
        CI_I(a)=str2num(CI_t{a,1});
    end
    % save the results of ECG
    output.ecg.mat_CIraw.signal = CI_I;
    output.ecg.mat_CIraw.DB.CI_times = t_snr;
    output.ecg.mat_CIraw.DB.lolo = lolo1;
    output.ecg.mat_CIraw.timestamps = t_snr;
    output.ecg.mat_CIraw.one_sec_dc = DB_inf(:,4);
    output.ecg.mat_CIraw.one_sec_ac = DB_inf(:,3);

else
    %% the PPG results saved
    eval(['output.ppg.' chan '.mat_CIraw.signal= CI_N6;']); % the CI per second
    eval(['output.ppg.' chan '.mat_CIraw.timestamps= t_snr;']); % the time for the CI
    eval(['output.ppg.' chan '.mat_CIraw.DB.signal_mn= CI_N7;']); % the mean over 5 sec of the CI
    eval(['output.ppg.' chan '.mat_CIraw.DB.CI_DBraw = DB_inf;']); % the debug info (all the parameters, those used)
    eval(['output.ppg.' chan '.mat_CIraw.DB.CI_explan = CI_p2;']); % the "explanation"
    output.ppg.(chan).mat_CIraw.one_sec_dc = DB_inf(:,5);
    output.ppg.(chan).mat_CIraw.one_sec_ac = DB_inf(:,4);

end



