%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Treeforbaddata>
% Content   : Script for recognizing the "bad signal parts"
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      : 15/10/2014
%  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input : all the debug information from CI_raw
% and the window number: "e"
% output: different noise causes => all "odd numbers" are converted to 1
% -> ie bad data-> all even numbers are potentially good data but will be
% checked further
%%%%%%%%%%%%%%%%%%%%%%

function [CI,p] = Treeforbaddata(DB_inf_t ,e)

CI_check = 'unknown';
while strcmp('unknown',CI_check)
    %% check fo flat line
    if DB_inf_t(e,6)  <= -3|| DB_inf_t(e,6) <= 3
        CI = 1;
        p='flat line';
        CI_check = 'known';
        return
        %% check for too much motion or other
    elseif DB_inf_t(e,6)  <= -600 || DB_inf_t(e,6) >= 600 %% was 2800 to be evaluated => should not filter out very good signal!!
        CI = 11;
        p='too much';
        CI_check = 'known';
        return
   elseif DB_inf_t(e,6)  < 15 %% to be evaluated => should not filter out very good signal!!
        CI = 111;
        p='not enough1';
        CI_check = 'known';
        return

    elseif DB_inf_t(e,4)  > 1800 %% to be evaluated => should not filter out very good signal!!
        CI = 111;
        p='not enough2';
        CI_check = 'known';
        return
    end
%%     Check for Saturation at top or bottom
    if DB_inf_t(e,5) >= 87776 && DB_inf_t(e,5) <= 87777
            CI = 3;
            p='saturation';
            CI_check = 'known';
            return
    elseif DB_inf_t(e,5) >= -87777 && DB_inf_t(e,5) <= -87776 
            CI = 13;
            p='saturation';
            CI_check = 'known';
            return
    end
%% check if there is too much motion ie std acc max x,y,z > 0.3 CI is 1 

if max(DB_inf_t(e,[10,11,12])) > 0.4 % 
   p='severe motion';
   CI = 19;
   CI_check = 'known';
   return
elseif max(DB_inf_t(e,[10,11,12]))  <= 0.3 %
   p='no/low motion';
   CI = 96;
   CI_check = 'known';
   return
end


    %% Check if not too much motion but low covariance or low FFT content than nothing available
if max(DB_inf_t(e,[10,11,12])) >= 0.2% low acc 
    if DB_inf_t(e,7) < 1e-3
        CI=5;
        CI_check = 'known';
        p='saturation top/bottom';
        return
    elseif DB_inf_t(e,1) < 5
        CI=7;
        CI_check = 'known';
        p='white noise';
        return
    end
end

%% If there is little motion the covariance can say something on the quality of the signal
   if DB_inf_t(e,7) <= 300
        CI=15;%1
        CI_check = 'known';
        p= 'low covariance';
        return
   end
   
   %% check for spikes
if e>1
    if DB_inf_t(e,6)> 2.5*DB_inf_t(e-1,6)
        CI=25;
        CI_check = 'known';
        p = 'spike in the data STD much higher than previous';
     return
    end
end
   

%% if still nothing available mark as unknown
if strcmp('unknown',CI_check)
    CI=22;
    CI_check = 'known';
    p = 'unknown';
    return
end
    
end
