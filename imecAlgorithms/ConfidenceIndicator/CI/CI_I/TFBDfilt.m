%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Treeforbaddata>
% Content   : Script for recognizing the "bad signal parts"
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      : 15/10/2014
%  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input : all the debug information from CI_raw
% and the window number: "e"
% output: different noise causes => all "odd numbers" are converted to 1
% -> ie bad data-> all even numbers are potentially good data but will be
% checked further
%%%%%%%%%%%%%%%%%%%%%%

function [CI,p] = TFBDfilt(DB_inf_t,DB_inf_t_nf ,e,chan)
% wordt deze wel gebruikt?
CI_check = 'unknown';
while strcmp('unknown',CI_check)
    %% check fo flat line

    if strcmp(chan,'e')

        if DB_inf_t(e,5)  > 468 && DB_inf_t(e,6)  <0.2 %           
           CI = 127;
           p='too much2';
           CI_check = 'known';
           return
        elseif DB_inf_t(e,7) > 0.2 
           CI = 131;
           p='Skew';
           CI_check = 'known';
        elseif DB_inf_t(e,4) > 156%5000 
           CI = 135;
           p='MeanUp';
           CI_check = 'known';
        elseif DB_inf_t(e,5)  < 0.3%10 %
           CI = 125;
           p='data loss';
           CI_check = 'known';
           return
        elseif DB_inf_t(e,3)  > 250  && DB_inf_t(e,6)  <0.2  %8000 
           CI = 129;
           p='Xcorup';
           CI_check = 'known';
           return
        elseif DB_inf_t(e,6)  > 0.6144% 
           CI = 139;
           p='AccUp';
           CI_check = 'known';
           return
        elseif abs(DB_inf_t(e,8))> 10/32
           CI = 149;
           p='DCUp';
           CI_check = 'known';
           return
% 
%         elseif DB_inf_t_nf(e,1)  > -1200  %
%             CI = 111;
%             p='wrist off';
%             CI_check = 'known';
%             return
%         elseif DB_inf_t(e,6)/DB_inf_t(e,3)  > 0.004  %
%             CI = 111;
%             p='Acc/amp';
%             CI_check = 'known';
%             return
        elseif DB_inf_t(e,6)  > 0.15 && DB_inf_t(e,6)/DB_inf_t(e,3)  > 0.0005  %DB_inf_t(e,6)  > 0.07 && DB_inf_t(e,6)/DB_inf_t(e,3)  > 0.0005  %
           CI = 1010;
           p='AccUp2';
           CI_check = 'known';
           return
        elseif DB_inf_t(e-1,3)*1.75 < DB_inf_t(e,3)%  > 0.03 && DB_inf_t(e,6)/DB_inf_t(e,3)  > 0.0001  %
           CI = 101;
           p='spike';
           CI_check = 'known';
           return
        end
     end
           

   

%% if still nothing available mark as unknown
if strcmp('unknown',CI_check)
    CI=22;
    CI_check = 'known';
    p = 'unknown';
    return
end
   
end
