function [CI_1P_d,lolo,CI] = CI_1ECG(C,ECGvals,lolo)

% Lead on/off
if ECGvals(1) >= 55000
%     disp('lead on detected')
    lolo=1;
elseif ECGvals(2) <= -35000
    lolo=0;
%     disp('lead off detected')
else
    lolo=lolo;
end
% lolo =1;
if lolo ==1
%     %% CI raw PPG 1
%     thXCP1flc = 0.5;%0.002;
%     thXCP1fhc = 30;%0.1;%0.0045;
%     if C >  thXCP1fhc %||  SNRPPG < thSNRPl 
%        xcppgf =  0; %red
%     elseif C <=  thXCP1fhc &&  C >= thXCP1flc  %&&  SNRPPG <=  thSNRPh  &&  SNRPPG >= thSNRPl
%        xcppgf = 1-((C-thXCP1flc)/(thXCP1fhc-thXCP1flc));
%     elseif C < thXCP1flc  %  &&  SNRPPG > thSNRPh
%        xcppgf = 1;%green
%     end
    
    
    te1 = 4; %0.5
    te2 = 25;%15;%150
    te3 = 50;%25%30;%300
%     te4 = %50;%500
    if C <= te1
        nrc = 4;
    elseif C > te1 && C <= te2
        nrc = 3;
    elseif C > te2 && C <= te3 
        nrc = 2;
%     elseif C > te3 && C <= te4
%         nrc = 2;
     else
        nrc = 1;
    end

    
    
    
    
    
    
    
    
    
    %% Maximum
    % Thresholds
    % Max value ECG
    ME = ECGvals(1);
    ta1 = -3000;
    ta2 = -2000;%-1000;
    ta3 = 2000;%500;
    ta4 = 4000;%1000;
    % ta5 =

    if ME <= ta1
        nrme = 1;
    elseif ME > ta1 && ME <= ta2
        nrme = 4;
    elseif ME > ta2 && ME <= ta3 
        nrme = 3;
    elseif ME > ta3 && ME <= ta4
        nrme = 2;
     else
        nrme = 1;
    end


    %% Minimum
    % min value ECG good!
    MI = ECGvals(2);
    tb1 = -10000;
    tb2 = -6000;
    tb3 = -4700;%-5000;
    tb4 = -4000;%-4300
    % tb5 =
    if MI <= tb1
        nrmi = 1;
    elseif MI > tb1 && MI <= tb2
        nrmi = 2;
    elseif MI > tb2 && MI <= tb3 
        nrmi = 3;
    elseif MI > tb3 && MI <= tb4
        nrmi = 4;
     else
        nrmi = 1;
    end

    %% Peak Peak
    % Peak-peak value ECG good for 3-4?
    PP = ECGvals(3);
    tc1 = 1500; %
    tc2 = 2600;%3400;
    tc3 = 5000; %6000
    tc4 = 25000;
    % tc5 = 

    if PP <= tc1
        nrpp = 0;
    elseif PP > tc1 && PP <= tc2
        nrpp = 4;
    elseif PP > tc2 && PP <= tc3 
        nrpp = 3;
    elseif PP > tc3 && PP <= tc4
        nrpp = 2;
     else
        nrpp = 1;
    end

    %% Standard deviation
    % STD ECG good for 2 vs 3 not for lolo
    SD = ECGvals(5);
    td1 = 250; %/280
    td2 = 380;%320
    td3 = 600;
    td4 = 1200;%1000
    % td5 =
    if SD <= td1
        nrsd = 1;
    elseif SD > td1 && SD <= td2
        nrsd = 4;
    elseif SD > td2 && SD <= td3 
        nrsd = 3;
    elseif SD > td3 && SD <= td4
        nrsd = 2;
     else
        nrsd = 1;
    end
    CI_1P_d = [lolo,nrc,nrme,nrmi,nrpp,nrsd,ME,MI,PP,SD];
    
else
    CI_1P_d = [lolo,0,0,0,0,0,0,0,0,0];
end

if mean(CI_1P_d(2:5)) <2
    CI = 1;
elseif mean(CI_1P_d(2:5)) <=2.8
    CI = 2;
elseif mean(CI_1P_d(2:5)) > 2.8 && mean(CI_1P_d(2:5)) <3.2
    CI = 3;
else mean(CI_1P_d(2:5)) >= 3.2;
    CI = 4;
end

% mean(CI_1P_d(1:8))
% CI
% endxcppgf