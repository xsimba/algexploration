%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <CI_raw>
% Content   : Main script for CI_raw calculation
% Version   : GIT 3
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History:
%  | Developer   | Version |    Date   |
%  |  ECWentink  |   0.1   | 01-08-2014|
%  |  ECWentink  |   0.2   | 01-08-2014|
%  |  ECWentink  |   0.3   | 08-08-2014|
%  |  ECWentink  |   2.0   | 08-08-2014|
%  |  ECWentink  |   2.1   | 30-09-2014|new model
%  |  ECWentink  |   2.2   | 01-10-2014|fix lit  vs ppg
%  |  ECWentink  |   3.0   | 01-10-2014|new thresholds for ppg and model for ECG for 21.1 data
%  |  ECWentink  |   3.1   | 01-10-2014|fast amp raise removal, extra thresholds low vs medium CI
%  |  ECWentink  |   4.0   | 04-03-2015| only for channel 'ppg.e', now using visual filtred data.
% |  ECWentink  |   5.0   | 07-05-2015| only for ECG now using visual filterd data and also attempting lead on off (ie value 0 and 1 are most likely lead or or very bad quality data, 2 is doubtful, 3 and 4 should be good ECG data).
% |  ECWentink  |   5.2   | 21-05-2015| added unit conversion
% |  ECWentink  |   5.2_a   | 24-06-2015| removed unit conversion
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input: all data, ECG/PPG and ACC data at least and the "channel" which
% you wish to calculte it on
%% output
% CI_raw: the CI_raw on raw data per second
% CI_raw_mn: the CI_raw averaged over 5 second in the past
% CI_DBraw: debug information
% CI_explan: the explanation/reasoning behind the 1-4 selection-> ie "too much motion " etc to
% determine why choise was made-> so easy for debugging
% CI_times: time in seconds!!
%%%%%%%%%%%%%%%%%%%%%%
function [output] = ECGCIrawv5_2a(input, offset)

output=input;

% deel p-p door de huidige ACG->betere match -> pas thresholds aan.
sig = input.ecg.signal;
% visual filtered data needs to be there
signv = input.ecg.visual.signal;
t=input.timestamps;

if isfield(input,'acc')
acc = input.acc.All;
else
    acc = zeros(4,length(sig));
end


if size(sig,2) == 1
    sig = sig';
end

if nargin <= 1
    offset = 1;
end
if isfield(input.ecg, 'ciraw')
    % Make the windows to match the band windows
    timeOffset = input.ecg.ciraw.timestamps(find(input.ecg.ciraw.timestamps >= 0, 1));
    offset = round(timeOffset * 128) + 1;
else
    timeOffset = 0;
end
if offset > 0
    sig = sig(offset:end);
    acc = acc(:,offset:end);
end

%% definitions of the parameters
Fs = 128;
%% windowing
wind = (1*Fs)-1; % 4sec for time domain stuff
udr = 0.5*Fs; % update rate, every second

%% accelerometer data calculations
e = 1;
DB_inf=zeros(round(size(sig,2)/udr),5);
DB_inffilt=zeros(round(size(sig,2)/udr),8);
e_top = ceil(min([size(acc,2), size(sig,2)-3])/udr);
SKfilt = zeros(e_top,1);
sigvals =zeros(e_top,5);
sigvalsfilt =zeros(e_top,5);
accvals =zeros(e_top,5);
CI_p2filt = cell(e_top,1);       

for a = 0:udr:min([size(acc,2), size(sig,2)-1])
    %% first CI will be 1 due to lack of data
    if (a)<wind
            DB_inf(e,1:5)=0;
            DB_inffilt(e,1:8)=0;
            
    else
        %% Assign data in 1 sec windows
        acc2sec = acc(4,a-wind:a)'; % 1 sec of only on the magnitude of acc
        sig2sec = sig(a-wind:a)';  % a sec the signal PPG
        
        sig2sec_t = sig(a-wind:a+1)';  % a sec the signal PPG
        signv2sec = signv(a-wind:a+1)';  % a sec the signal PPG
        sigdiff = diff(sig2sec_t);
        
        %% calc the the amplitude parameters (min, max, peak peak, mean, std)
        % Vals det calculates the: min, max, Peak-peak, mean and std of the window
        sigvals(e,:) = Valsdet(sig2sec);
        sigvalsfilt(e,:) = Valsdet(signv2sec);
        accvals(e,:) = Valsdet(acc2sec);
        %% the skewness without the signal processing toolbox
        SKfilt(e) = ((((sum((signv2sec-mean(signv2sec)).^3)))/length(signv2sec))/(std(signv2sec))^3)*-1;
        SK(e) = ((((sum((sig2sec-mean(sig2sec)).^3)))/length(sig2sec))/(std(sig2sec))^3)*-1;
        %% additional parameters
        exi(e) = ((sigvals(e,4)-sigvals(e,1))./sigvals(e,1)) - (((sigvals(e,4)-sigvals(e,1))./sigvals(e,1)) - ((sigvals(e,4)-sigvals(e,2))/sigvals(e,2)));
%         exifilt(e) = ((sigvalsfilt(e,4)-sigvalsfilt(e,1))./sigvalsfilt(e,1)) - (((sigvalsfilt(e,4)-sigvalsfilt(e,1))./sigvalsfilt(e,1)) - ((sigvalsfilt(e,4)-sigvalsfilt(e,2))/sigvalsfilt(e,2)));
        exifilt(e) = (sigvalsfilt(e,4)-sigvalsfilt(e,2))  ./(sigvalsfilt(e,4)-sigvalsfilt(e,1));
        exi2(e) = (sigvals(e,4)-sigvals(e,2))  ./(sigvals(e,4)-sigvals(e,1));
        
        difcorr(e) = max(xcov(sig2sec));
        DB_inf(e,:) = sigvals(e,:);
        DB_inffilt(e,:) = [sigvalsfilt(e,:),accvals(e,3), SKfilt(e),exi(e)];
        medecg(e) = median(sig2sec);
        max_amp =15000;%((15000)./ 2^18).*(1/24);%15000;
        max_sd = 1200;%((1200)./ 2^18).*(1/24);%1200;
        ti=0;
        thexifilt = -2;
        while ti== 0;
        if DB_inf(e,4)> 800;%((800)./ 2^18).*(1/24); %800
            tempi(e) = 0;
            ti=1;
            break
        elseif DB_inf(e,5)< 100;%((100)./ 2^18).*(1/24);%100
            tempi(e)=0;
            ti=9;
            break
        elseif DB_inf(e,4)<-9000;%((-9000)./ 2^18).*(1/24);%-9000
            tempi(e)=0;
            ti=2;
            break
            
        elseif DB_inffilt(e,7)<-2%  
            tempi(e)=4;
            ti=13;
            break
      elseif exi(e) <=0.2 && exi(e)>=-0.2 && DB_inf(e,3)<max_amp/2 && DB_inf(e,5)< max_sd/2    
           
            if DB_inf(e,5)>200;%((200)./ 2^18).*(1/24);%200
             tempi(e)=4;
             ti=5;
            else                
             tempi(e)=3;
             ti=14;
            end
                
            break
       elseif exi(e) <=0.3 && exi(e)>=-0.3 && DB_inf(e,3)<max_amp && DB_inf(e,5)< max_sd/2  
            tempi(e)=4;
            ti=10;
            break
       elseif exi(e) <=0.3 && exi(e)>=-0.3 && DB_inf(e,3)<max_amp &&   DB_inf(e,5)< max_sd 
            tempi(e)=3;
            ti=11;
            break
%         
       elseif (exi(e) >0.4 &&   exi(e) <=0.55  && DB_inf(e,3)<max_amp  ) || (exi(e) <-0.4  &&  exi(e) >=-0.55 && DB_inf(e,3)<max_amp  )% 0.35
            tempi(e) = 1;
            ti=4;
            break
        elseif (exi(e) >0.3 &&   exi(e) <=0.4   && DB_inf(e,3)<max_amp  ) || (exi(e) <-0.3  &&  exi(e) >=-0.4  && DB_inf(e,3)<max_amp   )  
            tempi(e) = 2;
            ti=3;
            break
        elseif DB_inf(e,3)>=max_amp
            tempi(e)=0;
            ti=6;
            break
        elseif DB_inf(e,5)>=max_sd
            tempi(e)=0;
            ti=7;
            break
        else
            tempi(e)=0;
            ti=8;
            break
        end
        
        end
  tg(e)=ti;
    end
    e=e+1;
end
clear e
t_snr = t(1:udr:size(sig,2)) + timeOffset;

%% the PPG results saved
% Main outputs
output.ecg.mat_CIraw.timestamps= t_snr'; % the time for the CI
output.ecg.mat_CIraw.signal=tempi;

%% Debug parameters
output.ecg.mat_CIraw.DB.CI_DBraw = DB_inf; % the debug info (all the parameters, those used and unused)
output.ecg.mat_CIraw.DB.CI_DBfilt = DB_inffilt; % the debug info (all the parameters, those used and unused)
output.ecg.mat_CIraw.DB.CI_DBrawacc =accvals;
output.ecg.mat_CIraw.DB.SK=SK;
output.ecg.mat_CIraw.DB.exi=exi;
output.ecg.mat_CIraw.DB.exi2=exi2;

output.ecg.mat_CIraw.DB.exifilt=exifilt;

output.ecg.mat_CIraw.DB.medecg = medecg;
output.ecg.mat_CIraw.DB.difcorr=difcorr;
output.ecg.mat_CIraw.DB.tg=tg;
output.ecg.mat_CIraw.one_sec_dc = DB_inf(:,4);
output.ecg.mat_CIraw.one_sec_ac = DB_inf(:,3);
