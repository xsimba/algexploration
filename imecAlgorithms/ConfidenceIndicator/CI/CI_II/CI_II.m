%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <CI_I>
% Content   : Main script for CI_I calculation
% Version   : GIT 0
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%  |  ECWentink  |   0.1   | 01-08-2014|
%  |  ECWentink  |   0.2   | 01-08-2014|
%  |  ECWentink  |   0.3   | 08-08-2014|
%  |  ECWentink  |   0.5   | 08-08-2014| changes for C incorporated
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input
%%%%%%%%%%%%%%%%%%%%%%
function [output] = CI_II(input,chan)
            
%     beats = input.ecg.beats_ts/input.ecg.DB.FreqTS;%
%     beatsa = input.ppg.a.beats_ts(1,:)/input.ppg.a.DB.FreqTS;
    
if strcmp(chan,'ecg')
beats = input.ecg.beats_ts/input.ecg.DB.FreqTS;
else
beats = eval(['input.ppg.' chan '.beats_ts(1,:)/input.ppg.a.DB.FreqTS;']);
CI_I = eval(['input.ppg.' chan '.CI_raw;']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ecg %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
output=input;

%% ECG
if strcmp(chan,'ecg')
output.ecg.CI_beat = zeros(1,size(beats,2));
%for every beat in beats
for e = 1:1:size(beats,2)
%4 second window    
beats_E = beats(beats<=beats(e) & beats>(beats(e)-4));
%if no beats or single beat
if size(beats_E,2) <= 1
    CI2_1t(e) = 1;
    continue;
end

SDBE(e)=std(beats_E.*128);
MBPE(e)=mean(diff(beats_E.*128));
SDMBE(e)=std(diff(beats_E.*128));
% [MBPE(e) SDBE(e) SDMBE(e)]

ti = ceil(beats_E(end));
lolo=1;
if(size(input.ecg.lolo,2) >= ti)
    lolo = input.ecg.lolo(ti);
else
%     lolo(e) = 0;
    break;
end
[CI_2E_d(e,:),CI2_E(e)] = CI_2ECG(SDMBE(e),SDBE(e),MBPE(e),lolo);
% output.ecg.CI_beat(e) = CI2_E(e);
output.ecg.lolo_beat(e) = lolo;


end
% return
else
%% PPG
%for every beat in beatsa
for e = 1:1:size(beats,2)
CI2_1t = zeros(1,size(beats,2));
    
%4 second window
beats_a =  beats(beats<=beats(e) & beats>(beats(e)-4));
% 
% if size(beats_a)==0
%    beats_a = nan;
% else
% end
% beats_a

%if no beats or single beat
if size(beats_a,2) <= 1
    CI2_1t(e) = 1;
    CI_2P1_d(e,1:6)= nan;
    continue;
end



[SDBP1(e), MBP1(e),SDMBP1(e),~,~,~,~,~,~] = CalcBDparam(beats_a,0,0);
% [MBP1(e) SDBP1(e) SDMBP1(e)]

[CI_2P1_d(e,:),CI2_1t(e)] = CI_2PPG(MBP1(e),SDBP1(e),SDMBP1(e));

end

% [CI2_DB_t,CI2_1] = CalcBDBparam(beats(1,:),CI2_1t);
[CI2_DB_t,CI2_1] = CalcBDBparam(beats(1,:),mean(CI_2P1_d(:,1:3)'),CI_I);

CI2_DB1 = [CI_2P1_d];
CI2_DB2 = [CI2_DB_t];

end

if strcmp(chan,'ecg')  
output.ecg.beats = input.ecg.beats_ts/input.ecg.DB.FreqTS;
output.ecg.CI_DBbeat=CI_2E_d;
output.ecg.ibi=diff(output.ecg.beats);
output.ecg.CI_beat = CI2_E;
% output.ecg.lolo_beat = lolo;
else
eval(['output.ppg.'  chan '.CI_beat = CI2_1;']);
eval(['output.ppg.'  chan '.beats' '=input.ppg.' chan '.beats_ts(:,:)/input.ppg.a.DB.FreqTS;']);
eval(['output.ppg.'  chan '.CI_DBbeat1 = CI2_DB1;']);
eval(['output.ppg.'  chan '.CI_DBbeat2 = CI2_DB2;']);

eval(['output.ppg.'  chan '.ibi = diff(output.ppg.' chan '.beats(1,:));']);

end

close all;


