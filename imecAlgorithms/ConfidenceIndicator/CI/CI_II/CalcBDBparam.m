
function [CI_temp1,CI_temp] = CalcBDBparam(BTS_PPG,CI_2,CI_I)
CI_temp1 =0;
for s = 3:1:length(BTS_PPG)-1
    CI_Ba(s)=1;
    if BTS_PPG(s)-BTS_PPG(s-1) <  (BTS_PPG(s-1) - BTS_PPG(s-2))*0.65
        CI_Ba(s)=1;
    elseif BTS_PPG(s)-BTS_PPG(s-1) >  (BTS_PPG(s-1) - BTS_PPG(s-2))*1.25
        CI_Ba(s)=1;
    elseif BTS_PPG(s) - BTS_PPG(s-1)> 2
        CI_Ba(s)=1;
    elseif BTS_PPG(s) - BTS_PPG(s-1)< 0.25
        CI_Ba(s)=1;
    else 
        CI_Ba(s)=4;
    end
    
    if s> 3 && round(BTS_PPG(s))> 0%
        CI_temp(s) = round(mean([CI_Ba(s),CI_2(s)]));
        CI_temp(s) = round(mean([CI_temp(s),CI_I(round(BTS_PPG(s))+1),CI_I(round(BTS_PPG(s))+1)]));

        CI_temp1(s,1) = round(mean(CI_Ba(s)));
        CI_temp1(s,2) = round(mean(CI_2(s)));
    elseif s>3
        CI_temp(s) = round(mean([CI_Ba(s),CI_2(s)]));
    else
        CI_temp(s)=1;
    end
end
        CI_temp(s+1)=1;
