function [SDBP, MBP,SDMBP,SDBPf, MBPf,SDMBPf,SDBPp, MBPp,SDMBPp] = CalcBDparam(BTS_PPG,BTS_PPG_f,BTS_PPG_p)
SDBP=std(BTS_PPG)*128;
MBP=mean(diff(BTS_PPG))*128;
SDMBP=std(diff(BTS_PPG))*128;

SDBPf=std(BTS_PPG_f);
MBPf=mean(diff(BTS_PPG_f));
SDMBPf=std(diff(BTS_PPG_f));
SDBPp=std(BTS_PPG_p);
MBPp=mean(diff(BTS_PPG_p));
SDMBPp=std(diff(BTS_PPG_p));
