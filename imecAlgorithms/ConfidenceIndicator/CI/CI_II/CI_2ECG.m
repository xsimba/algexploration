function [CI_1P_d,CIE2] = CI_2ECG(sdbme,sdbe,mbpe,lolo1)


    % Thresholds
    % Max value ECG
    
    tg1 = 100;%20
    tg2 = 125;%30
    tg3 = 150;%60
    tg4 = 175;%
    tg5 = 200;

    if sdbe <= tg1
        nrsd = 1;
    elseif sdbe> tg1 && sdbe <= tg2
        nrsd = 2;
    elseif sdbe > tg2 && sdbe <= tg3 
        nrsd = 3;
    elseif sdbe > tg3 && sdbe <= tg4
        nrsd = 4;
    elseif sdbe > tg4 && sdbe <= tg5
        nrsd = 5;
     else
        nrsd = 1;
    end 
    
    
    
              %% mbpe
    % Thresholds
%     mbpe =(sdbe./HTm)*100000;
    
    th1 = 50;%4
    th2 = 75;%6
    th3 = 90;%8
    th4 = 100;%
    th5 = 120;

    if mbpe <= th1
        nrmp = 1;
    elseif mbpe> th1 && mbpe <= th2
        nrmp = 2;
    elseif mbpe > th2 && mbpe <= th3 
        nrmp = 3;
    elseif mbpe > th3 && mbpe <= th4
        nrmp = 4;
    elseif mbpe > th4 && mbpe <= th5
        nrmp = 5;
     else
        nrmp = 1;
    end 
    
    
    
              %% sdbme
    % Thresholds
%     mbpe =(sdbe./HTm)*100000;
    
    tb1 = 1;%4
    tb2 = 2;%6
    tb3 = 4;%8
    tb4 = 20;%
    tb5 = 30;

    if sdbme <= tb1
        nrmsd = 1;
    elseif sdbme> tb1 && sdbme <= tb2
        nrmsd = 5;
    elseif sdbme > tb2 && sdbme <= tb3 
        nrmsd = 4;
    elseif sdbme > tb3 && sdbme <= tb4
        nrmsd = 3;
    elseif sdbme > tb4 && sdbme <= tb5
        nrmsd = 2;
     else
        nrmsd = 1;
    end 
    

    CI_1P_d = [nrsd,nrmsd,nrmp,sdbe,sdbme,mbpe];
    
    CIE2 =round(mean([nrmsd,nrmp])).*lolo1;

    
% 
