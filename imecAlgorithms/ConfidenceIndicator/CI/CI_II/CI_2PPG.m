function [CI_2P_d,CI_2] = CI_2PPG(mbp,sdbp,sdmbp)


          %% SDBP
    % Thresholds
       
    tg1 = 30;%
    tg2 = 40;%
    tg3 = 60;%
    tg4 = 100;%
    tg5 = 250;   
    
    if sdbp <= tg1
        nrsdp = 1;
    elseif sdbp> tg1 && sdbp <= tg2
        nrsdp = 2;
    elseif sdbp > tg2 && sdbp <= tg3 
        nrsdp = 3;
    elseif sdbp > tg3 && sdbp <= tg4
        nrsdp = 4;
    elseif sdbp > tg4 && sdbp <= tg5
        nrsdp = 5;
     else
        nrsdp = 1;
    end 
    
    
              %% MBP6
    % Thresholds
%     mbp =(sdbp./HTm)*100000;
    
    th1 = 20;%4
    th2 = 100;%6
    th3 = 200;%8
    th4 = 300;%
    th5 = 400;

    if mbp <= th1
        nrmp = 1;
    elseif mbp> th1 && mbp <= th2
        nrmp = 5;
    elseif mbp > th2 && mbp <= th3 
        nrmp = 4;
    elseif mbp > th3 && mbp <= th4
        nrmp = 3;
    elseif mbp > th4 && mbp <= th5
        nrmp = 2;
     else
        nrmp = 1;
    end 
    
    
       %% SDMBP6
    % Thresholds
    % Max value ECG
    tf1 = 0.2;%
    tf2 = 1.8;%
    tf3 = 4;%
    tf4 = 6;%
    tf5 =10;

    if sdmbp <= tf1
        nrsdm = 1;
    elseif sdmbp > tf1 && sdmbp <= tf2
        nrsdm = 2;
    elseif sdmbp > tf2 && sdmbp <= tf3 
        nrsdm = 3;
    elseif sdmbp > tf3 && sdmbp <= tf4
        nrsdm = 4;
    elseif sdmbp > tf4 && sdmbp <= tf5
        nrsdm = 5;
     else
        nrsdm = 1;
    end
    

    CI_2P_d = [nrsdp,nrsdm,nrmp,sdbp,sdmbp,mbp];
    CI_2=mean([nrsdp,nrsdm,nrmp]);

%     
