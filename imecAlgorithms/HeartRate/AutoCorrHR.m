% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : AutoCorrHR.m
%  Content    : Autocorrelation for heart rate estimation
%  Package    : 
%  Created by : Alex Young (alex.young@imec-nl.nl)
%  Start date : 08-09-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [HRridgeV HRridgeA] = AutoCorrHR(BeatSig, fs)
% BeatSig   Signal containing either PPG, ECG or BioZ like waveforms representing cardiac activity
% fs        Sample rate (Hz)
% HRridgeV  Heart rate ridge data, value (bpm)
% HRridgeA  Heart rate ridge data, amplitude (normalised to maximum)

Len = length(BeatSig);

% Heart rate estimation by autocorrelation
Troi = 3.0;  % Region of interest duration (s)
Tmar = 1.0;  % Margin duration (s)
Tupd = 0.5;  % Update resolution (s)
ThAC = 0.10; % 10% threshold to accept a correlation peak (normalised to 1 for 0 lag)
MxPk = 10;   % Maximum number of correlation peaks.
Wamp = 1;    % Use HR weighted amplitude as a sorting criteria = 1, or not = 0

SegROI = round(fs*Troi/2)*2; % Segment region of interest (even)
SegMar = round(fs*Tmar/2)*2; % Segment margin (even)
SegLen = SegROI+2*SegMar;    % Segment length: |-Mar-|-----ROI-----|-Mar-|
SegSkp = round(fs*Tupd);     % Segment skip
CsegLen = SegROI+SegMar;     % Circular segment length: |-----ROI-----|\Mar\|

xfseg = 0.5 - 0.5*cos(pi*(0:SegMar-1)/SegMar); % Cross fade function for margins (0 to 0.999)

Nseg = floor((Len-SegLen)/SegSkp) + 1; % UsableLen = SegLen + (n-1)*SegOvl

aac      = zeros(Nseg,CsegLen/2); % Autocorrelation display array
sdp      = zeros(1,Nseg);         % Beat signal std
HRridgeV = NaN(Nseg,MxPk);        % Ridge data for HR - instantaneous HR value (bpm)
HRridgeA = NaN(Nseg,MxPk);        % Ridge data for HR - instantaneous HR amplitude (normalised to 0 lag)
iHR      = zeros(1,MxPk);         % Instantaneous HR (for 1 segment)
aHR      = zeros(1,MxPk);         % Instantaneous HR amplitude (for 1 segment)

for n = 0:Nseg-1
    Seg = BeatSig((1:SegLen)+n*SegSkp)'; % Extract cardiac signal segment
    Cseg = [Seg(SegMar+1:SegMar+SegROI) Seg(1:SegMar).*xfseg+Seg(SegMar+SegROI+1:end).*(1-xfseg)]; % Linear to circular conversion
    ft = fft(Cseg);
    
    % Possibility to do frequency domain filtering here (but be careful of transition bands).
    % Placeholder for accelero masking/subtraction
    
    % Circular convolution in frequency domain
    af = ft.*conj(ft);
    ac = real(ifft(af)); ac = ac(1:CsegLen/2)/ac(1); % Normalised result, cull second half (as it is a reflection of the first half)
    
    aac(n+1,:) = max(ac,0); % Positive clamp (negative correlations can be rejected) for display purposes (but not interpolation!)

    % Peak detection
    pkdet = (ac(1:end-2) <= ac(2:end-1)).*(ac(2:end-1) > ac(3:end)).*(ac(2:end-1)>ThAC); % find n where x(n-1) <= x(n) > x(n+1) and when peak > ThAC
    pkdet = [0 pkdet 0]; %#ok<AGROW>
    
    pkdet(1:floor(0.25*fs)) = 0;   % Check valid time window of peaks (>=0.25 sec.)
    pkind = find(pkdet>0);         % Indexes of peaks
    Npk = min(length(pkind),MxPk); % Number of peaks, limited by MxPk
    
    if (Npk>0)
        for np = 1:Npk
            ind = pkind(np);
            % Quadratic curve fitting: y = a*x^2 + b*x + c, solve diff(y,x) = 0 for x, fit for x = -1, 0 and +1
            % x = -1, y(-1) = a - b + c   a = (y(+1) + y(-1) - 2*y(0))/2   y' = 2*a*x + b = 0, then x = -b/2a
            % x =  0, y( 0) =         c   b = (y(+1) - y(-1))/2            then ypk = a*x^2 + b*x + c = -b^2/4a + c = b*x/2 + c
            % x = +1, y(+1) = a + b + c   c = y(0)
            aa = (ac(ind+1) + ac(ind-1))/2 - ac(ind);
            bb = (ac(ind+1) - ac(ind-1))/2;
            cc = ac(ind);
            if (abs(aa)>abs(bb)) % |xx| <= 1/2
                xx = -bb/(2*aa);    % Peak offset position
                yy = bb*xx/2 + cc;  % Peak amplitude
                tt = (ind-1+xx)/fs; % Conversion to time
                iHR(np) = 60/tt;    % Conversion to instantaneous HR
                aHR(np) = yy;
                %disp(['Seg ',num2str(n),'. Peak position: ',num2str(round(1000*tt)/1000),' sec. (',num2str(round(100*tt*fs)/100),' samples), amplitude: ',num2str(yy*100,3),'%']);
            else
                disp(['Seg ',num2str(n),'. Debug: peak curve fitting failure,']);
                disp(['y values: ',num2str(ac(ind-1:ind+1)),'      Coeffs (abc): ',num2str([aa bb cc])]);
            end
        end
        if (Wamp==0), [~,i] = sort(aHR(1:Npk),'descend');             % Sort amplitudes (highest with lowest indexes)
        else          [~,i] = sort(aHR(1:Npk).*iHR(1:Npk),'descend'); % Sort amplitudes (highest with lowest indexes) - weighted by HR
        end
        HRridgeA(n+1,1:Npk) = aHR(i);
        HRridgeV(n+1,1:Npk) = iHR(i); % and their associated HRs
    end

% Rough HR estimate available by using HRridgeV(:,1)
    
    sdp(n+1) = std(Cseg); % Beat signal std
    
    if ((n==-2)||(n==-10)||(n==-65)), % Choose sections to plot (or make negative to disable)
        figure;hold on;plot((0:SegLen-1),Seg,'b');plot((0:CsegLen-1)+SegMar,Cseg,'r');legend('Input segment','Circular segment');title(['Segment ',num2str(n)]);
        spc = abs(ft);                                                spc = spc(1:CsegLen/2+1)/max(spc);
        spw = abs(fft(Seg.*(0.5-0.5*cos(2*pi*(0:SegLen-1)/SegLen)))); spw = spw(1:SegLen/2+1)/max(spw);
        
        figure;hold on;plot((0:CsegLen/2)*fs/CsegLen,spc,'b');plot((0:SegLen/2)*fs/SegLen,spw,'r');
        title(['Spectra,segment ',num2str(n)]);legend('Circular seg','Hanning win');ylabel('Amplitude (normalised)');xlabel('Frequency (Hz)');
        figure;hold on;plot((0:CsegLen/2-1),ac);title(['Autocorrelation, segment ',num2str(n)]);grid on;
        plot((0:CsegLen/2-1),ac./pkdet,'or');
    end
end

figure;plot((0:Nseg-1)*SegSkp/fs,sdp);title('Signal standard deviation per segment');xlabel('Time (s)');

load cmap_kbryw.mat; % Black (low values) background (good for screen)
%load cmap_wyrbk.mat; % White (low values) background (good for printing!)

aaci = interp2(aac',3,'spline'); % This actually means interpolate by a factor of 2^3 in both axis...
figure;imagesc((0:Nseg-1)*SegSkp/fs,(0:CsegLen/2-1)/fs,aaci,[0 1]);colormap(cmap);colorbar;grid on;
title('Autocorrelogram');xlabel('Time (s)');ylabel('Lag (s)');

% If you want to invert the colormap then use this:
% colormap(fliplr(colormap')');

load cmap_wyrbk.mat; % For sparse data
figure;hold on;
for q=MxPk:-1:1 % Plot lower amplitude data first so that it does not obscure the higher amplitude data
scatter((0:Nseg-1)*SegSkp/fs,HRridgeV(:,q),HRridgeA(:,q)*50,HRridgeA(:,q),'filled');colormap(cmap); colorbar;
end
cl = caxis;
caxis([0 cl(2)]); % Set lower colour map limit to zero
set(gca,'color',[0.76 0.87 0.76]); % Background colour light green
grid on;xlabel('Time (s)');ylabel('Instantaneous HR (bpm)');title('Autocorrelation ridge data');

end
