function SpecHarm(q, LenFFTsp, MaxFreq, fs, Desc, Hflg)
%SpecHarm: Spectrogram with optional harmonic analysis
%   Interpolated plot of a spectrogram and harmonic analysis (50% overlap)

% Variable
% q         Vector of input data
% LenFFTsp  Length of FFT
% MaxFreq   Maximum frequency to be displayed (Hz)
% fs        Sample frequency (Hz)
% Desc      Text description (to be added to plot titles)
% Hflg      0:disable, 1:enable harmonic plot

% Example use:
% fs = 360;
% t = (0:99999)/fs;
% y=sin(2*pi*t*1.3.*(1+t/1000))+0.35*cos(2*pi*t*2.6.*(1+t/1000))-0.25*cos(2*pi*t*3.9.*(1+t/1000))-0.15*sin(2*pi*t*5.2.*(1+t/1000));
% SpecHarm(y, 2048, 100, fs,'Test',1);

% Author: Alex Young, IMEC-NL
% Start date: 07 OCT 2013

dBmin = -80; % dB minimum value

q = reshape(q,1,[]); % Convert to 1xN format
LenDataIn = length(q);

MaxFreq = min(MaxFreq,fs/2); % Limit to Nyquist
xlft = 0:LenFFTsp-1; % FFT input
xft = 0:round(MaxFreq*LenFFTsp/fs); % FFT output bins limited by max frequency
win = (1-cos(xlft*2*pi/LenFFTsp)); % Hanning window

%q = q - mean(q); % Remove mean

% 50% FFT overlap
NumFFT = floor(2*LenDataIn/LenFFTsp-1); % Rearranging len = (NumFFT+1)*LenFFTsp/2
ft = zeros(NumFFT,length(xft));
for n = 0:NumFFT-1
    qp = q(1+n*LenFFTsp/2+xlft); % Extract time segment
    ftqp = abs(fft(qp.*win)); % |FFT| and crop output
    ft(n+1,:) = ftqp(xft+1);
end

mxft = max(max(ft)); % Maximum value of matrix
ft = 20*log10(ft/mxft + 1e-6); % dB scaling also required for harmonic detection, protection from log of zero

%% Spectrogram

% figure; imagesc(xft*fs/LenFFTsp,0:NumFFT-1,ft); colormap(hot); % Not interpolated

load cmap_kbryw.mat; % Black (low values) background (good for screen)
%load cmap_wyrbk.mat; % White (low values) background (good for printing!)

fti = interp2(ft',3,'spline'); % This actually means interpolate by a factor of 2^3 in both axis...
figure; imagesc((1:NumFFT)*LenFFTsp/(2*fs),xft*fs/LenFFTsp,fti,[dBmin 0]); colormap(cmap); colorbar; set(gca,'YDir','normal');grid on;
xlabel('Time (s)');ylabel('Frequency (Hz)'); title(['Spectrogram (dB amplitude) ',Desc]);

% If you want to invert the colormap then use this:
% colormap(fliplr(colormap')');

%% Harmonic display: sum harmonics of each frequency down from Nyquist
% Sum(log(x)) = log(prod(x))
if (Hflg == 1)
    Nhar = 3; % Number of harmonics
    fh = zeros(NumFFT,length(xft));
    
    for i = xft(Nhar+1:end-1)
        ac = zeros(NumFFT,1);
        for j = 1:Nhar
            bin = round(i*j/Nhar)+1;
            %ac = ac + ft(:,bin)+ ft(:,bin-1)+ ft(:,bin+1);           % Sum of local bins
            ac = ac + max([ft(:,bin)'; ft(:,bin-1)'; ft(:,bin+1)'])'; % Max of local bins
        end
        fh(:,i) = ac/Nhar;
    end
    mnfh = min(min(fh)); % Minimum value of fh
    fh(:,1:Nhar-1) = mnfh; % Fill non-populated elements with minimum value
    fh(:,end-1:end) = mnfh;
    
    fhi = interp2(fh',3,'spline');
    figure; imagesc((1:NumFFT)*LenFFTsp/(2*fs),xft*fs/(LenFFTsp*Nhar),fhi); colormap(cmap); colorbar; set(gca,'YDir','normal');grid on;
    xlabel('Time (s)'); ylabel('Frequency (Hz)'); title(['Harmonic detection (dB amplitude) ',Desc]);
end

end

