% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : FreqAnalyHR.m
%  Content    : Frequency analysis for heart rate estimation
%  Package    : 
%  Created by : Alex Young (alex.young@imec-nl.nl)
%  Start date : 08-09-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function [HRridgeRawV HRridgeV HRridgeA] = FreqAnalyHR(BeatSig, fs, AbsNotRel, Th)
% BeatSig   Signal containing either PPG, ECG or BioZ like waveforms representing cardiac activity
% fs           Sample rate (Hz)
% AbsNotRel    Flag to select between Realtive=0 and Absolute=1 thresholding
% Th           Absolute threshold or std contribution to relative threshold
% HRridgeRawV  Raw (not interpolated) value (bpm) heart rate ridge data
% HRridgeV  Heart rate ridge data, value (bpm)
% HRridgeA  Heart rate ridge data, amplitude (relative to zero lag)

Len = length(BeatSig);

% Heart rate estimation by frequency analysis
Troi = 7.0;  % Region of interest duration (s)
Tmar = 1.0;  % Margin duration (s)
Tupd = 0.5;  % Update resolution (s)
ThFA = 0.10; % 10% threshold to accept a frequency peak (normalised to 1 for highest peak)
MxPk = 20;   % Maximum number of correlation peaks.
HRmin =  27.0; % Minimum bpm
HRmax = 240.0; % Maximum bpm
Harm  = 3;     % Maximum harmonic
Wamp = 0;    % Use HR weighted amplitude as a sorting criteria = 1, or not = 0

FrMin = HRmin/60.0;
FrMax = HRmax*Harm/60.0;

SegROI = round(fs*Troi/2)*2; % Segment region of interest (even)
SegMar = round(fs*Tmar/2)*2; % Segment margin (even)
SegLen = SegROI+2*SegMar;    % Segment length: |-Mar-|-----ROI-----|-Mar-|
SegSkp = round(fs*Tupd);     % Segment skip
CsegLen = SegROI+SegMar;     % Circular segment length: |-----ROI-----|\Mar\|

xfseg = 0.5 - 0.5*cos(pi*(0:SegMar-1)/SegMar); % Cross fade function for margins (0 to 0.999)

Nseg = floor((Len-SegLen)/SegSkp) + 1; % UsableLen = SegLen + (n-1)*SegOvl

ffa         = zeros(Nseg,CsegLen/2); % Frequency analysis display array
sdp         = zeros(1,Nseg);         % Beat signal std
HRridgeRawV = NaN(Nseg,MxPk);        % Raw ridge data for HR - instantaneous raw HR value (bpm)
HRridgeV    = NaN(Nseg,MxPk);        % Ridge data for HR - instantaneous HR value (bpm)
HRridgeA    = NaN(Nseg,MxPk);        % Ridge data for HR - instantaneous HR amplitude (normalised to 0 lag)
iHR         = zeros(1,MxPk);         % Instantaneous HR, interpolated (for 1 segment)
rHR         = zeros(1,MxPk);         % Instantaneous HR, raw (for 1 segment)
aHR         = zeros(1,MxPk);         % Instantaneous HR amplitude (for 1 segment)

for n = 0:Nseg-1
    Seg = BeatSig((1:SegLen)+n*SegSkp)'; % Extract cardiac signal segment
    Cseg = [Seg(SegMar+1:SegMar+SegROI) Seg(1:SegMar).*xfseg+Seg(SegMar+SegROI+1:end).*(1-xfseg)]; % Linear to circular conversion
    ft = fft(Cseg);
    
    % Possibility to do frequency domain filtering here (but be careful of transition bands).
    % Placeholder for accelero masking/subtraction
    
    fa = abs(ft(1:CsegLen/2)); % Magnitude and cull to half length
    
    if (AbsNotRel==0), ThFA = mean(fa) + Th*std(fa); % Relative
    else               ThFA = Th; % Absolute
    end
    
    ffa(n+1,:) = fa/max(fa); % Store for display (normalised)

    % Peak detection
    pkdet = (fa(1:end-2) <= fa(2:end-1)).*(fa(2:end-1) > fa(3:end)).*(fa(2:end-1)>ThFA); % find n where x(n-1) <= x(n) > x(n+1) and when peak > ThFA
    pkdet = [0 pkdet 0]; %#ok<AGROW>
    
    pkdet(1:floor(FrMin*CsegLen/fs)) = 0; % Check valid frequency window of peaks (>HRmin and <HRmax*Harm)
    pkdet(ceil(FrMax*CsegLen/fs)+1:end) = 0;
    pkind = find(pkdet>0);         % Indexes of peaks
    Npk = min(length(pkind),MxPk); % Number of peaks, limited by MxPk
    
    if (Npk>0)
        for np = 1:Npk
            ind = pkind(np);
            % Quadratic curve fitting: y = a*x^2 + b*x + c, solve diff(y,x) = 0 for x, fit for x = -1, 0 and +1
            % x = -1, y(-1) = a - b + c   a = (y(+1) + y(-1) - 2*y(0))/2   y' = 2*a*x + b = 0, then x = -b/2a
            % x =  0, y( 0) =         c   b = (y(+1) - y(-1))/2            then ypk = a*x^2 + b*x + c = -b^2/4a + c = b*x/2 + c
            % x = +1, y(+1) = a + b + c   c = y(0)
            aa = (fa(ind+1) + fa(ind-1))/2 - fa(ind);
            bb = (fa(ind+1) - fa(ind-1))/2;
            cc = fa(ind);
            if (abs(aa)>abs(bb)) % |xx| <= 1/2
                xx = -bb/(2*aa);    % Peak offset position
                yy = bb*xx/2 + cc;  % Peak amplitude
                ff = (ind-1+xx)*fs/CsegLen; % Conversion to frequency
                iHR(np) = 60*ff;    % Conversion to instantaneous HR
                aHR(np) = yy;
                %disp(['Seg ',num2str(n),'. Peak position: ',num2str(round(1000*ff)/1000),' Hz, amplitude: ',num2str(yy*100,3),'%']);
            else
                disp(['Seg ',num2str(n),'. Debug: peak curve fitting failure,']);
                disp(['y values: ',num2str(fa(ind-1:ind+1)),'      Coeffs (abc): ',num2str([aa bb cc])]);
                aHR(np) = cc;
            end
            ff = (ind-1)*fs/CsegLen; % Conversion to frequency for raw, just peak bin frequency
            rHR(np) = 60*ff; % Conversion to instantaneous HR
        end
        if (Wamp==0), [~,i] = sort(aHR(1:Npk),'descend');             % Sort amplitudes (highest with lowest indexes)
        else          [~,i] = sort(aHR(1:Npk).*iHR(1:Npk),'descend'); % Sort amplitudes (highest with lowest indexes) - weighted by HR
        end
        HRridgeA(n+1,1:Npk) = aHR(i);
        HRridgeV(n+1,1:Npk) = iHR(i); % and their associated HRs
        HRridgeRawV(n+1,1:Npk) = rHR(i);
    end

    % Rough HR estimate available by using HRridgeV(:,1)
    
    sdp(n+1) = std(Cseg); % Beat signal std
    
    if ((n==-2)||(n==-10)||(n==-65)), % Choose sections to plot (or make negative to disable)
        figure;hold on;plot((0:SegLen-1),Seg,'b');plot((0:CsegLen-1)+SegMar,Cseg,'r');legend('Input segment','Circular segment');title(['Segment ',num2str(n)]);
        spc = abs(ft);                                                spc = spc(1:CsegLen/2+1)/max(spc);
        spw = abs(fft(Seg.*(0.5-0.5*cos(2*pi*(0:SegLen-1)/SegLen)))); spw = spw(1:SegLen/2+1)/max(spw);
        
        figure;hold on;plot((0:CsegLen/2)*fs/CsegLen,spc,'b');plot((0:SegLen/2)*fs/SegLen,spw,'r');
        title(['Spectra,segment ',num2str(n)]);legend('Circular seg','Hanning win');ylabel('Amplitude (normalised)');xlabel('Frequency (Hz)');
    end
end

figure;plot((0:Nseg-1)*SegSkp/fs,sdp);title('Signal standard deviation per segment');xlabel('Time (s)');

load cmap_kbryw.mat; % Black (low values) background (good for screen)
%load cmap_wyrbk.mat; % White (low values) background (good for printing!)

ffai = interp2(ffa',3,'spline'); % This actually means interpolate by a factor of 2^3 in both axis...
figure;imagesc((0:Nseg-1)*SegSkp/fs,(0:CsegLen/2-1)*fs/CsegLen,ffai,[0 1]);colormap(cmap);colorbar;set(gca,'YDir','normal');grid on; ylim([0 25]);
title('Spectrogram');xlabel('Time (s)');ylabel('Frequency (Hz)');

% If you want to invert the colormap then use this:
% colormap(fliplr(colormap')');

HRamp = NaN(Nseg,MxPk); 
for n = 1:Nseg
    HRamp(n,:) = HRridgeA(n,:)/max(HRridgeA(n,:)); % Must be normalised for this plot
end
load cmap_wyrbk.mat; % For sparse data
figure;hold on;
for q=MxPk:-1:1 % Plot lower amplitude data first so that it does not obscure the higher amplitude data
    scatter((0:Nseg-1)*SegSkp/fs,HRridgeV(:,q),HRamp(:,q)*50,HRamp(:,q),'filled');colormap(cmap); colorbar;
end
cl = caxis;
caxis([0 cl(2)]); % Set lower colour map limit to zero
set(gca,'color',[0.76 0.87 0.76]); % Background colour light green
grid on;xlabel('Time (s)');ylabel('Instantaneous HR (bpm)');title('Frequency analysis ridge data');

end
