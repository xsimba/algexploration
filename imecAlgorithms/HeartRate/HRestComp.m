% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : HRestComp.m
%  Content    : HR estimation comparison of methods
%  Package    : 
%  Created by : Alex Young (alex.young@imec-nl.nl)
%  Start date : 08-09-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

close all;
clear all;
clc;

addpath ../BeatDetection
addpath ../BeatDetection/Wavelets/Coeffs

%% Test signal acquisition


if (1) % Cycling
    load data_src+ref.mat; % ECG necklace
    ecgR = resample(ecg,1,2); % Reasample ECG from 256 to 128 Hz
end

if (0) % Running only (poor BD for ECG)
    load data_src.mat; output = data; ecgR = data.ecg.signal'; % Accelero cal.
    output.acc.signal.x = output.acc.x.signal;
    output.acc.signal.y = output.acc.y.signal;
    output.acc.signal.z = output.acc.z.signal;
end

if (0) % Walking-running (poor PPG for BD)
    load data_src_refecg.mat
    ecgR = resample(ecg,1,2); % Reasample ECG from 256 to 128 Hz
    output = data;
    output.acc.signal.x = output.acc.x.signal;
    output.acc.signal.y = output.acc.y.signal;
    output.acc.signal.z = output.acc.z.signal;
end

fs   = output.ppg.a.Fs; % Extract PPG channels (flip if necessary)
ppgA = -output.ppg.a.signal'; 
ppgB = -output.ppg.b.signal';
ppgC = -output.ppg.c.signal';
ppgD = -output.ppg.d.signal';
ppgE = -output.ppg.e.signal';
ppgF = -output.ppg.f.signal';
ppgG = -output.ppg.g.signal';
ppgH = -output.ppg.h.signal';
LenPPG = length(ppgH);

LenECG = length(ecgR);

LenAcc = length(output.acc.signal.x); % Extract ACC channels
acc = zeros(LenAcc,3);

% Default scale and offset settings for Bosch BMA280 (also dependent on configuration settings)
AccScaleOffset.xsc = 1/4096.0; AccScaleOffset.xof = 0.0;
AccScaleOffset.ysc = 1/4096.0; AccScaleOffset.yof = 0.0;
AccScaleOffset.zsc = 1/4096.0; AccScaleOffset.zof = 0.0;

cax = output.acc.signal.x*AccScaleOffset.xsc + AccScaleOffset.xof; % No calibration: apply current scales and offsets
cay = output.acc.signal.y*AccScaleOffset.ysc + AccScaleOffset.yof;
caz = output.acc.signal.z*AccScaleOffset.zsc + AccScaleOffset.zof;
AccUD = 0;

acc(:,1) = cax;
acc(:,2) = cay;
acc(:,3) = caz;
acm = sqrt(cax.^2+cay.^2+caz.^2); % Magnitude

ta = (0:LenAcc-1)/fs;
figure;hold on;plot(ta,cax,'b');plot(ta,cay,'r');plot(ta,caz,'m');plot(ta,acm,'k'); legend('AccX','AccY','AccZ','AccMag');
%figure;hold on;plot(ta,cax,'b');plot(ta,cay,'r');plot(ta,caz,'m');plot(ta,acm,'k');plot(ta,Stationary,'g','linewidth',2); legend('AccX','AccY','AccZ','AccMag','Still');
xlabel('Time (s)');ylabel('Acceleration (g)');
if (AccUD == 1), title('Calibrated accelerometer output');   disp('Calibrated accelerometer scale and offset values:');
else             title('Uncalibrated accelerometer output'); disp('Default accelerometer scale and offset values:');
end
disp(AccScaleOffset);


% Crop data to minimum size
MinLen = min([LenECG LenAcc LenPPG]);
ecgR = ecgR(1:MinLen);
ppgA = ppgA(1:MinLen);
ppgB = ppgB(1:MinLen);
ppgC = ppgC(1:MinLen);
ppgD = ppgD(1:MinLen);
ppgE = ppgE(1:MinLen);
ppgF = ppgF(1:MinLen);
ppgG = ppgG(1:MinLen);
ppgH = ppgH(1:MinLen);
acm  =  acm(1:MinLen);
acc  =  acc(1:MinLen,:);

% What does the input data look like?
if (0) % Plot or not?
    figure;hold on; % Remove mean and normalise
    q = ecgR-mean(ecgR); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[0,0,0]/7); % ECG
    q = ppgA-mean(ppgA); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[7,0,0]/7); % ppgA
    q = ppgB-mean(ppgB); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[6,0,1]/7); % ppgB
    q = ppgC-mean(ppgC); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[5,0,2]/7); % ppgC
    q = ppgD-mean(ppgD); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[4,0,3]/7); % ppgD
    q = ppgE-mean(ppgE); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[3,0,4]/7); % ppgE
    q = ppgF-mean(ppgF); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[2,0,5]/7); % ppgF
    q = ppgG-mean(ppgG); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[1,0,6]/7); % ppgG
    q = ppgH-mean(ppgH); q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[0,0,7]/7); % ppgH
    q = acm-mean(acm);   q = q/max(abs(q)); plot((0:MinLen-1)/fs,q,'Color',[0,5,0]/7); % ACC Mag
    xlabel('Time (s)');ylabel('Normalised amplitude');title('Input signals');
    legend('ECG','ppgA','ppgB','ppgC','ppgD','ppgE','ppgF','ppgG','ppgH','ACCm');
end

% What are the spectra?
if (0) % Plot or not?
    % Filter design:                      Fstop  Fpass  Astop  Apass  Fs
    h = fdesign.highpass('fst,fp,ast,ap', 0.250, 0.500, 50.00, 0.100, fs);
    % Hd = design(h, 'ellip', 'MatchExactly', 'both', 'SOSScaleNorm', 'Linf');
    Hd = design(h, 'ellip', 'MatchExactly', 'both');
    
    Fmax = 12;
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ecgR),1024,Fmax,fs,'ECG (necklace)', 0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgA),1024,Fmax,fs,'ppgA',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgB),1024,Fmax,fs,'ppgB',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgC),1024,Fmax,fs,'ppgC',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgD),1024,Fmax,fs,'ppgD',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgE),1024,Fmax,fs,'ppgE',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgF),1024,Fmax,fs,'ppgF',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgG),1024,Fmax,fs,'ppgG',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,ppgH),1024,Fmax,fs,'ppgH',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,cax) ,1024,Fmax,fs,'Acc X',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,cay) ,1024,Fmax,fs,'Acc Y',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,caz) ,1024,Fmax,fs,'Acc Z',0);
    SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,acm) ,1024,Fmax,fs,'Acc Mag',0);
end

% Load wavelet/filter coefficients
CWT_ECG = importdata('../BeatDetection/Wavelets/Coeffs/ECGwavelet2_128HzSupp.csv');      % ECG
CWT_PPG = importdata('../BeatDetection/Wavelets/Coeffs/PPGwavelet10_128HzDiffLPF.csv');  % PPG CWT coefficients 12 Hz passband, 2nd order numerical differentiation

%% Method 1: Beat detection
[BeatsTimestampsECG,~] = BD(ecgR, 'ECG');
HRestTecgBD1 = (BeatsTimestampsECG(2:end)+BeatsTimestampsECG(1:end-1))/(2*32768); % Time is mid point between 2 R-peaks
HRestVecgBD1 = (60*32768)./(BeatsTimestampsECG(2:end)-BeatsTimestampsECG(1:end-1)); % Inst. HR is recip. of time difference between R-peaks x 60

HRestTecgBD2 = (BeatsTimestampsECG(3:end)+BeatsTimestampsECG(1:end-2))/(2*32768); % Time is mid point between 3 R-peaks
HRestVecgBD2 = (2*60*32768)./(BeatsTimestampsECG(3:end)-BeatsTimestampsECG(1:end-2)); % Inst. HR is recip. of time difference between 2 R-peaks x 60 x 2

HRestTecgBD3 = (BeatsTimestampsECG(4:end)+BeatsTimestampsECG(1:end-3))/(2*32768); % Time is mid point between 4 R-peaks
HRestVecgBD3 = (3*60*32768)./(BeatsTimestampsECG(4:end)-BeatsTimestampsECG(1:end-3)); % Inst. HR is recip. of time difference between 3 R-peaks x 60 x 3

figure; hold on; plot(HRestTecgBD3,HRestVecgBD3,'.g'); plot(HRestTecgBD2,HRestVecgBD2,'.r'); plot(HRestTecgBD1,HRestVecgBD1,'.b');
title('Instantaneous HR from BD for ECG');xlabel('Time (s)');ylabel('Heart rate (bpm)');legend('3 beat interval','2 beat interval','1 beat interval');
yl = ylim; ylim([max(yl(1),25) min(yl(2),240)]); grid on;


[BeatsTimestampsPPG,~] = BD(ppgA, 'PPG');
HRestTppgBD1 = (BeatsTimestampsPPG(1,2:end)+BeatsTimestampsPPG(1,1:end-1))/(2*32768); % Time is mid point between 2 R-peaks
HRestVppgBD1 = (60*32768)./(BeatsTimestampsPPG(1,2:end)-BeatsTimestampsPPG(1,1:end-1)); % Inst. HR is recip. of time difference between R-peaks x 60

HRestTppgBD2 = (BeatsTimestampsPPG(1,3:end)+BeatsTimestampsPPG(1,1:end-2))/(2*32768); % Time is mid point between 3 R-peaks
HRestVppgBD2 = (2*60*32768)./(BeatsTimestampsPPG(1,3:end)-BeatsTimestampsPPG(1,1:end-2)); % Inst. HR is recip. of time difference between 2 R-peaks x 60 x 2

HRestTppgBD3 = (BeatsTimestampsPPG(1,4:end)+BeatsTimestampsPPG(1,1:end-3))/(2*32768); % Time is mid point between 4 R-peaks
HRestVppgBD3 = (3*60*32768)./(BeatsTimestampsPPG(1,4:end)-BeatsTimestampsPPG(1,1:end-3)); % Inst. HR is recip. of time difference between 3 R-peaks x 60 x 3

figure; hold on; plot(HRestTppgBD3,HRestVppgBD3,'.g'); plot(HRestTppgBD2,HRestVppgBD2,'.r'); plot(HRestTppgBD1,HRestVppgBD1,'.b');
title('Instantaneous HR from BD for PPG');xlabel('Time (s)');ylabel('Heart rate (bpm)');legend('3 beat interval','2 beat interval','1 beat interval');
yl = ylim; ylim([max(yl(1),25) min(yl(2),240)]); grid on;

%% Method 2: Frequency peak analysis
% Apply (wavelet) filter to restrict signal to frequency band of interest
% Use linear to circular conversion
% Perform FFT, 8 sec. length, set overlap for an update every 0.5 sec
% Peak enhancement: quadratic

% [HRestVecgFA HRestAecgFA] = FreqAnalyHR(filter(fliplr(CWT_ECG),1,ecgR), fs);
[HRestVecgFAraw HRestVecgFA HRestAecgFA] = FreqAnalyHR(filter(fliplr(CWT_ECG),1,ecgR), fs, 0, 0.5);
HRestTecgFA = (0:length(HRestVecgFA)-1)*0.5+4.0; % Time

% [HRestVppgFA HRestAppgFA] = FreqAnalyHR(filter(fliplr(CWT_PPG),1,ppgA), fs);
[HRestVppgFAraw HRestVppgFA HRestAppgFA] = FreqAnalyHR(filter(fliplr(CWT_PPG),1,ppgA), fs, 0, 0.5);
HRestTppgFA = (0:length(HRestVppgFA)-1)*0.5+4.0; % Time

%% Method 3: Autocorrelation
% Apply (wavelet) filter to restrict signal to frequency band of interest
% Use linear to circular conversion
% Perform FFT, 4 sec. length, set overlap for an update every 0.5 sec
% Perform conjugate and iFFT with compensation if necessary for dv/dt
% Peak enhancement: quadratic
[HRestVecgAC HRestAecgAC] = AutoCorrHR(filter(fliplr(CWT_ECG),1,ecgR), fs);
HRestTecgAC = (0:length(HRestVecgAC)-1)*0.5+2.0; % Time

[HRestVppgAC HRestAppgAC] = AutoCorrHR(filter(fliplr(CWT_PPG),1,ppgA), fs);
HRestTppgAC = (0:length(HRestVppgAC)-1)*0.5+2.0; % Time
