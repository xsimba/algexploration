%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <lombspect.m>
% Content   : Lomb normalized periodogram calculation
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
function [f,P] = lombspect(t,h)
% LOMB(T,H) computes the Lomb normalized periodogram (spectral
% power as a function of frequency) of a sequence of N data points H,
% sampled at times T, which are not necessarily evenly spaced. T and H must
% be vectors of equal size. The routine will calculate the spectral power
% for an increasing sequence of frequencies.
% 
% The returned values are arrays of frequencies considered (f), the
% associated spectral power (P). 
% Written by Dmitry Savransky 21 May 2008
% Modified by E.C. Wentink

%% calculate frequency vector
% The 4 in this formula is the amount of "oversampling" to get the right lenght of the data
%sample length and time span
T = max(t) - min(t);
f = (0:1/(T*4):(length(h)-1)/(2*T))'; % The frequencies vector with available frequencies in the data

%% Get the power for the frequencies
%mean and variance 
mu = mean(h); % the mean of the data
sig = var(h); % the variance of the data

%angular frequencies and constant offsets
w = 2*pi*f;
tau = atan2(sum(sin(2*w*t.'),2),sum(cos(2*w*t.'),2))./(2*w);

%spectral power
cterm = cos(w*t.' - repmat(w.*tau,1,length(t))); % the cosine term of the formula
sterm = sin(w*t.' - repmat(w.*tau,1,length(t))); % the sine term of the formula
P = (sum(cterm*diag(h-mu),2).^2./sum(cterm.^2,2) + ...
     sum(sterm*diag(h-mu),2).^2./sum(sterm.^2,2))/(2*sig); % The power per frequency

