%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <HRV_calc.m>
% Content   : Calculation of the main HRV parameters
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%% for Simba data (output from beat detector)
FreqTS = 32768; % Timestamp clock frequency
RR = diff(AllBeatTimeStamps)./FreqTS; % calculate the RR-interval per beat from ECG!!!
RP_time = (AllBeatTimeStamps(1:end-1)./FreqTS); % start time of each RR interval 
[f,P] = lombspect(RP_time',RR'); 
% figure;plot(f,P)

%% for data in seconds (using the manual annotations)
% RR = diff(qrs_location_manual); %% calculate the RR-interval per beat
% [f,P] = lombspect((qrs_location_manual(1:end-1))',RR');
% figure;plot(f,P)

%% calculate parameters
% Time domain parameter calculation
SDNN = std(RR); %standard deviation of RR-interval
pNN50_t = abs(diff(RR'));
pNN50 =  size(find(pNN50_t>50),1)/length(RR); % fraction of RRintervals > 50 ms

% Interval calculation 
v1 = find(abs((f-0.005))== min(abs((f-0.005)))); % find the freq closest to 0.005
v2 = find(abs((f-0.04))== min(abs((f-0.04)))); % find the freq closest to 0.04
v3 = find(abs((f-0.15))== min(abs((f-0.15)))); % find the freq closest to 0.15
v4 = find(abs((f-0.4))== min(abs((f-0.4)))); % find the freq closest to 0.4

% Frequency parameter calculation
VLFHRV2 =  f(find(P(v1-1:v2+1)==max(P(v1-1:v2+1)))+v1-2);  % very low frequency (between 0.005 and 0.04 Hz)
LFHRV2 = f(find(P(v2-1:v3+1)==max(P(v2-1:v3+1)))+v2-2); % low frequency component (between 0.04 and 0.15 Hz)
HFHRV2 = f(find(P(v3-1:v4+1)==max(P(v3-1:v4+1)))+v3-2); % high frequency component (between 0.15 and 0.4 Hz)






