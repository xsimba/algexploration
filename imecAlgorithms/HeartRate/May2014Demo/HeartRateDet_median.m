%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <HeartRateDet_median.m>
% Content   : Convert detected beats (in timestamps) into RR-interval and HR in BPM
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%% Inputs and outputs
% Input
% AllBeatTimeStamps: these are the TIMESTAMPS of every beat (independant if it is PPG/ECG/...)
% What comes out of Beat detection script
% Output:    
% RR:  Than the RR is RR-interval, the difference in timestamps between the beats devided by the timestamp to convert to seconds
% HR: is the heart rate in bpm calculated by dividing 60 by the RR-interval
%% Heart rate algorithm
FreqTS = 32768; % Timestamp clock frequency
RR = diff(AllBeatTimeStamps)./FreqTS; %% calculate the RR-interval per beat
Nrbeats2calc = 10;
udrb=2; % update rate per beat
pp=1;
for p = Nrbeats2calc+1:udrb:length(RR)
HR(pp) = 60./(median(RR(p-Nrbeats2calc:p)));
pp = pp+1;
end
figure;plot(HR)
