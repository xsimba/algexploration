% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : FourierPeakFit.m
%  Content    : Estimation of frequency and amplitude of a sinusiod given frequency peak data.
%  Package    : 
%  Created by : Alex Young (alex.young@imec-nl.nl)
%  Start date : 08-09-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

close all
clear all
clc

fs = 128; % Hz

% Heart rate estimation by frequency analysis
Troi = 7.0;  % Region of interest duration (s)
Tmar = 1.0;  % Margin duration (s)
ThFA = 0.9;  % Should be just a single peak

SegROI = round(fs*Troi/2)*2; % Segment region of interest (even)
SegMar = round(fs*Tmar/2)*2; % Segment margin (even)
SegLen = SegROI+2*SegMar;    % Segment length: |-Mar-|-----ROI-----|-Mar-|
CsegLen = SegROI+SegMar;     % Circular segment length: |-----ROI-----|\Mar\|

xfseg = 0.5 - 0.5*cos(pi*(0:SegMar-1)/SegMar); % Cross fade function for margins (0 to 0.999)

FreqStart = 0.50; % Start frequency (Hz), 30 bpm
FreqEnd   = 12.0; % End frequency (Hz), 3rd harmonic of 240 bpm
FreqInc   = 0.01; % Frequency increment
PhaseInc  = 9.00; % Phase increment  

% ffa      = zeros(Nseg,CsegLen/2); % Frequency analysis display array
% sdp      = zeros(1,Nseg);         % Beat signal std
% HRridgeV = NaN(Nseg,MxPk);        % Ridge data for HR - instantaneous HR value (bpm)
% HRridgeA = NaN(Nseg,MxPk);        % Ridge data for HR - instantaneous HR amplitude (normalised to 0 lag)
% iHR      = zeros(1,MxPk);         % Instantaneous HR (for 1 segment)
% aHR      = zeros(1,MxPk);         % Instantaneous HR amplitude (for 1 segment)

Nfr = floor((FreqEnd-FreqStart)/FreqInc)+1;
res = zeros(1,Nfr);
err = zeros(360,Nfr);

for n = 0:Nfr-1
    ff = FreqStart + n*FreqInc; % Frequency sweep
    b = ff*CsegLen/fs; % Fractional bin
    res(n+1) = b - round(b); % Residual
    for pp = 0:1:359 % Phase sweep
        Seg = sin(pp*pi/180+2*pi*ff*(0:SegLen-1)/fs); % Synthesize sine wave
        Cseg = [Seg(SegMar+1:SegMar+SegROI) Seg(1:SegMar).*xfseg+Seg(SegMar+SegROI+1:end).*(1-xfseg)]; % Linear to circular conversion
        ft = fft(Cseg);
        
        fa = abs(ft(1:CsegLen/2)); % Magnitude and cull to half length
        fa = fa/max(fa); % Normalise
        
        % Peak detection
        pkdet = (fa(1:end-2) <= fa(2:end-1)).*(fa(2:end-1) > fa(3:end)).*(fa(2:end-1)>ThFA); % find n where x(n-1) <= x(n) > x(n+1) and when peak > ThFA
        pkdet = [0 pkdet 0]; %#ok<AGROW>
        
        pkdet(1:floor(0.5*CsegLen/fs)) = 0;   % Check valid frequency window of peaks (>=0.5 Hz)
        pkind = find(pkdet>0);         % Indexes of peaks
        Npk = length(pkind); % Number of peaks
        
        if (Npk~=1)
            Npk
        end
        
        if (Npk>0)
            for np = 1:Npk
                ind = pkind(np);
                % Quadratic curve fitting: y = a*x^2 + b*x + c, solve diff(y,x) = 0 for x, fit for x = -1, 0 and +1
                % x = -1, y(-1) = a - b + c   a = (y(+1) + y(-1) - 2*y(0))/2   y' = 2*a*x + b = 0, then x = -b/2a
                % x =  0, y( 0) =         c   b = (y(+1) - y(-1))/2            then ypk = a*x^2 + b*x + c = -b^2/4a + c = b*x/2 + c
                % x = +1, y(+1) = a + b + c   c = y(0)
                aa = (fa(ind+1) + fa(ind-1))/2 - fa(ind);
                bb = (fa(ind+1) - fa(ind-1))/2;
                cc = fa(ind);
                if (abs(aa)>abs(bb)) % |xx| <= 1/2
                    xx = -bb/(2*aa);    % Peak offset position
                    yy = bb*xx/2 + cc;  % Peak amplitude
                    fe = (ind-1+xx)*fs/CsegLen; % Conversion to frequency
                    erF(pp,n+1) = ff - fe; % Frequency error
                    erA(pp,n+1) = 1 - yy; % Ampiltude error
                    %disp(['Seg ',num2str(n),'. Peak position: ',num2str(round(1000*ff)/1000),' Hz, amplitude: ',num2str(yy*100,3),'%']);
                else
                    disp(['Seg ',num2str(n),'. Debug: peak curve fitting failure,']);
                    disp(['y values: ',num2str(fa(ind-1:ind+1)),'      Coeffs (abc): ',num2str([aa bb cc])]);
                end
            end
        end
        
        if ((n==-2)||(n==-10)||(n==-65)), % Choose sections to plot (or make negative to disable)
            figure;hold on;plot((0:SegLen-1),Seg,'b');plot((0:CsegLen-1)+SegMar,Cseg,'r');legend('Input segment','Circular segment');title(['Segment ',num2str(n)]);
            spc = abs(ft);                                                spc = spc(1:CsegLen/2+1)/max(spc);
            spw = abs(fft(Seg.*(0.5-0.5*cos(2*pi*(0:SegLen-1)/SegLen)))); spw = spw(1:SegLen/2+1)/max(spw);
            
            figure;hold on;plot((0:CsegLen/2)*fs/CsegLen,spc,'b');plot((0:SegLen/2)*fs/SegLen,spw,'r');
            title(['Spectra,segment ',num2str(n)]);legend('Circular seg','Hanning win');ylabel('Amplitude (normalised)');xlabel('Frequency (Hz)');
        end
    end
end

figure;plot((0:Nseg-1)*SegSkp/fs,sdp);title('Signal standard deviation per segment');xlabel('Time (s)');

load cmap_kbryw.mat; % Black (low values) background (good for screen)
%load cmap_wyrbk.mat; % White (low values) background (good for printing!)

ffai = interp2(ffa',3,'spline'); % This actually means interpolate by a factor of 2^3 in both axis...
figure;imagesc((0:Nseg-1)*SegSkp/fs,(0:CsegLen/2-1)*fs/CsegLen,ffai,[0 1]);colormap(cmap);colorbar;set(gca,'YDir','normal');grid on;
title('Spectrogram');xlabel('Time (s)');ylabel('Frequency (Hz)');

% If you want to invert the colormap then use this:
% colormap(fliplr(colormap')');

load cmap_wyrbk.mat; % For sparse data
figure;hold on;
for q=MxPk:-1:1 % Plot lower amplitude data first so that it does not obscure the higher amplitude data
scatter((0:Nseg-1)*SegSkp/fs,HRridgeV(:,q),HRridgeA(:,q)*50,HRridgeA(:,q),'filled');colormap(cmap); colorbar;
end
cl = caxis;
caxis([0 cl(2)]); % Set lower colour map limit to zero
set(gca,'color',[0.76 0.87 0.76]); % Background colour light green
grid on;xlabel('Time (s)');ylabel('Instantaneous HR (bpm)');title('Frequency analysis ridge data');

