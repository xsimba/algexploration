function [DateStr] = UnixTimeToDateStr( UnixTime )
% Convert unix timestamp to date (yyyymmdd)
    % Roberto Garcia van der Westen
    % Input:   unixtime in ms 
    % Input format:  UnixTime = 1.426093491752683e+09
    % Output: string containing timestamp data 
    % Example: UnixTime = 1.426093491752683e+09
    %           DateStr = '20150311'; -> 11th of march 2015
 
    formatOut = 'yyyymmdd';
    unix_time = UnixTime;
    DateStr = datestr(unix_time/86400 + datenum(1970,1,1),formatOut);
