function [normalized_array] = normalize_array(array)
%This function normalizes an arbitrary array to the range from 0 to 1
     m = min(array);
     range = max(array) - m;
     array = (array - m) / range;
     normalized_array = array;
end

