% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : validation.m
%  Content    : All algorithms in a row
%  Version    : 0.1
%  Package    : SIMBA
%  Created by : F.Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 21-01-2015
%  Modification and Version History: 
%  | Developer | Version |    Date    |     Changes      |
%  | Fabian    | 1.0     |   14-10-14 |     Changed to only operate on local working copy of simba-common   |
%  |  fbeutel  | 2.0     | 16-12-2014 | Same as 1.0 but now for v2 of BP, incl new variable names etc |
%  |  fbeutel  | 6.0     | 21-01-2014 | Same as 2.0 but now with subjects from data collection imec BE |
%  |  fbeutel  | 7.0     | 20-03-2014 | Implementation of datapool selection for selective (training and) validation |
%  | F. Beutel | 8.0     |  10/04/15  | Use cell array 'invalid_files' to define excluded files instead of renaming the files |
%  | ehermeling| 8.1     |  24/06/15  | Made compatible with feature extraction (replaced PAT)
%  | F. Beutel | 8.5     |  11/08/15  | Expansion to new subject pools 4.2 and 4.3 for N=822 subjects in total |

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc;

%% Basic settings & definitions

%BP model version

%data folder
%%%data_folder = [pwd '\..\..\..\..\..\simband_data\PAT_based_BP_partial_IEEE_data_for_BP_model'];
%BP framework folder
%%%BP_framework_folder = pwd;

all_subjects = [1:822];%386
datapools_for_validation = {'2.1', '2.2', '2.3', '3.1', '3.2', '3.3', '4.2', '4.3'}; %2.1:imecNL, 2.2:imecBE, 2.3:Xmas, 3.1:imecBE LSDC imecTeam, 3.2:imecBE LSDC Asad, 
chan = 'e';

%uncomment to preselect subjects for validation
%     bad_subjects = [2, 12, 13, 15, 27, 30];
%     good_subjects = setdiff(all_subjects, bad_subjects)
%     all_subjects = good_subjects;

%define outcome matrices for errors of the respective models
outcome_matrix_sitting = [];
outcome_matrix_sitting_no_ssb = [];
outcome_matrix_standing = [];
outcome_matrix_standing_no_ssb = [];

%define matrix for keeping track on SSB (separately per intervention)
SSB_matrix_sitting = [];
SSB_matrix_standing = [];

%define vector for keeping track on PPG channels chosen (separately per intervention)
% channel_vector_sitting = []; %to output channels chosen by PAT
% channel_vector_standing = []; 

%define matrices for saving average BP levels per subject (for overview of recruitment/included subjects in current model)
SBP_classification_sitting = [];
DBP_classification_sitting = [];
SBP_classification_standing = [];
DBP_classification_standing = [];

%define vector for BP CI
CI_BP_vector_sitting = [];
CI_BP_vector_standing = [];

%define matrices for PPG channel selection
mean_PAT_sitting = [];
mean_PAT_standing = []; %not yet implemented

%load cell array containing invalid files
%%%cd(data_folder)
load('invalid_files.mat');

for subject_idx = 1:numel(all_subjects)

    subject = all_subjects(subject_idx);
    
    %define subject_ID
    if subject < 10
        subject_id = strcat('SP000', num2str(subject));
    elseif (subject >= 10) && (subject < 100)
        subject_id = strcat('SP00', num2str(subject));
    elseif (subject >= 100) && (subject < 1000)
        subject_id = strcat('SP0', num2str(subject));
    elseif subject >= 1000
        subject_id = strcat('SP', num2str(subject));
    end
        
    foldername = subject_id;
    folder_struct = what(foldername);
    path_folder_current_subject = folder_struct.path;
    
    experiments = {'_S_SI_1' '_S_SI_2' '_S_SI_3' '_S_ST_1' '_S_ST_2' '_S_ST_3' '_V'};
    
        %% Sitting Experiment
        
        SBP_classification_sitting_subject = [];
        DBP_classification_sitting_subject = [];
        
        plotting_colors = {'b' 'g' 'r' 'r' 'g' 'g' 'r' 'g'};
        legend_vector = [];
        
        for exp_idx = 1:3
            
            %%%cd(subject_folder)
            data_file = ([subject_id, experiments{exp_idx},'.mat'])
            
            if ~exist(data_file) || (ismember(data_file(1:end-4), invalid_files{:})) %end-4 to delete '.mat' file ending
                
                disp ('Missing Validation Data')
                
            else
                
                %load data and assign filename to struct
                 load(data_file);
                 data.filename = cellstr(data_file);
                 
                 if ismember(data.datapool.signal, datapools_for_validation)
                     
                     %
                     %load(data_file);
                     %data.filename = cellstr(data_file);
                     %%%cd(BP_framework_folder)
                     [data] = BP_validation(data, chan);
                     
                     %load matrix for BP CI
                     %%%cd(subject_folder)
                     load([subject_id '_CI_SSB_matrix_sitting.mat']);
                     
                     %fill outcome matrix with all experiments
                     outcome_matrix_sitting = [outcome_matrix_sitting; ...
                         str2num(subject_id(3:6)) ...
                         data.bp.sbpref ...
                         data.bp.dbpref ...
                         data.ppg.(chan).bp.sbp_loso ...
                         data.ppg.(chan).bp.dbp_loso ...
                         data.ppg.(chan).bp.sbp ...
                         data.ppg.(chan).bp.dbp, ...
                         data.ppg.(chan).bp.sbp_loso_CInt, ...
                         data.ppg.(chan).bp.dbp_loso_CInt, ...
                         data.ppg.(chan).bp.sbp_CInt, ...
                         data.ppg.(chan).bp.dbp_CInt, ...
                         data.ppg.(chan).bp.sbp_loso_glm, ...
                         data.ppg.(chan).bp.dbp_loso_glm, ...
                         data.ppg.(chan).bp.sbp_glm, ...
                         data.ppg.(chan).bp.dbp_glm];
                     
                     outcome_matrix_sitting_no_ssb = [outcome_matrix_sitting_no_ssb; ...
                         str2num(subject_id(3:6)) ...
                         data.bp.sbpref ...
                         data.bp.dbpref ...
                         data.ppg.(chan).bp.sbp_loso_no_ssb ...
                         data.ppg.(chan).bp.dbp_loso_no_ssb ...
                         data.ppg.(chan).bp.sbp_no_ssb ...
                         data.ppg.(chan).bp.dbp_no_ssb ...
                         data.ppg.(chan).bp.sbp_loso_glm_no_ssb ...
                         data.ppg.(chan).bp.dbp_loso_glm_no_ssb ...
                         data.ppg.(chan).bp.sbp_glm_no_ssb ...
                         data.ppg.(chan).bp.dbp_glm_no_ssb];
                     
                     %%%%%%%%%%%%%%%%%%%%%%%% redefine import criteria, otherwise the following features are expected -->
%                      %%%%%%%%%%%%%%%%%%% DONE
%                      if ~isfield(data.pat, 'ppg_upstgrad_avg_for_BP')
%                          data.spotcheck.ppg_upstgrad.signal = NaN;
%                      end
%                      if ~isfield(data.pat, 'ppg_pp_ft_amp_avg_for_BP')
%                          data.spotcheck.ppg_pp_ft_amp.signal = NaN;
%                      end       
%                      if ~isfield(data.pat, 'pp_sp_int_avg_for_BP')
%                          data.spotcheck.pp_sp_int.signal = NaN;
%                      end                       
%                      if ~isfield(data.pat, 'ppg_risetime_avg_for_BP')
%                          data.spotcheck.risetime.signal = NaN;
%                      end   
%                      if ~isfield(data.pat, 'ppg_decaytime_avg_for_BP')
%                          data.spotcheck.decaytime.signal = NaN;
%                      end
%                      if ~isfield(data.pat, 'ppg_RCtime_avg_for_BP')
%                          data.spotcheck.RCtime.signal = NaN;
%                      end                                        
%                      
%                      %%%%%%%%%%%%%%%%%%%
                     
                     SSB_matrix_sitting = [SSB_matrix_sitting; ...
                         str2num(subject_id(3:6)) ...
                         data.ssb.age.signal(1), ...
                         data.ssb.height.signal(1), ...
                         data.ssb.weight.signal(1), ...
                         data.bp.sbpref, ...
                         data.bp.dbpref, ...
                         data.ppg.(chan).spotcheck.pat.signal(1), ...
                         data.ppg.(chan).spotcheck.pat.sd(1), ...
                         data.ppg.(chan).spotcheck.CI(1), ...
                         data.ppg.(chan).spotcheck.HR.signal(1), ...
                         data.ppg.(chan).spotcheck.HR.sd(1), ...
                         data.ssb.arml.signal(1), ...
                         data.ppg.(chan).spotcheck.ppg_upstgrad.signal(1), ...
                         data.ppg.(chan).spotcheck.ppg_pp_ft_amp.signal(1),...
                         data.ppg.(chan).spotcheck.pp_sp_int.signal(1), ...
                         data.ppg.(chan).spotcheck.risetime.signal(1), ...
                         data.ppg.(chan).spotcheck.decaytime.signal(1), ...
                         data.ppg.(chan).spotcheck.RCtime.signal(1), ...
                         data.ppg.(chan).spotcheck.CIppg.signal(1)];
                     
%                      channel_vector_sitting = [channel_vector_sitting; ...
%                          data.ppg.(chan).pat.channel];
                     
                     
                     %%%%%%%%% Debug plot from AlIn.m %%%%%%%%%%%%
                     %             cd([BP_framework_folder '\..\..\ValidationFramework\ViewReporting']);
                     %             plotDebugInfoPATandBP(data, 'ppg.e', data_file)
                     
                     %%%%%%%%%PLOTTING EXPERIMENTS FOR PAT VARIATION%%%%%%%%%
                     
                     %                     %plot SELECTED ppg and detected beats per subject for debugging
                     %
                     %     %                 saveas(h,'selected_ppg_channels','fig')
                     %     %                 saveas(h,'selected_ppg_channels','png')
                     %
                     %                     %plot ALL ppg and and ECG
                     %                     legend_vector_all = [];
                     %                     mean_PAT_sitting_current_subject = []; %fill with mean PATs per channel
                     %                     figure (1000 + subject + exp_idx*100), hold on;
                     %                     channels = {'e'}%{'a','b', 'c', 'd', 'e', 'f', 'g', 'h'};
                     %                     channels_nr = {'4'}%{'0','1', '2', '3', '4', '5', '6', '7'};
                     %                     for channel_idx = 1:numel(channels)
                     %                         current_ppg = getfield(data.ppg, channels{channel_idx});
                     %                         mean_pat_ppg = round(nanmean(current_ppg.ppg.(chan).pat.pat_avg.signal(1,:)).*1000); %mean PAT of selected ppg
                     %                         mean_PAT_sitting_current_subject = [mean_PAT_sitting_current_subject, mean_pat_ppg];
                     %
                     %                         if strcmp(data.ppg.(chan).pat.channel,channels{channel_idx})
                     %                             legend_entry = [channels{channel_idx}, ' ', channels_nr{channel_idx}, ' ', num2str(mean_pat_ppg),' [ms]!'];
                     %                         else
                     %                             legend_entry = [channels{channel_idx}, ' ', channels_nr{channel_idx}, ' ', num2str(mean_pat_ppg),' [ms]-'];
                     %                         end
                     %                         legend_vector_all = [legend_vector_all; legend_entry];
                     %                         handle(channel_idx) = plot(current_ppg.signal, plotting_colors{channel_idx});
                     %                         %current_beat_locations = current_ppg.bd.upstroke;
                     %                         title(subject_id);
                     %                         legend(handle, legend_vector_all);
                     %                         %scatter(current_ppg.beats(1,4:end).*128, current_ppg.signal(current_beat_locations),'ok');
                     %                         text(length(current_ppg.signal)/2, mean(current_ppg.signal), channels{channel_idx}, 'FontSize',18)
                     %                     end
                     %                     %fill matrix with mean PATs per channel for current subject
                     %                     mean_PAT_sitting = [mean_PAT_sitting; mean_PAT_sitting_current_subject];
                     %
                     %     %                 saveas(handle,'all_ppg_channels','fig')
                     %     %                 saveas(handle,'all_ppg_channels','png')
                     %                     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                     
                     SBP_classification_sitting_subject = [SBP_classification_sitting_subject; data.bp.sbpref];
                     DBP_classification_sitting_subject = [DBP_classification_sitting_subject; data.bp.dbpref];
                     
                     %% BP_CI plot & save in data struct
                     
                     if ~exist('CI_SSB_matrix_sitting_loso.mat')
                         CI_SSB_matrix_sitting_loso = CI_SSB_matrix_sitting_no_loso;
                     end                     
                     
                     %%%cd(BP_framework_folder)
                     %define normalized data for boxplot of all other subjects
                     age_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,2));
                     height_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,3));
                     weight_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,4));
                     PAT_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,7));
                     PATSD_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,8));
                     HR_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,10));
                     HRSD_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,11));
                     % normalized data of current subject (as defined in BP.m)
                     %'data.ssb.age_norm'
                     data.ssb.age_norm_distance;
                     
                     number_rows = 1;
                     number_plots = 7;
                     
                     %                     figure(2000 + subject + exp_idx*100), hold on
                     %                     subplot(number_rows,number_plots,1), hold on
                     %                     boxplot(age_norm_loso,{'age'})
                     %                     scatter(1, data.ssb.age_norm, 'r', 'fill');
                     %                     subplot(number_rows,number_plots,2), hold on
                     %                     boxplot(height_norm_loso,{'height'})
                     %                     scatter(1, data.ssb.height_norm, 'r', 'fill');
                     %                     subplot(number_rows,number_plots,3), hold on
                     %                     boxplot(weight_norm_loso,{'weight'})
                     %                     scatter(1, data.ssb.weight_norm, 'r', 'fill');
                     %                     subplot(number_rows,number_plots,4), hold on
                     %                     boxplot(PAT_norm_loso,{'pat'})
                     %                     scatter(1, data.ppg.(chan).pat.pat_norm, 'r', 'fill');
                     %                     subplot(number_rows,number_plots,5), hold on
                     %                     boxplot(PATSD_norm_loso,{'pat sd'})
                     %                     scatter(1, data.ppg.(chan).pat.patSD_norm, 'r', 'fill');
                     %                     subplot(number_rows,number_plots,6), hold on
                     %                     boxplot(HR_norm_loso,{'hr'})
                     %                     scatter(1, data.ppg.(chan).pat.HR_norm, 'r', 'fill');
                     %                     subplot(number_rows,number_plots,7), hold on
                     %                     boxplot(HRSD_norm_loso,{'hr sd'})
                     %                     scatter(1, data.ppg.(chan).pat.HRSD_norm, 'r', 'fill');
                     
                     %calculate BP CI metric, i.e. averaged distance to the median
                     
                     data.ppg.(chan).bp.CI_bp = mean([data.ssb.age_norm_distance, ...
                         data.ssb.height_norm_distance, ...
                         data.ssb.weight_norm_distance, ...
                         data.ppg.(chan).spotcheck.pat_norm_distance, ...
                         data.ppg.(chan).spotcheck.patSD_norm_distance, ...
                         data.ppg.(chan).spotcheck.HR_norm_distance, ...
                         data.ppg.(chan).spotcheck.HRSD_norm_distance]);
                     
                     data.ppg.(chan).bp.CI_bp_ssb_only = mean([data.ssb.age_norm_distance, ...
                         data.ssb.height_norm_distance, ...
                         data.ssb.weight_norm_distance, ...
                         data.ppg.(chan).spotcheck.pat_norm_distance, ...
                         data.ppg.(chan).spotcheck.patSD_norm_distance, ...
                         data.ppg.(chan).spotcheck.HR_norm_distance, ...
                         data.ppg.(chan).spotcheck.HRSD_norm_distance]);
                     
                     CI_BP_vector_sitting = [CI_BP_vector_sitting; ...
                         str2num(subject_id(3:6)), ...
                         data.ssb.age_norm_distance,  ...
                         data.ssb.height_norm_distance,  ...
                         data.ssb.weight_norm_distance, ...
                         data.ppg.(chan).spotcheck.pat_norm_distance, ...
                         data.ppg.(chan).spotcheck.patSD_norm_distance, ...
                         data.ppg.(chan).spotcheck.HR_norm_distance, ...
                         data.ppg.(chan).spotcheck.HRSD_norm_distance, ...
                         data.ppg.(chan).bp.CI_bp, ...
                         data.ppg.(chan).bp.CI_bp_ssb_only];
                     
                 end
            end
        end
        
        SBP_classification_sitting = [SBP_classification_sitting; str2num(subject_id(3:6)) mean(SBP_classification_sitting_subject)];
        DBP_classification_sitting = [DBP_classification_sitting; str2num(subject_id(3:6)) mean(DBP_classification_sitting_subject)];
                
        %% Standing Experiment
%                 
%         SBP_classification_standing_subject = [];
%         DBP_classification_standing_subject = [];
%         
%         for exp_idx = 4:6
%             
%             %%%cd(subject_folder)
%             data_file = ([subject_id, experiments{exp_idx},'.mat'])
%             
%             if ~exist(data_file)  || (ismember(data_file(1:end-4), invalid_files{:})) %end-4 to delete '.mat' file ending
%                 disp ('Missing Experiment Data')
%             else 
%                  
%                 %load data and assign filename to struct
%                  load(data_file);
%                  data.filename = cellstr(data_file);
%                  
%                  if ismember(data.datapool.signal, datapools_for_validation)
%                      
%                      [data] = BP_validation(data, chan, -1);
%                      
%                      %load matrix for BP CI
%                      %%%cd(subject_folder)
%                      load([subject_id '_CI_SSB_matrix_standing.mat']);
%                      
%                      %fill outcome matrix with all experiments
%                      outcome_matrix_standing = [outcome_matrix_standing; ...
%                          str2num(subject_id(3:6)) ...
%                          data.bp.sbpref ...
%                          data.bp.dbpref ...
%                          data.ppg.(chan).bp.sbp_loso ...
%                          data.ppg.(chan).bp.dbp_loso ...
%                          data.ppg.(chan).bp.sbp ...
%                          data.ppg.(chan).bp.dbp, ...
%                          data.ppg.(chan).bp.sbp_loso_CInt, ...
%                          data.ppg.(chan).bp.dbp_loso_CInt, ...
%                          data.ppg.(chan).bp.sbp_CInt, ...
%                          data.ppg.(chan).bp.dbp_CInt, ...
%                          data.ppg.(chan).bp.sbp_loso_glm, ...
%                          data.ppg.(chan).bp.dbp_loso_glm, ...
%                          data.ppg.(chan).bp.sbp_glm, ...
%                          data.ppg.(chan).bp.dbp_glm];
%                      
%                      outcome_matrix_standing_no_ssb = [outcome_matrix_standing_no_ssb; ...
%                          str2num(subject_id(3:6)) ...
%                          data.bp.sbpref ...
%                          data.bp.dbpref ...
%                          data.ppg.(chan).bp.sbp_loso_no_ssb ...
%                          data.ppg.(chan).bp.dbp_loso_no_ssb ...
%                          data.ppg.(chan).bp.sbp_no_ssb ...
%                          data.ppg.(chan).bp.dbp_no_ssb ...
%                          data.ppg.(chan).bp.sbp_loso_glm_no_ssb ...
%                          data.ppg.(chan).bp.dbp_loso_glm_no_ssb ...
%                          data.ppg.(chan).bp.sbp_glm_no_ssb ...
%                          data.ppg.(chan).bp.dbp_glm_no_ssb];
%                      
%                      SSB_matrix_standing = [SSB_matrix_standing; ...
%                          str2num(subject_id(3:6)) ...
%                          data.ssb.age.signal(1), ...
%                          data.ssb.height.signal(1), ...
%                          data.ssb.weight.signal(1), ...
%                          data.bp.sbpref, ...
%                          data.bp.dbpref, ...
%                          data.ppg.(chan).spotcheck.pat.signal(1), ...
%                          data.ppg.(chan).spotcheck.pat.sd(1), ...
%                          data.ppg.(chan)..spotcheck.CI(1), ...
%                          data.ppg.(chan).spotcheck.HR.signal(1), ...
%                          data.ppg.(chan).spotcheck.HR.sd(1), ...
%                          data.ssb.arml.signal(1)];
%                      
% %                      channel_vector_standing = [channel_vector_standing; ...
% %                          data.ppg.(chan).pat.channel];
%                      
%                      SBP_classification_standing_subject = [SBP_classification_standing_subject; data.bp.sbpref];
%                      DBP_classification_standing_subject = [DBP_classification_standing_subject; data.bp.dbpref];
%                      
%                      %BP_CI plot & save in data struct
%                      
%                      %%%cd(BP_framework_folder)
%                      %define normalized data for boxplot of all other subjects
%                      age_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,2));
%                      height_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,3));
%                      weight_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,4));
%                      PAT_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,7));
%                      PATSD_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,8));
%                      HR_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,10));
%                      HRSD_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,11));
%                      % normalized data of current subject (as defined in BP.m)
%                      %'data.ssb.age_norm'
%                      data.ssb.age_norm_distance;
%                      
%                      number_rows = 1;
%                      number_plots = 7;
%                      
%                      %                     figure(2000 + subject + exp_idx*100), hold on
%                      %                     subplot(number_rows,number_plots,1), hold on
%                      %                     boxplot(age_norm_loso,{'age'})
%                      %                     scatter(1, data.ssb.age_norm, 'r', 'fill');
%                      %                     subplot(number_rows,number_plots,2), hold on
%                      %                     boxplot(height_norm_loso,{'height'})
%                      %                     scatter(1, data.ssb.height_norm, 'r', 'fill');
%                      %                     subplot(number_rows,number_plots,3), hold on
%                      %                     boxplot(weight_norm_loso,{'weight'})
%                      %                     scatter(1, data.ssb.weight_norm, 'r', 'fill');
%                      %                     subplot(number_rows,number_plots,4), hold on
%                      %                     boxplot(PAT_norm_loso,{'pat'})
%                      %                     scatter(1, data.ppg.(chan).pat.pat_norm, 'r', 'fill');
%                      %                     subplot(number_rows,number_plots,5), hold on
%                      %                     boxplot(PATSD_norm_loso,{'pat sd'})
%                      %                     scatter(1, data.ppg.(chan).pat.patSD_norm, 'r', 'fill');
%                      %                     subplot(number_rows,number_plots,6), hold on
%                      %                     boxplot(HR_norm_loso,{'hr'})
%                      %                     scatter(1, data.ppg.(chan).pat.HR_norm, 'r', 'fill');
%                      %                     subplot(number_rows,number_plots,7), hold on
%                      %                     boxplot(HRSD_norm_loso,{'hr sd'})
%                      %                     scatter(1, data.ppg.(chan).pat.HRSD_norm, 'r', 'fill');
%                      
%                      %calculate BP CI metric, i.e. averaged distance to the median
%                      
%                      data.ppg.(chan).bp.CI_bp = mean([data.ssb.age_norm_distance, ...
%                          data.ssb.height_norm_distance, ...
%                          data.ssb.weight_norm_distance, ...
%                          data.ppg.(chan).pat.pat_norm_distance, ...
%                          data.ppg.(chan).pat.patSD_norm_distance, ...
%                          data.ppg.(chan).pat.HR_norm_distance, ...
%                          data.ppg.(chan).pat.HRSD_norm_distance]);
%                      
%                      data.ppg.(chan).bp.CI_bp_ssb_only = mean([data.ssb.age_norm_distance, ...
%                          data.ssb.height_norm_distance, ...
%                          data.ssb.weight_norm_distance, ...
%                          data.ppg.(chan).pat.pat_norm_distance, ...
%                          data.ppg.(chan).pat.patSD_norm_distance, ...
%                          data.ppg.(chan).pat.HR_norm_distance, ...
%                          data.ppg.(chan).pat.HRSD_norm_distance]);
%                      
%                      CI_BP_vector_standing = [CI_BP_vector_standing; ...
%                          str2num(subject_id(3:6)), ...
%                          data.ssb.age_norm_distance,  ...
%                          data.ssb.height_norm_distance,  ...
%                          data.ssb.weight_norm_distance, ...
%                          data.ppg.(chan).pat.pat_norm_distance, ...
%                          data.ppg.(chan).pat.patSD_norm_distance, ...
%                          data.ppg.(chan).pat.HR_norm_distance, ...
%                          data.ppg.(chan).pat.HRSD_norm_distance, ...
%                          data.ppg.(chan).bp.CI_bp, ...
%                          data.ppg.(chan).bp.CI_bp_ssb_only];
%                      
%                  end
%             end
%         end      
%         
%         SBP_classification_standing = [SBP_classification_standing; str2num(subject_id(3:6)) mean(SBP_classification_standing_subject)];
%         DBP_classification_standing = [DBP_classification_standing; str2num(subject_id(3:6)) mean(DBP_classification_standing_subject)];        
        
        %% Variation Experiment
                
%         SBP_classification_variation_subject = [];
%         DBP_classification_variation_subject = [];
%         
%         for exp_idx = 4:6
%             
%             cd(subject_folder)
%             data_file = ([subject_id, experiments{exp_idx},'.mat'])
%             
%             if ~exist(data_file)
%                 disp ('Missing Experiment Data')
%             else
%                 load(data_file)
%                 cd('D:\SIMBA\simba-common_working_copy_BP_for_v2.0\data\Matlab\AllAlgorithms\')
%                 flip_PPG = 1;
%                 data = AllIn(data, flip_PPG);
%                 
%                 %fill outcome matrix with all experiments
%                 outcome_matrix_standing = [outcome_matrix_standing; ...
%                     str2num(subject_id(3:6)) ...
%                     data.bp.sbpref ...
%                     data.bp.dbpref ...
%                     data.ppg.(chan).bp.sbp_loso ...
%                     data.ppg.(chan).bp.dbp_loso ...
%                     data.ppg.(chan).bp.sbp ...
%                     data.ppg.(chan).bp.dbp, ...
%                     data.ppg.(chan).bp.sbp_loso_CInt, ...
%                     data.ppg.(chan).bp.dbp_loso_CInt, ...
%                     data.ppg.(chan).bp.sbp_CInt, ...
%                     data.ppg.(chan).bp.dbp_CInt]
%                 
%                 outcome_matrix_standing_no_ssb = [outcome_matrix_standing_no_ssb; ...
%                     str2num(subject_id(3:6)) ...
%                     data.bp.sbpref ...
%                     data.bp.dbpref ...
%                     data.ppg.(chan).bp.sbp_loso_no_ssb ...
%                     data.ppg.(chan).bp.dbp_loso_no_ssb ...
%                     data.ppg.(chan).bp.sbp_no_ssb ...
%                     data.ppg.(chan).bp.dbp_no_ssb]                
%                 
%                 SSB_matrix_standing = [SSB_matrix_standing; ...
%                     str2num(subject_id(3:6)) ...
%                     data.ssb.age, ...
%                     data.ssb.height, ...
%                     data.ssb.weight, ...
%                     data.bp.sbpref, ...
%                     data.bp.dbpref, ...
%                     data.ppg.(chan).pat.pat, ...
%                     data.ppg.(chan).pat.patSD, ...
%                     data.ppg.(chan).pat.CI_pat, ...
%                     data.ppg.(chan).pat.HR, ...
%                     data.ppg.(chan).pat.HRSD]
%                 
%                 channel_vector_standing = [channel_vector_standing; ...
%                     data.ppg.(chan).pat.channel]  
%                 
%                 SBP_classification_standing_subject = [SBP_classification_standing_subject; data.bp.sbpref];
%                 DBP_classification_standing_subject = [DBP_classification_standing_subject; data.bp.dbpref];
%                 
%             end
%         end      
%         
%         SBP_classification_standing = [SBP_classification_standing; str2num(subject_id(3:6)) mean(SBP_classification_standing_subject)];
%         DBP_classification_standing = [DBP_classification_standing; str2num(subject_id(3:6)) mean(DBP_classification_standing_subject)];                
        
        
end
        
%% Aggregate error metrics

%SITTING

%Sitting loso
SBP_est_sitting_rf = round(outcome_matrix_sitting(:,4));
DBP_est_sitting_rf= round(outcome_matrix_sitting(:,5)); 

error_SBP_sitting_rf = outcome_matrix_sitting(:,2) - round(outcome_matrix_sitting(:,4));%BP_ref-BP_est
error_DBP_sitting_rf = outcome_matrix_sitting(:,3) - round(outcome_matrix_sitting(:,5));

abs_error_SBP_sitting_rf = abs(error_SBP_sitting_rf);
abs_error_DBP_sitting_rf = abs(error_DBP_sitting_rf);

mean_abs_error_SBP_sitting_rf = mean(abs_error_SBP_sitting_rf);
mean_abs_error_DBP_sitting_rf = mean(abs_error_DBP_sitting_rf);

perc_in_thresh_SBP_sitting_rf = round(100.*(sum(abs_error_SBP_sitting_rf<=7)/numel(abs_error_SBP_sitting_rf)));
perc_in_thresh_DBP_sitting_rf = round(100.*(sum(abs_error_DBP_sitting_rf<=7)/numel(abs_error_DBP_sitting_rf)));

%Sitting loso GLM
SBP_est_sitting_glm = round(outcome_matrix_sitting(:,12));
DBP_est_sitting_glm = round(outcome_matrix_sitting(:,13));

error_SBP_sitting_glm = outcome_matrix_sitting(:,2) - SBP_est_sitting_glm;%BP_ref-BP_est
error_DBP_sitting_glm = outcome_matrix_sitting(:,3) - DBP_est_sitting_glm;

abs_error_SBP_sitting_glm = abs(error_SBP_sitting_glm);
abs_error_DBP_sitting_glm = abs(error_DBP_sitting_glm);

mean_abs_error_SBP_sitting_glm = nanmean(abs_error_SBP_sitting_glm);
mean_abs_error_DBP_sitting_glm = nanmean(abs_error_DBP_sitting_glm);

perc_in_thresh_SBP_sitting_glm = round(100.*(sum(abs_error_SBP_sitting_glm<=7)/numel(abs_error_SBP_sitting_glm)));
perc_in_thresh_DBP_sitting_glm = round(100.*(sum(abs_error_DBP_sitting_glm<=7)/numel(abs_error_DBP_sitting_glm)));

%Sitting loso no ssb
SBP_est_sitting_no_ssb = round(outcome_matrix_sitting_no_ssb(:,4));
DBP_est_sitting_no_ssb = round(outcome_matrix_sitting_no_ssb(:,5)); 

error_SBP_sitting_no_ssb_rf = outcome_matrix_sitting_no_ssb(:,2) - round(outcome_matrix_sitting_no_ssb(:,4));
error_DBP_sitting_no_ssb_rf = outcome_matrix_sitting_no_ssb(:,3) - round(outcome_matrix_sitting_no_ssb(:,5));

abs_error_SBP_sitting_no_ssb_rf = abs(error_SBP_sitting_no_ssb_rf);
abs_error_DBP_sitting_no_ssb_rf = abs(error_DBP_sitting_no_ssb_rf);

mean_abs_error_SBP_sitting_no_ssb_rf = mean(abs_error_SBP_sitting_no_ssb_rf);
mean_abs_error_DBP_sitting_no_ssb_rf = mean(abs_error_DBP_sitting_no_ssb_rf);

perc_in_thresh_SBP_sitting_no_ssb_rf = round(100.*(sum(abs_error_SBP_sitting_no_ssb_rf<=7)/numel(abs_error_SBP_sitting_no_ssb_rf)));
perc_in_thresh_DBP_sitting_no_ssb_rf = round(100.*(sum(abs_error_DBP_sitting_no_ssb_rf<=7)/numel(abs_error_DBP_sitting_no_ssb_rf)));

%Sitting loso GLM no ssb
SBP_est_sitting_no_ssb_glm = round(outcome_matrix_sitting_no_ssb(:,8));
DBP_est_sitting_no_ssb_glm = round(outcome_matrix_sitting_no_ssb(:,9)); 

error_SBP_sitting_no_ssb_glm = outcome_matrix_sitting(:,2) - SBP_est_sitting_no_ssb_glm;%BP_ref-BP_est
error_DBP_sitting_no_ssb_glm = outcome_matrix_sitting(:,3) - DBP_est_sitting_no_ssb_glm;

abs_error_SBP_sitting_no_ssb_glm = abs(error_SBP_sitting_no_ssb_glm);
abs_error_DBP_sitting_no_ssb_glm = abs(error_DBP_sitting_no_ssb_glm);

mean_abs_error_SBP_sitting_no_ssb_glm = nanmean(abs_error_SBP_sitting_no_ssb_glm);
mean_abs_error_DBP_sitting_no_ssb_glm = nanmean(abs_error_DBP_sitting_no_ssb_glm);

perc_in_thresh_SBP_sitting_no_ssb_glm = round(100.*(sum(abs_error_SBP_sitting_no_ssb_glm<=7)/numel(abs_error_SBP_sitting_no_ssb_glm)));
perc_in_thresh_DBP_sitting_no_ssb_glm = round(100.*(sum(abs_error_DBP_sitting_no_ssb_glm<=7)/numel(abs_error_DBP_sitting_no_ssb_glm)));

%Sitting Full
SBP_est_sitting_full_model_rf = round(outcome_matrix_sitting(:,6));
DBP_est_sitting_full_model_rf = round(outcome_matrix_sitting(:,7)); 

error_SBP_sitting_full_model_rf = outcome_matrix_sitting(:,2) - round(outcome_matrix_sitting(:,6));
error_DBP_sitting_full_model_rf = outcome_matrix_sitting(:,3) - round(outcome_matrix_sitting(:,7));

abs_error_SBP_sitting_full_model_rf = abs(error_SBP_sitting_full_model_rf);
abs_error_DBP_sitting_full_model_rf = abs(error_DBP_sitting_full_model_rf);

mean_abs_error_SBP_sitting_full_model_rf = mean(abs_error_SBP_sitting_full_model_rf);
mean_abs_error_DBP_sitting_full_model_rf = mean(abs_error_DBP_sitting_full_model_rf);

perc_in_thresh_SBP_sitting_full_model_rf = round(100.*(sum(abs_error_SBP_sitting_full_model_rf<=7)/numel(abs_error_SBP_sitting_full_model_rf)));
perc_in_thresh_DBP_sitting_full_model_rf = round(100.*(sum(abs_error_DBP_sitting_full_model_rf<=7)/numel(abs_error_DBP_sitting_full_model_rf)));    

%Sitting full GLM
SBP_est_sitting_full_model_glm = round(outcome_matrix_sitting(:,14));
DBP_est_sitting_full_model_glm = round(outcome_matrix_sitting(:,15));

error_SBP_sitting_full_model_glm = outcome_matrix_sitting(:,2) - SBP_est_sitting_full_model_glm;%BP_ref-BP_est
error_DBP_sitting_full_model_glm = outcome_matrix_sitting(:,3) - DBP_est_sitting_full_model_glm;

abs_error_SBP_sitting_full_model_glm = abs(error_SBP_sitting_full_model_glm);
abs_error_DBP_sitting_full_model_glm = abs(error_DBP_sitting_full_model_glm);

mean_abs_error_SBP_sitting_full_model_glm = nanmean(abs_error_SBP_sitting_full_model_glm);
mean_abs_error_DBP_sitting_full_model_glm = nanmean(abs_error_DBP_sitting_full_model_glm);

perc_in_thresh_SBP_sitting_full_model_glm = round(100.*(sum(abs_error_SBP_sitting_full_model_glm<=7)/numel(abs_error_SBP_sitting_full_model_glm)));
perc_in_thresh_DBP_sitting_full_model_glm = round(100.*(sum(abs_error_DBP_sitting_full_model_glm<=7)/numel(abs_error_DBP_sitting_full_model_glm)));

%Sitting Full no ssb
SBP_est_sitting_full_model_no_ssb_rf = round(outcome_matrix_sitting_no_ssb(:,6));
DBP_est_sitting_full_model_no_ssb_rf = round(outcome_matrix_sitting_no_ssb(:,7)); 

error_SBP_sitting_full_model_no_ssb_rf = outcome_matrix_sitting_no_ssb(:,2) - round(outcome_matrix_sitting_no_ssb(:,6));
error_DBP_sitting_full_model_no_ssb_rf = outcome_matrix_sitting_no_ssb(:,3) - round(outcome_matrix_sitting_no_ssb(:,7));

abs_error_SBP_sitting_full_model_no_ssb_rf = abs(error_SBP_sitting_full_model_no_ssb_rf);
abs_error_DBP_sitting_full_model_no_ssb_rf = abs(error_DBP_sitting_full_model_no_ssb_rf);

mean_abs_error_SBP_sitting_full_model_no_ssb_rf = mean(abs_error_SBP_sitting_full_model_no_ssb_rf);
mean_abs_error_DBP_sitting_full_model_no_ssb_rf = mean(abs_error_DBP_sitting_full_model_no_ssb_rf);

perc_in_thresh_SBP_sitting_full_model_no_ssb_rf = round(100.*(sum(abs_error_SBP_sitting_full_model_no_ssb_rf<=7)/numel(abs_error_SBP_sitting_full_model_no_ssb_rf)));
perc_in_thresh_DBP_sitting_full_model_no_ssb_rf = round(100.*(sum(abs_error_DBP_sitting_full_model_no_ssb_rf<=7)/numel(abs_error_DBP_sitting_full_model_no_ssb_rf)));
 
%Sitting full GLM no ssb
SBP_est_sitting_full_model_no_ssb_glm = round(outcome_matrix_sitting_no_ssb(:,10));
DBP_est_sitting_full_model_no_ssb_glm = round(outcome_matrix_sitting_no_ssb(:,11));

error_SBP_sitting_full_model_no_ssb_glm = outcome_matrix_sitting_no_ssb(:,2) - SBP_est_sitting_full_model_no_ssb_glm;%BP_ref-BP_est
error_DBP_sitting_full_model_no_ssb_glm = outcome_matrix_sitting_no_ssb(:,3) - DBP_est_sitting_full_model_no_ssb_glm;

abs_error_SBP_sitting_full_model_no_ssb_glm = abs(error_SBP_sitting_full_model_no_ssb_glm);
abs_error_DBP_sitting_full_model_no_ssb_glm = abs(error_DBP_sitting_full_model_no_ssb_glm);

mean_abs_error_SBP_sitting_full_model_no_ssb_glm = nanmean(abs_error_SBP_sitting_full_model_no_ssb_glm);
mean_abs_error_DBP_sitting_full_model_no_ssb_glm = nanmean(abs_error_DBP_sitting_full_model_no_ssb_glm);

perc_in_thresh_SBP_sitting_full_model_no_ssb_glm = round(100.*(sum(abs_error_SBP_sitting_full_model_no_ssb_glm<=7)/numel(abs_error_SBP_sitting_full_model_no_ssb_glm)));
perc_in_thresh_DBP_sitting_full_model_no_ssb_glm = round(100.*(sum(abs_error_DBP_sitting_full_model_no_ssb_glm<=7)/numel(abs_error_DBP_sitting_full_model_no_ssb_glm)));

%     %standing
% 
%     %standing loso
%     SBP_est_standing_rf = round(outcome_matrix_standing(:,4));
%     DBP_est_standing_rf= round(outcome_matrix_standing(:,5)); 
% 
%     error_SBP_standing_rf = outcome_matrix_standing(:,2) - round(outcome_matrix_standing(:,4));%BP_ref-BP_est
%     error_DBP_standing_rf = outcome_matrix_standing(:,3) - round(outcome_matrix_standing(:,5));
% 
%     abs_error_SBP_standing_rf = abs(error_SBP_standing_rf);
%     abs_error_DBP_standing_rf = abs(error_DBP_standing_rf);
% 
%     mean_abs_error_SBP_standing_rf = mean(abs_error_SBP_standing_rf);
%     mean_abs_error_DBP_standing_rf = mean(abs_error_DBP_standing_rf);
% 
%     perc_in_thresh_SBP_standing_rf = round(100.*(sum(abs_error_SBP_standing_rf<=7)/numel(abs_error_SBP_standing_rf)));
%     perc_in_thresh_DBP_standing_rf = round(100.*(sum(abs_error_DBP_standing_rf<=7)/numel(abs_error_DBP_standing_rf)));
% 
%     %standing loso GLM
%     SBP_est_standing_glm = round(outcome_matrix_standing(:,12));
%     DBP_est_standing_glm = round(outcome_matrix_standing(:,13));
% 
%     error_SBP_standing_glm = outcome_matrix_standing(:,2) - SBP_est_standing_glm;%BP_ref-BP_est
%     error_DBP_standing_glm = outcome_matrix_standing(:,3) - DBP_est_standing_glm;
% 
%     abs_error_SBP_standing_glm = abs(error_SBP_standing_glm);
%     abs_error_DBP_standing_glm = abs(error_DBP_standing_glm);
% 
%     mean_abs_error_SBP_standing_glm = nanmean(abs_error_SBP_standing_glm);
%     mean_abs_error_DBP_standing_glm = nanmean(abs_error_DBP_standing_glm);
% 
%     perc_in_thresh_SBP_standing_glm = round(100.*(sum(abs_error_SBP_standing_glm<=7)/numel(abs_error_SBP_standing_glm)));
%     perc_in_thresh_DBP_standing_glm = round(100.*(sum(abs_error_DBP_standing_glm<=7)/numel(abs_error_DBP_standing_glm)));
% 
%     %standing loso no ssb
%     SBP_est_standing_no_ssb = round(outcome_matrix_standing_no_ssb(:,4));
%     DBP_est_standing_no_ssb = round(outcome_matrix_standing_no_ssb(:,5)); 
% 
%     error_SBP_standing_no_ssb_rf = outcome_matrix_standing_no_ssb(:,2) - round(outcome_matrix_standing_no_ssb(:,4));
%     error_DBP_standing_no_ssb_rf = outcome_matrix_standing_no_ssb(:,3) - round(outcome_matrix_standing_no_ssb(:,5));
% 
%     abs_error_SBP_standing_no_ssb_rf = abs(error_SBP_standing_no_ssb_rf);
%     abs_error_DBP_standing_no_ssb_rf = abs(error_DBP_standing_no_ssb_rf);
% 
%     mean_abs_error_SBP_standing_no_ssb_rf = mean(abs_error_SBP_standing_no_ssb_rf);
%     mean_abs_error_DBP_standing_no_ssb_rf = mean(abs_error_DBP_standing_no_ssb_rf);
% 
%     perc_in_thresh_SBP_standing_no_ssb_rf = round(100.*(sum(abs_error_SBP_standing_no_ssb_rf<=7)/numel(abs_error_SBP_standing_no_ssb_rf)));
%     perc_in_thresh_DBP_standing_no_ssb_rf = round(100.*(sum(abs_error_DBP_standing_no_ssb_rf<=7)/numel(abs_error_DBP_standing_no_ssb_rf)));
% 
%     %standing loso GLM no ssb
%     SBP_est_standing_no_ssb_glm = round(outcome_matrix_standing_no_ssb(:,8));
%     DBP_est_standing_no_ssb_glm = round(outcome_matrix_standing_no_ssb(:,9)); 
% 
%     error_SBP_standing_no_ssb_glm = outcome_matrix_standing(:,2) - SBP_est_standing_no_ssb_glm;%BP_ref-BP_est
%     error_DBP_standing_no_ssb_glm = outcome_matrix_standing(:,3) - DBP_est_standing_no_ssb_glm;
% 
%     abs_error_SBP_standing_no_ssb_glm = abs(error_SBP_standing_no_ssb_glm);
%     abs_error_DBP_standing_no_ssb_glm = abs(error_DBP_standing_no_ssb_glm);
% 
%     mean_abs_error_SBP_standing_no_ssb_glm = nanmean(abs_error_SBP_standing_no_ssb_glm);
%     mean_abs_error_DBP_standing_no_ssb_glm = nanmean(abs_error_DBP_standing_no_ssb_glm);
% 
%     perc_in_thresh_SBP_standing_no_ssb_glm = round(100.*(sum(abs_error_SBP_standing_no_ssb_glm<=7)/numel(abs_error_SBP_standing_no_ssb_glm)));
%     perc_in_thresh_DBP_standing_no_ssb_glm = round(100.*(sum(abs_error_DBP_standing_no_ssb_glm<=7)/numel(abs_error_DBP_standing_no_ssb_glm)));
% 
%     %standing Full
%     SBP_est_standing_full_model_rf = round(outcome_matrix_standing(:,6));
%     DBP_est_standing_full_model_rf = round(outcome_matrix_standing(:,7)); 
% 
%     error_SBP_standing_full_model_rf = outcome_matrix_standing(:,2) - round(outcome_matrix_standing(:,6));
%     error_DBP_standing_full_model_rf = outcome_matrix_standing(:,3) - round(outcome_matrix_standing(:,7));
% 
%     abs_error_SBP_standing_full_model_rf = abs(error_SBP_standing_full_model_rf);
%     abs_error_DBP_standing_full_model_rf = abs(error_DBP_standing_full_model_rf);
% 
%     mean_abs_error_SBP_standing_full_model_rf = mean(abs_error_SBP_standing_full_model_rf);
%     mean_abs_error_DBP_standing_full_model_rf = mean(abs_error_DBP_standing_full_model_rf);
% 
%     perc_in_thresh_SBP_standing_full_model_rf = round(100.*(sum(abs_error_SBP_standing_full_model_rf<=7)/numel(abs_error_SBP_standing_full_model_rf)));
%     perc_in_thresh_DBP_standing_full_model_rf = round(100.*(sum(abs_error_DBP_standing_full_model_rf<=7)/numel(abs_error_DBP_standing_full_model_rf)));    
% 
%     %standing full GLM
%     SBP_est_standing_full_model_glm = round(outcome_matrix_standing(:,14));
%     DBP_est_standing_full_model_glm = round(outcome_matrix_standing(:,15));
% 
%     error_SBP_standing_full_model_glm = outcome_matrix_standing(:,2) - SBP_est_standing_full_model_glm;%BP_ref-BP_est
%     error_DBP_standing_full_model_glm = outcome_matrix_standing(:,3) - DBP_est_standing_full_model_glm;
% 
%     abs_error_SBP_standing_full_model_glm = abs(error_SBP_standing_full_model_glm);
%     abs_error_DBP_standing_full_model_glm = abs(error_DBP_standing_full_model_glm);
% 
%     mean_abs_error_SBP_standing_full_model_glm = nanmean(abs_error_SBP_standing_full_model_glm);
%     mean_abs_error_DBP_standing_full_model_glm = nanmean(abs_error_DBP_standing_full_model_glm);
% 
%     perc_in_thresh_SBP_standing_full_model_glm = round(100.*(sum(abs_error_SBP_standing_full_model_glm<=7)/numel(abs_error_SBP_standing_full_model_glm)));
%     perc_in_thresh_DBP_standing_full_model_glm = round(100.*(sum(abs_error_DBP_standing_full_model_glm<=7)/numel(abs_error_DBP_standing_full_model_glm)));
% 
%     %standing Full no ssb
%     SBP_est_standing_full_model_no_ssb_rf = round(outcome_matrix_standing_no_ssb(:,6));
%     DBP_est_standing_full_model_no_ssb_rf = round(outcome_matrix_standing_no_ssb(:,7)); 
% 
%     error_SBP_standing_full_model_no_ssb_rf = outcome_matrix_standing_no_ssb(:,2) - round(outcome_matrix_standing_no_ssb(:,6));
%     error_DBP_standing_full_model_no_ssb_rf = outcome_matrix_standing_no_ssb(:,3) - round(outcome_matrix_standing_no_ssb(:,7));
% 
%     abs_error_SBP_standing_full_model_no_ssb_rf = abs(error_SBP_standing_full_model_no_ssb_rf);
%     abs_error_DBP_standing_full_model_no_ssb_rf = abs(error_DBP_standing_full_model_no_ssb_rf);
% 
%     mean_abs_error_SBP_standing_full_model_no_ssb_rf = mean(abs_error_SBP_standing_full_model_no_ssb_rf);
%     mean_abs_error_DBP_standing_full_model_no_ssb_rf = mean(abs_error_DBP_standing_full_model_no_ssb_rf);
% 
%     perc_in_thresh_SBP_standing_full_model_no_ssb_rf = round(100.*(sum(abs_error_SBP_standing_full_model_no_ssb_rf<=7)/numel(abs_error_SBP_standing_full_model_no_ssb_rf)));
%     perc_in_thresh_DBP_standing_full_model_no_ssb_rf = round(100.*(sum(abs_error_DBP_standing_full_model_no_ssb_rf<=7)/numel(abs_error_DBP_standing_full_model_no_ssb_rf)));
% 
%     %standing full GLM no ssb
%     SBP_est_standing_full_model_no_ssb_glm = round(outcome_matrix_standing_no_ssb(:,10));
%     DBP_est_standing_full_model_no_ssb_glm = round(outcome_matrix_standing_no_ssb(:,11));
% 
%     error_SBP_standing_full_model_no_ssb_glm = outcome_matrix_standing_no_ssb(:,2) - SBP_est_standing_full_model_no_ssb_glm;%BP_ref-BP_est
%     error_DBP_standing_full_model_no_ssb_glm = outcome_matrix_standing_no_ssb(:,3) - DBP_est_standing_full_model_no_ssb_glm;
% 
%     abs_error_SBP_standing_full_model_no_ssb_glm = abs(error_SBP_standing_full_model_no_ssb_glm);
%     abs_error_DBP_standing_full_model_no_ssb_glm = abs(error_DBP_standing_full_model_no_ssb_glm);
% 
%     mean_abs_error_SBP_standing_full_model_no_ssb_glm = nanmean(abs_error_SBP_standing_full_model_no_ssb_glm);
%     mean_abs_error_DBP_standing_full_model_no_ssb_glm = nanmean(abs_error_DBP_standing_full_model_no_ssb_glm);
% 
%     perc_in_thresh_SBP_standing_full_model_no_ssb_glm = round(100.*(sum(abs_error_SBP_standing_full_model_no_ssb_glm<=7)/numel(abs_error_SBP_standing_full_model_no_ssb_glm)));
%     perc_in_thresh_DBP_standing_full_model_no_ssb_glm = round(100.*(sum(abs_error_DBP_standing_full_model_no_ssb_glm<=7)/numel(abs_error_DBP_standing_full_model_no_ssb_glm)));

%% Plot error boxplots    

%%%cd(BP_framework_folder)
%SITTING

%sitting loso
fig_sitting_LOSO_SSB = figure('Name','Sitting LOSO incl. SSB'); hold on;
title('Sitting LOSO incl. SSB');
boxplot([abs_error_SBP_sitting_rf, abs_error_DBP_sitting_rf],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_rf, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_rf, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_rf)])
text(2+0.2, mean_abs_error_DBP_sitting_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_rf)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')
savefig([path_folder_current_subject '\..\outcome_validation\fig_performance_rf_sitting_loso.fig']);

%sitting loso GLM
fig_sitting_LOSO_SSB_GLM = figure('Name','Sitting LOSO incl. SSB GLM'); hold on;
title('Sitting LOSO incl. SSB GLM');
boxplot([abs_error_SBP_sitting_glm, abs_error_DBP_sitting_glm],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_glm, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_glm, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_glm)])
text(2+0.2, mean_abs_error_DBP_sitting_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_glm)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_glm), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_glm), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')
savefig([path_folder_current_subject '\..\outcome_validation\fig_performance_glm_sitting_loso.fig']);

%sitting loso no ssb
fig_sitting_LOSO_SSB_no_ssb = figure('Name','Sitting LOSO excl. SSB'); hold on;
title('Sitting LOSO excl. SSB');
boxplot([abs_error_SBP_sitting_no_ssb_rf, abs_error_DBP_sitting_no_ssb_rf],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_no_ssb_rf, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_no_ssb_rf, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_no_ssb_rf)])
text(2+0.2, mean_abs_error_DBP_sitting_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_no_ssb_rf)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_no_ssb_rf), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_no_ssb_rf), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')

%sitting loso GLM no ssb
fig_sitting_LOSO_SSB_GLM_no_ssb = figure('Name','Sitting LOSO excl. SSB GLM'); hold on;
title('Sitting LOSO excl. SSB GLM');
boxplot([abs_error_SBP_sitting_no_ssb_glm, abs_error_DBP_sitting_no_ssb_glm],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_no_ssb_glm, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_no_ssb_glm, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_no_ssb_glm)])
text(2+0.2, mean_abs_error_DBP_sitting_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_no_ssb_glm)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_no_ssb_glm), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_no_ssb_glm), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')
savefig([path_folder_current_subject '\..\outcome_validation\fig_performance_glm_sitting_loso_no_ssb.fig']);

%sitting full
fig_sitting_full_SSB = figure('Name','Sitting Full Model incl. SSB'); hold on;
title('Sitting Full Model incl. SSB');
boxplot([abs_error_SBP_sitting_full_model_rf, abs_error_DBP_sitting_full_model_rf],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_full_model_rf, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_full_model_rf, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_full_model_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_full_model_rf)])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_full_model_rf)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_full_model_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_full_model_rf), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_full_model_rf), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')

%sitting full GLM
fig_sitting_full_SSB_GLM = figure('Name','Sitting Full Model incl. SSB GLM'); hold on;
title('Sitting Full Model incl. SSB GLM');
boxplot([abs_error_SBP_sitting_full_model_glm, abs_error_DBP_sitting_full_model_glm],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_full_model_glm, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_full_model_glm, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_full_model_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_full_model_glm)])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_full_model_glm)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_full_model_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_full_model_glm), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_full_model_glm), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')
savefig([path_folder_current_subject '\..\outcome_validation\fig_performance_glm_sitting_full_model.fig']);

%sitting full no ssb
fig_sitting_full_no_ssb = figure('Name','Sitting Full Model excl. SSB'); hold on;
title('Sitting Full Model excl. SSB');
boxplot([abs_error_SBP_sitting_full_model_no_ssb_rf, abs_error_DBP_sitting_full_model_no_ssb_rf],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_full_model_no_ssb_rf, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_full_model_no_ssb_rf, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_full_model_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_full_model_no_ssb_rf)])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_full_model_no_ssb_rf)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_full_model_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_full_model_no_ssb_rf), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_full_model_no_ssb_rf), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')

%sitting full GLM no SSB
fig_sitting_full_no_ssb_GLM = figure('Name','Sitting Full Model excl. SSB GLM'); hold on;
title('Sitting Full Model excl. SSB GLM');
boxplot([abs_error_SBP_sitting_full_model_no_ssb_glm, abs_error_DBP_sitting_full_model_no_ssb_glm],{'SBP','DBP'});
scatter(1, mean_abs_error_SBP_sitting_full_model_no_ssb_glm, 'r', 'fill');
scatter(2, mean_abs_error_DBP_sitting_full_model_no_ssb_glm, 'r', 'fill');
%MAD in plot
text(1+0.2, mean_abs_error_SBP_sitting_full_model_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_sitting_full_model_no_ssb_glm)])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_sitting_full_model_no_ssb_glm)])
% %of estimations below threshold
text(1+0.2, mean_abs_error_SBP_sitting_full_model_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_full_model_no_ssb_glm), '%'])
text(2+0.2, mean_abs_error_DBP_sitting_full_model_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_full_model_no_ssb_glm), '%'])
hline(7)
ylabel('Mean Absolute Deviation [mmHg]')
savefig([path_folder_current_subject '\..\outcome_validation\fig_performance_glm_sitting_full_model_no_ssb.fig']);


%STANDING

%     %standing loso
%     fig_standing_LOSO_SSB = figure('Name','Standing LOSO incl. SSB'); hold on;
%     title('Standing LOSO incl. SSB');
%     boxplot([abs_error_SBP_standing_rf, abs_error_DBP_standing_rf],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_rf, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_rf, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_rf)])
%     text(2+0.2, mean_abs_error_DBP_standing_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_rf)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_rf), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_rf), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')

%     %standing loso GLM
%     fig_standing_LOSO_SSB_GLM = figure('Name','Standing LOSO incl. SSB GLM'); hold on;
%     title('Standing LOSO incl. SSB GLM');
%     boxplot([abs_error_SBP_standing_glm, abs_error_DBP_standing_glm],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_glm, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_glm, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_glm)])
%     text(2+0.2, mean_abs_error_DBP_standing_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_glm)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_glm), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_glm), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')
%     %savefig('fig_performance_glm_standing_loso.fig')

%     %standing loso no ssb
%     fig_standing_LOSO_SSB_no_ssb = figure('Name','Standing LOSO excl. SSB'); hold on;
%     title('Standing LOSO excl. SSB');
%     boxplot([abs_error_SBP_standing_no_ssb_rf, abs_error_DBP_standing_no_ssb_rf],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_no_ssb_rf, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_no_ssb_rf, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_no_ssb_rf)])
%     text(2+0.2, mean_abs_error_DBP_standing_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_no_ssb_rf)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_no_ssb_rf), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_no_ssb_rf), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')

%     %standing loso GLM no ssb
%     fig_standing_LOSO_SSB_GLM_no_ssb = figure('Name','standing LOSO excl. SSB GLM'); hold on;
%     title('standing LOSO excl. SSB GLM');
%     boxplot([abs_error_SBP_standing_no_ssb_glm, abs_error_DBP_standing_no_ssb_glm],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_no_ssb_glm, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_no_ssb_glm, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_no_ssb_glm)])
%     text(2+0.2, mean_abs_error_DBP_standing_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_no_ssb_glm)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_no_ssb_glm), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_no_ssb_glm), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')
%     %savefig('fig_performance_glm_standing_loso_no_ssb.fig')

%     %standing full
%     fig_standing_full_SSB = figure('Name','Standing Full Model incl. SSB'); hold on;
%     title('Standing Full Model incl. SSB');
%     boxplot([abs_error_SBP_standing_full_model_rf, abs_error_DBP_standing_full_model_rf],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_full_model_rf, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_full_model_rf, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_full_model_rf)])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_full_model_rf)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_full_model_rf), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_full_model_rf), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')

%     %standing full GLM
%     fig_standing_full_SSB_GLM = figure('Name','standing Full Model incl. SSB GLM'); hold on;
%     title('standing Full Model incl. SSB GLM');
%     boxplot([abs_error_SBP_standing_full_model_glm, abs_error_DBP_standing_full_model_glm],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_full_model_glm, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_full_model_glm, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_full_model_glm)])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_full_model_glm)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_full_model_glm), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_full_model_glm), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')
%     %savefig('fig_performance_glm_standing_full_model.fig')

%     %standing full no ssb
%     fig_standing_full_SSB_no_ssb = figure('Name','Standing Full Model excl. SSB'); hold on;
%     title('Standing Full Model excl. SSB');
%     boxplot([abs_error_SBP_standing_full_model_no_ssb_rf, abs_error_DBP_standing_full_model_no_ssb_rf],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_full_model_no_ssb_rf, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_full_model_no_ssb_rf, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_full_model_no_ssb_rf)])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_no_ssb_rf, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_full_model_no_ssb_rf)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_full_model_no_ssb_rf), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_no_ssb_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_full_model_no_ssb_rf), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')

%     %standing full GLM no SSB
%     fig_standing_full_no_ssb_GLM = figure('Name','standing Full Model excl. SSB GLM'); hold on;
%     title('standing Full Model excl. SSB GLM');
%     boxplot([abs_error_SBP_standing_full_model_no_ssb_glm, abs_error_DBP_standing_full_model_no_ssb_glm],{'SBP','DBP'});
%     scatter(1, mean_abs_error_SBP_standing_full_model_no_ssb_glm, 'r', 'fill');
%     scatter(2, mean_abs_error_DBP_standing_full_model_no_ssb_glm, 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_SBP_standing_full_model_no_ssb_glm)])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_no_ssb_glm, ['mean MAD:', char(10), num2str(mean_abs_error_DBP_standing_full_model_no_ssb_glm)])
%     % %of estimations below threshold
%     text(1+0.2, mean_abs_error_SBP_standing_full_model_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_standing_full_model_no_ssb_glm), '%'])
%     text(2+0.2, mean_abs_error_DBP_standing_full_model_no_ssb_glm-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_standing_full_model_no_ssb_glm), '%'])
%     hline(7)
%     ylabel('Mean Absolute Deviation [mmHg]')
%     %savefig('fig_performance_glm_standing_full_model_no_ssb.fig')


%% Aggregate SSB and error matrix as overview

SSB_and_error_matrix_sitting = [SSB_matrix_sitting(:,1:11), ... %1:id, 2:age, 3:height, 4:weight, 5:SBPref, 6:DBPref, 7:PAT, 8:PATsd, 9:Spotcheck CI, 10:HR, 11:HRsd 
    error_SBP_sitting_rf, ... %12: error SBP loso random forest loso (leave-one-subject_out, i.e. respective subject not represented in model)
    error_DBP_sitting_rf, ... %13: error DBP loso random forest loso (leave-one-subject_out)
    outcome_matrix_sitting(:,8), ... %14: Confidence Interval SBP loso (random forest prediction outcome)
    outcome_matrix_sitting(:,9), ... %15: Confidence Interval DBP loso (random forest prediction outcome)
    error_SBP_sitting_full_model_rf, ... %16: error SBP random forest full model (respective subject represented in model)
    error_DBP_sitting_full_model_rf, ... %17: error DBP random forest full model (respective subject represented in model)
    outcome_matrix_sitting(:,10), ... %18: Confidence Interval SBP full model
    outcome_matrix_sitting(:,11), ... %19: Confidence Interval DBP full model
    SSB_matrix_sitting(:,12), ... %20: Armlength
    error_SBP_sitting_glm, ... %21: error SBP GLM loso
    error_DBP_sitting_glm, ... %22: error DBP GLM loso
    SBP_est_sitting_glm, ... %23: SBPest GLM loso
    DBP_est_sitting_glm, ... %24: DBPest GLM loso
    SBP_est_sitting_no_ssb_glm, ... %25: SBPest GLM without SSB loso
    DBP_est_sitting_no_ssb_glm, ... %26: DBPest GLM without SSB loso
    SBP_est_sitting_full_model_glm, ... %27: SBPest GLM full model
    DBP_est_sitting_full_model_glm, ... %28: DBPest GLM full model
    SBP_est_sitting_full_model_no_ssb_glm, ... %29: SBPest GLM without SSB full model
    DBP_est_sitting_full_model_no_ssb_glm, ... %30: DBPest GLM without SSB full model
    SSB_matrix_sitting(:,13), ... %31: upstroke gradient
    SSB_matrix_sitting(:,14), ... %32: primarypeak-foot amplitude
    SSB_matrix_sitting(:,15), ... %33: primarypeak-secondarypeak interval
    SSB_matrix_sitting(:,16), ... %34: risetime
    SSB_matrix_sitting(:,17), ... %35: decaytime
    SSB_matrix_sitting(:,18), ... %36: RCtime 
    SSB_matrix_sitting(:,19)]; %37: PPG CI raw

%     SSB_and_error_matrix_standing = [SSB_matrix_standing(:,1:11), ...
%         error_SBP_standing_rf, ...
%         error_DBP_standing_rf, ...
%         outcome_matrix_standing(:,8), ...
%         outcome_matrix_standing(:,9), ...
%         error_SBP_standing_full_model_rf, ...
%         error_DBP_standing_full_model_rf, ...
%         outcome_matrix_standing(:,10), ...
%         outcome_matrix_standing(:,11), ...
%         SSB_matrix_standing(:,12), ...
%         error_SBP_standing_glm, ...
%         error_DBP_standing_glm, ...
%         SBP_est_standing_glm, ...
%         DBP_est_standing_no_ssb_glm, ...
%         SBP_est_standing_glm, ...
%         DBP_est_standing_no_ssb_glm, ...
%         SBP_est_standing_full_model_glm, ...
%         DBP_est_standing_full_model_glm, ...
%         SBP_est_standing_full_model_no_ssb_glm, ...
%         DBP_est_standing_full_model_no_ssb_glm];


%% Plot SSB boxplots

SBP_classification_sitting_mean = SBP_classification_sitting; %before calculated as mean per subject
DBP_classification_sitting_mean = DBP_classification_sitting;

SBP_measured_sitting = SSB_and_error_matrix_sitting(:,5);
DBP_measured_sitting = SSB_and_error_matrix_sitting(:,6);

SBP_estimated_sitting_glm = SSB_and_error_matrix_sitting(:,23);
DBP_estimated_sitting_glm = SSB_and_error_matrix_sitting(:,24);

%     SBP_classification_standing_mean = SBP_classification_standing; %before calculated as mean per subject
%     DBP_classification_standing_mean = DBP_classification_standing;

%Parameters for plots
number_plots = 10;
number_rows = 3;
number_of_bins = 20;

%sitting

fig_sitting_SSB_overview = figure('Name', 'SSB overview sitting'); hold on;
subplot(number_rows,number_plots,1)
boxplot(SSB_matrix_sitting(:,2),{'age'})
subplot(number_rows,number_plots,2)
boxplot(SSB_matrix_sitting(:,3),{'height'})
subplot(number_rows,number_plots,3)
boxplot(SSB_matrix_sitting(:,4),{'weight'})
subplot(number_rows,number_plots,4)
boxplot(SSB_matrix_sitting(:,5),{'sbp'})
subplot(number_rows,number_plots,5)
boxplot(SSB_matrix_sitting(:,6),{'dbp'})
subplot(number_rows,number_plots,6)
boxplot(SSB_matrix_sitting(:,7),{'pat'})
subplot(number_rows,number_plots,7)
boxplot(SSB_matrix_sitting(:,8),{'pat sd'})
subplot(number_rows,number_plots,8)
boxplot(SSB_matrix_sitting(:,9),{'pat CI'})
subplot(number_rows,number_plots,9)
boxplot(SSB_matrix_sitting(:,10),{'hr'})
subplot(number_rows,number_plots,10)
boxplot(SSB_matrix_sitting(:,11),{'hr sd'})
suptitle('SSB overview sitting');

%Measured BP
subplot(number_rows,number_plots,[11:15]), hold on
hist(SBP_measured_sitting, number_of_bins)
xlabel('SBP')
ylabel('Frequency')
title('Histogram SBP_{meas}')
subplot(number_rows,number_plots,[16:20]), hold on
hist(DBP_measured_sitting, number_of_bins)
xlabel('DBP')
title('Histogram DBP_{meas}')

%Estimated BP GLM
subplot(number_rows,number_plots,[21:25]), hold on
hist(SBP_estimated_sitting_glm, number_of_bins)
xlabel('SBP')
ylabel('Frequency')
title('Histogram SBP_{est}')
subplot(number_rows,number_plots,[26:30]), hold on
hist(DBP_estimated_sitting_glm, number_of_bins)
xlabel('DBP')
title('Histogram DBP_{est}')

%safe histogram figure
savefig([path_folder_current_subject '\..\outcome_validation\fig_histogram_sitting.fig']);

%     %standing
% 
%     fig_standing_SSB_overview = figure('Name', 'SSB overview standing'); hold on;
%     subplot(number_rows,number_plots,1)
%     boxplot(SSB_matrix_standing(:,2),{'age'})
%     subplot(number_rows,number_plots,2)
%     boxplot(SSB_matrix_standing(:,3),{'height'})
%     subplot(number_rows,number_plots,3)
%     boxplot(SSB_matrix_standing(:,4),{'weight'})
%     subplot(number_rows,number_plots,4)
%     boxplot(SSB_matrix_standing(:,5),{'sbp'})
%     subplot(number_rows,number_plots,5)
%     boxplot(SSB_matrix_standing(:,6),{'dbp'})
%     subplot(number_rows,number_plots,6)
%     boxplot(SSB_matrix_standing(:,7),{'pat'})
%     subplot(number_rows,number_plots,7)
%     boxplot(SSB_matrix_standing(:,8),{'pat sd'})
%     subplot(number_rows,number_plots,8)
%     boxplot(SSB_matrix_standing(:,9),{'pat CI'})
%     subplot(number_rows,number_plots,9)
%     boxplot(SSB_matrix_standing(:,10),{'hr'})
%     subplot(number_rows,number_plots,10)
%     boxplot(SSB_matrix_standing(:,11),{'hr sd'})
%     suptitle('SSB overview standing');

% subplot(number_rows,number_plots,[11:15]), hold on
% hist(SBP_classification_sitting(:,2), number_of_bins)
% xlabel('SBP')
% ylabel('Frequency')
% title('Histogram SBP')
% subplot(number_rows,number_plots,[16:20]), hold on
% hist(DBP_classification_sitting(:,2), number_of_bins)
% xlabel('DBP')
% title('Histogram DBP')




%% PPG channel and estimation error
% channel_matrix_sitting = [channel_vector_sitting, num2str(abs_error_SBP_sitting_rf), num2str(abs_error_DBP_sitting_rf)];
% channel_matrix_standing = [channel_vector_standing, num2str(abs_error_SBP_standing_rf), num2str(abs_error_DBP_standing_rf)];

%% Histogram and mean PATs per channel

%     %convert channel character to number:
%     %channel_vector_sitting
%     for i = 1:length(channel_vector_sitting)
%         if strcmp(channel_vector_sitting(i), 'a')
%             channel_vector_sitting(i) = '0';
%         elseif strcmp(channel_vector_sitting(i), 'b')
%             channel_vector_sitting(i) = '1';
%         elseif strcmp(channel_vector_sitting(i), 'c')
%             channel_vector_sitting(i) = '2';
%         elseif strcmp(channel_vector_sitting(i), 'd')
%             channel_vector_sitting(i) = '3';
%         elseif strcmp(channel_vector_sitting(i), 'e')
%             channel_vector_sitting(i) = '4';        
%         elseif strcmp(channel_vector_sitting(i), 'f')
%             channel_vector_sitting(i) = '5';
%         elseif strcmp(channel_vector_sitting(i), 'g')
%             channel_vector_sitting(i) = '6';
%         elseif strcmp(channel_vector_sitting(i), 'h')
%             channel_vector_sitting(i) = '7';
%         end
%     end
% 
%     %channel_vector_standing
%     for i = 1:length(channel_vector_standing)
%         if strcmp(channel_vector_standing(i), 'a')
%             channel_vector_standing(i) = '0';
%         elseif strcmp(channel_vector_standing(i), 'b')
%             channel_vector_standing(i) = '1';
%         elseif strcmp(channel_vector_standing(i), 'c')
%             channel_vector_standing(i) = '2';
%         elseif strcmp(channel_vector_standing(i), 'd')
%             channel_vector_standing(i) = '3';
%         elseif strcmp(channel_vector_standing(i), 'e')
%             channel_vector_standing(i) = '4';        
%         elseif strcmp(channel_vector_standing(i), 'f')
%             channel_vector_standing(i) = '5';
%         elseif strcmp(channel_vector_standing(i), 'g')
%             channel_vector_standing(i) = '6';
%         elseif strcmp(channel_vector_standing(i), 'h')
%             channel_vector_standing(i) = '7';
%         end
%     end
% 
%     fig_hist_PPG_selection = figure('Name', 'Histogram PPG selection'); hold on;
%     %sitting
%     subplot(1,2,1), hold on
%     hist(str2num(channel_vector_sitting),0:1:7)
%     axis([0,7,0,40])
%     xlabel('Channel')
%     ylabel('Frequency')
%     title('Sitting')
%     %standing
%     subplot(1,2,2), hold on
%     hist(str2num(channel_vector_standing),0:1:7)
%     axis([0,7,0,40])
%     xlabel('Channel')
%     ylabel('Frequency')
%     title('Standing')


%% PAT comparison inter and intra-wavelength

% %only include valid green signals (comment to include all subjects)
% mean_PAT_sitting([1:3, 11:17, 32:37, 52:54, 64:66, 69],:) = [];
% 
% %inter wavelength
% green_red_difference = mean(mean_PAT_sitting(:,[2 5 6 8]),2) - mean(mean_PAT_sitting(:,[3 4 7]),2); %green - red
% mean_green_red_difference = mean(green_red_difference); 
% 
% green_blue_difference = mean(mean_PAT_sitting(:,[2 5 6 8]),2) - mean_PAT_sitting(:,1); %green - blue
% mean_green_blue_difference = mean(green_blue_difference);
% 
% green_1_4_difference = mean_PAT_sitting(:,2) - mean_PAT_sitting(:,5); %green_1 - green_4
% mean_green_1_4_difference = mean(green_1_4_difference);
% 
% green_4_5_difference = mean_PAT_sitting(:,5) - mean_PAT_sitting(:,6); %green_4 - green_5
% mean_green_4_5_difference = mean(green_4_5_difference);
% 
% green_4_7_difference = mean_PAT_sitting(:,5) - mean_PAT_sitting(:,8); %green_4 - green_7
% mean_green_4_7_difference = mean(green_4_7_difference);
% 
% green_1_7_difference = mean_PAT_sitting(:,2) - mean_PAT_sitting(:,8); %green_1 - green_7
% mean_green_1_7_difference = mean(green_1_7_difference);
% 
% fig_PPG_differences = figure('Name', 'Histogram PPG PAT differences'); hold on;
% subplot(3,2,1), hold on
% hist(green_red_difference) 
% xlabel('Diff PAT [ms]')
% ylabel('Freq')
% title('Diff Green-Red')
% 
% subplot(3,2,2), hold on
% hist(green_blue_difference) 
% xlabel('Diff PAT [ms]')
% ylabel('Freq')
% title('Diff Green-Blue')
% 
% subplot(3,2,3), hold on
% hist(green_1_4_difference) 
% xlabel('Diff PAT [ms]')
% ylabel('Freq')
% title('Diff Green ch1/b - Green ch4/e')
% 
% subplot(3,2,4), hold on
% hist(green_4_5_difference) 
% xlabel('Diff PAT [ms]')
% ylabel('Freq')
% title('Diff Green ch4/e - Green ch5/f')
% 
% %inner ulnar-central
% subplot(3,2,5), hold on
% hist(green_4_7_difference) 
% xlabel('Diff PAT [ms]')
% ylabel('Freq')
% title('Diff Green ch4/e - Green ch7/h')
% 
% %outer ulnar-central
% subplot(3,2,6), hold on
% hist(green_1_7_difference) 
% xlabel('Diff PAT [ms]')
% ylabel('Freq')
% title('Diff Green ch1/b - Green ch7/h')

%% CI_PAT overview
fig_CI_PAT = figure('Name', 'PAT CI metric overview sitting'); hold on;
%SBP sitting
    subplot(2,2,1), hold on
    scatter(SSB_and_error_matrix_sitting(:,12), SSB_and_error_matrix_sitting(:,9), 'r', 'fill')
    title('SBP sitting')
    xlabel('\epsilon BP_{ref}-BP_{est}')
    ylabel('CI PAT')
%DBP sitting
    subplot(2,2,2), hold on
    scatter(SSB_and_error_matrix_sitting(:,13), SSB_and_error_matrix_sitting(:,9), 'r', 'fill')    
    title('DBP sitting')
    xlabel('\epsilon BP_{ref}-BP_{est}')
    ylabel('CI PAT')
%     %SBP standing
%         subplot(2,2,3), hold on
%         scatter(SSB_and_error_matrix_standing(:,12), SSB_and_error_matrix_standing(:,9), 'r', 'fill')
%         title('SBP standing')
%         xlabel('\epsilon BP_{ref}-BP_{est}')
%         ylabel('CI PAT')
%     %DBP standing
%         subplot(2,2,4), hold on
%         scatter(SSB_and_error_matrix_standing(:,13), SSB_and_error_matrix_standing(:,9), 'r', 'fill')
%         title('DBP standing')
%         xlabel('\epsilon BP_{ref}-BP_{est}')
%         ylabel('CI PAT')

%% SD_Trees overview (quasi BP CI)
fig_SD_Trees = figure('Name', 'SD Trees'); hold on;
%SBP sitting
    subplot(2,2,1), hold on
    scatter(SSB_and_error_matrix_sitting(:,12), SSB_and_error_matrix_sitting(:,14), 'r', 'fill')
    title('SBP sitting')
    xlabel('\epsilon BP_{ref}-BP_{est}')
    ylabel('SD of tree responses')
%DBP sitting
    subplot(2,2,2), hold on
    scatter(SSB_and_error_matrix_sitting(:,13), SSB_and_error_matrix_sitting(:,15), 'r', 'fill')    
    title('DBP sitting')
    xlabel('\epsilon BP_{ref}-BP_{est}')
    ylabel('SD of tree responses')
%     %SBP standing
%         subplot(2,2,3), hold on
%         scatter(SSB_and_error_matrix_standing(:,12), SSB_and_error_matrix_standing(:,14), 'r', 'fill')
%         title('SBP standing')
%         xlabel('\epsilon BP_{ref}-BP_{est}')
%         ylabel('SD of tree responses')
%     %DBP standing
%         subplot(2,2,4), hold on
%         scatter(SSB_and_error_matrix_standing(:,13), SSB_and_error_matrix_standing(:,15), 'r', 'fill')
%         title('DBP standing')
%         xlabel('\epsilon BP_{ref}-BP_{est}')
%         ylabel('SD of tree responses')
    
%% BP_CI overview

%modify CI BP vectors to range from 1 to 5 
CI_BP_vector_sitting_new = [CI_BP_vector_sitting(:,1) abs((CI_BP_vector_sitting(:,2:end).*4)-5)];
% CI_BP_vector_standing_new = [CI_BP_vector_standing(:,1) abs((CI_BP_vector_standing(:,2:end).*4)-5)];

color_scheme=jet(8);
fig_BP_CI = figure('Name', 'BP CI metric overview sitting'); hold on;
%SBP sitting
    subplot(2,2,1), hold on
    for i=2:8
        scatter(SSB_and_error_matrix_sitting(:,12), CI_BP_vector_sitting_new(:,i), 30, color_scheme(i,:), 'd')
    end
    %mean of SSBs and continuous measures
    scatter(SSB_and_error_matrix_sitting(:,12), CI_BP_vector_sitting_new(:,9), 'r', 'fill')
    %mean of SSBs only
    scatter(SSB_and_error_matrix_sitting(:,12), CI_BP_vector_sitting_new(:,10), 'b', 'fill')
    title('SBP sitting')
    xlabel('\epsilon BP_{ref}-BP_{est}')
    ylabel('BP CI (Distance to median)')
%DBP sitting
    subplot(2,2,2), hold on
    for i=2:8
        scatter(SSB_and_error_matrix_sitting(:,13), CI_BP_vector_sitting_new(:,i), 30, color_scheme(i,:),  'd')
    end
    %mean of SSBs and continuous measures
    scatter(SSB_and_error_matrix_sitting(:,13), CI_BP_vector_sitting_new(:,9), 'r', 'fill')   
    %mean of SSBs only
    scatter(SSB_and_error_matrix_sitting(:,13), CI_BP_vector_sitting_new(:,10), 'b', 'fill')
    title('DBP sitting')
    xlabel('\epsilon BP_{ref}-BP_{est}')
    ylabel('BP CI (Distance to median)')
    legend('age', 'height', 'weight', 'pat', 'patSD', 'HR', 'HRSD', 'Avg SSB & Cont', 'Avg SSB only') 
%     %SBP standing
%         subplot(2,2,3), hold on
%         for i=2:8
%             scatter(SSB_and_error_matrix_standing(:,12), CI_BP_vector_standing_new(:,i), 30, color_scheme(i,:), 'd')
%         end
%         %mean of SSBs and continuous measures
%         scatter(SSB_and_error_matrix_standing(:,12), CI_BP_vector_standing_new(:,9), 'r', 'fill')
%         %mean of SSBs only
%         scatter(SSB_and_error_matrix_standing(:,12), CI_BP_vector_standing_new(:,10), 'b', 'fill')    
%         title('SBP standing')
%         xlabel('\epsilon BP_{ref}-BP_{est}')
%         ylabel('BP CI (Distance to median)')
%     %DBP standing
%         subplot(2,2,4), hold on
%         for i=2:8
%             scatter(SSB_and_error_matrix_standing(:,13), CI_BP_vector_standing_new(:,i), 30, color_scheme(i,:), 'd')
%         end
%         %mean of SSBs and continuous measures
%         scatter(SSB_and_error_matrix_standing(:,13), CI_BP_vector_standing_new(:,9), 'ro', 'fill')
%         %mean of SSBs only
%         scatter(SSB_and_error_matrix_standing(:,13), CI_BP_vector_standing_new(:,10), 'b', 'fill')        
%         title('DBP standing')
%         xlabel('\epsilon BP_{ref}-BP_{est}')
%         ylabel('BP CI (Distance to median)')
    
    
%% Scatter plot PAT, PWV and HR vs SBP

%prompt user to decide on plotting
gen_save_plots = input('Generate and save detailed scatter plots? <y/n>: ', 's');
if strcmp(gen_save_plots, 'y')

    %predefine figures 
    
    %SBP
    
        %PAT
        fig_scatter_PAT_SBP = figure('Name','PAT-SBP');
        xlabel('PAT [s]')
        ylabel('SBP [mmHg]')
        title('PAT-SBP subject')
        
        %PAT/gradient
        fig_scatter_PATgrad_SBP = figure('Name','PATgrad-SBP');
        xlabel('PATgrad [s^2/mmHg]')
        ylabel('SBP [mmHg]')
        title('PATgrad-SBP subject')        
        
        %PATsd
        fig_scatter_PATsd_SBP = figure('Name','PATsd-SBP');
        xlabel('PATsd [s]')
        ylabel('SBP [mmHg]')
        title('PATsd-SBP subject')        

        %PWV
        fig_scatter_PWV_SBP = figure('Name','PWV-SBP');
        xlabel('PWV [m/s]')
        ylabel('SBP [mmHg]')
        title('PWV-SBP subject')    

        %PWVgrad
        fig_scatter_PWVgrad_SBP = figure('Name','PWVgrad-SBP');
        xlabel('PWVgrad [m/mmHg]')
        ylabel('SBP [mmHg]')
        title('PWVgrad-SBP subject')         
        
        %PWV armlength
        fig_scatter_PWV_armlength_SBP = figure('Name','PWV armlength-SBP');
        xlabel('PWV via armlength [m/s]')
        ylabel('SBP [mmHg]')
        title('PWV_{armlength}-SBP subject')        

        %HR
        fig_scatter_HR_SBP = figure('Name','HR-SBP');
        xlabel('HR [bpm]')
        ylabel('SBP [mmHg]')
        title('HR-SBP subject')  
        
        %HRsd
        fig_scatter_HRsd_SBP = figure('Name','HRsd-SBP');
        xlabel('HRsd [bpm]')
        ylabel('SBP [mmHg]')
        title('HRsd-SBP subject')        

        %age
        fig_scatter_age_SBP = figure('Name','age-SBP');
        xlabel('age [years]')
        ylabel('SBP [mmHg]')
        title('age-SBP subject') 

        %height
        fig_scatter_height_SBP = figure('Name','height-SBP');
        xlabel('height [cm]')
        ylabel('SBP [mmHg]')
        title('height-SBP subject')  

        %weight
        fig_scatter_weight_SBP = figure('Name','weight-SBP');
        xlabel('weight [kg]')
        ylabel('SBP [mmHg]')
        title('weight-SBP subject')      

        %BMI
        fig_scatter_BMI_SBP = figure('Name','BMI-SBP');
        xlabel('BMI [kg/m^2]')
        ylabel('SBP [mmHg]')
        title('BMI-SBP subject') 
        
        %upstroke gradient
        fig_scatter_upstgrad_SBP = figure('Name','upstgrad-SBP');
        xlabel('upstgrad [a.u.]')
        ylabel('SBP [mmHg]')
        title('upstgrad-SBP subject') 
        
        %pp to ft amplitude
        fig_scatter_pp_ft_amp_SBP = figure('Name','pp_ft_amp-SBP');
        xlabel('pp_ft_amp [a.u.]')
        ylabel('SBP [mmHg]')
        title('pp_ft_amp-SBP subject')  
        
        %RCtime
        fig_scatter_RCtime_SBP = figure('Name','RCtime-SBP');
        xlabel('RCtime [a.u.]')
        ylabel('SBP [mmHg]')
        title('RCtime-SBP subject')         
        
    %DBP
    
        %PAT
        fig_scatter_PAT_DBP = figure('Name','PAT-DBP');
        xlabel('PAT [s]')
        ylabel('DBP [mmHg]')
        title('PAT-DBP subject')

        %PWV
        fig_scatter_PWV_DBP = figure('Name','PWV-DBP');
        xlabel('PWV [m/s]')
        ylabel('DBP [mmHg]')
        title('PWV-DBP subject')    

        %PWV armlength
        fig_scatter_PWV_armlength_DBP = figure('Name','PWV armlength-DBP');
        xlabel('PWV via armlength [m/s]')
        ylabel('DBP [mmHg]')
        title('PWV_{armlength}-DBP subject')        

        %HR
        fig_scatter_HR_DBP = figure('Name','HR-DBP');
        xlabel('HR [bpm]')
        ylabel('DBP [mmHg]')
        title('HR-DBP subject')    

        %age
        fig_scatter_age_DBP = figure('Name','age-DBP');
        xlabel('age [years]')
        ylabel('DBP [mmHg]')
        title('age-DBP subject') 

        %height
        fig_scatter_height_DBP = figure('Name','height-DBP');
        xlabel('height [cm]')
        ylabel('DBP [mmHg]')
        title('height-DBP subject')  

        %weight
        fig_scatter_weight_DBP = figure('Name','weight-DBP');
        xlabel('weight [kg]')
        ylabel('DBP [mmHg]')
        title('weight-DBP subject')      

        %BMI
        fig_scatter_BMI_DBP = figure('Name','BMI-DBP');
        xlabel('BMI [kg/m^2]')
        ylabel('DBP [mmHg]')
        title('BMI-DBP subject')         
    
    %legend entries
    legend_entries = [];
    matrix = SSB_and_error_matrix_sitting;
    %color change
    color_scheme=jet(length(unique(matrix(:,1))));
    j=1; %index to access color scheme
    
    %loop for SBP plots
    for i = unique(matrix(:,1))' %orig 3, 9, 23, 25, 26....[1:30]
        
        disp(['Plotting Subject', num2str(i)])

        matrix_subject = matrix(matrix(:,1) == i,:);   

        armlength = matrix_subject(:,20);          
        height = matrix_subject(:,3);        
        PAT = matrix_subject(:,7);
        PATsd = matrix_subject(:,8);
        PWV = height./PAT;
        PWV_armlength = armlength./PAT;
        SBP = matrix_subject(:,5);
        DBP = matrix_subject(:,6);
        HR = matrix_subject(:,10);
        age = matrix_subject(:,2);
        weight = matrix_subject(:,4); 
        BMI = weight./(height./100).^2;
        HRsd = matrix_subject(:,11);
        risetime = matrix_subject(:,34);
        upstgrad = matrix_subject(:,31);
        pp_ft_amp = matrix_subject(:,32);
        RCtime = matrix_subject(:,36);
        PATgrad = PAT./upstgrad;
        PWVgrad = PWV./upstgrad;

        %define legend entries for figure
        
        %define current legend entry
        if i < 10
            current_entry = ['SP000',num2str(i)];
        elseif (i >= 10) && (i < 100)
            current_entry = ['SP00',num2str(i)];
        elseif (i >= 100) && (i < 1000)
            current_entry = ['SP0',num2str(i)];
        elseif i >= 1000
            current_entry = ['SP',num2str(i)];
        end
              
            %add legend entry
            legend_entries = [legend_entries; current_entry]; 

        %plot PAT
        figure(fig_scatter_PAT_SBP); hold on;

            scatter(PAT, SBP, 50, color_scheme(j,:), 'fill')
            text(PAT-.005, SBP-.005, num2str(i))
            for k=1:length(PAT)
                text(PAT(k)+.001, SBP(k)+.001, num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');
        %axis([.15 .45 90 190])
        
        %plot PATgrad
        figure(fig_scatter_PATgrad_SBP); hold on;

            scatter(PATgrad, SBP, 50, color_scheme(j,:), 'fill')
            text(PATgrad-.000005, SBP-.005, num2str(i))
            for k=1:length(PATgrad)
                text(PATgrad(k)+.000001, SBP(k)+.001, num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');
        %axis([.15 .45 90 190])        
        
        %plot PATsd
        figure(fig_scatter_PATsd_SBP); hold on;

            scatter(PATsd, SBP, 50, color_scheme(j,:), 'fill')
            text(PATsd-.005, SBP-.005, num2str(i))
            for k=1:length(PATsd)
                text(PATsd(k)+.001, SBP(k)+.001, num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');       
        
        %plot PWV
        figure(fig_scatter_PWV_SBP); hold on;
  
            scatter(PWV, SBP, 50, color_scheme(j,:), 'fill')
            text(PWV-.005, SBP-.005, num2str(i))
            for k=1:length(PWV)
                text(PWV(k)+.001, SBP(k)+.001, num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');  
            
        %plot PWVgrad
        figure(fig_scatter_PWVgrad_SBP); hold on;
  
            scatter(PWVgrad, SBP, 50, color_scheme(j,:), 'fill')
            text(PWVgrad-.005, SBP-.005, num2str(i))
            for k=1:length(PWVgrad)
                text(PWVgrad(k)+.001, SBP(k)+.001, num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');              
            
        %plot PWV via armlength
        figure(fig_scatter_PWV_armlength_SBP); hold on;
  
            scatter(PWV_armlength, SBP, 50, color_scheme(j,:), 'fill')
            text(PWV_armlength-.005, SBP-.005, num2str(i))
            for k=1:length(PWV_armlength)
                text(PWV_armlength(k)+.001, SBP(k)+.001, num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');              
        
        %plot HR
        figure(fig_scatter_HR_SBP); hold on;

            scatter(HR, SBP, 50, color_scheme(j,:), 'fill')
            text(HR-.75, SBP, num2str(i))
            for k=1:length(HR)
                text(HR(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast'); 
            
        %plot HRsd
        figure(fig_scatter_HRsd_SBP); hold on;

            scatter(HRsd, SBP, 50, color_scheme(j,:), 'fill')
            text(HRsd-.75, SBP, num2str(i))
            for k=1:length(HRsd)
                text(HRsd(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');             
            
        %plot age
        figure(fig_scatter_age_SBP); hold on;

            scatter(age, SBP, 50, color_scheme(j,:), 'fill')
            text(age-.75, SBP, num2str(i))
            for k=1:length(age)
                text(age(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');    
            
        %plot height
        figure(fig_scatter_height_SBP); hold on;

            scatter(height, SBP, 50, color_scheme(j,:), 'fill')
            text(height-.75, SBP, num2str(i))
            for k=1:length(height)
                text(height(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');   
            
        %plot weight
        figure(fig_scatter_weight_SBP); hold on;

            scatter(weight, SBP, 50, color_scheme(j,:), 'fill')
            text(weight-.75, SBP, num2str(i))
            for k=1:length(weight)
                text(weight(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');    
            
        %plot BMI
        figure(fig_scatter_BMI_SBP); hold on;

            scatter(BMI, SBP, 50, color_scheme(j,:), 'fill')
            text(BMI-.75, SBP, num2str(i))
            for k=1:length(BMI)
                text(BMI(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');       
            
        %plot upstgrad
        figure(fig_scatter_upstgrad_SBP); hold on;

            scatter(upstgrad, SBP, 50, color_scheme(j,:), 'fill')
            text(upstgrad-.75, SBP, num2str(i))
            for k=1:length(BMI)
                text(upstgrad(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast'); 
            
        %plot pp_ft_amp
        figure(fig_scatter_pp_ft_amp_SBP); hold on;

            scatter(pp_ft_amp, SBP, 50, color_scheme(j,:), 'fill')
            text(pp_ft_amp-.75, SBP, num2str(i))
            for k=1:length(BMI)
                text(pp_ft_amp(k)+.75, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');        
            
        %plot RCtime
        figure(fig_scatter_RCtime_SBP); hold on;

            scatter(RCtime, SBP, 50, color_scheme(j,:), 'fill')
            text(RCtime-.005, SBP, num2str(i))
            for k=1:length(BMI)
                text(RCtime(k)+.005, SBP(k), num2str(k))
            end
            %legend(legend_entries, 'Location', 'NorthEast');              
            
        j=j+1;
    end    
    
    % add all data to scatterplot for simple fitting
    matrix_all = SSB_and_error_matrix_sitting;
    
        armlength = matrix_all(:,20);          
        height = matrix_all(:,3);        
        PAT = matrix_all(:,7);
        PWV = height./PAT;
        PWV_armlength = armlength./PAT;
        SBP = matrix_all(:,5);
        DBP = matrix_all(:,6);
        HR = matrix_all(:,10);
        age = matrix_all(:,2);
        weight = matrix_all(:,4); 
        BMI = weight./(height./100).^2;    
        risetime = matrix_all(:,34);
        upstgrad = matrix_all(:,31);
        pp_ft_amp = matrix_all(:,32);
        RCtime = matrix_all(:,36);    
   
    %SBP
        
        %plot PAT
        figure(fig_scatter_PAT_SBP); hold on;
            scatter(PAT, SBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_PAT_SBP.fig'])
            
        %plot PWV
        figure(fig_scatter_PWV_SBP); hold on;
            scatter(PWV, SBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_PWV_SBP.fig'])
            
        %plot PWV via armlength
        figure(fig_scatter_PWV_armlength_SBP); hold on;
            scatter(PWV_armlength, SBP, 50, 'k')  
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_PWV_armlength_SBP.fig'])            
        
        %plot HR
        figure(fig_scatter_HR_SBP); hold on;
            scatter(HR, SBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_HR_SBP.fig'])            
            
        %plot age
        figure(fig_scatter_age_SBP); hold on;
            scatter(age, SBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_age_SBP.fig'])              
            
        %plot height
        figure(fig_scatter_height_SBP); hold on;
            scatter(height, SBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_height_SBP.fig'])              
            
        %plot weight
        figure(fig_scatter_weight_SBP); hold on;
            scatter(weight, SBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_weight_SBP.fig'])              
            
        %plot BMI
        figure(fig_scatter_BMI_SBP); hold on;
            scatter(BMI, SBP, 50, 'k')   
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_BMI_SBP.fig'])     
        
        %plot upstgrad
        figure(fig_scatter_upstgrad_SBP); hold on;
            scatter(upstgrad, SBP, 50, 'k')   
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_upstgrad_SBP.fig'])        
            
        %plot pp_ft_amp
        figure(fig_scatter_pp_ft_amp_SBP); hold on;
            scatter(pp_ft_amp, SBP, 50, 'k')   
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_pp_ft_amp_SBP.fig'])                
        
        %plot RCtime
        figure(fig_scatter_RCtime_SBP); hold on;
            scatter(RCtime, SBP, 50, 'k')   
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_RCtime_SBP.fig'])         
        
    %DBP
        
        %plot PAT
        figure(fig_scatter_PAT_DBP); hold on;
            scatter(PAT, DBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_PAT_DBP.fig'])
            
        %plot PWV
        figure(fig_scatter_PWV_DBP); hold on;
            scatter(PWV, DBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_PWV_DBP.fig'])
            
        %plot PWV via armlength
        figure(fig_scatter_PWV_armlength_DBP); hold on;
            scatter(PWV_armlength, DBP, 50, 'k')  
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_PWV_armlength_DBP.fig'])            
        
        %plot HR
        figure(fig_scatter_HR_DBP); hold on;
            scatter(HR, DBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_HR_DBP.fig'])            
            
        %plot age
        figure(fig_scatter_age_DBP); hold on;
            scatter(age, DBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_age_DBP.fig'])              
            
        %plot height
        figure(fig_scatter_height_DBP); hold on;
            scatter(height, DBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_height_DBP.fig'])              
            
        %plot weight
        figure(fig_scatter_weight_DBP); hold on;
            scatter(weight, DBP, 50, 'k')
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_weight_DBP.fig'])              
            
        %plot BMI
        figure(fig_scatter_BMI_DBP); hold on;
            scatter(BMI, DBP, 50, 'k')   
        savefig([path_folder_current_subject '\..\outcome_validation\fig_scatter_BMI_DBP.fig']) 
end        
     
%plot for all features and BP outcomes in single figure

fig_scatterplots_all_parameters = figure('Name','Scatterplot all Parameters'); clf; hold on;
rows = 2;
columns = 9;

    %PWV-SBP
    subplot(rows, columns,1), scatter(PWV(:), SBP(:))
    xlabel('\Delta PWV'),ylabel('\Delta SBP')
    %PWV-DBP
    subplot(rows, columns,10), scatter(PWV(:), DBP(:))
    xlabel('\Delta PWV'),ylabel('\Delta DBP')

    %PAT-SBP
    subplot(rows, columns,2), scatter(PAT(:), SBP(:))
    xlabel('\Delta PAT'),ylabel('\Delta SBP')
    %PAT-DBP
    subplot(rows, columns,11), scatter(PAT(:), DBP(:))
    xlabel('\Delta PAT'),ylabel('\Delta DBP')

    %HR-SBP
    subplot(rows, columns,3), scatter(HR(:), SBP(:))
    xlabel('\Delta HR'),ylabel('\Delta SBP')
    %HR-DBP
    subplot(rows, columns,12), scatter(HR(:), DBP(:))
    xlabel('\Delta HR'),ylabel('\Delta DBP')
    
    %risetime-SBP
    subplot(rows, columns, 4), scatter(risetime(:), SBP(:))
    xlabel('\Delta risetime'),ylabel('SBP')
    %risetime-DBP
    subplot(rows, columns, 13), scatter(risetime(:), DBP(:))
    xlabel('\Delta risetime'),ylabel('DBP')   
    
    %risetime/PAT-SBP
    subplot(rows, columns, 5), scatter(PAT(:) ./ risetime(:), SBP(:))
    xlabel('\Delta PAT / \Delta risetime'),ylabel('SBP')
    %risetime/PAT-DBP
    subplot(rows, columns, 14), scatter(PAT(:) ./ risetime(:), DBP(:))
    xlabel('\Delta PAT / \Delta risetime'),ylabel('DBP')     

    %risetime-HR
    subplot(rows, columns,6), scatter(risetime(:), HR(:))
    xlabel('\Delta risetime'),ylabel('\Delta HR')
    %PAT-HR
    subplot(rows, columns,15), scatter(PAT(:), HR(:))
    xlabel('\Delta PAT'),ylabel('\Delta HR')
    
    %pp_ft_amp-SBP
    subplot(rows, columns,7), scatter(pp_ft_amp(:), SBP(:))
    xlabel('\Delta pp-ft amp'),ylabel('\Delta SBP')
    %pp_ft_amp-SBP
    subplot(rows, columns,16), scatter(pp_ft_amp(:), DBP(:))
    xlabel('\Delta pp-ft amp'),ylabel('\Delta DBP')    
    
    %upstgrad-SBP
    subplot(rows, columns,8), scatter(upstgrad(:), SBP(:))
    xlabel('\Delta upstgrad'),ylabel('\Delta SBP')
    %upstgrad-SBP
    subplot(rows, columns,17), scatter(upstgrad(:), DBP(:))
    xlabel('\Delta upstgrad'),ylabel('\Delta DBP')  
    
    %RCtime-SBP
    subplot(rows, columns,9), scatter(RCtime(:), SBP(:))
    xlabel('\Delta RCtime'),ylabel('\Delta SBP')
    %RCtime-SBP
    subplot(rows, columns,18), scatter(RCtime(:), DBP(:))
    xlabel('\Delta RCtime'),ylabel('\Delta DBP')     

    suptitle('Predictors scatterplot')
    %save figure 
    savefig([path_folder_current_subject '\..\outcome_validation\predictors_scatter.fig']) 

%% Save SSB and error matrices for further analysis/processing

save([path_folder_current_subject '\..\outcome_validation\SSB_and_error_matrix_sitting.mat'], 'SSB_and_error_matrix_sitting');
%save([path_folder_current_subject '\..\outcome_validation\SSB_and_error_matrix_standing.mat'], 'SSB_and_error_matrix_standing');
