function [data] = import_file(subject, experiment, foldername, file, spreadsheet_data)

 %check for existing .mat file --> csv does not need to be imported / will not be available when SAMI is there
        if  (0) %(0) in order to start new import; alternative: (1) to copy folder with original files
            
            exist([data_folder '\' foldername '\', file, '.mat']) 
            disp ([file ': .mat file exists, no .csv will be imported'])
            load([data_folder '\' foldername '\', file, '.mat']);
            
        elseif (~exist([[foldername,'/',file], '.csv']) && (str2num(subject) <= 32)) || ( ...
                    ( ~exist([[foldername,'/',file], '.mat']) && ( ((str2num(subject) > 32) && (str2num(subject) < 40)) || ((str2num(subject) > 32) && (str2num(subject) > 45) && (str2num(subject) < 85)) )) || ...
                    ( ~exist([[foldername,'/',file], '.csv']) && ( (str2num(subject) >= 40) && (str2num(subject) <= 45)) ) ... 
                ) || ...
                ( ~exist([[foldername,'/',file], '.csv']) && ( (str2num(subject) >= 85) && (str2num(subject) <= 106)) ) || ...
                ( ~exist([[foldername,'/',file], '.mat']) && (str2num(subject) > 106) )
            
            
            %IF no .csv exists for <= 32 OR 
                % no .mat for (>32 and <40) OR (>32 and >45 and <85) OR
                % no .csv for (40-45) 
            %OR no .csv exists for 85-106
            %OR no .mat exists for >106 (new large scale data collection!)
            
            
            %for 33-85 (excl 40-45, and here only for sitting) no csv exist; in others neither file exists but subject should be represented in overview and archived anyways
            
            disp ([file ': Missing Experiment Data'])
            
            %% Get spreadsheet data even though recordings are missing
            %such that SSB, ref BP, etc is available before executing BP Algo
            
            %Although some files are available as .mat file already, their ssb are not correct!!! (e.g. armlength 0 instead of NaN in spreadsheet)
            data.ssb.eth.signal = spreadsheet_data.ssb_spreadsheet_txtdata(2, str2num(subject)+1);
            data.ssb.gen.signal = spreadsheet_data.ssb_spreadsheet_txtdata(3, str2num(subject)+1);
            data.ssb.age.signal = spreadsheet_data.ssb_spreadsheet_ndata(1+3, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            data.ssb.height.signal = spreadsheet_data.ssb_spreadsheet_ndata(2+3, str2num(subject));
            data.ssb.weight.signal = spreadsheet_data.ssb_spreadsheet_ndata(3+3, str2num(subject));
            data.ssb.arml.signal = spreadsheet_data.ssb_spreadsheet_ndata(4+3, str2num(subject));
            data.ssb.uac.signal = spreadsheet_data.ssb_spreadsheet_ndata(5+3, str2num(subject));
            data.ssb.sprtfreq.signal = spreadsheet_data.ssb_spreadsheet_txtdata(9, str2num(subject)+1);
            data.ssb.sprttype.signal = spreadsheet_data.ssb_spreadsheet_txtdata(10, str2num(subject)+1);
            data.ssb.smoker.signal = spreadsheet_data.ssb_spreadsheet_ndata(8+3, str2num(subject));
            data.ssb.cold.signal = spreadsheet_data.ssb_spreadsheet_txtdata(12, str2num(subject)+1);
            %also get datapool membership from spreadsheet
            data.datapool.signal = spreadsheet_data.ssb_spreadsheet_txtdata(13, str2num(subject)+1);
            
            %get reference BP from spreadsheet
            if experiment == 1
                row = 1;
            elseif experiment == 2
                row = 4;
            elseif experiment == 3
                row = 7;
            elseif experiment == 4
                row = 10;
            elseif experiment == 5
                row = 13;
            elseif experiment == 6
                row = 16;
            elseif experiment == 7
                row = 19;
            end
            
            %BP reference 
            data.bp.sbpref = spreadsheet_data.bp_spreadsheet_ndata(row+1, str2num(subject));
            data.bp.dbpref = spreadsheet_data.bp_spreadsheet_ndata(row+2, str2num(subject));
            
            %get context from experiment number
            if (experiment == 1) || (experiment == 2) || (experiment == 3)
                data.context.posture.signal = cellstr('sitting');
                data.context.condition.signal = cellstr('rest');
            elseif (experiment == 4) || (experiment == 5) || (experiment == 6)
                data.context.posture.signal = cellstr('standing');
                data.context.condition.signal = cellstr('rest');
            elseif (experiment == 7)
                data.context.posture.signal = cellstr('sitting');
                data.context.condition.signal = cellstr('cycling_grad_inc_int');
            end
            
            
            %if there is no .csv available then also set relevant fields for later overview to NaN
            chan = {'a', 'b', 'c', 'd', 'e', 'f', 'g' , 'h'};
            
            for i = 1:numel(chan)   
                
                data.ppg.(chan{i}).bp.sbp.signal = NaN; %BP as estimated by current model
                data.ppg.(chan{i}).bp.dbp.signal = NaN;
                data.ppg.(chan{i}).spotcheck.pat.signal = NaN;
                data.ppg.(chan{i}).spotcheck.pat.signal = NaN;
                data.ppg.(chan{i}).spotcheck.pat.sd = NaN;
                data.ppg.(chan{i}).spotcheck.pat.sd = NaN;
                data.ppg.(chan{i}).spotcheck.HR.signal = NaN;
                data.ppg.(chan{i}).spotcheck.HR.signal = NaN;
                data.ppg.(chan{i}).spotcheck.HR.sd = NaN;
                data.ppg.(chan{i}).spotcheck.HR.sd = NaN;
                data.ppg.(chan{i}).spotcheck.CI = NaN;
                data.ppg.(chan{i}).spotcheck.timestamps = NaN;
                data.ppg.(chan{i}).mat_CIraw.signal = NaN;
                data.ppg.(chan{i}).spotcheck.CIppg.signal = NaN;
            end
              
        elseif ((str2num(subject) > 32) && (str2num(subject) < 40)) || ((str2num(subject) > 32) && (str2num(subject) > 45 && (str2num(subject) < 85))) || (str2num(subject) > 106)
            %if only original .mat exists but data needs to run through allin chain (not for subjects between 40-45 who have csv's and will be processed in next 'else'statement)
            %i.e. only relevant for imec BE study except those saved as .csv
            file_exists = 1;
            
            load([file, '.mat']);
            
            %Access csv data and parse PAT stream (if available)
%            cd(DataAccess_folder_algexploration)
            if (isfield(data, 'band_pat')) && (~isempty(data.band_pat.timestamps)) && (~isfield(data.band_pat, 'pat_avg_for_BP'))  %only if PAT stream is available AND filled AND not already parsed in that specific .mat file
                data = parsePATstreamV2(data); %until IMEC BE study 'isfield(data, 'band_pat_bp')', respecively 'parsePATstreamV2' was used
            end
            
            %Disabled due to error, which seems to occur with splitting!!!
            %   if isfield(data, 'band_pat_info')
            %        data = parse_PAT_INFO_stream(data);
            %   end
            
            %    if isfield(data, 'band_pat_bp')
            %        data = parse_PAT_BP_stream(data);
            %    end
            
            %change format of SSB and context variable
            %SSB
            if isfield(data, 'ssb')
                if isfield(data.ssb, 'age') && ~isempty(data.ssb.age) && ~isfield(data.ssb.age, 'signal') %field there && not empty && only for first import
                    data.ssb.age.signal = data.ssb.age;
                end
                if isfield(data.ssb, 'height') && ~isempty(data.ssb.height) && ~isfield(data.ssb.height, 'signal')
                    data.ssb.height.signal = data.ssb.height;
                end
                if isfield(data.ssb, 'weight') && ~isempty(data.ssb.weight) && ~isfield(data.ssb.weight, 'signal')
                    data.ssb.weight.signal = data.ssb.weight;
                end
                if isfield(data.ssb, 'arml') && ~isempty(data.ssb.arml) && ~isfield(data.ssb.arml, 'signal')
                    data.ssb.arml.signal = data.ssb.arml;
                end
                %context
                if isfield(data, 'context')
                    if isfield(data.context, 'posture') && ~isempty(data.context.posture) && ~isfield(data.context.posture, 'signal')
                        data.context.posture.signal = cellstr(data.context.posture);
                    end
                end
            end
                    
            %% Get spreadsheet data before executeAllAlgorithms
            %such that SSB, ref BP, etc is available before executing BP Algo
            
            %Although some files are available as .mat file already, their ssb are not correct!!! (e.g. armlength 0 instead of NaN in spreadsheet)
            data.ssb.eth.signal = spreadsheet_data.ssb_spreadsheet_txtdata(2, str2num(subject)+1);
            data.ssb.gen.signal = spreadsheet_data.ssb_spreadsheet_txtdata(3, str2num(subject)+1);
            data.ssb.age.signal = spreadsheet_data.ssb_spreadsheet_ndata(1+3, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            data.ssb.height.signal = spreadsheet_data.ssb_spreadsheet_ndata(2+3, str2num(subject));
            data.ssb.weight.signal = spreadsheet_data.ssb_spreadsheet_ndata(3+3, str2num(subject));
            data.ssb.arml.signal = spreadsheet_data.ssb_spreadsheet_ndata(4+3, str2num(subject));
            data.ssb.uac.signal = spreadsheet_data.ssb_spreadsheet_ndata(5+3, str2num(subject));
            data.ssb.sprtfreq.signal = spreadsheet_data.ssb_spreadsheet_txtdata(9, str2num(subject)+1);
            data.ssb.sprttype.signal = spreadsheet_data.ssb_spreadsheet_txtdata(10, str2num(subject)+1);
            data.ssb.smoker.signal = spreadsheet_data.ssb_spreadsheet_ndata(8+3, str2num(subject));
            data.ssb.cold.signal = spreadsheet_data.ssb_spreadsheet_txtdata(12, str2num(subject)+1);
            %also get datapool membership from spreadsheet
            data.datapool.signal = spreadsheet_data.ssb_spreadsheet_txtdata(13, str2num(subject)+1);
            
            %get reference BP from spreadsheet
            if experiment == 1
                row = 1;
            elseif experiment == 2
                row = 4;
            elseif experiment == 3
                row = 7;
            elseif experiment == 4
                row = 10;
            elseif experiment == 5
                row = 13;
            elseif experiment == 6
                row = 16;
            elseif experiment == 7
                row = 19;
            end
            
            %BP reference 
            data.bp.sbpref = spreadsheet_data.bp_spreadsheet_ndata(row+1, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            data.bp.dbpref = spreadsheet_data.bp_spreadsheet_ndata(row+2, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            
            %get context from experiment number
            if (experiment == 1) || (experiment == 2) || (experiment == 3)
                data.context.posture.signal = cellstr('sitting');
                data.context.condition.signal = cellstr('rest');
            elseif (experiment == 4) || (experiment == 5) || (experiment == 6)
                data.context.posture.signal = cellstr('standing');
                data.context.condition.signal = cellstr('rest');
            elseif (experiment == 7)
                data.context.posture.signal = cellstr('sitting');
                data.context.condition.signal = cellstr('cycling_grad_inc_int');
            end
                        
            
            %% Run the Algo Chain
            %cd(AllAlgorithms_folder)
            
            %assure compatibility of old data from first experiment
            if str2num(subject) <= 32
                flip_PPG = 1;
            else
                flip_PPG = 0;
            end
            
            if isfield(data, 'acc')
               run_TDE = 1;
            else
               run_TDE = 0;
            end            
            
            %only if basic signals are available, e.g. a subject where no
            %trial was recorded, '[...]SI_1' will be saved when this trial
            %is main import. For import of '[...]SI_2', '[...]SI_1' will be
            %found but should not run through AllAlgorithms and should only
            %contain SSB and BP ref data
            if isfield(data, 'ecg') && isfield(data, 'ppg')
                [data]=executeAllAlgorithms(data, flip_PPG, run_TDE);  
            else
                
                %if there is no .csv available then also set relevant fields for later overview to NaN
                chan = {'a', 'b', 'c', 'd', 'e', 'f', 'g' , 'h'};  
                for i = 1:numel(chan)
                    
                    data.ppg.(chan{i}).bp.sbp.signal = NaN; %BP as estimated by current model
                    data.ppg.(chan{i}).bp.dbp.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.CI = NaN;
                    data.ppg.(chan{i}).spotcheck.timestamps = NaN;
                    data.ppg.(chan{i}).mat_CIraw.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.CIppg.signal = NaN;
                end          
            end
            
            %Uncomment to plot
            %cd('.\ValidationFramework\ViewReporting')
            %plotDebugInfoPATandBP(data, 'ppg.e')
            
        else % for 1-32 & 40-45 & 85-106 --> if .csv does exist, .mat does not and file should be imported
            
            file_exists = 1;
            
            %Access csv data 
            data = getSimbandCsv([[foldername,'/',file], '.csv'], '3v1');
            
            %Access csv data and parse PAT stream (if available)
            %%%cd(DataAccess_folder_algexploration)
            if (isfield(data, 'band_pat'))
                data = parsePATstreamV2(data); %until IMEC BE study 'isfield(data, 'band_pat_bp')', respecively 'parsePATstreamV2' was used
            end
            if isfield(data, 'band_pat_info') 
                data = parse_PAT_INFO_stream(data);
            end
            if isfield(data, 'band_pat_bp') 
                data = parse_PAT_BP_stream(data);
            end
            
            %% Get spreadsheet data before executeAllAlgorithms
            %such that SSB, ref BP, etc is available before executing BP Algo
            
            %Although some files are available as .mat file already, their ssb are not correct!!! (e.g. armlength 0 instead of NaN in spreadsheet)
            data.ssb.eth.signal = spreadsheet_data.ssb_spreadsheet_txtdata(2, str2num(subject)+1);
            data.ssb.gen.signal = spreadsheet_data.ssb_spreadsheet_txtdata(3, str2num(subject)+1);
            data.ssb.age.signal = spreadsheet_data.ssb_spreadsheet_ndata(1+3, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            data.ssb.height.signal = spreadsheet_data.ssb_spreadsheet_ndata(2+3, str2num(subject));
            data.ssb.weight.signal = spreadsheet_data.ssb_spreadsheet_ndata(3+3, str2num(subject));
            data.ssb.arml.signal = spreadsheet_data.ssb_spreadsheet_ndata(4+3, str2num(subject));
            data.ssb.uac.signal = spreadsheet_data.ssb_spreadsheet_ndata(5+3, str2num(subject));
            data.ssb.sprtfreq.signal = spreadsheet_data.ssb_spreadsheet_txtdata(9, str2num(subject)+1);
            data.ssb.sprttype.signal = spreadsheet_data.ssb_spreadsheet_txtdata(10, str2num(subject)+1);
            data.ssb.smoker.signal = spreadsheet_data.ssb_spreadsheet_ndata(8+3, str2num(subject));
            data.ssb.cold.signal = spreadsheet_data.ssb_spreadsheet_txtdata(12, str2num(subject)+1);
            %also get datapool membership from spreadsheet
            data.datapool.signal = spreadsheet_data.ssb_spreadsheet_txtdata(13, str2num(subject)+1);
            
            %get reference BP from spreadsheet
            if experiment == 1
                row = 1;
            elseif experiment == 2
                row = 4;
            elseif experiment == 3
                row = 7;
            elseif experiment == 4
                row = 10;
            elseif experiment == 5
                row = 13;
            elseif experiment == 6
                row = 16;
            elseif experiment == 7
                row = 19;
            end
            
            %BP reference 
            data.bp.sbpref = spreadsheet_data.bp_spreadsheet_ndata(row+1, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            data.bp.dbpref = spreadsheet_data.bp_spreadsheet_ndata(row+2, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            
            %get context from experiment number
            if (experiment == 1) || (experiment == 2) || (experiment == 3)
                data.context.posture.signal = cellstr('sitting');
                data.context.condition.signal = cellstr('rest');
            elseif (experiment == 4) || (experiment == 5) || (experiment == 6)
                data.context.posture.signal = cellstr('standing');
                data.context.condition.signal = cellstr('rest');
            elseif (experiment == 7)
                data.context.posture.signal = cellstr('sitting');
                data.context.condition.signal = cellstr('cycling_grad_inc_int');
            end
            
            %% Run the Algorithm Chain            
            
            %assure compatibility of old data from first experiment
            if str2num(subject) <= 32
                flip_PPG = 1;
            else
                flip_PPG = 0;
            end
           
            if isfield(data, 'acc')
               run_TDE = 1;
            else
               run_TDE = 0;
            end
             
            if isfield(data, 'ecg') && isfield(data, 'ppg')
                [data]=executeAllAlgorithms(data, flip_PPG, run_TDE);
            else
                
                %if there is no .csv available then also set relevant fields for later overview to NaN
                chan = {'a', 'b', 'c', 'd', 'e', 'f', 'g' , 'h'};  
                for i = 1:numel(chan)
                    
                    data.ppg.(chan{i}).bp.sbp.signal = NaN; %BP as estimated by current model
                    data.ppg.(chan{i}).bp.dbp.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.pat.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.HR.sd = NaN;
                    data.ppg.(chan{i}).spotcheck.CI = NaN;
                    data.ppg.(chan{i}).mat_CIraw.signal = NaN;
                    data.ppg.(chan{i}).spotcheck.CIppg.signal = NaN;
                end          
            end
            
        end

        %subject_ID (only for BP model testing)
        %if subject < 10
            data.subject_id = strcat('SP', num2str(subject));
        %else
        %    data.subject_id = strcat('SP', num2str(subject));
        %end
        
        %Always get SSB from spreadsheet! Although some files are available as .mat file already, their ssb are not correct!!! (e.g. armlength 0 instead of NaN in spreadsheet)
            data.ssb.eth.signal = spreadsheet_data.ssb_spreadsheet_txtdata(2, str2num(subject)+1);
            data.ssb.gen.signal = spreadsheet_data.ssb_spreadsheet_txtdata(3, str2num(subject)+1);
            data.ssb.age.signal = spreadsheet_data.ssb_spreadsheet_ndata(1+3, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
            data.ssb.height.signal = spreadsheet_data.ssb_spreadsheet_ndata(2+3, str2num(subject));
            data.ssb.weight.signal = spreadsheet_data.ssb_spreadsheet_ndata(3+3, str2num(subject));
            data.ssb.arml.signal = spreadsheet_data.ssb_spreadsheet_ndata(4+3, str2num(subject));
            data.ssb.uac.signal = spreadsheet_data.ssb_spreadsheet_ndata(5+3, str2num(subject));
            data.ssb.sprtfreq.signal = spreadsheet_data.ssb_spreadsheet_txtdata(9, str2num(subject)+1);
            data.ssb.sprttype.signal = spreadsheet_data.ssb_spreadsheet_txtdata(10, str2num(subject)+1);
            data.ssb.smoker.signal = spreadsheet_data.ssb_spreadsheet_ndata(8+3, str2num(subject));
            data.ssb.cold.signal = spreadsheet_data.ssb_spreadsheet_txtdata(12, str2num(subject)+1);
            %also get datapool membership from spreadsheet
            data.datapool.signal = spreadsheet_data.ssb_spreadsheet_txtdata(13, str2num(subject)+1);
            
        
        %get reference BP from spreadsheet
        if experiment == 1
            row = 1;
        elseif experiment == 2
            row = 4;
        elseif experiment == 3
            row = 7;
        elseif experiment == 4
            row = 10;
        elseif experiment == 5
            row = 13;
        elseif experiment == 6
            row = 16;
        elseif experiment == 7
            row = 19;
        end
        
        %BP reference
        data.bp.sbpref = spreadsheet_data.bp_spreadsheet_ndata(row+1, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
        data.bp.dbpref = spreadsheet_data.bp_spreadsheet_ndata(row+2, str2num(subject)); %+3 because subject id's have changed to numbers instead of text for easier maintenance of spreadsheet
        
        %get context from experiment number
        if (experiment == 1) || (experiment == 2) || (experiment == 3)
            data.context.posture.signal = cellstr('sitting'); 
            data.context.condition.signal = cellstr('rest');
        elseif (experiment == 4) || (experiment == 5) || (experiment == 6)
            data.context.posture.signal = cellstr('standing');
            data.context.condition.signal = cellstr('rest');
        elseif (experiment == 7)
            data.context.posture.signal = cellstr('sitting');
            data.context.condition.signal = cellstr('cycling_grad_inc_int');
        end

end

