% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <import data for feature extraction>
% Content   : Main script for import of data to be used for BP model
% training and validation
% Version   : 1.1
% Author    : F. Beutel (fabian.beutel@imec-nl.nl)
%  Modification and Version History:
%  | Developer | Version |    Date    |     Changes           |
%  | F. Beutel |   1.0   |  21/01/15  | Starting from BP v6.0 this script imports the data collected from several experiments for subsequent model training and validation |
%  | F. Beutel |   1.1   |  30/03/15  | Modified to use 'polyfit' instead of 'fit' due to availability of toolboxes |
%  | F. Beutel |   2.0   |  10/04/15  | Use cell array 'invalid_files' to define excluded files instead of renaming the files |
%  | ehermeling|   2.1   |  24/06/15 | Made compatible with feature extraction (replaced PAT)

% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

clear all; close all; clc;

%% Basic settings & definitions
chan = 'e';
%BP model version
%%%version = '7.1';
%data folder
%data_folder = ['\\winnl.imec.be\simbadc\WinNT\Applications\simband_data_large_scale\PAT_based_BP_partial_IEEE_data_for_BP_model'];
%AllAlgorithms (simba-common) folder
%%%AllAlgorithms_folder = [pwd '\..\..\AllAlgorithms\'];
%BP_framework_folder
%%%BP_framework_folder = [pwd '\'];
%DataAccess (algexploration) folder
%%%DataAccess_folder_algexploration = [AllAlgorithms_folder '..\..\..\..\algexploration\ResearcherConsole\Matlab\DataAccess\'];
%DataAccess (simba-common) folder
%%%DataAccess_folder_simba_common = [AllAlgorithms_folder '..\ValidationFramework\DataAccess\'];
%Add schema for data access to current path
%%%addpath([DataAccess_folder_algexploration '\schemas'])
%ViewReporting (simba-common) folder
%%%ViewReporting_folder = [AllAlgorithms_folder '..\ValidationFramework\ViewReporting\'];
%define matrix for overview subjects and preselection
overview_subjects_sitting = []; %str2num(subject), data.bp.sbpref, data.bp.dbpref, data.ppg.e.bp.sbp.signal, data.ppg.e.bp.dbp.signal, data.pat.pat_avg_for_BP, data..spotcheck.CI, etc.

%% select subjects to be imported

%all subjects
subjects = {};

for i =1:822
    subject_string_without_zeros = num2str(i);
    zeros_to_add = 4-numel(subject_string_without_zeros);
    subject_string_with_zeros_and_spaces = [num2str(zeros(1,zeros_to_add)), subject_string_without_zeros];    
    subject_string_with_zeros_no_spaces = regexprep(subject_string_with_zeros_and_spaces,'[^\w'']','');   
    subjects{i} = subject_string_with_zeros_no_spaces;
    
    %# find empty cells (to be deleted below)
    emptyCells = cellfun(@isempty,subjects); 
    
end
%# delete empty cells
subjects(emptyCells) = [];

%bad subjects (in which reference measurements are clearly invalid due to no adherence to the protocol, e.g. talking during measurement or obvious upper arm muscle tension)
bad_subjects = {'0012', '0039'};


%% Define cell arrays to store invalid file names for training phase
invalid_files = {};

%% get BP and SSB data from xlsx files
%cd(data_folder);

%get relevant content from spreadsheet
spreadsheet_data.ssb_spreadsheet_ndata = xlsread(['PAT-based_BP_estimation_SSB_and_BP_data' '.xlsx'],'SSB');
[~,spreadsheet_data.ssb_spreadsheet_txtdata] = xlsread(['PAT-based_BP_estimation_SSB_and_BP_data' '.xlsx'],'SSB');
spreadsheet_data.bp_spreadsheet_ndata = xlsread(['PAT-based_BP_estimation_SSB_and_BP_data' '.xlsx'],'BP Data');

%% Start import
%Currently only for the primary experiment!

for subject_idx = 1:1:numel(subjects)
    
    subject = subjects{subject_idx};
    
    foldername = ['SP' subject];
    folder_struct = what(foldername);
    path_folder_current_subject = folder_struct.path;
    
    experiments = {'_S_SI_1' '_S_SI_2' '_S_SI_3' '_S_ST_1' '_S_ST_2' '_S_ST_3' '_V'} ;
    
    %to make sure that it is not taken over from previous experiment as
    %coefficients is only computed for first experiment (which might not be
    %available in all cases!)
    coefficients = 0;
    
    for experiment = 1:1:numel(experiments)
        
        file = [foldername sprintf(experiments{experiment})];
        
        %import file for current experiment
        disp(['Importing: ' file])
        %%%        cd(BP_framework_folder)
        data = import_file_FE(subject, experiment, foldername, file, spreadsheet_data);
        
        %% Sitting experiment import selection (Not to be used for BP CI research!)
        
        if experiment <= 3 %only for sitting
            
            %gather data from all three measurements per subject to
            %identify BP ref variability and/or expected PAT-BP relation
            
            evalc(['data_' num2str(experiment) '= data']);
            %import the respective other (normally two) experiments
            all_experiments = [1:3];
            other_experiments = setdiff(all_experiments, experiment);
            
            for other_experiments_idx = 1:numel(other_experiments)
                
                other_experiment = other_experiments(other_experiments_idx);
                file_other_experiment = [foldername sprintf(experiments{other_experiment})];
                disp(['Importing other experiment: ' file_other_experiment])
                %cd(BP_framework_folder)
                data_other_experiment = import_file_FE(subject, other_experiment, foldername, file_other_experiment, spreadsheet_data);
                evalc(['data_' num2str(other_experiment) '= data_other_experiment']);
                
            end
            
            
            %check for expected PAT-BP relation by fitting linear regression to triplet (only once for first experiment)
            if experiment == 1
                
                % in case of exceeded variaility, check for expected PAT-BP relation
                pat_current_subject = [data_1.ppg.(chan).spotcheck.pat.signal(1), data_2.ppg.(chan).spotcheck.pat.signal(1), data_3.ppg.(chan).spotcheck.pat.signal(1)]
                sbp_current_subject = [data_1.bp.sbpref(1), data_2.bp.sbpref(1), data_3.bp.sbpref(1)]
                dbp_current_subject = [data_1.bp.dbpref(1), data_2.bp.dbpref(1), data_3.bp.dbpref(1)]
                
                %define function linear fit
                %lin_fit = fittype('a*x+b');
                
                if (numel(pat_current_subject) > 1) && (sum(~isnan(sbp_current_subject)) > 0) %only possible for 2 or more values && when at least one refBP is available
                    %make sure that no incomplete value pairs exist (due to failing pat computation, ref BP are always expected)
                    pat_current_subject_plotting = pat_current_subject(~isnan(pat_current_subject) & ~isnan(sbp_current_subject) & ~isnan(dbp_current_subject));
                    sbp_current_subject_plotting = sbp_current_subject(~isnan(pat_current_subject) & ~isnan(sbp_current_subject) & ~isnan(dbp_current_subject));
                    dbp_current_subject_plotting = dbp_current_subject(~isnan(pat_current_subject) & ~isnan(sbp_current_subject) & ~isnan(dbp_current_subject));
                    
                    %linear fit
                    %[linear_fit, gof] = fit(pat_current_subject_plotting(~isnan(pat_current_subject_plotting))', sbp_current_subject_plotting(~isnan(sbp_current_subject_plotting))', lin_fit); %'StartPoint', [0 0], 'Lower', [-1000 -1000], 'Upper', [1000 1000])
                    %coefficients = coeffvalues(linear_fit);
                    %added by roberto to replace fittype
                    coefficients = polyfit(pat_current_subject_plotting(~isnan(pat_current_subject_plotting))', sbp_current_subject_plotting(~isnan(sbp_current_subject_plotting))',1);
                    function_linear_fit = @(pat_current_subject_plotting) coefficients(1) * pat_current_subject_plotting + coefficients(2);
                    
                    %uncomment to plot
                    %fig_field_validation_outcome = figure('Name', ['Fitting Subject ' subject]); hold on;
                    %scatter(pat_current_subject_plotting, sbp_current_subject_plotting)
                    %fplot(function_linear_fit, [min(pat_current_subject_plotting) max(pat_current_subject_plotting)],'r');
                    
                elseif  numel(pat_current_subject(~isnan(pat_current_subject))) == 1 %for single pat value
                    %make sure that no incomplete value pairs exist (due to failing pat computation or similar)
                    pat_current_subject_plotting = pat_current_subject(~isnan(pat_current_subject));
                    sbp_current_subject_plotting = sbp_current_subject(~isnan(pat_current_subject));
                    
                    %uncomment to plot
                    %fig_field_validation_outcome = figure('Name', ['Fitting Subject ' subject]); hold on;
                    %scatter(pat_current_subject_plotting, sbp_current_subject_plotting)
                    
                elseif numel(pat_current_subject(~isnan(pat_current_subject))) == 0 %no pat values detected
                    coefficients = 0;
                end
                
                mean_pat = nanmean(pat_current_subject);
                mean_sbp = nanmean(sbp_current_subject);
                mean_dbp = nanmean(dbp_current_subject);
                
                std_pat = nanstd(pat_current_subject);
                std_sbp = nanstd(sbp_current_subject);
                std_dbp = nanstd(dbp_current_subject);
                
                clear data_1
                clear data_2
                clear data_3
                clear pat_current_subject_plotting
                clear sbp_current_subject_plotting
                clear dbp_current_subject_plotting
            end
            
            %compute other relevant metrics
            pat_mean_distance = abs(mean_pat - data.ppg.(chan).spotcheck.pat.signal(1));
            sbp_mean_distance = abs(mean_sbp - data.bp.sbpref);
            dbp_mean_distance = abs(mean_dbp - data.bp.dbpref);
            
            %Check for validity for 1)BPref variability, 2)BPref availability and 3)CI
            
            %Assume validity of current data in the first place
            validity_current_import = 1;
            
            % check variability of BP ref (either SBP or DBP)
            if  sbp_mean_distance > 7 || dbp_mean_distance > 7
                validity_current_import = 2;
                %validity_current_import = 1; %uncomment to discard criterion
            end
            
            %in case of variability, check for expected BP-PAT relation for all three points
            %(only SBP as DBP relationship is expected to be worse)
            threepoint_relation = 0;
            if coefficients(1) < 0
                threepoint_relation = 1;
                validity_current_import = 1;
            end
            
            %if not between 3 points, check whether any two out of
            %three show expected SBP-PAT relation
            
            twopoint_relation = 0;
            other_exp_left_up = [];
            other_exp_right_down = [];
            if experiment == 1
                
                for other_experiments_idx = 1:numel(other_experiments)
                    
                    other_experiment = other_experiments(other_experiments_idx);
                    other_exp_left_up(other_experiment) =  (pat_current_subject(other_experiment) < pat_current_subject(experiment)) && (sbp_current_subject(other_experiment) > sbp_current_subject(experiment));
                    other_exp_right_down(other_experiment) = (pat_current_subject(other_experiment) > pat_current_subject(experiment)) && (sbp_current_subject(other_experiment) < sbp_current_subject(experiment));
                    
                end
                
                if any(other_exp_left_up) || any(other_exp_right_down)
                    twopoint_relation = 1;
                    validity_current_import = 1;
                end
                
            elseif experiment == 2
                
                for other_experiments_idx = 1:numel(other_experiments)
                    
                    other_experiment = other_experiments(other_experiments_idx);
                    other_exp_left_up(other_experiment) =  (pat_current_subject(other_experiment) < pat_current_subject(experiment)) && (sbp_current_subject(other_experiment) > sbp_current_subject(experiment));
                    other_exp_right_down(other_experiment) = (pat_current_subject(other_experiment) > pat_current_subject(experiment)) && (sbp_current_subject(other_experiment) < sbp_current_subject(experiment));
                    
                end
                
                if any(other_exp_left_up) || any(other_exp_right_down)
                    twopoint_relation = 1;
                    validity_current_import = 1;
                end
                
            elseif experiment == 3
                
                for other_experiments_idx = 1:numel(other_experiments)
                    
                    other_experiment = other_experiments(other_experiments_idx);
                    other_exp_left_up(other_experiment) =  (pat_current_subject(other_experiment) < pat_current_subject(experiment)) && (sbp_current_subject(other_experiment) > sbp_current_subject(experiment));
                    other_exp_right_down(other_experiment) = (pat_current_subject(other_experiment) > pat_current_subject(experiment)) && (sbp_current_subject(other_experiment) < sbp_current_subject(experiment));
                    
                end
                
                if any(other_exp_left_up) || any(other_exp_right_down)
                    twopoint_relation = 1;
                    validity_current_import = 1;
                end
                
            end
            
            % check validity with respect to PAT CI
            if data.ppg.(chan).spotcheck.CI < 3
                validity_current_import = 3;
                %validity_current_import = 1; %uncomment to discard criterion
            end
            
            % check availability of BP ref or Simband recording
            if isnan(data.bp.sbpref) || isnan(data.bp.sbpref) || ~isfield(data, 'ecg') || ~isfield(data, 'ppg')%if no ref BP exists no .mat file is generated
                validity_current_import = 4;
            end
            
            % check whether subject is invalid due to invalid reference BP
            if ismember(subject, bad_subjects)
                validity_current_import = 5;
                %validity_current_import = 1; %uncomment to discard criterion
            end
            
            % check whether PPG CI raw
            if data.ppg.(chan).spotcheck.CIppg.signal < 2
                validity_current_import = 6;
                %validity_current_import = 1; %uncomment to discard criterion
            end
            
            
            if validity_current_import == 1
                save([path_folder_current_subject '\' file '.mat'], 'data')
                delete([path_folder_current_subject '\' file '_excluded.mat']) %delete in case that criteria changed
            elseif validity_current_import == 2
                save([path_folder_current_subject '\' file '.mat'], 'data')
                %save([data_folder '\' foldername '\' file '_excluded'], 'data')
                disp([file ': File not imported due to variability of BP ref'])
                invalid_files{1} = [invalid_files{:} {file}];
            elseif validity_current_import == 3
                save([path_folder_current_subject '\' file '.mat'], 'data')
                %save([data_folder '\' foldername '\' file '_excluded'], 'data')
                disp([file ': File not imported due to CI PAT'])
                invalid_files{1} = [invalid_files{:} {file}];
            elseif validity_current_import == 4
                save([path_folder_current_subject '\' file '.mat'], 'data')
                %save([data_folder '\' foldername '\' file '_excluded'], 'data')
                disp([file ': File not imported due to to nonexisting file / availability of BP ref'])
                invalid_files{1} = [invalid_files{:} {file}];
            elseif validity_current_import == 5
                save([path_folder_current_subject '\' file '.mat'], 'data')
                %save([data_folder '\' foldername '\' file '_excluded'], 'data')
                disp([file ': File not imported due to bad subject'])
                invalid_files{1} = [invalid_files{:} {file}];
            elseif validity_current_import == 6
                save([path_folder_current_subject '\' file '.mat'], 'data')
                %save([data_folder '\' foldername '\' file '_excluded'], 'data')
                disp([file ': File not imported due to raw CI PPG'])
                invalid_files{1} = [invalid_files{:} {file}];
            end
            
            
            %make an overview of all relevant variables (to be compared against automatized validity judgement, e.g. valid PATs of 100 ms that were not encountered before
            
            %first check for availability of CI raw (could be missing as consequence of missing ACC, etc.)
            if ~isfield(data.ppg.e, 'mat_CIraw')
                data.ppg.e.mat_CIraw.signal = NaN;
            end
            
            overview_subjects_sitting = [overview_subjects_sitting; ...
                str2num(subject), ...
                data.bp.sbpref(1), ...
                data.bp.dbpref(1), ...
                data.ppg.e.bp.sbp.signal(1), ...
                data.ppg.e.bp.dbp.signal(1), ...
                data.ppg.(chan).spotcheck.pat.signal(1), ...
                data.ppg.(chan).spotcheck.CI(1), ...
                round(nanmean(data.ppg.e.mat_CIraw.signal)), ...
                data.ssb.age.signal(1), ...
                data.ssb.height.signal(1), ...
                data.ssb.weight.signal(1), ...
                data.ssb.arml.signal(1), ...
                mean_pat, ...
                std_pat, ...
                pat_mean_distance, ...
                mean_sbp, ...
                std_sbp, ...
                sbp_mean_distance, ...
                mean_dbp, ...
                std_dbp, ...
                dbp_mean_distance, ...
                threepoint_relation, ...
                twopoint_relation, ...
                validity_current_import];
            
            
            %uncomment to generate debug plot
            %cd(ViewReporting_folder)
            %plotDebugInfoPATandBP(data, 'ppg.e')
            
        elseif experiment > 3 %save standing & variation experiments with validity check only for bpref
            
            %Check for validity for 1)BPref variability, 2)BPref availability
            
            %Assume validity of current data in the first place
            validity_current_import = 1;
            
            % check availability of BP ref
            if isnan(data.bp.sbpref) || isnan(data.bp.sbpref) %if no ref BP exists no .mat file is generated
                validity_current_import = 4;
            end
            
            if validity_current_import == 1
                save([path_folder_current_subject '\' file '.mat'], 'data')
                %delete([path_folder_current_subject '\' file '_excluded.mat']) %delete in case that criteria changed
                
                % No further checks on variability or expected BP-PAT relation for standing
                
            elseif validity_current_import == 4
                save([path_folder_current_subject '\' file '.mat'], 'data')
                disp('File not imported due to nonexisting file / availability of BP ref')
                invalid_files{1} = [invalid_files{:} {file}];
            elseif validity_current_import == 5
                save([path_folder_current_subject '\' file '.mat'], 'data')
                disp([file ': File not imported due to bad subject'])
                invalid_files{1} = [invalid_files{:} {file}];
            end
            
        end
        
    end
end

%% save 'invalid_files' as detached variable for training phase
save([path_folder_current_subject, '\..\', 'invalid_files.mat'], 'invalid_files')

%% Output and save import 'overview_subjects_sitting' as detached variable for training phase
save([path_folder_current_subject, '\..\outcome_import\', 'overview_subjects_sitting.mat'], 'overview_subjects_sitting')


%% Compute important outcomes of import

%For all included subjects

%expected number of subjects and trials participated
disp('changed number of expected particpants for debugging')
% exp_subjects_participated = 386;
exp_subjects_participated = 2;
exp_trials = exp_subjects_participated*3;
%actual number of subjects participated
act_subjects_participated = numel(unique(overview_subjects_sitting(:,1)))
%acutal number of trials conducted
act_trials = numel(overview_subjects_sitting(:,1))

%subject id's in databases
subjects_v2_1 = 1:32;
subjects_v2_2 = 33:84;
subjects_v2_3 = 85:106;
subjects_v3_1 = 107:136;
subjects_v3_2 = 137:224;
subjects_v3_3 = 225:386;

%expected number of subjects in databases
exp_subjects_v2_1 = numel(subjects_v2_1);
exp_subjects_v2_2 = numel(subjects_v2_2);
exp_subjects_v2_3 = numel(subjects_v2_3);
exp_subjects_v3_1 = numel(subjects_v3_1);
exp_subjects_v3_2 = numel(subjects_v3_2);
exp_subjects_v3_3 = numel(subjects_v3_3);

%expected number of trials in databases
exp_trials_v2_1 = numel(subjects_v2_1)*3;
exp_trials_v2_2 = numel(subjects_v2_2)*3;
exp_trials_v2_3 = numel(subjects_v2_3)*3;
exp_trials_v3_1 = numel(subjects_v3_1)*3;
exp_trials_v3_2 = numel(subjects_v3_2)*3;
exp_trials_v3_3 = numel(subjects_v3_3)*3;

if exp_subjects_participated ~= act_subjects_participated
    disp('expected and actual number of subjects participated are not the same!')
end

if exp_trials ~= act_trials
    disp('expected and actual number of trials are not the same!')
end


%overview matrix only for subjects who are imported valid
overview_subjects_sitting_valid = overview_subjects_sitting(overview_subjects_sitting(:,end)==1, :);

%subjects who have at least one valid trial
subjects_valid = numel(unique(overview_subjects_sitting_valid(:,1)))
trials_valid = numel(overview_subjects_sitting_valid(:,1))

%ratio of participated and valid subjects
subjects_ratio = subjects_valid/act_subjects_participated
%ratio of executed and valid trials
trials_ratio = trials_valid/act_trials

%Per included database

%valid subjects per database
subjects_valid_v2_1 = numel(unique(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v2_1))));
subjects_valid_v2_2 = numel(unique(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v2_2))));
subjects_valid_v2_3 = numel(unique(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v2_3))));
subjects_valid_v3_1 = numel(unique(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v3_1))));
subjects_valid_v3_2 = numel(unique(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v3_2))));
subjects_valid_v3_3 = numel(unique(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v3_3))));

subjects_valid_v2 = subjects_valid_v2_1 + subjects_valid_v2_2 + subjects_valid_v2_3;
subjects_valid_v3 = subjects_valid_v3_1 + subjects_valid_v3_2 + subjects_valid_v3_3;

%valid trials per database
trials_valid_v2_1 = numel(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v2_1)));
trials_valid_v2_2 = numel(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v2_2)));
trials_valid_v2_3 = numel(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v2_3)));
trials_valid_v3_1 = numel(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v3_1)));
trials_valid_v3_2 = numel(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v3_2)));
trials_valid_v3_3 = numel(overview_subjects_sitting_valid(ismember(overview_subjects_sitting_valid(:,1), subjects_v3_3)));

trials_valid_v2 = trials_valid_v2_1 + trials_valid_v2_2 + trials_valid_v2_3;
trials_valid_v3 = trials_valid_v3_1 + trials_valid_v3_2 + trials_valid_v3_3;

%ratio of participated and valid subjects per database

subjects_ratio_v2_1 = subjects_valid_v2_1/exp_subjects_v2_1
subjects_ratio_v2_2 = subjects_valid_v2_2/exp_subjects_v2_2
subjects_ratio_v2_3 = subjects_valid_v2_3/exp_subjects_v2_3
subjects_ratio_v3_1 = subjects_valid_v3_1/exp_subjects_v3_1
subjects_ratio_v3_2 = subjects_valid_v3_2/exp_subjects_v3_2
subjects_ratio_v3_3 = subjects_valid_v3_3/exp_subjects_v3_3

%ratio of executed and valid trials
trials_ratio_v2_1 = trials_valid_v2_1/exp_trials_v2_1
trials_ratio_v2_2 = trials_valid_v2_2/exp_trials_v2_2
trials_ratio_v2_3 = trials_valid_v2_3/exp_trials_v2_3
trials_ratio_v3_1 = trials_valid_v3_1/exp_trials_v3_1
trials_ratio_v3_2 = trials_valid_v3_2/exp_trials_v3_2
trials_ratio_v3_3 = trials_valid_v3_3/exp_trials_v3_3

%% plotting etc
%
if 1  % debug plots
    overview_subjects_sitting_ci_cleaned = overview_subjects_sitting(overview_subjects_sitting(:,7) > 2, :);
    
    
    sbp = overview_subjects_sitting(:,2)
    pat = overview_subjects_sitting(:,6)
    age = overview_subjects_sitting(:,9)
    height = overview_subjects_sitting(:,10)
    weight = overview_subjects_sitting(:,11)
    arml =  overview_subjects_sitting(:,12)
    bmi = weight./((height./100).^2)
    
    sbp_ci_cleaned = overview_subjects_sitting_ci_cleaned(:,2)
    pat_ci_cleaned = overview_subjects_sitting_ci_cleaned(:,6)
    age_ci_cleaned = overview_subjects_sitting_ci_cleaned(:,9)
    height_ci_cleaned = overview_subjects_sitting_ci_cleaned(:,10)
    weight_ci_cleaned = overview_subjects_sitting_ci_cleaned(:,11)
    arml_ci_cleaned =  overview_subjects_sitting_ci_cleaned(:,12)
    bmi_ci_cleaned = weight_ci_cleaned./((height_ci_cleaned./100).^2)
    
    figure, hold on;
    
    subplot(2,2,1)
    scatter(pat, arml)
    xlabel('PAT [ms]')
    ylabel('Armlength [cm]')    
    
    subplot(2,2,2)
    scatter(pat_ci_cleaned, arml_ci_cleaned)
    subplot(2,2,3)
    scatter(pat, height)
    subplot(2,2,4)
    scatter(pat_ci_cleaned, height_ci_cleaned)
    
    figure, hold on
    scatter(pat_ci_cleaned, height_ci_cleaned)
    xlabel('PAT [ms]')
    ylabel('Height [cm]')
    
    figure, hold on
    scatter(pat_ci_cleaned, arml_ci_cleaned)
    xlabel('PAT [ms]')
    ylabel('Armlength [cm]')
    
    figure, hold on
    scatter(age_ci_cleaned, sbp_ci_cleaned)
    xlabel('Age [years]')
    ylabel('SBP [mmHg]')
    
    figure, hold on
    scatter(weight_ci_cleaned, sbp_ci_cleaned)
    xlabel('Weight [kg]')
    ylabel('SBP [mmHg]')
    
    figure, hold on
    scatter(bmi_ci_cleaned, sbp_ci_cleaned)
    xlabel('BMI [kg/m^2]')
    ylabel('SBP [mmHg]')
end