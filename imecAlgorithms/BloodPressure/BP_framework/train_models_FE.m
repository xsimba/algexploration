% ------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <train_models.m>
% Content   : Main script for training the LOO and full models
% Version   : 1.0
% Author    : F. Beutel (fabian.beutel@imec-nl.nl)
%  Modification and Version History:
%  | Developer | Version |    Date    |     Changes           |
%  | F. Beutel |   1.0   |  21/01/15  | Starting from BP v6.0 this script trains the models based on data collected from several experiments |
%  |  fbeutel  |   2.0   | 20-03-2014 | Implementation of datapool selection for selective training (and validation) |
%  | F. Beutel |   3.0   |  10/04/15  | Use cell array 'invalid_files' to define excluded files instead of renaming the files |
%  | F. Beutel |   3.1   |  23/04/15  | Save coefficients for linear relations between training variables |
%  | ehermeling|   3.2   |  24/06/15  | Made compatible with feature extraction (replaced PAT)
%  | F. Beutel |   4.0   |  06/08/15  | Introduce efficient training mode without LOOCV |

% Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

clear all; close all; clc;

%% Basic settings & definitions

apply_LOOCV = 0; %decide whether or not to use Leave-One-Out cross validation
%matrices required to be defined outside the loop in case that (apply_LOOCV = 0)
training_matrix_sitting_no_loso = []; 
training_matrix_sitting_no_ssb_no_loso = [];
CI_SSB_matrix_sitting_no_loso = [];
training_matrix_sitting_no_loso_subject = [];
training_matrix_sitting_no_ssb_no_loso_subject = [];
CI_SSB_matrix_sitting_no_loso_subject = [];

chan = 'e';

%BP model version
%data folder
%%data_folder = [pwd '\..\..\..\..\..\simband_data\PAT_based_BP_partial_IEEE_data_for_BP_model'];

all_subjects = [1:822]; %386
datapools_for_training = {'2.1', '2.2', '2.3', '3.1', '3.2', '3.3', '4.2', '4.3'}; %2.1:imecNL, 2.2:imecBE, 2.3:Xmas, 3.1:imecBE LSDC imecTeam, 3.2:imecBE LSDC Asad, 4.2:imecBE LSDC Asad (HBP-1300), 4.3:imecBE LSDC Jan (HBP-1300)

%uncomment to preselect subjects for training
%     bad_subjects = [2, 12, 13, 15, 27, 30];
%     good_subjects = setdiff(all_subjects, bad_subjects);
%     all_subjects = good_subjects;
    
experiments = {'_S_SI_1' '_S_SI_2' '_S_SI_3' '_S_ST_1' '_S_ST_2' '_S_ST_3' '_V'};

%load cell array containing invalid files
%%cd(data_folder)
load('invalid_files.mat');

%% Fill training matrices for current subject

if apply_LOOCV
    
    for subject_idx = 1:numel(all_subjects)
        
        subject = all_subjects(subject_idx);
        
        %define subject_ID
        if subject < 10
            subject_id = strcat('SP000', num2str(subject));
        elseif (subject >= 10) && (subject < 100)
            subject_id = strcat('SP00', num2str(subject));
        elseif (subject >= 100) && (subject < 1000)
            subject_id = strcat('SP0', num2str(subject));
        elseif subject >= 1000
            subject_id = strcat('SP', num2str(subject));
        end
        
        disp(['Training ',subject_id])
        
        foldername = subject_id;
        folder_struct = what(foldername);
        path_folder_current_subject = folder_struct.path;
        
        %%subject_folder = ([data_folder '\' subject_id]);
        
        training_matrix_sitting_loso = []; %SBP DBP PAT PAT PATsd HR HRsd age height weight
        training_matrix_sitting_no_ssb_loso = []; %SBP DBP PAT PAT PATsd HR HRsd
        %CI SSB matrix for current subject
        CI_SSB_matrix_sitting_loso = [];
        
        training_matrix_standing_loso = [];
        training_matrix_standing_no_ssb_loso = [];
        %CI SSB matrix for current subject
        CI_SSB_matrix_standing_loso = [];
        
        %     training_matrix_variation_loso = [];
        %     training_matrix_variation_no_ssb_loso = [];
        
        
        for loso_subject = setdiff(all_subjects, subject)
            
            %define loso subject_ID
            if loso_subject < 10
                loso_subject_id = strcat('SP000', num2str(loso_subject));
            elseif (loso_subject >= 10) && (loso_subject < 100)
                loso_subject_id = strcat('SP00', num2str(loso_subject));
            elseif (loso_subject >= 100) && (loso_subject < 1000)
                loso_subject_id = strcat('SP0', num2str(loso_subject));
            elseif loso_subject >= 1000
                loso_subject_id = strcat('SP', num2str(loso_subject));
            end
            
            %%loso_subject_folder = ([data_folder '\' loso_subject_id]);
            %%cd(loso_subject_folder)
            
            %% Sitting Experiment
            
            %traning matrices for current loso subject
            training_matrix_sitting_loso_subject = [];
            training_matrix_sitting_no_ssb_loso_subject = [];
            %CI SSB matrix for current loso subject
            CI_SSB_matrix_sitting_loso_subject = [];
            
            for exp_idx = 1:3
                
                data_file = ([loso_subject_id, experiments{exp_idx},'.mat']);
                
                if ~exist(data_file) || (ismember(data_file(1:end-4), invalid_files{:})) %end-4 to delete '.mat' file ending
                    
                    %disp (['Missing Experiment Data ', loso_subject_id, experiments{exp_idx}])
                    
                else
                    
                    load(data_file);
                    
                    if ismember(data.datapool.signal, datapools_for_training)
                        
                        training_matrix_sitting_loso_subject = [training_matrix_sitting_loso_subject; ...
                            data.bp.sbpref, ...
                            data.bp.dbpref, ...
                            data.ppg.(chan).spotcheck.pat.signal(1), ...
                            data.ppg.(chan).spotcheck.pat.sd(1), ...
                            data.ppg.(chan).spotcheck.HR.signal(1), ...
                            data.ppg.(chan).spotcheck.HR.sd(1), ...
                            data.ssb.age.signal(1), ...
                            data.ssb.height.signal(1), ...
                            data.ssb.weight.signal(1), ...
                            data.ssb.height.signal(1)./data.ppg.(chan).spotcheck.pat.signal(1), ... %PWV via height
                            data.ssb.height.signal(1)./data.ppg.(chan).spotcheck.pat.sd(1), ... %PWVsd via height
                            data.ssb.weight.signal(1)./(data.ssb.height.signal(1)./100).^2]; %BMI
                        
                        
                        training_matrix_sitting_no_ssb_loso_subject = [training_matrix_sitting_no_ssb_loso_subject; ...
                            data.bp.sbpref, ...
                            data.bp.dbpref, ...
                            data.ppg.(chan).spotcheck.pat.signal(1), ...
                            data.ppg.(chan).spotcheck.pat.sd(1), ...
                            data.ppg.(chan).spotcheck.HR.signal(1), ...
                            data.ppg.(chan).spotcheck.HR.sd(1)];
                        
                        
                        CI_SSB_matrix_sitting_loso_subject = [CI_SSB_matrix_sitting_loso_subject; ...
                            str2num(loso_subject_id(3:6)) ...
                            data.ssb.age.signal(1), ...
                            data.ssb.height.signal(1), ...
                            data.ssb.weight.signal(1), ...
                            data.bp.sbpref, ...
                            data.bp.dbpref, ...
                            data.ppg.(chan).spotcheck.pat.signal(1), ...
                            data.ppg.(chan).spotcheck.pat.sd(1), ...
                            data.ppg.(chan).spotcheck.CI(1), ...
                            data.ppg.(chan).spotcheck.HR.signal(1), ...
                            data.ppg.(chan).spotcheck.HR.sd(1)];
                        
                    end
                    
                end
            end
            
            %% Fill matrix for LOSO training
            
            %fill data in loso training matrix
            training_matrix_sitting_loso = [training_matrix_sitting_loso; training_matrix_sitting_loso_subject]; %SBP DBP PAT PAT PATsd HR HRsd age height weight
            training_matrix_sitting_no_ssb_loso = [training_matrix_sitting_no_ssb_loso; training_matrix_sitting_no_ssb_loso_subject]; %SBP DBP PAT PAT PATsd HR HRsd
            
            %% Fill matrix with SSB for CI
            CI_SSB_matrix_sitting_loso = [CI_SSB_matrix_sitting_loso; CI_SSB_matrix_sitting_loso_subject];
            
        end
        
        % For the first loop iteration / the first subject:
        % Save the training matrices of the last subject (currently in 'training_matrix_[  ]_loso_subject') in order to append them to those of the very last 'training_matrix_[  ]_loso' which contains all training data except those of the last subject
        % Consequently this yields the full training matrix/matrices
        
        if subject_idx == 1
            training_matrix_sitting_loso_last_subject = training_matrix_sitting_loso_subject;
            training_matrix_sitting_no_ssb_loso_last_subject = training_matrix_sitting_no_ssb_loso_subject;
        end
        
        %% Train and save individual LOSO model for current subject
        %%%    cd(subject_folder)
        ntrees = 11; %11
        min_leaf = 5; %default for method 'regression'
        n_var_to_sample = 3; %default: sqrt of number of input variables
        
        %sitting models RF
        sbp_model_sitting = TreeBagger(ntrees,[training_matrix_sitting_loso(:,3:9) training_matrix_sitting_loso(:,2)], training_matrix_sitting_loso(:,1),'Method','regression','NVarToSample', n_var_to_sample, 'MinLeaf', min_leaf);
        dbp_model_sitting = TreeBagger(ntrees,[training_matrix_sitting_loso(:,3:9)], training_matrix_sitting_loso(:,2),'Method','regression','NVarToSample', n_var_to_sample, 'MinLeaf', min_leaf);
        
        %sitting models GLM 6.5 in: PAT, PATsd, HR, HRsd, age, height, weight, (DBP_est)
        %     [sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,3:9) training_matrix_sitting_loso(:,2)], training_matrix_sitting_loso(:,1));
        %     [dbp_model_sitting_glm, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_loso(:,3:9)], training_matrix_sitting_loso(:,2));
        
        %sitting models GLM 7.0+
        %in: PWV(10), HR(5) age(7) weight(9) BMI (12) PWV*HR(10.*5) %out: SBP
        %[sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,10) training_matrix_sitting_loso(:,5) training_matrix_sitting_loso(:,7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12)], training_matrix_sitting_loso(:,1));
        [sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,10) training_matrix_sitting_loso(:,5) training_matrix_sitting_loso(:,7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12) (training_matrix_sitting_loso(:,10).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,1));
        
        %in: PWV, PWVsd (10:11), HR, HRsd, age (5:7) weight (9), BMI (12) (DBP_est) %out: SBP
        %with DBP for SBP and PATsd and HRsd
        %[sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,10:11) training_matrix_sitting_loso(:,5:7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12) training_matrix_sitting_loso(:,2)], training_matrix_sitting_loso(:,1));
        
        %in: PWV(10), HR(5) age(7) weight(9) BMI (12) PWV*HR(10.*5) %out: DBP
        [dbp_model_sitting_glm, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_loso(:,10) training_matrix_sitting_loso(:,5) training_matrix_sitting_loso(:,7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12) (training_matrix_sitting_loso(:,10).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,2));
        
        %save sitting RF models with all parameters per subject
        evalc([subject_id '_sbp_model_sitting = sbp_model_sitting']);
        evalc([subject_id '_dbp_model_sitting = dbp_model_sitting']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting'],'sbp_model_sitting','dbp_model_sitting', '-v7.3');
        
        %save sitting GLM models with all parameters per subject
        evalc([subject_id '_sbp_model_sitting_glm = sbp_model_sitting_glm']);
        evalc([subject_id '_dbp_model_sitting_glm = dbp_model_sitting_glm']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting_glm'],'sbp_model_sitting_glm','dbp_model_sitting_glm', '-v7.3');
        
        %sitting models no SSB
        sbp_model_sitting_no_ssb = TreeBagger(ntrees,[training_matrix_sitting_no_ssb_loso(:,3:6) training_matrix_sitting_no_ssb_loso(:,2)], training_matrix_sitting_no_ssb_loso(:,1),'Method','regression');%,'NVarToSample','all');
        dbp_model_sitting_no_ssb = TreeBagger(ntrees,[training_matrix_sitting_no_ssb_loso(:,3:6)], training_matrix_sitting_no_ssb_loso(:,2),'Method','regression');
        
        %sitting models GLM no SSB
        %in: PAT(3), HR(5) PAT*HR(3.*5) %out: SBP
        [sbp_model_sitting_glm_no_ssb, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,3) training_matrix_sitting_loso(:,5) (training_matrix_sitting_loso(:,3).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,1));
        %in: PAT(3), HR(5) PAT*HR(3.*5) %out: DBP
        [dbp_model_sitting_glm_no_ssb, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_loso(:,3) training_matrix_sitting_loso(:,5) (training_matrix_sitting_loso(:,3).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,2));
        
        %save sitting models without SSB per subject
        evalc([subject_id '_sbp_model_sitting_no_ssb = sbp_model_sitting_no_ssb']);
        evalc([subject_id '_dbp_model_sitting_no_ssb = dbp_model_sitting_no_ssb']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting_no_ssb'],'sbp_model_sitting_no_ssb','dbp_model_sitting_no_ssb', '-v7.3');
        
        %save sitting GLM models without SSB per subject
        evalc([subject_id '_sbp_model_sitting_glm_no_ssb = sbp_model_sitting_glm_no_ssb']);
        evalc([subject_id '_dbp_model_sitting_glm_no_ssb = dbp_model_sitting_glm_no_ssb']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting_glm_no_ssb'],'sbp_model_sitting_glm_no_ssb','dbp_model_sitting_glm_no_ssb', '-v7.3');
        
        %% Save individual CI SSB LOSO matrices for current subject
        
        %     CI_SSB_matrix_sitting_loso
        %     CI_SSB_matrix_standing_loso
        
        %sitting
        evalc([subject_id '_CI_SSB_matrix_sitting = CI_SSB_matrix_sitting_loso']);
        save([path_folder_current_subject '\' subject_id '_CI_SSB_matrix_sitting'],'CI_SSB_matrix_sitting_loso', '-v7.3');
        %standing
        %     evalc([subject_id '_CI_SSB_matrix_standing = CI_SSB_matrix_standing_loso']);
        %     save([path_folder_current_subject '\' subject_id '_CI_SSB_matrix_standing'],'CI_SSB_matrix_standing_loso');
        
    end
    
else %do not apply LOOCV (Code below mainly reused, potentially confusing)
    
    for subject_idx = 1:numel(all_subjects)
        
        subject = all_subjects(subject_idx);        
        
        %define loso subject_ID
        if subject < 10
            subject_id = strcat('SP000', num2str(subject));
        elseif (subject >= 10) && (subject < 100)
            subject_id = strcat('SP00', num2str(subject));
        elseif (subject >= 100) && (subject < 1000)
            subject_id = strcat('SP0', num2str(subject));
        elseif subject >= 1000
            subject_id = strcat('SP', num2str(subject));
        end
        
        disp(['Fill Matrix ',subject_id])
        
        foldername = subject_id;
        folder_struct = what(foldername);
        path_folder_current_subject = folder_struct.path;        
        
        
        %% Sitting Experiment
        
        %traning matrices for current NO loso subject (defined in beginning outside of loop!)
        
        %traning matrices for current NO loso subject
        training_matrix_sitting_no_loso_subject = [];
        training_matrix_sitting_no_ssb_no_loso_subject = [];
        %CI SSB matrix for current loso subject
        CI_SSB_matrix_sitting_no_loso_subject = [];        
        
        for exp_idx = 1:3
            
            data_file = ([subject_id, experiments{exp_idx},'.mat']);
            
            if ~exist(data_file) || (ismember(data_file(1:end-4), invalid_files{:})) %end-4 to delete '.mat' file ending
                
                %disp (['Missing Experiment Data ', subject_id, experiments{exp_idx}])
                
            else
                
                load(data_file);
                
                if ismember(data.datapool.signal, datapools_for_training)
                    
                    training_matrix_sitting_no_loso_subject = [training_matrix_sitting_no_loso_subject; ...
                        data.bp.sbpref, ...
                        data.bp.dbpref, ...
                        data.ppg.(chan).spotcheck.pat.signal(1), ...
                        data.ppg.(chan).spotcheck.pat.sd(1), ...
                        data.ppg.(chan).spotcheck.HR.signal(1), ...
                        data.ppg.(chan).spotcheck.HR.sd(1), ...
                        data.ssb.age.signal(1), ...
                        data.ssb.height.signal(1), ...
                        data.ssb.weight.signal(1), ...
                        data.ssb.height.signal(1)./data.ppg.(chan).spotcheck.pat.signal(1), ... %PWV via height
                        data.ssb.height.signal(1)./data.ppg.(chan).spotcheck.pat.sd(1), ... %PWVsd via height
                        data.ssb.weight.signal(1)./(data.ssb.height.signal(1)./100).^2]; %BMI
                    
                    
                    training_matrix_sitting_no_ssb_no_loso_subject = [training_matrix_sitting_no_ssb_no_loso_subject; ...
                        data.bp.sbpref, ...
                        data.bp.dbpref, ...
                        data.ppg.(chan).spotcheck.pat.signal(1), ...
                        data.ppg.(chan).spotcheck.pat.sd(1), ...
                        data.ppg.(chan).spotcheck.HR.signal(1), ...
                        data.ppg.(chan).spotcheck.HR.sd(1)];
                    
                    
                    CI_SSB_matrix_sitting_no_loso_subject = [CI_SSB_matrix_sitting_no_loso_subject; ...
                        str2num(subject_id(3:6)) ...
                        data.ssb.age.signal(1), ...
                        data.ssb.height.signal(1), ...
                        data.ssb.weight.signal(1), ...
                        data.bp.sbpref, ...
                        data.bp.dbpref, ...
                        data.ppg.(chan).spotcheck.pat.signal(1), ...
                        data.ppg.(chan).spotcheck.pat.sd(1), ...
                        data.ppg.(chan).spotcheck.CI(1), ...
                        data.ppg.(chan).spotcheck.HR.signal(1), ...
                        data.ppg.(chan).spotcheck.HR.sd(1)];
                    
                end
                
            end
        end
        
        %% Fill matrix for training without LOSO
        
        %fill data in loso training matrix
        training_matrix_sitting_no_loso = [training_matrix_sitting_no_loso; training_matrix_sitting_no_loso_subject]; %SBP DBP PAT PAT PATsd HR HRsd age height weight
        training_matrix_sitting_no_ssb_no_loso = [training_matrix_sitting_no_ssb_no_loso; training_matrix_sitting_no_ssb_no_loso_subject]; %SBP DBP PAT PAT PATsd HR HRsd
        
        
        %% Fill matrix with SSB for CI
        CI_SSB_matrix_sitting_no_loso = [CI_SSB_matrix_sitting_no_loso; CI_SSB_matrix_sitting_no_loso_subject];
        
    end
            
        %% Train and save individual !NOT-LOSO! model for current subject (always the same)
        
        %replace old variable to no_loso such that training code below can be used
        training_matrix_sitting_loso = training_matrix_sitting_no_loso;
        training_matrix_sitting_no_ssb_loso = training_matrix_sitting_no_ssb_no_loso;
        
        ntrees = 11; %11
        min_leaf = 5; %default for method 'regression'
        n_var_to_sample = 3; %default: sqrt of number of input variables
        
        %sitting models RF
        sbp_model_sitting = TreeBagger(ntrees,[training_matrix_sitting_loso(:,3:9) training_matrix_sitting_loso(:,2)], training_matrix_sitting_loso(:,1),'Method','regression','NVarToSample', n_var_to_sample, 'MinLeaf', min_leaf);
        dbp_model_sitting = TreeBagger(ntrees,[training_matrix_sitting_loso(:,3:9)], training_matrix_sitting_loso(:,2),'Method','regression','NVarToSample', n_var_to_sample, 'MinLeaf', min_leaf);
        
        %sitting models GLM 6.5 in: PAT, PATsd, HR, HRsd, age, height, weight, (DBP_est)
        %     [sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,3:9) training_matrix_sitting_loso(:,2)], training_matrix_sitting_loso(:,1));
        %     [dbp_model_sitting_glm, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_loso(:,3:9)], training_matrix_sitting_loso(:,2));
        
        %sitting models GLM 7.0+
        %in: PWV(10), HR(5) age(7) weight(9) BMI (12) PWV*HR(10.*5) %out: SBP
        %[sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,10) training_matrix_sitting_loso(:,5) training_matrix_sitting_loso(:,7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12)], training_matrix_sitting_loso(:,1));
        [sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,10) training_matrix_sitting_loso(:,5) training_matrix_sitting_loso(:,7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12) (training_matrix_sitting_loso(:,10).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,1));
        
        %in: PWV, PWVsd (10:11), HR, HRsd, age (5:7) weight (9), BMI (12) (DBP_est) %out: SBP
        %with DBP for SBP and PATsd and HRsd
        %[sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,10:11) training_matrix_sitting_loso(:,5:7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12) training_matrix_sitting_loso(:,2)], training_matrix_sitting_loso(:,1));
        
        %in: PWV(10), HR(5) age(7) weight(9) BMI (12) PWV*HR(10.*5) %out: DBP
        [dbp_model_sitting_glm, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_loso(:,10) training_matrix_sitting_loso(:,5) training_matrix_sitting_loso(:,7) training_matrix_sitting_loso(:,9) training_matrix_sitting_loso(:,12) (training_matrix_sitting_loso(:,10).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,2));
        
        %sitting models no SSB
        sbp_model_sitting_no_ssb = TreeBagger(ntrees,[training_matrix_sitting_no_ssb_loso(:,3:6) training_matrix_sitting_no_ssb_loso(:,2)], training_matrix_sitting_no_ssb_loso(:,1),'Method','regression');%,'NVarToSample','all');
        dbp_model_sitting_no_ssb = TreeBagger(ntrees,[training_matrix_sitting_no_ssb_loso(:,3:6)], training_matrix_sitting_no_ssb_loso(:,2),'Method','regression');
        
        %sitting models GLM no SSB
        %in: PAT(3), HR(5) PAT*HR(3.*5) %out: SBP
        [sbp_model_sitting_glm_no_ssb, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_loso(:,3) training_matrix_sitting_loso(:,5) (training_matrix_sitting_loso(:,3).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,1));
        %in: PAT(3), HR(5) PAT*HR(3.*5) %out: DBP
        [dbp_model_sitting_glm_no_ssb, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_loso(:,3) training_matrix_sitting_loso(:,5) (training_matrix_sitting_loso(:,3).*training_matrix_sitting_loso(:,5))], training_matrix_sitting_loso(:,2));
        
        %save individual !NOT-LOSO! model for current subject (always the same)
    for subject_idx = 1:numel(all_subjects)
        
        subject = all_subjects(subject_idx);
        
        %define loso subject_ID
        if subject < 10
            subject_id = strcat('SP000', num2str(subject));
        elseif (subject >= 10) && (subject < 100)
            subject_id = strcat('SP00', num2str(subject));
        elseif (subject >= 100) && (subject < 1000)
            subject_id = strcat('SP0', num2str(subject));
        elseif subject >= 1000
            subject_id = strcat('SP', num2str(subject));
        end
        
        disp(['Training ',subject_id])
        
        foldername = subject_id;
        folder_struct = what(foldername);
        path_folder_current_subject = folder_struct.path;         
        
        %save sitting models without SSB per subject
        evalc([subject_id '_sbp_model_sitting_no_ssb = sbp_model_sitting_no_ssb']);
        evalc([subject_id '_dbp_model_sitting_no_ssb = dbp_model_sitting_no_ssb']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting_no_ssb'],'sbp_model_sitting_no_ssb','dbp_model_sitting_no_ssb', '-v7.3');
        
        %save sitting GLM models without SSB per subject
        evalc([subject_id '_sbp_model_sitting_glm_no_ssb = sbp_model_sitting_glm_no_ssb']);
        evalc([subject_id '_dbp_model_sitting_glm_no_ssb = dbp_model_sitting_glm_no_ssb']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting_glm_no_ssb'],'sbp_model_sitting_glm_no_ssb','dbp_model_sitting_glm_no_ssb', '-v7.3');
        
        %save sitting RF models with all parameters per subject
        evalc([subject_id '_sbp_model_sitting = sbp_model_sitting']);
        evalc([subject_id '_dbp_model_sitting = dbp_model_sitting']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting'],'sbp_model_sitting','dbp_model_sitting', '-v7.3');
        
        %save sitting GLM models with all parameters per subject
        evalc([subject_id '_sbp_model_sitting_glm = sbp_model_sitting_glm']);
        evalc([subject_id '_dbp_model_sitting_glm = dbp_model_sitting_glm']);
        save([path_folder_current_subject '\' subject_id '_bp_models_sitting_glm'],'sbp_model_sitting_glm','dbp_model_sitting_glm', '-v7.3');        
        
        %% Save individual CI SSB LOSO matrices for current subject
                
        %sitting
        evalc([subject_id '_CI_SSB_matrix_sitting = CI_SSB_matrix_sitting_no_loso']);
        save([path_folder_current_subject '\' subject_id '_CI_SSB_matrix_sitting'],'CI_SSB_matrix_sitting_no_loso', '-v7.3');
        %standing
        %     evalc([subject_id '_CI_SSB_matrix_standing = CI_SSB_matrix_standing_loso']);
        %     save([path_folder_current_subject '\' subject_id '_CI_SSB_matrix_standing'],'CI_SSB_matrix_standing_loso');
        
    end
    training_matrix_sitting_full = training_matrix_sitting_loso;
    training_matrix_sitting_no_ssb_full = training_matrix_sitting_no_ssb_no_loso;
    
    
end

%% Train and save full model and training matrix

%%%cd(data_folder)

if apply_LOOCV == 1
    %merge training_matrix_loso (containing all training data except that of the last subject) and training_data_last_subject to full model training matrices
    training_matrix_sitting_full = [training_matrix_sitting_loso; training_matrix_sitting_loso_last_subject];
    training_matrix_sitting_no_ssb_full = [training_matrix_sitting_no_ssb_loso; training_matrix_sitting_no_ssb_loso_last_subject];
else
    training_matrix_sitting_full = training_matrix_sitting_no_loso;
    training_matrix_sitting_no_ssb_full = training_matrix_sitting_no_ssb_no_loso;  
end

% training_matrix_standing_full = [training_matrix_standing_loso; training_matrix_standing_loso_last_subject];
% training_matrix_standing_no_ssb_full = [training_matrix_standing_no_ssb_loso; training_matrix_standing_no_ssb_loso_last_subject];
 
% training_matrix_variation_full = [training_matrix_variation_loso; training_matrix_variation_loso_last_subject];
% training_matrix_variation_no_ssb_full = [training_matrix_variation_no_ssb_loso; training_matrix_variation_no_ssb_loso_last_subject];

%WITH SSB

%sitting models RF
ntrees=11;
%min_leaf = 2;

full_sbp_model_sitting = TreeBagger(ntrees,[training_matrix_sitting_full(:,3:9) training_matrix_sitting_full(:,2)], training_matrix_sitting_full(:,1),'Method','regression','NVarToSample',3, 'MinLeaf', min_leaf);
full_dbp_model_sitting = TreeBagger(ntrees,[training_matrix_sitting_full(:,3:9)], training_matrix_sitting_full(:,2),'Method','regression','NVarToSample',3 ,'MinLeaf', min_leaf);

%sitting models GLM 
%in: PWV(10), HR(5) age(7) weight(9) BMI (12) PWV*HR(10*5) %out: SBP
[full_sbp_model_sitting_glm, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_full(:,10) training_matrix_sitting_full(:,5) training_matrix_sitting_full(:,7) training_matrix_sitting_full(:,9) training_matrix_sitting_full(:,12) (training_matrix_sitting_full(:,10).*training_matrix_sitting_full(:,5))], training_matrix_sitting_full(:,1));

%in: PWV(10), HR(5) age(7) weight(9) BMI (12) PWV*HR(10*5) %out: DBP
[full_dbp_model_sitting_glm, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_full(:,10) training_matrix_sitting_full(:,5) training_matrix_sitting_full(:,7) training_matrix_sitting_full(:,9) training_matrix_sitting_full(:,12) (training_matrix_sitting_full(:,10).*training_matrix_sitting_full(:,5))], training_matrix_sitting_full(:,2));    

%save full sitting models with all parameters
full_sbp_model = full_sbp_model_sitting;
full_dbp_model = full_dbp_model_sitting;
save([path_folder_current_subject '\..\outcome_training\' 'full_bp_models_sitting'],'full_sbp_model','full_dbp_model', '-v7.3');

%save full sitting GLM models with all parameters
full_sbp_model = full_sbp_model_sitting_glm;
full_dbp_model = full_dbp_model_sitting_glm; 
save([path_folder_current_subject '\..\outcome_training\' 'full_bp_models_sitting_glm'], 'full_sbp_model', 'full_dbp_model', '-v7.3');

%NO SSB

%sitting models no SSB
full_sbp_model_sitting_no_ssb = TreeBagger(ntrees,[training_matrix_sitting_no_ssb_full(:,3:6) training_matrix_sitting_no_ssb_full(:,2)], training_matrix_sitting_no_ssb_full(:,1),'Method','regression');%,'NVarToSample','all');
full_dbp_model_sitting_no_ssb = TreeBagger(ntrees,[training_matrix_sitting_no_ssb_full(:,3:6)], training_matrix_sitting_no_ssb_full(:,2),'Method','regression');

%sitting models GLM no SSB 
%in: PAT(3), HR(5) PAT*HR(3*5) %out: SBP
[full_sbp_model_sitting_glm_no_ssb, dev_SBP, stats_SBP] = glmfit([training_matrix_sitting_full(:,3) training_matrix_sitting_full(:,5) (training_matrix_sitting_full(:,3).*training_matrix_sitting_full(:,5))], training_matrix_sitting_full(:,1));

%in: PAT(3), HR(5) PAT*HR(3*5) %out: DBP
[full_dbp_model_sitting_glm_no_ssb, dev_DBP, stats_DBP] = glmfit([training_matrix_sitting_full(:,3) training_matrix_sitting_full(:,5) (training_matrix_sitting_full(:,3).*training_matrix_sitting_full(:,5))], training_matrix_sitting_full(:,2));    

%save full sitting models without SSB
full_sbp_model = full_sbp_model_sitting_no_ssb;
full_dbp_model = full_dbp_model_sitting_no_ssb;
save([path_folder_current_subject '\..\outcome_training\' 'full_bp_models_sitting_no_ssb'],'full_sbp_model','full_dbp_model', '-v7.3');

%save full sitting GLM models without SSB
full_sbp_model = full_sbp_model_sitting_glm_no_ssb;
full_dbp_model = full_dbp_model_sitting_glm_no_ssb; 
save([path_folder_current_subject '\..\outcome_training\' 'full_bp_models_sitting_glm_no_ssb'], 'full_sbp_model', 'full_dbp_model', '-v7.3');

%save full sitting training matrix
save([path_folder_current_subject '\..\outcome_training\' 'training_matrix_sitting_full'], 'training_matrix_sitting_full', '-v7.3');

%% Fit and save coefficients (linear for now)

rows_without_nans = ~any(isnan(training_matrix_sitting_full),2);

%re-extract single vars from training matrix 
SBP_all_subjects_sitting = training_matrix_sitting_full(rows_without_nans,1);
DBP_all_subjects_sitting = training_matrix_sitting_full(rows_without_nans,2);
PAT_all_subjects_sitting = training_matrix_sitting_full(rows_without_nans,3);
PWV_all_subjects_sitting = training_matrix_sitting_full(rows_without_nans,10);
HR_all_subjects_sitting = training_matrix_sitting_full(rows_without_nans,5);

%linear fits

gen_lin_PAT_SBP_coeff = polyfit(PAT_all_subjects_sitting, SBP_all_subjects_sitting, 1);
gen_lin_PAT_SBP_fit = polyval(gen_lin_PAT_SBP_coeff, PAT_all_subjects_sitting);
gen_lin_PAT_SBP_function = @(PAT_all_subjects_sitting) gen_lin_PAT_SBP_coeff(1) * PAT_all_subjects_sitting + gen_lin_PAT_SBP_coeff(2);

gen_lin_PAT_DBP_coeff = polyfit(PAT_all_subjects_sitting, DBP_all_subjects_sitting, 1);
gen_lin_PAT_DBP_fit = polyval(gen_lin_PAT_DBP_coeff, PAT_all_subjects_sitting);
gen_lin_PAT_DBP_function = @(PAT_all_subjects_sitting) gen_lin_PAT_DBP_coeff(1) * PAT_all_subjects_sitting + gen_lin_PAT_DBP_coeff(2);

gen_lin_PAT_HR_SBP_coeff = polyfit(PAT_all_subjects_sitting.*HR_all_subjects_sitting, SBP_all_subjects_sitting, 1);
gen_lin_PAT_HR_SBP_fit = polyval(gen_lin_PAT_HR_SBP_coeff, PAT_all_subjects_sitting);
gen_lin_PAT_HR_SBP_function = @(PAT_all_subjects_sitting) gen_lin_PAT_HR_SBP_coeff(1).*PAT_all_subjects_sitting.*HR_all_subjects_sitting + gen_lin_PAT_HR_SBP_coeff(2);

gen_lin_PAT_HR_DBP_coeff = polyfit(PAT_all_subjects_sitting.*HR_all_subjects_sitting, DBP_all_subjects_sitting, 1);
gen_lin_PAT_HR_DBP_fit = polyval(gen_lin_PAT_HR_DBP_coeff, PAT_all_subjects_sitting);
gen_lin_PAT_HR_DBP_function = @(PAT_all_subjects_sitting) gen_lin_PAT_HR_DBP_coeff(1).*PAT_all_subjects_sitting.*HR_all_subjects_sitting + gen_lin_PAT_HR_DBP_coeff(2);

gen_lin_PAT_HR_coeff = polyfit(PAT_all_subjects_sitting, HR_all_subjects_sitting, 1);
gen_lin_PAT_HR_fit = polyval(gen_lin_PAT_HR_coeff, PAT_all_subjects_sitting);
gen_lin_PAT_HR_function = @(PAT_all_subjects_sitting) gen_lin_PAT_HR_coeff(1).*PAT_all_subjects_sitting + gen_lin_PAT_HR_SBP_coeff(2);

gen_lin_PWV_SBP_coeff = polyfit(PWV_all_subjects_sitting, SBP_all_subjects_sitting, 1);
gen_lin_PWV_SBP_fit = polyval(gen_lin_PWV_SBP_coeff, PWV_all_subjects_sitting);
gen_lin_PWV_SBP_function = @(PWV_all_subjects_sitting) gen_lin_PWV_SBP_coeff(1) * PWV_all_subjects_sitting + gen_lin_PWV_SBP_coeff(2);

gen_lin_PWV_DBP_coeff = polyfit(PWV_all_subjects_sitting, DBP_all_subjects_sitting, 1);
gen_lin_PWV_DBP_fit = polyval(gen_lin_PWV_DBP_coeff, PWV_all_subjects_sitting);
gen_lin_PWV_DBP_function = @(PWV_all_subjects_sitting) gen_lin_PWV_DBP_coeff(1) * PWV_all_subjects_sitting + gen_lin_PWV_DBP_coeff(2);

gen_lin_PWV_HR_SBP_coeff = polyfit(PWV_all_subjects_sitting.*HR_all_subjects_sitting, SBP_all_subjects_sitting, 1);
gen_lin_PWV_HR_SBP_fit = polyval(gen_lin_PWV_HR_SBP_coeff, PWV_all_subjects_sitting);
gen_lin_PWV_HR_SBP_function = @(PWV_all_subjects_sitting) gen_lin_PWV_HR_SBP_coeff(1).*PWV_all_subjects_sitting.*HR_all_subjects_sitting + gen_lin_PWV_HR_SBP_coeff(2);

gen_lin_PWV_HR_DBP_coeff = polyfit(PWV_all_subjects_sitting.*HR_all_subjects_sitting, DBP_all_subjects_sitting, 1);
gen_lin_PWV_HR_DBP_fit = polyval(gen_lin_PWV_HR_DBP_coeff, PWV_all_subjects_sitting);
gen_lin_PWV_HR_DBP_function = @(PWV_all_subjects_sitting) gen_lin_PWV_HR_DBP_coeff(1).*PWV_all_subjects_sitting.*HR_all_subjects_sitting + gen_lin_PWV_HR_DBP_coeff(2);

gen_lin_PWV_HR_coeff = polyfit(PWV_all_subjects_sitting, HR_all_subjects_sitting, 1);
gen_lin_PWV_HR_fit = polyval(gen_lin_PWV_HR_coeff, PWV_all_subjects_sitting);
gen_lin_PWV_HR_function = @(PWV_all_subjects_sitting) gen_lin_PWV_HR_coeff(1).*PWV_all_subjects_sitting + gen_lin_PWV_HR_coeff(2);

gen_lin_PWV_HR_DBP_coeff = polyfit(PWV_all_subjects_sitting.*HR_all_subjects_sitting, DBP_all_subjects_sitting, 1);
gen_lin_PWV_HR_DBP_fit = polyval(gen_lin_PWV_HR_DBP_coeff, PWV_all_subjects_sitting);
gen_lin_PWV_HR_DBP_function = @(PWV_all_subjects_sitting) gen_lin_PWV_HR_DBP_coeff(1).*PWV_all_subjects_sitting.*HR_all_subjects_sitting + gen_lin_PWV_HR_DBP_coeff(2);

gen_lin_HR_SBP_coeff = polyfit(HR_all_subjects_sitting, SBP_all_subjects_sitting, 1);
gen_lin_HR_SBP_fit = polyval(gen_lin_HR_SBP_coeff, HR_all_subjects_sitting);
gen_lin_HR_SBP_function = @(HR_all_subjects_sitting) gen_lin_HR_SBP_coeff(1) * HR_all_subjects_sitting + gen_lin_HR_SBP_coeff(2);

gen_lin_HR_DBP_coeff = polyfit(HR_all_subjects_sitting, DBP_all_subjects_sitting, 1);
gen_lin_HR_DBP_fit = polyval(gen_lin_HR_DBP_coeff, HR_all_subjects_sitting);
gen_lin_HR_DBP_function = @(HR_all_subjects_sitting) gen_lin_HR_DBP_coeff(1) * HR_all_subjects_sitting + gen_lin_HR_DBP_coeff(2);

%nonlinear (exponential) fits

%via 'fit' (depending on Matlab Statistics Toolbox)

%define custom equation
exp_fit = fittype('a*exp(b*x)+c');

[curve_PAT_orig_SBP_exp, gof_PAT_orig_SBP_exp] = fit(PAT_all_subjects_sitting,  SBP_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_PAT_SBP_coeff = coeffvalues(curve_PAT_orig_SBP_exp);
fun_gen_exp_PAT_SBP = @(PAT_all_subjects_sitting) gen_exp_PAT_SBP_coeff(1) * exp(gen_exp_PAT_SBP_coeff(2) * PAT_all_subjects_sitting) + gen_exp_PAT_SBP_coeff(3);

[curve_PAT_orig_DBP_exp, gof_PAT_orig_DBP_exp] = fit(PAT_all_subjects_sitting,  DBP_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_PAT_DBP_coeff = coeffvalues(curve_PAT_orig_DBP_exp);
fun_gen_exp_PAT_DBP = @(PAT_all_subjects_sitting) gen_exp_PAT_DBP_coeff(1) * exp(gen_exp_PAT_DBP_coeff(2) * PAT_all_subjects_sitting) + gen_exp_PAT_DBP_coeff(3);

[curve_PAT_orig_HR_exp, gof_PAT_orig_HR_exp] = fit(PAT_all_subjects_sitting,  HR_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_PAT_HR_coeff = coeffvalues(curve_PAT_orig_HR_exp);
fun_gen_exp_PAT_HR = @(PAT_all_subjects_sitting) gen_exp_PAT_HR_coeff(1) * exp(gen_exp_PAT_HR_coeff(2) * PAT_all_subjects_sitting) + gen_exp_PAT_HR_coeff(3);

[curve_PWV_orig_SBP_exp, gof_PWV_orig_SBP_exp] = fit(PWV_all_subjects_sitting,  SBP_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_PWV_SBP_coeff = coeffvalues(curve_PWV_orig_SBP_exp);
fun_gen_exp_PWV_SBP = @(PWV_all_subjects_sitting) gen_exp_PWV_SBP_coeff(1) * exp(gen_exp_PWV_SBP_coeff(2) * PWV_all_subjects_sitting) + gen_exp_PWV_SBP_coeff(3);

[curve_PWV_orig_DBP_exp, gof_PWV_orig_DBP_exp] = fit(PWV_all_subjects_sitting,  DBP_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_PWV_DBP_coeff = coeffvalues(curve_PWV_orig_DBP_exp);
fun_gen_exp_PWV_DBP = @(PWV_all_subjects_sitting) gen_exp_PWV_DBP_coeff(1) * exp(gen_exp_PWV_DBP_coeff(2) * PWV_all_subjects_sitting) + gen_exp_PWV_DBP_coeff(3);

[curve_PWV_orig_HR_exp, gof_PWV_orig_HR_exp] = fit(PWV_all_subjects_sitting,  HR_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_PWV_HR_coeff = coeffvalues(curve_PWV_orig_HR_exp);
fun_gen_exp_PWV_HR = @(PWV_all_subjects_sitting) gen_exp_PWV_HR_coeff(1) * exp(gen_exp_PWV_HR_coeff(2) * PWV_all_subjects_sitting) + gen_exp_PWV_HR_coeff(3);

[curve_HR_orig_SBP_exp, gof_HR_orig_SBP_exp] = fit(HR_all_subjects_sitting,  SBP_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_HR_SBP_coeff = coeffvalues(curve_HR_orig_SBP_exp);
fun_gen_exp_HR_SBP = @(HR_all_subjects_sitting) gen_exp_HR_SBP_coeff(1) * exp(gen_exp_HR_SBP_coeff(2) * HR_all_subjects_sitting) + gen_exp_HR_SBP_coeff(3);

[curve_HR_orig_DBP_exp, gof_HR_orig_DBP_exp] = fit(HR_all_subjects_sitting,  DBP_all_subjects_sitting, exp_fit, 'StartPoint', [0 0 0], 'Lower', [0 -1000 -1000], 'Upper', [10000 100 1000]);
gen_exp_HR_DBP_coeff = coeffvalues(curve_HR_orig_DBP_exp);
fun_gen_exp_HR_DBP = @(HR_all_subjects_sitting) gen_exp_HR_DBP_coeff(1) * exp(gen_exp_HR_DBP_coeff(2) * HR_all_subjects_sitting) + gen_exp_HR_DBP_coeff(3);

%     %via 'ezfit'
%     gen_exp_PAT_SBP = ezfit(PAT_all_subjects_sitting, SBP_all_subjects_sitting, 'exp')
%     gen_exp_PAT_SBP_coeff = gen_exp_PAT_SBP.m;
% 
%     gen_exp_PAT_DBP = ezfit(PAT_all_subjects_sitting, DBP_all_subjects_sitting, 'exp')
%     gen_exp_PAT_DBP_coeff = gen_exp_PAT_DBP.m;
% 
%     gen_exp_PAT_HR = ezfit(PAT_all_subjects_sitting, HR_all_subjects_sitting, 'exp')
%     gen_exp_PAT_HR_coeff = gen_exp_PAT_HR.m;
% 
%     gen_exp_PWV_SBP = ezfit(PWV_all_subjects_sitting, SBP_all_subjects_sitting, 'exp')
%     gen_exp_PWV_SBP_coeff = gen_exp_PWV_SBP.m;
% 
%     gen_exp_PWV_DBP = ezfit(PWV_all_subjects_sitting, DBP_all_subjects_sitting, 'exp')
%     gen_exp_PWV_DBP_coeff = gen_exp_PWV_DBP.m;
% 
%     gen_exp_PWV_HR = ezfit(PWV_all_subjects_sitting, HR_all_subjects_sitting, 'exp')
%     gen_exp_PWV_HR_coeff = gen_exp_PWV_HR.m;
% 
%     gen_exp_HR_SBP = ezfit(HR_all_subjects_sitting, SBP_all_subjects_sitting, 'exp')
%     gen_exp_HR_SBP_coeff = gen_exp_HR_SBP.m;
% 
%     gen_exp_HR_DBP = ezfit(HR_all_subjects_sitting, DBP_all_subjects_sitting, 'exp')
%     gen_exp_HR_DBP_coeff = gen_exp_HR_DBP.m;


%save coefficients for tracking (also later in BP model folder together with full models!)
%linear
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PAT_SBP_coeff.mat'], 'gen_lin_PAT_SBP_coeff');
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PAT_DBP_coeff.mat'], 'gen_lin_PAT_DBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PAT_HR_SBP_coeff.mat'], 'gen_lin_PAT_HR_SBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PAT_HR_DBP_coeff.mat'], 'gen_lin_PAT_HR_DBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PWV_SBP_coeff.mat'], 'gen_lin_PWV_SBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PWV_DBP_coeff.mat'], 'gen_lin_PWV_DBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PWV_HR_SBP_coeff.mat'], 'gen_lin_PWV_HR_SBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_PWV_HR_DBP_coeff.mat'], 'gen_lin_PWV_HR_DBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_HR_SBP_coeff.mat'], 'gen_lin_HR_SBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_lin_HR_DBP_coeff.mat'], 'gen_lin_HR_DBP_coeff'); 
%exponential
save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PAT_SBP_coeff.mat'], 'gen_exp_PAT_SBP_coeff');
save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PAT_DBP_coeff.mat'], 'gen_exp_PAT_DBP_coeff'); 
%save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PAT_HR_SBP_coeff.mat'], 'gen_exp_PAT_HR_SBP_coeff'); 
%save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PAT_HR_DBP_coeff.mat'], 'gen_exp_PAT_HR_DBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PWV_SBP_coeff.mat'], 'gen_exp_PWV_SBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PWV_DBP_coeff.mat'], 'gen_exp_PWV_DBP_coeff'); 
%save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PWV_HR_SBP_coeff.mat'], 'gen_exp_PWV_HR_SBP_coeff'); 
%save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_PWV_HR_DBP_coeff.mat'], 'gen_exp_PWV_HR_DBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_HR_SBP_coeff.mat'], 'gen_exp_HR_SBP_coeff'); 
save([path_folder_current_subject '\..\outcome_training\' 'gen_exp_HR_DBP_coeff.mat'], 'gen_exp_HR_DBP_coeff'); 


fig_fittings = figure('Name', 'Fittings'); hold on;

subplot(3,3,1), hold on;
%fig_fittings_PAT_SBP = figure('Name', 'PAT-SBP'); hold on;
scatter(PAT_all_subjects_sitting, SBP_all_subjects_sitting);
plot(PAT_all_subjects_sitting, gen_lin_PAT_SBP_fit);
fplot(fun_gen_exp_PAT_SBP, [min(PAT_all_subjects_sitting), max(PAT_all_subjects_sitting)])
%showfit(gen_exp_PAT_SBP, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('PAT');
ylabel('SBP');
title('PAT-SBP');

subplot(3,3,2), hold on;
%fig_fittings_PAT_DBP = figure('Name', 'PAT-DBP'); hold on;
scatter(PAT_all_subjects_sitting, DBP_all_subjects_sitting);
plot(PAT_all_subjects_sitting, gen_lin_PAT_DBP_fit);
fplot(fun_gen_exp_PAT_DBP, [min(PAT_all_subjects_sitting), max(PAT_all_subjects_sitting)])
%fplot(fun_gen_exp_PAT_DBP, [-10, 10])
%showfit(gen_exp_PAT_DBP, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('PAT');
ylabel('DBP');
title('PAT-DBP');

subplot(3,3,3), hold on;
%fig_fittings_PAT_HR = figure('Name', 'PAT-HR'); hold on;
scatter(PAT_all_subjects_sitting, HR_all_subjects_sitting);
plot(PAT_all_subjects_sitting, gen_lin_PAT_HR_fit);
fplot(fun_gen_exp_PAT_HR, [min(PAT_all_subjects_sitting), max(PAT_all_subjects_sitting)])
%fplot(fun_gen_exp_PAT_HR, [-10, 10])
%showfit(gen_exp_PAT_HR, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('PAT');
ylabel('HR');
title('PAT-HR');

subplot(3,3,4), hold on;
%fig_fittings_PWV_SBP = figure('Name', 'PWV-SBP'); hold on;
scatter(PWV_all_subjects_sitting, SBP_all_subjects_sitting);
plot(PWV_all_subjects_sitting, gen_lin_PWV_SBP_fit);
fplot(fun_gen_exp_PWV_SBP, [min(PWV_all_subjects_sitting), max(PWV_all_subjects_sitting)])
%showfit(gen_exp_PWV_SBP, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('PWV');
ylabel('SBP');
title('PWV-SBP');

subplot(3,3,5), hold on;
%fig_fittings_PWV_DBP = figure('Name', 'PWV-DBP'); hold on;
scatter(PWV_all_subjects_sitting, DBP_all_subjects_sitting);
plot(PWV_all_subjects_sitting, gen_lin_PWV_DBP_fit);
fplot(fun_gen_exp_PWV_DBP, [min(PWV_all_subjects_sitting), max(PWV_all_subjects_sitting)])
%fplot(fun_gen_exp_PWV_DBP, [-10, 10])
%showfit(gen_exp_PWV_DBP, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('PWV');
ylabel('DBP');
title('PWV-DBP');

subplot(3,3,6), hold on;
%fig_fittings_PWV_HR = figure('Name', 'PWV-HR'); hold on;
scatter(PWV_all_subjects_sitting, HR_all_subjects_sitting);
plot(PWV_all_subjects_sitting, gen_lin_PWV_HR_fit);
fplot(fun_gen_exp_PWV_HR, [min(PWV_all_subjects_sitting), max(PWV_all_subjects_sitting)])
%fplot(fun_gen_exp_PWV_HR, [-10, 10])
%showfit(gen_exp_PWV_HR, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('PWV');
ylabel('HR');
title('PWV-HR');

subplot(3,3,7), hold on;
%fig_fittings_HR_SBP = figure('Name', 'HR-SBP'); hold on;
scatter(HR_all_subjects_sitting, SBP_all_subjects_sitting);
plot(HR_all_subjects_sitting, gen_lin_HR_SBP_fit);
fplot(fun_gen_exp_HR_SBP, [min(HR_all_subjects_sitting), max(HR_all_subjects_sitting)])
%showfit(gen_exp_HR_SBP, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('HR');
ylabel('SBP');
title('HR-SBP');

subplot(3,3,8), hold on;
%fig_fittings_HR_DBP = figure('Name', 'HR-DBP'); hold on;
scatter(HR_all_subjects_sitting, DBP_all_subjects_sitting);
plot(HR_all_subjects_sitting, gen_lin_HR_DBP_fit);
fplot(fun_gen_exp_HR_DBP, [min(HR_all_subjects_sitting), max(HR_all_subjects_sitting)])
%fplot(fun_gen_exp_HR_DBP, [-10, 10])
%showfit(gen_exp_HR_DBP, 'fitcolor', 'red', 'fitlinewidth', 2);
xlabel('HR');
ylabel('DBP');
title('HR-DBP');