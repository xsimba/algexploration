% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : BP.m
%  Content    : Blood Pressure Computation FOR TRAINING AND VALIDATING MODELS
%  Package    : SIMBA.Validation
%  Created by : F. Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 12.9.14
%  Modification and Version History:
%  | Developer | Version |    Date    |     Changes      |
%  |  fbeutel  |   1.0   | 13-05-2014 | Code Refactoring |
%  |  ECWentink|   1.1   | 01-09-2014 | Generelize code  |
%  |  fbeutel  |   2.0   | 12-09-2014 | Align with data structure  |
%  |  fbeutel  |   2.1   | 16-09-2014 | Include SSB input  |
%  |  fbeutel  |   3.0   | 08-10-2014 | SSB read from input data,
%                                       Switch models based on availability SSB and posture
%                                       Use latest models
%                                       Second input argument for training mode |
%  |  fbeutel  |   3.1   | 10-10-2014 | Cleanup for implementation
%                                       (Integration of latest models,
%                                       Switch models dependent on case (availability ssb and context)
%                                       Deletion of individual models)
%  |  fbeutel  |   7.7   | 10-10-2014 | PURE TRAINING AND VALIDATION VERSION |
%
%  |  fbeutel  |   8.0   | 27-10-2014 | PURE TRAINING AND VALIDATION VERSION (now also with BP CI) |
%
%  |  fbeutel  |   9.0   | 16-12-2014 | Same as 8.0 but now for BP model v6.0|

%  |  fbeutel  |   10.0   | 28-01-2015 | Same as 9.0 added GLM|

%  |  fbeutel  |   11.0   | 23-04-2015 | Improve flexibility to use different channels |

%  |  fbeutel  |   12.0   | 28-04-2015 | Calibration and tracking functionality |
%
%
%  |  fbeutel  |   13.0   | 28-07-2015 | Compatibility with featureExtraction and latest BP.m version (0.8.1)|
%
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function [output] = BP_validation(input, chan, varargin)

output = input;
chan = chan(end);

switch nargin     
    case 3
        reference_time = varargin{1}; %reference date: either (array of) day(s), date(s), -1 of all files except current, etc. (see below)
        reference_context.daytime = {};
    case 4
        reference_time = varargin{1}; %reference date: either (array of) day(s), date(s), -1 of all files except current, etc. (see below)
        reference_context.daytime = varargin{2}; %reference context: cellstr with 'M', 'L' or 'A' for morning, lunch or afternoon
    otherwise
        reference_time = NaN;
        reference_context.daytime = {};       
end

%check whether reference time is (array of) day or date or negative
if isnan(reference_time(1))
    reference_time_type = 'n/a --> no calibration';
elseif (reference_time(1) == -1)
    reference_time_type = 'all_excl_current';
elseif reference_time(1) == -2
   reference_time_type = 'all_incl_current';   
elseif reference_time(1) == -3
   reference_time_type = 'latest_calibration';      
elseif reference_time(1) == 0
   reference_time_type = 'current';   
elseif reference_time(1) < 20150000
    reference_time_type = 'day'; %or trivial numbering
elseif reference_time(1) > 20150000     
    reference_time_type = 'date';
end

    %check whether reference time is single instance or array (only when reference_time_type is day or date)
    if strcmp(reference_time_type, 'day') || strcmp(reference_time_type, 'date')
        if numel(reference_time) ~= 1
            reference_time_type = [reference_time_type '_array'];
        else
            reference_time_type = [reference_time_type '_instance'];
        end
    end

    disp(['BP tracked with respect to: ' reference_time_type]);

    %check whether reference context is specified
    %define possible outcomes
    daytime_outcomes = {'M', 'L', 'A'};

    if isempty(reference_context.daytime)
        disp('No reference context specified, all daytimes will be considered.')
        reference_context.daytime = {'M', 'L', 'A'};
    elseif ismember(reference_context.daytime, daytime_outcomes) %check for correct outcome values
        reference_context.daytime = reference_context.daytime(ismember(reference_context.daytime, daytime_outcomes));
        disp('Reference context specified to:'); disp(char(reference_context.daytime));
    end

%% Load BP model

subject_id = input.subject_id;

% BP is computed for all LOSO models known to be available, so there is no testing for presence of SSB or context variables

if strcmp(input.context.posture.signal{1},'sitting')
    %% Sitting LOSO
    
%%%    cd(subject_folder)
    load([subject_id '_bp_models_sitting.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp_loso, output.ppg.(chan).bp.dbp_loso_CInt] = predict(dbp_model_sitting,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)']);
    
    [output.ppg.(chan).bp.sbp_loso, output.ppg.(chan).bp.sbp_loso_CInt] = predict(sbp_model_sitting,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)' ...
        output.ppg.(chan).bp.dbp_loso]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1_loso, output.ppg.(chan).bp.dbp1_loso_CInt] = predict(dbp_model_sitting,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))']);
    %
    %     [output.ppg.(chan).bp.sbp1_loso, output.ppg.(chan).bp.sbp1_loso_CInt] = predict(sbp_model_sitting,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         output.ppg.(chan).bp.dbp1_loso]);
    
    %disp(['The RF LOSO SBP is: ' num2str(output.ppg.(chan).bp.sbp_loso) ' and the RF LOSO DBP is: ' num2str(output.ppg.(chan).bp.dbp_loso)])
    
    %No SSB
    
    load([subject_id '_bp_models_sitting_no_ssb.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp_loso_no_ssb, output.ppg.(chan).bp.dbp_loso_no_ssb_CInt] = predict(dbp_model_sitting_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.sd(1)']);
    
    [output.ppg.(chan).bp.sbp_loso_no_ssb, output.ppg.(chan).bp.sbp_loso_no_ssb_CInt] = predict(sbp_model_sitting_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        output.ppg.(chan).bp.dbp_loso_no_ssb]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1_loso_no_ssb, output.ppg.(chan).bp.dbp1_loso_no_ssb_CInt] = predict(dbp_model_sitting_no_ssb,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1']);
    %
    %     [output.ppg.(chan).bp.sbp1_loso_no_ssb, output.ppg.(chan).bp.sbp1_loso_no_ssb_CInt] = predict(sbp_model_sitting_no_ssb,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         output.ppg.(chan).bp.dbp1_loso_no_ssb]);
    
    %disp(['The RF LOSO SBP without SSB is: ' num2str(output.ppg.(chan).bp.sbp_loso_no_ssb) ' and the RF LOSO DBP without SSB is: ' num2str(output.ppg.(chan).bp.dbp_loso_no_ssb) ])
    
    
    %% Sitting LOSO CI
    load([subject_id '_CI_SSB_matrix_sitting.mat'])
    
    if ~exist('CI_SSB_matrix_sitting_loso.mat')       
        CI_SSB_matrix_sitting_loso = CI_SSB_matrix_sitting_no_loso;
    end

    %%%cd(BP_framework_folder)
    %age
    
    %normalization and median computation from all subjects (excl current subject)
    age_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,2));
    median_age_norm_loso = median(age_norm_loso);
    age_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,2); input.ssb.age.signal(1)]);
    %compute absolute normalized distance to median
    age_norm_distance = abs(age_norm_all(end) - median_age_norm_loso);
    
    output.ssb.age_norm = age_norm_all(end);
    output.ssb.age_norm_distance = age_norm_distance;
    
    
    %height
    
    %normalization and median computation from all subjects (excl current subject)
    height_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,3));
    median_height_norm_loso = nanmedian(height_norm_loso);
    height_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,3); input.ssb.height.signal(1)]);
    %compute absolute normalized distance to median
    height_norm_distance = abs(height_norm_all(end) - median_height_norm_loso);
    
    output.ssb.height_norm = height_norm_all(end);
    output.ssb.height_norm_distance = height_norm_distance;
    
    %weight
    
    %normalization and median computation from all subjects (excl current subject)
    weight_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,4));
    median_weight_norm_loso = nanmedian(weight_norm_loso);
    weight_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,4); input.ssb.weight.signal(1)]);
    %compute absolute normalized distance to median
    weight_norm_distance = abs(weight_norm_all(end) - median_weight_norm_loso);
    
    output.ssb.weight_norm = weight_norm_all(end);
    output.ssb.weight_norm_distance = weight_norm_distance;
    
    %PAT
    
    %normalization and median computation from all subjects (excl current subject)
    PAT_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,7));
    median_PAT_norm_loso = nanmedian(PAT_norm_loso);
    PAT_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,7); input.ppg.(chan).spotcheck.pat.signal(1)]);
    %compute absolute normalized distance to median
    PAT_norm_distance = abs(PAT_norm_all(end) - median_PAT_norm_loso);
    
    output.ppg.(chan).spotcheck.pat_norm = PAT_norm_all(end);
    output.ppg.(chan).spotcheck.pat_norm_distance = PAT_norm_distance;
    
    %PATSD
    
    %normalization and median computation from all subjects (excl current subject)
    PATsd_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,8));
    median_PATsd_norm_loso = nanmedian(PATsd_norm_loso);
    PATsd_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,8); input.ppg.(chan).spotcheck.pat.sd(1)]);
    %compute absolute normalized distance to median
    PATsd_norm_distance = abs(PATsd_norm_all(end) - median_PATsd_norm_loso);
    
    output.ppg.(chan).spotcheck.patSD_norm = PATsd_norm_all(end);
    output.ppg.(chan).spotcheck.patSD_norm_distance = PATsd_norm_distance;
    
    %HR
    
    %normalization and median computation from all subjects (excl current subject)
    HR_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,10));
    median_HR_norm_loso = nanmedian(HR_norm_loso);
    HR_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,10); input.ppg.(chan).spotcheck.HR.signal(1)]);
    %compute absolute normalized distance to median
    HR_norm_distance = abs(HR_norm_all(end) - median_HR_norm_loso);
    
    output.ppg.(chan).spotcheck.HR_norm = HR_norm_all(end);
    output.ppg.(chan).spotcheck.HR_norm_distance = HR_norm_distance;
    
    %HRSD
    
    %normalization and median computation from all subjects (excl current subject)
    HRSD_norm_loso = normalize_array(CI_SSB_matrix_sitting_loso(:,11));
    median_HRSD_norm_loso = nanmedian(HRSD_norm_loso);
    HRSD_norm_all = normalize_array([CI_SSB_matrix_sitting_loso(:,11); input.ppg.(chan).spotcheck.HR.sd(1)]);
    %compute absolute normalized distance to median
    HRSD_norm_distance = abs(HRSD_norm_all(end) - median_HRSD_norm_loso);
    
    output.ppg.(chan).spotcheck.HRSD_norm = HR_norm_all(end);
    output.ppg.(chan).spotcheck.HRSD_norm_distance = HR_norm_distance;
    
    %% Sitting LOSO GLM
    
    %%%cd(subject_folder)
    load([subject_id '_bp_models_sitting_glm.mat'])     

%   From BPv7.0   
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_loso_glm = glmval(dbp_model_sitting_glm,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)', ...
        ((input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1)).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity'); %PWV*HR

    
    output.ppg.(chan).bp.sbp_loso_glm = glmval(sbp_model_sitting_glm,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ... 
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)', ...
        ((input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1)).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity'); %PWV*HR
    
    %No SSB
    
    load([subject_id '_bp_models_sitting_glm_no_ssb.mat'])
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_loso_glm_no_ssb = glmval(dbp_model_sitting_glm_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        (input.ppg.(chan).spotcheck.pat.signal(1).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity'); 

    output.ppg.(chan).bp.sbp_loso_glm_no_ssb = glmval(sbp_model_sitting_glm_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        (input.ppg.(chan).spotcheck.pat.signal(1).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity');     
    
% Left out variables:    
%        input.ppg.(chan).pat.HR_sd_for_BP.signal(1)', ...       
%         (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)' ... %BMI
%         output.ppg.(chan).bp.dbp_loso_glm], 'identity');
%        input.ppg.(chan).pat.HR_sd_for_BP.signal(1)', ...
    
%   Until BPv6.7

%     %for single PAT/BP
%     output.ppg.(chan).bp.dbp_loso_glm = glmval(dbp_model_sitting_glm,...
%         [input.ppg.(chan).pat.pat_avg_for_BP.signal(1)', ...
%         input.ppg.(chan).pat.pat_sd_for_BP.signal(1)', ...
%         input.ppg.(chan).pat.HR_avg_for_BP.signal(1)', ...
%         input.ppg.(chan).pat.HR_sd_for_BP.signal(1)', ...
%         input.ssb.age.signal(1)' ...
%         input.ssb.height.signal(1)' ...
%         input.ssb.weight.signal(1)'], 'identity');
%     
%     output.ppg.(chan).bp.sbp_loso_glm = glmval(sbp_model_sitting_glm,...
%         [input.ppg.(chan).pat.pat_avg_for_BP.signal(1)', ...
%         input.ppg.(chan).pat.pat_sd_for_BP.signal(1)', ...
%         input.ppg.(chan).pat.HR_avg_for_BP.signal(1)', ...
%         input.ppg.(chan).pat.HR_sd_for_BP.signal(1)', ...
%         input.ssb.age.signal(1)' ...
%         input.ssb.height.signal(1)' ...
%         input.ssb.weight.signal(1)' ...
%         output.ppg.(chan).bp.dbp_loso_glm], 'identity');


%% BP Tracker LOSO GLM

%Set references from this file/spotcheck for future files/spotchecks

    output.ppg.(chan).bp.tracker.this_reference.bp_est.sbp.signal = output.ppg.(chan).bp.sbp_loso_glm(1); %in BP.m 'output.ppg.(chan).ppg.(chan).bp.sbp_loso_glm;'
    output.ppg.(chan).bp.tracker.this_reference.bp_est.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
    
    output.ppg.(chan).bp.tracker.this_reference.bp_est.dbp.signal = output.ppg.(chan).bp.dbp_loso_glm(1);
    output.ppg.(chan).bp.tracker.this_reference.bp_est.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
    
    output.ppg.(chan).bp.tracker.this_reference.pat.signal = output.ppg.(chan).spotcheck.pat.signal(1);
    output.ppg.(chan).bp.tracker.this_reference.pat.timestamps = output.ppg.(chan).spotcheck.timestamps(1);
    
    output.ppg.(chan).bp.tracker.this_reference.pwv.signal = input.ssb.height.signal(1)./output.ppg.(chan).spotcheck.pat.signal(1);
    output.ppg.(chan).bp.tracker.this_reference.pwv.timestamps = output.ppg.(chan).spotcheck.timestamps(1);     
    
    output.ppg.(chan).bp.tracker.this_reference.HR.signal = output.ppg.(chan).spotcheck.HR.signal(1);
    output.ppg.(chan).bp.tracker.this_reference.HR.timestamps = output.ppg.(chan).spotcheck.timestamps(1);    
    
    if isfield(output, 'bp') %ppg independent bp field in root of data struct, containing cuff reference
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal = output.bp.sbpref(1);
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
        
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.signal = output.bp.dbpref(1);
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
    else
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal = NaN;
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.timestamps = NaN; %i ipv 1 in BP.m
        
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.signal = NaN;
        output.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.timestamps = NaN; %i ipv 1 in BP.m
    end

%Load coefficients for tracking
   
    %linear
    load('gen_lin_PAT_SBP_coeff.mat');
    load('gen_lin_PAT_DBP_coeff.mat');
    load('gen_lin_PAT_HR_coeff.mat');
    load('gen_lin_PWV_SBP_coeff.mat');
    load('gen_lin_PWV_DBP_coeff.mat');
    load('gen_lin_PWV_HR_coeff.mat');
    load('gen_lin_HR_SBP_coeff.mat');
    load('gen_lin_HR_DBP_coeff.mat');

    %exponential
    load('gen_exp_PAT_SBP_coeff.mat');
    load('gen_exp_PAT_DBP_coeff.mat');
    load('gen_exp_PAT_HR_coeff.mat');
    load('gen_exp_PWV_SBP_coeff.mat');
    load('gen_exp_PWV_DBP_coeff.mat');
    load('gen_exp_PWV_HR_coeff.mat');
    load('gen_exp_HR_SBP_coeff.mat');
    load('gen_exp_HR_DBP_coeff.mat');
    

%Search specified files/spotchecks for tracking reference in current database

    %search files on search path/current database from same subject
    subject_id_files = what(subject_id); %'what' only works when subject id is folder name where files are located!
    subject_id_all_mat_files = subject_id_files.mat;
    
    %search indices of files for certain experiments
    subject_id_file_indices_large_scale_sitting = [];
    subject_id_file_indices_consistency = [];
    
    for j = 1:numel(subject_id_all_mat_files)
        %large scale data collection sitting
        search_results_large_scale_sitting = findstr(subject_id_all_mat_files{j}, '_S_SI_');
        if ~isempty(search_results_large_scale_sitting)
            subject_id_file_indices_large_scale_sitting = [subject_id_file_indices_large_scale_sitting, j];
        end
        %consistency experiment
        search_results_consistency = findstr(subject_id_all_mat_files{j}, '_C_');
        if ~isempty(search_results_consistency)
            subject_id_file_indices_consistency = [subject_id_file_indices_consistency, j];
        end   
    end
    
    subject_id_mat_files_large_scale_sitting = subject_id_all_mat_files(subject_id_file_indices_large_scale_sitting);
    subject_id_mat_files_consistency = subject_id_all_mat_files(subject_id_file_indices_consistency);
    
    %all relevant files from subject (change to specific experiment type if desired)
    files_for_reference_preselection = [subject_id_mat_files_large_scale_sitting; subject_id_mat_files_consistency];
        
    %iterate over files to select and accumulate references and compute offset
    
        %define arrays to accumulate references
        %estimation based
        ref_sbp_cal = [];
        ref_dbp_cal = []; 
        %calibration based
        ref_sbp_est = [];
        ref_dbp_est = [];
        %generic
        ref_pat = [];
        ref_pwv = [];
        ref_HR = [];
              
        %select files for specified time
        time_selection = [];
        if strcmp(reference_time_type, 'n/a --> no calibration');
            files_for_reference_time_selected = [];
            
        elseif strcmp(reference_time_type, 'all_incl_current')
            files_for_reference_time_selected = files_for_reference_preselection;
            
        elseif strcmp(reference_time_type, 'all_excl_current')
            files_for_reference_time_selected = setdiff(files_for_reference_preselection, input.filename);
            
        elseif strcmp(reference_time_type, 'current')
            files_for_reference_time_selected = input.filename;
            
        elseif strcmp(reference_time_type, 'latest_calibration') %for now: latest calibration date, no time specified (daytime can be specified by context if desired!)

            %iterate over files to find dates
            calibration_dates = [];
            calibration_file_indices = [];
            for j = 1:numel(files_for_reference_preselection)  
                current_date = [];
                load(files_for_reference_preselection{j}) %files can simply be loaded in function since struct called 'data' is not handled here    
                if isfield(data.ppg.(chan).bp, 'tracker')
                    if ~isempty(data.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal)
                        if isfield (data.context, 'date')
                            current_date = data.context.date;
                        elseif isfield(data, 'startUnixTime')
                            current_date = UnixTimeToDateStr(data.startUnixTime);
                        else
                            current_date = UnixTimeToDateStr(0);
                        end
                    end
                end
                if ~isempty(current_date)
                    calibration_dates = [calibration_dates, str2num(current_date)];
                    calibration_file_indices = [calibration_file_indices, j];
                end
            end
                
            time_selection = calibration_file_indices(calibration_dates == max(calibration_dates));
            
            if isempty(time_selection)
                disp('Latest calibration not found.')
                files_for_reference_time_selected = [];
            else
                files_for_reference_time_selected = files_for_reference_preselection(time_selection);
            end
            
        elseif strcmp(reference_time_type, 'day_instance') || strcmp(reference_time_type, 'day_array')
            
            %iterate over all files to find day instance(s from array) 
            time_selection = []; %here: files wherein specified days are found
            for j = 1:numel(files_for_reference_preselection)  
                load(files_for_reference_preselection{j}) %files can simply be loaded in function since struct called 'data' is not handled here    
                if isfield(data.context, 'day')     
                    if ismember(data.context.day, reference_time)
                        time_selection = [time_selection, j];
                    end      
                end
            end
            if isempty(time_selection)
                disp('Specified day(s) not found.')
                files_for_reference_time_selected = [];
            else
                files_for_reference_time_selected = files_for_reference_preselection(time_selection);
            end
            
        elseif strcmp(reference_time_type, 'date_instance') || strcmp(reference_time_type, 'date_array')
            
            %iterate over all files to find date instance(s from array) 
            time_selection = []; %here: files wherein specified dates are found
            for j = 1:numel(files_for_reference_preselection)  
                load(files_for_reference_preselection{j}) %files can simply be loaded in function since struct called 'data' is not handled here    
                if isfield(data.context, 'date')     
                    if ismember(data.context.date, reference_time)
                        time_selection = [time_selection, j];
                    end      
                end
            end
            if isempty(time_selection)
                disp('Specified date(s) not found.')
                files_for_reference_time_selected = [];
            else
                files_for_reference_time_selected = files_for_reference_preselection(time_selection);
            end            
           
        end

        %select only files that fullfill reference context conditions
        context_selection = [];
        if ~isempty(files_for_reference_time_selected)
            if ~isnan(reference_context.daytime{1})
                for j = 1:numel(files_for_reference_time_selected)
                    load(files_for_reference_time_selected{j}) %files can simply be loaded in function since struct called 'data' is not handled here
                    if isfield(data.context, 'daytime')
                        if ismember(data.context.daytime, reference_context.daytime)
                            context_selection = [context_selection, j];
                        end 
                    end
                end
                if ~isempty(context_selection)
                    files_for_reference_time_and_context_selected = files_for_reference_time_selected(context_selection);
                else
                    files_for_reference_time_and_context_selected = files_for_reference_time_selected;
                end
            else %if no context is specified
                files_for_reference_time_and_context_selected = files_for_reference_time_selected;
            end
        else
            files_for_reference_time_and_context_selected = [];
        end
        
        %rename to short var
        files_for_reference = files_for_reference_time_and_context_selected;
        
        if isempty(files_for_reference)
            disp('No reference file as specified could be found --> No tracking results computed.')
            
            %assig NaNs to outcome vectors
            
            %pat_based
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.sbp.signal = NaN;
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); 
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.dbp.signal = NaN;
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); 
            %pwv_based
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.sbp.signal = NaN;
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);  
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.dbp.signal = NaN;
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);               
            %pat_based
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.sbp.signal = NaN;
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);  
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.dbp.signal = NaN;
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);              
            %pwv_based
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.sbp.signal = NaN;
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);           
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.dbp.signal = NaN;
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);             
            
        else
          
    %Define tracking references            
        for j = 1:numel(files_for_reference)
            load(files_for_reference{j}) %files can simply be loaded in function since struct called 'data' is not handled here
            %fill reference arrays if tracker references are available
            if isfield(data.ppg.(chan).bp, 'tracker')                   
                %estimation based
                ref_sbp_cal = [ref_sbp_cal, data.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal];
                ref_dbp_cal = [ref_dbp_cal, data.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.signal];
                %calibration based
                ref_sbp_est = [ref_sbp_est, data.ppg.(chan).bp.tracker.this_reference.bp_est.sbp.signal];
                ref_dbp_est = [ref_dbp_est, data.ppg.(chan).bp.tracker.this_reference.bp_est.dbp.signal];  
                %generic
                ref_pat = [ref_pat, data.ppg.(chan).bp.tracker.this_reference.pat.signal];
                ref_pwv = [ref_pwv, data.ppg.(chan).bp.tracker.this_reference.pwv.signal];
                ref_HR = [ref_HR, data.ppg.(chan).bp.tracker.this_reference.HR.signal];                   
            else
                %estimation based
                ref_sbp_cal = [ref_sbp_cal, NaN];
                ref_dbp_cal = [ref_dbp_cal, NaN];
                %calibration based
                ref_sbp_est = [ref_sbp_est, NaN];
                ref_dbp_est = [ref_dbp_est, NaN];
                %generic
                ref_pat = [ref_pat, NaN];
                ref_pwv = [ref_pwv, NaN];
                ref_HR = [ref_HR, NaN];                  
            end
        end
            
            %compute average of reference arrays
            ref_sbp_cal = nanmean(ref_sbp_cal);
            ref_dbp_cal = nanmean(ref_dbp_cal);
            
            %calibration based
            ref_sbp_est = nanmean(ref_sbp_est);
            ref_dbp_est = nanmean(ref_dbp_est);
            
            %generic
            ref_pat = nanmean(ref_pat);
            ref_pwv = nanmean(ref_pwv);
            ref_HR = nanmean(ref_HR);
            
            %add info on used references to ouput
            %estimation based
            output.ppg.(chan).bp.tracker.estimation_based.reference_files = files_for_reference;
            output.ppg.(chan).bp.tracker.estimation_based.reference_type = reference_time_type;
            output.ppg.(chan).bp.tracker.estimation_based.reference_context = reference_context;
            output.ppg.(chan).bp.tracker.estimation_based.reference_sbp = ref_sbp_est;
            output.ppg.(chan).bp.tracker.estimation_based.reference_dbp = ref_dbp_est;
            output.ppg.(chan).bp.tracker.estimation_based.reference_pat = ref_pat;
            output.ppg.(chan).bp.tracker.estimation_based.reference_pwv = ref_pwv;
            output.ppg.(chan).bp.tracker.estimation_based.reference_HR = ref_HR;            
            
            %calibration based
            output.ppg.(chan).bp.tracker.calibration_based.reference_files = files_for_reference;
            output.ppg.(chan).bp.tracker.calibration_based.reference_type = reference_time_type;
            output.ppg.(chan).bp.tracker.calibration_based.reference_context = reference_context;
            output.ppg.(chan).bp.tracker.calibration_based.reference_sbp = ref_sbp_cal;
            output.ppg.(chan).bp.tracker.calibration_based.reference_dbp = ref_dbp_cal;
            output.ppg.(chan).bp.tracker.calibration_based.reference_pat = ref_pat;
            output.ppg.(chan).bp.tracker.calibration_based.reference_pwv = ref_pwv;
            output.ppg.(chan).bp.tracker.calibration_based.reference_HR = ref_HR;            
            
    %Compute differences and new tracking estimations

            %compute tracking difference (current file - previous reference(s))
            diff_sbp_cal = output.ppg.(chan).bp.tracker.this_reference.bp_est.sbp.signal - ref_sbp_cal;
            diff_dbp_cal = output.ppg.(chan).bp.tracker.this_reference.bp_est.dbp.signal - ref_dbp_cal;
            
            diff_sbp_est = output.ppg.(chan).bp.tracker.this_reference.bp_est.sbp.signal - ref_sbp_est;
            diff_dbp_est = output.ppg.(chan).bp.tracker.this_reference.bp_est.dbp.signal - ref_dbp_est;
            
            diff_pat = output.ppg.(chan).bp.tracker.this_reference.pat.signal - ref_pat;
            diff_pwv = output.ppg.(chan).bp.tracker.this_reference.pwv.signal - ref_pwv;
            diff_hr = output.ppg.(chan).bp.tracker.this_reference.pwv.signal - ref_pwv;
            
            %compute relative changes between between current file/spotcheck and estimation reference
            %pat_based
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.sbp.signal = ref_sbp_est + diff_pat.*gen_lin_PAT_SBP_coeff(1) + diff_hr.*gen_lin_HR_SBP_coeff(1);
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
            
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.dbp.signal = ref_dbp_est + diff_pat.*gen_lin_PAT_DBP_coeff(1) + diff_hr.*gen_lin_HR_DBP_coeff(1);
            output.ppg.(chan).bp.tracker.estimation_based.via_pat.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
            
            %pwv_based
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.sbp.signal = ref_sbp_est + diff_pwv.*gen_lin_PWV_SBP_coeff(1) + diff_hr.*gen_lin_HR_SBP_coeff(1);
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); 
            
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.dbp.signal = ref_dbp_est + diff_pwv.*gen_lin_PWV_DBP_coeff(1) + diff_hr.*gen_lin_HR_DBP_coeff(1);
            output.ppg.(chan).bp.tracker.estimation_based.via_pwv.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); 
                                                      
            %compute relative changes between between current file/spotcheck and calibration reference
            %pat_based
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.sbp.signal = ref_sbp_cal + diff_pat.*gen_lin_PAT_SBP_coeff(1) + diff_hr.*gen_lin_HR_SBP_coeff(1);
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);
            
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.dbp.signal = ref_dbp_cal + diff_pat.*gen_lin_PAT_DBP_coeff(1) + diff_hr.*gen_lin_HR_DBP_coeff(1);
            output.ppg.(chan).bp.tracker.calibration_based.via_pat.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1);             
            
            %pwv_based
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.sbp.signal = ref_sbp_cal + diff_pat.*gen_lin_PWV_SBP_coeff(1) + diff_hr.*gen_lin_HR_SBP_coeff(1);
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.sbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m
            
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.dbp.signal = ref_dbp_cal + diff_pat.*gen_lin_PWV_DBP_coeff(1) + diff_hr.*gen_lin_HR_DBP_coeff(1);
            output.ppg.(chan).bp.tracker.calibration_based.via_pwv.dbp.timestamps = input.ppg.(chan).spotcheck.timestamps(1); %i ipv 1 in BP.m            
            
            %%%UNCOMMENT to check direct output performance by subtracting relative change from original absolute estimation
            %output.ppg.(chan).bp.sbp_loso_glm(1) = output.ppg.(chan).bp.sbp_loso_glm(1) - diff_sbp_cal;
            %output.ppg.(chan).bp.dbp_loso_glm(1) = output.ppg.(chan).bp.dbp_loso_glm(1) - diff_dbp_cal; 
%             output.ppg.(chan).bp.sbp_loso_glm(1) = ref_sbp_cal + diff_pat.*gen_lin_PWV_SBP_coeff(1) + diff_hr.*gen_lin_HR_SBP_coeff(1);
%             output.ppg.(chan).bp.dbp_loso_glm(1) = ref_dbp_cal + diff_pat.*gen_lin_PWV_DBP_coeff(1) + diff_hr.*gen_lin_HR_DBP_coeff(1);
            
        end


    
    %% Sitting full model
        
    %%%cd(data_folder)
    load(['full_bp_models_sitting.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp, output.ppg.(chan).bp.dbp_CInt] = predict(full_dbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)']);
    
    [output.ppg.(chan).bp.sbp, output.ppg.(chan).bp.sbp_CInt] = predict(full_sbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)' ...
        output.ppg.(chan).bp.dbp]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1, output.ppg.(chan).bp.dbp1_CInt] = predict(full_dbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))']);
    %
    %     [output.ppg.(chan).bp.sbp1, output.ppg.(chan).bp.sbp1_CInt] = predict(full_sbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         output.ppg.(chan).bp.dbp1]);
    
    %disp(['The full SBP is: ' num2str(output.ppg.(chan).bp.sbp) ' and the full DBP is: ' num2str(output.ppg.(chan).bp.dbp) ])
       
    
    %No SSB
    
    load(['full_bp_models_sitting_no_ssb.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp_no_ssb, output.ppg.(chan).bp.dbp_no_ssb_CInt] = predict(full_dbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)']);
    
    [output.ppg.(chan).bp.sbp_no_ssb, output.ppg.(chan).bp.sbp_no_ssb_CInt] = predict(full_sbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        output.ppg.(chan).bp.dbp_no_ssb]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1_no_ssb, output.ppg.(chan).bp.dbp1_no_ssb_CInt] = predict(full_dbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1']);
    %
    %     [output.ppg.(chan).bp.sbp1_no_ssb, output.ppg.(chan).bp.sbp1_no_ssb_CInt] = predict(full_sbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         output.ppg.(chan).bp.dbp1_no_ssb]);
    
    %disp(['The RF full SBP without SSB is: ' num2str(output.ppg.(chan).bp.sbp_no_ssb) ' and The RF full DBP without SSB is: ' num2str(output.ppg.(chan).bp.dbp_no_ssb)])

    
    %% Sitting full model GLM
    
    %%%cd(data_folder)
    load(['full_bp_models_sitting_glm.mat'])    

%   From BPv7.0    
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_glm = glmval(full_dbp_model,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)', ...
        ((input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1)).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity'); %PWV*HR

    output.ppg.(chan).bp.sbp_glm = glmval(full_sbp_model,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)', ...
        ((input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1)).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity'); %PWV*HR
    
    %No SSB
    
    load(['full_bp_models_sitting_glm_no_ssb.mat']) 
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_glm_no_ssb = glmval(full_dbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        (input.ppg.(chan).spotcheck.pat.signal(1).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity'); 

    output.ppg.(chan).bp.sbp_glm_no_ssb = glmval(full_sbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        (input.ppg.(chan).spotcheck.pat.signal(1).*input.ppg.(chan).spotcheck.HR.signal(1))'], 'identity');         
    
    
elseif strcmp(input.context.posture.signal{1},'standing')
    %% standing LOSO
    
    %%%cd(subject_folder)
    load([subject_id '_bp_models_standing.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp_loso, output.ppg.(chan).bp.dbp_loso_CInt] = predict(dbp_model_standing,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)']);
    
    [output.ppg.(chan).bp.sbp_loso, output.ppg.(chan).bp.sbp_loso_CInt] = predict(sbp_model_standing,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)' ...
        output.ppg.(chan).bp.dbp_loso]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1_loso, output.ppg.(chan).bp.dbp1_loso_CInt] = predict(dbp_model_standing,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))']);
    %
    %     [output.ppg.(chan).bp.sbp1_loso, output.ppg.(chan).bp.sbp1_loso_CInt] = predict(sbp_model_standing,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         output.ppg.(chan).bp.dbp1_loso]);
    
    %disp(['The RF LOSO SBP is: ' num2str(output.ppg.(chan).bp.sbp_loso) ' and The RF LOSO DBP is: ' num2str(output.ppg.(chan).bp.dbp_loso)])
    
    %No SSB
    
    load([subject_id '_bp_models_standing_no_ssb.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp_loso_no_ssb, output.ppg.(chan).bp.dbp_loso_no_ssb_CInt] = predict(dbp_model_standing_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)']);
    
    [output.ppg.(chan).bp.sbp_loso_no_ssb, output.ppg.(chan).bp.sbp_loso_no_ssb_CInt] = predict(sbp_model_standing_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        output.ppg.(chan).bp.dbp_loso_no_ssb]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1_loso_no_ssb, output.ppg.(chan).bp.dbp1_loso_no_ssb_CInt] = predict(dbp_model_standing_no_ssb,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1']);
    %
    %     [output.ppg.(chan).bp.sbp1_loso_no_ssb, output.ppg.(chan).bp.sbp1_loso_no_ssb_CInt] = predict(sbp_model_standing_no_ssb,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         output.ppg.(chan).bp.dbp1_loso_no_ssb]);
    
    %disp(['The RF LOSO SBP without SSB is: ' num2str(output.ppg.(chan).bp.sbp_loso_no_ssb) ' and The RF LOSO DBP without SSB is: ' num2str(output.ppg.(chan).bp.dbp_loso_no_ssb) ])
    
    %% Standing LOSO CI
    load([subject_id '_CI_SSB_matrix_standing.mat'])
    
    %age
    %%%cd(BP_framework_folder)
    %normalization and median computation from all subjects (excl current subject)
    age_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,2));
    median_age_norm_loso = nanmedian(age_norm_loso);
    age_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,2); input.ssb.age.signal(1)]);
    %compute absolute normalized distance to median
    age_norm_distance = abs(age_norm_all(end) - median_age_norm_loso);
    
    output.ssb.age_norm = age_norm_all(end);
    output.ssb.age_norm_distance = age_norm_distance;
    
    %height
    
    %normalization and median computation from all subjects (excl current subject)
    height_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,3));
    median_height_norm_loso = nanmedian(height_norm_loso);
    height_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,3); input.ssb.height.signal(1)]);
    %compute absolute normalized distance to median
    height_norm_distance = abs(height_norm_all(end) - median_height_norm_loso);
    
    output.ssb.height_norm = height_norm_all(end);
    output.ssb.height_norm_distance = height_norm_distance;
    
    %weight
    
    %normalization and median computation from all subjects (excl current subject)
    weight_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,4));
    median_weight_norm_loso = nanmedian(weight_norm_loso);
    weight_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,4); input.ssb.weight.signal(1)]);
    %compute absolute normalized distance to median
    weight_norm_distance = abs(weight_norm_all(end) - median_weight_norm_loso);
    
    output.ssb.weight_norm = weight_norm_all(end);
    output.ssb.weight_norm_distance = weight_norm_distance;
    
    %PAT
    
    %normalization and median computation from all subjects (excl current subject)
    PAT_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,7));
    median_PAT_norm_loso = nanmedian(PAT_norm_loso);
    PAT_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,7); input.ppg.(chan).spotcheck.pat.signal(1)]);
    %compute absolute normalized distance to median
    PAT_norm_distance = abs(PAT_norm_all(end) - median_PAT_norm_loso);
    
    output.ppg.(chan).pat.pat_norm = PAT_norm_all(end);
    output.ppg.(chan).pat.pat_norm_distance = PAT_norm_distance;
    
    %PATSD
    
    %normalization and median computation from all subjects (excl current subject)
    PATsd_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,8));
    median_PATsd_norm_loso = nanmedian(PATsd_norm_loso);
    PATsd_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,8); input.ppg.(chan).spotcheck.pat.sd(1)]);
    %compute absolute normalized distance to median
    PATsd_norm_distance = abs(PATsd_norm_all(end) - median_PATsd_norm_loso);
    
    output.ppg.(chan).pat.patSD_norm = PATsd_norm_all(end);
    output.ppg.(chan).pat.patSD_norm_distance = PATsd_norm_distance;
    
    %HR
    
    %normalization and median computation from all subjects (excl current subject)
    HR_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,10));
    median_HR_norm_loso = nanmedian(HR_norm_loso);
    HR_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,10); input.ppg.(chan).spotcheck.HR.signal(1)]);
    %compute absolute normalized distance to median
    HR_norm_distance = abs(HR_norm_all(end) - median_HR_norm_loso);
    
    output.ppg.(chan).pat.HR_norm = HR_norm_all(end);
    output.ppg.(chan).pat.HR_norm_distance = HR_norm_distance;
    
    %HRSD
    
    %normalization and median computation from all subjects (excl current subject)
    HRSD_norm_loso = normalize_array(CI_SSB_matrix_standing_loso(:,11));
    median_HRSD_norm_loso = nanmedian(HRSD_norm_loso);
    HRSD_norm_all = normalize_array([CI_SSB_matrix_standing_loso(:,11); input.ppg.(chan).spotcheck.HR.sd(1)]);
    %compute absolute normalized distance to median
    HRSD_norm_distance = abs(HRSD_norm_all(end) - median_HRSD_norm_loso);
    
    output.ppg.(chan).pat.HRSD_norm = HR_norm_all(end);
    output.ppg.(chan).pat.HRSD_norm_distance = HR_norm_distance;
    
    
    %% standing LOSO GLM
    
    %%%cd(subject_folder)
    load([subject_id '_bp_models_standing_glm.mat'])     

%   From BPv7.0    
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_loso_glm = glmval(dbp_model_standing_glm,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)'], 'identity'); 

    output.ppg.(chan).bp.sbp_loso_glm = glmval(sbp_model_standing_glm,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)'], 'identity');   
    
    %No SSB
    
    load([subject_id '_bp_models_standing_glm_no_ssb.mat'])
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_loso_glm_no_ssb = glmval(dbp_model_standing_glm_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)'], 'identity'); 

    output.ppg.(chan).bp.sbp_loso_glm_no_ssb = glmval(sbp_model_standing_glm_no_ssb,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)'], 'identity');      
    
    %% standing full model
    
    %%%cd(data_folder)
    load(['full_bp_models_standing.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp, output.ppg.(chan).bp.dbp_CInt] = predict(full_dbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)']);
    
    [output.ppg.(chan).bp.sbp, output.ppg.(chan).bp.sbp_CInt] = predict(full_sbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        input.ssb.age.signal(1)' ...
        input.ssb.height.signal(1)' ...
        input.ssb.weight.signal(1)' ...
        output.ppg.(chan).bp.dbp]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1, output.ppg.(chan).bp.dbp1_CInt] = predict(full_dbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))']);
    %
    %     [output.ppg.(chan).bp.sbp1, output.ppg.(chan).bp.sbp1_CInt] = predict(full_sbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         (input.ssb.age .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.height .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         (input.ssb.weight .* ones(1,length(input.ppg.(chan).pat.pat1)))' ...
    %         output.ppg.(chan).bp.dbp1]);
    
    %disp(['The RF full SBP is: ' num2str(output.ppg.(chan).bp.sbp) ' and The RF full DBP is: ' num2str(output.ppg.(chan).bp.dbp) ])
    
    %No SSB
    
    load(['full_bp_models_standing_no_ssb.mat'])
    
    %for single PAT/BP
    [output.ppg.(chan).bp.dbp_no_ssb, output.ppg.(chan).bp.dbp_no_ssb_CInt] = predict(full_dbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)']);
    
    [output.ppg.(chan).bp.sbp_no_ssb, output.ppg.(chan).bp.sbp_no_ssb_CInt] = predict(full_sbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ...
        input.ppg.(chan).spotcheck.pat.sd(1)', ...
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ppg.(chan).spotcheck.HR.sd(1)', ...
        output.ppg.(chan).bp.dbp_no_ssb]);
    
    %     %for continuous PAT/BP stream
    %     [output.ppg.(chan).bp.dbp1_no_ssb, output.ppg.(chan).bp.dbp1_no_ssb_CInt] = predict(full_dbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1']);
    %
    %     [output.ppg.(chan).bp.sbp1_no_ssb, output.ppg.(chan).bp.sbp1_no_ssb_CInt] = predict(full_sbp_model,...
    %         [input.ppg.(chan).pat.pat1' ...
    %         input.ppg.(chan).pat.patSD1' ...
    %         input.ppg.(chan).pat.HR1' ...
    %         input.ppg.(chan).pat.HRSD1' ...
    %         output.ppg.(chan).bp.dbp1_no_ssb]);
    
    %disp(['The RF full SBP without SSB is: ' num2str(output.ppg.(chan).bp.sbp_no_ssb) ' and The RF full DBP without SSB is: ' num2str(output.ppg.(chan).bp.dbp_no_ssb)])
    
    %% standing full model GLM
    
    %%%cd(data_folder)
    load(['full_bp_models_standing_glm.mat'])    

%   From BPv7.0    
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_glm = glmval(full_dbp_model,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)'], 'identity'); 

    output.ppg.(chan).bp.sbp_glm = glmval(full_sbp_model,...
        [(input.ssb.height.signal(1)./input.ppg.(chan).spotcheck.pat.signal(1))', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)', ...
        input.ssb.age.signal(1)', ...
        input.ssb.weight.signal(1)', ...
        (input.ssb.weight.signal(1)./(input.ssb.height.signal(1)./100).^2)'], 'identity');   
    
    %No SSB
    
    load(['full_bp_models_standing_glm_no_ssb.mat']) 
    
    %for single PAT/BP
    output.ppg.(chan).bp.dbp_glm_no_ssb = glmval(full_dbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)'], 'identity'); 

    output.ppg.(chan).bp.sbp_glm_no_ssb = glmval(full_sbp_model,...
        [input.ppg.(chan).spotcheck.pat.signal(1)', ... 
        input.ppg.(chan).spotcheck.HR.signal(1)'], 'identity');     
    
    
end




end