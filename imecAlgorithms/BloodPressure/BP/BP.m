% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : BP.m
%  Content    : Blood Pressure Computation
%  Package    : SIMBA.Validation
%  Created by : F. Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 12.9.14
%  Modification and Version History:
%  | Developer | Version            |    Date    |     Changes      |
%  |  fbeutel  |   1.0              | 13-05-2014 | Code Refactoring |
%  |  ECWentink|   1.1              | 01-09-2014 | Generelize code  |
%  |  fbeutel  |   2.0              | 12-09-2014 | Align with data structure  |
%  |  fbeutel  |   2.1              | 16-09-2014 | Include SSB input  |
%  |  fbeutel  |   3.0              | 08-10-2014 | Referring to 3.0 of RF model! Official SIMBA 3.0 BP is Linear Regression Model!
%                                                  SSB read from input data
%                                                  Switch models based on availability SSB and posture
%                                                  Use latest models
%                                                  Second input argument for training mode |
%  |  fbeutel  |   3.1              | 10-10-2014 | Cleanup for C implementation
%                                                  (Integration of latest models,
%                                                  Switch models dependent on case (availability ssb and context)
%                                                  Deletion of individual models)
%  |  fbeutel  |   4.1              | 19-10-2014 | Final models for code freeze added to folder |
%  |  fbeutel  |   4.1.1            | 04-11-2014 | Adjust predict function input for new data struct formatting |
%  |  fbeutel  |   4.2              | 03-12-2014 | Adapt to new output of PAT script, Output BP per channel |
%  |  fbeutel  |   5.0              | 17-12-2014 | Added new model based on single PPG channel (e/green) |
%                                                | Compute BP only in cases where all non-SSB parameters are present, else give error |
%  |  fbeutel  |   5.1              | 30-01-2015 | Adjusted code to handle different SSB settings per csv file |
%  |  fbeutel  |   7.0              | 11-03-2015 | Switch to GLM model with PWV, HR, age, weight and BMI as inputs |
%  |  fbeutel  | 0.7.0              | 12-03-2015 | Same as 7.0: Introduction of new version tracking |
%  |  fbeutel  | 0.7.1              | 23-04-2015 | Basic calibration and tracking funtionality |
%  |  fbeutel  | 0.7.2              | 29-04-2015 | Expanded output parameters; Calibration&Tracking validated by consistency experiment data |
%  |  fbeutel  | 0.7.3              | 07-05-2015 | Add trial number as reference context criterion |
%  |  fbeutel  | 0.7.4              | 13-05-2015 | Select tracking type --> Database or single file |
%  |  fbeutel  | 0.7.5              | 26-05-2015 | Adjustment of tracker coefficients and minor bugfixes |
%  |  fbeutel  | 0.7.6              | 27-05-2015 | For tracker only: Include interaction term between HR and PWV, adjust coefficients accordingly (defined from exercise and rest) |
%  |  fbeutel  | 0.7.7              | 27-05-2015 | Definite set of coefficients for 0.8.0 C implementation |
%  |  fbeutel  | Absolute BP: 0.8.0 | 05-06-2015 | Introduction of separate BP Tracker versioning |
%              | BP Tracker:  0.1.0 |            |  |
%  | ehermeling| Absolute BP  0.8.1 | 24-06-2015 | Made compatible with FeatureExtraction algorithm (which replaced of PAT.m)
%   	       | BP Tracker:  0.1.1 |            |  |
%  |  fbeutel  | Absolute BP  0.8.1 | 02-07-2015 | Definite set of tracker coefficients for 0.9.0 release |
%   	       | BP Tracker:  0.1.2 |            | (BP CI for now defined by spotcheck.CI in featureExtraction.m) |
%  |  fbeutel  | Absolute BP  0.8.1 | 07-07-2015 | --- |
%   	       | BP Tracker:  0.1.3 |            | Adjust coefficients --> Delta coefficients; Add new reference features: "upstgrad" and "pp_ft_amp"  |
%  |  fbeutel  | Absolute BP  0.8.1 | 09-07-2015 | --- |
%   	       | BP Tracker:  0.1.4 |            | Adjustment coefficients after bugfixes in featureExtraction.m |
%  |  fbeutel  | Absolute BP  0.8.1 | 10-07-2015 | --- |
%   	       | BP Tracker:  0.1.5 |            | Adjustment reference spotcheck --> Average out of first three; Corresponding adjustment coefficients |
%  |  fbeutel  | Absolute BP  0.8.1 | 23-07-2015 | --- |
%   	       | BP Tracker:  0.1.6 |            | Inclusion of GLM model with PWA features besides PAT |
%  |  fbeutel  | Absolute BP  0.8.1 | 23-07-2015 | --- |
%   	       | BP Tracker:  0.2.0 |            | Definite set of tracker coefficients for 0.11.0 release |
%  |  fbeutel  | Absolute BP  0.8.1 | 23-07-2015 | --- |
%   	       | BP Tracker:  0.2.1 |            | Inclusion of PATfoot as reference |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function [input] = BP(input, chan, tracking_type, varargin) 

%define limiting values for GLM models (obtained from histogram of valid measured BP values +/- 10mmHg)
sbp_min = 80;
sbp_max = 200;
dbp_min = 40;
dbp_max = 150;

%% Set input variables
chan = chan(end);
spotcheckVars = input.ppg.(chan).spotcheck;
nSpotchecks = length(spotcheckVars.pat.signal);
if isfield(input, 'subject_id')
    subject_id = input.subject_id;
elseif isfield(input, 'filename')
    [subject_id, remain] = strtok(input.filename,'_');
else
    subject_id = 'SP0000';
    disp('No subject ID available!')
end

if strcmp(tracking_type, 'database')
    
    disp('Tracking in Database Mode')
    
    switch nargin
        case 4
            use_reference = varargin{1};
            if use_reference==1;              
                subject_files = what([subject_id, '_imported']);
                subject_path = subject_files.path;   
                load([subject_path,'\',subject_id,'_ID2ref_spotcheck']);           
            else
                use_reference=0;
            end
            
            %not necessary in database mode, but variable should exist
            reference_spotcheck_estimation = 0;
    end
    
elseif strcmp(tracking_type, 'file')
    
    disp('Tracking in File Mode')
    
    %Input variables for tracking with single file
    switch nargin
        case 4
            reference_spotcheck_estimation = varargin{1}; %reference spotcheck in file (set to -99 in order to use band reference spotcheck values)
            reference_spotcheck_calibration = 1; %reference calibration (~= reference spotcheck when spotchecks are in two files while metadata contains all reference BPs together)
        case 5
            reference_spotcheck_estimation = varargin{1};
            reference_spotcheck_calibration = varargin{2};
        otherwise
            reference_spotcheck_estimation = 1;
    end
    
    %Allocate placeholders for output
    
    %references
    bp.tracker.file.reference.sbp_est = [];
    bp.tracker.file.reference.dbp_est = [];
    bp.tracker.file.reference.pat = [];
    bp.tracker.file.reference.pwv = [];
    bp.tracker.file.reference.HR = [];
    bp.tracker.file.reference.pp_ft_amp = [];
    bp.tracker.file.reference.upstgrad = [];
    bp.tracker.file.reference.RCtime = [];
    
    %bp output
    bp.tracker.file.estimation_based.sbp = ones(nSpotchecks,1)*NaN;
    bp.tracker.file.estimation_based.dbp = ones(nSpotchecks,1)*NaN;
    %bp.tracker.file.calibration_based.sbp = ones(nSpotchecks,1)*NaN;
    %bp.tracker.file.calibration_based.dbp = ones(nSpotchecks,1)*NaN;
    
    %Send warning if file contains single spotcheck
    if nSpotchecks == 1
        disp('Only single spotcheck in file --> Tracker will equal absolute BP estimation')
    end
    
    
end

try
    if ~iscell(input.context.posture.signal)
        for i = 1:nSpotchecks
            
            last_10_posture_timestamps = input.context.posture.timestamps(find(input.context.posture.timestamps <= spotcheckVars.timestamps(i), 10, 'last')); %updated either 1sec or with ECG_r
            posture_signal = round(nanmean(input.context.posture.signal(ismember(input.context.posture.timestamps, last_10_posture_timestamps))));
            if posture_signal(i) == 2;
                posture{i} = 'standing';
                postureValid = 1;
            elseif posture_signal(i) == 1;
                posture{i} = 'sitting';
                postureValid = 1;
            else
                postureValid = 0;
            end
        end
        
    else
        if  length(input.context.posture.signal) == 1
            posture = repmat(input.context.posture.signal,nSpotchecks,1);
        else
            posture = input.context.posture.signal;
        end
        postureValid = 1;
    end
    
catch
    postureValid = 0;
end

%% Prepare SSB data from excel spreadsheet (NO C IMPLEMENTATION REQUIRED)

%prepare data where SSB and context variables are imported from excel file and not coming from the band (e.g. used in BP framework).
%in this case in and context vars only exists once and without timestamps!
%SSB vars

try   age = repmat(input.ssb.age.signal,nSpotchecks,1); ageValid = age(1) > 0;
catch; ageValid = 0;     end
try   height = repmat(input.ssb.height.signal,nSpotchecks,1); heightValid = height(1) > 0;
catch; heightValid = 0;  end
try   weight = repmat(input.ssb.weight.signal,nSpotchecks,1); weightValid = weight(1) > 0;
catch; weightValid = 0;  end

if heightValid && weightValid; bmi = weight./(height/100).^2; end

%% Select model and compute absolute BP

for i = 1:nSpotchecks
    if postureValid && strcmp(posture{i},'standing')
        if ageValid && heightValid && weightValid
            load('emb_full_bp_models_standing_glm.mat') %if posture is specified as standing, ssb field is present and no ssb is empty
            disp('Specified model: Standing / SSB')
            
        else
            load('emb_full_bp_models_standing_glm_no_ssb.mat') %if posture is specified as standing, ssb field is present but any ssb is empty
            disp('Specified model: Standing / No SSB')
        end
    else
        if ageValid && heightValid && weightValid
            load('emb_full_bp_models_sitting_glm.mat') %if posture is specified as sitting, ssb field is present and no ssb is empty
            disp('Specified model: Sitting / SSB')
        else
            load('emb_full_bp_models_sitting_glm_no_ssb.mat') %if posture is specified as sitting, ssb field is present but any ssb is empty
            disp('Specified model: Sitting / No SSB')
            
        end
    end
    
    if ageValid && heightValid && weightValid
        % GLM incl. SSB
        bp.dbp.signal(i) = glmval(full_dbp_model,[height(i)/spotcheckVars.pat.signal(i),spotcheckVars.HR.signal(i),age(i),weight(i), bmi(i)],'identity');
        bp.sbp.signal(i) = glmval(full_sbp_model,[height(i)/spotcheckVars.pat.signal(i),spotcheckVars.HR.signal(i),age(i),weight(i), bmi(i)],'identity');
    else %No SSB
        bp.dbp.signal(i) = glmval(full_dbp_model,[spotcheckVars.pat.signal(i),spotcheckVars.HR.signal(i)],'identity');
        bp.sbp.signal(i) = glmval(full_sbp_model,[spotcheckVars.pat.signal(i),spotcheckVars.HR.signal(i)],'identity');
    end
    bp.dbp.timestamps(i) = spotcheckVars.timestamps(i);
    bp.dbp.signal(i) = max([dbp_min,min([dbp_max,bp.dbp.signal(i)])]);
    bp.sbp.timestamps(i) = spotcheckVars.timestamps(i);
    bp.sbp.signal(i) = max([sbp_min,min([sbp_max,bp.sbp.signal(i)])]);
        
    %% Calibration and Tracking
    
    %Load glm coeffcients as derived in validation phase of tracker framework
    load('glm_models_pwv');
    load('glm_models_pat');
    
    %Corresponding GLM models (to properly define coefficients below)
        %SBP model: glmfit([delta_PWV/PAT_qual, delta_pp_ft_amp_qual, delta_upstgrad_qual], delta_SBP_qual);
        %DBP model: glmfit([delta_HR_qual, delta_upstgrad_qual], delta_DBP_qual);
    
    %linear
    %PAT(if height is not available)
        glm_alpha_sbp_pat = glm_sbp_model_pat(1);
        glm_alpha_dbp_pat = glm_dbp_model_pat(1);
        
        glm_PAT_SBP_coeff_pat = glm_sbp_model_pat(2);
        glm_PAT_DBP_coeff_pat = 0;
        
        glm_PATfoot_SBP_coeff_pat = 0;
        glm_PATfoot_DBP_coeff_pat = 0;
           
        glm_HR_SBP_coeff_pat = 0;
        glm_HR_DBP_coeff_pat = glm_dbp_model_pat(2);
        
        glm_pp_ft_amp_SBP_coeff_pat = glm_sbp_model_pat(3);
        glm_pp_ft_amp_DBP_coeff_pat = 0;
        
        glm_upstgrad_SBP_coeff_pat = glm_sbp_model_pat(4);
        glm_upstgrad_DBP_coeff_pat = glm_dbp_model_pat(3);
        
        glm_RCtime_SBP_coeff_pat = 0;
        glm_RCtime_DBP_coeff_pat = 0;
        
    %PWV
        glm_alpha_sbp_pwv = glm_sbp_model_pwv(1);
        glm_alpha_dbp_pwv = glm_dbp_model_pwv(1);
        
        glm_PWV_SBP_coeff_pwv = glm_sbp_model_pwv(2);
        glm_PWV_DBP_coeff_pwv = 0;
        
        glm_PWVfoot_SBP_coeff_pat = 0;
        glm_PWVfoot_DBP_coeff_pat = 0;
           
        glm_HR_SBP_coeff_pwv = 0;
        glm_HR_DBP_coeff_pwv = glm_dbp_model_pwv(2);
        
        glm_pp_ft_amp_SBP_coeff_pwv = glm_sbp_model_pwv(3);
        glm_pp_ft_amp_DBP_coeff_pwv = 0;
        
        glm_upstgrad_SBP_coeff_pwv = glm_sbp_model_pwv(4);
        glm_upstgrad_DBP_coeff_pwv = glm_dbp_model_pwv(3);
        
        glm_RCtime_SBP_coeff_pwv = 0;
        glm_RCtime_DBP_coeff_pwv = 0;
        
        %Set BP estimation from this/these spotcheck(s) as tracking reference for future spotcheck(s) (and perhaps continuous mode)
        %SBPref
        bp.tracker.this_reference.bp_est.sbp.signal(i) = bp.sbp.signal(i);
        bp.tracker.this_reference.bp_est.sbp.timestamps(i) = spotcheckVars.timestamps(i);
        %DBPref
        bp.tracker.this_reference.bp_est.dbp.signal(i) = bp.dbp.signal(i);
        bp.tracker.this_reference.bp_est.dbp.timestamps(i) = spotcheckVars.timestamps(i);
        %PAT
        bp.tracker.this_reference.pat.signal(i) = spotcheckVars.pat.signal(i);
        bp.tracker.this_reference.pat.timestamps(i) = spotcheckVars.timestamps(i);
        %PATfoot
        bp.tracker.this_reference.pat_foot.signal(i) = spotcheckVars.pat_foot.signal(i);
        bp.tracker.this_reference.pat_foot.timestamps(i) = spotcheckVars.timestamps(i);        
        %PWV & PWVfoot
        if ageValid && heightValid && weightValid
            bp.tracker.this_reference.pwv.signal(i) = height(i)./spotcheckVars.pat.signal(i);
            bp.tracker.this_reference.pwv.timestamps(i) = spotcheckVars.timestamps(i);
            %PWVfoot
            bp.tracker.this_reference.pwv_foot.signal(i) = height(i)./spotcheckVars.pat_foot.signal(i);
            bp.tracker.this_reference.pwv_foot.timestamps(i) = spotcheckVars.timestamps(i);            
        else
            bp.tracker.this_reference.pwv.signal(i) = NaN;
            bp.tracker.this_reference.pwv.timestamps(i) = NaN;
            %PWVfoot
            bp.tracker.this_reference.pwv_foot.signal(i) = NaN;
            bp.tracker.this_reference.pwv_foot.timestamps(i) = NaN;
        end
        % HR
        bp.tracker.this_reference.HR.signal(i) = spotcheckVars.HR.signal(i);
        bp.tracker.this_reference.HR.timestamps(i) = spotcheckVars.timestamps(i);
        % Risetime
        bp.tracker.this_reference.risetime.signal(i) = spotcheckVars.risetime.signal(i);
        bp.tracker.this_reference.risetime.timestamps(i) = spotcheckVars.timestamps(i);
        % Gradient
        bp.tracker.this_reference.pp_ft_amp.signal(i) = spotcheckVars.ppg_pp_ft_amp.signal(i);
        bp.tracker.this_reference.pp_ft_amp.timestamps(i) = spotcheckVars.timestamps(i);
        % PP-FT amplitude
        bp.tracker.this_reference.upstgrad.signal(i) = spotcheckVars.ppg_upstgrad.signal(i);
        bp.tracker.this_reference.upstgrad.timestamps(i) = spotcheckVars.timestamps(i);
        % RC time
        bp.tracker.this_reference.RCtime.signal(i) = spotcheckVars.RCtime.signal(i);
        bp.tracker.this_reference.RCtime.timestamps(i) = spotcheckVars.timestamps(i);         
        
        
        if isfield(input, 'bp') && reference_spotcheck_estimation ~= -99 %ppg independent bp field in root of data struct, containing cuff reference
            bp.tracker.this_reference.bp_cal.sbp.signal(i) = input.bp.sbpref;
            bp.tracker.this_reference.bp_cal.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            bp.tracker.this_reference.bp_cal.dbp.signal(i) = input.bp.dbpref;
            bp.tracker.this_reference.bp_cal.dbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        elseif strcmp(tracking_type, 'file') && reference_spotcheck_estimation == -99
            bp.tracker.this_reference.bp_cal.sbp.signal(i) = input.bp.tracker.band.reference.sbp_cal.signal(1);
            bp.tracker.this_reference.bp_cal.sbp.timestamps(i) = input.bp.tracker.band.reference.sbp_cal.timestamps(1);
            
            bp.tracker.this_reference.bp_cal.dbp.signal(i) = input.bp.tracker.band.reference.dbp_cal.signal(1);
            bp.tracker.this_reference.bp_cal.dbp.timestamps(i) = input.bp.tracker.band.reference.dbp_cal.timestamps(1);
            
        else
            bp.tracker.this_reference.bp_cal.sbp.signal(i) = NaN;
            bp.tracker.this_reference.bp_cal.sbp.timestamps(i) = NaN;
            
            bp.tracker.this_reference.bp_cal.dbp.signal(i) = NaN;
            bp.tracker.this_reference.bp_cal.dbp.timestamps(i) = NaN;
        end
        
        
    %tracking for database type
    if strcmp(tracking_type, 'database')
        
        
        %Search specified files/spotchecks for tracking reference in current database
        
        
        
        if ~strcmp(subject_id, 'SP0000')
            %search files on search path/current database from same subject
            
            if exist([subject_id, '_imported']) == 7%in database mode/for consistency experiment analysis there is a specified folder containing the imported files
                subject_id_files = what([subject_id, '_imported']);
            else %in file based model the files in the subject folder contain all information
                subject_id_files = what(subject_id); %'what' only works when subject id is folder name where files are located!
            end
            
            subject_id_all_mat_files = subject_id_files.mat;
            
        else
            subject_id_all_mat_files = [];
        end
        
        %search indices of files for certain experiments
        subject_id_file_indices_large_scale_sitting = [];
        subject_id_file_indices_consistency = [];
        
        for j = 1:numel(subject_id_all_mat_files)
            %large scale data collection sitting
            search_results_large_scale_sitting = findstr(subject_id_all_mat_files{j}, '_S_SI_');
            if ~isempty(search_results_large_scale_sitting)
                subject_id_file_indices_large_scale_sitting = [subject_id_file_indices_large_scale_sitting, j];
            end
            %consistency experiment
            search_results_consistency = findstr(subject_id_all_mat_files{j}, '_C_');
            if ~isempty(search_results_consistency)
                subject_id_file_indices_consistency = [subject_id_file_indices_consistency, j];
            end
        end
        
        subject_id_mat_files_large_scale_sitting = subject_id_all_mat_files(subject_id_file_indices_large_scale_sitting);
        subject_id_mat_files_consistency = subject_id_all_mat_files(subject_id_file_indices_consistency);
        
        %all relevant files from subject (change to specific experiment type if desired)
        files_for_reference_preselection = [subject_id_mat_files_large_scale_sitting; subject_id_mat_files_consistency];
        
        %Iterate over files to select and accumulate references and compute offset
        
        %define arrays to accumulate references
        %estimation based
        ref_sbp_cal = [];
        ref_dbp_cal = [];
        %calibration based
        ref_sbp_est = [];
        ref_dbp_est = [];
        %generic
        ref_pat = [];
        ref_pat_foot = [];
        ref_pwv = [];
        ref_pwv_foot = [];
        ref_HR = [];
        ref_spotcheck_CI = [];
        ref_risetime = [];
        ref_pp_ft_amp = [];
        ref_upstgrad = [];
        ref_RCtime = [];
        
        if use_reference==0
            disp('No reference file as specified could be found --> No tracking results computed.')
            
            %assig NaNs to outcome vectors
            
            %pat_based
            bp.tracker.estimation_based.via_pat.sbp.signal(i) = NaN;
            bp.tracker.estimation_based.via_pat.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            bp.tracker.estimation_based.via_pat.dbp.signal(i) = NaN;
            bp.tracker.estimation_based.via_pat.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            %pwv_based
            bp.tracker.estimation_based.via_pwv.sbp.signal(i) = NaN;
            bp.tracker.estimation_based.via_pwv.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            bp.tracker.estimation_based.via_pwv.dbp.signal(i) = NaN;
            bp.tracker.estimation_based.via_pwv.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            %pat_based
            bp.tracker.calibration_based.via_pat.sbp.signal(i) = NaN;
            bp.tracker.calibration_based.via_pat.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            bp.tracker.calibration_based.via_pat.dbp.signal(i) = NaN;
            bp.tracker.calibration_based.via_pat.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            %pwv_based
            bp.tracker.calibration_based.via_pwv.sbp.signal(i) = NaN;
            bp.tracker.calibration_based.via_pwv.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            bp.tracker.calibration_based.via_pwv.dbp.signal(i) = NaN;
            bp.tracker.calibration_based.via_pwv.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            
        else
            
            %Define tracking reference
            
            for j = 1:numel(ref_Spotchecks)
                load([subject_id_files.path '\' ref_Spotchecks(j).file_name]) %files can simply be loaded in function since struct called 'data' is not handled here
                %fill reference arrays if tracker references are available
                if isfield(data.ppg.(chan).bp, 'tracker')
                    %estimation based
                    ref_sbp_cal = [ref_sbp_cal, data.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal];
                    ref_dbp_cal = [ref_dbp_cal, data.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.signal];
                    %calibration based
                    ref_sbp_est = [ref_sbp_est, data.ppg.(chan).bp.tracker.this_reference.bp_est.sbp.signal];
                    ref_dbp_est = [ref_dbp_est, data.ppg.(chan).bp.tracker.this_reference.bp_est.dbp.signal];
                    %generic
                    ref_pat = [ref_pat, data.ppg.(chan).bp.tracker.this_reference.pat.signal];
                    ref_pat_foot = [ref_pat_foot, data.ppg.(chan).bp.tracker.this_reference.pat_foot.signal];
                    ref_pwv = [ref_pwv, data.ppg.(chan).bp.tracker.this_reference.pwv.signal];
                    ref_pwv_foot = [ref_pwv_foot, data.ppg.(chan).bp.tracker.this_reference.pwv_foot.signal];
                    ref_HR = [ref_HR, data.ppg.(chan).bp.tracker.this_reference.HR.signal];
                    ref_spotcheck_CI = [ref_spotcheck_CI, data.ppg.(chan).spotcheck.CI];
                    ref_risetime = [ref_risetime, data.ppg.(chan).spotcheck.risetime.signal];
                    ref_pp_ft_amp = [ref_pp_ft_amp, data.ppg.(chan).spotcheck.ppg_pp_ft_amp.signal];
                    ref_upstgrad = [ref_upstgrad, data.ppg.(chan).spotcheck.ppg_upstgrad.signal];
                    ref_RCtime = [ref_RCtime, data.ppg.(chan).spotcheck.RCtime.signal];
                else
                    %estimation based
                    ref_sbp_cal = [ref_sbp_cal, NaN];
                    ref_dbp_cal = [ref_dbp_cal, NaN];
                    %calibration based
                    ref_sbp_est = [ref_sbp_est, NaN];
                    ref_dbp_est = [ref_dbp_est, NaN];
                    %generic
                    ref_pat = [ref_pat, NaN];
                    ref_pat_foot = [ref_pat_foot, NaN];
                    ref_pwv = [ref_pwv, NaN];
                    ref_pwv_foot = [ref_pwv_foot, NaN];
                    ref_HR = [ref_HR, NaN];
                    ref_pp_ft_amp = [ref_pp_ft_amp, NaN];
                    ref_upstgrad = [ref_upstgrad, NaN];
                    ref_RCtime = [ref_RCtime, NaN];
                end
            end
            
            %compute average of reference arrays
            ref_sbp_cal = nanmean(ref_sbp_cal);
            ref_dbp_cal = nanmean(ref_dbp_cal);
            
            %calibration based
            ref_sbp_est = nanmean(ref_sbp_est);
            ref_dbp_est = nanmean(ref_dbp_est);
            
            %generic
            ref_pat = nanmean(ref_pat);
            ref_pat_foot = nanmean(ref_pat_foot);
            ref_pwv = nanmean(ref_pwv);
            ref_pwv_foot = nanmean(ref_pwv_foot);
            ref_HR = nanmean(ref_HR);
            ref_spotcheck_CI = nanmean(ref_spotcheck_CI);
            ref_risetime = nanmean(ref_risetime);
            ref_pp_ft_amp = nanmean(ref_pp_ft_amp);
            ref_upstgrad = nanmean(ref_upstgrad);
            ref_RCtime = nanmean(ref_RCtime);
              
            %add info on used references to ouput
            %estimation based
            %ref_Spotchecks(rr).file_name = file_name;
            %ref_Spotchecks(rr).daytime = ref_daytime{rr};
            %ref_Spotchecks(rr).trial_nr = ref_trial_nr{rr};
            %ref_Spotchecks(rr).day = ref_day{rr};
            
            bp.tracker.estimation_based.reference_files{j} = ref_Spotchecks(j).file_name;
            bp.tracker.estimation_based.reference_type{j} = ref_Spotchecks(j).daytime;
            bp.tracker.estimation_based.reference_context{j} = ref_Spotchecks(j).context;
            bp.tracker.estimation_based.reference_sbp(i) = ref_sbp_est;
            bp.tracker.estimation_based.reference_dbp(i) = ref_dbp_est;
            bp.tracker.estimation_based.reference_pat(i) = ref_pat;
            bp.tracker.estimation_based.reference_pat_foot(i) = ref_pat_foot;
            bp.tracker.estimation_based.reference_pwv(i) = ref_pwv;
            bp.tracker.estimation_based.reference_pwv_foot(i) = ref_pwv_foot;            
            bp.tracker.estimation_based.reference_HR(i) = ref_HR;
            bp.tracker.estimation_based.reference_spotcheck_CI(i) = ref_spotcheck_CI;
            bp.tracker.estimation_based.reference_risetime(i) = ref_risetime;
            bp.tracker.estimation_based.reference_pp_ft_amp(i) = ref_pp_ft_amp;
            bp.tracker.estimation_based.reference_upstgrad(i) = ref_upstgrad;
            bp.tracker.estimation_based.reference_RCtime(i) = ref_RCtime;
            
            %calibration based
            bp.tracker.calibration_based.reference_files{j} = ref_Spotchecks(j).file_name;
            bp.tracker.calibration_based.reference_type{j} = ref_Spotchecks(j).daytime;
            bp.tracker.calibration_based.reference_context{j} =  ref_Spotchecks(j).context;
            bp.tracker.calibration_based.reference_sbp(i) = ref_sbp_cal;
            bp.tracker.calibration_based.reference_dbp(i) = ref_dbp_cal;
            bp.tracker.calibration_based.reference_pat(i) = ref_pat;
            bp.tracker.calibration_based.reference_pat_foot(i) = ref_pat_foot;            
            bp.tracker.calibration_based.reference_pwv(i) = ref_pwv;
            bp.tracker.calibration_based.reference_pwv_foot(i) = ref_pwv_foot;             
            bp.tracker.calibration_based.reference_HR(i) = ref_HR;
            bp.tracker.calibration_based.reference_spotcheck_CI(i) = ref_spotcheck_CI;
            bp.tracker.calibration_based.reference_risetime(i) = ref_risetime;
            bp.tracker.calibration_based.reference_pp_ft_amp(i) = ref_pp_ft_amp;
            bp.tracker.calibration_based.reference_upstgrad(i) = ref_upstgrad;
            bp.tracker.calibration_based.reference_RCtime(i) = ref_RCtime;
            
            %Compute differences and new tracking estimations
            
            %compute tracking difference (current file - previous reference(s))
            diff_sbp_cal = bp.tracker.this_reference.bp_est.sbp.signal - ref_sbp_cal;
            diff_dbp_cal = bp.tracker.this_reference.bp_est.dbp.signal - ref_dbp_cal;
            
            diff_sbp_est = bp.tracker.this_reference.bp_est.sbp.signal - ref_sbp_est;
            diff_dbp_est = bp.tracker.this_reference.bp_est.dbp.signal - ref_dbp_est;
            
            diff_pat = bp.tracker.this_reference.pat.signal - ref_pat;
            diff_pat_foot = bp.tracker.this_reference.pat_foot.signal - ref_pat_foot;
            diff_pwv = bp.tracker.this_reference.pwv.signal - ref_pwv;
            diff_pwv_foot = bp.tracker.this_reference.pwv_foot.signal - ref_pwv_foot;
            diff_hr = bp.tracker.this_reference.HR.signal - ref_HR;
            diff_risetime = bp.tracker.this_reference.risetime.signal - ref_risetime;
            diff_pp_ft_amp = bp.tracker.this_reference.pp_ft_amp.signal - ref_pp_ft_amp;
            diff_upstgrad = bp.tracker.this_reference.upstgrad.signal - ref_upstgrad;
            diff_RCtime = bp.tracker.this_reference.RCtime.signal - ref_RCtime;
            
            %compute relative changes between between current file/spotcheck and estimation reference
            %pat_based
            bp.tracker.estimation_based.via_pat.sbp.signal(i) = ref_sbp_est + glm_alpha_sbp_pat + diff_pat.*glm_PAT_SBP_coeff_pat + diff_hr.*glm_HR_SBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pat + diff_upstgrad.*glm_upstgrad_SBP_coeff_pat + diff_RCtime.*glm_RCtime_SBP_coeff_pat;
            bp.tracker.estimation_based.via_pat.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            bp.tracker.estimation_based.via_pat.dbp.signal(i) = ref_dbp_est + glm_alpha_dbp_pat + diff_pat.*glm_PAT_DBP_coeff_pat + diff_hr.*glm_HR_DBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pat + diff_upstgrad.*glm_upstgrad_DBP_coeff_pat + diff_RCtime.*glm_RCtime_DBP_coeff_pat;
            bp.tracker.estimation_based.via_pat.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            %pwv_based
            bp.tracker.estimation_based.via_pwv.sbp.signal(i) = ref_sbp_est + glm_alpha_sbp_pwv + diff_pwv.*glm_PWV_SBP_coeff_pwv + diff_hr.*glm_HR_SBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_SBP_coeff_pwv + diff_RCtime.*glm_RCtime_SBP_coeff_pwv;
            bp.tracker.estimation_based.via_pwv.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            bp.tracker.estimation_based.via_pwv.dbp.signal(i) = ref_dbp_est + glm_alpha_dbp_pwv + diff_pwv.*glm_PWV_DBP_coeff_pwv + diff_hr.*glm_HR_DBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_DBP_coeff_pwv + diff_RCtime.*glm_RCtime_DBP_coeff_pwv;
            bp.tracker.estimation_based.via_pwv.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            %compute relative changes between between current file/spotcheck and calibration reference
            %pat_based
            bp.tracker.calibration_based.via_pat.sbp.signal(i) = ref_sbp_cal + glm_alpha_sbp_pat  + diff_pat.*glm_PAT_SBP_coeff_pat + diff_hr.*glm_HR_SBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pat + diff_upstgrad.*glm_upstgrad_SBP_coeff_pat + diff_RCtime.*glm_RCtime_SBP_coeff_pat;
            bp.tracker.calibration_based.via_pat.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            bp.tracker.calibration_based.via_pat.dbp.signal(i) = ref_dbp_cal + glm_alpha_dbp_pat + diff_pat.*glm_PAT_DBP_coeff_pat + diff_hr.*glm_HR_DBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pat + diff_upstgrad.*glm_upstgrad_DBP_coeff_pat + diff_RCtime.*glm_RCtime_DBP_coeff_pat;
            bp.tracker.calibration_based.via_pat.dbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            %pwv_based
            bp.tracker.calibration_based.via_pwv.sbp.signal(i) = ref_sbp_cal + glm_alpha_sbp_pwv + diff_pwv.*glm_PWV_SBP_coeff_pwv + diff_hr.*glm_HR_SBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_SBP_coeff_pwv + diff_RCtime.*glm_RCtime_SBP_coeff_pwv;
            bp.tracker.calibration_based.via_pwv.sbp.timestamps(i) = spotcheckVars.timestamps(i);
            
            bp.tracker.calibration_based.via_pwv.dbp.signal(i) = ref_dbp_cal + glm_alpha_dbp_pwv + diff_pwv.*glm_PWV_DBP_coeff_pwv + diff_hr.*glm_HR_DBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_DBP_coeff_pwv + diff_RCtime.*glm_RCtime_DBP_coeff_pwv;
            bp.tracker.calibration_based.via_pwv.dbp.timestamps(i) = spotcheckVars.timestamps(i);
                        
        end
        
    end
    
    %% Output BP to console (if valid)
    
    %Absolute BP
    if isnan(spotcheckVars.pat.signal(i)) | isnan(spotcheckVars.pat.sd(i)) | isnan(spotcheckVars.HR.signal(i)) | isnan(spotcheckVars.HR.sd(i))
        bp.sbp.signal(i) = NaN;
        bp.sbp.timestamps(i) = NaN;
        bp.dbp.signal(i) = NaN;
        bp.dbp.timestamps(i) = NaN;
        disp(['No absolute BP computed at spotcheck ' num2str(i) ' due to invalid inputs'])
    else
        disp(['The absolute SBP for channel ' chan ' is: ' num2str(bp.sbp.signal(i)') ' and the absolute DBP is: ' num2str(bp.dbp.signal(i)') ' at spotcheck ' num2str(i)])
    end
    
    %output database tracker
    if strcmp(tracking_type, 'database')
        
        %Relative/Tracked BP wrt estimation
        if isnan(bp.tracker.estimation_based.via_pat.sbp.signal(i)) && isnan(bp.tracker.estimation_based.via_pwv.sbp.signal(i))
            disp(['No relative BP computed at spotcheck ' num2str(i) ' due to invalid inputs'])
            disp('(With respect to preceding estimation(s))')
        elseif ~isnan(bp.tracker.estimation_based.via_pwv.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.estimation_based.via_pwv.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.estimation_based.via_pwv.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding estimation(s))')
        elseif ~isnan(bp.tracker.estimation_based.via_pat.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.estimation_based.via_pat.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.estimation_based.via_pat.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding estimation(s))')
        end
        
        %Relative/Tracked BP wrt calibration
        if isnan(bp.tracker.calibration_based.via_pat.sbp.signal(i)) && isnan(bp.tracker.calibration_based.via_pwv.sbp.signal(i))
            disp(['No relative BP computed at spotcheck ' num2str(i) ' due to invalid inputs'])
            disp('(With respect to preceding calibration(s))')
        elseif ~isnan(bp.tracker.calibration_based.via_pwv.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.calibration_based.via_pwv.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.calibration_based.via_pwv.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding calibration(s))')
        elseif ~isnan(bp.tracker.calibration_based.via_pat.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.calibration_based.via_pat.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.calibration_based.via_pat.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding calibration(s))')
        end
        
    end

    %% File based tracker
    
    %Determine reference for single file-based tracker
    if strcmp(tracking_type, 'file')
        
        if reference_spotcheck_estimation == -99 % when band values should be used
            
                bp.tracker.file.reference.sbp_est = input.bp.tracker.band.reference.sbp_est.signal(1);
                bp.tracker.file.reference.dbp_est = input.bp.tracker.band.reference.dbp_est.signal(1);
                bp.tracker.file.reference.sbp_cal = input.bp.tracker.band.reference.sbp_cal.signal(1);
                bp.tracker.file.reference.dbp_cal = input.bp.tracker.band.reference.dbp_cal.signal(1);
                bp.tracker.file.reference.pat = input.bp.tracker.band.reference.pat.signal(1);
                %bp.tracker.file.reference.pat_foot = input.bp.tracker.band.reference.pat_foot.signal(1); not yet available in streams!
                bp.tracker.file.reference.pwv = height(i)./input.bp.tracker.file.reference.pat(1);
                %bp.tracker.file.reference.pwv_foot = height(i)./input.bp.tracker.band.reference.pat_foot.signal(1); not yet available in streams!                
                bp.tracker.file.reference.HR = input.bp.tracker.band.reference.HR.signal(1);
                
                %check variables for tracker 2.0 are available from band, otherwise assign NaNs
                if isfield(input.bp.tracker.band.reference, 'pp_ft_amp')     
                    bp.tracker.file.reference.pp_ft_amp = input.bp.tracker.band.reference.pp_ft_amp.signal(1);
                    bp.tracker.file.reference.upstgrad = input.bp.tracker.band.reference.upstgrad.signal(1);
                    bp.tracker.file.reference.RCtime = input.bp.tracker.band.reference.RCtime.signal(1);   
                else
                    bp.tracker.file.reference.pp_ft_amp = NaN;
                    bp.tracker.file.reference.upstgrad = NaN;
                    bp.tracker.file.reference.RCtime = NaN; 
                end            
            
        else %use defined spotcheck for reference values
            
            %check whether current spotcheck is supposed to be reference and add information
            if ismember(i, reference_spotcheck_estimation)
                bp.tracker.file.reference.sbp_est = [bp.tracker.file.reference.sbp_est, bp.sbp.signal(i)];
                bp.tracker.file.reference.dbp_est = [bp.tracker.file.reference.dbp_est, bp.dbp.signal(i)];
                bp.tracker.file.reference.pat = [bp.tracker.file.reference.pat, spotcheckVars.pat.signal(i)];
                bp.tracker.file.reference_pat_foot = [bp.tracker.file.reference.pat_foot, spotcheckVars.pat_foot.signal(i)];
                if heightValid
                    bp.tracker.file.reference.pwv = [bp.tracker.file.reference.pwv, height(i)./spotcheckVars.pat.signal(i)];
                    bp.tracker.file.reference_pwv_foot = [bp.tracker.file.reference.pwv_foot, height(i)./spotcheckVars.pat_foot.signal(i)];
                else
                    bp.tracker.file.reference.pwv = nan(1,1);
                    bp.tracker.file.reference.pwv_foot = nan(1,1);
                end
                bp.tracker.file.reference.HR = [bp.tracker.file.reference.HR, spotcheckVars.HR.signal(i)];
                bp.tracker.file.reference.pp_ft_amp = [bp.tracker.file.reference.pp_ft_amp, spotcheckVars.ppg_pp_ft_amp.signal(i)];
                bp.tracker.file.reference.upstgrad = [bp.tracker.file.reference.upstgrad, spotcheckVars.ppg_upstgrad.signal(i)];
                bp.tracker.file.reference.RCtime = [bp.tracker.file.reference.RCtime, spotcheckVars.RCtime.signal(i)];
            end
            
            %add cuff-based BP if available
            if isfield(input.bp, 'sbpref') %ppg independent bp field in root of data struct, containing cuff reference
                bp.tracker.file.reference.sbp_cal = input.bp.sbpref;
                bp.tracker.file.reference.dbp_cal = input.bp.dbpref;
            else
                bp.tracker.file.reference.sbp_cal = NaN;
                bp.tracker.file.reference.dbp_cal = NaN;
            end        
            
        end
        
    end
    
end

%% Compute output for file-based BP tracker

if strcmp(tracking_type, 'file')
    
    %first take averages of references
    bp.tracker.file.reference.sbp_est = nanmean(bp.tracker.file.reference.sbp_est);
    bp.tracker.file.reference.dbp_est = nanmean(bp.tracker.file.reference.dbp_est);
    %(...except for calibration, take first cuff reading...)
    bp.tracker.file.reference.sbp_cal = bp.tracker.file.reference.sbp_cal(reference_spotcheck_calibration);
    bp.tracker.file.reference.dbp_cal = bp.tracker.file.reference.dbp_cal(reference_spotcheck_calibration);
    
    bp.tracker.file.reference.pat = nanmean(bp.tracker.file.reference.pat);
    bp.tracker.file.reference.pat_foot = nanmean(bp.tracker.file.reference.pat_foot);
    bp.tracker.file.reference.pwv = nanmean(bp.tracker.file.reference.pwv);
    bp.tracker.file.reference.pwv_foot = nanmean(bp.tracker.file.reference.pwv_foot);    
    bp.tracker.file.reference.HR = nanmean(bp.tracker.file.reference.HR);
    bp.tracker.file.reference.pp_ft_amp = nanmean(bp.tracker.file.reference.pp_ft_amp);
    bp.tracker.file.reference.upstgrad = nanmean(bp.tracker.file.reference.upstgrad);
    bp.tracker.file.reference.RCtime = nanmean(bp.tracker.file.reference.RCtime);
    
    bp.tracker.file.reference.spotcheck = reference_spotcheck_estimation;
    
    %shorten variable names for better overview
    ref_sbp_est = bp.tracker.file.reference.sbp_est;
    ref_dbp_est = bp.tracker.file.reference.dbp_est;
    ref_sbp_cal = bp.tracker.file.reference.sbp_cal;
    ref_dbp_cal = bp.tracker.file.reference.dbp_cal;
    ref_pat = bp.tracker.file.reference.pat;
    ref_pwv = bp.tracker.file.reference.pwv;
    ref_HR = bp.tracker.file.reference.HR;
    %ref_risetime = [];
    ref_pp_ft_amp = bp.tracker.file.reference.pp_ft_amp;
    ref_upstgrad = bp.tracker.file.reference.upstgrad;
    ref_RCtime = bp.tracker.file.reference.RCtime;
    
    
    %compute tracked BP per spotcheck
    for i = 1:nSpotchecks
                
        diff_pat = bp.tracker.this_reference.pat.signal(i) - ref_pat;
        diff_pwv = bp.tracker.this_reference.pwv.signal(i) - ref_pwv;
        diff_hr = bp.tracker.this_reference.HR.signal(i) - ref_HR;
        %diff_risetime = bp.tracker.this_reference.risetime.signal - ref_risetime; %currently not used (either offline or in band)
        diff_pp_ft_amp = bp.tracker.this_reference.pp_ft_amp.signal(i) - ref_pp_ft_amp;
        diff_upstgrad = bp.tracker.this_reference.upstgrad.signal(i) - ref_upstgrad;
        diff_RCtime = bp.tracker.this_reference.RCtime.signal(i) - ref_RCtime;
        
        %compute relative changes between between current file/spotcheck and estimation reference
        %pat_based
        bp.tracker.file.estimation_based.via_pat.sbp.signal(i) = ref_sbp_est + glm_alpha_sbp_pat + diff_pat.*glm_PAT_SBP_coeff_pat + diff_hr.*glm_HR_SBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pat + diff_upstgrad.*glm_upstgrad_SBP_coeff_pat + diff_RCtime.*glm_RCtime_SBP_coeff_pat;
        bp.tracker.file.estimation_based.via_pat.sbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        bp.tracker.file.estimation_based.via_pat.dbp.signal(i) = ref_dbp_est + glm_alpha_dbp_pat + diff_pat.*glm_PAT_DBP_coeff_pat + diff_hr.*glm_HR_DBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pat + diff_upstgrad.*glm_upstgrad_DBP_coeff_pat + diff_RCtime.*glm_RCtime_DBP_coeff_pat;
        bp.tracker.file.estimation_based.via_pat.dbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        %pwv_based
        bp.tracker.file.estimation_based.via_pwv.sbp.signal(i) = ref_sbp_est + glm_alpha_sbp_pwv + diff_pwv.*glm_PWV_SBP_coeff_pwv + diff_hr.*glm_HR_SBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_SBP_coeff_pwv + diff_RCtime.*glm_RCtime_SBP_coeff_pwv;
        bp.tracker.file.estimation_based.via_pwv.sbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        bp.tracker.file.estimation_based.via_pwv.dbp.signal(i) = ref_dbp_est + glm_alpha_dbp_pwv + diff_pwv.*glm_PWV_DBP_coeff_pwv + diff_hr.*glm_HR_DBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_DBP_coeff_pwv + diff_RCtime.*glm_RCtime_DBP_coeff_pwv;
        bp.tracker.file.estimation_based.via_pwv.dbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        %compute relative changes between between current file/spotcheck and calibration reference
        %pat_based
        bp.tracker.file.calibration_based.via_pat.sbp.signal(i) = ref_sbp_cal + glm_alpha_sbp_pat  + diff_pat.*glm_PAT_SBP_coeff_pat + diff_hr.*glm_HR_SBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pat + diff_upstgrad.*glm_upstgrad_SBP_coeff_pat + diff_RCtime.*glm_RCtime_SBP_coeff_pat;
        bp.tracker.file.calibration_based.via_pat.sbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        bp.tracker.file.calibration_based.via_pat.dbp.signal(i) = ref_dbp_cal + glm_alpha_dbp_pat + diff_pat.*glm_PAT_DBP_coeff_pat + diff_hr.*glm_HR_DBP_coeff_pat + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pat + diff_upstgrad.*glm_upstgrad_DBP_coeff_pat + diff_RCtime.*glm_RCtime_DBP_coeff_pat;
        bp.tracker.file.calibration_based.via_pat.dbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        %pwv_based
        bp.tracker.file.calibration_based.via_pwv.sbp.signal(i) = ref_sbp_cal + glm_alpha_sbp_pwv + diff_pwv.*glm_PWV_SBP_coeff_pwv + diff_hr.*glm_HR_SBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_SBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_SBP_coeff_pwv + diff_RCtime.*glm_RCtime_SBP_coeff_pwv;
        bp.tracker.file.calibration_based.via_pwv.sbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        bp.tracker.file.calibration_based.via_pwv.dbp.signal(i) = ref_dbp_cal + glm_alpha_dbp_pwv + diff_pwv.*glm_PWV_DBP_coeff_pwv + diff_hr.*glm_HR_DBP_coeff_pwv + diff_pp_ft_amp.*glm_pp_ft_amp_DBP_coeff_pwv + diff_upstgrad.*glm_upstgrad_DBP_coeff_pwv + diff_RCtime.*glm_RCtime_DBP_coeff_pwv;
        bp.tracker.file.calibration_based.via_pwv.dbp.timestamps(i) = spotcheckVars.timestamps(i);
        
        
        %Tracked BP wrt estimation
        if isnan(bp.tracker.file.estimation_based.via_pat.sbp.signal(i)) && isnan(bp.tracker.file.estimation_based.via_pwv.sbp.signal(i))
            disp(['No relative BP computed at spotcheck ' num2str(i) ' due to invalid inputs'])
            disp('(With respect to preceding estimation(s))')
        elseif ~isnan(bp.tracker.file.estimation_based.via_pwv.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.file.estimation_based.via_pwv.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.file.estimation_based.via_pwv.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding estimation(s))')
        elseif ~isnan(bp.tracker.file.estimation_based.via_pat.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.file.estimation_based.via_pat.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.file.estimation_based.via_pat.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding estimation(s))')
        end
        
        %Tracked BP wrt calibration
        if  isnan(bp.tracker.file.calibration_based.via_pat.sbp.signal(i)) && isnan(bp.tracker.file.calibration_based.via_pwv.sbp.signal(i))
            disp(['No relative BP computed at spotcheck ' num2str(i) ' due to invalid inputs'])
            disp('(With respect to preceding calibration(s))')
        elseif ~isnan(bp.tracker.file.calibration_based.via_pwv.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.file.calibration_based.via_pwv.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.file.calibration_based.via_pwv.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding calibration(s))')
        elseif ~isnan(bp.tracker.file.calibration_based.via_pat.sbp.signal(i))
            disp(['The relative SBP for channel ' chan ' is: ' num2str(bp.tracker.file.calibration_based.via_pat.sbp.signal(i)') ' and the relative DBP is: ' num2str(bp.tracker.file.calibration_based.via_pat.dbp.signal(i)') ' at spotcheck ' num2str(i)])
            disp('(With respect to preceding calibration(s))')
        end
        
    end
    
end

%disp('end of BP_FE.m')
input.ppg.(chan).bp = bp;
%% Plot command outside script

%figure, hold on, plot(data.ppg.e.bp.tracker.file.estimation_based.via_pwv.sbp.timestamps, data.ppg.e.bp.tracker.file.estimation_based.via_pwv.sbp.signal, 'ro-'), plot(data.ppg.e.bp.sbp.timestamps, data.ppg.e.bp.sbp.signal, 'bo-'), xlabel('time'), ylabel('SBP'), legend('tracker', 'absolute')

