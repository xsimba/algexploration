function convertCsv2NDataStructs(n)
% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : convertCsv2NDataStructs.m
%  Content    : 2 convert csv files with multiple spotchecks
%  Version    : 0.1
%  Package    : SIMBA
%  Created by : E. Hermeling (evelien.hermeling@imec-nl.nl)
%  Date       : 17-03-2015
%  Modification and Version History:
%  | Developer    | Version |    Date    |     Changes      |
%  |
%
% Input: asks user folder that contains csv-files (within subfolders)
% Output: mat-files with data-struct that can be used by AllIn.m
%
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clc

% by default each csv-file is split into 3 data-structs (if possible)
if nargin==0
    n = 3;
end


% folder may contain subfolder and other (not relevant files).
% (will process only csv-files)
folder = uigetdir;
cd(folder);
filesInFolder = dir('*.*');

filesInFolder = filesInFolder(3:end);
splitCsvAndSaveAsMat(filesInFolder,n)
%loop through all files and folders


function splitCsvAndSaveAsMat(files,nSpotChecksDefault)
mainFolder = pwd;

% max- and minimal spot check duration
maxDurationSC  = 55; % in sample points;
minDurationSC  = 20; % in sample points;

for f = 1:length(files)
    if files(f).isdir
        % run function for the subfolder.
        cd(files(f).name)
        filesInFolder = dir('*.*');
        filesInFolder = filesInFolder(3:end);
        splitCsvAndSaveAsMat(filesInFolder,nSpotChecksDefault)
        cd(mainFolder);
        
    elseif strcmp(files(f).name(end-2:end),'csv')
        %load data
        disp(files(f).name)
        data2 = getSimbandCsv(files(f).name);
        
        % the end of each csv name contains spot checks that needs to be
        % included
        
        locUnderscore = strfind(files(f).name,'_');
        locUnderscore = locUnderscore(end);
        nSpotChecks = min(length(files(f).name(1:end-4)) - locUnderscore,nSpotChecksDefault);
        
        spotChecks2Include = zeros(nSpotChecks,1);
        
        for spotC = 1:nSpotChecks
            spotChecks2Include(spotC) = str2double(files(f).name(locUnderscore+spotC));
        end
     
        fileName = files(f).name(1:locUnderscore-1);
        
        
        
        % find spot check end-flags (3)
        % spot check ends just before
        if isfield(data2,'spot_check')
            endSC = find(data2.spot_check.signal==3)-1;
            beginSC = NaN*zeros(size(endSC));
            
            
            for spotC = 1:length(endSC)
                % identify start of spot check (last time it was 2)
                % spot check start just after
                bSC = find(data2.spot_check.signal(1:endSC(spotC))==2,1,'last')+1;
                if isempty(bSC)
                    if endSC(spotC)< maxDurationSC
                        beginSC(spotC) = 1;
                    else
                        beginSC(spotC)  =   NaN;
                        endSC(spotC)    =   NaN;
                    end
                else
                    beginSC(spotC) = bSC;
                end
                if all(data2.spot_check.signal(bSC:endSC(spotC))==1) && ...  % during spot check flag should be 1
                        (endSC(spotC)-beginSC(spotC)> minDurationSC) && (endSC(spotC)-beginSC(spotC)< maxDurationSC) % spot check should have certain length
                    
                    disp('ok')
                else
                    disp('not ok')
                    % (probably) not a good spot check
                    % remove spot check
                    beginSC(spotC)  =   NaN;
                    endSC(spotC)    =   NaN;
                    
                end
            end
            beginSC = beginSC(~isnan(beginSC));            
            endSC = endSC(~isnan(endSC));
            spotChecks = 1:length(beginSC);
            
              
            if length(beginSC) >= max(spotChecks2Include)
                
                % in case number of spot checks exceeds those indicate in
                % filename, use latest spot checks (last term zero if OK)            
                spotChecks2IncludeShifted = spotChecks2Include + (length(beginSC)-max(spotChecks2Include));
                
                % split data2 struct
                for spotC = 1 : nSpotChecks
                    data = splitCSVsimple(data2,beginSC(spotChecks2IncludeShifted(spotC)),endSC(spotChecks2IncludeShifted(spotC)));
                    save( cat(2,fileName,'_',num2str(spotC),'.mat'),'data')
                    disp(cat(2,'saving: ',fileName,'_',num2str(spotC),'.mat'))
                end
                
            else
               %keyboard
                % not organized as expected
            end
            
        end
    else
        % not a folder or csv-file
    end
end



