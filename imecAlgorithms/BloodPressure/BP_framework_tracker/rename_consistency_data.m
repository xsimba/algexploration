%%Rename files of consistency experiment

clear all
close all hidden
clc

%desired output date format
formatOut = 'yyyymmdd'; 

%folder containing the data
folder = '\\winnl\simba\Databases\data_collection_consistency_experiment\SIMBAND data\SP0006\';

%look for file names
files = dir(folder);

%copy and rename the files according to format SP00xx_C_yyyymmdd_A_SI_123 
for ii = 3:length(files)-1
    creation_date = files(ii,1).datenum;
    creation_date = ['_' datestr(creation_date,formatOut) '_'];
    
    current_file_name = files(ii,1).name;
    day = ['_' current_file_name(10) current_file_name(11) '_'];
    
    new_file_name = regexprep(current_file_name,day,creation_date);
    
    old = [folder current_file_name];
    new = [folder 'renamed\' new_file_name];
    
    copyfile(old,new);
end