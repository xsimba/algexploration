%%Rename files of consistency experiment

clear all
close all hidden
clc


for i=1:5
    
    if i+12<10
        %desired subject_id
        new_subject_id = ['SP000' num2str(i+12)]; 
    else
        new_subject_id = ['SP00' num2str(i+12)];
    end
        
    
    %folder containing the data
    folder = ['C:\Users\beutel18\Dropbox\Work\simba\simband_data_consistency_experiment\0013-0017_original_imec_BE_v1.3\SP000' num2str(i) '\'];
    
    %look for file names
    files = dir(folder);
    
    %rename the files wrt subject id
    for ii = 3:length(files)
        
        current_file_name = files(ii,1).name;
        [current_subject_id, remain] = strtok(current_file_name, '_');
        
        new_file_name = regexprep(current_file_name, current_subject_id, new_subject_id)
        
        old = [folder current_file_name];
        new = [folder '..\' new_subject_id '\' new_file_name];
        
        copyfile(old,new);
    end
    
end