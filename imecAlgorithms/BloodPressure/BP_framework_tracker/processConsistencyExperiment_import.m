% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : processConsistencyExperiment_import_FE.m
%  Content    : Wrapper for loading Simband and spreadsheet data, execution
%  of all algorithms and generating overviews and error plots of the data
%  Version    : 1.0
%  Package    : SIMBA
%  Created by : F.Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 17-6-2015
%  Modification and Version History:
%  | Developer  | Version |    Date    |   Changes				      											|
%  | F.Beutel   |   1.0   |  17-06-15  
%  | E.Hermeling|   1.1   |  24-06-15  |   Made compatible with feature extraction (replaced PAT)               |

%1)First rename the csv files if necessary, e.g. to get from day to date
%2)Then split them in three .mat files using "convertCsv2NDataStructs.m'
%3)Then use this script to run through all algorithm chain while BP Tracker
%is in unspecified database mode --> [data] = BP_FE(data, 'e', 'database'); 


%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc; warning off;

% example multiple reference files
ref_daytime = {'M','M','M'};
ref_trial_nr = [1 2 3];
ref_day = [1 1 1];

% example single reference files
% ref_daytime = {'M'};
% ref_trial_nr = [1];
% ref_day = [1];

N_subjects = 17; %17
start_subject = 1; %1
%Explanation:
    %no data recorded
%good_subjects = []; 

subjects = {'0001', '0002', '0003', '0004', '0005', '0006', '0007', '0008', '0009', '0010', ...
            '0011', '0012', '0013', '0014', '0015', '0016', '0017'};

database = 'simband_data_consistency_experiment';

%spreadsheet_data
BP_data = xlsread(['PAT-based_BP_estimation_SSB_and_BP_consistency.xlsx'], 'BP Data');
SSB_data = xlsread(['PAT-based_BP_estimation_SSB_and_BP_consistency.xlsx'], 'SSB');

metrics_matrix =[];

for i = start_subject:N_subjects
     clear ref_Spotchecks
    subject_nr = i;
    subject_id = subjects(i);
    subject_id_str = ['SP' subject_id{1}];
    bp_data_column_offset = subject_nr + 5;    % column offset ex: subject 1 start at the 6th column
    ssb_data_column_offset =1;
    
    %look for date of first measurement available
    start_idx = find(~isnan(BP_data(:,bp_data_column_offset)));
    
    if isempty(start_idx)
        disp(['No data found for subject ', num2str(subject_nr)])
        
    else
        
        start_date = BP_data(start_idx(1)+1,1);
        
        %define number of days as integers (first colum contains date)
        day = BP_data(start_idx(1):end,1);
        %find all days
        day_idx = find(~isnan(day));
        
        %assign day integer (e.g. 20150311) to all fields
        for j = day_idx(1:end)'
            day(j-1:j+25) = day(j)*ones(length(day(j-1:j+25)),1); %27 is number of rows per day in spreadsheet
        end
        
        %1D vector containing SBP,DBP and HR
        parameters = BP_data(start_idx:end,bp_data_column_offset);
        
        %2D matrix containing SBP,DBP and HR as colums, respectively
        parameters = reshape(parameters,3,length(parameters)/3)';
        
        %resample date vector
        day = reshape(day,3,length(day)/3)'; %from 27 rows per day...
        day = day(:,1); %...to 3
        day_str = num2str(day);
        date_num = datenum(day_str,'yyyymmdd'); %error if subject does not start a consistency experiment in the morning!!!
        
        %add all to metrics matrix
        metrics_matrix = [date_num parameters nan*ones(length(parameters),18)];        
        
        %find all files in subject directory
        subject_id_files = what(subject_id_str); %'what' only works when subject id is a folder name where files are located!
        subject_id_all_mat_files = subject_id_files.mat;
        subject_id_path = subject_id_files.path
        
        %iterate over all files to add metadata info for BP tracker in database mode
        for k = 1:length(subject_id_all_mat_files)
            
                file_name = subject_id_all_mat_files{k};
                display(file_name);
                
                file_date_num = datenum(file_name(10:17),'yyyymmdd');
                file_daytime = file_name(19);
                trial_nr = str2double(file_name(end-4));
                
                load([subject_id_path '\' file_name]);
                data.context.daytime  = {file_daytime};
                data.context.trial_nr = [trial_nr];
                data.context.date     = [str2double(file_name(10:17))];
                data.context.day      = [file_date_num - date_num(1)+1];
                data.filename = file_name;
                data.context.posture = {'sitting'};
                data.ssb.age.signal = [SSB_data(1,subject_nr+ssb_data_column_offset)];
                data.ssb.height.signal = [SSB_data(2,subject_nr+ssb_data_column_offset)];
                data.ssb.weight.signal = [SSB_data(3,subject_nr+ssb_data_column_offset)];
                
                %execute all algorithms with BP to add all references
                [data] = executeAllAlgorithms(data,0,1);
                
                %add BP reference information from metrics matrix to respective fields
                %in the data structure for BP tracker
                
                    %first determine where to access metrics matrix...
                    row_idx = find(metrics_matrix(:,1)==file_date_num,1);
                    idx_offset = 0;

                    if(file_daytime == 'M' && trial_nr == 1)                        
                        idx_offset = 0;                            
                    elseif (file_daytime == 'M' && trial_nr == 2)
                        idx_offset = 1;                  
                    elseif (file_daytime == 'M' && trial_nr == 3)
                        idx_offset = 2;   
                    elseif (file_daytime == 'L' && trial_nr == 1)
                        idx_offset = 3;     
                    elseif (file_daytime == 'L' && trial_nr == 2)
                        idx_offset = 4;     
                    elseif (file_daytime == 'L' && trial_nr == 3)
                        idx_offset = 5;    
                    elseif (file_daytime == 'A' && trial_nr == 1)
                        idx_offset = 6;  
                    elseif (file_daytime == 'A' && trial_nr == 2)
                        idx_offset = 7;   
                    elseif (file_daytime == 'A' && trial_nr == 3)
                        idx_offset = 8;
                    end
                    
                    for rr = 1:length(ref_daytime);
                        if (file_daytime == ref_daytime{rr}) && (trial_nr == ref_trial_nr(rr))&& ...
                                (data.context.day == ref_day(rr))
                            ref_Spotchecks(rr).file_name = file_name;
                            ref_Spotchecks(rr).daytime = ref_daytime{rr};
                            ref_Spotchecks(rr).trial_nr = ref_trial_nr(rr);
                            ref_Spotchecks(rr).day = ref_day(rr);
                            ref_Spotchecks(rr).context = data.context;
                        end
                    end
                    SBP_ref = metrics_matrix(row_idx+idx_offset,2);
                    DBP_ref = metrics_matrix(row_idx+idx_offset,3);
                    HR_ref  = metrics_matrix(row_idx+idx_offset,4);
                    
                    %besides setting the cuff BP into the corresponding
                    %tracker field for calibration, also add it to the root
                    %of the data struct --> it is not replaced by BP
                    %tracker assignment of 'this_reference'
                    data.bp.sbpref = SBP_ref;
                    data.bp.dbpref = DBP_ref;
                    
                    %...then add fields to data struct (per channel)
                    channel_array = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
                    for l = 1:numel(channel_array)
                        chan = channel_array{l};
                        %check if signals were recorded for channel
                        if isfield(data.ppg, chan)                        
                            data.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal  = SBP_ref;
                            data.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.signal  = DBP_ref;
                            data.ppg.(chan).bp.tracker.this_reference.bp_cal.hr.signal  = HR_ref;
                        else
                            disp(['No metadata added for channel ', chan])
                        end
                    end
                %make directory and save file
                mkdir([subject_id_path, '\..\' subject_id_str '_imported'])
                save([subject_id_path, '\..\' subject_id_str '_imported\' file_name],'data'); 
                
                            
        end
       save([subject_id_path, '\..\' subject_id_str '_imported\' subject_id_str '_ID2ref_spotcheck'],'ref_Spotchecks');
    end   
  
end


