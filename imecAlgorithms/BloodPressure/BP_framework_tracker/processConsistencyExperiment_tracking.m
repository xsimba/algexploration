% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : processConsistencyExperiment_tracking.m
%  Content    : Wrapper for loading Simband and spreadsheet data, execution
%  of all algorithms and generating overviews and error plots of the data
%  Version    : 1.0
%  Package    : SIMBA
%  Created by : F.Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 17-6-2015
%  Modification and Version History:
%  | Developer  | Version |    Date    |     Changes      |
%  | F.Beutel   |   1.0   |  17-06-15  |                  |
%  | E.Hermeling|   1.1   |  24-06-15  |   Made compatible with feature extraction (replaced PAT)               |

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc; warning off;

N_subjects = 17; %17
start_subject = 1; %1
%Explanation:
    %no data recorded
%good_subjects = []; 

subjects = {'0001', '0002', '0003', '0004', '0005', '0006', '0007', '0008', '0009', '0010', ...
            '0011', '0012', '0013', '0014', '0015', '0016', '0017'};

database = 'simband_data_consistency_experiment';

metrics_matrix =[];

for i = start_subject:N_subjects
    
    subject_nr = i;
    subject_id = subjects(i);
    subject_id_str = ['SP' subject_id{1}];
             
        %find all files in IMPORTED subject directory (which needs to be on
        %Matlab path by then!)
        subject_id_files = what([subject_id_str, '_imported']); %'what' only works when subject id is a folder name where files are located!
        if numel(subject_id_files) == 0           
            disp(['No Data available for subject ' subject_id_str])
            subject_id_all_mat_files = [];
        else
            subject_id_all_mat_files = subject_id_files.mat;
            subject_id_path = subject_id_files.path
        end
        
        %iterate over all files to add metadata info for BP tracker in database mode
        for k = 1:length(subject_id_all_mat_files)
            
                file_name = subject_id_all_mat_files{k};
                display(file_name);
 
                load([subject_id_path '\' file_name]);
                %execute all algorithms with BP.m in specified database
                %mode for tracking (complete executeAllAlgorithms not necessary!)
                [data] = BP(data, 'e', 'database', 1); %only define that you want to use the reference spotchecks
                %[data] = executeAllAlgorithms_FE(data,0);
                
                %make directory and save file
                mkdir([subject_id_path, '\..\' subject_id_str '_tracked'])
                save([subject_id_path, '\..\' subject_id_str '_tracked\' file_name],'data');
        end              
end

