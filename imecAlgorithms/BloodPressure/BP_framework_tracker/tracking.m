clear all
close all hidden
clc
cd('C:\Users\garcia69\Desktop\Tracker analysis\Tracker leuven\SP0001\SPLIT\SP0001\SP0001\output');

load('SP0001_metrics_matrix.mat')

%% cuff triplet flag
cuff_flag = nan*ones(length(metrics_matrix),1);
off = (1:3:length(metrics_matrix))';

for xx = 1:3:length(metrics_matrix)
    min = nanmin(metrics_matrix(xx:xx+2,2));
    max = nanmax(metrics_matrix(xx:xx+2,2));

    if((max-min)>6.5)
    cuff_flag(xx:xx+2) = 130;
    end
    
end

metrics_matrix = [metrics_matrix, cuff_flag];

% create vector for daytime separation (better visualisation)
date = [];
[C,ia,ic] = unique(metrics_matrix(:,1));


for ii = ia' 
    %morning
    date(ii)   = ic(ii)+0.2;
    date(ii+1) = ic(ii)+0.24;
    date(ii+2) = ic(ii)+0.28;
    %lunch
    date(ii+3) = ic(ii)+0.4;
    date(ii+4) = ic(ii)+0.44;
    date(ii+5) = ic(ii)+0.48;
    %afternoon
    date(ii+6) = ic(ii)+0.6;
    date(ii+7) = ic(ii)+0.64;
    date(ii+8) = ic(ii)+0.68;
end

%introduce NaN inbetween days to segment data per day nr.
test = [];
newDate =[];

for ii = ia';
    
    a = metrics_matrix(ii:(ii+8),:);
    b = nan*ones(1,23);
    test = [test;a;b];
    
    c = date(ii:(ii+8))';
    newDate = [newDate;c;nan];
end

%% plot showing measured BP, estimated BP's, PAT and HR
track = figure;
ax1 = subplot(3,1,1);
plot(newDate,test(:,2),'o-','MarkerFaceColor','b'); hold on;
plot(newDate,test(:,17),'o-r','MarkerFaceColor','r');
plot(newDate,test(:,18),'o-k','MarkerFaceColor','k');
plot(newDate,test(:,23)+15,'+m','MarkerFaceColor','m');
ylim([100 150])
title('Tracking test (9 spot checks a day: 3 morning, 3 lunch and 3 evening)')
AX = legend('Reference','Estimated offset ','Calibrated offset'...
,'Ref. variation'...    
,'location','south','orientation','horizontal');
legend('boxoff')

LEG = findobj(AX,'type','text');
set(LEG,'FontSize',12)

ylabel('SBP [mmHg]')

ax2 = subplot(3,1,2);
plot(newDate,test(:,12),'o-','MarkerFaceColor','b')
ylabel('PAT [sec]')
ylim([0.20 0.50])

ax3 = subplot(3,1,3);
plot(newDate,test(:,4),'o-','MarkerFaceColor','b');hold on
plot(newDate,test(:,22),'o-r','MarkerFaceColor','r')
AX = legend('Reference HR','SIMBAND HR'...
,'location','south','orientation','horizontal');
legend('boxoff')

LEG = findobj(AX,'type','text');
set(LEG,'FontSize',12)

ylim([40 90])
ylabel('HR [BPM]')
xlabel('Day nr.')

linkaxes([ax1,ax2,ax3],'x')

orient landscape


%% trending error plot
std1 = nanstd(diff(metrics_matrix(:,17))-diff(metrics_matrix(:,2)));
std2 = nanstd(diff(metrics_matrix(:,18))-diff(metrics_matrix(:,2)));
mean1 = nanmean(diff(metrics_matrix(:,17))-diff(metrics_matrix(:,2)));
mean2 = nanmean(diff(metrics_matrix(:,18))-diff(metrics_matrix(:,2)));

t = newDate(2:end);

track = figure;
ax1=subplot(2,1,1)
plot(t,diff(test(:,2)),'o-','MarkerFaceColor','b'); hold on;
plot(t,diff(test(:,17)),'o-r','MarkerFaceColor','r');
plot(t,diff(test(:,18)),'o-k','MarkerFaceColor','k');
legend('Reference','Estimated offset','Calibrated offset',...
        'location','south','orientation','horizontal')
title({'Trending error';...
    ['Estimated offset: \mu = ' num2str(mean1) ',\sigma= ' num2str(std1)];...
    ['Calibrated offset: \mu = ' num2str(mean2) ',\sigma = ' num2str(std2)]})
ylabel('\Delta SBP [mmHg]')
ax2 = subplot(2,1,2)
plot(t,diff(test(:,17))-diff(test(:,2)),'o-r','MarkerFaceColor','r');hold on
plot(t,diff(test(:,18))-diff(test(:,2)),'o-k','MarkerFaceColor','k');

legend('Estimated offset','Calibrated offset',...
        'location','south','orientation','horizontal')
ylabel('\Delta SBP [mmHg]')
xlabel('Day nr.')
orient landscape

linkaxes([ax1,ax2],'x')

%% absolute plot
figure;
std1 = nanstd(test(:,17)-test(:,2));
std2 = nanstd(test(:,18)-test(:,2));
mean1 = nanmean(test(:,17)-test(:,2));
mean2 = nanmean(test(:,18)-test(:,2));

error = [7*ones(length(test(:,1)),1) -7*ones(length(test(:,1)),1)];

plot(newDate,test(:,17)-test(:,2),'o-r','MarkerFaceColor','r');hold on
plot(newDate,test(:,18)-test(:,2),'o-k','MarkerFaceColor','k');
plot(newDate,error,'-.','color',[150 150 150]/256);

title({'Absolute error';...
    ['Estimated offset: \mu = ' num2str(mean1) ',\sigma= ' num2str(std1)];...
    ['Calibrated offset: \mu = ' num2str(mean2) ',\sigma = ' num2str(std2)]})
legend('Estimated offset','Calibrated offset','7 mmHg',...
        'location','south','orientation','horizontal')
ylabel('SBP [mmHg]')
xlabel('Day nr.')



