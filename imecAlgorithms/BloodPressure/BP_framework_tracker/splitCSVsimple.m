function DATA = splitCSVsimple(data,SC_start_index,SC_end_index)

% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : splitCSVSimple.m
%  Content    : Splits data struct for different Spot checks and saved them to
%               .MAT. this is a simplified version of splitCSV
%               spot check identification is no longer in this file
%  Version    : 1.0
%  Package    : SIMBA
%  Created by : P. Pradhapan (paruthi.pradhapan@imec-nl.nl)
%  Date       : 09-02-2015
%  Modification and Version History:
%  | Developer     | Version |    Date    |     Changes      |
%  | P. Pradhapan  |   0.1   |  19-01-15  | Import .CSV and manually annotate to split Spot checks and save to .MAT |
%  | P. Pradhapan  |   0.5   |  09-02-15  | Automate splitting with SC terminating at 3 |
%  | E. Hermeling  |   1.0   |  17-03-15  | Remove spotcheck identification
%  | E. Hermeling  |   1.0   |  17-03-15  | split more data (ssb, gsr,
%    
%

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

SC_start_timestamps                        =    data.spot_check.timestamps(SC_start_index); % Find SC start time using the start index
SC_end_timestamps                          =    data.spot_check.timestamps(SC_end_index); % Find SC termination time using the termination index

% Reading the X position for SC based on ECG timestamp:
[~, ECG_start_index]              =    min(abs(data.timestamps - (SC_start_timestamps-2))); % Buffer 2 seconds before actual SC start
[~, ECG_end_index]                =    min(abs(data.timestamps - (SC_end_timestamps+2)));   % Buffer 2 seconds after actual SC end

% Splitting and writing the data to a new structure DATA:

% Timestamps:
DATA.timestamps                        =    data.timestamps(ECG_start_index:ECG_end_index);

% Accelerometer data:
if isfield(data, 'acc') == 1
    accChannels = fieldnames(data.acc);
    for ch = 1:length(accChannels)
        DATA.acc.(accChannels{ch}).signal  =    data.acc.(accChannels{ch}).signal(ECG_start_index:ECG_end_index);
    end
end

% ECG signal:
if isfield(data, 'ecg') == 1
    DATA.ecg.signal                    =    data.ecg.signal(ECG_start_index:ECG_end_index);
end

% PPG signal:
if isfield(data, 'ppg') == 1
    ppgChannels = fieldnames(data.ppg);
    for ch = 1:length(ppgChannels);
        if isfield(data.ppg.(ppgChannels{ch}),'signal');
            DATA.ppg.(ppgChannels{ch}).signal = data.ppg.(ppgChannels{ch}).signal(ECG_start_index:ECG_end_index);
        end
        if isfield(data.ppg.(ppgChannels{ch}),'agc_current')
            [~, agc_current_start_index]          =    min(abs(data.ppg.(ppgChannels{ch}).agc_current.timestamps - SC_start_timestamps));
            [~, agc_current_end_index]            =    min(abs(data.ppg.(ppgChannels{ch}).agc_current.timestamps - SC_end_timestamps));
            
            
            DATA.ppg.(ppgChannels{ch}).agc_current.timestamps     =   data.ppg.(ppgChannels{ch}).agc_current.timestamps(agc_current_start_index:agc_current_end_index);
            DATA.ppg.(ppgChannels{ch}).agc_current.signal        =   data.ppg.(ppgChannels{ch}).agc_current.signal(agc_current_start_index:agc_current_end_index);
            DATA.ppg.(ppgChannels{ch}).agc_current.startUnixTime  =   data.ppg.(ppgChannels{ch}).agc_current.startUnixTime;
        end
        
        
    end
    
end

% UNIX time:
DATA.startUnixTime                     =    data.startUnixTime;

% SSB
if isfield(data,'ssb')
    ssbChannels = fieldnames(data.ssb);
    for ch = 1:length(ssbChannels)
        
        % ssb timestamp should be within (near??) begin and end spot check
        ssb_index  =   find((data.ssb.weight.timestamps>SC_start_timestamps)& (data.ssb.weight.timestamps<SC_end_timestamps));
        DATA.ssb.(ssbChannels{ch}).startUnixTime   =  data.ssb.(ssbChannels{ch}).startUnixTime;
        if ~isempty(ssb_index)
            DATA.ssb.(ssbChannels{ch}).timestamp      =  data.ssb.(ssbChannels{ch}).timestamps(ssb_index);
            DATA.ssb.(ssbChannels{ch}).signal         =  data.ssb.(ssbChannels{ch}).signal(ssb_index);
        else
            DATA.ssb.(ssbChannels{ch}).timestamp    = [];
            DATA.ssb.(ssbChannels{ch}).signal       = [];
        end
    end
    
end

% band_DBP:
if isfield(data,'band_DBP') == 1
    band_DBP_index  =   find((data.band_SBP.timestamps>SC_start_timestamps)& (data.band_SBP.timestamps<SC_end_timestamps));
    DATA.band_DBP.startUnixTime    =    data.band_DBP.startUnixTime;
    if ~isempty(band_DBP_index)
        DATA.band_DBP.timestamps       =    data.band_DBP.timestamps(band_DBP_index);
        DATA.band_DBP.signal           =    data.band_DBP.signal(band_DBP_index);
        
    else % Else fill empty
        DATA.band_DBP.timestamps       =    [];
        DATA.band_DBP.signal           =    [];
    end
end

%band_SBP:
if isfield(data,'band_SBP') == 1
    band_SBP_index  =   find((data.band_SBP.timestamps>SC_start_timestamps)& (data.band_SBP.timestamps<SC_end_timestamps));
    DATA.band_SBP.startUnixTime    =    data.band_SBP.startUnixTime;
    if ~isempty(band_SBP_index)
        DATA.band_SBP.timestamps       =    data.band_SBP.timestamps(band_SBP_index);
        DATA.band_SBP.signal           =    data.band_SBP.signal(band_SBP_index);
        
    else % Else fill empty
        DATA.band_SBP.timestamps       =    [];
        DATA.band_SBP.signal           =    [];
        
    end
end

%gsr
if isfield(data,'gsr')
    [~, gsr_start_index]   =    min(abs(data.gsr.gain.timestamps - SC_start_timestamps));
    [~, gsr_end_index]     =    min(abs(data.gsr.gain.timestamps - SC_end_timestamps));
    
    DATA.gsr.gain.timestamps    =    data.gsr.gain.timestamps(gsr_start_index:gsr_end_index);
    DATA.gsr.gain.signal        =    data.gsr.gain.signal(gsr_start_index:gsr_end_index);
    DATA.gsr.gain.startUnixTime =    data.gsr.gain.startUnixTime;
end


%     %band_pat_bp:
if isfield(data,'band_pat')
    % NEEDS TO BE VERIFIED (QUESTIONNAIRE 1)
    [~,index_pat_start]        =    min(abs(data.band_pat.timestamps - SC_start_timestamps)); % Finding first position within SC range
    [~,index_pat_end]          =    min(abs(data.band_pat.timestamps - SC_end_timestamps)); % Finding first position outside SC range and moving back 1 step
    
%     if isempty(index_pat_end) == 1
%         index_pat_end = length(data.band_pat.timestamps);
%     end
    %
    DATA.band_pat.timestamps           =    data.band_pat.timestamps(index_pat_start:index_pat_end);
    DATA.band_pat.startUnixTime        =    data.band_pat.startUnixTime;
    DATA.band_pat.signal               =    data.band_pat.signal(index_pat_start:index_pat_end);
end
%band_pat_info:                                                        
if isfield(data,'band_pat_info') == 1
    [~, pat_info_start_index]          =    min(abs(data.band_pat_info.timestamps - SC_start_timestamps));
    [~, pat_info_end_index]            =    min(abs(data.band_pat_info.timestamps - SC_end_timestamps));
    
    DATA.band_pat_info.timestamps      =    data.band_pat_info.timestamps(pat_info_start_index:pat_info_end_index);
    DATA.band_pat_info.startUnixTime   =    data.band_pat_info.startUnixTime;    % NEEDS TO BE VERIFIED (QUESTIONNAIRE 1)
    DATA.band_pat_info.signal          =    data.band_pat_info.signal(pat_info_start_index:pat_info_end_index);
end

%context:
if isfield(data,'context') == 1
    [~, context_start_index]           =    min(abs(data.context.posture.timestamps - SC_start_timestamps));
    [~, context_end_index]             =    min(abs(data.context.posture.timestamps - SC_end_timestamps));
    
    DATA.context.posture.timestamps    =    data.context.posture.timestamps(context_start_index:context_end_index);
    DATA.context.posture.startUnixTime =    data.context.posture.startUnixTime;    % NEEDS TO BE VERIFIED (QUESTIONNAIRE 1)
    DATA.context.posture.signal        =    data.context.posture.signal(context_start_index:context_end_index);
end

% Skin temperature:
if isfield(data,'skintemp') == 1
    [~, skintemp_start_index]          =    min(abs(data.skintemp.timestamps - SC_start_timestamps));
    [~, skintemp_end_index]            =    min(abs(data.skintemp.timestamps - SC_end_timestamps));
    
    DATA.skintemp.timestamps           =    data.skintemp.timestamps(skintemp_start_index:skintemp_end_index);
    DATA.skintemp.startUnixTime        =    data.skintemp.startUnixTime;   % NEEDS TO BE VERIFIED (QUESTIONNAIRE 1)
    DATA.skintemp.signal               =    data.skintemp.signal(skintemp_start_index:skintemp_end_index);
end

%Spot check:
DATA.spot_check.timestamps             =    data.spot_check.timestamps(SC_start_index:SC_end_index);
DATA.spot_check.startUnixTime          =    data.spot_check.startUnixTime;  % NEEDS TO BE VERIFIED (QUESTIONNAIRE 1)
DATA.spot_check.signal                 =    data.spot_check.signal(SC_start_index:SC_end_index);




