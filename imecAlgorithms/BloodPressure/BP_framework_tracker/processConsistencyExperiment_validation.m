% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : processConsistencyExperiment_validation_FE.m
%  Content    : Wrapper for loading Simband and spreadsheet data, execution
%  of all algorithms and generating overviews and error plots of the data
%  Version    : 1.0
%  Package    : SIMBA
%  Created by : F.Beutel (fabian.beutel@imec-nl.nl)
%  Date       : 17-6-2015
%  Modification and Version History:
%  | Developer  | Version |    Date    |     Changes      														|
%  | F.Beutel   |   1.0   |  19-06-15  |                  														|
%  | E.Hermeling|   1.1   |  24-06-15  |   Made compatible with feature extraction (replaced PAT)               |

%Use this script to generate the outcome metrics of the analysis

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc; warning off;

chan = 'e';

N_subjects = 17; %17
start_subject = 1; %1
%Explanation:
%no data recorded
%good_subjects = [];

subjects = {'0001', '0002', '0003', '0004', '0005', '0006', '0007', '0008', '0009', '0010', ...
    '0011', '0012', '0013', '0014', '0015', '0016', '0017'};

database = 'simband_data_consistency_experiment';

metrics_matrix =[];

%to be filled with the following metrics

%columns
%1:subject ID
%2:trial_nr
%3:gender
%4:age
%5:height
%6:weight
%7:SBP measured
%8:DBP measured
%9:SimbandID
%10:SBPest,Simband
%11:DBPest,Simband
%12:SBPest,absolute BP
%13:DBPest,absolute BP
%14:SBPest,track,PWV
%15:DBPest,track,PWV
%16:SBPest,track,PAT
%17:DBPest,track,PAT
%18:SBPcal,track,PWV
%19:DBPcal,track,PWV
%20:SBPcal,track,PAT
%21:DBPcal,track,PAT
%22:PAT
%23:HR
%24:PAT_CI
%25:PPG_CI_raw
%26:ECG_CI_raw
%27:risetime_avg
%28:risetime_sd
%29:risetime_sd_all_spotchecks
%30:number of files for subject
%31:number of spotchecks for subject
%32:number of spotchecks in last file
%33:number of calibration spotchecks (= number of files)
%absolute errors%(only PWV errors added)
%34:error_SBP_est_track_PWV
%35:error_SBP_est_track_PWV_abs
%36:error_SBP_est_track_PWV_mean_abs
%37:error_SBP_est_track_PWV_mean_abs_rest
%38:error_SBP_est_track_PWV_mean_abs_rest_exerc
%39:error_DBP_est_track_PWV
%40:error_DBP_est_track_PWV_abs
%41:error_DBP_est_track_PWV_mean_abs
%42:error_DBP_est_track_PWV_mean_abs_rest
%43:error_DBP_est_track_PWV_mean_abs_rest_exerc
%44:error_SBP_cal_track_PWV
%45:error_SBP_cal_track_PWV_abs
%46:error_SBP_cal_track_PWV_mean_abs
%47:error_SBP_cal_track_PWV_mean_abs_rest
%48:error_SBP_cal_track_PWV_mean_abs_rest_exerc
%49:error_DBP_cal_track_PWV
%50:error_DBP_cal_track_PWV_abs
%51:error_DBP_cal_track_PWV_mean_abs
%52:error_DBP_cal_track_PWV_mean_abs_rest
%53:error_DBP_cal_track_PWV_mean_abs_rest_exerc
%relative errors
%54:diff_error_SBP_est_track_PWV
%55:diff_error_SBP_est_track_PWV_abs
%56:diff_error_SBP_est_track_PWV_mean_abs
%57:diff_error_SBP_est_track_PWV_mean_abs_rest
%58:diff_error_SBP_est_track_PWV_mean_abs_rest_exerc
%59:diff_error_DBP_est_track_PWV
%60:diff_error_DBP_est_track_PWV_abs
%61:diff_error_DBP_est_track_PWV_mean_abs
%62:diff_error_DBP_est_track_PWV_mean_abs_rest
%63:diff_error_DBP_est_track_PWV_mean_abs_rest_exerc
%64:diff_error_SBP_cal_track_PWV
%65:diff_error_SBP_cal_track_PWV_abs
%66:diff_error_SBP_cal_track_PWV_mean_abs
%67:diff_error_SBP_cal_track_PWV_mean_abs_rest
%68:diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc
%69:diff_error_DBP_cal_track_PWV
%70:diff_error_DBP_cal_track_PWV_abs
%71:diff_error_DBP_cal_track_PWV_mean_abs
%72:diff_error_DBP_cal_track_PWV_mean_abs_rest
%73:diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc

%74:PP_SP_int_avg
%75:date
%76:overall_spotcheck_nr
%77:risetime_difference
%78:risetime_difference_abs
%79:PWV
%80:daytime

%81:error_SBP_no_tracking
%82:error_DBP_no_tracking

%83:delta_PAT
%84:delta_PWV
%85:delta_HR
%86:delta_SBP
%87:delta_DBP
%88:delta_pp_ft_amp
%89:delta_upstgrad
%90:delta_RC_time

%91:day

%92:PATfoot
%93:delta_PATfoot

for i = start_subject:N_subjects
    
    metrics_matrix_subject = [];
    
    subject_nr = i;
    subject_id = subjects(i);
    subject_id_str = ['SP' subject_id{1}];
    
    %find all files in TRACKED subject directory (which needs to be on
    %Matlab path by then!)
    subject_id_files = what([subject_id_str, '_tracked']); %'what' only works when subject id is a folder name where files are located!
    if numel(subject_id_files) == 0
        disp(['No Data available for subject ' subject_id_str])
        subject_id_all_mat_files = [];
       
        %insert NaNs for all fields in metrics matrix
        subject = [str2num(subject_id{1})];
        trial_nr = [NaN];
        gender = [NaN];
        age = [NaN];
        height = [NaN];
        weight = [NaN];
        SBP_meas = [NaN];
        DBP_meas = [NaN];
        SimbandID = [NaN];
        Simband_SBP = [NaN];
        Simband_DBP = [NaN];
        SBP_est_abs = [NaN];
        DBP_est_abs = [NaN];
        SBP_est_track_PWV = [NaN];
        DBP_est_track_PWV = [NaN];
        SBP_est_track_PAT = [NaN];
        DBP_est_track_PAT = [NaN];
        SBP_cal_track_PWV = [NaN];
        DBP_cal_track_PWV = [NaN];
        SBP_cal_track_PAT = [NaN];
        DBP_cal_track_PAT = [NaN];
        PAT = [NaN];
        HR = [NaN];
        spotcheck_CI = [NaN];
        PPG_CI_raw = [NaN];
        ECG_CI_raw = [NaN];
        risetime_avg = [NaN];
        risetime_sd = [NaN];
        risetime_sd_all_spotchecks = [NaN];
        number_of_files_for_subject = [NaN]; %not required, UCSF4 only
        number_of_spotchecks_for_subject = [NaN]; %not required, UCSF4 only
        number_of_spotchecks_last_file = [NaN]; %not required, UCSF4 only
        number_of_calibration_spotchecks = [NaN]; %not required, UCSF4 only
        error_SBP_est_track_PWV = [NaN];
        error_SBP_est_track_PWV_abs = [NaN];
        error_SBP_est_track_PWV_mean_abs = [NaN];
        error_SBP_est_track_PWV_mean_abs_rest = [NaN];
        error_SBP_est_track_PWV_mean_abs_rest_exerc = [NaN];
        error_DBP_est_track_PWV = [NaN];
        error_DBP_est_track_PWV_abs = [NaN];
        error_DBP_est_track_PWV_mean_abs = [NaN];
        error_DBP_est_track_PWV_mean_abs_rest = [NaN];
        error_DBP_est_track_PWV_mean_abs_rest_exerc = [NaN];
        error_SBP_cal_track_PWV = [NaN];
        error_SBP_cal_track_PWV_abs = [NaN];
        error_SBP_cal_track_PWV_mean_abs = [NaN];
        error_SBP_cal_track_PWV_mean_abs_rest = [NaN];
        error_SBP_cal_track_PWV_mean_abs_rest_exerc = [NaN];
        error_DBP_cal_track_PWV = [NaN];
        error_DBP_cal_track_PWV_abs = [NaN];
        error_DBP_cal_track_PWV_mean_abs = [NaN];
        error_DBP_cal_track_PWV_mean_abs_rest = [NaN];
        error_DBP_cal_track_PWV_mean_abs_rest_exerc = [NaN];
        diff_error_SBP_est_track_PWV = [NaN];
        diff_error_SBP_est_track_PWV_abs = [NaN];
        diff_error_SBP_est_track_PWV_mean_abs = [NaN];
        diff_error_SBP_est_track_PWV_mean_abs_rest = [NaN];
        diff_error_SBP_est_track_PWV_mean_abs_rest_exerc = [NaN];
        diff_error_DBP_est_track_PWV = [NaN];
        diff_error_DBP_est_track_PWV_abs = [NaN];
        diff_error_DBP_est_track_PWV_mean_abs = [NaN];
        diff_error_DBP_est_track_PWV_mean_abs_rest = [NaN];
        diff_error_DBP_est_track_PWV_mean_abs_rest_exerc = [NaN];
        diff_error_SBP_cal_track_PWV = [NaN];
        diff_error_SBP_cal_track_PWV_abs = [NaN];
        diff_error_SBP_cal_track_PWV_mean_abs = [NaN];
        diff_error_SBP_cal_track_PWV_mean_abs_rest = [NaN];
        diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc = [NaN];
        diff_error_DBP_cal_track_PWV = [NaN];
        diff_error_DBP_cal_track_PWV_abs = [NaN];
        diff_error_DBP_cal_track_PWV_mean_abs = [NaN];
        diff_error_DBP_cal_track_PWV_mean_abs_rest = [NaN];
        diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc = [NaN];
        
        PP_SP_int_avg = [NaN];
        date_spotcheck = [NaN];
        overall_spotcheck_nr = [NaN];     
        delta_risetime = [NaN];
        delta_risetime_abs = [NaN]; 
        PWV = [NaN];
        daytime = [NaN];
        
        error_SBP_no_tracking_abs = [NaN];
        error_DBP_no_tracking_abs = [NaN];
        
        delta_PAT = [NaN];
        delta_PWV = [NaN];
        delta_HR = [NaN];
        delta_SBP = [NaN];
        delta_DBP = [NaN];          
        delta_pp_ft_amp = [NaN];
        delta_upstgrad = [NaN];
        delta_RCtime = [NaN];
        
        day = [NaN];
        
        PATfoot = [NaN];
        delta_PATfoot = [NaN];
        
    else
        subject_id_all_mat_files = subject_id_files.mat;
        subject_id_path = subject_id_files.path
        
        %empty all metrics
        subject = [];
        trial_nr = [];
        gender = [];
        age = [];
        height = [];
        weight = [];
        SBP_meas = [];
        DBP_meas = [];
        SimbandID = [];
        Simband_SBP = [];
        Simband_DBP = [];
        SBP_est_abs = [];
        DBP_est_abs = [];
        SBP_est_track_PWV = [];
        DBP_est_track_PWV = [];
        SBP_est_track_PAT = [];
        DBP_est_track_PAT = [];
        SBP_cal_track_PWV = [];
        DBP_cal_track_PWV = [];
        SBP_cal_track_PAT = [];
        DBP_cal_track_PAT = [];
        PAT = [];
        HR = [];
        spotcheck_CI = [];
        PPG_CI_raw = [];
        ECG_CI_raw = [];
        risetime_avg = [];
        risetime_sd = [];
        risetime_sd_all_spotchecks = [];
        number_of_files_for_subject = []; %not required, UCSF4 only
        number_of_spotchecks_for_subject = []; %not required, UCSF4 only
        number_of_spotchecks_last_file = []; %not required, UCSF4 only
        number_of_calibration_spotchecks = []; %not required, UCSF4 only
        %(only PWV errors added)
        %absolute errors
        error_SBP_est_track_PWV = [];
        error_SBP_est_track_PWV_abs = [];
        error_SBP_est_track_PWV_mean_abs = [];
        error_SBP_est_track_PWV_mean_abs_rest = [];
        error_SBP_est_track_PWV_mean_abs_rest_exerc = [];
        error_DBP_est_track_PWV = [];
        error_DBP_est_track_PWV_abs = [];
        error_DBP_est_track_PWV_mean_abs = [];
        error_DBP_est_track_PWV_mean_abs_rest = [];
        error_DBP_est_track_PWV_mean_abs_rest_exerc = [];
        error_SBP_cal_track_PWV = [];
        error_SBP_cal_track_PWV_abs = [];
        error_SBP_cal_track_PWV_mean_abs = [];
        error_SBP_cal_track_PWV_mean_abs_rest = [];
        error_SBP_cal_track_PWV_mean_abs_rest_exerc = [];
        error_DBP_cal_track_PWV = [];
        error_DBP_cal_track_PWV_abs = [];
        error_DBP_cal_track_PWV_mean_abs = [];
        error_DBP_cal_track_PWV_mean_abs_rest = [];
        error_DBP_cal_track_PWV_mean_abs_rest_exerc = [];
        %relative errors
        diff_error_SBP_est_track_PWV = [];
        diff_error_SBP_est_track_PWV_abs = [];
        diff_error_SBP_est_track_PWV_mean_abs = [];
        diff_error_SBP_est_track_PWV_mean_abs_rest = [];
        diff_error_SBP_est_track_PWV_mean_abs_rest_exerc = [];
        diff_error_DBP_est_track_PWV = [];
        diff_error_DBP_est_track_PWV_abs = [];
        diff_error_DBP_est_track_PWV_mean_abs = [];
        diff_error_DBP_est_track_PWV_mean_abs_rest = [];
        diff_error_DBP_est_track_PWV_mean_abs_rest_exerc = [];
        diff_error_SBP_cal_track_PWV = [];
        diff_error_SBP_cal_track_PWV_abs = [];
        diff_error_SBP_cal_track_PWV_mean_abs = [];
        diff_error_SBP_cal_track_PWV_mean_abs_rest = [];
        diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc = [];
        diff_error_DBP_cal_track_PWV = [];
        diff_error_DBP_cal_track_PWV_abs = [];
        diff_error_DBP_cal_track_PWV_mean_abs = [];
        diff_error_DBP_cal_track_PWV_mean_abs_rest = [];
        diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc = [];
        %no delta errors added here!
        
        PP_SP_int_avg = [];
        date_spotcheck = [];
        overall_spotcheck_nr = []; 
        delta_risetime = [];
        delta_risetime_abs = [];  
        PWV = [];
        daytime = [];
        
        error_SBP_no_tracking_abs = [];
        error_DBP_no_tracking_abs = [];        
        
        delta_PAT = [];
        delta_PWV = [];
        delta_HR = [];
        delta_SBP = [];
        delta_DBP = [];   
        delta_pp_ft_amp = [];
        delta_upstgrad = [];        
        delta_RCtime = [];
        
        day = [];
        
        PATfoot = [];
        delta_PATfoot = [];        
        
    end
    
    %iterate over all files to extract metrics, generate plots and
    %error/performance metrics
    if numel(subject_id_files) ~= 0
        
        %first sort files properly in chronological order
        subject_id_all_mat_files_sorted = [];
        for ii = 1:length(subject_id_all_mat_files)
            current_file = subject_id_all_mat_files{ii};
            current_file_date = str2num(current_file(10:17));
            current_file_daytime = double(current_file(19));
            current_file_trial = str2num(current_file(end-4));
            
            if ii == 1
                subject_id_all_mat_files_sorted = [subject_id_all_mat_files_sorted; {current_file}];
            else
                last_file_date = str2num(last_file(10:17));
                last_file_daytime = double(last_file(19));
                last_file_trial = str2num(last_file(end-4));
                
                if current_file_date > last_file_date
                    subject_id_all_mat_files_sorted = [subject_id_all_mat_files_sorted; {current_file}];
                elseif current_file_date < last_file_date
                    subject_id_all_mat_files_sorted = [{current_file}; subject_id_all_mat_files_sorted];
                elseif current_file_date == last_file_date
                    %use inverse order of M(77), L(76), and A(65) to insert filename
                    if current_file_daytime < last_file_daytime
                        subject_id_all_mat_files_sorted = [subject_id_all_mat_files_sorted; {current_file}];
                    elseif current_file_daytime > last_file_daytime %insert before last three files (only happens when daytime changes)
                        subject_id_all_mat_files_sorted = [subject_id_all_mat_files_sorted(1:last_file_index-3); {current_file}; subject_id_all_mat_files_sorted(last_file_index-2:end)];
                    elseif current_file_daytime == last_file_daytime
                        if current_file_trial > last_file_trial %insert after last file
                            subject_id_all_mat_files_sorted = [subject_id_all_mat_files_sorted(1:last_file_index); {current_file}; subject_id_all_mat_files_sorted(last_file_index+1:end)];
                        elseif current_file_trial < last_file_trial %insert before last file
                            subject_id_all_mat_files_sorted = [subject_id_all_mat_files_sorted(1:last_file_index-1); {current_file}; subject_id_all_mat_files_sorted(last_file_index:end)];
                        end
                    end
                end
            end
            last_file = current_file; 
            last_file_index = find(ismember(subject_id_all_mat_files_sorted, last_file));
        end
        
        subject_id_all_mat_files = subject_id_all_mat_files_sorted;
        
        for k = 1:length(subject_id_all_mat_files)
            
            file_name = subject_id_all_mat_files{k};
            display(file_name);
            load([subject_id_path '\' file_name]);
            
            %define/compute all necessary metrics (similar to UCSF4)
            
            subject = [subject, str2num(subject_id{1})];
            trial_nr = [trial_nr, data.context.trial_nr];
            gender = [gender, NaN];
            age = [age, data.ssb.age.signal];
            height = [height, data.ssb.height.signal];
            weight = [weight, data.ssb.weight.signal];
            SBP_meas = [SBP_meas, data.bp.sbpref];
            DBP_meas = [DBP_meas, data.bp.dbpref];
            SimbandID = [SimbandID, NaN]; %not required, UCSF4 only
            Simband_SBP = [Simband_SBP, NaN]; %not required, UCSF4 only
            Simband_DBP = [Simband_DBP, NaN]; %not required, UCSF4 only
            SBP_est_abs = [SBP_est_abs, data.ppg.(chan).bp.sbp.signal ];
            DBP_est_abs = [DBP_est_abs, data.ppg.(chan).bp.dbp.signal ];
            SBP_est_track_PWV = [SBP_est_track_PWV, data.ppg.(chan).bp.tracker.estimation_based.via_pwv.sbp.signal];
            DBP_est_track_PWV = [DBP_est_track_PWV, data.ppg.(chan).bp.tracker.estimation_based.via_pwv.dbp.signal];
            SBP_est_track_PAT = [SBP_est_track_PAT, data.ppg.(chan).bp.tracker.estimation_based.via_pat.sbp.signal];
            DBP_est_track_PAT = [DBP_est_track_PAT, data.ppg.(chan).bp.tracker.estimation_based.via_pat.dbp.signal];
            SBP_cal_track_PWV = [SBP_cal_track_PWV, data.ppg.(chan).bp.tracker.calibration_based.via_pwv.sbp.signal];
            DBP_cal_track_PWV = [DBP_cal_track_PWV, data.ppg.(chan).bp.tracker.calibration_based.via_pwv.dbp.signal];
            SBP_cal_track_PAT = [SBP_cal_track_PAT, data.ppg.(chan).bp.tracker.calibration_based.via_pat.sbp.signal];
            DBP_cal_track_PAT = [DBP_cal_track_PAT, data.ppg.(chan).bp.tracker.calibration_based.via_pat.dbp.signal];
            PAT = [PAT, data.ppg.(chan).spotcheck.pat.signal];
            PATfoot = [PATfoot, data.ppg.(chan).spotcheck.pat_foot.signal];
            HR = [HR, data.ppg.(chan).spotcheck.HR.signal];
            spotcheck_CI = [spotcheck_CI, data.ppg.(chan).spotcheck.CI];
            PPG_CI_raw = [PPG_CI_raw, data.ppg.(chan).spotcheck.CIppg.signal];
            ECG_CI_raw = [ECG_CI_raw, data.ppg.(chan).spotcheck.CIecg.signal]; %not used yet, to be averaged by feature extraction
            risetime_avg = [risetime_avg, data.ppg.(chan).spotcheck.risetime.signal];
            risetime_sd = [risetime_sd, data.ppg.(chan).spotcheck.risetime.sd];
            risetime_sd_all_spotchecks = [risetime_sd_all_spotchecks, nanstd(risetime_sd)];
            number_of_files_for_subject = [number_of_files_for_subject, NaN]; %not required, UCSF4 only
            number_of_spotchecks_for_subject = [number_of_spotchecks_for_subject, NaN]; %not required, UCSF4 only
            number_of_spotchecks_last_file = [number_of_spotchecks_last_file, NaN]; %not required, UCSF4 only
            number_of_calibration_spotchecks = [number_of_calibration_spotchecks, NaN]; %not required, UCSF4 only
            %(only PWV errors added)
            %absolute errors
            error_SBP_est_track_PWV = [error_SBP_est_track_PWV, SBP_est_track_PWV(end) - SBP_meas(end)];
            error_SBP_est_track_PWV_abs = [error_SBP_est_track_PWV_abs, abs(error_SBP_est_track_PWV(end))];
            error_SBP_est_track_PWV_mean_abs = [error_SBP_est_track_PWV_mean_abs, nanmean(error_SBP_est_track_PWV_abs)];
            error_SBP_est_track_PWV_mean_abs_rest = [error_SBP_est_track_PWV_mean_abs_rest, NaN];%not required, UCSF4 only
            error_SBP_est_track_PWV_mean_abs_rest_exerc = [error_SBP_est_track_PWV_mean_abs_rest_exerc, NaN];%not required, UCSF4 only
            
            error_DBP_est_track_PWV = [error_DBP_est_track_PWV, DBP_est_track_PWV(end) - DBP_meas(end)];
            error_DBP_est_track_PWV_abs = [error_DBP_est_track_PWV_abs, abs(error_DBP_est_track_PWV(end))];
            error_DBP_est_track_PWV_mean_abs = [error_DBP_est_track_PWV_mean_abs, nanmean(error_DBP_est_track_PWV_abs)];
            error_DBP_est_track_PWV_mean_abs_rest = [error_DBP_est_track_PWV_mean_abs_rest, NaN];%not required, UCSF4 only
            error_DBP_est_track_PWV_mean_abs_rest_exerc = [error_DBP_est_track_PWV_mean_abs_rest_exerc, NaN];%not required, UCSF4 only
            
            %first 3 elements are required in order to calculate their
            %average for calibration, so corresponding errors are invalid
            if numel(SBP_meas) <= 3
                
                error_SBP_cal_track_PWV = [error_SBP_cal_track_PWV, NaN];
                error_SBP_cal_track_PWV_abs = [error_SBP_cal_track_PWV_abs, NaN];
                error_SBP_cal_track_PWV_mean_abs = [error_SBP_cal_track_PWV_mean_abs, NaN];
                error_SBP_cal_track_PWV_mean_abs_rest = [error_SBP_cal_track_PWV_mean_abs_rest, NaN];%not required, UCSF4 only
                error_SBP_cal_track_PWV_mean_abs_rest_exerc = [error_SBP_cal_track_PWV_mean_abs_rest_exerc, NaN];%not required, UCSF4 only
                
                error_DBP_cal_track_PWV = [error_DBP_cal_track_PWV, NaN];
                error_DBP_cal_track_PWV_abs = [error_DBP_cal_track_PWV_abs, NaN];
                error_DBP_cal_track_PWV_mean_abs = [error_DBP_cal_track_PWV_mean_abs, NaN];
                error_DBP_cal_track_PWV_mean_abs_rest = [error_DBP_cal_track_PWV_mean_abs_rest, NaN];%not required, UCSF4 only
                error_DBP_cal_track_PWV_mean_abs_rest_exerc = [error_DBP_cal_track_PWV_mean_abs_rest_exerc, NaN];%not required, UCSF4 only
                
            else
                
                error_SBP_cal_track_PWV = [error_SBP_cal_track_PWV, SBP_cal_track_PWV(end) - SBP_meas(end)];
                error_SBP_cal_track_PWV_abs = [error_SBP_cal_track_PWV_abs, abs(error_SBP_cal_track_PWV(end))];
                error_SBP_cal_track_PWV_mean_abs = [error_SBP_cal_track_PWV_mean_abs, nanmean(error_SBP_cal_track_PWV_abs)];
                error_SBP_cal_track_PWV_mean_abs_rest = [error_SBP_cal_track_PWV_mean_abs_rest, NaN];%not required, UCSF4 only
                error_SBP_cal_track_PWV_mean_abs_rest_exerc = [error_SBP_cal_track_PWV_mean_abs_rest_exerc, NaN];%not required, UCSF4 only
                
                error_DBP_cal_track_PWV = [error_DBP_cal_track_PWV, DBP_cal_track_PWV(end) - DBP_meas(end)];
                error_DBP_cal_track_PWV_abs = [error_DBP_cal_track_PWV_abs, abs(error_DBP_cal_track_PWV(end))];
                error_DBP_cal_track_PWV_mean_abs = [error_DBP_cal_track_PWV_mean_abs, nanmean(error_DBP_cal_track_PWV_abs)];
                error_DBP_cal_track_PWV_mean_abs_rest = [error_DBP_cal_track_PWV_mean_abs_rest, NaN];%not required, UCSF4 only
                error_DBP_cal_track_PWV_mean_abs_rest_exerc = [error_DBP_cal_track_PWV_mean_abs_rest_exerc, NaN];%not required, UCSF4 only
            
            end
            
            PP_SP_int_avg = [PP_SP_int_avg, data.ppg.(chan).spotcheck.pp_sp_int.signal];
            date_spotcheck = [date_spotcheck, data.context.date];
            day = [day, data.context.day];
            overall_spotcheck_nr = [overall_spotcheck_nr, k];  
            delta_risetime = [delta_risetime, data.ppg.(chan).bp.tracker.this_reference.risetime.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_risetime];
            delta_risetime_abs = [delta_risetime_abs, abs(delta_risetime(end))]; 
            PWV = [PWV, data.ssb.height.signal./100./data.ppg.(chan).spotcheck.pat.signal];
            daytime = [daytime, double(file_name(19))];
            
            %first 3 elements are required in order to calculate their
            %average, so corresponding errors are invalid
            if numel(SBP_meas) <= 3
                error_SBP_no_tracking_abs = [error_SBP_no_tracking_abs, NaN];
                error_DBP_no_tracking_abs = [error_DBP_no_tracking_abs, NaN];                       
            else 
                error_SBP_no_tracking_abs = [error_SBP_no_tracking_abs, abs(data.bp.sbpref - mean(SBP_meas([1 2 3])))];
                error_DBP_no_tracking_abs = [error_DBP_no_tracking_abs, abs(data.bp.dbpref - mean(DBP_meas([1 2 3])))];       
            end
            
            
            delta_PAT = [delta_PAT, data.ppg.(chan).bp.tracker.this_reference.pat.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_pat];                    
            delta_PATfoot = [delta_PATfoot, data.ppg.(chan).bp.tracker.this_reference.pat_foot.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_pat_foot];                                     
            delta_PWV = [delta_PWV, data.ppg.(chan).bp.tracker.this_reference.pwv.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_pwv];
            delta_HR = [delta_HR, data.ppg.(chan).bp.tracker.this_reference.HR.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_HR];
            delta_SBP = [delta_SBP, data.ppg.(chan).bp.tracker.this_reference.bp_cal.sbp.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_sbp];
            delta_DBP = [delta_DBP, data.ppg.(chan).bp.tracker.this_reference.bp_cal.dbp.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_dbp];             
            delta_pp_ft_amp = [delta_pp_ft_amp, data.ppg.(chan).bp.tracker.this_reference.pp_ft_amp.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_pp_ft_amp];             
            delta_upstgrad = [delta_upstgrad, data.ppg.(chan).bp.tracker.this_reference.upstgrad.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_upstgrad];
            delta_RCtime = [delta_RCtime, data.ppg.(chan).bp.tracker.this_reference.RCtime.signal - data.ppg.(chan).bp.tracker.calibration_based.reference_RCtime];                                     
            
        end
    end
    
    
    
    if numel(subject_id_files) ~= 0
        
        %relative errors (outside the loop because of diff function
        diff_error_SBP_est_track_PWV = [diff_error_SBP_est_track_PWV, NaN, diff(SBP_est_track_PWV) - diff(SBP_meas)];
        diff_error_SBP_est_track_PWV_abs = [diff_error_SBP_est_track_PWV_abs, abs(diff_error_SBP_est_track_PWV)];
        diff_error_SBP_est_track_PWV_mean_abs = [diff_error_SBP_est_track_PWV_mean_abs, ones(1,length(error_SBP_est_track_PWV)).*nanmean(diff_error_SBP_est_track_PWV_abs)];
        diff_error_SBP_est_track_PWV_mean_abs_rest = [diff_error_SBP_est_track_PWV_mean_abs_rest, ones(1,length(error_SBP_est_track_PWV)).*NaN];%not required, UCSF4 only
        diff_error_SBP_est_track_PWV_mean_abs_rest_exerc = [diff_error_SBP_est_track_PWV_mean_abs_rest_exerc, ones(1,length(error_SBP_est_track_PWV)).*NaN];%not required, UCSF4 only
        
        diff_error_DBP_est_track_PWV = [diff_error_DBP_est_track_PWV, NaN, diff(DBP_est_track_PWV) - diff(DBP_meas)];
        diff_error_DBP_est_track_PWV_abs = [diff_error_DBP_est_track_PWV_abs, abs(diff_error_DBP_est_track_PWV)];
        diff_error_DBP_est_track_PWV_mean_abs = [diff_error_DBP_est_track_PWV_mean_abs, ones(1,length(error_DBP_est_track_PWV)).*nanmean(diff_error_DBP_est_track_PWV_abs)];
        diff_error_DBP_est_track_PWV_mean_abs_rest = [diff_error_DBP_est_track_PWV_mean_abs_rest, ones(1,length(error_DBP_est_track_PWV)).*NaN];%not required, UCSF4 only
        diff_error_DBP_est_track_PWV_mean_abs_rest_exerc = [diff_error_DBP_est_track_PWV_mean_abs_rest_exerc, ones(1,length(error_DBP_est_track_PWV)).*NaN];%not required, UCSF4 only
        
        diff_error_SBP_cal_track_PWV = [diff_error_SBP_cal_track_PWV, NaN, diff(SBP_cal_track_PWV) - diff(SBP_meas)];
        diff_error_SBP_cal_track_PWV_abs = [diff_error_SBP_cal_track_PWV_abs, abs(diff_error_SBP_cal_track_PWV)];
        diff_error_SBP_cal_track_PWV_mean_abs = [diff_error_SBP_cal_track_PWV_mean_abs, ones(1,length(error_SBP_cal_track_PWV)).*nanmean(diff_error_SBP_cal_track_PWV_abs)];
        diff_error_SBP_cal_track_PWV_mean_abs_rest = [diff_error_SBP_cal_track_PWV_mean_abs_rest, ones(1,length(error_SBP_cal_track_PWV)).*NaN];%not required, UCSF4 only
        diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc = [diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc, ones(1,length(error_SBP_cal_track_PWV)).*NaN];%not required, UCSF4 only
        
        diff_error_DBP_cal_track_PWV = [diff_error_DBP_cal_track_PWV, NaN, diff(DBP_cal_track_PWV) - diff(DBP_meas)];
        diff_error_DBP_cal_track_PWV_abs = [diff_error_DBP_cal_track_PWV_abs, abs(diff_error_DBP_cal_track_PWV)];
        diff_error_DBP_cal_track_PWV_mean_abs = [diff_error_DBP_cal_track_PWV_mean_abs, ones(1,length(error_DBP_cal_track_PWV)).*nanmean(diff_error_DBP_cal_track_PWV_abs)];
        diff_error_DBP_cal_track_PWV_mean_abs_rest = [diff_error_DBP_cal_track_PWV_mean_abs_rest, ones(1,length(error_DBP_cal_track_PWV)).*NaN];%not required, UCSF4 only
        diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc = [diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc, ones(1,length(error_DBP_cal_track_PWV)).*NaN];%not required, UCSF4 only
        
    end
    
    %% Generate plots per subject before qualification
   
    plotting_pre_qual = 0;
    
    if plotting_pre_qual == 1;
        
        figure('Name',['Tracking Subject Pre-Qual', subject_id{1}]); clf; hold on;
        %tracking trajectories
        
        %SBP
        subplot(4,4, [1 2 5 6]), hold on
        plot(SBP_meas,'r'); %reference BP
        plot(SBP_est_abs', 'm:o'); %SBP_est_abs
        plot(SBP_est_track_PWV', 'b-o'); %est PWV based tracker SBP
        plot(SBP_est_track_PAT','b--o'); %est PAT based tracker SBP
        plot(SBP_cal_track_PWV', 'g-o'); %cal PWV based tracker SBP
        plot(SBP_cal_track_PAT','g--o'); %cal PAT based tracker SBP
        if ~isnan(SBP_meas(1))
            plot(ones(1,numel(SBP_meas)).*nanmean(SBP_meas([1 2 3])), 'r--'); %first SBP calibration
        end
        legend('SBP_{cuff}', 'SBP_{absolute}', 'SBP_{tracker, est, PWV}', 'SBP_{tracker, est, PAT}', 'SBP_{tracker, cal, PWV}', 'SBP_{tracker, cal, PAT}')
        xlabel('Spotcheck #')
        ylabel('SBP [mmHg]')
        
        %absolute errors boxplot
        subplot(4,4, [3]), hold on
        title('Absolute Tracking Errors SBP');
        boxplot([error_SBP_est_track_PWV_abs', error_SBP_cal_track_PWV_abs'],{'est,PWV', 'cal,PWV'});
        scatter(1, error_SBP_est_track_PWV_mean_abs(end), 'r', 'fill');
        scatter(2, error_SBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
        %MAD in plot
        text(1+0.2, error_SBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_SBP_est_track_PWV_mean_abs(end))])
        text(2+0.2, error_SBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_SBP_cal_track_PWV_mean_abs(end))])
        % %of estimations below threshold
        %text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
        %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
        hline(7)
        ylabel('Absolute Deviation [mmHg]')
        
        %relative errors boxplot
        %         subplot(4,4, [7]), hold on
        %         title('Relative Tracking Errors SBP');
        %         boxplot([diff_error_SBP_est_track_PWV_abs'],{'est/cal, PWV'});
        %         scatter(1, diff_error_SBP_est_track_PWV_mean_abs(end), 'r', 'fill');
        %         scatter(2, diff_error_SBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
        %         %MAD in plot
        %         text(1+0.2, diff_error_SBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_SBP_est_track_PWV_mean_abs(end))])
        %         text(2+0.2, diff_error_SBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_SBP_cal_track_PWV_mean_abs(end))])
        %         % %of estimations below threshold
        %         %text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
        %         %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
        %         hline(7)
        %         ylabel('Absolute Deviation [mmHg]')
        
        %errors over spotchecks
        subplot(4,4, [7]), hold on
        title('Tracking Error SBP cal,PWV');
        bar(1:numel(error_SBP_cal_track_PWV_abs), error_SBP_cal_track_PWV_abs, 'b')
        scatter(1:numel(error_SBP_cal_track_PWV_mean_abs), error_SBP_cal_track_PWV_mean_abs, 'r', 'fill')
        xlabel('Spotcheck #')
        ylabel('error [mmHg]')
        
        %DBP
        subplot(4,4, [9 10 13 14]), hold on
        plot(DBP_meas,'r'); %reference BP
        plot(DBP_est_abs', 'm:o'); %DBP_est_abs
        plot(DBP_est_track_PWV', 'b-o'); %est PWV based tracker DBP
        plot(DBP_est_track_PAT','b--o'); %est PAT based tracker DBP
        plot(DBP_cal_track_PWV', 'g-o'); %cal PWV based tracker DBP
        plot(DBP_cal_track_PAT','g--o'); %cal PAT based tracker DBP
        if ~isnan(SBP_meas(1))
            plot(ones(1,numel(DBP_meas)).*DBP_meas(1), 'r--'); %first SBP calibration
        end
        legend('DBP_{cuff}', 'DBP_{absolute}', 'DBP_{tracker, est, PWV}', 'DBP_{tracker, est, PAT}', 'DBP_{tracker, cal, PWV}', 'DBP_{tracker, cal, PAT}')
        xlabel('Spotcheck #')
        ylabel('DBP [mmHg]')
        
        %absolute errors boxplot
        subplot(4,4, [11]), hold on
        title('Absolute Tracking Errors DBP');
        boxplot([error_DBP_est_track_PWV_abs', error_DBP_cal_track_PWV_abs'],{'est, PWV','cal, PWV'});
        scatter(1, error_DBP_est_track_PWV_mean_abs(end), 'r', 'fill');
        scatter(3, error_DBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
        %MAD in plot
        text(1+0.2, error_DBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_DBP_est_track_PWV_mean_abs(end))])
        text(2+0.2, error_DBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_DBP_cal_track_PWV_mean_abs(end))])
        % %of estimations below threshold
        %text(1+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
        %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
        hline(7)
        ylabel('Absolute Deviation [mmHg]')
        
        %relative errors boxplot
        %         subplot(4,4, [15]), hold on
        %         title('Relative Tracking Errors DBP');
        %         boxplot([diff_error_DBP_est_track_PWV_abs'],{'est/cal, PWV'});
        %         scatter(1, diff_error_DBP_est_track_PWV_mean_abs(end), 'r', 'fill');
        %         scatter(3, diff_error_DBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
        %         %MAD in plot
        %         text(1+0.2, diff_error_DBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_DBP_est_track_PWV_mean_abs(end))])
        %         text(2+0.2, diff_error_DBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_DBP_cal_track_PWV_mean_abs(end))])
        %         % %of estimations below threshold
        %         %text(1+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
        %         %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
        %         hline(7)
        %         ylabel('Absolute Deviation [mmHg]')
        
        %errors over spotchecks
        subplot(4,4, [15]), hold on
        title('Tracking Error DBP cal,PWV');
        bar(1:numel(error_DBP_cal_track_PWV_abs), error_DBP_cal_track_PWV_abs, 'b')
        scatter(1:numel(error_DBP_cal_track_PWV_mean_abs), error_DBP_cal_track_PWV_mean_abs, 'r', 'fill')
        xlabel('Spotcheck #')
        ylabel('error [mmHg]')
        
        %signals and CIs
        subplot(4,4, [4]), hold on, plot(PAT), plot(risetime_avg); legend('PAT', 'Risetime'), xlabel('Spotcheck #'), ylabel('[s]');
        subplot(4,4, [8]), plot(HR), xlabel('Spotcheck #'), ylabel('HR [bpm]');
        subplot(4,4, [12]), hold on, plot(spotcheck_CI), plot(PPG_CI_raw), legend('Spotcheck CI', 'PPG CI'), xlabel('Spotcheck #'), ylabel('CI'), axis([0 numel(spotcheck_CI) 1 5]);
        subplot(4,4, [16]), plot(PP_SP_int_avg), xlabel('Spotcheck #'), ylabel('PP-SP int [s]');
        
        %save tracking figure per subject
        savefig([subject_id_path, '\..\..\outcome_analysis\', subject_id_str, '_tracking_trajectory'])
        
    end
      
    %% Append calculated quantities of subject to overall metrics_matrix

        metrics_matrix = [metrics_matrix;
            subject', ...
            trial_nr', ...
            gender', ...
            age', ...
            height', ...
            weight', ...
            SBP_meas', ...
            DBP_meas', ...
            SimbandID', ...
            Simband_SBP', ...
            Simband_DBP', ...
            SBP_est_abs', ...
            DBP_est_abs', ...
            SBP_est_track_PWV', ...
            DBP_est_track_PWV', ...
            SBP_est_track_PAT', ...
            DBP_est_track_PAT', ...
            SBP_cal_track_PWV', ...
            DBP_cal_track_PWV', ...
            SBP_cal_track_PAT', ...
            DBP_cal_track_PAT', ...
            PAT', ...
            HR', ...
            spotcheck_CI', ...
            PPG_CI_raw', ...
            ECG_CI_raw', ...
            risetime_avg', ...
            risetime_sd', ...
            risetime_sd_all_spotchecks', ...
            number_of_files_for_subject', ...
            number_of_spotchecks_for_subject', ...
            number_of_spotchecks_last_file', ...
            number_of_calibration_spotchecks', ...
            error_SBP_est_track_PWV', ...
            error_SBP_est_track_PWV_abs', ...
            error_SBP_est_track_PWV_mean_abs', ...
            error_SBP_est_track_PWV_mean_abs_rest', ...
            error_SBP_est_track_PWV_mean_abs_rest_exerc', ...
            error_DBP_est_track_PWV', ...
            error_DBP_est_track_PWV_abs', ...
            error_DBP_est_track_PWV_mean_abs', ...
            error_DBP_est_track_PWV_mean_abs_rest', ...
            error_DBP_est_track_PWV_mean_abs_rest_exerc', ...
            error_SBP_cal_track_PWV', ...
            error_SBP_cal_track_PWV_abs', ...
            error_SBP_cal_track_PWV_mean_abs', ...
            error_SBP_cal_track_PWV_mean_abs_rest', ...
            error_SBP_cal_track_PWV_mean_abs_rest_exerc', ...
            error_DBP_cal_track_PWV', ...
            error_DBP_cal_track_PWV_abs', ...
            error_DBP_cal_track_PWV_mean_abs', ...
            error_DBP_cal_track_PWV_mean_abs_rest', ...
            error_DBP_cal_track_PWV_mean_abs_rest_exerc', ...
            diff_error_SBP_est_track_PWV', ...
            diff_error_SBP_est_track_PWV_abs', ...
            diff_error_SBP_est_track_PWV_mean_abs', ...
            diff_error_SBP_est_track_PWV_mean_abs_rest', ...
            diff_error_SBP_est_track_PWV_mean_abs_rest_exerc', ...
            diff_error_DBP_est_track_PWV', ...
            diff_error_DBP_est_track_PWV_abs', ...
            diff_error_DBP_est_track_PWV_mean_abs', ...
            diff_error_DBP_est_track_PWV_mean_abs_rest', ...
            diff_error_DBP_est_track_PWV_mean_abs_rest_exerc', ...
            diff_error_SBP_cal_track_PWV', ...
            diff_error_SBP_cal_track_PWV_abs', ...
            diff_error_SBP_cal_track_PWV_mean_abs', ...
            diff_error_SBP_cal_track_PWV_mean_abs_rest', ...
            diff_error_SBP_cal_track_PWV_mean_abs_rest_exerc', ...
            diff_error_DBP_cal_track_PWV', ...
            diff_error_DBP_cal_track_PWV_abs', ...
            diff_error_DBP_cal_track_PWV_mean_abs', ...
            diff_error_DBP_cal_track_PWV_mean_abs_rest', ...
            diff_error_DBP_cal_track_PWV_mean_abs_rest_exerc', ...
            PP_SP_int_avg', ...
            date_spotcheck', ...
            overall_spotcheck_nr', ...
            delta_risetime', ...
            delta_risetime_abs', ...            
            PWV', ...
            daytime', ...
            error_SBP_no_tracking_abs', ...
            error_DBP_no_tracking_abs', ...
            delta_PAT', ...
            delta_PWV', ... 
            delta_HR', ... 
            delta_SBP', ... 
            delta_DBP', ...     
            delta_pp_ft_amp', ...
            delta_upstgrad', ...
            delta_RCtime', ...
            day', ...
            PATfoot', ...
            delta_PATfoot', ...
            ];
        
end

%save metrics matrix before additional analysis
save([subject_id_path '\..\metrics_matrix_raw.mat'], 'metrics_matrix');
%also keep as separate variable for certain error metrics
metrics_matrix_raw = metrics_matrix;

%...also for analysis outcome folder
save([subject_id_path, '\..\..\outcome_analysis\metrics_matrix_raw.mat'], 'metrics_matrix');

%% Additional analysis BP Tracker
    
%close all
load('metrics_matrix_raw')

%one line per subject for already averaged metrics (e.g. mean abs error of all/rest/rest_exerc)

%find unique subject IDs and output corresponding idxs of last row per
%subject
[unique_subject_id, unique_subject_id_idx] = unique(metrics_matrix(:,1), 'last');
%use idxs to get THE LAST row per subject 
metrics_matrix_last_row_per_subject = metrics_matrix(unique_subject_id_idx,:);

% subject mean errors (just few metrics)
 
    %list mean absolute errors (per subject for all tracking spotchecks)
    %SBP
    mean_errors_all_SBP = metrics_matrix_last_row_per_subject(:,46);
    %DBP
    mean_errors_all_DBP = metrics_matrix_last_row_per_subject(:,51);
    %compute overall mean errors (based on per-subject-mean)
    %SBP
    overall_mean_error_all_SBP = nanmean(mean_errors_all_SBP);
    %DBP
    overall_mean_error_all_DBP = nanmean(mean_errors_all_DBP);
        
% individual errors (with reference variability check)

% Clean matrix from variability in BP reference 

%1) Delete all rows(trials) being NaN (i.e. Subject3)
metrics_matrix_no_nans = metrics_matrix(~isnan(metrics_matrix(:,2)), :);

%2) Insert/delete elements/rows where they do not follow the expected sequence of trials (whatsoever...)
N_trials = size(metrics_matrix_no_nans,1); %total number of trials without nans
expected_sequence = [1 2 3];
expected_vector = repmat(expected_sequence,1, ceil(N_trials/3));

i=1;
while ~all(metrics_matrix_no_nans(:,2)' == expected_vector(1:N_trials)) %as long as the trials sequence does not match the expected sequence
    
    sequence_match_test = metrics_matrix_no_nans([i:i+2],2)' == expected_sequence;
    
    if all(sequence_match_test) %if expected sequence shows up in steps of three
        i = i+3;
    else %if expected sequence is not found
        disp(['error at ' num2str(i)])
        first_idx_faulty_seq = i;
        current_subject_nr = metrics_matrix_no_nans(first_idx_faulty_seq,1);
        idxs_correct_seq = findstr(metrics_matrix_no_nans([i:end],2)', expected_sequence); %find next points where expected sequence is complete again
        first_idx_correct_seq = idxs_correct_seq(1);
        bad_elements_idxs = [i : i+first_idx_correct_seq-2];       
        
        %delete if all 3 are bad elements
        if sum(sequence_match_test) == 0
            %delete all columns for bad elements and shorten N_trials_vector
            disp([num2str(numel(bad_elements_idxs)) ' rows deleted' ])
            metrics_matrix_no_nans(bad_elements_idxs,:) = [];
            N_trials = N_trials - numel(bad_elements_idxs);    
        else
            %insert row of nans where elements are missing
            insertions = 3-numel(bad_elements_idxs);
            insert_element = find(sequence_match_test == 0);
            disp([num2str(insertions) ' rows inserted' ])
            metrics_matrix_no_nans = [metrics_matrix_no_nans(1:bad_elements_idxs(end),:); [current_subject_nr, insert_element ,ones(1,size(metrics_matrix_no_nans,2)-2).*NaN]; metrics_matrix_no_nans(bad_elements_idxs(end)+1:end,:)];
            N_trials = N_trials + insertions;
        end
        
    end
    
end

metrics_matrix_clean = metrics_matrix_no_nans;

%3) Check for variability in reference BP for 3 consecutive trials

%initialize new columns for mean, SD, and validity, respectively for SBP and DBP
BP_ref_validation_matrix = NaN*ones(size(metrics_matrix_clean,1),6);
max_deviation = 5; %for now 5mmHg (lower thresholds lead to slightly better results in %, but severely decrease N!)

for i = 1:3:size(metrics_matrix_clean,1)
   
    triplet_SBP_meas = metrics_matrix_clean([i:i+2], 7);
    triplet_DBP_meas = metrics_matrix_clean([i:i+2], 8);
    
    mean_triplet_SBP_meas = nanmean(triplet_SBP_meas);
    mean_triplet_DBP_meas = nanmean(triplet_DBP_meas);
    sd_triplet_SBP_meas = nanstd(triplet_SBP_meas);
    sd_triplet_DBP_meas = nanstd(triplet_DBP_meas);    
    
    deviations_SBP = triplet_SBP_meas-mean_triplet_SBP_meas;
    deviations_SBP_abs = abs(deviations_SBP);
    valid_SBP = deviations_SBP_abs < max_deviation;
    deviations_DBP = triplet_DBP_meas-mean_triplet_DBP_meas;
    deviations_DBP_abs = abs(deviations_DBP);
    valid_DBP = deviations_DBP_abs < max_deviation;
    
    BP_ref_validation_matrix([i:i+2], :) = [mean_triplet_SBP_meas*ones(3,1), ...
        sd_triplet_SBP_meas*ones(3,1), ...
        valid_SBP, ...
        mean_triplet_DBP_meas*ones(3,1), ...
        sd_triplet_DBP_meas*ones(3,1), ...
        valid_DBP, ...
        ];  
end

metrics_matrix = [metrics_matrix_clean, BP_ref_validation_matrix];

%save metrics matrix after additional analysis (clean, incl. BP_ref matrix)
save([subject_id_path '\..\metrics_matrix_clean.mat'], 'metrics_matrix');
%...also for analysis outcome folder
save([subject_id_path, '\..\..\outcome_analysis\metrics_matrix_clean.mat'], 'metrics_matrix');

    %list individual absolute errors (for all subjects tracking spotchecks)
    %SBP
    errors_all_SBP = metrics_matrix(:,45);
    %DBP
    errors_all_DBP = metrics_matrix(:,50);
    
    %list individual CIs (for all subjects tracking spotchecks)
    spotcheck_CI = metrics_matrix(:,24);
    PPG_CI = metrics_matrix(:,25);
    risetime = metrics_matrix(:,27);
    delta_risetime = metrics_matrix(:,77);
    delta_risetime_abs = metrics_matrix(:,78); 
    PAT = metrics_matrix(:,22);
    PATfoot = metrics_matrix(:, 92); 
    HR = metrics_matrix(:,23);
    PWV = metrics_matrix(:,79);
    SBP_meas = metrics_matrix(:,7);
    DBP_meas = metrics_matrix(:,8);
    SBP_cal_track_PWV = metrics_matrix(:,18);
    DBP_cal_track_PWV = metrics_matrix(:,19);
    delta_PAT = metrics_matrix(:,83);
    delta_PWV = metrics_matrix(:,84); 
    delta_HR = metrics_matrix(:,85); 
    delta_SBP = metrics_matrix(:,86);
    delta_DBP = metrics_matrix(:,87);  
    delta_pp_ft_amp = metrics_matrix(:,88);  
    delta_upstgrad = metrics_matrix(:,89); 
    delta_RCtime = metrics_matrix(:,90);
    delta_PATfoot = metrics_matrix(:,93);
    SBP_ref_validity = metrics_matrix(:,96);
    DBP_ref_validity = metrics_matrix(:,99);    

    %visually correlate subject mean errors with subject mean CIs/risetimeSD
    figure, hold on; 
    %SBP
    subplot(2,3,1), hold on;
    scatter(spotcheck_CI, errors_all_SBP, 'b')
    scatter(PPG_CI, errors_all_SBP, 'r')
    ylabel('error')
    title('SBP')
    legend('Spotcheck CI', 'PPG CI')
    subplot(2,3,2), hold on;
    scatter(delta_risetime, errors_all_SBP, 'g')
    ylabel('error')
    legend('Risetime Deviation')
    subplot(2,3,3), hold on;
    scatter(delta_RCtime, errors_all_SBP, 'g')
    ylabel('error')
    legend('RCtime Deviation')    
    %DBP
    subplot(2,3,4), hold on;
    scatter(spotcheck_CI, errors_all_DBP, 'b')
    scatter(PPG_CI, errors_all_DBP, 'r')
    ylabel('error')
    title('DBP')
    legend('Spotcheck CI', 'PPG CI')
    subplot(2,3,5), hold on;
    scatter(delta_risetime, errors_all_DBP, 'g')
    ylabel('error')
    legend('Risetime Deviation')
    subplot(2,3,6), hold on;
    scatter(delta_RCtime, errors_all_DBP, 'g')
    ylabel('error')
    legend('RCtime Deviation')        
    
    %save figure
    savefig([subject_id_path, '\..\..\outcome_analysis\CI_vs_error_scatter'])            

    %list mean absolute errors (per subject for all tracking spotchecks), with avg CI's > 3 
    criterion_fulfilled_idxs = (spotcheck_CI>=3 & PPG_CI>=3 & delta_risetime_abs <=.5 & SBP_ref_validity == 1 & DBP_ref_validity == 1 & PAT>0.15 & PAT<0.45 & delta_PWV<300);
    %SBP
    SBP_error_exceedance_despite_crit_idxs = criterion_fulfilled_idxs & metrics_matrix(:,45)>7;
    errors_all_SBP_CI_fulfilled = metrics_matrix(criterion_fulfilled_idxs,45);
    %DBP
    DBP_error_exceedance_despite_crit_idxs = criterion_fulfilled_idxs & metrics_matrix(:,50)>7;    
    errors_all_DBP_CI_fulfilled = metrics_matrix(criterion_fulfilled_idxs,50);

    %overall mean errors, with avg CI's > 3 
    %SBP
    overall_mean_error_all_SBP_CI_fulfilled = nanmean(errors_all_SBP_CI_fulfilled);
    %DBP
    overall_mean_error_all_DBP_CI_fulfilled = nanmean(errors_all_DBP_CI_fulfilled);
    
    %errors without tracking / without CI
        errors_all_SBP_no_tracking = metrics_matrix_raw(:, 81);
        errors_all_DBP_no_tracking = metrics_matrix_raw(:, 82);
        %overall mean error without tracking
        %SBP
        overall_mean_error_SBP_no_tracking = nanmean(metrics_matrix_raw(:,81));
        %DBP
        overall_mean_error_DBP_no_tracking = nanmean(metrics_matrix_raw(:,82));
    %errors without tracking / with CI
        errors_all_SBP_no_tracking_CI = metrics_matrix(criterion_fulfilled_idxs, 81);
        errors_all_DBP_no_tracking_CI = metrics_matrix(criterion_fulfilled_idxs, 82);
        %overall mean error without tracking
        %SBP
        overall_mean_error_SBP_no_tracking_CI = nanmean(metrics_matrix(criterion_fulfilled_idxs,81));
        %DBP
        overall_mean_error_DBP_no_tracking_CI = nanmean(metrics_matrix(criterion_fulfilled_idxs,82));       
        
    %which subjects / spotchecks / errors fulfilled CI but did not achieve error lower than 7?
    %SBP
    outliers_all_SBP = [metrics_matrix(SBP_error_exceedance_despite_crit_idxs, 1), metrics_matrix(SBP_error_exceedance_despite_crit_idxs, 76), metrics_matrix(SBP_error_exceedance_despite_crit_idxs, 44)]
    %DBP
    outliers_all_DBP = [metrics_matrix(DBP_error_exceedance_despite_crit_idxs, 1), metrics_matrix(DBP_error_exceedance_despite_crit_idxs, 76), metrics_matrix(DBP_error_exceedance_despite_crit_idxs, 49)]

    N_all = numel(unique_subject_id);% N_subjects;

    N_data_metadata_match = size(metrics_matrix,1); %+6 where algo chain crashed for different reasons

    N_criteria_fulfilled = sum(criterion_fulfilled_idxs);

    % Boxplots mean errors subjects & overall mean errors
    %row1: no CI criteria applied
    %row2: CI criteriat applied

    fig_boxplots_pre_qual = figure('Name','Boxplots'); clf; hold on;
    
    %No tracking, no criteria
        %all
        subplot(1,4,1), hold on;
        boxplot([errors_all_SBP_no_tracking, errors_all_DBP_no_tracking],{'SBP', 'DBP'});
        scatter(1, overall_mean_error_SBP_no_tracking, 'r', 'fill');
        scatter(2, overall_mean_error_DBP_no_tracking, 'r', 'fill');  
        %N in plot
        text(1.5, nanmean(errors_all_SBP_no_tracking)-3, ['N=', num2str(numel(errors_all_SBP_no_tracking(~isnan(errors_all_SBP_no_tracking))))])
        %MAD in plot
        text(1+0.2, overall_mean_error_SBP_no_tracking+3, ['MAD:', char(10), num2str(overall_mean_error_SBP_no_tracking)])
        text(2+0.2, overall_mean_error_DBP_no_tracking+3, ['MAD:', char(10), num2str(overall_mean_error_DBP_no_tracking)])
        %of estimations below threshold
        perc_in_thresh_errors_all_SBP_no_tracking = sum(errors_all_SBP_no_tracking(~isnan(errors_all_SBP_no_tracking)) <= 7)./numel(errors_all_SBP_no_tracking(~isnan(errors_all_SBP_no_tracking))).*100;
        perc_in_thresh_errors_all_DBP_no_tracking = sum(errors_all_DBP_no_tracking(~isnan(errors_all_DBP_no_tracking)) <= 7)./numel(errors_all_DBP_no_tracking(~isnan(errors_all_DBP_no_tracking))).*100;
        text(1+0.2, overall_mean_error_SBP_no_tracking-2, ['(', num2str(perc_in_thresh_errors_all_SBP_no_tracking), '%)'])
        text(2+0.2, overall_mean_error_DBP_no_tracking-2, ['(', num2str(perc_in_thresh_errors_all_DBP_no_tracking), '%)'])
        %threshold
        hline(7);
        ylim([0 70])
        ylabel('Mean Absolute Deviation [mmHg]')
        title('No tracking / no CI criteria applied')   
    %No tracking, CI criteria applied
        %all
        subplot(1,4,2), hold on;
        boxplot([errors_all_SBP_no_tracking_CI, errors_all_DBP_no_tracking_CI],{'SBP', 'DBP'});
        scatter(1, overall_mean_error_SBP_no_tracking_CI, 'r', 'fill');
        scatter(2, overall_mean_error_DBP_no_tracking_CI, 'r', 'fill');  
        %N in plot
        text(1.5, nanmean(errors_all_SBP_no_tracking_CI)-3, ['N=', num2str(numel(errors_all_SBP_no_tracking_CI(~isnan(errors_all_SBP_no_tracking_CI))))])
        %MAD in plot
        text(1+0.2, overall_mean_error_SBP_no_tracking_CI+3, ['MAD:', char(10), num2str(overall_mean_error_SBP_no_tracking_CI)])
        text(2+0.2, overall_mean_error_DBP_no_tracking_CI+3, ['MAD:', char(10), num2str(overall_mean_error_DBP_no_tracking_CI)])
        %of estimations below threshold
        perc_in_thresh_errors_all_SBP_no_tracking_CI = sum(errors_all_SBP_no_tracking_CI(~isnan(errors_all_SBP_no_tracking_CI)) <= 7)./numel(errors_all_SBP_no_tracking_CI(~isnan(errors_all_SBP_no_tracking_CI))).*100;
        perc_in_thresh_errors_all_DBP_no_tracking_CI = sum(errors_all_DBP_no_tracking_CI(~isnan(errors_all_DBP_no_tracking_CI)) <= 7)./numel(errors_all_DBP_no_tracking_CI(~isnan(errors_all_DBP_no_tracking_CI))).*100;
        text(1+0.2, overall_mean_error_SBP_no_tracking_CI-2, ['(', num2str(perc_in_thresh_errors_all_SBP_no_tracking_CI), '%)'])
        text(2+0.2, overall_mean_error_DBP_no_tracking_CI-2, ['(', num2str(perc_in_thresh_errors_all_DBP_no_tracking_CI), '%)'])
        %threshold
        hline(7);
        ylim([0 70])
        ylabel('Mean Absolute Deviation [mmHg]')
        title('No tracking / CI criteria applied ')               
    %No criteria
        %all
        subplot(1,4,3), hold on;
        boxplot([errors_all_SBP, errors_all_DBP],{'SBP', 'DBP'});
        scatter(1, overall_mean_error_all_SBP, 'r', 'fill');
        scatter(2, overall_mean_error_all_DBP, 'r', 'fill');  
        %N in plot
        text(1.5, nanmean(errors_all_SBP)-3, ['N=', num2str(numel(errors_all_SBP(~isnan(errors_all_SBP))))])
        %MAD in plot
        text(1+0.2, overall_mean_error_all_SBP+3, ['MAD:', char(10), num2str(overall_mean_error_all_SBP)])
        text(2+0.2, overall_mean_error_all_DBP+3, ['MAD:', char(10), num2str(overall_mean_error_all_DBP)])
        %of estimations below threshold
        perc_in_thresh_errors_all_SBP = sum(errors_all_SBP(~isnan(errors_all_SBP)) <= 7)./numel(errors_all_SBP(~isnan(errors_all_SBP))).*100;
        perc_in_thresh_errors_all_DBP = sum(errors_all_DBP(~isnan(errors_all_DBP)) <= 7)./numel(errors_all_DBP(~isnan(errors_all_DBP))).*100;
        text(1+0.2, overall_mean_error_all_SBP-2, ['(', num2str(perc_in_thresh_errors_all_SBP), '%)'])
        text(2+0.2, overall_mean_error_all_DBP-2, ['(', num2str(perc_in_thresh_errors_all_DBP), '%)'])
        %threshold
        hline(7);
        ylim([0 70])
        ylabel('Mean Absolute Deviation [mmHg]')
        title('No CI criteria applied')
    %CI criteria applied
        %all
        subplot(1,4,4), hold on;
        boxplot([errors_all_SBP_CI_fulfilled, errors_all_DBP_CI_fulfilled],{'SBP', 'DBP'});
        scatter(1, overall_mean_error_all_SBP_CI_fulfilled, 'r', 'fill');
        scatter(2, overall_mean_error_all_DBP_CI_fulfilled, 'r', 'fill');
        %N in plot
        text(1.5, nanmean(errors_all_SBP_CI_fulfilled)-1, ['N=', num2str(numel(errors_all_SBP_CI_fulfilled(~isnan(errors_all_SBP_CI_fulfilled))))])     
        %MAD in plot
        text(1+0.2, overall_mean_error_all_SBP_CI_fulfilled+3, ['MAD:', char(10), num2str(overall_mean_error_all_SBP_CI_fulfilled)])
        text(2+0.2, overall_mean_error_all_DBP_CI_fulfilled+3, ['MAD:', char(10), num2str(overall_mean_error_all_DBP_CI_fulfilled)])     
        %of estimations below threshold
        perc_in_thresh_errors_all_SBP_CI_fulfilled = sum(errors_all_SBP_CI_fulfilled(~isnan(errors_all_SBP_CI_fulfilled)) <= 7)./numel(errors_all_SBP_CI_fulfilled(~isnan(errors_all_SBP_CI_fulfilled))).*100;
        perc_in_thresh_errors_all_DBP_CI_fulfilled = sum(errors_all_DBP_CI_fulfilled(~isnan(errors_all_DBP_CI_fulfilled)) <= 7)./numel(errors_all_DBP_CI_fulfilled(~isnan(errors_all_DBP_CI_fulfilled))).*100;
        text(1+0.2, overall_mean_error_all_SBP_CI_fulfilled-2, ['(', num2str(perc_in_thresh_errors_all_SBP_CI_fulfilled), '%)'])
        text(2+0.2, overall_mean_error_all_DBP_CI_fulfilled-4, ['(', num2str(perc_in_thresh_errors_all_DBP_CI_fulfilled), '%)'])
        %threshold   
        hline(7);
        ylim([0 70])
        ylabel('Mean Absolute Deviation [mmHg]')
        title('CI criteria applied')   
        
    %save figure
    savefig([subject_id_path, '\..\..\outcome_analysis\boxplots'])        

%% Overview scatterplots 

rows = 2;
columns = 5;

%Pre-Qualification
fig_scatterplots_pre_qual = figure('Name','Scatterplot Overview Pre-Qualification'); clf; hold on;

    %PWV-SBP
    subplot(rows, columns, 1), scatter(PWV, SBP_meas)
    xlabel('PWV'),ylabel('SBP')
    %PWV-DBP
    subplot(rows, columns, 6), scatter(PWV, DBP_meas)
    xlabel('PWV'),ylabel('DBP')

    %PAT-SBP
    subplot(rows, columns, 2), scatter(PAT, SBP_meas)
    xlabel('PAT'),ylabel('SBP')
    %PAT-DBP
    subplot(rows, columns, 7), scatter(PAT, DBP_meas)
    xlabel('PAT'),ylabel('DBP')

    %HR-SBP
    subplot(rows, columns, 3), scatter(HR, SBP_meas)
    xlabel('HR'),ylabel('SBP')
    %HR-DBP
    subplot(rows, columns, 8), scatter(HR, DBP_meas)
    xlabel('HR'),ylabel('DBP')

    %risetime-SBP
    subplot(rows, columns, 4), scatter(risetime, SBP_meas)
    xlabel('risetime'),ylabel('SBP')
    %risetime-DBP
    subplot(rows, columns, 9), scatter(risetime, DBP_meas)
    xlabel('risetime'),ylabel('DBP')    
    
    %PATfoot-SBP
    subplot(rows, columns,5), scatter(PATfoot, SBP_meas)
    xlabel('PATfoot'),ylabel('SBP')
    %PAT-HR
    subplot(rows, columns, 10), scatter(PAT, HR)
    xlabel('PAT'),ylabel('HR')

    suptitle('Pre-Qualification')
    %save figure
    savefig([subject_id_path, '\..\..\outcome_analysis\predictors_scatter_pre-quali'])            

%Post-Quali
fig_scatterplots_post_qual = figure('Name','Scatterplot Overview Post-Qualification'); clf; hold on;
columns = 6;%%%%%%%%%%
    %PWV-SBP
    subplot(rows, columns,1), scatter(PWV(criterion_fulfilled_idxs), SBP_meas(criterion_fulfilled_idxs))
    xlabel('PWV'),ylabel('SBP')
    %PWV-DBP
    subplot(rows, columns,7), scatter(PWV(criterion_fulfilled_idxs), DBP_meas(criterion_fulfilled_idxs))
    xlabel('PWV'),ylabel('DBP')

    %PAT-SBP
    subplot(rows, columns,2), scatter(PAT(criterion_fulfilled_idxs), SBP_meas(criterion_fulfilled_idxs))
    xlabel('PAT'),ylabel('SBP')
    %PAT-DBP
    subplot(rows, columns,8), scatter(PAT(criterion_fulfilled_idxs), DBP_meas(criterion_fulfilled_idxs))
    xlabel('PAT'),ylabel('DBP')

    %HR-SBP
    subplot(rows, columns,3), scatter(HR(criterion_fulfilled_idxs), SBP_meas(criterion_fulfilled_idxs))
    xlabel('HR'),ylabel('SBP')
    %HR-DBP
    subplot(rows, columns,9), scatter(HR(criterion_fulfilled_idxs), DBP_meas(criterion_fulfilled_idxs))
    xlabel('HR'),ylabel('DBP')
    
    %risetime-SBP
    subplot(rows, columns, 4), scatter(risetime(criterion_fulfilled_idxs), SBP_meas(criterion_fulfilled_idxs))
    xlabel('risetime'),ylabel('SBP')
    %risetime-DBP
    subplot(rows, columns, 10), scatter(risetime(criterion_fulfilled_idxs), DBP_meas(criterion_fulfilled_idxs))
    xlabel('risetime'),ylabel('DBP')     

    %PATfoot-SBP
    subplot(rows, columns,5), scatter(PATfoot(criterion_fulfilled_idxs), SBP_meas(criterion_fulfilled_idxs))
    xlabel('PATfoot'),ylabel('SBP')
    %PAT-HR
    subplot(rows, columns,11), scatter(PAT(criterion_fulfilled_idxs), HR(criterion_fulfilled_idxs))
    xlabel('PAT'),ylabel('HR') 
  
    suptitle('Post-Qualification')
    %save figure
    savefig([subject_id_path, '\..\..\outcome_analysis\predictors_scatter_post-quali'])            

%Pre-Quali Delta
fig_scatterplots_pre_qual_delta = figure('Name','Scatterplot Overview Pre-Qualification_Delta'); clf; hold on;
columns = 6;%%%%%%%%%%
    %PWV-SBP
    subplot(rows, columns,1), scatter(delta_PWV, delta_SBP)
    xlabel('\Delta PWV'),ylabel('\Delta SBP')
    %PWV-DBP
    subplot(rows, columns,7), scatter(delta_PWV, delta_DBP)
    xlabel('\Delta PWV'),ylabel('\Delta DBP')

    %PAT-SBP
    subplot(rows, columns,2), scatter(delta_PAT, delta_SBP)
    xlabel('\Delta PAT'),ylabel('\Delta SBP')
    %PAT-DBP
    subplot(rows, columns,8), scatter(delta_PAT, delta_DBP)
    xlabel('\Delta PAT'),ylabel('\Delta DBP')

    %HR-SBP
    subplot(rows, columns,3), scatter(delta_HR, delta_SBP)
    xlabel('\Delta HR'),ylabel('\Delta SBP')
    %HR-DBP
    subplot(rows, columns,9), scatter(delta_HR, delta_DBP)
    xlabel('\Delta HR'),ylabel('\Delta DBP')
    
    %risetime-SBP
    subplot(rows, columns, 4), scatter(delta_risetime, delta_SBP)
    xlabel('\Delta risetime'),ylabel('SBP')
    %risetime-DBP
    subplot(rows, columns, 10), scatter(delta_risetime, delta_DBP)
    xlabel('\Delta risetime'),ylabel('DBP')        

    %risetime-HR
    subplot(rows, columns,5), scatter(delta_risetime, delta_HR)
    xlabel('\Delta risetime'),ylabel('\Delta HR')
    %PAT-HR
    subplot(rows, columns,11), scatter(delta_PAT, delta_HR)
    xlabel('\Delta PAT'),ylabel('\Delta HR')
    
    %PATfoot-SBP
    subplot(rows, columns,6), scatter(delta_PATfoot, delta_SBP)
    xlabel('\Delta PATfoote'),ylabel('\Delta SBP')
    %PATfoot-DBP
    subplot(rows, columns,12), scatter(delta_PATfoot, delta_DBP)
    xlabel('\Delta PATfoot'),ylabel('\Delta DBP')      

    suptitle('Pre-Qualification (Delta)')
    %save figure
    savefig([subject_id_path, '\..\..\outcome_analysis\predictors_scatter_pre-quali_delta'])    

%Post-Quali Delta
fig_scatterplots_post_qual_delta = figure('Name','Scatterplot Overview Post-Qualification_Delta'); clf; hold on;
columns = 10;%%%%%%%%%%
    %PWV-SBP
    subplot(rows, columns,1), scatter(delta_PWV(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta PWV'),ylabel('\Delta SBP')
    %PWV-DBP
    subplot(rows, columns,11), scatter(delta_PWV(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta PWV'),ylabel('\Delta DBP')

    %PAT-SBP
    subplot(rows, columns,2), scatter(delta_PAT(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta PAT'),ylabel('\Delta SBP')
    %PAT-DBP
    subplot(rows, columns,12), scatter(delta_PAT(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta PAT'),ylabel('\Delta DBP')

    %HR-SBP
    subplot(rows, columns,3), scatter(delta_HR(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta HR'),ylabel('\Delta SBP')
    %HR-DBP
    subplot(rows, columns,13), scatter(delta_HR(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta HR'),ylabel('\Delta DBP')
    
    %risetime-SBP
    subplot(rows, columns, 4), scatter(delta_risetime(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta risetime'),ylabel('SBP')
    %risetime-DBP
    subplot(rows, columns, 14), scatter(delta_risetime(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta risetime'),ylabel('DBP')   
    
    %risetime/PAT-SBP
    subplot(rows, columns, 5), scatter(delta_PAT(criterion_fulfilled_idxs) ./ delta_risetime(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta PAT / \Delta risetime'),ylabel('SBP')
    %risetime/PAT-DBP
    subplot(rows, columns, 15), scatter(delta_PAT(criterion_fulfilled_idxs) ./ delta_risetime(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta PAT / \Delta risetime'),ylabel('DBP')     

    %risetime-HR
    subplot(rows, columns,6), scatter(delta_risetime(criterion_fulfilled_idxs), delta_HR(criterion_fulfilled_idxs))
    xlabel('\Delta risetime'),ylabel('\Delta HR')
    %PAT-HR
    subplot(rows, columns,16), scatter(delta_PAT(criterion_fulfilled_idxs), delta_HR(criterion_fulfilled_idxs))
    xlabel('\Delta PAT'),ylabel('\Delta HR')
    
    %pp_ft_amp-SBP
    subplot(rows, columns,7), scatter(delta_pp_ft_amp(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta pp-ft amp'),ylabel('\Delta SBP')
    %pp_ft_amp-SBP
    subplot(rows, columns,17), scatter(delta_pp_ft_amp(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta pp-ft amp'),ylabel('\Delta DBP')    
    
    %upstgrad-SBP
    subplot(rows, columns,8), scatter(delta_upstgrad(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta upstgrad'),ylabel('\Delta SBP')
    %upstgrad-SBP
    subplot(rows, columns,18), scatter(delta_upstgrad(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta upstgrad'),ylabel('\Delta DBP')  
    
    %RCtime-SBP
    subplot(rows, columns,9), scatter(delta_RCtime(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta RCtime'),ylabel('\Delta SBP')
    %RCtime-SBP
    subplot(rows, columns,19), scatter(delta_RCtime(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta RCtime'),ylabel('\Delta DBP')   
    
    %PATfoot-SBP
    subplot(rows, columns,10), scatter(delta_PATfoot(criterion_fulfilled_idxs), delta_SBP(criterion_fulfilled_idxs))
    xlabel('\Delta PATfoote'),ylabel('\Delta SBP')
    %PATfoot-DBP
    subplot(rows, columns,20), scatter(delta_PATfoot(criterion_fulfilled_idxs), delta_DBP(criterion_fulfilled_idxs))
    xlabel('\Delta PATfoot'),ylabel('\Delta DBP')     

    suptitle('Post-Qualification (Delta)')
    %save figure
    savefig([subject_id_path, '\..\..\outcome_analysis\predictors_scatter_post-quali_delta'])     

%% Bland Altman Plots

%General Settings BA Plots

    gnames = {};%{territories, states}; % names of groups in data {dimension 1 and 2}
    Corr_info = {'n','SSE','r2','eq'}; % stats to display of Corr_elation scatter plot
    BAinfo = {'SD'}; % stats to display on Bland-ALtman plot
    limits = 'Tight'; % how to set the axes limits
    symbols = 's'; % symbols for the data sets (default)
    colors = '';% colors for different data sets
    markercolor =  'r'; %color for single set marker
    tit = '';

    fig_BA_BP_simband = figure('Name','Bland-Altman Plots'); clf; hold on;

%Prior to qualification
    
    %SBP
    label = {'SBP_{meas}','SBP_{est}','mmHg'}; % Names of data sets
    BA_title = 'BA Pre-Qualification';
    corr_title = 'Correlation Pre-Qualification';
    
    [cr, fig, statsStruct] = BlandAltman(subplot(2,2,1), SBP_meas, SBP_cal_track_PWV, label, tit, gnames, Corr_info, BAinfo, limits, colors, symbols, markercolor, corr_title, BA_title);
    %DBP
    label = {'DBP_{meas}','DBP_{est}','mmHg'}; % Names of data sets
    BA_title = 'BA Pre-Qualification';
    corr_title = 'Correlation Pre-Qualification';
    [cr, fig, statsStruct] = BlandAltman(subplot(2,2,2), DBP_meas, DBP_cal_track_PWV, label, tit, gnames, Corr_info, BAinfo, limits, colors, symbols, markercolor, corr_title, BA_title);
    
%After qualification

    %SBP
    label = {'SBP_{meas}','SBP_{est}','mmHg'}; % Names of data sets
    BA_title = 'BA Post-Qualification';
    corr_title = 'Correlation Post-Qualification';
    [cr, fig, statsStruct] = BlandAltman(subplot(2,2,3), SBP_meas(criterion_fulfilled_idxs), SBP_cal_track_PWV(criterion_fulfilled_idxs), label, tit, gnames, Corr_info, BAinfo, limits, colors, symbols, markercolor, corr_title, BA_title);
    %DBP
    label = {'DBP_{meas}','DBP_{est}','mmHg'}; % Names of data sets
    BA_title = 'BA Post-Qualification';
    corr_title = 'Correlation Post-Qualification';
    [cr, fig, statsStruct] = BlandAltman(subplot(2,2,4), DBP_meas(criterion_fulfilled_idxs), DBP_cal_track_PWV(criterion_fulfilled_idxs), label, tit, gnames, Corr_info, BAinfo, limits, colors, symbols, markercolor, corr_title, BA_title);
    
%save figure
savefig([subject_id_path, '\..\..\outcome_analysis\Bland-Altman'])                


%% Analysis on subject 16 only
% 
% %raw matrix for BP without tracking
%     [unique_subject_id, unique_subject_id_idx_start] = unique(metrics_matrix_raw(:,1), 'first');
%     [unique_subject_id, unique_subject_id_idx_stop] = unique(metrics_matrix_raw(:,1), 'last');
% 
%     metrics_matrix_raw_S16 = metrics_matrix_raw([unique_subject_id_idx_start(16):unique_subject_id_idx_stop(16)], :);
% 
%     errors_SBP_no_tracking_S16 = metrics_matrix_raw_S16(:,81);
%     errors_DBP_no_tracking_S16 = metrics_matrix_raw_S16(:,82);
% 
%     mean_error_SBP_no_tracking_S16 = nanmean(errors_SBP_no_tracking_S16);
%     mean_error_DBP_no_tracking_S16 = nanmean(errors_DBP_no_tracking_S16);
% 
%     figure,hold on;
%     
%     subplot(1,2,1), hold on;
%     boxplot([errors_SBP_no_tracking_S16, errors_DBP_no_tracking_S16],{'SBP', 'DBP'});
%             scatter(1, mean_error_SBP_no_tracking_S16, 'r', 'fill');
%             scatter(2, mean_error_DBP_no_tracking_S16, 'r', 'fill');  
%             %N in plot
%             text(1.5, nanmean(errors_SBP_no_tracking_S16)-3, ['N=', num2str(numel(errors_SBP_no_tracking_S16))])
%             %MAD in plot
%             text(1+0.2, mean_error_SBP_no_tracking_S16+3, ['MAD:', char(10), num2str(mean_error_SBP_no_tracking_S16)])
%             text(2+0.2, mean_error_DBP_no_tracking_S16+3, ['MAD:', char(10), num2str(mean_error_DBP_no_tracking_S16)])
%             %of estimations below threshold
%             perc_in_thresh_errors_all_SBP_no_tracking = sum(errors_SBP_no_tracking_S16 <= 7)./numel(errors_SBP_no_tracking_S16).*100;
%             perc_in_thresh_errors_all_DBP_no_tracking = sum(errors_DBP_no_tracking_S16 <= 7)./numel(errors_DBP_no_tracking_S16).*100;
%             text(1+0.2,  mean_error_SBP_no_tracking_S16-2, ['(', num2str(perc_in_thresh_errors_all_SBP_no_tracking), '%)'])
%             text(2+0.2,  mean_error_DBP_no_tracking_S16-2, ['(', num2str(perc_in_thresh_errors_all_DBP_no_tracking), '%)'])
%             %threshold
%             hline(7);
%             ylim([0 70])
%             ylabel('Mean Absolute Deviation [mmHg]')
%             title('No BP tracking Subject 16')    
% 
% %CI cleaned matrix for BP tracking
%     metrics_matrix_CI_clean = metrics_matrix(criterion_fulfilled_idxs, :);
% 
%     [unique_subject_id, unique_subject_id_idx_start] = unique(metrics_matrix_CI_clean(:,1), 'first');
%     [unique_subject_id, unique_subject_id_idx_stop] = unique(metrics_matrix_CI_clean(:,1), 'last');
% 
%     metrics_matrix_CI_clean_S16 = metrics_matrix_CI_clean([unique_subject_id_idx_start(15):unique_subject_id_idx_stop(15)], :);
% 
%     errors_SBP_tracking_S16 = metrics_matrix_CI_clean_S16(:,45);
%     errors_DBP_tracking_S16 = metrics_matrix_CI_clean_S16(:,50);
% 
%     mean_error_SBP_tracking_S16 = nanmean(errors_SBP_tracking_S16);
%     mean_error_DBP_tracking_S16 = nanmean(errors_DBP_tracking_S16);
% 
%     subplot(1,2,2), hold on;
%     boxplot([errors_SBP_tracking_S16, errors_DBP_tracking_S16],{'SBP', 'DBP'});
%             scatter(1, mean_error_SBP_tracking_S16, 'r', 'fill');
%             scatter(2, mean_error_DBP_tracking_S16, 'r', 'fill');  
%             %N in plot
%             text(1.5, nanmean(errors_SBP_tracking_S16)-3, ['N=', num2str(numel(errors_SBP_tracking_S16))])
%             %MAD in plot
%             text(1+0.2, mean_error_SBP_tracking_S16+3, ['MAD:', char(10), num2str(mean_error_SBP_tracking_S16)])
%             text(2+0.2, mean_error_DBP_tracking_S16+3, ['MAD:', char(10), num2str(mean_error_DBP_tracking_S16)])
%             %of estimations below threshold
%             perc_in_thresh_errors_all_SBP_tracking = sum(errors_SBP_tracking_S16 <= 7)./numel(errors_SBP_tracking_S16).*100;
%             perc_in_thresh_errors_all_DBP_tracking = sum(errors_DBP_tracking_S16 <= 7)./numel(errors_DBP_tracking_S16).*100;
%             text(1+0.2,  mean_error_SBP_tracking_S16-2, ['(', num2str(perc_in_thresh_errors_all_SBP_tracking), '%)'])
%             text(2+0.2,  mean_error_DBP_tracking_S16-2, ['(', num2str(perc_in_thresh_errors_all_DBP_tracking), '%)'])
%             %threshold
%             hline(7);
%             ylim([0 70])
%             ylabel('Mean Absolute Deviation [mmHg]')
%             title('CI-qualified BP tracking Subject 16')  

%% Create new coefficients by means of GLM fit 

%define vectors containing qualified samples

    %dependent vars
    delta_SBP_qual = delta_SBP(criterion_fulfilled_idxs);
    delta_DBP_qual = delta_DBP(criterion_fulfilled_idxs);
    %independent vars
    delta_PAT_qual = delta_PAT(criterion_fulfilled_idxs);
    delta_PWV_qual = delta_PWV(criterion_fulfilled_idxs);
    delta_HR_qual = delta_HR(criterion_fulfilled_idxs);
    delta_risetime_qual = delta_risetime(criterion_fulfilled_idxs);
    delta_pp_ft_amp_qual = delta_pp_ft_amp(criterion_fulfilled_idxs);
    delta_upstgrad_qual = delta_upstgrad(criterion_fulfilled_idxs);
    delta_RCtime_qual = delta_RCtime(criterion_fulfilled_idxs);

%train models
    %with PWV
        %SBP
        [glm_sbp_model_pwv, glm_dev_SBP_pwv, glm_stats_SBP_pwv] = glmfit([delta_PWV_qual, delta_pp_ft_amp_qual, delta_upstgrad_qual], delta_SBP_qual);
        %DBP
        [glm_dbp_model_pwv, glm_dev_DBP_pwv, glm_stats_DBP_pwv] = glmfit([delta_HR_qual, delta_upstgrad_qual], delta_DBP_qual);
    %with PAT (if PWV is not available due to height)
        %SBP
        [glm_sbp_model_pat, glm_dev_SBP_pat, glm_stats_SBP_pat] = glmfit([delta_PAT_qual, delta_pp_ft_amp_qual, delta_upstgrad_qual], delta_SBP_qual);
        %DBP
        glm_dbp_model_pat = glm_dbp_model_pwv; %since PWV/PAT is not used in DBP prediction
    
save([subject_id_path '\..\..\outcome_analysis\' 'glm_models_pwv'],'glm_sbp_model_pwv','glm_dbp_model_pwv', '-v7.3');
save([subject_id_path '\..\..\outcome_analysis\' 'glm_models_pat'],'glm_sbp_model_pat','glm_dbp_model_pat', '-v7.3'); 


%% Fitting regression models

fig_PAT_comparison = figure('Name', 'PAT comparison'); hold on;
xlim_abs = [.1, .4];
xlim_delta = [-.15 .15];

%PAT SBP   
    x_raw = PAT;
    x = PAT(criterion_fulfilled_idxs);
    y_raw = SBP_meas;
    y = SBP_meas(criterion_fulfilled_idxs);
    fig_title = 'PAT upstroke BD';
    x_title = 'PAT [s]';
    y_title = 'SBP [mmHg]';

    subplot(2,2,1), hold on;        
    
    coefficients = polyfit(x, y, 1);
    function_abs = @(x_raw) coefficients(1) * x_raw + coefficients(2);
    scatter(x, y);
    fplot(function_abs, [min(x_raw), max(x_raw)]);
    xlabel(x_title)
    ylabel(y_title)
    title(fig_title)
    %Statistics
    y_fit = polyval(coefficients, x);
    resid = y - y_fit;
    SSresid = sum(resid.^2);
    SStotal = (length(y)-1) * var(y);
    Rsq = 1 - SSresid/SStotal;
    %Plot R2 in figure
    xcoord = min(xlim_abs) + .7.*(max(xlim_abs) - min(xlim_abs));
    ycoord = min(y_raw) + .6.*(max(y_raw) - min(y_raw));
    text(xcoord, ycoord, {['R^2 = ' , num2str(Rsq)], ['\beta = ', num2str(coefficients(1))]});
    xlim(xlim_abs);    

%PATfoot SBP   
    x_raw = PATfoot;
    x = PATfoot(criterion_fulfilled_idxs);
    y_raw = SBP_meas;
    y = SBP_meas(criterion_fulfilled_idxs);
    fig_title = 'PAT foot TDE';
    x_title = 'PAT foot TDE [s]';
    y_title = 'SBP [mmHg]';

    subplot(2,2,2), hold on;  
    
    coefficients = polyfit(x, y, 1);
    function_abs = @(x_raw) coefficients(1) * x_raw + coefficients(2);    
    scatter(x, y);
    fplot(function_abs, [min(x_raw), max(x_raw)]);
    xlabel(x_title)
    ylabel(y_title)
    title(fig_title)
    %Statistics
    y_fit = polyval(coefficients, x);
    resid = y - y_fit;
    SSresid = sum(resid.^2);
    SStotal = (length(y)-1) * var(y);
    Rsq = 1 - SSresid/SStotal;
    %Plot R2 in figure
    xcoord = min(xlim_abs) + .7.*(max(xlim_abs) - min(xlim_abs));
    ycoord = min(y_raw) + .6.*(max(y_raw) - min(y_raw));
    text(xcoord, ycoord, {['R^2 = ' , num2str(Rsq)], ['\beta = ', num2str(coefficients(1))]});
    xlim(xlim_abs);      
    
%delta PAT SBP   
    x_raw = delta_PAT;
    x = delta_PAT(criterion_fulfilled_idxs);
    y_raw = delta_SBP;
    y = delta_SBP(criterion_fulfilled_idxs);
    fig_title = '\Delta PAT upstroke BD';
    x_title = '\Delta PAT upstroke BD [s]';
    y_title = '\Delta SBP [mmHg]';

    subplot(2,2,3), hold on;      
    
    coefficients = polyfit(x, y, 1);
    function_abs = @(x_raw) coefficients(1) * x_raw + coefficients(2);
    scatter(x, y);
    fplot(function_abs, [min(x_raw), max(x_raw)]);
    xlabel(x_title)
    ylabel(y_title)
    title(fig_title)
    %Statistics
    y_fit = polyval(coefficients, x);
    resid = y - y_fit;
    SSresid = sum(resid.^2);
    SStotal = (length(y)-1) * var(y);
    Rsq = 1 - SSresid/SStotal;
    %Plot R2 in figure
    xcoord = min(xlim_delta) + .7.*(max(xlim_delta) - min(xlim_delta));
    ycoord = min(y_raw) + .6.*(max(y_raw) - min(y_raw));
    text(xcoord, ycoord, {['R^2 = ' , num2str(Rsq)], ['\beta = ', num2str(coefficients(1))]});     
    xlim(xlim_delta);    
    
%delta PATfoot SBP   
    x_raw = delta_PATfoot;
    x = delta_PATfoot(criterion_fulfilled_idxs);
    y_raw = delta_SBP;
    y = delta_SBP(criterion_fulfilled_idxs);
    fig_title = '\Delta PAT foot TDE';
    x_title = '\Delta PAT foot TDE [s]';
    y_title = '\Delta SBP [mmHg]';

    subplot(2,2,4), hold on;  
    
    coefficients = polyfit(x, y, 1);
    function_abs = @(x_raw) coefficients(1) * x_raw + coefficients(2);
    scatter(x, y);
    fplot(function_abs, [min(x_raw), max(x_raw)]);
    xlabel(x_title)
    ylabel(y_title)
    title(fig_title)
    %Statistics
    y_fit = polyval(coefficients, x);
    resid = y - y_fit;
    SSresid = sum(resid.^2);
    SStotal = (length(y)-1) * var(y);
    Rsq = 1 - SSresid/SStotal;
    %Plot R2 in figure
    xcoord = min(xlim_delta) + .7.*(max(xlim_delta) - min(xlim_delta));
    ycoord = min(y_raw) + .6.*(max(y_raw) - min(y_raw));
    text(xcoord, ycoord, {['R^2 = ' , num2str(Rsq)], ['\beta = ', num2str(coefficients(1))]});    
    xlim(xlim_delta);

%% Generate plots per subject after qualification 
%with indications of valid spotchecks, etc.pp.

%iterate over subjects

for i = start_subject:N_subjects

    metrics_matrix_subject = [];

    subject_nr = i;
    subject_id = subjects(i);
    subject_id_str = ['SP' subject_id{1}];

    %get indices for current subject
    current_subject_all_row_indices = find(metrics_matrix(:,1) == subject_nr);
    criterion_fulfilled_idxs_subject = criterion_fulfilled_idxs(current_subject_all_row_indices);

    %define metrix_matrix for current subject
    metrics_matrix_subject = metrics_matrix(current_subject_all_row_indices, :);
    if isempty(metrics_matrix_subject)
        metrics_matrix_subject = ones(1,size(metrics_matrix_subject,2))*NaN;
    end
       
    %define new timescale for x-axis to separate days etc
    %ones(size(metrics_matrix_subject,1) + size(metrics_matrix_subject,1)/9*3, 1).*NaN; %/9 every day gets three extra fields for separation
    days = metrics_matrix_subject(:,91);
    new_timescale = 1;
    insertions = 0;
    separators = 1;
    %criterion_fulfilled_idxs_subject = [];
    
    for j=1:size(metrics_matrix_subject,1)
        
        %for very first element (ie day 1) insert row of NaNs and 0 in metrics matrix and vector of CI fulfilled idxs respectively
        if j==1
            metrics_matrix_subject = [ones(1,size(metrics_matrix_subject,2)).*NaN; metrics_matrix_subject];
            criterion_fulfilled_idxs_subject = [0; criterion_fulfilled_idxs_subject];
            insertions = insertions+1;
        end
        
        if  metrics_matrix_subject(j+insertions,80) == 77 %Morning
            
            if (metrics_matrix_subject(j+insertions,2) == 1)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.1;
                separators(j+insertions) = 0; 
            elseif (metrics_matrix_subject(j+insertions,2) == 2)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.2;
                separators(j+insertions) = 0;
            elseif (metrics_matrix_subject(j+insertions,2) == 3)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.3;
                new_timescale = [new_timescale, days(j)+0.35];
                separators(j+insertions) = 0;
                separators = [separators, 1];
                %insert corresponding NaNs in metrics_matrix and 0 in vector of CI fulfilled idxs
                metrics_matrix_subject = [metrics_matrix_subject(1:j+insertions,:); ones(1,size(metrics_matrix_subject,2)).*NaN; metrics_matrix_subject(j+insertions+1:end,:)];
                criterion_fulfilled_idxs_subject = [criterion_fulfilled_idxs_subject(1:j+insertions); 0; criterion_fulfilled_idxs_subject(j+insertions+1:end)];
                insertions = insertions+1;
            end          

        elseif  metrics_matrix_subject(j+insertions,80) == 76 %Lunch
            
            if (metrics_matrix_subject(j+insertions,2) == 1)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.4;
                separators(j+insertions) = 0;                
            elseif (metrics_matrix_subject(j+insertions,2) == 2)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.5;
                separators(j+insertions) = 0;
            elseif (metrics_matrix_subject(j+insertions,2) == 3)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.6;
                new_timescale = [new_timescale, days(j)+0.65];
                separators(j+insertions) = 0;
                separators = [separators, 1];                
                %insert corresponding NaNs in metrics_matrix and 0 in vector of CI fulfilled idxs
                metrics_matrix_subject = [metrics_matrix_subject(1:j+insertions,:); ones(1,size(metrics_matrix_subject,2)).*NaN; metrics_matrix_subject(j+insertions+1:end,:)];
                criterion_fulfilled_idxs_subject = [criterion_fulfilled_idxs_subject(1:j+insertions); 0; criterion_fulfilled_idxs_subject(j+insertions+1:end)];
                insertions = insertions+1;
            end     

        elseif  metrics_matrix_subject(j+insertions,80) == 65 %Afternoon
            
            if (metrics_matrix_subject(j+insertions,2) == 1)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.7;
                separators(j+insertions) = 0;
            elseif (metrics_matrix_subject(j+insertions,2) == 2)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.8;
                separators(j+insertions) = 0;                
            elseif (metrics_matrix_subject(j+insertions,2) == 3)
                %insert new timescale elements
                new_timescale(j+insertions) = days(j)+0.9;
                new_timescale = [new_timescale, days(j)+1];
                separators(j+insertions) = 0;
                separators = [separators, 1];                
                %insert corresponding NaNs in metrics_matrix and 0 in vector of CI fulfilled idxs
                metrics_matrix_subject = [metrics_matrix_subject(1:j+insertions,:); ones(1,size(metrics_matrix_subject,2)).*NaN; metrics_matrix_subject(j+insertions+1:end,:)];
                criterion_fulfilled_idxs_subject = [criterion_fulfilled_idxs_subject(1:j+insertions); 0; criterion_fulfilled_idxs_subject(j+insertions+1:end)];
                insertions = insertions+1;
            end            
            
        end
        
    end

    %define relevant variables for plots
    SBP_meas = metrics_matrix_subject(:, 7); 
    DBP_meas = metrics_matrix_subject(:, 8); 
    SBP_cal_track_PWV = metrics_matrix_subject(:,18);
    DBP_cal_track_PWV = metrics_matrix_subject(:,19); 
    
    error_SBP_cal_track_PWV = metrics_matrix_subject(:,44);
    error_SBP_cal_track_PWV_abs = metrics_matrix_subject(:,45);    
    error_SBP_cal_track_PWV_mean_abs = metrics_matrix_subject(:,46);
    
    error_DBP_cal_track_PWV = metrics_matrix_subject(:,49);
    error_DBP_cal_track_PWV_abs = metrics_matrix_subject(:,50);    
    error_DBP_cal_track_PWV_mean_abs = metrics_matrix_subject(:,51);   
    
    delta_SBP = metrics_matrix_subject(:,86);
    delta_DBP = metrics_matrix_subject(:,87);
    delta_PAT = metrics_matrix_subject(:,83);
    
%     spotcheck_CI = metrics_matrix(:,24);
%     PPG_CI = metrics_matrix(:,25);
%     risetime = metrics_matrix(:,27);
%     delta_risetime = metrics_matrix(:,77);
%     delta_risetime_abs = metrics_matrix(:,78); 
%     PAT = metrics_matrix(:,22);
%     HR = metrics_matrix(:,23);
%     PWV = metrics_matrix(:,79);
%     SBP_meas = metrics_matrix(:,7);
%     DBP_meas = metrics_matrix(:,8);
% 
%     delta_PAT = metrics_matrix(:,83);
%     delta_PWV = metrics_matrix(:,84); 
%     delta_HR = metrics_matrix(:,85); 
%     delta_SBP = metrics_matrix(:,86);
%     delta_DBP = metrics_matrix(:,87);  
%     delta_pp_ft_amp = metrics_matrix(:,88);  
%     delta_upstgrad = metrics_matrix(:,89); 
%     delta_RCtime = metrics_matrix(:,90); 
%     SBP_ref_validity = metrics_matrix(:,94);
%     DBP_ref_validity = metrics_matrix(:,97);     
    
    figure('Name',['Tracking Subject Post-Qual', subject_id{1}]); clf; hold on;
    %tracking trajectories

    %SBP
    subplot(4,4, [1 2 5 6]), hold on
    plot(new_timescale, SBP_meas,'r-o'); %reference BP
    scatter(new_timescale(find(criterion_fulfilled_idxs_subject)), SBP_meas(find(criterion_fulfilled_idxs_subject)),'r', 'fill'); %reference BP
    plot(new_timescale, SBP_cal_track_PWV', 'g-o'); %cal PWV based tracker SBP
    scatter(new_timescale(find(criterion_fulfilled_idxs_subject)), SBP_cal_track_PWV(find(criterion_fulfilled_idxs_subject)),'g', 'fill'); %tracked BP
    %if available, plot calibration as constant line
    if numel(SBP_meas)>=3
        plot(new_timescale, ones(1,numel(new_timescale)).*nanmean(SBP_meas([1 2 3])), 'r--'); %SBP calibration (avg of first 3)
    end    
    legend('SBP_{cuff}', 'SBP_{cuff} CI OK', 'SBP_{track, cal, PWV}', 'SBP_{track, cal, PWV} CI OK', 'Offset Calibration')
    xlabel('Spotcheck #')
    ylabel('SBP [mmHg]')
    
    %DBP
    subplot(4,4, [9 10 13 14]), hold on
    plot(new_timescale, DBP_meas,'r-o'); %reference BP
    scatter(new_timescale(find(criterion_fulfilled_idxs_subject)), DBP_meas(find(criterion_fulfilled_idxs_subject)),'r', 'fill'); %reference BP
    plot(new_timescale, DBP_cal_track_PWV', 'g-o'); %cal PWV based tracker SBP
    scatter(new_timescale(find(criterion_fulfilled_idxs_subject)), DBP_cal_track_PWV(find(criterion_fulfilled_idxs_subject)),'g', 'fill'); %tracked BP
    %if available, plot calibration as constant line
    if numel(DBP_meas)>=3
        plot(new_timescale, ones(1,numel(new_timescale)).*nanmean(DBP_meas([1 2 3])), 'r--'); %SBP calibration (avg of first 3)
    end    
    legend('DBP_{cuff}', 'DBP_{cuff} CI OK', 'DBP_{track, cal, PWV}', 'DBP_{track, cal, PWV} CI OK', 'Offset Calibration')
    xlabel('Spotcheck #')
    ylabel('DBP [mmHg]')    
    
    %error over time/spotchecks
    subplot(4,4, [7]), hold on
    title('Tracking Error SBP cal,PWV');
    bar(new_timescale(find(criterion_fulfilled_idxs_subject)), error_SBP_cal_track_PWV(find(criterion_fulfilled_idxs_subject)), 'w')
    bar(new_timescale(find(criterion_fulfilled_idxs_subject)), error_SBP_cal_track_PWV_abs(find(criterion_fulfilled_idxs_subject)), 'b')
    scatter(new_timescale(find(criterion_fulfilled_idxs_subject)), error_SBP_cal_track_PWV_mean_abs(find(criterion_fulfilled_idxs_subject)), 'r', 'fill')
    xlabel('Spotcheck #')
    ylabel('error [mmHg]')
    
    
    %scatterplot and fit of deltaPAT vs deltaSBP
    x_raw = delta_PAT;
    x = delta_PAT(find(criterion_fulfilled_idxs_subject));
    y_raw = delta_SBP;
    y = delta_SBP(find(criterion_fulfilled_idxs_subject));
    fig_title = '\Delta PAT';
    x_title = '\Delta PAT[s]';
    y_title = '\Delta SBP[mmHg]';

    subplot(4,4,[3]), hold on;  
    
    if ~isempty(x)    
        coefficients = polyfit(x, y, 1);
        function_abs = @(x_raw) coefficients(1) * x_raw + coefficients(2);
        scatter(x, y);
        fplot(function_abs, [min(x_raw), max(x_raw)]);
        xlabel(x_title)
        ylabel(y_title)
        title(fig_title)
        %Statistics
        y_fit = polyval(coefficients, x);
        resid = y - y_fit;
        SSresid = sum(resid.^2);
        SStotal = (length(y)-1) * var(y);
        Rsq = 1 - SSresid/SStotal;
        %Plot R2 in figure
        xcoord = min(x_raw) + .7.*(max(x_raw) - min(x_raw));
        ycoord = min(y_raw) + .6.*(max(y_raw) - min(y_raw));
        text(xcoord, ycoord, {['R^2 = ' , num2str(Rsq)], ['\beta = ', num2str(coefficients(1))]});    
    else   
        text(0, 0, {['No data available']});      
    end
    
    %error over time/spotchecks
    subplot(4,4, [15]), hold on
    title('Tracking Error DBP cal,PWV');
    bar(new_timescale(find(criterion_fulfilled_idxs_subject)), error_DBP_cal_track_PWV(find(criterion_fulfilled_idxs_subject)), 'w')
    bar(new_timescale(find(criterion_fulfilled_idxs_subject)), error_DBP_cal_track_PWV_abs(find(criterion_fulfilled_idxs_subject)), 'b')
    scatter(new_timescale(find(criterion_fulfilled_idxs_subject)), error_DBP_cal_track_PWV_mean_abs(find(criterion_fulfilled_idxs_subject)), 'r', 'fill')
    xlabel('Spotcheck #')
    ylabel('error [mmHg]')    

end

%     figure('Name',['Tracking Subject Post-Qual', subject_id{1}]); clf; hold on;
%     %tracking trajectories
% 
%     %SBP
%     subplot(4,4, [1 2 5 6]), hold on
%     plot(SBP_meas,'r'); %reference BP
%     plot(SBP_est_abs', 'm:o'); %SBP_est_abs
%     plot(SBP_est_track_PWV', 'b-o'); %est PWV based tracker SBP
%     plot(SBP_est_track_PAT','b--o'); %est PAT based tracker SBP
%     plot(SBP_cal_track_PWV', 'g-o'); %cal PWV based tracker SBP
%     plot(SBP_cal_track_PAT','g--o'); %cal PAT based tracker SBP
%     if ~isnan(SBP_meas(1))
%         plot(ones(1,numel(SBP_meas)).*SBP_meas(1), 'r--'); %first SBP calibration
%     end
%     legend('SBP_{cuff}', 'SBP_{absolute}', 'SBP_{tracker, est, PWV}', 'SBP_{tracker, est, PAT}', 'SBP_{tracker, cal, PWV}', 'SBP_{tracker, cal, PAT}')
%     xlabel('Spotcheck #')
%     ylabel('SBP [mmHg]')
% 
%     %absolute errors boxplot
%     subplot(4,4, [3]), hold on
%     title('Absolute Tracking Errors SBP');
%     boxplot([error_SBP_est_track_PWV_abs', error_SBP_cal_track_PWV_abs'],{'est,PWV', 'cal,PWV'});
%     scatter(1, error_SBP_est_track_PWV_mean_abs(end), 'r', 'fill');
%     scatter(2, error_SBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, error_SBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_SBP_est_track_PWV_mean_abs(end))])
%     text(2+0.2, error_SBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_SBP_cal_track_PWV_mean_abs(end))])
%     % %of estimations below threshold
%     %text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
%     %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
%     hline(7)
%     ylabel('Absolute Deviation [mmHg]')
% 
%     %relative errors boxplot
%     %         subplot(4,4, [7]), hold on
%     %         title('Relative Tracking Errors SBP');
%     %         boxplot([diff_error_SBP_est_track_PWV_abs'],{'est/cal, PWV'});
%     %         scatter(1, diff_error_SBP_est_track_PWV_mean_abs(end), 'r', 'fill');
%     %         scatter(2, diff_error_SBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
%     %         %MAD in plot
%     %         text(1+0.2, diff_error_SBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_SBP_est_track_PWV_mean_abs(end))])
%     %         text(2+0.2, diff_error_SBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_SBP_cal_track_PWV_mean_abs(end))])
%     %         % %of estimations below threshold
%     %         %text(1+0.2, mean_abs_error_SBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_SBP_sitting_rf), '%'])
%     %         %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
%     %         hline(7)
%     %         ylabel('Absolute Deviation [mmHg]')
% 
%     %errors over spotchecks
%     subplot(4,4, [7]), hold on
%     title('Tracking Error SBP cal,PWV');
%     bar(1:numel(error_SBP_cal_track_PWV_abs), error_SBP_cal_track_PWV_abs, 'b')
%     scatter(1:numel(error_SBP_cal_track_PWV_mean_abs), error_SBP_cal_track_PWV_mean_abs, 'r', 'fill')
%     xlabel('Spotcheck #')
%     ylabel('error [mmHg]')
% 
%     %DBP
%     subplot(4,4, [9 10 13 14]), hold on
%     plot(DBP_meas,'r'); %reference BP
%     plot(DBP_est_abs', 'm:o'); %DBP_est_abs
%     plot(DBP_est_track_PWV', 'b-o'); %est PWV based tracker DBP
%     plot(DBP_est_track_PAT','b--o'); %est PAT based tracker DBP
%     plot(DBP_cal_track_PWV', 'g-o'); %cal PWV based tracker DBP
%     plot(DBP_cal_track_PAT','g--o'); %cal PAT based tracker DBP
%     if ~isnan(SBP_meas(1))
%         plot(ones(1,numel(DBP_meas)).*DBP_meas(1), 'r--'); %first SBP calibration
%     end
%     legend('DBP_{cuff}', 'DBP_{absolute}', 'DBP_{tracker, est, PWV}', 'DBP_{tracker, est, PAT}', 'DBP_{tracker, cal, PWV}', 'DBP_{tracker, cal, PAT}')
%     xlabel('Spotcheck #')
%     ylabel('DBP [mmHg]')
% 
%     %absolute errors boxplot
%     subplot(4,4, [11]), hold on
%     title('Absolute Tracking Errors DBP');
%     boxplot([error_DBP_est_track_PWV_abs', error_DBP_cal_track_PWV_abs'],{'est, PWV','cal, PWV'});
%     scatter(1, error_DBP_est_track_PWV_mean_abs(end), 'r', 'fill');
%     scatter(3, error_DBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
%     %MAD in plot
%     text(1+0.2, error_DBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_DBP_est_track_PWV_mean_abs(end))])
%     text(2+0.2, error_DBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(error_DBP_cal_track_PWV_mean_abs(end))])
%     % %of estimations below threshold
%     %text(1+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
%     %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
%     hline(7)
%     ylabel('Absolute Deviation [mmHg]')
% 
%     %relative errors boxplot
%     %         subplot(4,4, [15]), hold on
%     %         title('Relative Tracking Errors DBP');
%     %         boxplot([diff_error_DBP_est_track_PWV_abs'],{'est/cal, PWV'});
%     %         scatter(1, diff_error_DBP_est_track_PWV_mean_abs(end), 'r', 'fill');
%     %         scatter(3, diff_error_DBP_cal_track_PWV_mean_abs(end), 'r', 'fill');
%     %         %MAD in plot
%     %         text(1+0.2, diff_error_DBP_est_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_DBP_est_track_PWV_mean_abs(end))])
%     %         text(2+0.2, diff_error_DBP_cal_track_PWV_mean_abs(end), ['MAD:', char(10), num2str(diff_error_DBP_cal_track_PWV_mean_abs(end))])
%     %         % %of estimations below threshold
%     %         %text(1+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
%     %         %text(2+0.2, mean_abs_error_DBP_sitting_rf-2, ['Estimations <= 7: ', char(10), num2str(perc_in_thresh_DBP_sitting_rf), '%'])
%     %         hline(7)
%     %         ylabel('Absolute Deviation [mmHg]')
% 
%     %errors over spotchecks
%     subplot(4,4, [15]), hold on
%     title('Tracking Error DBP cal,PWV');
%     bar(1:numel(error_DBP_cal_track_PWV_abs), error_DBP_cal_track_PWV_abs, 'b')
%     scatter(1:numel(error_DBP_cal_track_PWV_mean_abs), error_DBP_cal_track_PWV_mean_abs, 'r', 'fill')
%     xlabel('Spotcheck #')
%     ylabel('error [mmHg]')
% 
%     %signals and CIs
%     subplot(4,4, [4]), hold on, plot(PAT), plot(risetime_avg); legend('PAT', 'Risetime'), xlabel('Spotcheck #'), ylabel('[s]');
%     subplot(4,4, [8]), plot(HR), xlabel('Spotcheck #'), ylabel('HR [bpm]');
%     subplot(4,4, [12]), hold on, plot(spotcheck_CI), plot(PPG_CI_raw), legend('Spotcheck CI', 'PPG CI'), xlabel('Spotcheck #'), ylabel('CI'), axis([0 numel(spotcheck_CI) 1 5]);
%     subplot(4,4, [16]), plot(PP_SP_int_avg), xlabel('Spotcheck #'), ylabel('PP-SP int [s]');
% 
%     %save tracking figure per subject
%     savefig([subject_id_path, '\..\..\outcome_analysis\', subject_id_str, '_tracking_trajectory'])
% 
% end
   
   