function [metrics_matrix] = process_consistency_data_FE(subject_id)

display('Select .xlsx file with SSB and BP measurements')
[BP_FileName,BP_PathName,FilterIndex] = uigetfile('.xlsx');


cd(BP_PathName)
BP_data = xlsread(BP_FileName,'BP Data');
SSB_data = xlsread(BP_FileName,'SSB');


metrics_matrix =[];

subject = subject_id;
subject_nr = str2double(strrep(subject,'SP','')); %% retrieve subject nr from subject file name

column_offset = subject_nr + 5;    % column offset ex: subject 1 start at the 6th column
ssb_column_offset =1;
%row offset: look for date of first measurement available
start_idx = find(~isnan(BP_data(:,column_offset)));
start_date = BP_data(start_idx(1)+1,1);

day = BP_data(start_idx(1):end,1); % first colum contains date

idx = find(~isnan(day));

for ii = idx(1:end)'
    day(ii-1:ii+25) = day(ii)*ones(length(day(ii-1:ii+25)),1);
end

%1D vector containing SBP,DBP and HR
parameters = BP_data(start_idx:end,column_offset);

%2D matrix containing SBP,DBP and HR
parameters = reshape(parameters,3,length(parameters)/3)';

%resample date vector
day = reshape(day,3,length(day)/3)';
day = day(:,1);
day_str = num2str(day);
date_num = datenum(day_str,'yyyymmdd');

metrics_matrix = [date_num parameters nan*ones(length(parameters),18)];


cdir = uigetdir;
cd(cdir)

if ~isdir([cdir '\' subject_id])
    mkdir([cdir '\' subject_id]);
end

if ~isdir([cdir '\' subject_id '\output\'])
    mkdir([cdir '\' subject_id '\output\']);
end

files = dir(cdir);

for ii = 1:length(files)
    
    % if not a directory
    if ~files(ii).isdir
        file_name = files(ii,1).name;
        display(file_name);
        
        file_date_num = datenum(file_name(10:17),'yyyymmdd');
        file_daytime = file_name(19);
        trial_nr = str2double(file_name(end-4));
        
        load(file_name);
        data.context.daytime  = {file_daytime};
        data.context.trial_nr = [trial_nr];
        data.context.date     = [str2double(file_name(10:17))];
        data.context.day      = [file_date_num - date_num(1)+1];
        data.filename = file_name;
        data.context.posture = {'sitting'};
        data.ssb.age.signal = [SSB_data(1,subject_nr+ssb_column_offset)];
        data.ssb.height.signal = [SSB_data(2,subject_nr+ssb_column_offset)];
        data.ssb.weight.signal = [SSB_data(3,subject_nr+ssb_column_offset)];
        
        [data] = executeAllAlgorithms(data,0);
        
        row_idx = find(metrics_matrix(:,1)==file_date_num);
        idx_offset = 0;
        
        if(file_daytime == 'M' && trial_nr == 1)
            idx_offset = 0;
            
        elseif (file_daytime == 'M' && trial_nr == 2)
            idx_offset = 1;
            
        elseif (file_daytime == 'M' && trial_nr == 3)
            idx_offset = 2;
            
        elseif (file_daytime == 'L' && trial_nr == 1)
            idx_offset = 3;
            
        elseif (file_daytime == 'L' && trial_nr == 2)
            idx_offset = 4;
            
        elseif (file_daytime == 'L' && trial_nr == 3)
            idx_offset = 5;
            
        elseif (file_daytime == 'A' && trial_nr == 1)
            idx_offset = 6;
            
        elseif (file_daytime == 'A' && trial_nr == 2)
            idx_offset = 7;
            
        elseif (file_daytime == 'A' && trial_nr == 3)
            idx_offset = 8;
        end
        
        %add calibration (absolute + estimate) if channel exists
        if isfield(data.ppg, 'a') && isfield(data.ppg.a,'signal') && isfield(data.ppg.a,'pat')
            [data] = purge(data,'a',metrics_matrix,row_idx(1),idx_offset);
        end
        if isfield(data.ppg, 'b') && isfield(data.ppg.b,'signal') && isfield(data.ppg.b,'pat')
            [data] = purge(data,'b',metrics_matrix,row_idx(1),idx_offset);
        end
        if isfield(data.ppg, 'c') && isfield(data.ppg.c,'signal') && isfield(data.ppg.c,'pat')
            [data] = purge(data,'c',metrics_matrix,row_idx(1),idx_offset); 
        end
        if isfield(data.ppg, 'd') && isfield(data.ppg.d,'signal') && isfield(data.ppg.d,'pat')
            [data] = purge(data,'d',metrics_matrix,row_idx(1),idx_offset);
            metrics_matrix(row_idx(1)+idx_offset,5) = data.ppg.d.spotcheck.pat.signal;
            metrics_matrix(row_idx(1)+idx_offset,6) = data.ppg.d.spotcheck.CI;
            metrics_matrix(row_idx(1)+idx_offset,7) = data.ppg.d.spotcheck.CIppg.signal; 
            metrics_matrix(row_idx(1)+idx_offset,8) = data.ppg.d.spotcheck.ppg_upstgrad.signal;
            metrics_matrix(row_idx(1)+idx_offset,9) = data.ppg.d.spotcheck.ppg_pp_ft_amp.signal;
            metrics_matrix(row_idx(1)+idx_offset,10)= data.ppg.d.bp.tracker.estimation_based.via_pwv.sbp.signal;
            metrics_matrix(row_idx(1)+idx_offset,11)= data.ppg.d.bp.tracker.calibration_based.via_pwv.sbp.signal;
                     
            
        end
        if isfield(data.ppg, 'e') && isfield(data.ppg.e,'signal') && isfield(data.ppg.e,'pat')
            [data] = purge(data,'e',metrics_matrix,row_idx(1),idx_offset);
            metrics_matrix(row_idx(1)+idx_offset,12)= data.ppg.e.spotcheck.pat.signal;
            metrics_matrix(row_idx(1)+idx_offset,13)= data.ppg.e.spotcheck.CI;
            metrics_matrix(row_idx(1)+idx_offset,14)= data.ppg.e.spotcheck.CIppg.signal; 
            metrics_matrix(row_idx(1)+idx_offset,15)= data.ppg.e.spotcheck.ppg_upstgrad.signal;
            metrics_matrix(row_idx(1)+idx_offset,16)= data.ppg.e.spotcheck.ppg_pp_ft_amp.signal;
            metrics_matrix(row_idx(1)+idx_offset,17)= data.ppg.e.bp.tracker.estimation_based.via_pwv.sbp.signal;
            metrics_matrix(row_idx(1)+idx_offset,18)= data.ppg.e.bp.tracker.calibration_based.via_pwv.sbp.signal;
            metrics_matrix(row_idx(1)+idx_offset,19)= data.ppg.e.spotcheck.risetime.signal;
            metrics_matrix(row_idx(1)+idx_offset,20)= data.ppg.e.spotcheck.decaytime.signal;
            metrics_matrix(row_idx(1)+idx_offset,21)= data.ppg.e.spotcheck.RCtime.signal;
            metrics_matrix(row_idx(1)+idx_offset,22)= data.ppg.e.spotcheck.HR.signal;

        end
        
        if isfield(data.ppg, 'f') && isfield(data.ppg.f,'signal') && isfield(data.ppg.f,'pat')
            [data] = purge(data,'f',metrics_matrix,row_idx(1),idx_offset);
        end
        if isfield(data.ppg, 'g') && isfield(data.ppg.g,'signal') && isfield(data.ppg.g,'pat')
            [data] = purge(data,'g',metrics_matrix,row_idx(1),idx_offset);
        end
        if isfield(data.ppg, 'h') && isfield(data.ppg.h,'signal') && isfield(data.ppg.h,'pat')
            [data] = purge(data,'h',metrics_matrix,row_idx(1),idx_offset);
        end
        
        save([cdir '\' subject_id '\' file_name],'data');
    end
end

      save([cdir '\' subject_id '\output\' subject_id '_metrics_matrix.mat'],'metrics_matrix');
end

function [output] = purge(input,chan,metrics_matrix,row,offset)

output = input;
chan = chan(end);

SBP_ref = metrics_matrix(row+offset,2);
DBP_ref = metrics_matrix(row+offset,3);
HR_ref  = metrics_matrix(row+offset,4);

%reference SBP
eval(['output.ppg.' chan '.bp.tracker.this_reference.bp_cal.sbp.signal = [SBP_ref];']);

%estimate SBP
eval(['output.ppg.' chan '.bp.tracker.this_reference.bp_est.sbp.signal = [input.ppg.' chan '.bp.sbp.signal];']);

%reference DBP
eval(['output.ppg.' chan '.bp.tracker.this_reference.bp_cal.dbp.signal = [DBP_ref];']);

%estimate DBP
eval(['output.ppg.' chan '.bp.tracker.this_reference.bp_est.dbp.signal = [input.ppg.' chan '.bp.dbp.signal];']);

eval(['output.ppg.' chan '.bp.tracker.this_reference.bp_cal.hr.signal  = [HR_ref];']);

end


