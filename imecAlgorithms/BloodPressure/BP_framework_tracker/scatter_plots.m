clear all
close all hidden
clc

cd('C:\Users\garcia69\Desktop\Tracker analysis\Rest_model\SP0001\renamed\SPLIT\SP0001\SP0001\output\');
sp1 = load('SP0001_metrics_matrix');
sp1 = sp1.metrics_matrix;

cd('C:\Users\garcia69\Desktop\Tracker analysis\Rest_model\SP0002\SPLIT\SP0002\SP0002\output\');
sp2 = load('SP0002_metrics_matrix');
sp2 = sp2.metrics_matrix;

cd('C:\Users\garcia69\Desktop\Tracker analysis\Rest_model\SP0004\SPLIT\SP0004\SP0004\output\');
sp4 = load('SP0004_metrics_matrix');
sp4 = sp4.metrics_matrix;

figure;
subplot(2,1,1)
scatter(170./sp1(:,12),sp1(:,2),'filled');hold on
scatter(183./sp2(:,12),sp2(:,2),'r','filled');
scatter(179./sp4(:,12),sp4(:,2),'k','filled');
legend('SP0001','SP0002','SP0004','orientation','horizontal')
ylim([90 150])
xlim([200 1100])
ylabel('SBP [mmHg]')

subplot(2,1,2)
scatter([170./sp1(:,12);183./sp2(:,12);179./sp4(:,12)],...
    [sp1(:,2);sp2(:,2);sp4(:,2)],...
    50,[sp1(:,13);sp2(:,13);sp4(:,13)],'filled')
ylim([90 150])
xlim([200 1100])
xlabel('PWV [cm/sec]')
ylabel('SBP [mmHg]')


map = [ 256, 0, 0
        250, 225, 35
        0, 0, 256
        80, 160, 60
        0, 256, 0]/256;
colormap(map)
colorbar('north')

figure;
subplot(2,1,1)
scatter(sp1(:,22),sp1(:,2),'filled');hold on
scatter(sp2(:,22),sp2(:,2),'r','filled');
scatter(sp4(:,22),sp4(:,2),'k','filled');
legend('SP0001','SP0002','SP0004','orientation','horizontal')
%ylim([90 150])
%xlim([200 1100])
ylabel('SBP [mmHg]')

subplot(2,1,2)
scatter([sp1(:,22);sp2(:,22);sp4(:,22)],...
    [sp1(:,2);sp2(:,2);sp4(:,2)],...
    50,[sp1(:,13);sp2(:,13);sp4(:,13)],'filled')
%ylim([90 150])
%xlim([200 1100])
xlabel('HR [BPM]')
ylabel('SBP [mmHg]')

map = [ 256, 0, 0
        250, 225, 35
        0, 0, 256
        80, 160, 60
        0, 256, 0]/256;
colormap(map)
colorbar('north')

