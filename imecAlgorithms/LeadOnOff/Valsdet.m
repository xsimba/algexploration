
%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <PPG_ampdet2.m>
% Content   : Track the peak-peak value of journal data, mean and std
% Version   : GIT 2
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
% 
function [valsdat] = Valsdet(Jdata)
% Jdata
for tr= 1:1:size(Jdata,2)
    valsdat(tr,1) = max(Jdata(:,tr));
    valsdat(tr,2) = min(Jdata(:,tr));
    valsdat(tr,3) = valsdat(tr,1) - valsdat(tr,2);
    valsdat(tr,4) = mean(Jdata(:,tr));
    valsdat(tr,5) = std(Jdata(:,tr));
    
end
