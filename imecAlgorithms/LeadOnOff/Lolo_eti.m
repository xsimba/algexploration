%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Lolo_eti>
% Content   : Main script for lead_on /lead off detection using ETI
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History:
%  | Developer   | Version |    Date   |
%  |  ECWentink  |   1.0   | 01-08-2014|
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input: ETI data
%% output
% lolo: Lead on off per half second
% timestamps:the timestamps beloning to the lolo data stream
% w: debug information
% CI_DBraweti2: the explanation/reasoning behind the 1-4 selection-> ie "too much motion " etc to
% determine why choise was made-> so easy for debugging
% CI_times: time in seconds!!
%%%%%%%%%%%%%%%%%%%%%%
function [output] = Lolo_eti(input, offset)

output=input;


sig = input.eti.signal;
t=input.timestamps;

if isfield(input,'acc')
acc = input.acc.All;
else
    acc = zeros(4,length(sig));
end


if size(sig,2) == 1
    sig = sig';
end

if nargin > 1
    sig = sig(offset:end);
    acc = acc(:,offset:end);
end

%% definitions of the parameters
Fs = 128;
%% windowing
wind = (1*Fs)-1; % 4sec for time domain stuff
udr = 0.5*Fs; % update rate, every second

%% accelerometer data calculations

e = 1;

DB_infeti=zeros(round(size(sig,2)/udr),5);
e_top = ceil(min([size(acc,2), size(sig,2)-3])/udr);
etitest = zeros(round(size(sig,2)/udr),1);
sigetivals =zeros(e_top,5);

for a = 0:udr:min([size(acc,2), size(sig,2)-1])
    %% first CI will be 1 due to lack of data
    if (a)<wind           
            DB_infeti(e,1:5)=0;
    else
        %% Assign data in 1 sec windows
                sigeti2sec = sig(a-wind:a)';  % a sec the signal PPG
        
        %% calc the the amplitude parameters (min, max, peak peak, mean, std)
        sigetivals(e,:) = Valsdet(sigeti2sec);
        
        DB_infeti(e,:) = sigetivals(e,:);
        
        if DB_infeti(e,3)<650 && DB_infeti(e,5)<150
            etitest(e)=1;
        else
            etitest(e)=0;
        end
      
        
    end
    e=e+1;
end
clear e
% t=t';
t_snr = t(1:udr:size(sig,2));
% t_snr=t_snr';

%% the PPG results saved
% Main outputs
output.eti.mat_lolo.timestamps= t_snr'; % the time for the CI
output.eti.mat_lolo.signal=etitest;

%% Debug parameters
output.eti.mat_lolo.DB.DBraweti = DB_infeti; % the debug info (all the parameters, those used and unused)


