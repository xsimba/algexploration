clear all;close all;clc;
load('sub2.mat')

data.acc.All = [data.acc.x.signal;data.acc.y.signal;data.acc.z.signal];
data.acc.All(4,:) = sqrt(sum(data.acc.All.^2, 1));

data = Lolo_main(data);


figure(1);plotyy(data.timestamps,data.ecg.signal,data.ecg.mat_CIraw.timestamps,data.ecg.mat_CIraw.DB.lolo)
