clear all;close all;clc;
filename ='.\ETS7_HM.mat';
load(filename)


data.acc.All = [data.acc.x.signal;data.acc.y.signal;data.acc.z.signal];
data.acc.All(4,:) = sqrt(sum(data.acc.All.^2, 1));


%% calculate lolo using ECG only
data = Lolo_ecg(data);
% figure(11);plot(data.timestamps-data.timestamps(1),data.ecg.signal);hold on;plot(data.ecg.mat_lolo.timestamps-data.timestamps(1),data.ecg.mat_lolo.signal*1000,'r')


%% calculate lol using ETI only
data = Lolo_eti(data);
figure(22);plot(data.timestamps-data.timestamps(1),data.ecg.signal);hold on;plot(data.timestamps-data.timestamps(1),data.eti.signal,'c');plot(data.eti.mat_lolo.timestamps-data.timestamps(1),data.eti.mat_lolo.signal*1000,'r');plot(data.ecg.mat_lolo.timestamps-data.timestamps(1),data.ecg.mat_lolo.signal*1000,'m');legend('ecg','eti','lolo_{eti}','lolo_{ecg}')


