%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <CI_raw>
% Content   : Main script for CI_raw calculation
% Version   : GIT 3
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History:
%  | Developer   | Version |    Date   |
%  |  ECWentink  |   0.1   | 01-08-2014|
%  |  ECWentink  |   0.2   | 01-08-2014|
%  |  ECWentink  |   0.3   | 08-08-2014|
%  |  ECWentink  |   2.0   | 08-08-2014|
%  |  ECWentink  |   2.1   | 30-09-2014|new model
%  |  ECWentink  |   2.2   | 01-10-2014|fix lit  vs ppg
%  |  ECWentink  |   3.0   | 01-10-2014|new thresholds for ppg and model for ECG for 21.1 data
%  |  ECWentink  |   3.1   | 01-10-2014|fast amp raise removal, extra thresholds low vs medium CI
%  |  ECWentink  |   4.0   | 04-03-2015| only for channel 'ppg.e', now using visual filtred data.
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input: all data, ECG/PPG and ACC data at least and the "channel" which
% you wish to calculte it on
%% output
% CI_raw: the CI_raw on raw data per second
% CI_raw_mn: the CI_raw averaged over 5 second in the past
% CI_DBraw: debug information
% CI_explan: the explanation/reasoning behind the 1-4 selection-> ie "too much motion " etc to
% determine why choise was made-> so easy for debugging
% CI_times: time in seconds!!
%%%%%%%%%%%%%%%%%%%%%%
function [output] = Lolo_main(input)

output=input;

% deel p-p door de huidige ACG->betere match -> pas thresholds aan.
sig = eval(['input.ecg.signal']);
% visual filtered data needs to be there
signv = diff(input.ecg.signal);

t=input.timestamps;
acc = input.acc.All;

if size(sig,2) == 1
    sig = sig';
end


%% definitions of the parameters
Fs = 128;
%% windowing
wind = (1*Fs)-1; % 4sec for time domain stuff
udr = 0.5*Fs; % update rate, every second

%% accelerometer data calculations

e = 1;

DB_inf=zeros(size(sig,2)/udr,5);
DB_inffilt=zeros(size(sig,2)/udr,7);
e_top = ceil(min([size(acc,2), size(sig,2)-3])/udr);
SKfilt = zeros(e_top,1);
sigvals =zeros(e_top,5);
sigvalsfilt =zeros(e_top,5);
accvals =zeros(e_top,5);
CI_p2filt = cell(e_top,1);
         Lolo_est = 0;
LL_check = 'lead_off';
for a = 3:udr:min([size(acc,2), size(sig,2)-3])
    %% first CI will be 1 due to lack of data
    if (a)<wind
            DB_inf(e,1:9)=0;
            DB_inffilt(e,1:7)=0;
            Lolo_est(e)=0;
    else
        %% Assign data in 1 sec windows
        acc2sec = acc(4,a-wind:a)'; % 1 sec of only on the magnitude of acc
        sig2sec = sig(a-wind:a)';  % a sec the signal PPG
        sig2sec_temp = sig(a-wind:a+1)';  % a sec the signal PPG
        signv2sec = diff(sig2sec_temp); % 1 sec visual filtered PPG

        %% calc the the amplitude parameters (min, max, peak peak, mean, std)
        % Vals det calculates the: min, max, Peak-peak, mean and std of the window
        sigvals(e,:) = Valsdet(sig2sec);
        sigvalsfilt(e,:) = Valsdet(signv2sec);
        accvals(e,:) = Valsdet(acc2sec);

        % the skewness
%         SKfilt(e)= skewness((signv2sec));  
% the skewness without the signal processing toolbox
        SKfilt(e) = ((((sum((signv2sec-mean(signv2sec)).^3)))/length(signv2sec))/(std(signv2sec))^3)*-1;
        

        DB_inf2(e,:) = sigvals(e,:);
        DB_inffilt(e,:) = [sigvalsfilt(e,:),accvals(e,3), SKfilt(e)];
       
        
        if e > 4
            if Lolo_est(e-1) > 0
                LL_check = 'lead_on';
            else
                LL_check = 'lead_off';
            end
        end
        
        [Lolo_est(e),~] = Lolo(DB_inf2(:,:),DB_inffilt(:,:),e,LL_check,accvals(:,:));
        
        if Lolo_est(e)<0
            lolo(e) = 0;
        else
            lolo(e)= 1;
        end

    end
    e=e+1;
end
clear e
t=t';
t_snr = t(1:udr:size(sig,2));%
t_snr=t_snr';

%% the PPG results saved
% Main outputs
output.ecg.mat_CIraw.timestamps= t_snr'; % the time for the CI
output.ecg.mat_CIraw.DB.lolo=lolo;

%% Debug parameters
output.ecg.mat_CIraw.DB.CI_DBraw = DB_inf; % the debug info (all the parameters, those used and unused)
output.ecg.mat_CIraw.DB.CI_DBraw2 = DB_inf2; % the debug info (all the parameters, those used and unused)
output.ecg.mat_CIraw.DB.CI_DBfilt = DB_inffilt; % the debug info (all the parameters, those used and unused)
output.ecg.mat_CIraw.DB.CI_DBrawacc =accvals;
output.ecg.mat_CIraw.DB.Lolo_est=Lolo_est;
