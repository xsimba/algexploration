%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Treeforbaddata>
% Content   : Script for recognizing the "bad signal parts"
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      : 15/10/2014
%  Modification and Version History: 
%  | Developer   | Version |    Date   | 
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input : all the debug information from CI_raw
% and the window number: "e"
% output: different noise causes => all "odd numbers" are converted to 1
% -> ie bad data-> all even numbers are potentially good data but will be
% checked further
%%%%%%%%%%%%%%%%%%%%%%

function [LL,p] = Lolo(DB_inf2,DB_inffilt,e,LL_check,accvals)

p='undetermined';

if DB_inffilt(e,1)<1 && DB_inffilt(e,2)<1 && DB_inffilt(e,3)<1  && DB_inffilt(e,4)<1 && DB_inffilt(e,5)<1
           LL=0;
            p='dataloss';
           LL_check = 'dataloss';
           return
elseif strcmp('lead_off',LL_check)

        if DB_inffilt(e-1,4)<DB_inffilt(e-1,5) && DB_inffilt(e,5)<=DB_inffilt(e,4) &&  accvals(e,3)<0.2 || DB_inffilt(e-1,4)>=DB_inffilt(e-1,5) && DB_inffilt(e,5)>DB_inffilt(e,4)  &&  accvals(e,3)<0.2 || DB_inffilt(e,5)-DB_inffilt(e,4)<20 &&  accvals(e,3)<0.2
           LL=2;
            p='Cross';
           LL_check = 'lead_on';
           return
        elseif (DB_inffilt(e,5) - DB_inffilt(e,4))/DB_inffilt(e,5)<0.7  &&  accvals(e,3)<0.2
            LL=1;
            p='Cross';
           LL_check = 'lead_on';
           return
       elseif DB_inffilt(e,7) <= -3 && DB_inf2(e,2)<-4000 && DB_inf2(e,3)> 3000 && DB_inf2(e,2)<0  &&  accvals(e,3)<0.2 && DB_inf2(e,4 )<0 
           LL = 3;
           p='SKdown';
           LL_check = 'lead_on';
           return
       elseif  DB_inf2(e,3)> 5000 % || DB_inf2(e,4)< -10000
           LL = -5;
           p='keepLeadoff';
           LL_check = 'lead_off';
           return
        else
           LL = -7;
           p='keepleadoff';
           LL_check = 'lead_off';
           return
            
        end

elseif strcmp('lead_on',LL_check)
           
        if DB_inffilt(e,7) >=1.4   && DB_inf2(e,4)> -1000 &&  DB_inf2(e-1,4)> -7000 %&& DB_inf2(e-1,4)< -3000 %&& DB_inf2(e,1 )>0 && DB_inf2(e,2)>0
           LL = -1;
           p='SKUP';
           LL_check = 'lead_off';
           return
        elseif DB_inf2(e-1,4)> -7000 && DB_inf2(e-1,4)< -3000 && DB_inf2(e,4)-DB_inf2(e-1,4)> 15000 %&& DB_inf2(e,1 )>0 && DB_inf2(e,2)>0
           LL = -2;
           p='MeanUP';
           LL_check = 'lead_off';
           return
           
          
        else
           LL = 5;
           p='keepLeadon';
           LL_check = 'lead_on';
           return
        end
else
        if  DB_inf2(e,4)>-500  || DB_inf2(e,4)< -10000
           LL = -6;
           p='noLeadoff';
           LL_check = 'lead_off';
           return
        elseif  DB_inf2(e,5)>10000 
           LL = -3;
           p='noLeadoff';
           LL_check = 'lead_off';
           return
        elseif  DB_inf2(e,5)> 15000 
           LL = -8;
           p='keepLeadoff';
           LL_check = 'lead_off';
           return
        elseif  DB_inf2(e,3)> 5000 
           LL = -8;
           p='keepLeadoff';
           LL_check = 'lead_off';
           return
        elseif  DB_inf2(e,4)> 0 
           LL = -8;
           p='keepLeadoff';
           LL_check = 'lead_off';
           return

       elseif DB_inffilt(e,7) <= -3 && DB_inf2(e,2)<-4000 && DB_inf2(e,3)> 3000 && DB_inf2(e,2)<0  %&& DB_inf2(e,1 )<2000 
           LL = 4;
           p='SKdown';
           LL_check = 'lead_on';
           return
        else
           LL = -0.5;
           p='unknown';
           LL_check = 'lead_unknown';
           p = 'lead_unknown';
           return
        end
end           

