%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Lolo_ecg>
% Content   : Main script for lead on/off based on ECG
% Version   : GIT 3
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Date      :  14/03/2014
%  Modification and Version History:
%  | Developer   | Version |    Date   |
%  |  ECWentink  |   1.0   | 03-2015| initial version
%  |  SWeijs     |   1.1   | 04-2015| c-code matching
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
%%%%%%%%%%%%
% Input: ECG data
%% output
% lolo_ecg.signal: lead on/off based on ecg signal only per half second
% .timestamps: the matching timestamps
% DB.lolo: debug information
%%%%%%%%%%%%%%%%%%%%%%
function [output] = Lolo_ecg(input, offset)

output=input;

% deel p-p door de huidige ACG->betere match -> pas thresholds aan.
sig = input.ecg.signal;
% visual filtered data needs to be there
t=input.timestamps;

if isfield(input,'acc')
acc = input.acc.All;
else
    acc = zeros(4,length(sig));
end


if size(sig,2) == 1
    sig = sig';
end

if nargin > 1
    sig = sig(offset:end);
    acc = acc(:,offset:end);
end

%% definitions of the parameters
Fs = 128;
%% windowing
wind = (1*Fs)-1; % 4sec for time domain stuff
udr = 0.5*Fs; % update rate, every second

%% accelerometer data calculations

e = 1;

DB_inf=zeros(round(size(sig,2)/udr),5);
DB_inffilt=zeros(round(size(sig,2)/udr),8);
e_top = ceil(min([size(acc,2), size(sig,2)-3])/udr);
SKfilt = zeros(e_top,1);
sigvals =zeros(e_top,5);
sigvalsfilt =zeros(e_top,5);
accvals =zeros(e_top,5);
CI_p2filt = cell(e_top,1);
         Lolo_est = 0;
LL_check = 'lead_off';

for a = 0:udr:min([size(acc,2), size(sig,2)-1])
    %% first CI will be 1 due to lack of data
    if (a)<wind
            DB_inf(e,1:9)=0;
            DB_inffilt(e,1:8)=0;
            Lolo_est(e)=0;
    else
        %% Assign data in 1 sec windows
        acc2sec = acc(4,a-wind:a)'; % 1 sec of only on the magnitude of acc
        sig2sec = sig(a-wind:a)';  % a sec the signal PPG
        sig2sec_temp = sig(a-wind:a+1)';  % a sec the signal PPG
        signv2sec = diff(sig2sec_temp); % 1 sec visual filtered PPG

        
        %% calc the the amplitude parameters (min, max, peak peak, mean, std)
        % Vals det calculates the: min, max, Peak-peak, mean and std of the window
        sigvals(e,:) = Valsdet(sig2sec);
        sigvalsfilt(e,:) = Valsdet(signv2sec);
        accvals(e,:) = Valsdet(acc2sec);

  
        %% the skewness without the signal processing toolbox
        SKfilt(e) = ((((sum((signv2sec-mean(signv2sec)).^3)))/length(signv2sec))/(std(signv2sec))^3)*-1;
        SK(e) = ((((sum((sig2sec-mean(sig2sec)).^3)))/length(sig2sec))/(std(sig2sec))^3)*-1;
%% additional parameters
        exi(e) = ((sigvals(e,4)-sigvals(e,1))./sigvals(e,1)) - (((sigvals(e,4)-sigvals(e,1))./sigvals(e,1)) - ((sigvals(e,4)-sigvals(e,2))/sigvals(e,2)));
        exi2(e) = (sigvals(e,4) -sigvals(e,1)) /(sigvals(e,4)-sigvals(e,2));
        
%         DB_inf2(e,:) = sigvals(e,:);
        DB_inffilt(e,:) = [sigvalsfilt(e,:),accvals(e,3), SKfilt(e),exi(e)];
       
        
      
        if e > 4
            if Lolo_est(e-1) > 0
                LL_check = 'lead_on';
            else
                LL_check = 'lead_off';
            end
        end
        
        [Lolo_est(e),~] = Lolo(sigvals(:,:),DB_inffilt(:,:),e,LL_check,accvals(:,:));
               
        if Lolo_est(e)<=0
            lolo2(e) = 0;
        else
            lolo2(e)= 1;
        end

    end
    e=e+1;
end
clear e
t_snr = t(1:udr:size(sig,2));%

%% the PPG results saved
% Main outputs
output.ecg.mat_lolo.timestamps= t_snr'; % the time for the CI
output.ecg.mat_lolo.signal=lolo2;

%% Debug parameters
output.ecg.mat_lolo.DB.siganl = DB_inf; % the debug info (all the parameters, those used and unused)
output.ecg.mat_lolo.DB.DBraw = sigvals; % the debug info (all the parameters, those used and unused)
output.ecg.mat_lolo.DB.DBfilt = DB_inffilt; % the debug info (all the parameters, those used and unused)
output.ecg.mat_lolo.DB.DBrawacc =accvals;
output.ecg.mat_lolo.DB.Lolo_est=Lolo_est;
output.ecg.mat_lolo.DB.SK=SK;


