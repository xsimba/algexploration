function Hd = FilterDesignGSR128to32Hz
%FILTERDESIGNGSR1024TO128HZ Returns a discrete-time filter object.

% MATLAB Code
% Generated by MATLAB(R) 8.3 and the DSP System Toolbox 8.6.
% Generated on: 01-May-2015 15:28:22

Fpass = 5;    % Passband Frequency
Fstop = 16;   % Stopband Frequency
Apass = 0.5;  % Passband Ripple (dB)
Astop = 80;   % Stopband Attenuation (dB)
Fs    = 128;  % Sampling Frequency

h = fdesign.lowpass('fp,fst,ap,ast', Fpass, Fstop, Apass, Astop, Fs);

Hd = design(h, 'equiripple', ...
    'MinOrder', 'any', ...
    'StopbandShape', 'flat');



