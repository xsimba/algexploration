function yoACCb = ACClinInt(yfACC, NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamACC, DlACC, TSstartACC, FsACCe, TSfreq)
%ACClinInt ACC linear interpolation process

yoACCb = zeros(1,(NsegOUT-1)*NsamOUT);
for seg = 1:NsegOUT-1
    %% ACC block processing
    % Find segment edges for ACC
    ACC_first = DlACC +                               (TSstartOUT(seg) - TSstartACC)*16*FsACCe/TSfreq;
    ACC_last  = DlACC + (NsamOUT-1)*16*FsACCe/FsOUT + (TSstartOUT(seg) - TSstartACC)*16*FsACCe/TSfreq;

    [~, ACC_first_seg] = min(ACC_first.*(ACC_first>=0) + (ACC_first<0)*NsamACC*16);
    [~, ACC_last_seg ] = min(ACC_last .*(ACC_last >=0) + (ACC_last <0)*NsamACC*16);
    
    % Calculate 'local' FsACC
    if (ACC_last_seg < length(TSstartACC))
        FsACCl = (ACC_last_seg+1-ACC_first_seg)*NsamACC*TSfreq/(TSstartACC(ACC_last_seg+1)-TSstartACC(ACC_first_seg));
        
        % Re-calculate sample indexes
        ACC_first = DlACC + (TSstartOUT(seg) - TSstartACC)*16*FsACCl/TSfreq;
        [ACC_first_sam, ACC_first_seg] = min(ACC_first.*(ACC_first>=0) + (ACC_first<0)*NsamACC*16);
        
        ACCstartSam = ACC_first_sam + (ACC_first_seg-1)*NsamACC*16;
        ACCsamInc   = 16*FsACCl/FsOUT;
        
        Sacc = ACCstartSam + ACCsamInc*(0:NsamOUT-1); % ACC sample indexes
        Sacc = max(min(Sacc,length(yfACC)-2),1); % Limit from 1 to ACC vector length
        SIacc = floor(Sacc);  % Integer part
        SFacc = Sacc - SIacc; % Fractional part
        
        y0 = yfACC(SIacc+1); % index+0
        y1 = yfACC(SIacc+2); % Index+1
        
        % Map to y = pb*x + pc, where x = 0 or +1
        pc = y0; % Solve for pb and pc
        pb = y1-y0;
        
        idx = (seg-1)*NsamOUT + 1;
        yoACCb(idx:idx+NsamOUT-1) = pb.*SFacc + pc; % Interpolation range: x = 0.0 to +1.0        
    end
end

end

