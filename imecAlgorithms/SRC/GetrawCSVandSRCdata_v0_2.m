% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : GetrawCSVandSRCdata.m
%  Content    : Import raw data from the band from .csv files and SRC convert them
%  Version    : 0.2
%  Package    : SIMBA
%  Created by : E.C. Wentink 
%  Date       : 16-07-2014
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function [output] = GetrawCSVandSRCdata_v0_2(folder_name, Channels)

warning off;
b = str2num((Channels));
% folder_name = 'D:\DATA\SIMBA\Algorithms\SRC_1\Simba';% your data should be in this folder in csv files

%% get the CSV's into matlab
%the output will be a .mat file called "data' in the folder of 'folder_name'
CSV2MAT2(folder_name,b); % if you already have the 'data' file in the folder, than comment this line for speed, the next line will load the data.

%% load the .mat file if the above is already done, this saves some time
load([ folder_name '\data.mat' ]);

%% plot the raw data
figure;
subplot(2,1,1);plot(ECG);title('ECG before SRC')
subplot(2,1,1);plot(ETI);title('ETI before SRC')
subplot(2,1,1);plot(PPG1(:,2),'b');hold on;plot(PPG2(:,2),'g');plot(PPG3(:,2),'r');plot(PPG4(:,2),'m');plot(PPG5(:,2),'y');plot(PPG6(:,2),'k');legend('blue', 'green', 'red', 'IR','green','green') ;title('PPG before SRC')
subplot(2,1,2);plot(AccX(:,2),'b');hold on;plot(AccY(:,2),'r');plot(AccZ(:,2),'g');title('ACC before SRC');legend('X','Y','Z')

%% Get the SRC data
% in the struct "output" all the SCR converted signals are present
[output,Fs] = test_1(folder_name);

%save the above
save(['.\' folder_name '\data_src' ],'output','Fs');




