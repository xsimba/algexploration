% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : SRC.m
%  Content    : Sample Rate Converter
%  Version    : 0.2
%  Package    : SIMBA.Validation
%  Created by : A. Young 
%  Date       : 24-04-2014
%  Modification and Version History: 
%  | Developer     | Version |    Date   |  Comments   | 
%  | p.casale      |   1.0   | 12-05-2014| Refactoring |
%  | E.C. Wentink  |   1.1   | 16-07-2014| added 2 ppg channels (now 6) |
%  | E.C. Wentink  |   1.2   | 01-09-2014| changed naming of Acc data more conventional |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
function output = SRC(input, mask)
    output = input;
    % clear all;
    % close all;
    % clc;
    FIR_functions = {'ECGsrcDec4','ECGsrcDec4','PPGsrcInt16','BioZsrcDec8','ACCsrcInt16'};
    num_of_fir = length(FIR_functions);
    FIR = cell(1,num_of_fir);
    for idx = 1:num_of_fir
        FIR{idx} = feval(FIR_functions{idx});
    end

    % Filter group delays
    delays = zeros(1,num_of_fir);
    for idx = 1:num_of_fir
        temp = grpdelay(FIR{idx});
        % Reduce to single values, this is the group delay referred to the input sample rates
        delays(idx) = temp(1);
    end

    % Block based processing
    TSfreq = 32768;

    TSref = 0;
    if mask(1)
        TSref = input.ecg.TSstart(1); 
    end % Make reference ecg as then no illegal interpolation will be performed on it.

    if mask(1)
        input.ecg.TSstart  = input.ecg.TSstart  - TSref; 
    end
    
    if mask(2)
        input.eti.TSstart = input.eti.TSstart  - TSref; 
    end
    
%     if (Enableppg),    TSstartppga = TSstartppga - TSref;
%                        TSstartppgb = TSstartppgb - TSref; end
%     if (Enableppg102), TSstartppgc = TSstartppgc - TSref;
%                        TSstartppgd = TSstartppgd - TSref; end
    if mask(3)
        input.ppg.a.TSstart = input.ppg.a.TSstart - TSref;
        input.ppg.b.TSstart = input.ppg.b.TSstart - TSref;
        input.ppg.c.TSstart = input.ppg.c.TSstart - TSref;
        input.ppg.d.TSstart = input.ppg.d.TSstart - TSref;
        input.ppg.e.TSstart = input.ppg.e.TSstart - TSref;
        input.ppg.f.TSstart = input.ppg.f.TSstart - TSref;
    end
    
    if mask(4)
        input.BioZ.TSstart = input.BioZ.TSstart  - TSref;  
    end
    
    if mask(5)
        input.acc.TSstart = input.acc.TSstart  - TSref; 
    end

    MinList = [];
    if mask(1)                                                                                    
        MinList = [MinList length(input.ecg.signal)/input.ecg.Fs];   
    end
    if  mask(3)
        Fsppge = (length(input.ppg.a.TSstart)-1)*input.ppg.a.Nsam*TSfreq/...
            (input.ppg.a.TSstart(end)-input.ppg.a.TSstart(1)); 
        MinList = [MinList length(input.ppg.a.signal)/Fsppge]; 
    end % Number of samples/time duration
    
    if  mask(5)
        Fsacce = (length(input.acc.TSstart)-1)*input.acc.Nsam*TSfreq/...
            (input.acc.TSstart(end)-input.acc.TSstart(1));    
        MinList = [MinList length(input.acc.signal.x)/Fsacce]; 
    end 
    Tout = min(MinList); % Minimum time duration of output stream

    % Define output parameters
    FsOUT = 128; % Output sample rate
    SegDur = 0.500; % Segment duration (s)
    NsamOUT = round(FsOUT*SegDur); % Number of samples per block at output rate
    NsegOUT = floor(Tout*FsOUT/NsamOUT); % Number of segments
    TSstartOUT  = (0:NsegOUT-1)*NsamOUT*TSfreq/FsOUT;  % All output start time stamps
        
    % Filtering and decimation/interpolation for SRC
    if mask(1)
        yfecg   = filter(FIR{1},input.ecg.signal);    
        % Down sample to FsOUT
        yfecg   = yfecg(1:(input.ecg.Fs/FsOUT):end); 
        % Synchronous  SRC for ecg
        output.ecg.Fs = FsOUT ;
        output.ecg.signal = LinearInt( yfecg,   NsegOUT, NsamOUT, TSstartOUT, FsOUT, delays(1),input.ecg.TSstart,input.ecg.Fs,  TSfreq); 
    end    

    if mask(2)
        yfeti   = filter(FIR{2},input.eti.signal);   
        % Down sample to FsOUT
        yfeti   = yfeti(1:(input.eti.Fs/FsOUT):end); 
        % Synchronous  SRC for eti at same rate as ecg
        output.eti.Fs = FsOUT ;
        output.eti.signal   = LinearInt( yfeti,   NsegOUT, NsamOUT, TSstartOUT, FsOUT, delays(2),input.eti.TSstart,input.eti.Fs,TSfreq); 
    end

    if mask(3)
        % Interpolate to ~1600 Hz (16*Fsppg)
        yfppga  = filter(FIR{3},input.ppg.a.signal); 
        yfppgb  = filter(FIR{3},input.ppg.b.signal);
        yfppgc  = filter(FIR{3},input.ppg.c.signal); 
        yfppgd  = filter(FIR{3},input.ppg.d.signal);
        yfppge = filter(FIR{3},input.ppg.e.signal); 
        yfppgf = filter(FIR{3},input.ppg.f.signal);
        % Asynchronous SRC for ppg
        output.ppg.a.Fs = FsOUT ;
        output.ppg.a.signal  = PPGquadInt(yfppga,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.ppg.a.Nsam, delays(3),  input.ppg.a.TSstart, Fsppge, TSfreq); 
        output.ppg.b.Fs = FsOUT ;
        output.ppg.b.signal  = PPGquadInt(yfppgb,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.ppg.b.Nsam, delays(3),  input.ppg.b.TSstart, Fsppge, TSfreq);
        output.ppg.c.Fs = FsOUT ;
        output.ppg.c.signal  = PPGquadInt(yfppgc,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.ppg.c.Nsam, delays(3),  input.ppg.c.TSstart, Fsppge, TSfreq); 
        output.ppg.d.Fs = FsOUT ;
        output.ppg.d.signal  = PPGquadInt(yfppgd,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.ppg.d.Nsam, delays(3),  input.ppg.d.TSstart, Fsppge, TSfreq);
        output.ppg.e.Fs = FsOUT ;
        output.ppg.e.signal  = PPGquadInt(yfppge,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.ppg.e.Nsam, delays(3),  input.ppg.e.TSstart, Fsppge, TSfreq); 
        output.ppg.f.Fs = FsOUT ;
        output.ppg.f.signal  = PPGquadInt(yfppgf,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.ppg.f.Nsam, delays(3),  input.ppg.f.TSstart, Fsppge, TSfreq);
    end

    if mask(4)
        yfBioZi = filter(FIR{4},input.BioZ.signal); 
        yfBioZq = filter(FIR{4},input.BioZ.yq); 
        % Down sample to FsOUT
        yfBioZi = yfBioZi(1:(input.BioZ.Fs/FsOUT):end); 
        yfBioZq = yfBioZq(1:(input.BioZ.Fs/FsOUT):end);
        % Synchronous  SRC for BioZ
        output.BioZ.Fs = FsOUT ;
        output.BioZ.signal = LinearInt( yfBioZi, NsegOUT, NsamOUT, TSstartOUT, FsOUT, delays(4), input.BioZ.TSstart, input.BioZ.Fs, TSfreq); 
        yoBioZq = LinearInt( yfBioZq, NsegOUT, NsamOUT, TSstartOUT, FsOUT, delays(4), input.BioZ.TSstart, input.BioZ.Fs, TSfreq);
    end

    if mask(5)
        % Interpolate to ~2000 Hz (16*Fsacc)
        yfaccx  = filter(FIR{5},input.acc.signal.x);   
        yfaccy  = filter(FIR{5},input.acc.signal.y);   
        yfaccz  = filter(FIR{5},input.acc.signal.z);   
        output.acc.Fs = FsOUT ;
        output.acc.x.signal  = ACClinInt( yfaccx,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.acc.Nsam, delays(5),  input.acc.TSstart,  Fsacce, TSfreq); % Asynchronous SRC for acc
        output.acc.y.signal  = ACClinInt( yfaccy,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.acc.Nsam, delays(5),  input.acc.TSstart,  Fsacce, TSfreq);
        output.acc.z.signal  = ACClinInt( yfaccz,  NsegOUT, NsamOUT, TSstartOUT, FsOUT, input.acc.Nsam, delays(5),  input.acc.TSstart,  Fsacce, TSfreq);
    end
    
end



