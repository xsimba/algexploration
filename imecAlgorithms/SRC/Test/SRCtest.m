%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : SRCtest.m
% Content   : Matlab script for test of the SRC (sample rate converter) algorithm using synthetic waveforms
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

clear all;
close all;
clc;

% Add paths for SRC.m and filters
addpath ../

% Build filters to obtain frequency responses
HdECG      = ECGsrcDec4();        % FIR: ECG  dec by  4 input rate  512 Hz
HdBioZ     = BioZsrcDec8();       % FIR: BioZ dec by  8 input rate 1024 Hz
HdPPG      = PPGsrcInt16();       % FIR: PPG  int by 16 input rate ~100 Hz
HdACC      = ACCsrcInt16();       % FIR: ACC  int by 16 input rate ~125 Hz

% Filter to band limit to 30 Hz
Hd30HzMain = Main30HzLPF128kFs(); % IIR: Main filter LPF 30 Hz/0.1 dB pass, 49.5 Hz/125 dB stop @ 128kHz rate

% Filter responses
figure; hold on;
[h,w]=freqz(HdECG,65536);        plot(w*512   /(2*pi),max(20*log10(abs(h)   ),-140),'m');
[h,w]=freqz(HdBioZ,65536);       plot(w*1024  /(2*pi),max(20*log10(abs(h)   ),-140),'c');
[h,w]=freqz(HdPPG,65536);        plot(w*100*16/(2*pi),max(20*log10(abs(h)/16),-140),'b'); % Assuming 100 Hz PPG
[h,w]=freqz(HdACC,65536);        plot(w*125*16/(2*pi),max(20*log10(abs(h)/16),-140),'r'); % Assuming 125 Hz ACC
[h,w]=freqz(Hd30HzMain,1048576); plot(w*128000/(2*pi),max(20*log10(abs(h)   ),-140),'k'); % 128 kHz, many frequency points
grid on; title('Filter responses.'); legend('ECG','BioZ','PPG','ACC','main');
%grid on; title('Filter response.');
ylim([-140 0.1]); xlim([0 800]);
clear h w;

% Filter group delay
[Dlmain,w] = grpdelay(Hd30HzMain,131072);
figure; plot(w*128000/(2*pi),Dlmain,'k'); grid on; xlim([0 50]);


%% Test
FsMain = 128000; % Mother sample rate, all sample rates must be derrived from this by division by an integer.

SigLenMain = round(FsMain*11); % 10+1 seconds
Ymain = zeros(1,SigLenMain);

Shape = 2; % 0: Triangle, 1: Raised cosine, 2: Sine, 3: DC offset, tone and 2nd harmonic
Ftest = 1.00; % Frequency of test waveform (Hz)

hPPG = abs(freqz(HdPPG.Numerator,1.0,[0 Ftest 2*Ftest],1600))/16; % PPG 16*100 = 1600
hACC = abs(freqz(HdACC.Numerator,1.0,[0 Ftest 2*Ftest],2000))/16; % ACC 16*125 = 2000
disp(['PPG frequency response at DC, Ftest (',num2str(Ftest),' Hz) and 2Ftest (',num2str(2*Ftest),' Hz): ',num2str(hPPG,8)]);
disp(['ACC frequency response at DC, Ftest (',num2str(Ftest),' Hz) and 2Ftest (',num2str(2*Ftest),' Hz): ',num2str(hACC,8)]);

if     (Shape == 0), Ymain(64001:192001) = 1 - abs(((-64000:64000)-0.25)/64000); % Triangle
elseif (Shape == 1), Ymain(64001:192001) = (1 + cos(pi*(-64000:64000)/64000))/2; % Raised cosine
elseif (Shape == 2), x = 2*pi*(0:SigLenMain-1)*Ftest/FsMain; Ymain = sin(x);
else                 x = 2*pi*(0:SigLenMain-1)*Ftest/FsMain; Ymain = 2 + sin(x) + 0.6*sin(2*x+1.0);                     
end

% 0.5 second taper in and out
Ymain(1:64000) = Ymain(1:64000).*(1-cos(pi*(0:63999)/64000))/2;
Ymain(end-63999:end) = Ymain(end-63999:end).*(1+cos(pi*(0:63999)/64000))/2;

Ymain = Ymain/max(Ymain); % Normalise

YmainF = filter(Hd30HzMain,Ymain); % Apply filter
YmainF = YmainF/max(YmainF);       % Normalise

% We could run the filter in the reverse direction too to eliminate non-linear phase effects... but it's not a requirement.
% YmainF = fliplr(filter(Hd30HzMain,fliplr(YmainF))); % Apply filter in reverse direction
% YmainF = YmainF/max(YmainF);                        % Normalise

% figure;hold on;plot(Ymain,'b');plot(YmainF,'r');legend('Unfiltered','Filtered'); % Determine required delay

% Align so that peak is at 1.000 seconds exactly for waveforms with one distinct peak
if (Shape == 0), YmainF = YmainF(10227:end); end % Traingle
if (Shape == 1), YmainF = YmainF(9653:end);  end % Raised cosine

figure;hold on;grid on;
plot((0:length(Ymain)-1)/FsMain,Ymain,'b');
plot((0:length(YmainF)-1)/FsMain,YmainF,'r');
title('Mother sample source (128kHz sample rate)');xlabel('Time (s)');legend('Unfiltered','Filtered')

FsECG  = 512;   % Fixed sample rate for ECG
FsBioZ = 1024;  % Fixed sample rate for BioZ
FsPPGr = 101.0; % Variable (requested) sample rate for PPG (exact rates: 100.0, 102.4 but any frequency can be chosen)
FsACCr = 125.0; % Variable (requested) sample rate for ACC (exact rates: 125.0, 128.0 but any frequency can be chosen)

% Sample rate division ratios for decimation
DivECG  = FsMain/FsECG;
DivBioZ = FsMain/FsBioZ;
DivPPG  = round(FsMain/FsPPGr); FsPPG = FsMain/DivPPG; % Adjusted sample rate for PPG
DivACC  = round(FsMain/FsACCr); FsACC = FsMain/DivACC; % Adjusted sample rate for ACC

disp(['Division ratios, ECG: ',num2str(DivECG),', BioZ: ',num2str(DivBioZ),', PPG: ',num2str(DivPPG),', ACC: ',num2str(DivACC)]);
disp(['Sample rates PPG: ',num2str(FsPPGr,12),' -> ',num2str(FsPPG,12),', ACC: ',num2str(FsACCr,12),' -> ',num2str(FsACC,12)]);

% Simulated input streams: decimation and apply time stamps
TSfreq = 32768;
yECG  = YmainF(1:                DivECG: end); xECG  = (0:                DivECG: length(YmainF)-1)*TSfreq/FsMain;
yBioZ = YmainF(1:                DivBioZ:end); xBioZ = (0:                DivBioZ:length(YmainF)-1)*TSfreq/FsMain;
yPPG  = YmainF(1:                DivPPG: end); xPPG  = (0:                DivPPG: length(YmainF)-1)*TSfreq/FsMain;
yPPGs = YmainF(1+round(DivPPG/2):DivPPG: end); xPPGs = (0+round(DivPPG/2):DivPPG: length(YmainF)-1)*TSfreq/FsMain; % PPG sampled with approx. 1/2 sample delay (time multiplexed)
yACC  = YmainF(1:                DivACC: end); xACC  = (0:                DivACC: length(YmainF)-1)*TSfreq/FsMain;

figure; hold on; grid on;
plot(xECG, yECG, '-ok');
plot(xBioZ,yBioZ,'-ob');
plot(xPPG, yPPG, '-or');
plot(xPPGs,yPPGs,'--or');
plot(xACC, yACC, '-og');
legend('ECG','BioZ','PPG','PPGs','ACC');
xlabel('Time (timestamps @ 32768 Hz)');

% FsOUT = 128;
% 
% NsamOUT = 64; % Number of samples per block at output rate
% NsegOUT = floor(length(Ymain)*FsOUT/(FsMain*NsamOUT)); % Number of segments

NsamECG = 256; % Number of samples per input block at ECG rate
NsegECG = floor(length(xECG)/NsamECG); % Number of segments

NsamBioZ = 512; % Number of samples per input block at BioZ rate
NsegBioZ = floor(length(xBioZ)/NsamBioZ); % Number of segments

NsamPPG = 50; % Number of samples per input block at PPG rate
NsegPPG = floor(length(xPPGs)/NsamPPG); % Number of segments

NsamACC = 125; % Number of samples per input block at ACC rate
NsegACC = floor(length(xACC)/NsamACC); % Number of segments

% Trim inputs to multiples of block sizes
yECG  = yECG(1:NsegECG*NsamECG);
yBioZ = yBioZ(1:NsegBioZ*NsamBioZ);
yPPG  = yPPG(1:NsegPPG*NsamPPG);
yPPGs = yPPGs(1:NsegPPG*NsamPPG);
yACC  = yACC(1:NsegACC*NsamACC);


input.ECG.signal  = yECG(1:NsegECG*NsamECG); % ECG signal
input.ECG.TSstart = xECG(1:NsamECG:end);     % ECG timestamps for each block
input.ECG.Fs      = FsECG;

input.BioZ.signal.i = yBioZ(1:NsegBioZ*NsamBioZ);  % BioZ (in-phase) signal
input.BioZ.signal.q = -yBioZ(1:NsegBioZ*NsamBioZ); % BioZ (quadrature) signal = negative of in-phase for test
input.BioZ.TSstart  = xBioZ(1:NsamBioZ:end);       % BioZ timestamps for each block
input.BioZ.Fs       = FsBioZ;

input.PPG.a.signal  = yPPG(1:NsegPPG*NsamPPG);  % PPG signal A
input.PPG.a.TSstart = xPPG(1:NsamPPG:end);      % PPG signal A timestamps for each block
input.PPG.a.Nsam    = NsamPPG;
input.PPG.b.signal  = yPPGs(1:NsegPPG*NsamPPG); % PPG signal B
input.PPG.b.TSstart = xPPGs(1:NsamPPG:end);     % PPG signal B timestamps for each block
input.PPG.b.Nsam    = NsamPPG;
input.PPG.c.signal  = yPPG(1:NsegPPG*NsamPPG);  % PPG signal C (copy of A)
input.PPG.c.TSstart = xPPG(1:NsamPPG:end);      % PPG signal C timestamps for each block
input.PPG.c.Nsam    = NsamPPG;
input.PPG.d.signal  = yPPGs(1:NsegPPG*NsamPPG); % PPG signal D (copy of B)
input.PPG.d.TSstart = xPPGs(1:NsamPPG:end);     % PPG signal D timestamps for each block
input.PPG.d.Nsam    = NsamPPG;

input.ACC.signal.x = yACC(1:NsegACC*NsamACC);                  % ACC(x) signal
input.ACC.signal.y = -yACC(1:NsegACC*NsamACC);                 % ACC(y) signal = negative of ACC(x)
input.ACC.signal.z = zeros(1,length(yACC(1:NsegACC*NsamACC))); % ACC(z) signal = zeros
input.ACC.TSstart  = xACC(1:NsamACC:end);                      % ACC timestamps for each block
input.ACC.Nsam     = NsamACC;

%       ECG ETI PPG BioZ ACC
mask = [ 1   0   1   1    1 ];

% Call SRC
output = SRC(input, mask);


leg = {}; % Empty legend (cell array)
figure; hold on;
if mask(1), plot((0:length(output.ECG.signal)-1)/output.ECG.Fs,     output.ECG.signal,'.-b');    leg = [leg 'ECG'];   end
if mask(2), plot((0:length(output.ETI.signal)-1)/output.ETI.Fs,     output.ETI.signal,'s-c');    leg = [leg 'ETI'];   end
if mask(3), plot((0:length(output.BioZ.signal.i)-1)/output.BioZ.Fs, output.BioZ.signal.i,'.-k'); leg = [leg 'BioZi'];
            plot((0:length(output.BioZ.signal.q)-1)/output.BioZ.Fs, output.BioZ.signal.q,'o-k'); leg = [leg 'BioZq']; end
if mask(4), plot((0:length(output.PPG.a.signal)-1)/output.PPG.a.Fs, output.PPG.a.signal,'.-g');  leg = [leg 'PPGa'];
            plot((0:length(output.PPG.b.signal)-1)/output.PPG.b.Fs, output.PPG.b.signal,'o-g');  leg = [leg 'PPGb'];  end
if mask(5), plot((0:length(output.ACC.signal.x)-1)/output.ACC.Fs,   output.ACC.signal.x,'.-m');  leg = [leg 'ACCx'];       end
title('Aligned output block processing');legend(leg);

%% Align input and output data




% hPPG = abs(freqz(HdPPG.Numerator,1.0,[0 Ftest 2*Ftest],1600))/16; % PPG 16*100 = 1600
% hACC = abs(freqz(HdACC.Numerator,1.0,[0 Ftest 2*Ftest],2000))/16; % ACC 16*125 = 2000
% disp(['PPG frequency response at DC, Ftest (',num2str(Ftest),' Hz) and 2Ftest (',num2str(2*Ftest),' Hz): ',num2str(hPPG,8)]);
% disp(['ACC frequency response at DC, Ftest (',num2str(Ftest),' Hz) and 2Ftest (',num2str(2*Ftest),' Hz): ',num2str(hACC,8)]);

% Try for ECG only...

for n = 1:5
    if mask(n)
        
    end
end

q = [TSstartPPGa 2*TSstartPPGa(end)-TSstartPPGa(end-1)]; % Create a point past the end to make the interp work  
TSinterpPPGa = interp1(q, 1:1/NsamPPG:length(q));        % Interpolate the timestampts to give one stamp to each sample
TSinterpPPGa = TSinterpPPGa(1:end-1);                    % Take off the last one to adjust the length

t2 = 0:1/FsOUT:((length(yoPPGa))-1)/FsOUT;
figure;plot(t2,yoPPGa); hold on;plot(TSinterpPPGa/TSfreq,yPPGa,'r');





