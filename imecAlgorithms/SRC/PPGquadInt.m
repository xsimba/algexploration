function yoPPGb = PPGquadInt(yfPPG, NsegOUT, NsamOUT, TSstartOUT, FsOUT, NsamPPG, DlPPG, TSstartPPG, FsPPGe, TSfreq)
%PPGquadInt PPG quadratic interpolation process

yoPPGb = zeros(1,(NsegOUT-1)*NsamOUT);
for seg = 1:NsegOUT-1
    %% PPG block processing
    % Find segment edges for PPG
    PPG_first = DlPPG +                               (TSstartOUT(seg) - TSstartPPG)*16*FsPPGe/TSfreq;
    PPG_last  = DlPPG + (NsamOUT-1)*16*FsPPGe/FsOUT + (TSstartOUT(seg) - TSstartPPG)*16*FsPPGe/TSfreq;

    [~, PPG_first_seg] = min(PPG_first.*(PPG_first>=0) + (PPG_first<0)*NsamPPG*16);
    [~, PPG_last_seg ] = min(PPG_last .*(PPG_last >=0) + (PPG_last <0)*NsamPPG*16);
    
    % Calculate 'local' FsPPG
    if (PPG_last_seg < length(TSstartPPG))
        FsPPGl = (PPG_last_seg+1-PPG_first_seg)*NsamPPG*TSfreq/(TSstartPPG(PPG_last_seg+1)-TSstartPPG(PPG_first_seg));
        
        % Re-calculate sample indexes
        PPG_first = DlPPG + (TSstartOUT(seg) - TSstartPPG)*16*FsPPGl/TSfreq;
        [PPG_first_sam, PPG_first_seg] = min(PPG_first.*(PPG_first>=0) + (PPG_first<0)*NsamPPG*16);
        
        PPGstartSam = PPG_first_sam + (PPG_first_seg-1)*NsamPPG*16;
        PPGsamInc   = 16*FsPPGl/FsOUT;
        
        Sppg = PPGstartSam + PPGsamInc*(0:NsamOUT-1); % PPG sample indexes
        Sppg = max(min(Sppg,length(yfPPG)-2),1); % Limit from 1 to PPG vector length
        SIppg = round(Sppg);  % Integer part
        SFppg = Sppg - SIppg; % Fractional part
        
        yz = yfPPG(SIppg);   % Index-1
        y0 = yfPPG(SIppg+1); % index+0
        y1 = yfPPG(SIppg+2); % Index+1
        
        % Map to y = pa*x^2 + pb*x + pc, where x = -1, 0 or +1
        pa = (yz+y1)/2-y0; % Solving for pa, pb and pc
        pb = (y1-yz)/2;
        pc = y0;
        
        idx = (seg-1)*NsamOUT + 1;
        yoPPGb(idx:idx+NsamOUT-1) = pa.*SFppg.^2 + pb.*SFppg + pc; % Interpolation range: x = -0.5 to +0.5
    end
end

end

