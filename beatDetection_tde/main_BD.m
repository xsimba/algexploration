clear all;
load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\FloydData\12052014\12052014_randy.mat')
index_track=5;
tracks = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
eval(['InputStream = data.ppg.' , tracks(index_track), '.signal;']);
% eval(['signal = (data.ppg.' , tracks(index_track), '.signal);']);

[BeatInfo] = TDE_BD( InputStream, 128);


plot(BeatInfo.time ,BeatInfo.InputStream_PPG);
hold on;
plot(BeatInfo.time(BeatInfo.locations),BeatInfo.InputStream_PPG(BeatInfo.locations),'r*')
plot(BeatInfo.time(BeatInfo.locations_raw),BeatInfo.InputStream_PPG(BeatInfo.locations_raw),'go')

figure
plot(data.timestamps,data.ppg.e.signal)
hold on;
plot(data.timestamps(BeatInfo.locations+500),data.ppg.e.signal(BeatInfo.locations+500),'r*')
plot(data.timestamps(BeatInfo.locations_inv+500),data.ppg.e.signal(BeatInfo.locations_inv+500),'g*')