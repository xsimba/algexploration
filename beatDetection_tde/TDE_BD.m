% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : TDE_BD.m
%  Content    : Beat Detection for PPG signal
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 03-17-2015
%  Modification and Version History:
%  | Developer | Version |    Date   |
%  |  Yelei    |   1.0   | 03-17-2015|

% --------------------------------------------------------------------------------

function [BeatInfo] = TDE_BD( InputStream, Fs, varargin)

if ~exist('Fs')
   Fs = 128; 
end

sFilter.N     = 10;     % Order
sFilter.Fstop = 3.5;    % Stopband Frequency
sFilter.Astop = 60;     % Stopband Attenuation (dB)
% Fs    = 128;    % Sampling Frequency
sFilter.n_ma = 5;

gp = +3.6;
h = fdesign.lowpass('n,fst,ast', sFilter.N, sFilter.Fstop, sFilter.Astop, Fs);
Hd = design(h, 'cheby2');

InputStream_PPG = filter(Hd,InputStream);
InputStream_PPG=InputStream_PPG(501:end);
dev_sig=(diff(InputStream_PPG));

time_delay = 50;
BeatInfo.locations = find(dev_sig(1:end-time_delay)<=dev_sig(time_delay:end-1) & dev_sig(2:end-time_delay+1)>dev_sig(time_delay+1:end));
BeatInfo.locations_inv = find(dev_sig(1:end-time_delay)>=dev_sig(time_delay:end-1) & dev_sig(2:end-time_delay+1)<dev_sig(time_delay+1:end));
BeatInfo.locations_raw = find(InputStream_PPG(1:end-time_delay)<=InputStream_PPG(time_delay:end-1) & InputStream_PPG(2:end-time_delay+1)>InputStream_PPG(time_delay+1:end));

dev_sig2=-dev_sig;
%      time_delay = 50;
BeatInfo.locations2 = find(dev_sig2(1:end-time_delay)<=dev_sig2(time_delay:end-1) & dev_sig2(2:end-time_delay+1)>dev_sig2(time_delay+1:end));
BeatInfo.locations2_inv = find(dev_sig2(1:end-time_delay)>=dev_sig2(time_delay:end-1) & dev_sig2(2:end-time_delay+1)<dev_sig2(time_delay+1:end));
%     plot(data.timestamps,data.ppg.e.signal)

time_delay = 70;
BeatInfo.locations3 = find(dev_sig(1:end-time_delay)<=dev_sig(time_delay:end-1) & dev_sig(2:end-time_delay+1)>dev_sig(time_delay+1:end));

BeatInfo.time = (1:length(InputStream_PPG))/128;
BeatInfo.InputStream_PPG = InputStream_PPG;

end