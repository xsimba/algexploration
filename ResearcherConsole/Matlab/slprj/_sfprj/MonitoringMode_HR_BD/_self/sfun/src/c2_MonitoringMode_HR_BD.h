#ifndef __c2_MonitoringMode_HR_BD_h__
#define __c2_MonitoringMode_HR_BD_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_MonitoringMode_HR_BDInstanceStruct
#define typedef_SFc2_MonitoringMode_HR_BDInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint8_T c2_is_active_c2_MonitoringMode_HR_BD;
  real_T c2_Idx_max;
  boolean_T c2_Idx_max_not_empty;
  real_T c2_Amp_max;
  boolean_T c2_Amp_max_not_empty;
  real_T c2_Amp_pre;
  boolean_T c2_Amp_pre_not_empty;
  real_T c2_Amp_nxt;
  boolean_T c2_Amp_nxt_not_empty;
  real_T c2_Idx_ppg;
  boolean_T c2_Idx_ppg_not_empty;
  real_T c2_tempAmp_middle;
  boolean_T c2_tempAmp_middle_not_empty;
  real_T c2_tempAmp_pre;
  boolean_T c2_tempAmp_pre_not_empty;
  real_T c2_tempAmp_nxt;
  boolean_T c2_tempAmp_nxt_not_empty;
  real_T *c2_ppgIn;
  real_T *c2_ppgBeatSample_PositiveOffset;
  boolean_T *c2_resetFlag;
} SFc2_MonitoringMode_HR_BDInstanceStruct;

#endif                                 /*typedef_SFc2_MonitoringMode_HR_BDInstanceStruct*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */
extern const mxArray *sf_c2_MonitoringMode_HR_BD_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c2_MonitoringMode_HR_BD_get_check_sum(mxArray *plhs[]);
extern void c2_MonitoringMode_HR_BD_method_dispatcher(SimStruct *S, int_T method,
  void *data);

#endif
