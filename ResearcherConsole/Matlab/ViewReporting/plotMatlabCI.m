function okCode = plotMatlabCI( data,signal )
    
        okCode = 0 ; 
        ifFullScreen = 0 ; 
        ifLegend = 0 ;
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953;];
       
        if strcmpi(signal,'ecg')
            
            if isfield(data.ecg ,'CI_raw') 
                s = data.ecg.signal;
                s_times = data.timestamps-data.timestamps(1) ; 
                ci_m = data.ecg.CI_raw ;
                % assumes times are integer starting from 0
                ci_m_time = data.ecg.CI_times;
                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];  
            end
            
         elseif strcmpi(signal(1:3),'ppg')
            channel = lower(signal(end));
            if isfield(data.ppg.(channel) ,'CI_raw')
                s = data.ppg.(channel).signal;
                s_times = data.timestamps-data.timestamps(1) ; 
                ci_m = data.ppg.(channel).CI_raw ;
                % assumes times are integer starting from 0
                ci_m_time = round(data.ppg.(channel).CI_times);
                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];
            end
            
        end
        
        if ifFullScreen
            figure('units','normalized','outerposition',[0 0 1 1])
        end
        
        % hack for legend
        if ifLegend
            plot(1,s(1),'color',col(1,:));hold on;
            plot(1,s(1),'color',col(2,:));hold on;
            plot(1,s(1),'color',col(3,:));hold on;
            plot(1,s(1),'color',col(4,:));hold on;
            [~,objh,~,~] =legend({'1','2','3','4'});
            set(objh,'linewidth',2.3);
        end
        % 

        for idx = 1:length(ci_m_time)
            try
                if ci_m(idx)~= 0
                    timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                    plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
                end
            catch err
                if strcmpi(err.identifier,'MATLAB:badsubscript')
                    %keyboard()
                    disp('end of stream');
                else
                    keyboard()
                end
            end
        end 
        ylabel('Matlab CI')

        title(upper(signal))
                
end