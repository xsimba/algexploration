function plotname = generate_histogram( data,params )

    figure('units','normalized','outerposition',[0 0 1 1])
    hist(data,params.nbins);grid on;
    h = findobj(gca,'Type','patch');
    set(h,'FaceColor',[.8 .8 1],'EdgeColor','k')
    if isfield(params,'xlims')
        xlim(params.xlims)
    end
    ylabel(params.ylabel);
    xlabel(params.xlabel)
    plotname = params.plotname;
    saveas(gcf,plotname,'fig');
    saveas(gcf,plotname,'png');
    
    close all

end

