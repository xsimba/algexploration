function okCode = plotBeats( data,signal,channel )
   

        if ~exist('spotMode','var')
            spotMode = 0 ;
        end
    
        okCode = 0 ; 
        
        if strcmpi(signal,'ecg')
           
            if isfield(data.(signal),'beats') && ~isfield(data.(signal),'beats_c')
                
                s = data.ecg.signal;
                s_time = data.timestamps;
                matlabBeats_time = data.ecg.beats(2:end);
                
                figure('units','normalized','outerposition',[0 0 1 1])
                if spotMode && isfield(data,'spot_check')
                    s = zscore(s);
                    bandBeats_Amp = interp1(s_time,s,matlabBeats_time);
                    plot(data.spot_check.timestamps,data.spot_check.signal,'r','LineWidth',2);hold on
                    legend('SpotCheck Enabled')
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(matlabBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                    ylabel('Matlab Beats');
                else
                    bandBeats_Amp = interp1(s_time,s,matlabBeats_time);
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(matlabBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                    xlabel('Time [s]');ylabel('Matlab Beats');

                end
                
                title(upper(signal));
                
            elseif isfield(data.(signal) ,'beats') && isfield(data.(signal),'beats_c')

                s = data.(signal).signal;
                m_bts = round(data.fs*data.(signal).beats(:)) ;
                m_bts(isnan(m_bts)) = [];
                m_bts(m_bts<=0) = [];
                c_bts = round(data.fs*data.(signal).beats_c(:)) ;
                c_bts = c_bts(5:end);
                c_bts(c_bts<=0) = [];
                ax1 = subplot(211); plot(s);hold on;plot(m_bts,s(m_bts),'or');
                ylabel('Matlab Beats')
                ax2 = subplot(212); plot(s);hold on;plot(c_bts,s(c_bts),'or');
                ylabel('C Beats')
                title(ax1,signal)
                title(ax2,signal)
                linkaxes([ax1,ax2],'xy');

            end
        elseif strcmpi(signal(1:3),'ppg')
            channel = lower(signal(end));
            if isfield(data.ppg.(channel),'beats') && ~isfield(data.ppg.(channel),'beats_c')
                                
                s = data.ppg.(channel).signal;
                s_time = data.timestamps;
                matlabBeats_time = data.ppg.(channel).beats(1,4:end);
                
                
                figure('units','normalized','outerposition',[0 0 1 1])
                if spotMode && isfield(data,'spot_check')
                    s = zscore(s);
                    bandBeats_Amp = interp1(s_time,s,matlabBeats_time);
                    plot(data.spot_check.timestamps,data.spot_check.signal,'r','LineWidth',2);hold on
                    legend('SpotCheck Enabled')
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(matlabBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                    ylabel('Matlab Beats');
                else
                    bandBeats_Amp = interp1(s_time,s,matlabBeats_time);
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(matlabBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                    xlabel('Time [s]');ylabel('Matlab Beats');
                end
                
                title(upper(signal))
                
            elseif isfield(data.ppg.(channel),'beats') && isfield(data.ppg.(channel),'beats_c')
                
                s = data.ppg.(channel).signal;
                fs = data.fs;
                m_bts = round(data.fs*data.ppg.(channel).beats) ;
                m_bts(m_bts<=0) = NaN;
                m_bts_1 = m_bts(1,:);
                m_bts_1(isnan(m_bts_1)) = [];
                m_bts_2 = m_bts(2,:);
                m_bts_2(isnan(m_bts_2)) = [];
                m_bts_4 = m_bts(4,:);
                m_bts_4(isnan(m_bts_4)) = [];
                c_bts = round(data.fs*data.ppg.(channel).beats_c) ;
                c_bts = c_bts(:,5:end);
                c_bts(c_bts<=0) = NaN;
                c_bts_1 = c_bts(1,:);
                c_bts_1(isnan(c_bts_1)) = [];
                c_bts_2 = c_bts(2,:);
                c_bts_2(isnan(c_bts_2)) = [];
                c_bts_4 = c_bts(4,:);
                c_bts_4(isnan(c_bts_4)) = [];
                ax1 = subplot(211); plot((1:length(s))/fs,s);hold on;plot(m_bts_1/fs,s(m_bts_1),'or');plot(m_bts_2/fs,s(m_bts_2),'+r');plot(m_bts_4/fs,s(m_bts_4),'*r');
                ylabel('Matlab Beats')
                ax2 = subplot(212); plot((1:length(s))/fs,s);hold on;plot(c_bts_1/fs,s(c_bts_1),'or');plot(c_bts_2/fs,s(c_bts_2),'+r');plot(c_bts_4/fs,s(c_bts_4),'*r');
                ylabel('C Beats')
                title(ax1,[signal,'.',channel])
                title(ax2,[signal,'.',channel])
                linkaxes([ax1,ax2],'xy');
                
            end
            
        end
        
        okCode = 1 ;
end
