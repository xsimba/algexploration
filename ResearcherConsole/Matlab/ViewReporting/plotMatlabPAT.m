function [fig_debug_plot_matlab,okCode] = plotMatlabPAT_FE(data)

chan = 'e';

if isfield(data, 'spot_check')
    spotCheck = 1;
else
    spotCheck = 0;
end

if isfield(data,'pat')
    timestamps = data.ppg.(chan).features.timestamps;
    
    beat2beat_pats = data.ppg.(chan).features.pat.signal;%
    
    averaged_pats = data.ppg.(chan).features.pat.runningAverage;
    
    averaged_stdpat = data.ppg.(chan).features.pat.runningAverage_sd;
    
    hr_raw = data.ppg.(chan).features.HR.signal;%
    
    averaged_HR = data.ppg.(chan).features.HR.runningAverage;
    averaged_stdHR = data.ppg.(chan).features.HR.runningAverage_sd;
    
    scrsz = get(0,'ScreenSize');
    fig_debug_plot_matlab = figure('Name', 'Matlab Debug Plot PAT, HR and BP FE', 'units','normalized','outerposition',[0 0 1 1]); hold on;
    suptitle('Matlab')
    
    pat_subplot = subplot(2, 1, 1); hold on;
    errorbar(timestamps,averaged_pats,averaged_stdpat,'ok');
    legend('PAT','Location','NW');hold on
    plot(timestamps, beat2beat_pats,'go')
    %pat avg for BP
    plot(data.ppg.e.spotcheck.timestamps, data.ppg.e.spotcheck.pat.signal, 'r+','MarkerSize', 10)
    if spotCheck
        plot(data.spot_check.timestamps, nanmean(averaged_pats)*data.spot_check.signal,'r--','LineWidth',2)
        legend({'PAT avg (all valid PATs)','PAT raw (all raw PATs)', 'PATavg for BP', 'Spot Check'},'Location','NW');
    end
    ylim([0.15 0.45])
    ylabel('Matlab PAT');
    
    
    
    hr_subplot = subplot(2, 1, 2); hold on;
    errorbar(timestamps, averaged_HR,averaged_stdHR,'ok');legend('HR','Location','NW');hold on
    plot(timestamps, hr_raw,'go')
    %hr avg for BP
    plot(data.ppg.e.spotcheck.timestamps, data.ppg.e.spotcheck.HR.signal, 'r+','MarkerSize', 10)
    if spotCheck
        plot(data.spot_check.timestamps, nanmean(averaged_HR)*data.spot_check.signal,'r--','LineWidth',2)
        legend({'HR avg (all valid HRs)','HR raw (all raw HRs)', 'HRavg for BP','Spot Check'},'Location','NW');
    end
    ylabel('Matlab HR');
    
    all_axes = [pat_subplot, hr_subplot];
    linkaxes(all_axes, 'x')
    
end

if isfield(data.ppg.(chan),'bp')
    
    if isfield(data.ppg.(chan).bp.tracker, 'file')
        
        for i=1:length(data.ppg.(chan).bp.dbp.timestamps)
            
            timeBP = data.ppg.(chan).bp.dbp.timestamps(i);
            
            deltaSBP = data.ppg.e.bp.tracker.file.calibration_based.via_pwv.sbp.signal(i) - data.ppg.e.bp.tracker.file.reference.sbp_cal;
            deltaDBP = data.ppg.e.bp.tracker.file.calibration_based.via_pwv.dbp.signal(i) - data.ppg.e.bp.tracker.file.reference.dbp_cal;
            
            subplot(211)
            %BP Ref
            text(timeBP, nanmax(averaged_pats)+0.1, ...
                ['BP Ref: ',num2str(data.ppg.e.bp.tracker.file.reference.sbp_cal), '/', num2str(data.ppg.e.bp.tracker.file.reference.dbp_cal)],'FontSize',10);
            %BP Ref Band
            text(timeBP, nanmax(averaged_pats)+0.075,...
                ['BP Ref Band: ',num2str(data.bp.tracker.band.reference.sbp_cal.signal(1)), '/', num2str(data.bp.tracker.band.reference.dbp_cal.signal(1))],'FontSize',10);
            %BP Absolute
            text(timeBP, nanmax(averaged_pats)+0.05,...
                ['BP: ',num2str(round(data.ppg.(chan).bp.sbp.signal(i))),'/',num2str(round(data.ppg.(chan).bp.dbp.signal(i)))],'FontSize',10);
            %BP Delta
            text(timeBP, nanmax(averaged_pats)+0.025,...
                ['BP Delta: ',num2str(deltaSBP), '/', num2str(deltaDBP)],'FontSize',10);
            %BP Delta Band
            text(timeBP, nanmax(averaged_pats)+0,...
                ['BP Delta Band: ',num2str(data.bp.tracker.band.sbp_delta.signal(i)),'/',num2str(data.bp.tracker.band.dbp_delta.signal(i))],'FontSize',10);
            %plot HR
            text(timeBP, nanmax(averaged_pats)-0.025,...
                ['HR: ',num2str(round(data.ppg.(chan).spotcheck.HR.signal(i)))],'FontSize',10);
            %plot CI
            text(timeBP, nanmax(averaged_pats)-0.05,...
                ['CI: ',num2str(round(data.ppg.(chan).spotcheck.CI(i)))],'FontSize',10);
            %Matlab PAT
            text(timeBP, nanmax(averaged_pats)-0.075,...
                ['PAT: ',num2str(data.ppg.(chan).spotcheck.pat.signal(i))],'FontSize',10);
            %Band PAT
            text(timeBP, nanmax(averaged_pats)-0.1,...
                ['PAT Band: ',num2str(data.pat.typical.signal(i)./1000)],'FontSize',10);
            %Matlab upstgrad
            text(timeBP, nanmax(averaged_pats)-0.125,...
                ['upstgrad: ',num2str(data.ppg.(chan).spotcheck.ppg_upstgrad.signal(i))],'FontSize',10);
            %Band upstgrad
            text(timeBP, nanmax(averaged_pats)-0.15,...
                ['upstgrad Band: ',num2str(data.validation.ppg_upstgrad.signal(i))],'FontSize',10);
            %Matlab pp_ft_amp
            text(timeBP, nanmax(averaged_pats)-0.175,...
                ['pp_ft_amp: ',num2str(data.ppg.(chan).spotcheck.ppg_upstgrad.signal(i))],'FontSize',10);
            %Band pp_ft_amp
            text(timeBP, nanmax(averaged_pats)-0.2,...
                ['pp_ft_amp Band: ',num2str(data.validation.ppg_upstgrad.signal(i))],'FontSize',10);
            
            subplot(212)
            text(timeBP, nanmax(averaged_HR),...
                ['BP: ',num2str(round(data.ppg.(chan).bp.sbp.signal(i))),'-',num2str(round(data.ppg.(chan).bp.dbp.signal(i)))],'FontSize',15);
            
        end
        okCode = 1;
        
    end
end

