function [fig_debug_plot] = plotDebugInfoPATandBP(data, chan, debugplot_title)

chan = chan(end);

%basic settings
marker_size = 5;
legend_entries = {}; %to be filled with signal infos

%only necessary for manual plotting of band beat amplitude (both ECG & PPG)
t_offset = data.timestamps(1);

%definition figure
fig_debug_plot = figure('Name', ['Debug Plot PAT and BP ' debugplot_title]); hold on;

%basic signals that should always be available

    %ppg signal
    plot(data.timestamps, data.ppg.(chan).signal - nanmean(data.ppg.(chan).signal), 'b', 'LineWidth', 1.3)
    legend_entries{end+1} = 'PPG';
    
    %ppg matlab beats
    %plot(data.ppg.(chan).bd.upstroke, data.ppg.(chan).bd.debug.usamp - nanmean(data.ppg.(chan).signal),'ro','MarkerSize',marker_size,'LineWidth',2)
    %legend_entries{end+1} = 'PPG upstrokes';
    
    %ppg matlab primary peaks     
    plot(data.ppg.(chan).bd.foot,data.ppg.(chan).bd.debug.ftamp - nanmean(data.ppg.(chan).signal),'ro','MarkerSize',marker_size,'LineWidth',2)
    legend_entries{end+1} = 'PPG foot';
    plot(data.ppg.(chan).bd.upstroke,data.ppg.(chan).bd.debug.usamp - nanmean(data.ppg.(chan).signal),'go','MarkerSize',marker_size,'LineWidth',2)
    legend_entries{end+1} = 'PPG upstroke';
    plot(data.ppg.(chan).bd.pripeak,data.ppg.(chan).bd.debug.ppamp - nanmean(data.ppg.(chan).signal),'mo','MarkerSize',marker_size,'LineWidth',2)
    legend_entries{end+1} = 'PPG primary peak';
    plot(data.ppg.(chan).bd.secpeak,data.ppg.(chan).bd.debug.spamp - nanmean(data.ppg.(chan).signal),'yo','MarkerSize',marker_size,'LineWidth',2)
    legend_entries{end+1} = 'PPG secondary peak';
    plot(data.ppg.(chan).bd.dicrnot,data.ppg.(chan).bd.debug.dnamp - nanmean(data.ppg.(chan).signal),'ko','MarkerSize',marker_size,'LineWidth',2)
    legend_entries{end+1} = 'PPG dicrotic notch';
    
    %ecg signal
    plot(data.timestamps, data.ecg.signal - nanmean(data.ecg.signal),'k','LineWidth',1.3)
    legend_entries{end+1} = 'ECG';
    %ecg matlab beats
    plot(data.ecg.bd.rpeak, data.ecg.bd.debug.usamp - nanmean(data.ecg.signal),'go','MarkerSize',marker_size,'LineWidth',2)
    legend_entries{end+1} = 'ECG beats M';
    %feature CI HR
    plot(data.ppg.e.features.timestamps+0.1, data.ppg.e.features.HR.CI*1000,'c*')
    legend_entries{end+1} = 'HR CI'; 
    %feature CI PAT
    plot(data.ppg.e.features.timestamps, data.ppg.e.features.pat.CI*1000,'r*')
    legend_entries{end+1} = 'PAT CI';    
    %PAT hq interval
    plot(data.ppg.e.highquality.timestamps, data.ppg.e.highquality.nElapsedBeats*1000, 'c')
    legend_entries{end+1} = 'PAT HQ Interval';    
    %PAT hq interval threshold
    plot(data.ppg.e.highquality.timestamps, ones(1,length(data.ppg.e.highquality.timestamps))*10*1000, 'c:')  
    legend_entries{end+1} = 'PAT HQ Interval th';     
    
    %CI values raw (no legend entry required)
    CI_raw_signal = round(data.ppg.e.features.pat.CI(~isnan(data.ppg.e.features.pat.CI)));
    CI_raw_timestamps = data.ppg.e.features.timestamps(~isnan(data.ppg.e.features.pat.CI));
    for i=1:length(CI_raw_signal)
        text(CI_raw_timestamps(i), 500, num2str(CI_raw_signal(i)));
    end
    %high quality CI (no legend entry required)
    highquality_CI = round(data.ppg.e.highquality.CI(~isnan(data.ppg.e.highquality.timestamps)));
    timestamps_HQ_CI = data.ppg.e.highquality.timestamps(~isnan(data.ppg.e.highquality.timestamps));
    for i=1:length(highquality_CI)
        text(timestamps_HQ_CI(i), 1000, num2str(highquality_CI(i)));
    end     
    
%additional signals not always available   
    
    %spot check 
    if isfield(data, 'spot_check')
        plot(data.spot_check.timestamps, data.spot_check.signal*10000 - nanmean(data.spot_check.signal*10000), '--md','MarkerSize',marker_size,'LineWidth',2);
        legend_entries{end+1} = 'Spot Check';
    end
    
%     %lolo flag
%     if isfield(data, 'ecg_lead')
%         plot(data.ecg_lead.timestamps, data.ecg_lead.signal*10000 - nanmean(data.ecg_lead.signal*10000) , '--kd','MarkerSize',marker_size,'LineWidth',2)
%         legend_entries{end+1} = 'LoLo';   
%     end    
   
%     %ecg ecg band beats
%     if isfield(data, 'ecg_lead')
%         %define non-negative access indices in case of offsets or similar
%         access_indices_ecg = round((data.ecg.bd.rpeak(1:end) - t_offset)*128+1);
%         access_indices_ecg = access_indices_ecg(access_indices_ecg>0);
%         deleted_elements_ecg = abs(numel(access_indices_ecg) - numel(data.ecg.bd.rpeak));
%         plot(data.ecg.bd.rpeak(1+deleted_elements_ecg:end), data.ecg.signal(access_indices_ecg) - nanmean(data.ecg.signal), 'gx','MarkerSize',marker_size,'LineWidth',2)
%         legend_entries{end+1} = 'ECG beats C';
%     end
    
%     %ppg band beats
%     if isfield(data.ppg.(chan),'band_beats')
%         plot(data.ppg.(chan).band_beats.timestamps, data.ppg.(chan).band_beats.signal, 'r+','MarkerSize',marker_size,'LineWidth',2) %too low amplitude, probably AGC related?!
%         legend_entries{end+1} = 'PPG beats C';
%         %alternative...(not possible with getSimbandCsv)
%         %define non-negative access indices in case of offsets or similar
%         access_indices_ppg = round((data.ppg.(chan).band_beats.timestamps(1:end) - t_offset)*128+1);
%         access_indices_ppg = access_indices_ppg(access_indices_ppg>0);
%         deleted_elements_ppg = abs(numel(access_indices_ppg) - numel(data.ppg.(chan).band_beats.timestamps));
%         plot(data.ppg.(chan).band_beats.timestamps(1+deleted_elements_ppg:end), data.ppg.(chan).signal(access_indices_ppg) - nanmean(data.ppg.(chan).signal), 'rx','MarkerSize',marker_size,'LineWidth',2)
%         legend_entries{end+1} = 'PPG beats C alt';
%     end
    
%figure parameters

legend(legend_entries);
xlabel('Time[s]')
ylabel('Amplitude')
title(['Selected PPG channel: ' chan]);

