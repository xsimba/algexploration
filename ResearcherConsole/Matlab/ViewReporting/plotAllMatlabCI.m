function ax = plotAllMatlabCI( data )

    
    ppgChannels = 'abcdefgh';
    
    figure('units','normalized','outerposition',[0 0 1 1])
    
    for idx = 1:numel(ppgChannels)
        
        if isfield(data.ppg,ppgChannels(idx))
            ax{idx}=subplot(4,2,idx);
            plotMatlabCI(data,['ppg.',ppgChannels(idx)]);
        end
    end
    
    linkaxes([ax{:}], 'x');

end