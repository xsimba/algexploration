function filename = write_report_ppt( ppt_output_dir,varargin )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    filename = fullfile(ppt_output_dir,['Report_',datestr(fix(clock),30),'.ppt']);
    for idx = 1:numel(varargin)
        this_plots = varargin{idx};
        for jdx = 1:numel(this_plots)
            openfig([this_plots{jdx},'.fig']);
            %saveppt(filename,'-f1',['-s',this_plots{jdx}(2:end)]);
            saveppt(filename,'-f1');
            close all
        end
    end

end

