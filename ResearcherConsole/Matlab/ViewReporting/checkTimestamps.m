function okCode = checkTimestamps( data )
    
    okCode = 0 ; 
    signalsToPlot = {'ecg','acc.x','acc.y','acc.z',...
        'ppg.a','ppg.b','ppg.c','ppg.d',...
        'ppg.e','ppg.f','ppg.g','ppg.h'};

    figure('units','normalized','outerposition',[0 0 1 1]);hold on
    colors = 'kbgcmkbgcmkbgcm';
    for sigIdx = 1:numel(signalsToPlot)
        thiSignal = signalsToPlot{sigIdx};
        if length( thiSignal ) == 3 
            plot(data.(signalsToPlot{sigIdx}).timestamps+(sigIdx),colors(sigIdx),'LineWidth',1.3);
        elseif length( thiSignal ) == 5 
            plot(data.(thiSignal(1:3)).(thiSignal(end)).timestamps+(sigIdx),colors(sigIdx),'LineWidth',1.3);
        end
    end 
    legend(signalsToPlot,'Location','Best');
    xlabel('Samples');ylabel('Time');
    okCode = 1 ; 
end

