function okCode = plotPpgSelectionOLD( data )
    okCode = 0 ; 
    
    ppgChannels = 'abcdefgh';
    default = 5;
    colring = 'bgrmggrgbgrmggrg';
    
    ppg_selection = data.ppg_selection.signal+1 ;
    ppgSelTime = data.ppg_selection.timestamps;
    selection = zeros(8,numel(data.timestamps));
        
    timeIndex = find(data.timestamps >= data.timestamps(1) & data.timestamps < ppgSelTime(1));
    selection(default,timeIndex) = 1;
    
    for idx = 1:numel(ppgSelTime)-1
        timeIndex = find(data.timestamps >= ppgSelTime(idx) & data.timestamps < ppgSelTime(idx+1));
        selection(ppg_selection(idx),timeIndex) = 1;
    end
    
    timeIndex = find(data.timestamps >= ppgSelTime(end) & data.timestamps < data.timestamps(end));
    selection(ppg_selection(end),timeIndex) = 1;
    
    figure('units','normalized','outerposition',[0 0 1 1])
    
    for idx = 1:numel(ppgChannels)
        s = zscore(data.ppg.(ppgChannels(idx)).signal);
        %s = s-min(s)-1;
        if isfield(data.ppg,ppgChannels(idx))
            ax{idx} = subplot(4,2,idx);
            if sum(selection(idx,:))~=0
               %imagesc(repmat(selection(idx,:),50,1));hold on;
               imagesc(selection(idx,:));hold on;
            end
            plot(s,colring(idx),'LineWidth',0.8);
            set(gca,'YDir','normal');%ylim([floor(min(s)) ceil(max(s))])
            %plot(zscore(data.ppg.(ppgChannels(idx)).signal)+8,'k','LineWidth',3);
            %axis off;
            %plotCurrentBandCI(data,['ppg.',ppgChannels(idx)],selection(idx,:));
            ylabel(upper(['ppg.',ppgChannels(idx)]));
        end
    end
    
    okCOde = 1 ; 
    %ax1 = plotAllBandCI( data )
    
    linkaxes([ax{:}], 'xy');
end


function plotCurrentBandCI( data,signal, selection )
    
        okCode = 0 ;  
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953;];
            
            channel = lower(signal(end));
            if isfield(data.ppg.(channel) ,'band_CIraw')
                
                %f1 = figure('units','normalized','outerposition',[0 0 1 1])

                ci_m = data.ppg.(channel).band_CIraw.signal ;
                ci_m_time = data.ppg.(channel).band_CIraw.timestamps;
                ci_m_time = round(ci_m_time-data.timestamps(1)) ; 
                ci_m_time(ci_m_time<0) = [];

                s = data.ppg.(channel).signal;
                s_times = data.timestamps-data.timestamps(1) ; 
                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];
                
                imagesc(selection);hold on;
                
                plot(s,'-k','Markersize',14);
%                 
%                 plot(1,s(1),'color',col(1,:));hold on;
%                 plot(1,s(1),'color',col(2,:));hold on;
%                 plot(1,s(1),'color',col(3,:));hold on;
%                 plot(1,s(1),'color',col(4,:));hold on;
%    
%                 [~,objh,~,~] =legend({'1','2','3','4'});
%                 set(objh,'linewidth',1.5);
%                 
% 
%                 
%                 for idx = 1:length(ci_m_time)-1
%                     try
%                         if ci_m(idx)~= 0
%                             timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
%                             plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
%                         end
%                     catch err
%                         if strcmpi(err.identifier,'MATLAB:badsubscript')
%                             disp('end of stream');
%                         else
%                             keyboard()
%                         end
%                     end
%                 end 
%                 ylabel('Band CI')
%                 title(upper(signal))
%                 set(gca,'XTick',[1:1280:idx+1])
%                 set(gca,'XTickLabel',10*[0:numel([0:1280:idx])])   
%                 set(gca,'FontSize',20);
       
            end

end

