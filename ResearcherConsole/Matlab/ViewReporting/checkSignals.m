function okCode = checkSignals( data )
    
    okCode = 0 ; 
    signalsToPlot = {'ecg','acc.x','acc.y','acc.z',...
        'ppg.a','ppg.b','ppg.c','ppg.d',...
        'ppg.e','ppg.f','ppg.g','ppg.h'};

    figure('units','normalized','outerposition',[0 0 1 1])
    
    for sigIdx = 1:numel(signalsToPlot)
        try
            thiSignal = signalsToPlot{sigIdx};
            ax{sigIdx} = subplot(3,4,sigIdx);
            if length( thiSignal ) == 3 
                plot(data.(signalsToPlot{sigIdx}).timestamps,data.(signalsToPlot{sigIdx}).signal,'LineWidth',1.3);
            elseif length( thiSignal ) == 5 
                plot(data.(thiSignal(1:3)).(thiSignal(end)).timestamps, ...
                    data.(thiSignal(1:3)).(thiSignal(end)).signal,'LineWidth',1.3);
            end
            xlabel('Time [s]');ylabel(upper(thiSignal));
        catch err
            keyboard()
        end
    end 
    
    linkaxes([ax{:}], 'x');
    okCode = 1 ; 
end

