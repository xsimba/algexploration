function okCode = plotBandCIAll( data)

okCode = 0 ;

col = [0.8242    0.8242    0.8242;
    0.4375    0.5000    0.5625;
    1.0000    0.4961    0.3125;
    0.6016    0.8008    0.1953;];

%
tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
figure('units','normalized','outerposition',[0 0 1 1])
clf;

axNum = 1;
if isfield(data, 'ecg') && isfield(data.ecg ,'band_CIraw'),
    ax{axNum} = subplot(331);
    
    
    ci_m = data.ecg.band_CIraw.signal ;
    ci_m_time = round(data.ecg.band_CIraw.timestamps);
    ci_m_time = ci_m_time-data.timestamps(1) ;
    ci_m_time(ci_m_time<0) = [];
    
    s = data.ecg.signal;
    s_times = data.timestamps-data.timestamps(1) ;
    notInUseIdx = s_times<ci_m_time(1);
    s(notInUseIdx) = [];
    s_times(notInUseIdx) = [];
    
    
    plot(1,s(1),'color',col(1,:));hold on;
    plot(1,s(1),'color',col(2,:));hold on;
    plot(1,s(1),'color',col(3,:));hold on;
    plot(1,s(1),'color',col(4,:));hold on;
    
    for idx = 1:length(ci_m_time)
        try
            if ci_m(idx)~= 0
                timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
            end
        catch err
            if strcmpi(err.identifier,'MATLAB:badsubscript')
                disp('end of stream');
            else
                keyboard()
            end
        end
    end
    ylabel(['CI Band ', lower(signal)])
    
end

for k = 2:9,
    signal = tracks{k};
    
    channel = lower(signal(end));
    if isfield(data.ppg, channel) && isfield(data.ppg.(channel) ,'band_CIraw'),
        axNum = axNum +1;
        ax{axNum} = subplot(3,3,k);
        
        ci_m = data.ppg.(channel).band_CIraw.signal ;
        ci_m_time = data.ppg.(channel).band_CIraw.timestamps;
        ci_m_time = round(ci_m_time-data.timestamps(1)) ;
        ci_m_time(ci_m_time<0) = [];
        
        s = data.ppg.(channel).signal;
        s_times = data.timestamps-data.timestamps(1) ;
        notInUseIdx = s_times<ci_m_time(1);
        s(notInUseIdx) = [];
        s_times(notInUseIdx) = [];
        
        plot(1,s(1),'color',col(1,:));hold on;
        plot(1,s(1),'color',col(2,:));hold on;
        plot(1,s(1),'color',col(3,:));hold on;
        plot(1,s(1),'color',col(4,:));hold on;
        
        if (k==3),
            [~,objh,~,~] =legend({'1','2','3','4'});
            set(objh,'linewidth',2.3);
        end
        
        
        for idx = 1:length(ci_m_time)-1
            try
                if ci_m(idx)~= 0
                    timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                    plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
                end
            catch err
                if strcmpi(err.identifier,'MATLAB:badsubscript')
                    disp('end of stream');
                else
                    keyboard()
                end
            end
        end
        ylabel(['Band CI ', lower(signal)])
        
    end
end

okCode = 1 ;
linkaxes([ax{:}], 'x');

end

