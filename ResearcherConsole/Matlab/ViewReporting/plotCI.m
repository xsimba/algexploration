function okCode = plotCI( data,signal )
    
        okCode = 0 ;  
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953;];
       
        if strcmpi(signal,'ecg')
            
            figure('units','normalized','outerposition',[0 0 1 1])
            
            if isfield(data.ecg ,'CI_raw') && ~isfield(data.ecg ,'CI_raw_c')

                s = data.ecg.signal;
                ci_m = data.ecg.CI_raw ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4'},'Location','SE');
                set(objh,'linewidth',2.3);x

                k = 1;
                for idx = 1:128:length(s)
                    try
                        if ci_m(k)~=0
                            plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        end
                        k = k + 1;
                    catch err
                    end
                end
                ylabel('Matlab CI','FontSize',20);xlabel('Time [s]','FontSize',20);

                title(signal)
                set(gca,'XTick',[1:1280:idx+1])
                set(gca,'XTickLabel',10*[0:numel([0:1280:idx])])   
                set(gca,'FontSize',20);

            elseif isfield(data.ecg ,'CI_raw') && isfield(data.ecg ,'CI_raw_c')

                s = data.ecg.signal;
                ci_m = data.ecg.CI_raw ;
                ci_c = data.ecg.CI_raw_c ;
                ci_cts = data.ecg.CI_raw_cts ;
                ci_c = [ones(1,ci_cts(1)) ci_c];

                ax1 = subplot(211);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4'},'Location','SE');
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        if ci_m(k)~=0
                            plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI','FontSize',20);xlabel('Time [s]','FontSize',20);
                
                ax2 = subplot(212);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4'},'Location','SE');
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        if ci_m(k)~=0
                            plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_c(k),:),'LineWidth',1.7);hold on;
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI','FontSize',20);xlabel('Time [s]','FontSize',20);
                
                title(ax1,upper(signal),'FontSize',20)
                set(ax1,'XTick',[1:1280:idx+1])
                set(ax1,'XTickLabel',10*[0:numel([0:1280:idx])])   
                set(ax1,'FontSize',14);

                title(ax2,upper(signal),'FontSize',20)
                set(ax2,'XTick',[1:1280:idx+1])
                set(ax2,'XTickLabel',10*[0:numel([0:1280:idx])])   
                set(ax2,'FontSize',14);

                linkaxes([ax1,ax2],'xy');
            end            
        elseif strcmpi(signal(1:3),'ppg')
            
            figure('units','normalized','outerposition',[0 0 1 1])
            channel = lower(signal(end));
            if isfield(data.ppg.(channel) ,'CI_raw') && ~isfield(data.ppg.(channel) ,'CI_raw_c')

                s = data.ppg.(channel).signal;
                ci_m = data.ppg.(channel).CI_raw ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
   
                [~,objh,~,~] =legend({'1','2','3','4'},'Location','SE');
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        if ci_m(k)~=0
                            plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        end
                        k = k + 1;
                    catch err
                    end
                end
                ylabel('Matlab CI','FontSize',20);xlabel('Time [s]','FontSize',20);
                        
                title(upper(signal),'FontSize',20)
                set(gca,'XTick',[1:1280:idx+1])
                set(gca,'XTickLabel',10*[0:numel([0:1280:idx])])                        
                set(gca,'FontSize',14);  
                
            elseif isfield(data.ppg.(channel) ,'CI_raw') && isfield(data.ppg.(channel) ,'CI_raw_c')

                s = data.ppg.(channel).signal;
                ci_m = data.ppg.(channel).CI_raw ;
                ci_c = data.ppg.(channel).CI_raw_c ;
                ci_cts = data.ppg.(channel).CI_raw_cts ;
                ci_c = [ones(1,ci_cts(1)) ci_c];
                ci_m(ci_m<=0) = 1;
                ci_c(ci_c<=0) = 1;
                
                ax1 = subplot(211);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
   
                [~,objh,~,~] =legend({'1','2','3','4'},'Location','SE');
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        if ci_m(k)~=0
                            plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_m(k),:),'LineWidth',1.7);hold on;
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI','FontSize',20);xlabel('Time [s]','FontSize',20);

                ax2 = subplot(212); 
                
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on; 
                
                [~,objh,~,~] =legend({'1','2','3','4'},'Location','SE');
                set(objh,'linewidth',2.3);
                
                k = 1;
                for idx = 1:128:length(s)
                    try
                        if ci_m(k)~=0
                            plot(idx:idx+128,s(idx:idx+128),'Color',col(ci_c(k),:),'LineWidth',1.7);hold on;
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI','FontSize',20);xlabel('Time [s]','FontSize',20);
                
                title(ax1,upper(signal),'FontSize',20)
                set(ax1,'XTick',[1:1280:idx+1])
                set(ax1,'XTickLabel',10*[0:numel([0:1280:idx])])                        
                set(ax1,'FontSize',14);  

                title(ax2, upper(signal),'FontSize',20)
                set(ax2,'XTick',[1:1280:idx+1])
                set(ax2,'XTickLabel',10*[0:numel([0:1280:idx])])                        
                set(ax2,'FontSize',14);  

                linkaxes([ax1,ax2],'xy');
                
            end
        end
        
        okCode = 1 ;
end
