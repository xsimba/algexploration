function okCode = plotPpgSelection( data )
    okCode = 0 ; 
    
    ppgChannels = 'abcdefgh';
    defaultChannel = 5;
    
    ppg_selection = data.ppg_selection.signal+1 ;
    ppgSelTime = data.ppg_selection.timestamps;
    selection = zeros(8,numel(data.timestamps));
        
    timeIndex = find(data.timestamps >= data.timestamps(1) & data.timestamps < ppgSelTime(1));
    selection(defaultChannel,timeIndex) = 1;
    
    for idx = 1:numel(ppgSelTime)-1
        timeIndex = find(data.timestamps >= ppgSelTime(idx) & data.timestamps < ppgSelTime(idx+1));
        selection(ppg_selection(idx),timeIndex) = 1;
    end
    
    timeIndex = find(data.timestamps >= ppgSelTime(end) & data.timestamps < data.timestamps(end));
    selection(ppg_selection(end),timeIndex) = 1;
    colSel = [0.4648    0.5312    0.5977; 1    0    0;];
    
    figure('units','normalized','outerposition',[0 0 1 1])
    
    for idx = 1:numel(ppgChannels)
       
        if isfield(data.ppg,ppgChannels(idx))
            ax{idx} = subplot(4,2,idx);hold on
%             if sum(selection(idx,:)==0)
%                 plot(data.timestamps,data.ppg.(ppgChannels(idx)).signal,'Color',colSel(1,:));
%             else
%                 findChanges = find([0 diff(selection(idx,:))]~=0);
%                 for jdx = 1:numel(findChanges)
%                     
%                 end
%                 
%                 
%             end
            for jdx = 1:numel(data.timestamps)
                
                plot(data.timestamps(jdx),data.ppg.(ppgChannels(idx)).signal(jdx),'Color',colSel(selection(idx,jdx)+1,:),'LineWidth',2);
            end
            %plot(data.timestamps,data.ppg.(ppgChannels(idx)).signal,'color',selection);
            %plotSelection( data.timestamps, data.ppg.(ppgChannels(idx)).signal, selection )
            ylabel(upper(['ppg.',ppgChannels(idx)]));
        end
    end
     
    %ax1 = plotAllBandCI( data )
    
    linkaxes([ax{:}], 'x');
end
