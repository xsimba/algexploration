function okCode = plotBandCI( data,signal )
    
        okCode = 0 ;  
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953;];
       
        if strcmpi(signal,'ecg')
            
            if isfield(data.ecg ,'band_CIraw') 
                
                figure('units','normalized','outerposition',[0 0 1 1])
                                
                ci_m = data.ecg.band_CIraw.signal ;
                ci_m_time = round(data.ecg.band_CIraw.timestamps);
                ci_m_time = ci_m_time-data.timestamps(1) ; 
                ci_m_time(ci_m_time<0) = [];

                s = data.ecg.signal;
                s_times = data.timestamps-data.timestamps(1) ; 
                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];


                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);

                for idx = 1:length(ci_m_time)
                    try
                        if ci_m(idx)~= 0
                            timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                            plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
                        end
                    catch err
                        if strcmpi(err.identifier,'MATLAB:badsubscript')
                            disp('end of stream');
                        else
                            keyboard()
                        end
                    end
                end 
                ylabel('CI Band')

                title(upper(signal))
                
            end
            
        elseif strcmpi(signal(1:3),'ppg')
            
            channel = lower(signal(end));
            if isfield(data.ppg.(channel) ,'band_CIraw')
                
                figure('units','normalized','outerposition',[0 0 1 1])

                ci_m = data.ppg.(channel).band_CIraw.signal ;
                ci_m_time = data.ppg.(channel).band_CIraw.timestamps;
                ci_m_time = round(ci_m_time-data.timestamps(1)) ; 
                ci_m_time(ci_m_time<0) = [];

                s = data.ppg.(channel).signal;
                s_times = data.timestamps-data.timestamps(1) ; 
                notInUseIdx = s_times<ci_m_time(1);
                s(notInUseIdx) = [];
                s_times(notInUseIdx) = [];
                
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
   
                [~,objh,~,~] =legend({'1','2','3','4'});
                set(objh,'linewidth',2.3);
                

                for idx = 1:length(ci_m_time)-1
                    try
                        if ci_m(idx)~= 0
                            timestamps = s_times>ci_m_time(idx) & s_times<=ci_m_time(idx+1);
                            plot( s_times(timestamps),s(timestamps),'Color',col(ci_m(idx),:),'LineWidth',1.7);hold on;
                        end
                    catch err
                        if strcmpi(err.identifier,'MATLAB:badsubscript')
                            disp('end of stream');
                        else
                            keyboard()
                        end
                    end
                end 
                ylabel('Band CI')
                title(upper(signal))
       
            end
        end
        
        okCode = 1 ;
end

