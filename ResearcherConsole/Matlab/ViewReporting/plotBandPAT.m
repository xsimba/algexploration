function plotBandPAT(data)

if isfield(data, 'spot_check')
    spotCheck = 1;
else
    spotCheck = 0;
end
    
fig_debug_plot_band = figure('Name', 'Simband Debug Plot PAT, HR and BP', 'units','normalized','outerposition',[0 0 1 1]); hold on;
suptitle('Simband')

    if isfield(data,'pat_bp')
        
        pat_subplot = subplot(2, 1, 1); hold on;
        errorbar(data.pat_info.timestamps, data.pat_info.pat_avg, data.pat_info.pat_std,'ok','MarkerSize',10);legend('pat','Location','NW');hold on
        plot(data.pat_info.timestamps, data.pat_info.pat_raw,'go')
        %pat avg for BP
        plot(data.pat_bp.timestamps, data.pat_bp.pat_avg_for_BP, 'r+','MarkerSize', 10)
        if spotCheck
            plot(data.spot_check.timestamps,nanmean(data.pat_bp.pat_avg_for_BP)*data.spot_check.signal,'r--','LineWidth',2);hold on
            legend({'PAT avg (all valid PATs)','PAT raw (all raw PATs)', 'PATavg for BP', 'Spot Check'},'Location','NW');
        end
        ylim([0.15 0.45])
        ylabel('Band pat'); 

        hr_subplot = subplot(2, 1, 2); hold on;
        errorbar(data.pat_info.timestamps, data.pat_info.HR_avg, data.pat_info.HR_std,'ok','MarkerSize',10);legend('HR','Location','NW');hold on
        plot(data.pat_info.timestamps, data.pat_info.HR_raw, 'go')
        %hr avg for BP
        plot(data.pat_bp.timestamps, data.pat_bp.HR_avg_for_BP, 'r+','MarkerSize', 10)
        if spotCheck
            plot(data.spot_check.timestamps,nanmean(data.pat_bp.HR_avg_for_BP)*data.spot_check.signal,'r--','LineWidth',2);hold on
            legend({'HR avg (all valid HRs)','HR raw (all raw HRs)', 'HRavg for BP','Spot Check'},'Location','NW');
        end
        ylabel('Band HR');

        all_axes = [pat_subplot, hr_subplot];
        linkaxes(all_axes, 'x');
        
    end
    
    if isfield(data,'bp') 
        
        if isfield(data.bp,'sbp') && isfield(data.bp,'dbp')
            
            if isfield(data.bp.sbp,'signal') && isfield(data.bp.dbp,'signal')
                
                if ~isempty(data.bp.sbp.signal) && ~isempty(data.bp.dbp.signal)
                    
                    for i=1:length(data.bp.sbp.timestamps)
                        
                        timeBP = data.bp.sbp.timestamps(i);
                        
                        %plot BP
                        subplot(211)
                        text(timeBP, nanmax(data.pat_bp.pat_avg_for_BP),...
                            ['BP: ',num2str(data.bp.sbp.signal(i)),'-',num2str(data.bp.dbp.signal(i))],'FontSize',18);
                        %plot HR
                        text(timeBP, nanmax(data.pat_bp.pat_avg_for_BP)-0.05,...
                            ['HR: ',num2str(round(data.pat_bp.HR_avg_for_BP(i)))],'FontSize',18);
                        %plot CI
                        text(timeBP, nanmax(data.pat_bp.pat_avg_for_BP)-0.1,...
                            ['CI: ',num2str(round(data.pat_bp.pat_CI_for_BP(i)))],'FontSize',18);
                        
                        subplot(212)
                        text(timeBP, nanmax(data.pat_bp.HR_avg_for_BP),...
                            ['BP: ',num2str(data.bp.sbp.signal(i)),'-',num2str(data.bp.dbp.signal(i))],'FontSize',18);                      
                        
                    end
                    
                end
                
            end
            
        end
        
    else
        disp('No BP info from Simband')
    end
    
    if ~isfield(data, 'pat_bp') || ~isfield(data, 'pat_info')
        disp('No pat/HR info from Simband')
    end
    
    if ~isfield(data,'spot_check')
        disp('No Spot Check info from Simband')
    end
    
end

