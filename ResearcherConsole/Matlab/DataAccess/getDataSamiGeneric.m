function outData = getDataSamiGeneric(samiCredentials, times, forceLoad, dbDir, ...
    pythonLoadFunction, deviceSchema)
% getDataSAMI Retrieves data from SAMI and saves data in .mat file in SAMI
% cache
%
% data = getDataSami(samiCredentials, times, forceLoad, dbDir, alignFlag)
%
%   INPUTS:
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%       - forceLoad: flag to override cache
%       - dbDir: database directory
%       - pythonLoadFunction: python function to call
%       - deviceSchema: device schema
%
%   OUTPUT:
%       - outData: struct containing Simband data
%
%   Usage Example:
%       samiCredentials.uid    = 'xxx'
%       samiCredentials.did    = 'xxx'
%       samiCredentials.tok    = 'xxx'
%       samiCredentials.deviceType    = 'gearFit'
%       times.startTime        = '123456789' - Unix Time
%       times.endTime          = '123499999' - Unix Time
%       data = getData(samiCredentials,times);
%       dataset_directory = fullfile('DataBases','SAMI');

if ~exist('dbDir', 'var'),
    dbDir = fullfile('.','DataBases','SAMI');
end

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

if ~exist('pythonLoadFunction', 'var'),
    pythonLoadFunction = @genericPythonFetch;
end

%
% look up the schema based on device type
%
if ~exist('deviceSchema', 'var'),
    switch (samiCredentials.deviceType),
        case 'gearFit'
            deviceSchema = @gearFitSamiV1;
        case 'fitbitFlex'
            deviceSchema = @fitbitFlexSamiV1;
        case 'withingsDevice'
            deviceSchema = @withingsSamiV1;
        otherwise
            deviceSchema = @simbaSchemaSamiV1;
    end
end

schema = deviceSchema();
%uid = samiCredentials.uid;
did = samiCredentials.did;
tok = samiCredentials.tok;
device = samiCredentials.deviceType;
startTime = times.startTime;
endTime = times.endTime;

hashString = genHashString([did,tok,device,startTime,endTime]);

filename = fullfile(dbDir, [hashString, '.mat']);
if ~(exist(filename,'file')==2) || forceLoad,
    data = pythonLoadFunction(did, tok, device, startTime, endTime);
    if ~exist(dbDir,'dir'),
        mkdir(dbDir);
    end
    save(fullfile(dbDir, hashString),'data');
    delete('MatlabTransfer.mat');
    
else
    disp('Loading from local cache.');
    load(filename);
end

dataIn = data;
outData.timeStamps = dataIn.timeStamps; % part of the adapter below
outData.samiTimeStamps = dataIn.samiTimeStamps; % part of the adapter below

%
% adapter from python output to researcher console
%
% TODO: unify this adapter logic with the same from the CSV parser
%    
namesIn = fieldnames(dataIn);
for i = 1:length(namesIn),
    nameIdx = strcmp(namesIn{i}, schema(:,1));
    assert(sum(nameIdx)<=1, 'Schema corrupted.');
    if (sum(nameIdx) == 1),
        fieldIdx = find(nameIdx);
        fieldMatlab{i} = schema{fieldIdx, 2};
        eval(['outData.',fieldMatlab{i},' = dataIn.',namesIn{i}, ';']);
    elseif (~strcmp(namesIn{1},'timeStamps')),
        disp(['SAMI field ', namesIn{i},' not found in schema.']);
        %
        % TODO: add a input flag to handle strict vs. loose typing (for
        % debug)
        %
        % outData.(namesIn{i}) = dataIn.(namesIn{i});
    end
end

end

