function raw_data = getDataSamiRaw(samiCredentials, times, forceLoad, dbDir)
% getDataSAMI Retrieves data from SAMI and saves data in .mat file in ./DataBases/SAMI
%   INPUTS:
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%   OUTPUT:
%       - data: struct containing Simband data
%   Usage Example:
%       samiCredentials.uid    = 'xxx'
%       samiCredentials.did    = 'xxx'
%       samiCredentials.tok    = 'xxx'
%       times.startTime        = '123456789' - Unix Time
%       times.endTime          = '123499999' - Unix Time
%       data = getData(samiCredentials,times);
% dataset_directory = fullfile('DataBases','SAMI');

%
% TODO: have getDataSamiRaw and getDataSami both call a common low level
% function
%

if ~exist('dbDir', 'var'),
    dbDir = fullfile('.','Databases');
end

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

hashString = genHashString(samiCredentials, times);

filename = fullfile(dbDir, [hashString, '_raw.mat']);

if ~(exist(filename,'file')==2) || forceLoad,
    
    python_command = 'python ';
    python_code = fullfile('..','DataAccessLayer','getSamiData.py');
    s1 = ' --uid '   ;
    s2 = ' --did '   ;
    s3 = ' --dtoken ';
    s4 = ' --sdate ' ;
    s5 = ' --edate ' ;
    string = [python_command python_code ...
        s1 samiCredentials.uid           ...
        s2 samiCredentials.did           ...
        s3 samiCredentials.tok           ...
        s4 times.startTime                   ...
        s5 times.endTime];
    
    system (string);
    raw_data = load('MatlabTransferRaw.mat');
    data = load('MatlabTransfer.mat');
    hashString = genHashString(samiCredentials, times);
    save(fullfile(dbDir, hashString),'data');
    save(fullfile(dbDir, [hashString,'_raw']), 'raw_data');
    delete('MatlabTransfer.mat')
    delete('MatlabTransferRaw.mat')

else
    disp('Loading from local cache.');
    load(filename);
end

%
% could have some translation layer here potentially
%

end

function hash = genHashString(samiCredentials, times)
z = java.lang.String([samiCredentials.uid,samiCredentials.did,...
    samiCredentials.tok,times.startTime,times.endTime]);
md5 = java.security.MessageDigest.getInstance('MD5');
adapter = javax.xml.bind.annotation.adapters.HexBinaryAdapter;
hString = adapter.marshal(md5.digest(z.getBytes()));
hash = char(hString);
end
