function [data, junk] = genericPythonFetch(did, dtoken, device, sdate, edate)
%
%genericPythonFetch(uid, did, dtoken, device, sdate, edate)
%
%Input: user id, device id, device token, device type, start date and end date
%
%
global RC_DATA_ACCESS_LAYER

if (isempty(RC_DATA_ACCESS_LAYER)),
    RC_DATA_ACCESS_LAYER = ['..',filesep,'DataAccessLayer'];
end

%
% There may be a lower level way to do this, but the savings are small
% compared to network traffic delays currently.
% 
s1 = ['python ',RC_DATA_ACCESS_LAYER,filesep,'getSamiGeneric.py --did '];
%s2 = ' --did ';
s3 = ' --dtoken ';
s31 = ' --device ';
s4 = ' --sdate ';
s5 = ' --edate ';
string = [s1 did s3 dtoken s31 device s4 sdate s5 edate];
disp(string)
system (string);

%
% load data into workspace
%
data = load('MatlabTransfer.mat');
junk = [];

end
