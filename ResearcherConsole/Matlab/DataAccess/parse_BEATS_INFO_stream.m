function [data] = parse_BEATS_INFO_stream(data, chan)

gaps = find(diff(data.ppg.(chan).beats.info.timestamps) > 1e-4);
dummies = gaps(diff(gaps) < 16) + 1;

if gaps(1) == 1
    dummies = [1 dummies];
end
if gaps(end) == length(data.ppg.(chan).beats.info.timestamps) - 1
    dummies = [dummies (gaps(end) + 1)];
end

if max(abs(data.ppg.(chan).beats.info.signal(dummies))) ~= 0
    error('Failed parsing beats info stream');
end

data.ppg.(chan).beats.info.timestamps(dummies) = [];
data.ppg.(chan).beats.info.signal(dummies) = [];

N = length(data.ppg.(chan).beats.info.timestamps)/16;
timestamps = reshape(data.ppg.(chan).beats.info.timestamps,16,N);
timestamps = timestamps(1,:);
signal = reshape(data.ppg.(chan).beats.info.signal,16,N); 

features.timestamps = zeros(1,N);
features.upstroke = zeros(1,N);
features.foot = zeros(1,N);
features.secpeak = zeros(1,N);
features.pripeak = zeros(1,N);
features.dicrnot = zeros(1,N);
features.spftamp = zeros(1,N);
features.dnftamp = zeros(1,N);
features.ppftamp = zeros(1,N);
features.upstgrad = zeros(1,N);
features.decaytime = zeros(1,N);
features.risetime = zeros(1,N);
features.RCtime = zeros(1,N);
features.filtamp.usamp = zeros(1,N);
features.filtamp.ftamp = zeros(1,N);
features.filtamp.ppamp = zeros(1,N);
features.filtamp.dnamp = zeros(1,N);
features.filtamp.spamp = zeros(1,N);

for i = (1:N)
    t = timestamps(i);
    features.timestamps(i) = t;
    features.upstroke(i) = t;
    features.foot(i) = t + signal(1,i);
    features.secpeak(i) = t + signal(2,i);
    features.pripeak(i) = t + signal(3,i);
    features.dicrnot(i) = t + signal(4,i);
    features.spftamp(i) = signal(5,i);
    features.dnftamp(i) = signal(6,i);
    features.ppftamp(i) = signal(7,i);
    features.upstgrad(i) = signal(8,i);
    features.decaytime(i) = signal(9,i);
    features.risetime(i) = signal(10,i);
    features.RCtime(i) = signal(11,i);
    features.filtamp.usamp(i) = signal(12,i);
    features.filtamp.ftamp(i) = signal(13,i);
    features.filtamp.spamp(i) = signal(14,i);
    features.filtamp.ppamp(i) = signal(15,i);
    features.filtamp.dnamp(i) = signal(16,i);
end

data.ppg.(chan).beats.info.features = features;


