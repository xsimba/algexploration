function outData = getDataSamiBasicHistorical(samiCredentials, times, forceLoad, dbDir, ...
    alignFlag, pythonLoadFunction, deviceSchema)
% getDataSamiBasicHistorical Retrieves data from SAMI and saves data in .mat file in SAMI
% cache
%
% data = getDataSamiBasicHistorical(samiCredentials, times, forceLoad, dbDir, alignFlag)
%
%   INPUTS:
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%       - forceLoad: flag to override cache
%       - dbDir: database directory
%       - alignFlag: flag to produce aligned data or unaligned data
%
%   OUTPUT:
%       - outData: struct containing Simband data
%   Usage Example:
%       samiCredentials.did    = 'xxx'
%       samiCredentials.tok    = 'xxx'
%       times.startTime        = '123456789' - Unix Time
%       times.endTime          = '123499999' - Unix Time
%       data = getData(samiCredentials,times);
% dataset_directory = fullfile('DataBases','SAMI');
if ~exist('dbDir', 'var'),
    dbDir = fullfile('.','DataBases','SAMI');
end

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

if ~exist('pythonLoadFunction', 'var'),
    pythonLoadFunction = @simbaPythonFetchHistorical;
end

if ~exist('deviceSchema', 'var'),
    deviceSchema = @simbaSchemaSamiV4;
end

alignFlagLogical = strcmp(alignFlag, 'aligned');

schema = deviceSchema();
%uid = samiCredentials.uid;
did = samiCredentials.did;
tok = samiCredentials.tok;
startTime = times.startTime;
endTime = times.endTime;

hashString = genHashString([did,tok,startTime,endTime]);

if (alignFlagLogical),
    filename = fullfile(dbDir, [hashString, '_src.mat']);
else
    filename = fullfile(dbDir, [hashString, '.mat']);
end
if ~(exist(filename,'file')==2) || forceLoad,
    [data, srcData] = pythonLoadFunction(did, tok, startTime, endTime);
    if ~exist(dbDir,'dir'),
        mkdir(dbDir);
    end
    save(fullfile(dbDir, hashString),'data');
    save(fullfile(dbDir, [hashString,'_src']), 'srcData');
    delete('MatlabTransferSrc.mat');
    delete('MatlabTransfer.mat');
    
else
    disp('Loading from local cache.');
    load(filename);
end

if (alignFlagLogical),
    dataIn = srcData;
    outData.timestamps = dataIn.timestamps; % part of the adapter below
else
    dataIn = data;
end

%
% adapter from python output to researcher console
%
% TODO: unify this adapter logic with the same from the CSV parser
%    
namesIn = fieldnames(dataIn);
for i = 1:length(namesIn),
    nameIdx = strcmp(namesIn{i}, schema(:,1));
    assert(sum(nameIdx)<=1, 'Schema corrupted.');
    if (sum(nameIdx) == 1),
        fieldIdx = find(nameIdx);
        fieldMatlab{i} = schema{fieldIdx, 2};
        [rows,columns] = eval(['size(dataIn.',namesIn{i},');']);
        if rows == 3,
            eval(['outData.',fieldMatlab{i},'.timestamps = dataIn.',namesIn{i},'(1,:);']);
            eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(2,:);']);
            eval(['outData.',fieldMatlab{i},'.samiTimestamps = dataIn.',namesIn{i},'(3,:);']);
            eval(['outData.',fieldMatlab{i},'.unixTimeStamps = outData.',fieldMatlab{i},'.timestamps;']);
            %eval(['outData.',fieldMatlab{i},'.startUnixTime = outData.',fieldMatlab{i},'.timestamps(1);']);
            if (alignFlagLogical),
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.timestamps(1))/1000;']);
            else
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.',fieldMatlab{i},'.timestamps(1))/1000;']);
            end
        end
        if rows == 2,
            eval(['outData.',fieldMatlab{i},'.timestamps = dataIn.',namesIn{i},'(1,:);']);
            eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(2,:);']);
            eval(['outData.',fieldMatlab{i},'.unixTimeStamps = outData.',fieldMatlab{i},'.timestamps;']);
            if (alignFlagLogical),
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.timestamps(1))/1000;']);
            else
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.',fieldMatlab{i},'.timestamps(1))/1000;']);
            end
        end
        if rows == 1,
            eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(1,:);']);
%         
%                 
        %eval(['outData.',fieldMatlab{i},' = dataIn.',namesIn{i}, ';']);
    %elseif (~strcmp(namesIn{1},'timestamps')),
        %disp(['SAMI field ', namesIn{i},' not found in schema.']);
        %
        % TODO: add a input flag to handle strict vs. loose typing (for
        % debug)
        %
        % outData.(namesIn{i}) = dataIn.(namesIn{i});
    end
end


end

