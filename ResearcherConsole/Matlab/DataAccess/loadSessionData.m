function c = loadSessionData(fname)
%
% loads a SAMI session data file with credentials and timestamps.
%

[junk,junk,credentials] = xlsread(fname, 'Devices');
[junk,junk,sessions] = xlsread(fname, 'Sessions');

c = {};

for i = 2:size(sessions,1);
    s = sessions(i,:);
    tempc.sessionID = s{1};
    tempc.time.startTime = sprintf('%.0f',s{3});
    tempc.time.endTime   = sprintf('%.0f',s{4});
    if (length(s) > 7),
        tempc.WebGuiCsv = printNanAsEmpty(s{5});
        tempc.ver0mat   = printNanAsEmpty(s{6});
        tempc.metricsFile = printNanAsEmpty(s{7});
        tempc.refFile = printNanAsEmpty(s{8});
    end
    
    deviceNick = s{2};
    if isnan(deviceNick),
        continue
    end
    deviceInd  = strmatch(deviceNick, credentials(:,1), 'exact');
    if isempty(deviceInd),
        missingNames.(deviceNick) = 1;
        tempc.cred.did = '';
        tempc.cred.uid = '';
        tempc.cred.tok = '';
    else
        
        tempc.cred.did = credentials{deviceInd,2};
        tempc.cred.uid = credentials{deviceInd,3};
        tempc.cred.tok = credentials{deviceInd,4};
        if (size(credentials,2) == 4),
            tempc.cred.deviceType = 'simband';
        else
            tempc.cred.deviceType = credentials{deviceInd,5};
        end
    end
    
    c{i-1} = tempc;
end

% 
% print warning report on missing device names, currently turned off. 
%
if exist('missingNames', 'var') && 0,
    mDevices = fieldnames(missingNames);
    for i = 1:length(mDevices),
        disp(['Sessions database, missing device name, ', mDevices{i}]);
    end
end

end

function out = printNanAsEmpty(x)

if isnan(x),
    out = '';
else
    out = x;
end

end
