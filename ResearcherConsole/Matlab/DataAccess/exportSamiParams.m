function a = exportSamiParams(param)
%
% Write a Sami parameter parameters structure to JSON
%
% see also readSamiClipboard

a = '{ ';
a = strcat(a, keymapint('startDate', param.startDate), ',');
a = strcat(a, keymapint('endDate', param.endDate), ',');
a = strcat(a, keymapstr('sdids', param.sdids), ',');
a = strcat(a, keymapstr('uids', param.uids));
a = strcat(a, '}');

end


function out = keymapstr(key, value)

out = strcat('"', key, '" : "', value, '"');

end

function out = keymapint(key, value)

out = strcat('"', key, '" : ', sprintf('%d',value));

end
