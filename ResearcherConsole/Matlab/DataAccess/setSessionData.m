function  [sc, time] = setSessionData(c, name)
%
% expects a list of sessions and a keyword
%
idx = cellfun(@(x) isequal(x.sessionID, name), c);

if (sum(idx) == 0),
    error('No session by that name listed');
end

s = c{idx};

sc = s.cred;
time = s.time;