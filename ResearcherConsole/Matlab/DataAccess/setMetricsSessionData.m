function  fname = setMetricsSessionData(c, name)
%
% expects a list of sessions and a keyword
%
idx = cellfun(@(x) isequal(x.sessionID, name), c);

if (sum(idx) == 0),
    error('No session by that name listed');
end

s = c{idx};

fname = s.metricsFile;