function [finapres_raw] = parseFinapresRaw(fileName)

    %The first 2 lines contain headers and Units information
    finapres_raw = csvread(fileName,2,0);
    
end