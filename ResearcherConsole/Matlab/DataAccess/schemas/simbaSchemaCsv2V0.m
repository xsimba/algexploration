function schema = simbaSchemaCsv2V0

schema = {...    
%
%
%
%
%
%
%
%
%
%
'ECG_LEAD',  'ecg_lead', 'Signal';
'ECG',  'ecg.signal', 'SrcSignal';
%
%
%
%
%
%
%
%
'PPG_0',  'ppg.a.signal', 'SrcSignal';
'PPG_1',  'ppg.b.signal', 'SrcSignal';
'PPG_2',  'ppg.c.signal', 'SrcSignal';
'PPG_3',  'ppg.d.signal', 'SrcSignal';
%
%
'PPG_4',  'ppg.e.signal', 'SrcSignal';
'PPG_5',  'ppg.f.signal', 'SrcSignal';
'PPG_6',  'ppg.g.signal', 'SrcSignal';
'PPG_7',  'ppg.h.signal', 'SrcSignal';
'ACCELERO_X',  'acc.x.signal', 'SrcSignal';
'ACCELERO_Y',  'acc.y.signal', 'SrcSignal';
'ACCELERO_Z',  'acc.z.signal', 'SrcSignal';
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
'BIOZ_0',  'bioz.i', 'Signal';
'BIOZ_1',  'bioz.q', 'Signal';
'GSR_0',  'gsr.phasic', 'Signal';
'GSR_1',  'gsr.tonic', 'Signal';
%
%
%
%
%
%
%
%
};
end