function schema = gearFitSchemaSamiV1

schema = {...
'heartRate',  'heartRate'; 
'stepCount',  'stepCount'; 
'state',  'state'; 
'activity',  'activity'; 
'description',  'description'; 	
    };
end