function schema = simbaSchemaSamiV2

schema = {...
'hr_ci',  'hr_ci'; 
'hr',  'band_hr'; 
'hrv',  'hrv'; 
'displayState',  'displayState'; 
'heartRateTypical',  'heartRateTypical'; 
'spot_check',  'spot_check'; 
'posture',  'posture'; 
'steps',  'steps'; 
'activityValue',  'activityValue'; 
'activityConfidence',  'activityConfidence'; 
%
'postBBQpreSmoothHr',  'band_presmooth_biosem_hr'; 
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
'ecg_lead',  'ecg_lead'; 
'ecgsignal',  'ecg.signal'; 
%
%
%
%
%
%
%
%
'ppgasignal',  'ppg.a.signal'; 
'ppgbsignal',  'ppg.b.signal'; 
'ppgcsignal',  'ppg.c.signal'; 
'ppgdsignal',  'ppg.d.signal'; 
%
%
'ppgesignal',  'ppg.e.signal'; 
'ppgfsignal',  'ppg.f.signal'; 
'ppggsignal',  'ppg.g.signal'; 
'ppghsignal',  'ppg.h.signal'; 
'accxsignal',  'acc.x.signal'; 
'accysignal',  'acc.y.signal'; 
'acczsignal',  'acc.z.signal'; 
'ppgavisualsignal',  'ppg.a.visual.signal'; 
'ppgbvisualsignal',  'ppg.b.visual.signal'; 
'ppgcvisualsignal',  'ppg.c.visual.signal'; 
'ppgdvisualsignal',  'ppg.d.visual.signal'; 
'ppgevisualsignal',  'ppg.e.visual.signal'; 
'ppgfvisualsignal',  'ppg.f.visual.signal'; 
'ppggvisualsignal',  'ppg.g.visual.signal'; 
'ppghvisualsignal',  'ppg.h.visual.signal'; 
'ecgvisualsignal',  'ecg.visual.signal'; 
%
%
%
%
%
%
%
%
'band_SBP',  'band_SBP'; 
'band_DBP',  'band_DBP'; 
'BioZ',  'bioz.signal'; 
%
'biozi',  'bioz.i'; 
'biozq',  'bioz.q'; 
'gsrtonic',  'gsr.phasic'; 
'gsrphasic',  'gsr.tonic'; 
'skinTemperature',  'skintemp'; 
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
'ecgConfidence',  'ecg.band_CIraw'; 
'ppgaband_CIraw',  'ppg.a.band_CIraw'; 
'ppgbband_CIraw',  'ppg.b.band_CIraw'; 
'ppgcband_CIraw',  'ppg.c.band_CIraw'; 
'ppgdband_CIraw',  'ppg.d.band_CIraw'; 
'ppgeband_CIraw',  'ppg.e.band_CIraw'; 
'ppgfband_CIraw',  'ppg.f.band_CIraw'; 
'ppggband_CIraw',  'ppg.g.band_CIraw'; 
'ppghband_CIraw',  'ppg.h.band_CIraw'; 
    };
end