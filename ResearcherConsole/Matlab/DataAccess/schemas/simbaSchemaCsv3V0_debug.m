function schema = simbaSchemaCsv3V0_debug

schema = {...
    %
%
'HR',  'band_hr', 'Rate';
'BB_BIOSEM_HR',  'band_hr_bb_BioSem', 'Rate';
'BIOSEM_HR_0',  'ppg.a.band_hr_Biosem', 'Rate';
'BIOSEM_HR_1',  'ppg.b.band_hr_Biosem', 'Rate';
'BIOSEM_HR_2',  'ppg.c.band_hr_Biosem', 'Rate';
'BIOSEM_HR_3',  'ppg.d.band_hr_Biosem', 'Rate';
'BIOSEM_HR_4',  'ppg.e.band_hr_Biosem', 'Rate';
'BIOSEM_HR_5',  'ppg.f.band_hr_Biosem', 'Rate';
'BIOSEM_HR_6',  'ppg.g.band_hr_Biosem', 'Rate';
'BIOSEM_HR_7',  'ppg.h.band_hr_Biosem', 'Rate';
'BIOSEM_HR_0_SIGMA',  'ppg.a.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_1_SIGMA',  'ppg.b.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_2_SIGMA',  'ppg.c.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_3_SIGMA',  'ppg.d.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_4_SIGMA',  'ppg.e.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_5_SIGMA',  'ppg.f.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_6_SIGMA',  'ppg.g.band_hr_BiosemSigma', 'Rate';
'BIOSEM_HR_7_SIGMA',  'ppg.h.band_hr_BiosemSigma', 'Rate';
'HR_0_CI',  'ppg.a.band_hr_BiosemCI', 'Rate';
'HR_1_CI',  'ppg.b.band_hr_BiosemCI', 'Rate';
'HR_2_CI',  'ppg.c.band_hr_BiosemCI', 'Rate';
'HR_3_CI',  'ppg.d.band_hr_BiosemCI', 'Rate';
'HR_4_CI',  'ppg.e.band_hr_BiosemCI', 'Rate';
'HR_5_CI',  'ppg.f.band_hr_BiosemCI', 'Rate';
'HR_6_CI',  'ppg.g.band_hr_BiosemCI', 'Rate';
'HR_7_CI',  'ppg.h.band_hr_BiosemCI', 'Rate';
%
%
%
%
'ECG_LEAD',  'ecg_lead', 'Signal';
'ECG',  'ecg.signal', 'Signal';
%
%
%
%
%
%
%
%
'PPG_0',  'ppg.a.signal', 'Signal';
'PPG_1',  'ppg.b.signal', 'Signal';
'PPG_2',  'ppg.c.signal', 'Signal';
'PPG_3',  'ppg.d.signal', 'Signal';
%
%
'PPG_4',  'ppg.e.signal', 'Signal';
'PPG_5',  'ppg.f.signal', 'Signal';
'PPG_6',  'ppg.g.signal', 'Signal';
'PPG_7',  'ppg.h.signal', 'Signal';
'ACCELERO_X',  'acc.x.signal', 'Signal';
'ACCELERO_Y',  'acc.y.signal', 'Signal';
'ACCELERO_Z',  'acc.z.signal', 'Signal';
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
%
'BP_SYSTOLIC',  'band_SBP', 'Feature';
'BP_DIASTOLIC',  'band_DBP', 'Feature';
%
%
'BIOZ_0',  'bioz.i', 'Signal';
'BIOZ_1',  'bioz.q', 'Signal';
'GSR_0',  'gsr.phasic', 'Signal';
'GSR_1',  'gsr.tonic', 'Signal';
'SKINTEMP',  'skintemp', 'Signal';
%
%
%
%
%
%
%
%
%
%
'BD_ECG',  'ecg.band_beats', 'Feature';
'BD_PPG',  'ppg.band_beats_HR', 'Feature';
'ECG_BD_CI',  'ecg.band_beats_CI', 'Feature';
'PPG_0_BD',  'ppg.a.band_beats', 'Feature';
'PPG_0_BD_CI',  'ppg.a.band_beats_CI', 'Feature';
'PPG_1_BD',  'ppg.b.band_beats', 'Feature';
'PPG_1_BD_CI',  'ppg.b.band_beats_CI', 'Feature';
'PPG_2_BD',  'ppg.c.band_beats', 'Feature';
'PPG_2_BD_CI',  'ppg.c.band_beats_CI', 'Feature';
'PPG_3_BD',  'ppg.d.band_beats', 'Feature';
'PPG_3_BD_CI',  'ppg.d.band_beats_CI', 'Feature';
'PPG_4_BD',  'ppg.e.band_beats', 'Feature';
'PPG_4_BD_CI',  'ppg.e.band_beats_CI', 'Feature';
'PPG_5_BD',  'ppg.f.band_beats', 'Feature';
'PPG_5_BD_CI',  'ppg.f.band_beats_CI', 'Feature';
'PPG_6_BD',  'ppg.g.band_beats', 'Feature';
'PPG_6_BD_CI',  'ppg.g.band_beats_CI', 'Feature';
'PPG_7_BD',  'ppg.h.band_beats', 'Feature';
'PPG_7_BD_CI',  'ppg.h.band_beats_CI', 'Feature';
'ECG_CI',  'ecg.band_CIraw', 'Feature';
'PPG_0_CI',  'ppg.a.band_CIraw', 'Feature';
'PPG_1_CI',  'ppg.b.band_CIraw', 'Feature';
'PPG_2_CI',  'ppg.c.band_CIraw', 'Feature';
'PPG_3_CI',  'ppg.d.band_CIraw', 'Feature';
'PPG_4_CI',  'ppg.e.band_CIraw', 'Feature';
'PPG_5_CI',  'ppg.f.band_CIraw', 'Feature';
'PPG_6_CI',  'ppg.g.band_CIraw', 'Feature';
'PPG_7_CI',  'ppg.h.band_CIraw', 'Feature';
};
end
