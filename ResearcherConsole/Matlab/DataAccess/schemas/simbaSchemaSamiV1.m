function schema = simbaSchemaSamiV1

schema = {...
'HRV',  'HRV'; 
'HR',  'HR'; 
'PAT',  'PAT'; 
'ECGHeartBeat',  'ECGHeartBeat'; 
'HeartBeat',  'HeartBeat'; 
'ECGRaw',  'ECGRaw'; 
'ppg_blue',  'ppg_blue'; 
'ppg_red',  'ppg_red'; 
'ppg_green_center',  'ppg_green_center'; 
'ppg_green_side',  'ppg_green_side'; 
'ECG',  'ECG'; 
'PPG_20',  'PPG_20'; 
'PPG_21',  'PPG_21'; 
'PPG_10',  'PPG_10'; 
'PPG_11',  'PPG_11'; 
'Accelerometer0',  'Accelerometer1'; 
'Accelerometer1',  'Accelerometer2'; 
'Accelerometer2',  'Accelerometer3'; 
'PPG_3_VISUAL',  'PPG_3_VISUAL'; 
'PPG_1_VISUAL',  'PPG_1_VISUAL'; 
'PPG_2_VISUAL',  'PPG_2_VISUAL'; 
'PPG_0_VISUAL',  'PPG_0_VISUAL'; 
'ECG_VISUAL',  'ECG_VISUAL'; 
'PPGFilt_10',  'PPGFilt_10'; 
'PPGFilt_11',  'PPGFilt_11'; 
'PPGFilt_20',  'PPGFilt_20'; 
'PPGFilt_21',  'PPGFilt_21'; 
'PPGPre_10',  'PPGPre_10'; 
'PPGPre_11',  'PPGPre_11'; 
'PPGPre_20',  'PPGPre_20'; 
'PPGPre_21',  'PPGPre_21'; 
'ppg_green_side_visual',  'ppg_green_side_visual'; 
'ppg_green_center_visual',  'ppg_green_center_visual'; 
'ppg_blue_visual',  'ppg_blue_visual'; 
'ppg_red_visual',  'ppg_red_visual'; 
'BP1',  'BP1'; 
'BP2',  'BP2'; 
'BioZ',  'BioZ'; 
'BioZRaw',  'BioZRaw'; 
'ppg_ir1_visual',  'ppg_ir1_visual'; 
'ppg_ir1',  'ppg_ir1'; 
'ppg_ir2_visual',  'ppg_ir2_visual'; 
'ppg_ir2',  'ppg_ir2'; 
'PPG_4_VISUAL',  'PPG_4_VISUAL'; 
%
%
%
%
    };
end