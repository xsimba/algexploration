function schema = fitbitFlexSchemaSamiV1

schema = {...
'stepCount',  'stepCount'; 
'calories',  'calories'; 
'asleepTime',  'asleepTime'; 
'timeInBed',  'timeInBed'; 
    };
end