function schema = withingsSamiV1

schema = {...
    'weight',  'weight';
    'groupId',  'groupId';
    'category',  'category';
    'attrib',  'attrib';
    'heartRate',  'heartRate';
    'fatFreeMass',  'fatFreeMass';
    'fatRatio',  'fatRatio';
    'fat MassWeight',  'fat MassWeight';
    'bloodOxygenLevel',  'bloodOxygenLevel';
    'steps',  'steps';
    'distance',  'distance';
    'calories',  'calories';
    'elevation',  'elevation';
    'softActivityDuration',  'softActivityDuration';
    'moderateActivityDuration',  'moderateActivityDuration';
    'intenseActivityDuration',  'intenseActivityDuration';
    'SleepStartDate',  'SleepStartDate';
    'state',  'state';
    'SleepEndDate',  'SleepEndDate';
    'sleep',  'sleep';    
    };
end