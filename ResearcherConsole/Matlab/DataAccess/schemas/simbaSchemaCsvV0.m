function schema = simbaSchemaCsvV0

schema = {...
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    'ECG:0',  'ecg.signal', 'SrcSignal';
    'PPG_2:0',  'ppg.c.signal', 'SrcSignal';
    'PPG_2:1',  'ppg.d.signal', 'SrcSignal';
    'PPG_1:0',  'ppg.a.signal', 'SrcSignal';
    'PPG_1:1',  'ppg.b.signal', 'SrcSignal';
    'Accelerometer:0',  'acc.x.signal', 'SrcSignal';
    'Accelerometer:1',  'acc.y.signal', 'SrcSignal';
    'Accelerometer:2',  'acc.z.signal', 'SrcSignal';
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    'BioZ:0',  'bioz.signal', 'SrcSignal';
    %
    %
    %
    %
    %
    %
    %
    'PPG_3:0',  'ppg.e.signal', 'SrcSignal';
    'PPG_3:1',  'ppg.f.signal', 'SrcSignal';
    %
    };
end