function schema = vcSchemaSamiV1

schema = {...
    'ts', 'ts'; 
    'timestampMicro', 't';
    'ecg', 'ecg';
    'cts', 'cts';
    };
end