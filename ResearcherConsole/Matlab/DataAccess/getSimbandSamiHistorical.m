function data = getSimbandSamiHistorical(samiCredentials, times, version, forceLoad)
% getSimbandSamiHistorical Retrieves data from SAMI and saves data in .mat file in ./DataBases/SAMI
%   INPUTS:
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%   OUTPUT:
%       - data: struct containing Simband data
%   Usage Example:
%       samiCredentials.uid    = 'xxx'
%       samiCredentials.did    = 'xxx'
%       samiCredentials.tok    = 'xxx'
%       times.startTime        = '123456789' - Unix Time
%       times.endTime          = '123499999' - Unix Time
%       data = getData(samiCredentials,times);
% dataset_directory = fullfile('DataBases','SAMI');

%
% TODO: have getDataSamiRaw and getDataSami both call a common low level
% function
%
global RC_CONSOLE_DATABASE

if isempty(RC_CONSOLE_DATABASE),
    RC_CONSOLE_DATABASE = fullfile('.','DataBases');
end

dbDir = fullfile(RC_CONSOLE_DATABASE,'SAMI');

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

if ~exist('version', 'var'),
    version = 'v4';
end

pythonLoadFunction = @simbaPythonFetchHistorical;

switch(version)
    case 'v1'
        deviceSchema = @simbaSchemaSamiV1;
    case 'v0'
        deviceSchema = @simbaSchemaSamiV0;
    case 'v2'
        deviceSchema = @simbaSchemaSamiV2;
    case 'v3'
        deviceSchema = @simbaSchemaSamiV3;
    case 'v4'
        deviceSchema = @simbaSchemaSamiV4;
    case 'v5'
        deviceSchema = @simbaSchemaSamiV5;
    otherwise
        error('device schema not recognized');
end
data = getDataSamiBasic (samiCredentials, times, forceLoad, dbDir, ...
    'not aligned', pythonLoadFunction, deviceSchema);

end
