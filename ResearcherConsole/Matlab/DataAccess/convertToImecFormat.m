
% RC_setup;
% 
% sessionList = {'UCSFP7', 'UCSFP10'};


function convertToImecFormat(c, sessionList)

for i = 1:length(sessionList),
    load(sessionList{i});

    
    
    data.spot_check(1,:) = data.spot_check.timestamps;
    data.spot_check(2,:) = data.spot_check.signal;

    data.spot_check.timestamps = data.spot_check.timestamps - data.spot_check.timestamps(1);

    save(sessionList{i}, 'data');
    
%     AddTracks(c, sessionList{i});
end

