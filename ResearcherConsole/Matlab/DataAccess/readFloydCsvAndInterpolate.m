% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : readFloydCsvAndInterpolate.m
%  Content    : Import data from .csv taking into account data loss 
%  Package    : SIMBA.Validation
%  Created by : F. Beutel (fabian.beutel@imec-nl.nl) & A.Young & A.M.Tautan
%  Date       : 07.11.2014
%  Modification and Version History:
%  | Developer | Version |    Date    |     Changes      |
%  |  fbeutel  |   1.0   | 
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl)
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------


function data = readFloydCsvAndInterpolate(csvFileName)

% read file like csv:it's safer for getting the num of columns
csvData = csvread(csvFileName,2);
numFields = size(csvData,2);
clear csvData ;

% create the string format for the header and the data
headerFrmt = [];
formatStr = [];
for idx = 1:numFields-1
    headerFrmt = [headerFrmt, '%[^,],'];
    formatStr = [formatStr, '%f'];
end
headerFrmt = [headerFrmt, '%[^,\r\n]'];
formatStr = [formatStr,'%[^\n\r]'];

% open the file
fid = fopen(csvFileName, 'r');
% scan the first line ( header ) and get correspondent names
header = textscan(fid, headerFrmt, 1);
header = cellfun(@(c) c{1},header,'UniformOutput',0);

% scan the file and get data - Empty cells are NaN
dataArray = textscan(fid, formatStr, 'Delimiter', ',', ...
    'EmptyValue' ,NaN,'HeaderLines' ,1, 'ReturnOnError', false);

% close file
fclose(fid);

% read the schema (function below in this .m-file)
schema = simbaSchemaCsv3V0();

%find the concurrent start and end indices of the data in the .csv
start_indices=[];
stop_indices=[];
for hIdx = 2:numel(header)
    
    % looks for the corresponding field in the schema-rosetta
    fieldIndex = find(strcmpi(schema(:,1),header{hIdx}));
    
    try
        type = schema{fieldIndex,3};
    catch err
        keyboard()
    end
    if strcmp(type,'SrcSignal') %|| strcmp(type,'SrcSignal32') || strcmp(type,'SrcSignal01')
        thisSignal = dataArray{hIdx};
        
        % conversion from cell to double array
        if iscell(thisSignal)
            thisSignalArray = nan(1,numel(thisSignal));
            for idx = 1:numel(thisSignal)
                if ~isempty(thisSignal(idx))
                    thisSignalArray(idx) = str2double(thisSignal(idx)) ;
                end
            end
            thisSignal = thisSignalArray';
        end
        
        % check all numerical value that are not NaNs
        start_indices=[start_indices, find(~isnan(thisSignal),1,'first')];
        stop_indices=[stop_indices, find(~isnan(thisSignal),1,'last')];
    end
    
    start_index = max(start_indices);
    stop_index = min(stop_indices);
end
%truncate the data array using the found start and end indices
for i=1:numel(dataArray)
    current_cell = dataArray{i};
    current_cell = current_cell(start_index : stop_index);
    dataArray{i} = current_cell;
end

% parse the remaining columns and populat the structure
% each field has now a field signal and timestamps
% for BEAT DETECTORS those two fields are the same
% since the output is a timestamp

% gets all the timestamps of the CSV
allTimestamps = dataArray{1};
for hIdx = 2:numel(header)
    
    try
        
        thisSignal = dataArray{hIdx};
        
        % conversion from cell to double array
        if iscell(thisSignal)
             thisSignalArray = nan(1,numel(thisSignal));
            for idx = 1:numel(thisSignal)
                if ~isempty(thisSignal(idx))
                   thisSignalArray(idx) = str2double(thisSignal(idx)) ;
                end
            end
            thisSignal = thisSignalArray';
        end

            % check all numerical value that are not NaNs
            validIndices = ~isnan(thisSignal);
            correspondentTimestamps = allTimestamps(validIndices);
            thisSignal = thisSignal(validIndices);
                     
            % looks for the corresponding field in the schema-rosetta
            fieldIndex = find(strcmpi(schema(:,1),header{hIdx}));
            
            
            type = schema{fieldIndex,3};
            if strcmp(type,'SrcSignal')
                [thisSignal, correspondentTimestamps] = interpolate_gaps(thisSignal, correspondentTimestamps, 0.008, 128);
                thisSignal=thisSignal';
                correspondentTimestamps=correspondentTimestamps';
            elseif strcmp(type,'SrcSignal32')
                [thisSignal, correspondentTimestamps] = interpolate_gaps(thisSignal, correspondentTimestamps, 0.033, 32);
                thisSignal=thisSignal';
                correspondentTimestamps=correspondentTimestamps';
            elseif strcmp(type,'SrcSignal01')
                [thisSignal, correspondentTimestamps] = interpolate_gaps(thisSignal, correspondentTimestamps, 11, 0.1);
                thisSignal=thisSignal';
                correspondentTimestamps=correspondentTimestamps';
            end
            
            eval(['data.' schema{fieldIndex,2} '.signal=thisSignal;']);
            eval(['data.' schema{fieldIndex,2} '.timestamps=correspondentTimestamps;']);
             
    catch err
        
        disp([header{hIdx} ':' char(err.message)]);
        keyboard()
        
    end
end





end


function [thisSignal, correspondentTimestamps] = interpolate_gaps(thisSignal, correspondentTimestamps, threshold, fs)
deltaTime = diff(correspondentTimestamps);
gap_detector =  deltaTime > threshold; 
Nsamp = round(deltaTime(gap_detector)*fs);
new_thisSignal = [];
new_Timestamps = [];
startIdx = 1;
if length(Nsamp)>0
    gap_idx = find(gap_detector);
    for idx =1:length(Nsamp)
        gap_starttime = correspondentTimestamps(gap_idx(idx));
        gap_endtime = correspondentTimestamps(gap_idx(idx)+1);
        gap_startvalue = thisSignal(gap_idx(idx));
        gap_endvalue = thisSignal(gap_idx(idx)+1);
        
        inter_signals = gap_startvalue + (1:Nsamp(idx)-1)*(gap_endvalue-gap_startvalue)/Nsamp(idx);
        new_thisSignal = [new_thisSignal thisSignal(startIdx:gap_idx(idx))' inter_signals]; %#ok<AGROW>
        
        inter_timestamps = gap_starttime + (1:Nsamp(idx)-1)*(gap_endtime-gap_starttime)/Nsamp(idx);
        new_Timestamps = [new_Timestamps correspondentTimestamps(startIdx:gap_idx(idx))' inter_timestamps]; %#ok<AGROW>
        
        startIdx = gap_idx(idx)+1;
    end
end
thisSignal = [new_thisSignal thisSignal(startIdx:end)']';
correspondentTimestamps = [new_Timestamps correspondentTimestamps(startIdx:end)']';



end
        

function schema = simbaSchemaCsv3V0()

schema = {...
    %
    'HR',  'band_hr', 'Rate';
    'BB_BIOSEM_HR',  'band_hr_bb_BioSem', 'Rate';
    'BIOSEM_HR_0',  'ppg.a.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_1',  'ppg.b.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_2',  'ppg.c.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_3',  'ppg.d.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_4',  'ppg.e.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_5',  'ppg.f.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_6',  'ppg.g.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_7',  'ppg.h.band_hr_Biosem', 'Rate';
    'BIOSEM_HR_0_SIGMA',  'ppg.a.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_1_SIGMA',  'ppg.b.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_2_SIGMA',  'ppg.c.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_3_SIGMA',  'ppg.d.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_4_SIGMA',  'ppg.e.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_5_SIGMA',  'ppg.f.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_6_SIGMA',  'ppg.g.band_hr_BiosemSigma', 'Rate';
    'BIOSEM_HR_7_SIGMA',  'ppg.h.band_hr_BiosemSigma', 'Rate';
    'HR_0_CI',  'ppg.a.band_hr_BiosemCI', 'Rate';
    'HR_1_CI',  'ppg.b.band_hr_BiosemCI', 'Rate';
    'HR_2_CI',  'ppg.c.band_hr_BiosemCI', 'Rate';
    'HR_3_CI',  'ppg.d.band_hr_BiosemCI', 'Rate';
    'HR_4_CI',  'ppg.e.band_hr_BiosemCI', 'Rate';
    'HR_5_CI',  'ppg.f.band_hr_BiosemCI', 'Rate';
    'HR_6_CI',  'ppg.g.band_hr_BiosemCI', 'Rate';
    'HR_7_CI',  'ppg.h.band_hr_BiosemCI', 'Rate';
    %
    %
    %
    %
    'ECG_LEAD',  'ecg_lead', 'Feature';
    'ECG',  'ecg', 'SrcSignal';
    %
    %
    %
    'SPOT_CHECK',   'spot_check',   'Feature';
    'PAT',  'band_pat', 'Feature';
    %
    'PPG_SELECTION',  'ppg_selection', 'Feature';
    %
    %
    %
    'PPG_0',  'ppg.a', 'SrcSignal';
    'PPG_1',  'ppg.b', 'SrcSignal';
    'PPG_2',  'ppg.c', 'SrcSignal';
    'PPG_3',  'ppg.d', 'SrcSignal';
    %
    %
    'PPG_4',  'ppg.e', 'SrcSignal';
    'PPG_5',  'ppg.f', 'SrcSignal';
    'PPG_6',  'ppg.g', 'SrcSignal';
    'PPG_7',  'ppg.h', 'SrcSignal';
    'ACCELERO_X',  'acc.x', 'SrcSignal';
    'ACCELERO_Y',  'acc.y', 'SrcSignal';
    'ACCELERO_Z',  'acc.z', 'SrcSignal';
    %
    %
    %
    %
    %
    %
    %
    %
    %
    %
    'POSTURE',  'posture', 'Feature';
    %
    %
    %
    %
    %
    %
    'BP_SYSTOLIC',  'band_SBP', 'Feature';
    'BP_DIASTOLIC',  'band_DBP', 'Feature';
    %
    %
    'BIOZ_0',  'bioz.i', 'SrcSignal';
    'BIOZ_1',  'bioz.q', 'SrcSignal';
    'GSR_0',  'gsr.phasic', 'SrcSignal32';
    'GSR_1',  'gsr.tonic', 'SrcSignal32';
    'SKINTEMP',  'skintemp', 'SrcSignal01';
    %
    %
    %
    %
    %
    'BAND_ON_WRIST', 'band_on_wrist', 'Signal';
    'ACTIVITY',  'activity', 'Signal';
    %
    %
    %
    %
    'BD_ECG',  'ecg.band_beats', 'Feature';
    'BD_PPG',  'ppg.band_beats_HR', 'Feature';
    'ECG_BD_CI',  'ecg.band_beats_CI', 'Feature';
    'PPG_0_BD',  'ppg.a.band_beats', 'Feature';
    'PPG_0_BD_CI',  'ppg.a.band_beats_CI', 'Feature';
    'PPG_1_BD',  'ppg.b.band_beats', 'Feature';
    'PPG_1_BD_CI',  'ppg.b.band_beats_CI', 'Feature';
    'PPG_2_BD',  'ppg.c.band_beats', 'Feature';
    'PPG_2_BD_CI',  'ppg.c.band_beats_CI', 'Feature';
    'PPG_3_BD',  'ppg.d.band_beats', 'Feature';
    'PPG_3_BD_CI',  'ppg.d.band_beats_CI', 'Feature';
    'PPG_4_BD',  'ppg.e.band_beats', 'Feature';
    'PPG_4_BD_CI',  'ppg.e.band_beats_CI', 'Feature';
    'PPG_5_BD',  'ppg.f.band_beats', 'Feature';
    'PPG_5_BD_CI',  'ppg.f.band_beats_CI', 'Feature';
    'PPG_6_BD',  'ppg.g.band_beats', 'Feature';
    'PPG_6_BD_CI',  'ppg.g.band_beats_CI', 'Feature';
    'PPG_7_BD',  'ppg.h.band_beats', 'Feature';
    'PPG_7_BD_CI',  'ppg.h.band_beats_CI', 'Feature';
    'ECG_CI',  'ecg.band_CIraw', 'Feature';
    'PPG_0_CI',  'ppg.a.band_CIraw', 'Feature';
    'PPG_1_CI',  'ppg.b.band_CIraw', 'Feature';
    'PPG_2_CI',  'ppg.c.band_CIraw', 'Feature';
    'PPG_3_CI',  'ppg.d.band_CIraw', 'Feature';
    'PPG_4_CI',  'ppg.e.band_CIraw', 'Feature';
    'PPG_5_CI',  'ppg.f.band_CIraw', 'Feature';
    'PPG_6_CI',  'ppg.g.band_CIraw', 'Feature';
    'PPG_7_CI',  'ppg.h.band_CIraw', 'Feature';
    };
end

