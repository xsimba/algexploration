function [data, params] = getTrialsSnapshot(forceLoad, ...
    alignFlag, deviceSchema, Jsonstring)
% getTrialsSnapshot imports data into the Matlab workspace 
% after getting the SAMI credentials from the SAMI Trial-admin tool.
%
%   INPUTS [defaults]:
%       - forceLoad[0]: force loading from SAMI rather than local cache
%       - alignFlag['aligned']: 'aligned' for time-aligned data, 'debug' 
%       - deviceSchema['simbaSchemaSamiV5']: Matlab data structure Schema
%       - Jsonstring [read from clipboard]: JSON credentials for SAMI input
%   OUTPUT:
%       - data: struct containing Simband data
% 
%   Usage Example:
%       data = getTrialsSnapshot;

global RC_CONSOLE_DATABASE

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

if ~exist('alignFlag', 'var'),
    alignFlag = 'aligned';
end

if ~exist('deviceSchema', 'var'),
    deviceSchema = @simbaSchemaSamiV5;
end

if (~exist('dbDir', 'var') && ~exist('RC_CONSOLE_DATABASE', 'var')),
    dbDir = fullfile('.','DataBases','SAMI');
elseif exist('RC_CONSOLE_DATABASE', 'var'),
    dbDir = RC_CONSOLE_DATABASE;
end

if ~exist('pythonLoadFunction', 'var'),
    pythonLoadFunction = @simbaTrialsFetch;
end

%
% construct parameters
%
if ~exist('Jsonstring'),
    params = readSamiClipboard;
else
    temp = parse_json(JsonString);
    params = temp{1};
end

% pick out only the first sdid from the string and assume that it is
% a simband device
params.sdid = strtok(params.sdids, ',');

data = readSamiTrialsData(params, forceLoad, ...
    alignFlag, deviceSchema, dbDir, pythonLoadFunction);

end