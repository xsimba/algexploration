function  fnameUnix = convertSSICpathToUnix(fnameWindows)
%
% expects a list of sessions and a keyword
%
fname = strrep(fnameWindows, '\\105.140.2.7\', '/srv/samba/');
fname = strrep(fname, '\', '/');

fnameUnix = fname;