function [data, srcData] = simbaPythonFetch(did, dtoken, sdate, edate)
%
% simbaPythonFetch(did, dtoken, sdate, edate)
%
%Input: device id, device token, start date and end date
%
%
global RC_DATA_ACCESS_LAYER

if (isempty(RC_DATA_ACCESS_LAYER)),
    RC_DATA_ACCESS_LAYER = ['..',filesep,'DataAccessLayer'];
end

%
% There may be a lower level way to do this, but the savings are small
% compared to network traffic delays currently.
% 
s1 = ['python ',RC_DATA_ACCESS_LAYER,filesep,'getSamiSimband.py --did '];
%s2 = ' --did ';
s3 = ' --dtoken ';
s4 = ' --sdate ';
s5 = ' --edate ';
string = [s1 did s3 dtoken s4 sdate s5 edate];
system (string);

%
% load data into workspace
%
data = load('MatlabTransfer.mat');
srcData = load('MatlabTransferSrc.mat');

end
