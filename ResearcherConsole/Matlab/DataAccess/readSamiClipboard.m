function params = readSamiClipboard(JsonString)
%
% reads a JSON from the clipboard originating from SAMI Trial or Portal
% tools and places the output into a params structure.  With an
% optional argument, it parses the argument string.
%
% see also exportSamiParams
%
if (nargin == 0)
    JsonString = strtrim(clipboard('paste'));
end

% basic validation
if (length(JsonString) == 0 || JsonString(end) ~= '}')
    error('Clipboard does not seem to contain a JSON from SAMI');
end

temp = parse_json(JsonString);
params = temp{1};

