function outData = readSamiTrialsData(params, forceLoad, ...
    alignFlag, deviceSchema, dbDir, pythonLoadFunction)
% readSamiTrialsData retrieves data from a SAMI trial and loads into the
% workspace.
%
%
% function data = readSamiTrialsData(params, forceLoad, ...
%    alignFlag, deviceSchema)
%
%   INPUTS [defaults]:
%       - params: structure containing SAMI access parameters
%       - forceLoad[0]: force loading from SAMI rather than local cache
%       - alignFlag['aligned']: 'aligned' for time-aligned data, 'debug'
%       - deviceSchema['simbaSchemaSamiV5']: Matlab data structure Schema
%   OUTPUT:
%       - data: struct containing Simband data
%
%   Usage Example:
%       params = readSamiClipboard;
%       data = readSamiTrialsData(params);

global RC_CONSOLE_DATABASE

if ~exist('params', 'var'),
    error('getSamiTrials need input SAMI credentials');
end

if (~exist('dbDir', 'var') && ~exist('RC_CONSOLE_DATABASE', 'var')),
    dbDir = fullfile('.','DataBases','SAMI');
elseif exist('RC_CONSOLE_DATABASE', 'var'),
    dbDir = RC_CONSOLE_DATABASE;
end

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

if ~exist('deviceSchema', 'var'),
    deviceSchema = @simbaSchemaSamiV5;
end

if ~exist('alignFlag', 'var'),
    alignFlag = 1;
end

if ~exist('pythonLoadFunction', 'var'),
    pythonLoadFunction = @simbaTrialsFetch;
end

alignFlagLogical = strcmp(alignFlag, 'aligned');

schema = deviceSchema();

% pick out only the first sdid from the string and assume that it is
% a simband device
if ~isfield(params, 'sdid'),
    params.sdid = strtok(params.sdids, ',');
end

did = params.sdid;
startDate = sprintf('%12.0f',params.startDate);
endDate = sprintf('%12.0f',params.endDate);
%trialId = params.trialId;

hashString = genHashString([did,startDate,endDate]);

if (alignFlagLogical),
    filename = fullfile(dbDir, [hashString, '_src.mat']);
else
    filename = fullfile(dbDir, [hashString, '.mat']);
end
if ~(exist(filename,'file')==2) || forceLoad,
    [data, srcData] = pythonLoadFunction(params);
    if ~exist(dbDir,'dir'),
        mkdir(dbDir);
    end
    save(fullfile(dbDir, hashString),'data');
    save(fullfile(dbDir, [hashString,'_src']), 'srcData');
    delete('MatlabTransferSrc.mat');
    delete('MatlabTransfer.mat');
    
else
    disp('Loading from local cache.');
    load(filename);
end

if (alignFlagLogical),
    dataIn = srcData;
    outData.timestamps = dataIn.timestamps; % part of the adapter below
    outData.unixTimeStamps = outData.timestamps;
    outData.timestamps = (outData.timestamps-outData.timestamps(1))/1000;
else
    dataIn = data;
end

%
% adapter from python output to researcher console
%
% TODO: unify this adapter logic with the same from the CSV parser
%
namesIn = fieldnames(dataIn);
for i = 1:length(namesIn),
    nameIdx = strcmp(namesIn{i}, schema(:,1));
    assert(sum(nameIdx)<=1, 'Schema corrupted.');
    if (sum(nameIdx) == 1),
        fieldIdx = find(nameIdx);
        fieldMatlab{i} = schema{fieldIdx, 2};
        [rows,columns] = eval(['size(dataIn.',namesIn{i},');']);
        if rows == 3,
            eval(['outData.',fieldMatlab{i},'.timestamps = dataIn.',namesIn{i},'(1,:);']);
            eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(2,:);']);
            eval(['outData.',fieldMatlab{i},'.samiTimestamps = dataIn.',namesIn{i},'(3,:);']);
            eval(['outData.',fieldMatlab{i},'.unixTimeStamps = outData.',fieldMatlab{i},'.timestamps;']);
            %eval(['outData.',fieldMatlab{i},'.startUnixTime = outData.',fieldMatlab{i},'.timestamps(1);']);
            if (alignFlagLogical),
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.timestamps(1))/1000;']);
            else
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.',fieldMatlab{i},'.timestamps(1))/1000;']);
            end
        end
        if rows == 2,
            eval(['outData.',fieldMatlab{i},'.timestamps = dataIn.',namesIn{i},'(1,:);']);
            eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(2,:);']);
            eval(['outData.',fieldMatlab{i},'.unixTimeStamps = outData.',fieldMatlab{i},'.timestamps;']);
            if (alignFlagLogical),
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.timestamps(1))/1000;']);
            else
                eval(['outData.',fieldMatlab{i},'.timestamps = (outData.',fieldMatlab{i},'.timestamps-outData.',fieldMatlab{i},'.timestamps(1))/1000;']);
            end
        end
        if rows == 1,
            eval(['outData.',fieldMatlab{i},'.signal = dataIn.',namesIn{i},'(1,:);']);
            %
            %
            %eval(['outData.',fieldMatlab{i},' = dataIn.',namesIn{i}, ';']);
            %elseif (~strcmp(namesIn{1},'timestamps')),
            %disp(['SAMI field ', namesIn{i},' not found in schema.']);
            %
            % TODO: add a input flag to handle strict vs. loose typing (for
            % debug)
            %
            % outData.(namesIn{i}) = dataIn.(namesIn{i});
        end
    end    
end

outData.samiParams = params;

end
