function [data] = parsePATstreamV1(data)
%Expected Sequence: PAT, PATsd, HR, HRsd, PAT_CI, (PAT raw if present)))
%Note that the blocks are not always complete, but it is assumed that all present blocks start with a PAT!

pat_stream_timestamps = data.band_pat.timestamps;
pat_stream = data.band_pat.signal;

%define new fields in data array
data.band_pat.timestamps = [];
data.band_pat.pat_avg = [];
data.band_pat.pat_std = [];
data.band_pat.HR_avg = [];
data.band_pat.HR_std = [];
data.band_pat.pat_CI = [];
data.band_pat.pat_raw = [];

%find the locations of data blocks that belong together
block_spacing = 0.05; %depends on HR but it is assumed that entire block is written within 50ms --> block spacing is > 50ms
block_locations =  [1, find(diff(pat_stream_timestamps) > block_spacing)];
block_elements = []; %elements in the block (probably incomplete!)
block_timestamps = []; %timestamps for each element in the block as coming from the csv (--> important in order to identify their sequence)
block_timestamp = []; %single timestamp for entire quitet (eventually they all correspond/refer to a single PAT timestamp)
time_spacing = 0.000016; %time spacing between elements in a single block is ~0.000016 (exact: 1.52590219002491e-05)

for i = 1:numel(block_locations)
    
    if i == 1 %for first block
        block_elements = pat_stream(block_locations(i) : block_locations(i+1));
        block_timestamps = pat_stream_timestamps(block_locations(i) : block_locations(i+1));
        block_timestamp = pat_stream_timestamps(block_locations(i)); %i.e. 'block_timestamps(1)'
    elseif i == numel(block_locations) %for the last block
        block_elements = pat_stream(block_locations(i)+1 : numel(pat_stream)); %+1 outside parentheses due to application of diff function
        block_timestamps = pat_stream_timestamps(block_locations(i)+1 : numel(pat_stream_timestamps));
        block_timestamp = pat_stream_timestamps(block_locations(i)+1);
    else %for any other block
        block_elements = pat_stream(block_locations(i)+1 : block_locations(i+1)); %+1 outside parentheses due to application of diff function
        block_timestamps = pat_stream_timestamps(block_locations(i)+1 : block_locations(i+1));
        block_timestamp = pat_stream_timestamps(block_locations(i)+1);
    end
    
    %timestamp and PAT are assumed to be always available when block is detected
    data.band_pat.timestamps(i) = block_timestamp;
    data.band_pat.pat_avg(i) = block_elements(1);
    %assign NaN's to the other feature vectors at current block location (they either remain in case of missing data or are replaced by values)
    data.band_pat.pat_std(i) = NaN;
    data.band_pat.HR_avg(i) = NaN;
    data.band_pat.HR_std(i) = NaN;
    data.band_pat.pat_CI(i) = NaN;
    data.band_pat.pat_raw(i) = NaN;
    
    %check for completeness of block and assigna vals to vars
    for j = 1:numel(block_elements)
        if (j == 2) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing)
            data.band_pat.pat_std(i) = block_elements(2);
        end
        
        if (j == 3) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 2*time_spacing)
            data.band_pat.HR_avg(i) = block_elements(3);
        end
        
        if (j == 4) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 3*time_spacing)
            data.band_pat.HR_std(i) = block_elements(4);
        end
        
        if (j == 5) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 4*time_spacing)
            data.band_pat.pat_CI(i) =  block_elements(5);
        end
        
        if numel(block_elements) == 6 %only if raw PAT is outputted
            if (j == 6) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 5*time_spacing)
                data.band_pat.pat_raw(i) = block_elements(6);
            end
        end
    end
    
end

end