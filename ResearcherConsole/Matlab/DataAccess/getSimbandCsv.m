function data = getSimbandCsv(filename, version, startTimeStamp, unixTime)
%
% function data = getSimbandCsv(filename, version, startTimeStamp)
%
% getDataCsv reads in Csv data into the Research Console format, as
% specified in the Rosetta.xlsx spreadsheet.
%
% A. Khalak, 8/13/2014
%

if (~exist('startTimeStamp', 'var')),
    startTimeStamp = 0;
end

if (~exist('version', 'var')),
    version = '4v2';
end

if (~exist('unixTime','var')),
    unixTime = 'ON';
end

switch lower(version)
    case ('v1')
        schema = simbaSchemaCsvV1;
    case ('2v0')
        schema = simbaSchemaCsv2V0;
    case ('v0')
        schema = simbaSchemaCsvV0;
    case ('3v0')
        schema = simbaSchemaCsv3V0;
    case ('3v0-debug')
        schema = simbaSchemaCsv3V0_debug;
    case ('3v1')
        schema = simbaSchemaCsv3V1;
    case ('4v2')
        schema = simbaSchemaCsv4V2;
    case ('3v1-debug')
        schema = simbaSchemaCsv3V1_debug;
    case ('4v2-debug')
        schema = simbaSchemaCsv4V2_debug;
    otherwise
        error ('version not recognized');
end


%
% assert that file exists
%
fid = fopen(filename, 'r');
assert(fid > 0, 'Cannot read file %s ', filename)

%
% Steps
% 1) Look at the header, determine # fields, indentify in schema, etc.
% 2) Slurp in the data
% 3) Determine the timeGrid for the SrcSignal fields
% 4) Assign the values of the SrcSignal fields as 1xN vectors, and
%    2xM_i for the rest, where N is the timeGrid length, and 2xM_i
%    corresponds to the timestamp and
%

header_line = fgetl(fid);

% Count number of columns
ncols = sum(header_line == ',') + 1;
column_names = textscan( header_line, '%s', 'delimiter', ',' );
column_names = column_names{1};
assert(numel(column_names) == ncols);

% figure out column matlab name and type from schema
formatStr = '%f';
fieldMatlab{1} = 'timestamps';
fieldType{1} = 'time';
for i = 2:ncols,
    nameIdx = strcmp(column_names{i}, schema(:,1));
    assert(sum(nameIdx)<=1, 'Schema corrupted.');
    if (sum(nameIdx) == 1),
        fieldIdx = find(nameIdx);
        fieldMatlab{i} = schema{fieldIdx, 2};
        fieldType{i}   = schema{fieldIdx, 3};
    end
    formatStr = [formatStr,'%f'];
end
formatStr = [formatStr,'%[^\n\r]'];

% slurp in the data
frewind(fid);
dataArray = textscan(fid, formatStr, 'Delimiter', ',', ...
    'EmptyValue' ,Inf,'HeaderLines' ,1, 'ReturnOnError', false);
frewind(fid);
fclose(fid);

% determine the timeGrid for the SrcSignal fields
timeAlignedCheck = strcmp('SrcSignal', fieldType);
timeAlignedCols = find(timeAlignedCheck);
timeAlignedCheck(1) = 1; % assign time as time-aligned for next step
nonTimeAlignedCols = find(~timeAlignedCheck);
checkUnixTime = strcmp(unixTime,'ON');
% go through all of the timeAlignedColumns and only take data when
% all fields are present
if length(timeAlignedCols > 0),
    idx = timeAlignedCols(1);
    timeGridIndx = ~isinf(dataArray{idx});
    for j = 2:length(timeAlignedCols),
        jdx = timeAlignedCols(j);
        timeGridIndx = timeGridIndx & ~isinf(dataArray{jdx});
    end
    data.(fieldMatlab{1}) = startTimeStamp + dataArray{1}(timeGridIndx)';

    % place the time aligned columns into the data output
    %
    % since the v0 (imec) schema has multiple levels for each
    % entry, we need to use the matlab 'eval' command.
    %
    for i = 1:length(timeAlignedCols),
        idx = timeAlignedCols(i);
        if (~isempty(fieldMatlab{idx})),
            if (strcmp(version,'4v2') || strcmp(version, '4v2-debug') || strcmp(version, '3v1') || strcmp(version, '3v1-debug')),
                evalstring = ['data.',fieldMatlab{idx},'.signal = dataArray{idx}(timeGridIndx)''; '];
                eval (evalstring);
            else
                evalstring = ['data.',fieldMatlab{idx},' = dataArray{idx}(timeGridIndx)'';'];
                eval(evalstring);
            end
        end
    end
    if checkUnixTime == 1,
        data.startUnixTime = data. (fieldMatlab{1})(1);
        data. (fieldMatlab{1}) = data. (fieldMatlab{1})...
              - data. (fieldMatlab{1})(1);
    end
end

% checkUnixTime = strcmp(unixTime,'ON');
% if checkUnixTime == 1,
%     data.startUnixTime = data. (fieldMatlab{1})(1);
%     data. (fieldMatlab{1}) = data. (fieldMatlab{1})...
%               - data. (fieldMatlab{1})(1);
% end
    
for i = 1:length(nonTimeAlignedCols),
    idx = nonTimeAlignedCols(i);
    timeGridIndx = ~isinf(dataArray{:,idx});
    if (~isempty(fieldMatlab{idx})),
        evalstring = ['data.',fieldMatlab{idx}, ...
            '.timestamps = dataArray{1}(timeGridIndx)'';'];
        eval(evalstring);
        if checkUnixTime == 1,
            eval(['data.',fieldMatlab{idx},'.startUnixTime = data.',fieldMatlab{idx},'.timestamps(1);']);
            if isfield(data,'startUnixTime'),
                evalString = ['data.',fieldMatlab{idx},'.timestamps = data.',...
                       fieldMatlab{idx},'.timestamps - data.startUnixTime;'];
                eval(evalString);
            else
                evalString = ['data.',fieldMatlab{idx},'.timestamps = data.',...
                    fieldMatlab{idx},'.timestamps - data.',fieldMatlab{idx},'.timestamps(1);'];
                eval(evalString);
            end
        end
        evalstring = ['data.',fieldMatlab{idx}, ...
                '.signal = dataArray{idx}(timeGridIndx)'';'];
        eval(evalstring);
                
        
    end
end

end

