function [data] = parsePATstreamV2(data)
%Expected Sequence: 

% // Final result for BP
% float HR_avg_for_BP;
% float HR_std_for_BP;
% float PAT_avg_for_BP;
% float PAT_std_for_BP;
% float PAT_CI_for_BP
% 
% // Intermediate result
% float HR_raw;
% float HR_raw_error_flag;
% float HR_avg;
% float HR_std;
% float HR_hq_avg;
% float HR_hq_std;
% 
% float pat_raw;
% float pat_raw_error_flag;
% float pat_avg;
% float pat_std;
% float pat_hq_avg;
% float pat_hq_std;
% 
% float pat_raw_CI;

pat_stream_timestamps = data.band_pat.timestamps;
pat_stream = data.band_pat.signal;
%convert -1 to NaN
pat_stream(pat_stream == -1) = NaN;

%define new fields in data array
data.band_pat.timestamps = []; %generated in the parser, not in the band!
data.band_pat.HR_avg_for_BP = [];
data.band_pat.HR_std_for_BP = [];
data.band_pat.pat_avg_for_BP = [];
data.band_pat.pat_std_for_BP = [];

data.band_pat.pat_CI_for_BP = [];  

data.band_pat.HR_raw = [];
data.band_pat.HR_raw_error_flag = [];
data.band_pat.HR_avg = [];
data.band_pat.HR_std = [];
data.band_pat.HR_hq_avg = [];
data.band_pat.HR_hq_std = [];

data.band_pat.pat_raw = [];
data.band_pat.pat_raw_error_flag = [];
data.band_pat.pat_avg = [];
data.band_pat.pat_std = [];
data.band_pat.pat_hq_avg = [];
data.band_pat.pat_hq_std = [];

data.band_pat.pat_raw_CI = [];  

%find the locations of data blocks that belong together
block_spacing = 0.05; %depends on HR but it is assumed that entire block is written within 50ms --> block spacing is > 50ms
block_locations =  [1, find(diff(pat_stream_timestamps) > block_spacing)];
block_elements = []; %elements in the block (probably incomplete!)
block_timestamps = []; %timestamps for each element in the block as coming from the csv (--> important in order to identify their sequence)
block_timestamp = []; %single timestamp for entire quitet (eventually they all correspond/refer to a single pat timestamp)
time_spacing = 0.000016; %time spacing between elements in a single block is ~0.000016 (exact: 1.52590219002491e-05)

for i = 1:numel(block_locations)
    
    if numel(block_locations) > 1 %only if more than one block is detected
        
        if i == 1 %for first block
            block_elements = pat_stream(block_locations(i) : block_locations(i+1));
            block_timestamps = pat_stream_timestamps(block_locations(i) : block_locations(i+1));
            block_timestamp = pat_stream_timestamps(block_locations(i)); %i.e. 'block_timestamps(1)'
        elseif i == numel(block_locations) %for the last block
            block_elements = pat_stream(block_locations(i)+1 : numel(pat_stream)); %+1 outside parentheses due to application of diff function
            block_timestamps = pat_stream_timestamps(block_locations(i)+1 : numel(pat_stream_timestamps));
            block_timestamp = pat_stream_timestamps(block_locations(i)+1);
        else %for any other block
            block_elements = pat_stream(block_locations(i)+1 : block_locations(i+1)); %+1 outside parentheses due to application of diff function
            block_timestamps = pat_stream_timestamps(block_locations(i)+1 : block_locations(i+1));
            block_timestamp = pat_stream_timestamps(block_locations(i)+1);
        end
        
        %timestamp and pat are assumed to be always available when block is detected
        data.band_pat.timestamps(i) = block_timestamp;
        data.band_pat.HR_avg_for_BP(i) = block_elements(1);
        
    else %if only single block was found
        
        data.band_pat.timestamps = [];
        data.band_pat.HR_avg_for_BP = [];
        
    end
    

    %assign NaN's to the other feature vectors at current block location (they either remain in case of missing data or are replaced by values)
    data.band_pat.HR_std_for_BP(i) = NaN;
    data.band_pat.pat_avg_for_BP(i) = NaN;
    data.band_pat.pat_std_for_BP(i) = NaN;
    
    data.band_pat.pat_CI_for_BP(i) = NaN;  
    
    data.band_pat.HR_raw(i) = NaN;
    data.band_pat.HR_raw_error_flag(i) = NaN;
    data.band_pat.HR_avg(i) = NaN;
    data.band_pat.HR_std(i) = NaN;
    data.band_pat.HR_hq_avg(i) = NaN;
    data.band_pat.HR_hq_std(i) = NaN;

    data.band_pat.pat_raw(i) = NaN;
    data.band_pat.pat_raw_error_flag(i) = NaN;
    data.band_pat.pat_avg(i) = NaN;
    data.band_pat.pat_std(i) = NaN;
    data.band_pat.pat_hq_avg(i) = NaN;
    data.band_pat.pat_hq_std(i) = NaN;

    data.band_pat.pat_raw_CI(i) = NaN;    
    
    
    %check for completeness of block and assigna vals to vars
    for j = 1:numel(block_elements)
        if (j == 2) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing)
            data.band_pat.HR_std_for_BP(i) = block_elements(2);
        end
        
        if (j == 3) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 2*time_spacing)
            data.band_pat.pat_avg_for_BP(i) = block_elements(3);
        end
        
        if (j == 4) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 3*time_spacing)
            data.band_pat.pat_std_for_BP(i) = block_elements(4);
        end
        
        if (j == 5) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 4*time_spacing)
            data.band_pat.pat_CI_for_BP(i) =  block_elements(5);
        end        
        
        if (j == 6) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 5*time_spacing)
            data.band_pat.HR_raw(i) =  block_elements(6);
        end
        
        if (j == 7) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 6*time_spacing)
            data.band_pat.HR_raw_error_flag(i) =  block_elements(7);
        end
        
        if (j == 8) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 7*time_spacing)
            data.band_pat.HR_avg(i) =  block_elements(8);
        end
        
        if (j == 9) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 8*time_spacing)
            data.band_pat.HR_std(i) =  block_elements(9);
        end
        
        if (j == 10) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 9*time_spacing)
            data.band_pat.HR_hq_avg(i) =  block_elements(10);
        end
        
        if (j == 11) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 10*time_spacing)
            data.band_pat.HR_hq_std(i) =  block_elements(11);
        end
        
        if (j == 12) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 11*time_spacing)
            data.band_pat.pat_raw(i) =  block_elements(12);
        end
        
        if (j == 13) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 12*time_spacing)
            data.band_pat.pat_raw_error_flag(i) =  block_elements(13);
        end        
        
        if (j == 14) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 13*time_spacing)
            data.band_pat.pat_avg(i) =  block_elements(14);
        end   
        
        if (j == 15) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 14*time_spacing)
            data.band_pat.pat_std(i) =  block_elements(15);
        end   
        
        if (j == 16) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 15*time_spacing)
            data.band_pat.pat_hq_avg(i) =  block_elements(16);
        end   
        
        if (j == 17) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 16*time_spacing)
            data.band_pat.pat_hq_std(i) =  block_elements(17);
        end   
        
        if (j == 18) && (diff([block_timestamps(j-1), block_timestamps(j)]) < time_spacing) && (diff([block_timestamps(1), block_timestamps(j)]) < 17*time_spacing)
            data.band_pat.pat_raw_CI(i) =  block_elements(18);
        end           

    end
    
end

end