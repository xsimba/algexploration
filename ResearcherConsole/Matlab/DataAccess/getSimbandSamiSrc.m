function srcData = getSimbandSamiSrc(samiCredentials, times, version, forceLoad)
% getDataSAMI Retrieves data from SAMI and saves data in .mat file in ./DataBases/SAMI
%
% data = getDataSami(samiCredentials, times, forceLoad, dbDir, alignFlag)
%
%   INPUTS:
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%       - forceLoad: flag to override cache
%       - dbDir: database directory
%       - alignFlag: flag to produce aligned data or unaligned data
%
%   OUTPUT:
%       - outData: struct containing Simband data
%   Usage Example:
%       samiCredentials.uid    = 'xxx'
%       samiCredentials.did    = 'xxx'
%       samiCredentials.tok    = 'xxx'
%       times.startTime        = '123456789' - Unix Time
%       times.endTime          = '123499999' - Unix Time
%       data = getData(samiCredentials,times);
% dataset_directory = fullfile('DataBases','SAMI');

global RC_CONSOLE_DATABASE

if isempty(RC_CONSOLE_DATABASE),
    RC_CONSOLE_DATABASE = fullfile('.','DataBases');
end

dbDir = fullfile(RC_CONSOLE_DATABASE,'SAMI');

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

if ~exist('version', 'var'),
    version = 'v4';
end

pythonLoadFunction = @simbaPythonFetch;
switch(version)
    case 'v1'
        deviceSchema = @simbaSchemaSamiV1;
    case 'v0'
        deviceSchema = @simbaSchemaSamiV0;
    case 'v2'
        deviceSchema = @simbaSchemaSamiV2;
    case 'v3'
        deviceSchema = @simbaSchemaSamiV3;
    case 'v4'
        deviceSchema = @simbaSchemaSamiV4;
    case 'v5'
        deviceSchema = @simbaSchemaSamiV5;
    otherwise
        error('device schema not recognized');
end

srcData = getDataSamiBasic (samiCredentials, times, forceLoad, ...
    dbDir, 'aligned', pythonLoadFunction, deviceSchema);

if isfield(srcData, 'timestamps'),
    srcData.unixTimeStamps = srcData.timestamps;
    if ~isempty(srcData.timestamps),
        srcData.timestamps = (srcData.timestamps - srcData.timestamps(1))/1000;
    end

end

