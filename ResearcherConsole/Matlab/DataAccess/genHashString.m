function hash = genHashString(inString)
%
% genHashString generates an MD5 hash from the input string
%
z = java.lang.String(inString);
md5 = java.security.MessageDigest.getInstance('MD5');
adapter = javax.xml.bind.annotation.adapters.HexBinaryAdapter;
hString = adapter.marshal(md5.digest(z.getBytes()));
hash = char(hString);

end
