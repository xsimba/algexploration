function [data, srcData] = simbaPortalFetch(params)
%
% simbaPortalFetch(params)
%
% Input: parameter structure containing 
%   sdid:  device id, 
%   startDate: start datetime stamp in UNIX time since epoch [ms]
%   endDate: end datetime stamp in UNIX time since epoch [ms]
%
global RC_DATA_ACCESS_LAYER

if (isempty(RC_DATA_ACCESS_LAYER)),
    RC_DATA_ACCESS_LAYER = ['..',filesep,'DataAccessLayer'];
end

%
% There may be a lower level way to do this, but the savings are small
% compared to network traffic delays currently.
% 
s1 = ['python ',RC_DATA_ACCESS_LAYER,filesep,'getSamiSimbandExpPortalAuthenticate.py --startDate "', ...
    num2str(params.startDate), '" --endDate "', num2str(params.endDate), '" --sdid "', params.sdid];
string = [s1];
system (string, '-echo');

%
% load data into workspace
%
data = load('MatlabTransfer.mat');
srcData = load('MatlabTransferSrc.mat');

end