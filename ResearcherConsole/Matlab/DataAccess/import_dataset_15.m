%% Import data from text file.
% Script for importing data from the following text file:
%
%    C:\code\alg_devel\ResearcherConsole\Matlab\15 - data.csv
%
% To extend the code to different selected data or a different text file,
% generate a function instead of a script.

% Auto-generated by MATLAB on 2014/07/29 14:16:51

%% Initialize variables.
filename = 'C:\code\alg_devel\ResearcherConsole\Matlab\15 - data.csv';
delimiter = ',';
startRow = 2;

%% Format string for each line of text:
%   column1: double (%f)
%	column2: text (%s)
%   column3: double (%f)
%	column4: double (%f)
%   column5: double (%f)
%	column6: double (%f)
%   column7: double (%f)
%	column8: double (%f)
%   column9: double (%f)
%	column10: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%s%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID = fopen(filename,'r');

%% Read columns of data according to format string.
% This call is based on the structure of the file used to generate this
% code. If an error occurs for a different file, try regenerating the code
% from the Import Tool.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);

%% Close the text file.
fclose(fileID);

%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.

%% Allocate imported array to column variable names
clear data

fullTime   = dataArray{:,1};
ECGLeadOn0 = dataArray{:,2};
PPG_10 = dataArray{:,7};
ecg_LOn    = [];
for i = 1:length(ECGLeadOn0),
    ecg_LOn(i) = str2double(ECGLeadOn0{i});
end
ecgLeadIndx = find(~isnan(ecg_LOn));
otherIndx   = find(~isnan(PPG_10));

data.ECGLeadtime = fullTime(ecgLeadIndx);
data.time = fullTime(otherIndx);

data.ECGLeadOn0 = str2double(ECGLeadOn0(ecgLeadIndx));
data.ECG0 = dataArray{:, 3}(otherIndx);
data.Accelerometer0 = dataArray{:, 4}(otherIndx);
data.Accelerometer1 = dataArray{:, 5}(otherIndx);
data.Accelerometer2 = dataArray{:, 6}(otherIndx);
data.PPG_10 = dataArray{:, 7}(otherIndx);
data.PPG_11 = dataArray{:, 8}(otherIndx);
data.PPG_20 = dataArray{:, 9}(otherIndx);
data.PPG_21 = dataArray{:, 10}(otherIndx);



%% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray ans;