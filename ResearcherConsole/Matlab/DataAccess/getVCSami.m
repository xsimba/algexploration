function data = getVCSami(samiCredentials, times, forceLoad)
% getDataSAMI Retrieves data from SAMI and saves data in .mat file in ./DataBases/SAMI
%   INPUTS:
%       - samiCredentials: struct with user id,device id,device token
%       - times: struct with start date and end data
%   OUTPUT:
%       - data: struct containing Simband data
%   Usage Example:
%       samiCredentials.uid    = 'xxx'
%       samiCredentials.did    = 'xxx'
%       samiCredentials.tok    = 'xxx'
%       times.startTime        = '123456789' - Unix Time
%       times.endTime          = '123499999' - Unix Time
%       data = getData(samiCredentials,times);
% dataset_directory = fullfile('DataBases','SAMI');

%
% TODO: have getDataSamiRaw and getDataSami both call a common low level
% function
%
global RC_CONSOLE_DATABASE

if isempty(RC_CONSOLE_DATABASE),
    RC_CONSOLE_DATABASE = fullfile('.','DataBases','SAMI');
end

if ~exist('forceLoad', 'var'),
    forceLoad = false;
end

pythonLoadFunction = @vcPythonFetch;
deviceSchema = @vcSchemaSamiV1;

data = getDataSamiBasic (samiCredentials, times, forceLoad, ...
    RC_CONSOLE_DATABASE, ...
    'not aligned', pythonLoadFunction, deviceSchema);

end
