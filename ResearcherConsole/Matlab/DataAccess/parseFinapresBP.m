function [finapres_bp] = parseFinapresBP(fileName)

    fid = fopen(fileName,'r');
    
    %Skip Header and Units lines
    fgetl(fid);
    fgetl(fid);
    
    %Skip all lines starting with a "0.000" timestamp 
    %Find the first line from where data can be read
    line = fgetl(fid);
    C = strsplit(line,',');
    lineCount = 3;
    while(strcmp(C{1},'0.000')==1)
        line = fgetl(fid);
        C = strsplit(line,',');
        lineCount = lineCount+1;
    end
    
    %Read CSV data
    finapres_bp = csvread(fileName,lineCount-1,0);
    
end