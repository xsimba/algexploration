function data = getLocalSimbandData(userId, intervention , posture, measurement)
% interventions are in {'static','variation','consistency'}
% posture is in {'Sitting','Standing'}
% Measurement is in {1,2,3}
    try
    % type of intervention
    switch intervention
        case 'static'
            if userId < 10
                folder_name = ['SP0',num2str(userId)];
            else
                folder_name = ['SP',num2str(userId)];
            end
            itv = 'S';
        case 'variation'
            if userId < 10
                folder_name = ['SP0',num2str(userId)];
            else
                folder_name = ['SP',num2str(userId)];
            end
            itv = 'V';
        case 'consistency'
           if userId < 10
                folder_name = ['SC0',num2str(userId)];
            else
                folder_name = ['SC',num2str(userId)];
            end
            itv = 'S';
    end
    
    % type of posture
    switch posture
        case 'standing'
            pos = 'ST';
        case 'sitting'
            pos = 'SI';
    end
    
    % assembling filename
    if strcmpi(itv,'V')
        filename = fullfile('Databases','Simba',folder_name,[folder_name,'_',itv]);
    else
        filename = fullfile('Databases','Simba',folder_name,[folder_name,'_',itv,'_',pos,'_',num2str(measurement)]);
    end
    
    try
        load(filename);
    catch err
        if strcmpi(err.identifier,'MATLAB:load:couldNotReadFile')
            error('Sorry! These data are not available yet...')
        else
            disp(err)
        end
    end
        
    all_ssb = xlsread(fullfile('Databases','Simba','PAT-based_BP_estimation_SSB_and_BP_data.xlsx'),1);
    ssb = all_ssb(:,userId);
    data.ssb.age = ssb(1);
    data.ssb.height = ssb(2);
    data.ssb.weight = ssb(3);
    data.ssb.posture = strcmpi(posture,'standing');
    
    all_refs = xlsread(fullfile('Databases','Simba','PAT-based_BP_estimation_SSB_and_BP_data.xlsx'),2);
    all_refs = reshape(all_refs(:,userId),3,length(all_refs)/3);
    if strcmpi(intervention,'variation')
        ref = all_refs(:,7);
    elseif strcmpi(intervention,'static')
        all_refs = all_refs(:,1:6);
        if strcmpi(posture,'sitting')
            all_refs = all_refs(:,1:3);
        elseif strcmpi(posture,'standing')
            all_refs = all_refs(:,4:6);
        end
        ref = all_refs(:,measurement);
    elseif strcmpi(intervention,'consistency')
        all_refs = all_refs(:,8:13);
        if strcmpi(posture,'sitting')
            all_refs = all_refs(:,1:3);
        elseif strcmpi(posture,'standing')
            all_refs = all_refs(:,4:6);
        end
        ref = all_refs(:,measurement);
    end
        
    data.reference.sbp = ref(1);
    data.reference.dbp = ref(2);
    data.reference.hr = ref(3);

%     % set the accelerometer properly
%     data.acc.x.signal = abs(data.acc.x.signal); 
%     data.acc.y.signal = abs(data.acc.y.signal); 
%     data.acc.z.signal = abs(data.acc.z.signal); 
    try
        data.acc.All(1,:) = data.acc.x.signal;
        data.acc.All(2,:) = data.acc.y.signal;
        data.acc.All(3,:) = data.acc.z.signal;

        data.acc.All(4,:) = sqrt(data.acc.x.signal.^2 + data.acc.y.signal.^2 + data.acc.z.signal.^2);
        data.acc.All = (data.acc.All./(2^14));
    catch err
        disp(err)
    end
    % inversion
    data = PPGinv(data);
    
     catch err
        disp(err)
    end
end

