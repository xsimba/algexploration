function [finapres_ASCII] = parseFinapresASCII(fileName)

    %The first 4 lines contain headers and Units information
    finapres_ASCII = csvread(fileName,4,0);

end