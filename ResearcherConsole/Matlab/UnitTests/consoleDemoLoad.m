%
% Research Console Test Script
%
% This script demonstrates loading a dataset from Sami into
% the matlab workspace.
%
% A. Khalak, 8/14/2014

%
% add researcher console scripts to the path
% 
setRCpath;

%
% load session database
%
c = loadSessionData('./Databases/MySessions.xlsx');
[sc, time] = setSamiSessionData(c, 'test003');

% get data from SAMI
data = getSimbandSami(sc,time, 0, 'v1');

