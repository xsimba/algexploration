%
% Load WebGuiCsv and aave to Researcher Console v0 format
%
setRCpath;

%
% load session references and select WebGuiCsv
%
c = loadSessionData('./Databases/MySessions.xlsx');
csvFilename = setWebGuiSessionData(c, 'ssicPre001.1');

% load data into workspace in v0 format
data = getSimbandCsv(csvFilename, 'v0');


