function setRCpath(RC_MATLAB_DIR)

%
% todo: remove RC_MATLAB_DIR as an argument and take it in as a global?
%
global SSIC_ALGO_DIR IMEC_ALGO_DIR RC_CONSOLE_DATABASE EXTERNAL_DATABASE

if ~exist('RC_MATLAB_DIR', 'var'),
    RC_MATLAB_DIR = '.';
end

% add paths to packages

addpath(fullfile(RC_MATLAB_DIR, 'DataAccess'));
addpath(fullfile(RC_MATLAB_DIR, 'DataAccess/schemas'));
addpath(fullfile(RC_MATLAB_DIR, 'DataMining'));
addpath(fullfile(RC_MATLAB_DIR, 'ThirdParty'));
addpath(fullfile(RC_MATLAB_DIR, 'ValidationFramework'));
addpath(fullfile(RC_MATLAB_DIR, 'ViewReporting'));
addpath(fullfile(RC_MATLAB_DIR, 'PipelineProcessing'));

%
%add paths to imec algorithms

addpath(fullfile(IMEC_ALGO_DIR, 'BeatDetection'));
addpath(fullfile(IMEC_ALGO_DIR, 'BeatDetection', 'Docs'));
addpath(fullfile(IMEC_ALGO_DIR, 'BeatDetection', 'Wavelets'));
addpath(fullfile(IMEC_ALGO_DIR, 'BeatDetection/Wavelets/Coeffs'));
addpath(fullfile(IMEC_ALGO_DIR, 'BloodPressure', 'BP_framework'));
addpath(fullfile(IMEC_ALGO_DIR, 'BloodPressure', 'BP_framework_tracker'));
addpath(fullfile(IMEC_ALGO_DIR, 'BloodPressure', 'BP'));
addpath(fullfile(IMEC_ALGO_DIR, 'ConfidenceIndicator/CI/CI_I'));
addpath(fullfile(IMEC_ALGO_DIR, 'ConfidenceIndicator/CI/CI_II'));
addpath(fullfile(IMEC_ALGO_DIR, 'HeartRate'));
addpath(fullfile(IMEC_ALGO_DIR, 'PPGInversion', 'PPGinv'));
addpath(fullfile(IMEC_ALGO_DIR, 'FeatureExtraction'));
addpath(fullfile(IMEC_ALGO_DIR, 'PulseArrivalTime/PAT'));
addpath(fullfile(IMEC_ALGO_DIR, 'SRC', 'Test'));

% 
%
% add hooks into the RC-based code at SSIC within algorithm development
%
addpath(fullfile(SSIC_ALGO_DIR, 'BiosemHR', 'biosem_matlab'));
addpath(fullfile(SSIC_ALGO_DIR, 'BiosemHR', 'biosem_analysis_matlab'));
addpath(fullfile(SSIC_ALGO_DIR, 'beatInterval_autocorr'));
addpath(fullfile(SSIC_ALGO_DIR, 'beatInterval_frequency'));
addpath(fullfile(SSIC_ALGO_DIR, 'InstFreqHR'));
addpath(fullfile(SSIC_ALGO_DIR, 'InstFreqHR', 'filters'));
addpath(fullfile(SSIC_ALGO_DIR, 'MotionDetector'));
addpath(fullfile(SSIC_ALGO_DIR, 'beatInterval_tde'));
addpath(fullfile(SSIC_ALGO_DIR, 'OOBTracker'));
addpath(fullfile(SSIC_ALGO_DIR, 'gaussianFitPPG'));
addpath(fullfile(SSIC_ALGO_DIR, 'MonitoringMode_HR_BD')); 
addpath(fullfile(SSIC_ALGO_DIR, 'Simulink_library'));

%add paths to RC database and external databases (e.g. local copy of former 'simband_data' repo)

addpath(genpath(RC_CONSOLE_DATABASE))
addpath(genpath(EXTERNAL_DATABASE))
