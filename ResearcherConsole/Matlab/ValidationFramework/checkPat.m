function [ metrics ] = checkPat( data, timeRange )

metrics.pat_avg_err = checkPatField(data, 'pat_avg', timeRange);
metrics.pat_sd_err  = checkPatField(data, 'pat_sd', timeRange);
metrics.HR_avg_err  = checkPatField(data, 'HR_avg', timeRange);
metrics.HR_sd_err   = checkPatField(data, 'HR_sd', timeRange);

end

function [ error ] = checkPatField( data, field, timeRange )

N = length(data.pat_c.timestamps);

e = zeros(1,N);
r = zeros(1,N);
for i = (1:N)
    t = data.pat_c.timestamps(i);
    if t <= timeRange(1) || t > timeRange(2)
        continue
    end
    
    n = find(abs(data.pat.(field).timestamps - t) < 0.001);
    if length(n) == 1
        patM = data.pat.(field).signal(n);
        patC = data.pat_c.(field).signal(i);
        e(i) = abs(patM - patC);
        r(i) = e(i) / patM;
    end
end

error = sum(e)/N;

end

