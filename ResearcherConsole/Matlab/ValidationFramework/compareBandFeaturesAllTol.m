function metrics = compareBandFeatures( computedOutput,data,signalReference )
%compareBandFeatures compares the output of doTest with simband features
%   Usage Example: 
%       output = doTest(alg,signal,Cfg);
%       metrics = compareBandFeatures( output,signal )
%           where metrics is a struct with
%           metrics.truePositiveRate
%           metrics.tpositivePredictiveValue
%           metrics.falseNegativeRate
 
    
  
    temp = data.(signalReference);
    
    reference = (temp(1,:)-temp(1,1))/1000;
    time_tolerance = 0.05:0.05:.75;
    x_points = numel(time_tolerance);

    true_positive_rate = zeros(1,x_points);
    positive_predictive_value = zeros(1,x_points);
    false_negative_rate = zeros(1,x_points);

    detections = repmat(computedOutput,numel(reference),1);
    time_boundaries = abs(bsxfun(@minus,detections,reference'));

    for tolIdx = 1:x_points

        m = compute_metrics(computedOutput,temp,tol);

    end

metrics.time_tolerance = time_tolerance ; 
metrics.truePositiveRate = true_positive_rate;
metrics.falseNegativeRate = false_negative_rate;
metrics.positivePredictiveValue = positive_predictive_value;

end

