function output = doTest( algorithm,data,parameters )
%doTest runs algorithm on signal
%   INPUTS: 
%       - algorithm: algorithm to test. It shoould be stored in the pathr
%           "Algorithms/algorithm"
%       - signal: channnel used by the algorithm
%       - configuration: all the parameters needed embedded in a struct
%   OUTPUT:
%       - output: output of the algorithm
%   Usage Example for Beat Detector: 
%       detected_beats_timestamps = doTest('BDBasic','ecg',Cfg)
%       where
%            "BDBasic" is stored in ./Algorithms/BDBasic
%            Cfg.Fs = 128 ; struct containing the algorithms parameters  
    
    % addpath 
    algorithms_directory = fullfile('Algorithms',algorithm);          
    addpath(fullfile(pwd,algorithms_directory));

    % runs the function here
    try
        % check this with ASIF
        output = feval(algorithm,data,parameters);
    catch err
        disp(['Error: ',err.identifier])
        output = err ;
    end
    
    rmpath(fullfile(pwd,algorithms_directory));
    
end

