function [bd_metric] = bd_metric(beats, reference_annotation,CI, RefType)

% RefType:
% 1. Annotaiton: manual annotated beats
% 2. ECG: use ECG R peaks as reference

%################## outputs pre-define #####################
true_positive  = 0;   % correct beats
false_positive = 0;   % false detection
false_negative = 0;   % miss detection
ind_FP =[];           % index of annotation intervals where false detection occured
ind_FN =[];           % index of annotation intervals where miss detection occured
ind_FP_highCI  = [];  % index of annotation intervals where false detection occured & signal CIraw = 3 or 4
ind_FN_highCI  = [];  % index of annotation intervals where miss detection occured & signal CIraw = 3 or 4

ind_pp=1;
RR_pp = []; % 1st row: ibi of detected points; 2nd row: ibi of annotation; 3rd row: time index of ibi 

%###########################################################

% if exist('RefType') && strcmp(RefType,'Annotation')
% Annotation
for i = 1:length(reference_annotation)-1
    % find number of detected beats located in current ref interval
    [ind] = find (beats >= reference_annotation(i) & beats < reference_annotation(i+1));
    if length(ind) == 1 % TP
        true_positive    = true_positive+1;
        
        if i~=length(reference_annotation)-1
            RR_pp(1,ind_pp) = beats(ind+1)-beats(ind);
            RR_pp(2,ind_pp) = reference_annotation(i+1)-reference_annotation(i);
            RR_pp(3,ind_pp) = beats(ind+1);
            ind_pp = ind_pp+1;
        end
    elseif length(ind) >1 % FP;
        true_positive    = true_positive+1;
        false_positive   = false_positive+length(ind)-1;
        ind_FP           = [ind_FP i];
        
        if i~=length(reference_annotation)-1
            RR_pp(1,ind_pp) = beats(ind(end)+1)-beats(ind(1));
            RR_pp(2,ind_pp) = reference_annotation(i+1)-reference_annotation(i);
            RR_pp(3,ind_pp) = beats(ind(1)+1);
            ind_pp = ind_pp+1;
        end        
        
    else % FN
%         ind_pp
        false_negative   = false_negative+1;
        ind_FN           = [ind_FN i];
        ind_pp = ind_pp-1;
%         ind_pp
    end
    
end
% else
% ECG R peaks
if exist('CI')
    temp=find(CI==1 | CI==2);
    for i =1:length(ind_FP)
        if isempty(find(abs(reference_annotation(ind_FP(i))-temp)<2))
            ind_FP_highCI = [ind_FP_highCI, ind_FP(i)];
        end
    end
    for i =1:length(ind_FN)
        
        if isempty(find(abs(reference_annotation(ind_FN(i))-temp)<2))
            ind_FN_highCI = [ind_FN_highCI, ind_FN(i)];
        end
    end    
end

ibi_Error = RR_pp(1,:) - RR_pp(2,:);
ibi_RMSError = norm(ibi_Error)/sqrt(length(ibi_Error));


bd_metric.true_positive       = true_positive;
% bd_metric.true_positive_rate  = true_positive;
bd_metric.false_positive      = false_positive;
% bd_metric.false_positive_rate = false_positive;
bd_metric.false_negative      = false_negative;
% bd_metric.false_negative_rate = false_negative;
bd_metric.ind_FP      = ind_FP;
bd_metric.ind_FN      = ind_FN;

bd_metric.ind_FP_highCI      = ind_FP_highCI;
bd_metric.ind_FN_highCI      = ind_FN_highCI;

bd_metric.RR_pp =RR_pp;
bd_metric.ibi_Error =ibi_Error;
bd_metric.ibi_RMSError =ibi_RMSError;
% end
