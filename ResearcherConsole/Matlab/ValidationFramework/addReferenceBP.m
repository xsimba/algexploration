function data = addReferenceBP( data, referenceBP )
% referenceBP = [SBP,DBP]
    
    data.reference_SBP =  referenceBP(1);
    data.reference_DBP =  referenceBP(2);


end

