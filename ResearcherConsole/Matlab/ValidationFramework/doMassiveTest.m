function interbeat_time = doMassiveTest( algorithm,parameters )

    
    interventions = {'static','variation','consistency'};
    posture = {'sitting','standing'};
    measurements = 1:3;
    num_of_subjects = 21 ; 
    
    interbeat_time = [];

    for sIdx = 1:num_of_subjects
        for ii = 1 : numel(interventions)
            for jj = 1 : numel(posture)
                for kk = 1 : numel(measurements)
                    data = getLocalSimbandData(sIdx,interventions{ii},posture{jj},kk);
                    try
                        if strcmpi(parameters(1:3),'ECG')
                            this_beats = doTest('BD',data.(lower(parameters(1:3))).signal,parameters);
                            interbeat_time = [ interbeat_time diff(this_beats) ] ;
                        elseif strcmpi(parameters(1:3),'PPG')
                            this_beats = doTest('BD',data.(lower(parameters(1:3))).(lower(parameters(end))).signal,parameters(1:3));
                            interbeat_time = [ interbeat_time diff(this_beats(1,:)) ] ;
                        end
                    catch err
                        disp(err.identifier)
                    end
                end
            end
        end
    end
    
    figure('units','normalized','outerposition',[0 0 1 1])
    hist(interbeat_time,5000);
    xlim([0 5]);
    xlabel('Seconds');
    
end

