function output = doTestData( data, algorithm,signal,configuration )
%doTest runs algorithm on signal on a specific data set
%   INPUTS: 
%       - algorithm: algorithm to test. It shoould be stored in the pathr
%           "Algorithms/algorithm"
%       - signal: channnel used by the algorithm
%       - configuration: all the parameters needed embedded in a struct
%   OUTPUT:
%       - output: output of the algorithm
%   Usage Example for Beat Detector: 
%       detected_beats_timestamps = doTest('BDBasic','ecg',Cfg)
%       where
%            "BDBasic" is stored in ./Algorithms/BDBasic
%            Cfg.Fs = 128 ; struct containing the algorithms parameters  


thisData = extractSignalData(data, signal);

% runs the function here
output = feval(algorithm,this_signal,configuration);

end

