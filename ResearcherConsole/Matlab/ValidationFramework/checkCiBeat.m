function [ metrics ] = checkCiBeat( data, channel, timeRange )

if strcmp(channel,'ecg')
    d = data.ecg;
else
    d = data.ppg.(channel(end));
end

e = zeros(1,length(d.CI_beats_cts));
for i = (1:length(d.CI_beats_cts))
    if d.CI_beats_cts(i) <= timeRange(1) || d.CI_beats_cts(i) > timeRange(2)
        continue
    end
    
    n = find(abs(d.beats(1,:) - d.CI_beats_cts(i)) < 0.001);
    if length(n) == 1
        if n > length(d.CI_beat)
            break
        end
        ciM = d.CI_beat(n);
        ciC = d.CI_beat_c(i);
        e(i) = abs(ciM - ciC);
    end
end

metrics.error = e;
metrics.errors = sum(e > 0);

end

