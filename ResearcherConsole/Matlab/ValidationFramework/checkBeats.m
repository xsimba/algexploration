function [ metrics ] = checkBeats( beatsM, beatsC, timeRange )

    startM = find(beatsM >= timeRange(1), 1, 'first') - 1;
    startC = find(beatsC >= timeRange(1), 1, 'first') - 1;
    
    beatsM(1:startM) = [];
    beatsM(beatsM >= timeRange(2)) = [];
    beatsC(1:startC) = [];
    beatsC(beatsC >= timeRange(2)) = [];
    
    metrics = compute_metrics(beatsC,beatsM,0.001);
    
    metrics.tp_ts = beatsM(metrics.tp_indx == 1);
    metrics.fn_ts = beatsM(metrics.fn_indx == 1);
    metrics.fp_ts = beatsC(metrics.fp_indx == 1);

    metrics.tp_indx = [ zeros(1,startM) metrics.tp_indx ];
    metrics.fn_indx = [ zeros(1,startM) metrics.fn_indx ];
    metrics.fp_indx = [ zeros(1,startC) metrics.fp_indx ];

    metrics.errors = metrics.NrFalsePositives + metrics.NrFalseNegatives;

end

