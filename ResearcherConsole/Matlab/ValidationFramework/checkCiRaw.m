function [ metrics ] = checkCiRaw( data, channel, timeRange )

if strcmp(channel,'ecg')
    d = data.ecg;
else
    d = data.ppg.(channel(end));
end

ciRawM = d.CI_raw;
ciRawC = d.CI_raw_c;
ciRawCts = d.CI_raw_cts;
ciRawC = [ones(1, ciRawCts(1)) ciRawC];

r = (timeRange(1)):int32(timeRange(end));
ciRawM = ciRawM(r);
ciRawC = ciRawC(r);

metrics.error = max(abs(ciRawM - ciRawC));
if metrics.error > 0
    metrics.error_ts = find(abs(ciRawM - ciRawC) == metrics.error) + double(r(1) - 1); 
else
    metrics.error_ts = [];
end
metrics.errors = length(metrics.error_ts);

end

