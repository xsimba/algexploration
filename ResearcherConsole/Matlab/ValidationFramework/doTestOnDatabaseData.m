function output = doTestOnDatabaseData( algorithm,parameters,varargin )
%doTest runs algorithm on signal
%   INPUTS: 
%       - algorithm: algorithm to test. It shoould be stored in the pathr
%           "Algorithms/algorithm"
%       - signal: channnel used by the algorithm
%       - configuration: all the parameters needed embedded in a struct
%   OUTPUT:
%       - output: output of the algorithm
%   Usage Example for Beat Detector: 
%       detected_beats_timestamps = doTest('BDBasic','ecg',Cfg)
%       where
%            "BDBasic" is stored in ./Algorithms/BDBasic
%            Cfg.Fs = 128 ; struct containing the algorithms parameters  
    
    % addpath 
    dataset_directory = fullfile('.','DataBases');
    algorithms_directory = fullfile('Algorithms',algorithm);          
    addpath(fullfile(pwd,algorithms_directory));
    
    % code to deal with raw data before SRC
    % %%%% bugprone code: CHECK IT FOR SMARTER SOLUTION %%%% %%
    if nargin==2
        file_ends_with = '*.mat';
        data_starts_with='';
    elseif nargin==3
        file_ends_with = '*raw.mat';
        data_starts_with='raw_';
    end
    % loads data
    % looks for test files in the dataset directory 
    test_files = dir(fullfile(pwd,dataset_directory,file_ends_with));    
           
    load(fullfile(pwd,dataset_directory,test_files(1).name));
    input = eval([data_starts_with,'data']);
    % runs the function here
    try
        % check this with ASIF
        output = feval(algorithm,input,parameters);
    catch err
        disp(['Error: ',err.identifier])
        output = err ;
    end
    
    rmpath(fullfile(pwd,algorithms_directory));
    
end

