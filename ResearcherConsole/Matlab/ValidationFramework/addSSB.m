function data = addSSB( data, ssb )
% ssb = [age,weight, height]
    thisSSB.age = ssb(1) ; 
    thisSSB.weight = ssb(2) ; 
    thisSSB.height = ssb(3) ;
    
    data.ssb = thisSSB ; 


end

