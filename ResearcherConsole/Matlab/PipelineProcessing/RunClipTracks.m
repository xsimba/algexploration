function data1 = RunClipTracks(data, splitStart, splitEnd, version)
%
% add interbeattime tracks
%

if (~exist('filterFlag')),
    filterFlag = 0;
end

if (~exist('version', 'var')),
    version = '3v0';
end

switch lower(version)
    case ('v1')
        schema = simbaSchemaCsvV1;
    case ('2v0')
        schema = simbaSchemaCsv2V0;
    case ('v0')
        schema = simbaSchemaCsvV0;
    case ('3v0')
        schema = simbaSchemaCsv3V0;
    otherwise
        error ('version not recognized');
end

%
% may want to add some input validation
%

%%
% loop over fields
%
[tim, indStart] = min(abs(data.timestamps-splitStart));
[tim, indEnd] = min(abs(data.timestamps-splitEnd));

tracks = {'ecg', 'acc.x', 'acc.y', 'acc.z', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
if isfield(data.ppg ,'e')
    tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
end
if isfield(data.ppg ,'g')
    tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
end

data1.timestamps = data.timestamps(indStart:indEnd);
for j = 1:length(tracks),
    curTrack = tracks{j};
    eval(['thistrack = data.',curTrack,'.signal;']);
    eval(['data1.',curTrack,'.signal = data.',curTrack,'.signal(indStart:indEnd);']);
end

end
