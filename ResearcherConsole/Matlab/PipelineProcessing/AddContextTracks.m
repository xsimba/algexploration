function AddContextTracks(c, sessionList, contextList)

assert(numel(sessionList)==numel(contextList),'SessionList and Context List must contain the same number of elements');

global SIMBA_COMMON_DIR

simba_common_directory = fullfile(SIMBA_COMMON_DIR, 'data\Matlab');

addpath(genpath(simba_common_directory));

for i = 1:length(sessionList),
    
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
        %input = data;
            
    end
    
    
    load(metricsFilename)
    
    % NOTE: THIS IS DUE TO WHAT WRITTEN IN THE BP FUNCTION
    data.context.posture{1} = contextList{i};
    
    save(metricsFilename, 'data');

end

