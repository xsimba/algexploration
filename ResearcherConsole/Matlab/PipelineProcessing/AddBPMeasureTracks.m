function AddBPMeasureTracks(c, sessionList, bpMeasureList)

assert(numel(sessionList)==numel(bpMeasureList),'SessionList and Measure List must contain the same number of elements');

global SIMBA_COMMON_DIR

simba_common_directory = fullfile(SIMBA_COMMON_DIR, 'data\Matlab');

addpath(genpath(simba_common_directory));

for i = 1:length(sessionList),
    
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
        %input = data;
            
    end
    
    
    load(metricsFilename)
    
    sbpref = interp1(bpMeasureList{i}.timestamps,bpMeasureList{i}.sbp,data.timestamps);
    dbpref = interp1(bpMeasureList{i}.timestamps,bpMeasureList{i}.dbp,data.timestamps);
   
    data.bp.sbpref = sbpref;
    data.bp.dbpref = dbpref;
    
    save(metricsFilename, 'data');

end

