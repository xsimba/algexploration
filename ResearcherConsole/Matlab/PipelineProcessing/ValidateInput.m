function timeStep = ValidateInput(c, sessionList, version)
%
% validate input data
%

if (~exist('version', 'var')),
    version = '3v0';
end

if (length(version) > 5 && strcmp(version(end-5:end), 'debug')),
    debugSchema = 1;
else
    debugSchema = 0;
end


%%
% loop over sessions
%
hasData = 1;
for i = 1:length(sessionList),
    dataFilename = setV0MatSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(dataFilename);
    
    disp(['processing ', sessionList{i}]);
    try
        if (debugSchema == 0),
     
            timeStep = diff(data.timestamps);
            timeStep = timeStep - timeStep(1);
            timeStep = (timeStep < 0.1);
            assert (all(timeStep));
    else
        
        tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', ...
            'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h', 'acc.x', 'acc.y', ...
            'acc.z'};
        
        for j = 1:length(tracks),
            curTrack = tracks{j};
            eval(['hasData = exists(''data.',curTrack,'.signal'',''var'');']);
            if (hasData),
                eval(['time = data.',curTrack,'.timestamps;']);
                timeStep = diff(time);
                timeStep = timeStep - timeStep(1);
                timeStep = (timeStep < 0.1);
                assert (all(timeStep));
            end
        end
        
        end
    catch err
        display('There are gaps in the data');
    end
    
end

end

