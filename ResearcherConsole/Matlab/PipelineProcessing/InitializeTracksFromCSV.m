function InitializeTracksFromCSV(c, sessionList, version, fromUnix, unixTime)

if ~exist('version', 'var')
    version = '4v2';
    debug_version = '4v2-debug';
end

if strcmp(version, '3v1')
    debug_version = '3v1-debug';
end

if ~exist('fromUnix', 'var')
    fromUnix = 0;
end

if ~exist('unixTime','var')
    unixTime = 'ON';
end


for i = 1:length(sessionList),
    csvFilename = setWebGuiSessionData(c, sessionList{i});
    matFilename = setV0MatSessionData(c, sessionList{i});

    if (fromUnix == 1),
        csvFilename = convertSSICpathToUnix(csvFilename);
        matFilename = convertSSICpathToUnix(matFilename);
    end

    
    % load data into workspace in 3v1 format
    srcdata = getSimbandCsv(csvFilename, version, 0, unixTime);
    srcdata.fs = 128;
    debugdata = getSimbandCsv(csvFilename, debug_version, 0, unixTime);
    debugdata.fs = 128;
    %
    if strcmp(version, '3v1') || strcmp(version,'4v2'),
        data = srcdata;
        save (matFilename, 'data');
    end
    if strcmp(version, '3v1-debug') || strcmp(version, '4v2-debug'),
        data = debugdata;
        save(matFilename, 'data');
    end
    
    srctimestamps = srcdata.timestamps;
    signalcount = 0;
    existingsignals = {};
    debugsignals = {'ppg','ecg','acc'};
    
    if isfield(debugdata,'ppg'),
        tracks = {'a','b','c','d','e','f','g','h'};
        for i = 1:numel(tracks),
            if isfield(debugdata.ppg,tracks{i}),
                signal = strcat('debugdata.ppg.',tracks{i});
                if isfield(eval(signal), 'timestamps'),
                    signalcount = signalcount + 1;
                    existingsignals{signalcount} = signal;
                end
            end
        end
    end
    
    if isfield(debugdata,'ecg'),
        if isfield(debugdata.ecg,'timestamps'),
            signalcount = signalcount + 1;
            existingsignals{signalcount} = ('debugdata.ecg');
        end
    end
    
    if isfield(debugdata,'acc'),
        tracks = {'x','y','z'};
        for i = 1:numel(tracks),
            if isfield(debugdata.acc,tracks{i}),
                signal = strcat('debugdata.acc.',tracks{i});
                if isfield(eval(signal), 'timestamps'),
                    signalcount = signalcount + 1;
                    existingsignals{signalcount} = signal;
                end
            end
        end
    end
    
    for i = 1:numel(existingsignals),
        debugtimestamps = strcat(existingsignals{i},'.timestamps');
        if length(srctimestamps) ~= length(eval(debugtimestamps)),
            display('Warning: All data may not be downloaded in the time aligned data structure. Download data using debug schema in order to download all the data');
            break;
        end
    end

end

