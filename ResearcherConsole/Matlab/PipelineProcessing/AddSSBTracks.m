function AddSSBTracks(c, sessionList, ssbList)

assert(numel(sessionList)==numel(ssbList),'sessionList and SSB List must contain the same number of elements');

global SIMBA_COMMON_DIR

simba_common_directory = fullfile(SIMBA_COMMON_DIR, 'data\Matlab');

addpath(genpath(simba_common_directory));

for i = 1:length(sessionList),
    dataFilename = setV0MatSessionData(c, sessionList{i});
    metricsFilename = setMetricsSessionData(c, sessionList{i});
   % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
        %input = data;
            
    end
    
    
    data.ssb = ssbList{i};
    
    save(metricsFilename, 'data');

end

