

function AddTdeInterbeatTracks_simu(c, sessionList)
%
% add interbeattime tracks
%
%
% may want to add some input validation
%
% mask_time = 10;
%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    dataFilename = setV0MatSessionData(c, sessionList{i});
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
    end
    
    disp(['Running AddTdeInterbeatTracks() on ', sessionList{i}]);
    
    tracks = { 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg, 'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
        %         eval(['track = data.',curTrack,'.bd;']);
        %         hasfield = isfield(track, {'upstroke'});
        %         if hasfield,
        channel=j;
        options = simset('SrcWorkspace','current');
        %[time, ~, HR] = sim('InstFreqHR', [data.timestamps(1), data.timestamps(end)], options);
%         sim('InstFreqHR_derv', [data.timestamps(1), data.timestamps(end)], options);
        sim('tde_ibi', [data.timestamps(1), data.timestamps(end)], options);
        
        % ****************** Signal Processing ******************************
        %
        % 1. load signal
        % 2. apply low pass
        % 3. take first derivative (equivalent to high pass filter)?
        % 4. Calculate crossing locations from TDE
        % 5. Compute IBI
        
%         eval(['signal = data.',curTrack,'.signal;']);
%         InputStream_PPG = filter(Hd, signal);
        
%         InputStream_PPG=InputStream_PPG(mask_time*128:end);
        
        %
        % process filtered points and compute ibi's
        %

        %     time_delay = time_delay+1;
        
        %     for debugging
        %     locations2 = find(dev_sig(1:end-time_delay)<=dev_sig(time_delay:end-1) & dev_sig(2:end-time_delay+1)>dev_sig(time_delay+1:end));
        %
        % prepare output
        %
        %    timestamps = [881/128+locations(2:end)/128];
        timestamps = simout.Time(find(simout.Data(:,2)==1));
        ibi_out    = double(simout.Data(find(simout.Data(:,2)==1),1))/128;
        
        %
        % mask out filter transient
        %
        %         mask_time = find(timestamps<=5);
        %         ibi_out(1:(mask_time)) = 0;
        
        
        eval(['data.' , curTrack, '.tde_ibi.timestamps = timestamps;']);
        eval(['data.' , curTrack, '.tde_ibi.ibi = ibi_out;']);
        
%         plot(timestamps,60./ibi_out,'.')
%         pause;close all;
        
        %         end
    end
    
    %%
    %    if (exist(metricsFilename, 'file')),
    save (metricsFilename, 'data');
    %     else
    %         load(dataFilename);
    %         save (dataFilename, 'data');
    %    end
    %     save (metricsFilename, 'data');
    
end

end
