function AddTdeInterbeatTracks(c, sessionList)
%
% add interbeattime tracks
%
%
% may want to add some input validation 
%
mask_time = 10;
%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    dataFilename = setV0MatSessionData(c, sessionList{i});
    % load data into workspace in v0 format
   if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
   end
    
    disp(['Running AddTdeInterbeatTracks() on ', sessionList{i}]);

    tracks = { 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg, 'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
        %         eval(['track = data.',curTrack,'.bd;']);
        %         hasfield = isfield(track, {'upstroke'});
        %         if hasfield,
        
        
        % ******************** Filter Design*******************************
        N     = 10;   % Order
        Fstop = 3.5;    % Stopband Frequency
        Astop = 60;   % Stopband Attenuation (dB)
        Fs    = 128;  % Sampling Frequency
        n_ma  = 5;
        h     = fdesign.lowpass('n,fst,ast', N, Fstop, Astop, Fs);
        Hd    = design(h, 'cheby2');
        
        
        % ****************** Signal Processing ******************************
        %
        % 1. load signal
        % 2. apply low pass
        % 3. take first derivative (equivalent to high pass filter)?
        % 4. Calculate crossing locations from TDE
        % 5. Compute IBI
        
        eval(['signal = data.',curTrack,'.signal;']);       
        InputStream_PPG = filter(Hd, signal);
        
        InputStream_PPG=InputStream_PPG(mask_time*128:end);
        
        %
        % process filtered points and compute ibi's
        %
        time_delay = 50;
        %    cross = zeros(size(dev_sig));
        locations = [];
        ibi = [];
        firstCross = 0;
        for i = 2:(length(InputStream_PPG)),
            %
            % 3. take first derviative
            %
            dev_sig(i)=InputStream_PPG(i) - InputStream_PPG(i-1);
            
            if (i > time_delay+1),
                %
                % 4. Calculate crossing locations from TDE
                %
                crossFlag = dev_sig(i-time_delay-1)   <= dev_sig(i-1) && ...
                    dev_sig(i-time_delay) > dev_sig(i);
                if (crossFlag),
                    locations = [locations i];
                    %
                    % 5. compute ibi
                    %
                    if (firstCross),
                        l = length(locations);
                        ibi   = [ibi (locations(l)-locations(l-1))];
                        
                    end
                    firstCross = 1;
                end
            end
        end
        
        %     time_delay = time_delay+1;
        
        %     for debugging
        %     locations2 = find(dev_sig(1:end-time_delay)<=dev_sig(time_delay:end-1) & dev_sig(2:end-time_delay+1)>dev_sig(time_delay+1:end));
        %
        % prepare output
        %
        %    timestamps = [881/128+locations(2:end)/128];
        timestamps = [locations(2:end)/128+mask_time];
        ibi_out = double(ibi/128);
        
        %
        % mask out filter transient
        %
%         mask_time = find(timestamps<=5);
%         ibi_out(1:(mask_time)) = 0;
        
        eval(['data.' , curTrack, '.tde_ibi.timestamps = timestamps;']);
        eval(['data.' , curTrack, '.tde_ibi.ibi = ibi_out;']);
        
  

%         end
    end

    %%
%    if (exist(metricsFilename, 'file')),
        save (metricsFilename, 'data');
%     else
%         load(dataFilename);
%         save (dataFilename, 'data');
%    end
%     save (metricsFilename, 'data');

end

end
