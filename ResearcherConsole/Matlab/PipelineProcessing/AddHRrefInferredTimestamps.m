function AddHRrefInferredTimestamps(c, sessionList)

for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(metricsFilename);
 
    disp(['Running AddHRrefInferredTimestamps() on ', sessionList{i}]);
        
    % Polar Chest Strap - Assumes 1 Hz sample recording rate
    timestamps = 0:(length(data.HR_ref.signal) - 1);
    data.HR_ref.inferredTimestamps = timestamps(:);
   
    save (metricsFilename, 'data');

end

end