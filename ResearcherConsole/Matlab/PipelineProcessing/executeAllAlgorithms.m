% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : executeAllAlgorithms.m
%  Content    : Execute all algorithms in a row
%  Version    : 2.0
%  Package    : SIMBA
%  Created by : E.C. Wentink (eva.wentink@imec-nl.nl)
%  Date       : 28-08-2014
%  Modification and Version History: 
%  | Developer  | Version |    Date    |     Changes      |
%  | EC WEntink | 0.9     | 23/09/3014 | Now function, new BP, PPG flipping
%  | EC WEntink | 0.91    | 23/09/3014 | flipping to start of script,added note
%  | AM Tautan  | 0.92    | 20/10/2014 | removed Acc conversion to Gs 
%  | F.Beutel   |   1.2   |  15-12-14  | compatibility with old and incomplete datasets |
%  | F.Beutel   |   2.0   |  15-04-15  | Renamed, Independent of relative paths now, Moved example data to separate folder |
%  | E.Hermeling|   2.1   |  24-06-15  | Made compatible with feature extraction (replaced PAT)               |
%  | F.Beutel   |   3.0   |  04-08-15  | Added Yelei's TDE BD |


%NOTE: remove conversion to G from here if the driver outputs G's else
%CI_raw will fail!!

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------
% Folder: put this script into a folder that has as subfolders all the
% separate functions "SRC" "BD" "CI/CI_I" CI/CI_II" "PAT" and "BP"
% It will go to each separate folder for the different algorithms, so each
% algo can have its own update and just be plugged into the folder without
% having to change the other algos.
% Inputs: 
% folder_name: Specify the foldername with the data you wisch to test below
% kanalen: The number of ppg channels that are available in the dataset (to
% be automized)
% output is the "output: strct with all the data and relevant parameters
% displayed will be the PAT and its channel and the BP beloning to it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [data]=executeAllAlgorithms(data, flip, run_TDE)

%% prepare Acc data
if isfield(data,'acc')
    if isfield(data.acc,'signal')
        data.acc.All = [data.acc.signal.x,data.acc.signal.y,data.acc.signal.z];
        data.acc.x.signal=data.acc.signal.x;
        data.acc.y.signal=data.acc.signal.y;
        data.acc.z.signal=data.acc.signal.z;
        clear data.acc.signal.x data.acc.signal.y data.acc.signal.z
    else
        if ~isfield(data.acc,'x') || ~isfield(data.acc,'y') || ~isfield(data.acc,'z')
            data.acc.x.signal = zeros(1,length(data.timestamps));
            data.acc.y.signal = zeros(1,length(data.timestamps));
            data.acc.z.signal = zeros(1,length(data.timestamps));
            disp('No accelerometer data available')
        end
        data.acc.All = [data.acc.x.signal;data.acc.y.signal;data.acc.z.signal];
    end
else
    disp('No accelerometer data available --> No CI raw available')
end

%% run CI_I
%cd('..\ConfidenceIndicator\CI\CI_I\');
disp('Starting CI for raw signals')

    if isfield(data,'acc')
        data.acc.All(4,:) = sqrt(sum(data.acc.All.^2, 1));
        if mean(data.acc.All(4,:))<2 
            data.acc.All = (data.acc.All);
        else% Conversion to G
            data.acc.All = (data.acc.All./(2^14))*4;   % Conversion to G
        end
    end



% Run CI on raw data
if isfield(data,'acc')
    [data] = CI_raw(data,'ecg');
    if isfield(data.ppg ,'a')
        [data] = CI_raw(data,'a');
    end
    if isfield(data.ppg ,'b')
        [data] = CI_raw(data,'b');
    end
    if isfield(data.ppg ,'c')
        [data] = CI_raw(data,'c');
    end
    if isfield(data.ppg ,'d')
        if isfield(data.ppg.d ,'signal')
        [data] = CI_raw(data,'d');
        end
    end
    if isfield(data.ppg ,'e')
        if isfield(data.ppg.e,'visual')
           [data] = CIrawII_e(data,'e');
        else
            Params  = importdata('PPGvisFilt128Hz.csv'); % Load ECG CWT coefficients
            data.ppg.e.visual.signal = filter(Params,1,data.ppg.e.signal); 
            [data] = CIrawII_e(data,'e');
        end

    end
    if isfield(data.ppg ,'f')
        [data] = CI_raw(data,'f');
    end
    if isfield(data.ppg ,'g')
        [data] = CI_raw(data,'g');
    end
    if isfield(data.ppg ,'h')
        [data] = CI_raw(data,'h');
    end
end


%% flip PPG
if flip ==1
    disp('Starting PPG inversion')
    [data] = PPGinv(data);
end

%% Beat detections in matlab
%cd('.\BeatDetection\');
disp('Starting Beat detection')
 
[data]  = BDstruc(data,'ecg');
[data]  = CI_BD(data,'ecg');
% if isfield(data.ppg ,'a')
%     [data] = BDstruc(data,'ppg.a');
% end
% if isfield(data.ppg ,'b')
%     [data] = BDstruc(data,'ppg.b');
% end
% if isfield(data.ppg ,'c')
%     [data] = BDstruc(data,'ppg.c');
% end
if isfield(data.ppg ,'d')
    if isfield(data.ppg.d ,'signal')
    [data] = BDstruc(data,'ppg.d');
    [data] = FiltAmp(data,'ppg.d', 7); %changed filter bandwith to 7Hz
    [data] = CI_BD(data,'ppg.d');
    end
end
if isfield(data.ppg ,'e')
    [data] = BDstruc(data,'ppg.e', 7); %changed filter bandwith to 7Hz
    [data] = FiltAmp(data,'ppg.e', 7); %changed filter bandwith to 7Hz
    [data] = CI_BD(data,'ppg.e');
end
% if isfield(data.ppg ,'f')
%     [data] = BDstruc(data,'ppg.f');
% end
% if isfield(data.ppg ,'g')
%     [data] = BDstruc(data,'ppg.g');
% end
% if isfield(data.ppg ,'h')
%     [data] = BDstruc(data,'ppg.h');
% end

%% Beat detection from TDE
disp('Starting TDE Beat Detection')
if isfield(data.ppg ,'e') && run_TDE
    [data] = runTDE_HR_BD_debug_Fabian(data)
end

%% run (pseudo) SNR calculations in matlabif needed
% works best for PPG when the filtering of the BD is set to 5 Hz.
% cd('.\SNR\');
% disp('Starting SNR calculation')
% 
% [data]=SNR_checker(data,'ecg');
% if isfield(data.ppg ,'a')
%  [data]=SNR_checker(data,'ppg.a');
% end
% if isfield(data.ppg ,'b')
%     [data]=SNR_checker(data,'ppg.b');
% end
% if isfield(data.ppg ,'c')
%     [data]=SNR_checker(data,'ppg.c');
% end
% if isfield(data.ppg ,'d')
%     [data]=SNR_checker(data,'ppg.d');
% end
% if isfield(data.ppg ,'e')
%    [data]=SNR_checker(data,'ppg.e');
% end
% if isfield(data.ppg ,'f')
% [data]=SNR_checker(data,'ppg.f');
% end
% if isfield(data.ppg ,'g')
% [data]=SNR_checker(data,'ppg.g');
% end
% if isfield(data.ppg ,'h')
% [data]=SNR_checker(data,'ppg.h');
% end
% cd('..\');





%% run CI_II 
% run the CI on the beats, so for each beat you will get a CI 

% disabled for PAT computation
%     cd('.\ConfidenceIndicator\CI\CI_II\');
%     disp('Starting CI_II')
%     [data] = CI_II(data,'ecg');
%     [data] = CI_II(data,'a');
%     [data] = CI_II(data,'b');
%     [data] = CI_II(data,'c');
%     [data] = CI_II(data,'d');
%     if isfield(data.ppg ,'e')
%     [data] = CI_II(data,'e');
%     [data] = CI_II(data,'f');
%     end
%     if isfield(data.ppg ,'g')
%     [data] = CI_II(data,'g');
%     [data] = CI_II(data,'h');
%     end
%     cd('..\..\..\');

% cd('.\Plot\'); 
% PlotCI(data)
% cd('..\');
% close all;

%% run Feature Extraction
disp('Starting Feature Extraction')

%if isfield(data.ppg ,'a')
%    [data] = featureExtraction(data,'ppg.a');
%    [data] = PPG_HR_Extraction(data,'ppg.a');
%end
%if isfield(data.ppg ,'b')
%    [data] = featureExtraction(data,'ppg.b');
%    [data] = PPG_HR_Extraction(data,'ppg.b');
%end
%if isfield(data.ppg ,'c')
%    [data] = featureExtraction(data,'ppg.c');
%    [data] = PPG_HR_Extraction(data,'ppg.c');
%end
%if isfield(data.ppg ,'d')
%    [data] = featureExtraction(data,'ppg.d');
%    [data] = PPG_HR_Extraction(data,'ppg.d');
%end

if isfield(data.ppg ,'e')
    [data] = featureExtraction(data,'ppg.e');
    [data] = PPG_HR_Extraction(data,'ppg.e');
end
%if isfield(data.ppg ,'f')
%    [data] = featureExtraction(data,'ppg.f');
%    [data] = PPG_HR_Extraction(data,'ppg.f');
%end
%if isfield(data.ppg ,'g')
%    [data] = featureExtraction(data,'ppg.g');
%    [data] = PPG_HR_Extraction(data,'ppg.g');
%end
%if isfield(data.ppg ,'h')
%    [data] = featureExtraction(data,'ppg.h');
%    [data] = PPG_HR_Extraction(data,'ppg.h');
%end

%% run BP
disp('Starting BP calculation')

%database mode
    [data] = BP(data, 'e', 'database', 0); %unspecified database mode --> for import of consistency! (0 for use_reference)
    %[data] = BP(data, 'e', 'database', [1], {'M'}, [1]); %specified database mode --> for tracking of consistency! Here: day 1, morning, first trial as reference (i.e. very first one)
    %[data] = BP(data, 'e', 'file', -99, 1); %specified file mode; Here for tracker validation where -99 takes cuff values entered in band
    %[data] = BP(data, 'e', 'file', 1, 1); %specified file mode; Here for large scale data import --> no tracking results computed
end
