function data = AddFreqInterbeatTracks_v3(c, sessionList, fromUnix)

if ~exist('fromUnix'),
    fromUnix = 0;
end

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    if (fromUnix == 1),
        metricsFilename = convertSSICpathToUnix(metricsFilename);        
    end
        
    % load data into workspace in v0 format
    load(metricsFilename);
    
    disp(['processing ', sessionList{i}]);

%    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    
    Params.Fs = 128;
    
%     output.acc.signal.x = data.acc.x.signal;
%     output.acc.signal.y = data.acc.y.signal;
%     output.acc.signal.z = data.acc.z.signal;
%     
%     % Default scale and offset settings for Bosch BMA280 (also dependent on configuration settings)
%     AccScaleOffset.xsc = 1/4096.0; AccScaleOffset.xof = 0.0;
%     AccScaleOffset.ysc = 1/4096.0; AccScaleOffset.yof = 0.0;
%     AccScaleOffset.zsc = 1/4096.0; AccScaleOffset.zof = 0.0;
%     
%     cax = output.acc.signal.x*AccScaleOffset.xsc + AccScaleOffset.xof; % No calibration: apply current scales and offsets
%     cay = output.acc.signal.y*AccScaleOffset.ysc + AccScaleOffset.yof;
%     caz = output.acc.signal.z*AccScaleOffset.zsc + AccScaleOffset.zof;
%     AccUD = 0;
%     
%     acc(:,1) = cax;
%     acc(:,2) = cay;
%     acc(:,3) = caz;
%     cam = sqrt(cax.^2+cay.^2+caz.^2); % Magnitude
%     
%     InputStream_ACC_raw = cay; % Select ACC input
    
    % % Rough calibration               MinX,  MaxX,  MinY,  MaxY,  MinZ, MaxZ,  PlotFlag
    [AccOut] = AccGainOffset(data.acc, -3100, +4400, -3350, +4050, -2400, +5150, 0); % NavyaBand Calibration
%     [AccOut] = AccGainOffset(data.acc, -4096, +4096, -4096, +4096, -4096, +4096, 0); % Default Calibration for g = 4096
    % [AccOut] = AccGainOffset(data.acc,    -1,    +1,    -1,    +1,    -1,    +1, 0); % Default Calibration for g = 1
    %
    InputStream_ACC_raw = AccOut.y.signal; % Select ACC channel x, y z or m
    ActionDetector_ACC_raw = AccOut.m.signal;
    
    %% Examine input channels
    h = fdesign.highpass('fst,fp,ast,ap', 0.250, 0.500, 50.00, 0.100, Params.Fs);
    HdSOS = design(h, 'ellip','MatchExactly', 'both');
    Fmax = 12;
    
    if (0)
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,cax),1024,Fmax,Params.Fs,'acc X', 2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,cay),1024,Fmax,Params.Fs,'acc Y', 2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,caz),1024,Fmax,Params.Fs,'acc Z', 2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,cam),1024,Fmax,Params.Fs,'acc mag', 2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.a.signal),1024,Fmax,Params.Fs,'ppg a',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.b.signal),1024,Fmax,Params.Fs,'ppg b',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.c.signal),1024,Fmax,Params.Fs,'ppg c',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.d.signal),1024,Fmax,Params.Fs,'ppg d',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.e.signal),1024,Fmax,Params.Fs,'ppg e',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.f.signal),1024,Fmax,Params.Fs,'ppg f',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.g.signal),1024,Fmax,Params.Fs,'ppg g',2);
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.h.signal),1024,Fmax,Params.Fs,'ppg h',2);
    end
    
    
    % Filter design
    if (1)
        % Wavelet filter                     Fpass, Fstop, Apass, Astop, Fs
        h = fdesign.lowpass('fp,fst,ap,ast',  8.00, 10.75, 0.100, 61.00, Params.Fs); % Tune to get integer group delay
        Hd = design(h, 'equiripple');
        kd = [+1 0 -1]; dd = 1.0; % Make wavelet combining LPF and diff...
        Hd.Numerator = conv(Hd.Numerator,kd); % Convolve LPF with differentiation kernel
        Params.hpf = Hd;
        gd = grpdelay(Params.hpf); Params.hpf_gd = gd(1);
        disp(['Filter delay = ',num2str(Params.hpf_gd/Params.Fs),' s (',num2str(Params.hpf_gd),' samples)']);
    end
    
    % q = abs(fft([Params.hpf zeros(1,32768-length(Params.hpf))]));q = max(20*log10(q/max(q)),-100);
    % figure;plot((0:16384)*Params.Fs/32768,q(1:16385));xlabel('Frequency (Hz)');ylabel('Normalised magnitude (dB)');title('Filter response');
    
    % Align ACC data to PPG
    InputStream_ACC = [InputStream_ACC_raw(1)*ones(1,Params.hpf_gd) InputStream_ACC_raw];
    
    ActionDetector_ACC = [ActionDetector_ACC_raw(1)*ones(1,Params.hpf_gd) ActionDetector_ACC_raw]; 

    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg ,'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
            eval(['InputStream_PPG_raw = data.',curTrack,'.signal;']);
            
            % Handle initial conditions
            InputStream_PPG = filter(Params.hpf,[InputStream_PPG_raw(1)*ones(1,2*Params.hpf_gd) InputStream_PPG_raw]); % Perform filter (reverse coefficient order for C impl.)
            InputStream_PPG = InputStream_PPG(2*Params.hpf_gd+1:end);
            
            [CI_freq, ibi_freq, amp_freq, timestep,index_seg_end, num_pks,action_flag] = generate_ibi_freq_v3(InputStream_PPG, InputStream_ACC, ActionDetector_ACC);

            if (j==0) % Zero to disable, 1 to 8 = a to g
                figure;hold on;
                plot(InputStream_PPG_raw-mean(InputStream_PPG_raw),'b');plot(InputStream_PPG,'r');
                plot((InputStream_ACC-mean(InputStream_ACC))*1,'g');title('Filter in/out');legend('Raw PPG','Filt PPG','ACC');
                
                SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,InputStream_PPG),1024,Fmax,Params.Fs,'ppg filt',2);
            end
       
            eval(['data.',curTrack,'.freq_HR.CI_freq = CI_freq;']);
            eval(['data.',curTrack,'.freq_HR.ibi_freq = ibi_freq;']);
            eval(['data.',curTrack,'.freq_HR.amp_freq = amp_freq;']);
            eval(['data.',curTrack,'.freq_HR.timestep = timestep;']);
            eval(['data.',curTrack,'.freq_HR.time = data.timestamps(index_seg_end);']);
            eval(['data.',curTrack,'.freq_HR.num_pks = num_pks;']);
            eval(['data.',curTrack,'.freq_HR.action_flag = action_flag;']);
    end

    %%

    save (metricsFilename, 'data');

end

end
