function data = AddAcfInterbeatTracks(c, sessionList, filterFlag)
%
% add interbeattime tracks
%

if (~exist('filterFlag')),
    filterFlag = 0;
end

%
% may want to add some input validation 
%

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(metricsFilename);
    
    disp(['processing ', sessionList{i}]);

%    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
       if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg ,'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
            eval(['thisPPG = data.',curTrack,'.signal;']);
            AcfInterbeat = generate_ibi_acf(thisPPG,filterFlag, data.fs);
            eval(['data.',curTrack,'.ibi_acf = AcfInterbeat;']);
    end

    %%

    save (metricsFilename, 'data');

end

end
