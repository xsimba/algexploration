function AddInstFreqHRTracks(c, sessionList)

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    dataFilename = setV0MatSessionData(c, sessionList{i});
    % load data into workspace in v0 format
   if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
   end
    
    disp(['Running AddInstFreqHRTracks() on ', sessionList{i}]);

    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg, 'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
%%Running motion detection on accel    
options = simset('SrcWorkspace','current');
sim('motionDetector_20141216', [data.timestamps(1), data.timestamps(end)], options);

data.motion_flag.timestamps = motionFlag.time;
data.motion_flag.signal = squeeze(motionFlag.signals.values);

    for j = 1:8,
        curTrack = tracks{j};
        channel = j;
         
    %startime = data.timestamps(1);
    %endtime = data.timestamps(end);
    options = simset('SrcWorkspace','current');
    %[time, ~, HR] = sim('InstFreqHR', [data.timestamps(1), data.timestamps(end)], options);
%         sim('InstFreqHR_derv', [data.timestamps(1), data.timestamps(end)], options);
    sim('InstFreqHR_currentCcodeImpl', [data.timestamps(1), data.timestamps(end)], options);

    ibi_hilbert = squeeze(60./HRout.signals.values);
    eval(['data.',curTrack,'.ibi_hilbert = ibi_hilbert;']);
    eval(['data.',curTrack,'.hilbert_timestamps = HRout.time;']);
    end
    
    
    save (metricsFilename, 'data');
    %
end

end
