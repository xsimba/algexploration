function data = AddBiosemTracks(c, sessionList, metric, fromUnix)
%
% Adds tracks associated with Biosemantic HR.  Includes some of the
% algorithm + data fusion logic in here -- todo: move to algo area.
%
% Asif Khalak
% asif.khalak@samsung.com
% 15 Sept 2014

if ~exist('fromUnix', 'var'),
    fromUnix = 0;
end


%global RC_MATLAB_DIR

timeGridInterval = 1.0;

%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.15;
bioLimits.rateLimitUp = 0.15;
bioLimits.windowSize = 10;
bioLimits.sigmaCutoff = 2.5;

if ~exist('metric', 'var'),
    metric = 'interbeat';
end;

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    dataFilename = setV0MatSessionData(c, sessionList{i});
    % load data into workspace in v0 format
   if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
   end
    % load data into workspace in v0 format
    
    if (fromUnix == 1),
        metricsFilename = convertSSICpathToUnix(metricsFilename);
    end

    
    
    disp(['Running AddBiosemTracks on ',sessionList{i}]);
    
    %
    % n.b. the compute modules must be *causal* for the algorithm
    % to work in real-time.  Not currently run-time optimized.
    %
%    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg ,'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};

        %
        % biosemantic filtering and hypothesis selection
        %
        if (strmatch(metric, 'ibi_freq2', 'exact')),
            data = computeBiosemFreqFilter(data, curTrack, bioLimits);
        else
            data = computeBiosemBeatFilter(data, curTrack, bioLimits, metric);
        end
        
        %
        % compute final statistics (output is in biosemStatMed) 
        %
        data = computeBiosemStaticStat(data, curTrack, 'biosemInterbeats');
        data = computeBiosemQuality(data, curTrack);
    end
    
    %
    % save metrics output
    %
    save (metricsFilename, 'data');
    
end

end


