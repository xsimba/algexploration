function AddTracksUnix(c, sessionList, firmwareversion)

if (~exist('firmwareversion','var')),
    firmwareversion = 'v0.23.0';
end


AddTracks(c, sessionList, firmwareversion, 1)

end

