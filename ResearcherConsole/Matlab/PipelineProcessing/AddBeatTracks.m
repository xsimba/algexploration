function AddBeatTracks(c, sessionList, freshWrite)
%
% this function creates a new metrics file, and adds the beat tracks using BD v3 for ECG and v2 for PPG
%

global RC_MATLAB_DIR

%
% default don't disturb other tracks
%
if ~exist('freshWrite', 'var'),
    freshWrite = 0;
end

for i = 1:length(sessionList),
    dataFilename = setV0MatSessionData(c, sessionList{i});
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file') && ~freshWrite),
        load(metricsFilename);
    else
        load(dataFilename);
    end

    %% Run beat detector and add beats to each stream    

    FreqTS = 32768;                                 % Timestamp clock frequency

    addpath(genpath([RC_MATLAB_DIR, '\Algorithms\BD_ECG_v3']));
    if isfield(data, 'ecg'),
        BeatsTimestamps = BD_ECG_v3_formatv0( data,'ecg');
        ActBeatsTimestamps = BeatsTimestamps/FreqTS+data.timestamps(1);
        beatVals=interp1(data.timestamps, data.ecg.signal, ActBeatsTimestamps);
    end
    rmpath(genpath([RC_MATLAB_DIR, '\Algorithms\BD_ECG_v3']));
    data.ecg.beats = [ActBeatsTimestamps(:) beatVals(:)]';

    %
    % for some reason PPG beat timestamps are in sec vs. ecg in clock cycles
    %
    addpath(genpath([RC_MATLAB_DIR, '\Algorithms\BD_PPG_v2']));
    if isfield(data, 'ppg'),
        if isfield(data.ppg, 'a'),
            BeatsTimestamps = BD_PPG_v2_formatv0( data.ppg,'a');
            ActBeatsTimestamps = BeatsTimestamps+data.timestamps(1);
            if(length(data.timestamps) == length(data.ppg.a.signal)),
                beatVals=interp1(data.timestamps, data.ppg.a.signal, ActBeatsTimestamps);
                data.ppg.a.beats = [ActBeatsTimestamps(:) beatVals(:)]';
            end
        end

        if isfield(data.ppg, 'b'),
            BeatsTimestamps = BD_PPG_v2_formatv0( data.ppg,'b');
            ActBeatsTimestamps = BeatsTimestamps+data.timestamps(1);
            if(length(data.timestamps) == length(data.ppg.b.signal)),
                beatVals=interp1(data.timestamps, data.ppg.b.signal, ActBeatsTimestamps);
                data.ppg.b.beats = [ActBeatsTimestamps(:) beatVals(:)]';
            end
        end

        if isfield(data.ppg, 'c'),
            BeatsTimestamps = BD_PPG_v2_formatv0( data.ppg,'c');
            ActBeatsTimestamps = BeatsTimestamps+data.timestamps(1);
            if(length(data.timestamps)== length(data.ppg.c.signal)),
                beatVals=interp1(data.timestamps, data.ppg.c.signal, ActBeatsTimestamps);
                data.ppg.c.beats = [ActBeatsTimestamps(:) beatVals(:)]';
            end
        end

        if isfield(data.ppg, 'd'),
            BeatsTimestamps = BD_PPG_v2_formatv0( data.ppg,'d');
            ActBeatsTimestamps = BeatsTimestamps+data.timestamps(1);
            if(length(data.timestamps) == length(data.ppg.d.signal)),
                beatVals=interp1(data.timestamps, data.ppg.d.signal, ActBeatsTimestamps);
                data.ppg.d.beats = [ActBeatsTimestamps(:) beatVals(:)]';
            end
        end
    end
    rmpath(genpath([RC_MATLAB_DIR, '\Algorithms\BD_PPG_v2']));
    
    clear ActBeatsTimestamps beatVals BeatsTimestamps
    
    %
    % overwrite the metrics file with this new one having the beats
    %
    if ~isfield(data, 'fs'),
        data.fs = 128;  % hard code 128Hz if not already set.
    end
    save(metricsFilename, 'data');
    
end