function data = AddBiosemTracksNoSave(c, sessionList)
%
% this function creates a new metrics file, and adds the beat tracks using BD v3 for ECG and v2 for PPG
%

%global RC_MATLAB_DIR

timeGridInterval = 1.0;

%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.3;
bioLimits.rateLimitUp = 0.3;
bioLimits.windowSize = 5;

metric = 'ibi_acf';

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(metricsFilename);
    
    disp(['Processing ',sessionList{i}]);
    
    %
    % n.b. the compute modules must be *causal* for the algorithm
    % to work in real-time.  Not currently run-time optimized.
    %
    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg ,'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
        %
        % initialize statistics
        %
        data = computeBiosemStaticStat(data, curTrack, metric);
        
        %
        % biosemantic filtering and hypothesis selection
        %
        data = computeBiosemBeatFilter(data, curTrack, bioLimits, metric);
        data = computeBiosemQuality(data, curTrack);
        
        %
        % compute final statistics
        %
        data = computeBiosemStaticStat(data, curTrack, 'biosemInterbeats');
    end
    
    %
    % save metrics output
    %
    save (metricsFilename, 'data');
    
end

end

