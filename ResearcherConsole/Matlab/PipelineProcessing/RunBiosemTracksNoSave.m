function data = RunBiosemTracksNoSave(data, metric)
%
% this function creates a new metrics file, and adds the beat tracks using BD v3 for ECG and v2 for PPG
%

%global RC_MATLAB_DIR

timeGridInterval = 1.0;

%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.3;             % .15 or .30
bioLimits.rateLimitUp = 0.3;

algoParam.cvCutoff = 2.5;
algoParam.deltaT1   = 15;                  % reset time;
algoParam.deltaT2   = 25;                  % post-reset time;
algoParam.minSigma   = 4;                  % standard deviation threshold
algoParam.signalPowerCut = 0.1;
algoParam.windowSize = 5;
algoParam.initialHR = 70;
algoParam.diffusionConstant = 0.8;

bioLimits.windowSize = algoParam.windowSize; % for backwards compatibility

if ~exist('metric', 'var'),
    metric = 'interbeat';
end;


disp('Processing ');

%
% n.b. the compute modules must be *causal* for the algorithm
% to work in real-time.  Not currently run-time optimized.
%
%    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
if isfield(data.ppg ,'e')
    tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
end
if isfield(data.ppg ,'g')
    tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
end

for j = 1:length(tracks),
    curTrack = tracks{j};
    
    %
    % biosemantic filtering and hypothesis selection
    %
    
    if (strmatch(metric, 'ibi_freq2')),
        data = computeBiosemFreqFilter(data, curTrack, bioLimits, algoParam);
    else       
        data = computeBiosemBeatFilter(data, curTrack, bioLimits, metric);
    end
    
    %
    % compute final statistics (output is in biosemStatMed)
    %
    data = computeBiosemStaticStat(data, curTrack, 'biosemInterbeats');
    
    %
    % compute quality of biosemantic filter
    % 
    data = computeBiosemQuality(data, curTrack);

end

end

