function RMSErrors = AddHRPerformanceMetricsTracks(c, sessionList, timeRange)
% Heart Rate RMS Error Calculation

%%
% loop over sessions
%
RMSErrors = zeros(length(sessionList), 8);

for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    dataFilename = setV0MatSessionData(c, sessionList{i});
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
    end
    
    disp(['Adding HR Estimation Performance Metrics for ', sessionList{i}]);
    
    assert(isfield(data, 'HR_ref'), 'Error in AddHRPerfromanceMetricsTracks: HR reference signal required in session data struct.')
    
    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg, 'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    % Calc error for band output HR (regardless of PPG channel used,
    % etc., there is only one output)
    if isfield(data, 'band_hr')
        HRstruct = data.band_hr;
        HR_ref_interp = interp1(data.HR_ref.inferredTimestamps, data.HR_ref.signal, HRstruct.timestamps);
        HRstruct.HR_Error = HRstruct.signal - HR_ref_interp;
        
        goodIdx = find((HRstruct.timestamps > timeRange{i}(1)) && (HRstruct.timestamps < timeRange{i}(2)));
        HRstruct.HR_RMSError = norm(HRstruct.HR_Error(goodIdx))/sqrt(length(HRstruct.HR_Error(goodIdx)));
        
        data.band_hr.HR_Error = HRstruct.HR_Error;
        data.band_hr.HR_RMSError = HRstruct.HR_RMSError;
    end
    
    % Calc error for signal specific PPG channels
    for j = 1:8
        curTrack = tracks{j};
        channel = j;
             
        if isfield(eval(['data.', curTrack]), 'biosemStatMed') && isfield(eval(['data.', curTrack]), 'biosemTime')
            HRest_time = eval(['data.', curTrack, '.biosemTime']);
            HRest = eval(['data.', curTrack, '.biosemStatMed.muHR']);
            
            HR_ref_interp = interp1(data.HR_ref.inferredTimestamps, data.HR_ref.signal, HRest_time);
            
            HR_Error = HRest - HR_ref_interp;
            eval(['data.', curTrack, '.biosemStatMed.HR_Error = HR_Error;']);
            
            goodIdx = find((HRest_time > timeRange{i}(1)) & (HRest_time < timeRange{i}(2)) & ~isnan(HR_Error));
            HR_RMSError = norm(HR_Error(goodIdx))/sqrt(length(HR_Error(goodIdx)));
            eval(['data.', curTrack, '.biosemStatMed.HR_RMSError = HR_RMSError;']);
            
            disp(['   ', curTrack, ' HR RMS Error - ', num2str(HR_RMSError)]);
            
            RMSErrors(i,j) = HR_RMSError;
        end
        
    end
    
    save (metricsFilename, 'data');
    
end

