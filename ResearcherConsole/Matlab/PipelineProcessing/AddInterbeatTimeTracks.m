function AddInterbeatTimeTracks(c, sessionList)
%
% add interbeattime tracks
%

%
% may want to add some input validation 
%

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(metricsFilename);
    
    disp(['processing ', sessionList{i}]);

    tracks = { 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg, 'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    for j = 1:length(tracks),
        curTrack = tracks{j};
        eval(['track = data.',curTrack,'.bd;']);
        hasfield = isfield(track, {'upstroke'});
        if hasfield,
            eval(['thisData = data.',curTrack,'.bd.upstroke;']);
            [n,m] = size(thisData);
            Interbeat = zeros(n,m);
            %Interbeat(1,:) = thisData(1,1:end-1);
            %Interbeat(2,:) = diff(thisData(1,:));
            Interbeat = diff(thisData);
            eval(['data.',curTrack,'.ibi = Interbeat;']);
        end
    end

    %%

    save (metricsFilename, 'data');

end

end
