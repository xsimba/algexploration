function data = AddTracksNoSave(c, sessionList)

global SIMBA_COMMON_DIR

if ~exist('simba_common_dir_location', 'var'),
    simba_common_directory = fullfile(SIMBA_COMMON_DIR, 'data\Matlab\AllAlgorithms\AlIn');
else
    simba_common_directory = fullfile(simba_common_dir_location, 'data\Matlab\AllAlgorithms\AlIn');
end

addpath(genpath(simba_common_directory));


for i = 1:length(sessionList),
    dataFilename = setV0MatSessionData(c, sessionList{i});
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
    end
    
    output = AlIn(data);
    
%     % load data into workspace in v0 format
%     if  exist('data','var')
%         output=data;
%     end
% 
%     output.acc.All = [output.acc.x.signal;output.acc.y.signal;output.acc.z.signal];
%     input=output;
%     %% Run confidence indicator for all streams
%     %% run CI_I
%         
%     CI_dir = fullfile(simba_common_directory, '\CI\CI_I\');
%     addpath(genpath(CI_dir));
%     disp('Starting CI_I')
%     [output] = CI_I(input);
%     input=output;
%     close all;
%     
%     %% Beat detections in matlab
%     BD_dir = fullfile(simba_common_directory, '\BD\');
%     addpath(genpath(BD_dir));
%     disp('Starting Beat detection')
%     [output.ppg.beats_ts,output.ppg.DB]     = BD(output.ppg.signal,'ppg');
%     [output.ppg.a.beats_ts,output.ppg.a.DB] = BD(output.ppg.a.signal,'ppg');
%     [output.ppg.b.beats_ts,output.ppg.b.DB] = BD(output.ppg.b.signal,'ppg');
%     [output.ppg.c.beats_ts,output.ppg.c.DB] = BD(output.ppg.c.signal,'ppg');
%     [output.ppg.d.beats_ts,output.ppg.d.DB] = BD(output.ppg.d.signal,'ppg');
%     if isfield(output.ppg ,'e')
%         [output.ppg.e.beats_ts,output.ppg.e.DB] = BD(output.ppg.e.signal,'ppg');
%         [output.ppg.f.beats_ts,output.ppg.f.DB] = BD(output.ppg.f.signal,'ppg');
%     end
%     input=output;
%     
%     %% run CI_II
%     CI_II_dir = fullfile(simba_common_directory, '\CI\CI_II');
%     addpath(genpath(CI_II_dir));
%     disp('Starting CI_II')
%     [output] = CI_II(input);
%     input=output;
%     close all; 
%     
%     %
%     % calculate PAT
%     %
%     PAT_dir = fullfile(simba_common_directory, '\PAT\');
%     addpath(genpath(PAT_dir));
%     disp('Starting PAT calculation')
%     [output] = CalcPAT(input);
%     input = output;
%     %
%     
%     %% run BP
%     BP_dir = fullfile(simba_common_directory, '\BP\');
%     addpath(genpath(BP_dir));
%     disp('Starting BP calculation')
%     [output] = BP(input);
    
%
% packing data
%
% TODO: this is pretty unmaintainable.  create a loop for this.

%
% schema translation to Rosetta 
%

data = output;
data.ppg.a.beatAmp = output.ppg.a.DB.BeatAmp;
data.ppg.b.beatAmp = output.ppg.b.DB.BeatAmp;
data.ppg.c.beatAmp = output.ppg.c.DB.BeatAmp;
data.ppg.d.beatAmp = output.ppg.d.DB.BeatAmp;
if isfield(output.ppg ,'e')
    data.ppg.beatAmp = output.ppg.e.DB.BeatAmp;
    data.ppg.beatAmp = output.ppg.f.DB.BeatAmp;
end
    
%     data.ppg.CI_raw = output.ppg.CI_raw;
%     data.ppg.CI_times = output.ppg.CI_times;
%     data.ppg.LeadOn = output.ppg.lolo;
%     data.ppg.beats_ts = output.ppg.beats_ts;
%     data.ppg.beats = output.ppg.beats;
%     data.ppg.CI_beats = output.ppg.CI_beat;
%     data.ppg.a.beats_ts = output.ppg.a.beats_ts;
%     data.ppg.b.beats_ts = output.ppg.b.beats_ts;
%     data.ppg.c.beats_ts = output.ppg.c.beats_ts;
%     data.ppg.d.beats_ts = output.ppg.d.beats_ts;
%     if isfield(output.ppg ,'e')
%         data.ppg.e.beats_ts = output.ppg.e.beats_ts;
%         data.ppg.f.beats_ts = output.ppg.e.beats_ts;
%     end
%     
% 
%     data.ppg.CI_beats = output.ppg.CI_beat;
%     data.ppg.ibi = output.ppg.ibi;
%     data.ppg.a.ibi = output.ppg.a.ibi;
%     data.ppg.b.ibi = output.ppg.b.ibi;
%     data.ppg.c.ibi = output.ppg.c.ibi;
%     data.ppg.d.ibi = output.ppg.d.ibi;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.ibi = output.ppg.e.ibi;
%         data.ppg.f.ibi = output.ppg.f.ibi;
%     end
%     data.ppg.a.beats = output.ppg.a.beats;
%     data.ppg.b.beats = output.ppg.b.beats;
%     data.ppg.c.beats = output.ppg.c.beats;
%     data.ppg.d.beats = output.ppg.d.beats;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.beats = output.ppg.e.beats;
%         data.ppg.f.beats = output.ppg.e.beats;
%     end
%     data.ppg.a.CI_raw = output.ppg.a.CI_raw;
%     data.ppg.b.CI_raw = output.ppg.b.CI_raw;
%     data.ppg.c.CI_raw = output.ppg.c.CI_raw;
%     data.ppg.d.CI_raw = output.ppg.d.CI_raw;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.CI_raw = output.ppg.e.CI_raw;
%         data.ppg.f.CI_raw = output.ppg.f.CI_raw;
%     end
%     data.ppg.a.CI_times = output.ppg.a.CI_times;
%     data.ppg.b.CI_times = output.ppg.b.CI_times;
%     data.ppg.c.CI_times = output.ppg.c.CI_times;
%     data.ppg.d.CI_times = output.ppg.d.CI_times;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.CI_times = output.ppg.e.CI_times;
%         data.ppg.f.CI_times = output.ppg.f.CI_times;
%     end
%     data.ppg.a.CI_beats = output.ppg.a.CI_beat;
%     data.ppg.b.CI_beats = output.ppg.b.CI_beat;
%     data.ppg.c.CI_beats = output.ppg.c.CI_beat;
%     data.ppg.d.CI_beats = output.ppg.d.CI_beat;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.CI_beats = output.ppg.e.CI_beat;
%         data.ppg.f.CI_beats = output.ppg.f.CI_beat;
%     end
%     
%     data.pat.pat = output.pat.pat;
%     data.pat.Ch = output.pat.Channel;
%     data.pat.HR = output.pat.HR;
%     data.bp.sbp = output.bp.sbp;
%     data.bp.dbp = output.bp.dbp;
% 
%     
%     data.ppg.a.pat = output.ppg.a.pat;
%     data.ppg.b.pat = output.ppg.b.pat;
%     data.ppg.c.pat = output.ppg.c.pat;
%     data.ppg.d.pat = output.ppg.d.pat;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.pat = output.ppg.e.pat;
%         data.ppg.f.pat = output.ppg.f.pat;
%     end
%     data.ppg.a.CI_pat = output.ppg.a.CI_pat;
%     data.ppg.b.CI_pat = output.ppg.b.CI_pat;
%     data.ppg.c.CI_pat = output.ppg.c.CI_pat;
%     data.ppg.d.CI_pat = output.ppg.d.CI_pat;
%     if isfield(output.ppg, 'e')
%         data.ppg.e.CI_pat = output.ppg.e.CI_pat;
%         data.ppg.f.CI_pat = output.ppg.f.CI_pat;
%     end
    
    clear output input

    
end

