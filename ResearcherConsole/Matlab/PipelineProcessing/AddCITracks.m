function AddCITracks(c, sessionList, simba_common_dir_location)
%
% this function adds the confidence indicator to an existing _metrics.mat
% file
%

global SIMBA_COMMON_DIR

if ~exist('simba_common_dir_location', 'var'),
    simba_common_dir_location = SIMBA_COMMON_DIR;
end

for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(metricsFilename);
    
    %% Run confidence indicator for all streams
    
    %
    % calculate confidence indicator for all tracks
    %
    output = Main_CI_I_v0_1_formatVer0(data, simba_common_dir_location);
    
    %
    % unpack CI into data
    %
    sixChanLogical = exist('data.ppg.e.signal', 'var');
    
    data.ecg.CI_times = output.ecg.CI_times;
    data.ecg.CI = output.ecg.CI;
    
    data.ppg.a.CI_times = output.ppg.a.CI_times;
    data.ppg.a.CI = output.ppg.a.CI;
    
    data.ppg.b.CI_times = output.ppg.b.CI_times;
    data.ppg.b.CI = output.ppg.b.CI;
    
    data.ppg.c.CI_times = output.ppg.c.CI_times;
    data.ppg.c.CI = output.ppg.c.CI;
    
    data.ppg.d.CI_times = output.ppg.d.CI_times;
    data.ppg.d.CI = output.ppg.d.CI;
    
    if (sixChanLogical),
        data.ppg.e.CI_times = output.ppg.c.CI_times;
        data.ppg.e.CI = output.ppg.c.CI;
        
        data.ppg.f.CI_times = output.ppg.d.CI_times;
        data.ppg.f.CI = output.ppg.d.CI;
    end
    
    
    %
    % overwrite the metrics file with this new one having the beats
    %
    save(metricsFilename, 'data');
end
