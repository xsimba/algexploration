function AddTracks(c, sessionList, firmwareVersion, fromUnix)

if (~exist('firmwareversion','var')),
    firmwareVersion = 'v0.23.0';
end

if (~exist('fromUnix','var')),
    fromUnix = 0;
end


for i = 1:length(sessionList),
    dataFilename = setV0MatSessionData(c, sessionList{i});
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    if (fromUnix == 1),
        dataFilename = convertSSICpathToUnix(dataFilename);
        metricsFilename = convertSSICpathToUnix(metricsFilename);
    end
    
    verStr = firmwareVersion(1:min(length(firmwareVersion), 5));
    verNo = str2num(verStr(4:5));
    
    % load data into workspace in v0 format
    if (exist(metricsFilename, 'file')),
        load(metricsFilename);
    else
        load(dataFilename);
    end
    
    if strcmp(verStr, 'v0.20') || strcmp(verStr, 'v0.19'),
        invertPPG = 1;
    elseif (verNo > 20 && verNo < 23),
        invertPPG = 1;
    else
        invertPPG = 0;
    end
    
    disp(['Running AddTracks() on ',sessionList{i}]);
    
    data = executeAllAlgorithms(data, invertPPG);
    
    %     data.ppg.a.beatAmp = data.ppg.a.DB.BeatAmp;
    %     data.ppg.b.beatAmp = data.ppg.b.DB.BeatAmp;
    %     data.ppg.c.beatAmp = data.ppg.c.DB.BeatAmp;
    %     data.ppg.d.beatAmp = data.ppg.d.DB.BeatAmp;
    %     if isfield(data.ppg ,'e')
    %        data.ppg.e.beatAmp = data.ppg.e.DB.BeatAmp;
    %        data.ppg.f.beatAmp = data.ppg.f.DB.BeatAmp;
    %     end
    %     if isfield(data.ppg, 'g')
    %        data.ppg.g.beatAmp = data.ppg.g.DB.BeatAmp;
    %        data.ppg.h.beatAmp = data.ppg.h.DB.BeatAmp;
    %     end
    save(metricsFilename, 'data');
    
end

