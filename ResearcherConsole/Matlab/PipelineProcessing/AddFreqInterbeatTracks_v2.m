function data = AddFreqInterbeatTracks_v2(c, sessionList, fromUnix)

if ~exist('fromUnix'),
    fromUnix = 0;
end

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    if (fromUnix == 1),
        metricsFilename = convertSSICpathToUnix(metricsFilename);        
    end
    
    
    
    % load data into workspace in v0 format
    load(metricsFilename);
    
    disp(['processing ', sessionList{i}]);

%    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    
    output.acc.signal.x = data.acc.x.signal;
    output.acc.signal.y = data.acc.y.signal;
    output.acc.signal.z = data.acc.z.signal;
    
    % Default scale and offset settings for Bosch BMA280 (also dependent on configuration settings)
    AccScaleOffset.xsc = 1/4096.0; AccScaleOffset.xof = 0.0;
    AccScaleOffset.ysc = 1/4096.0; AccScaleOffset.yof = 0.0;
    AccScaleOffset.zsc = 1/4096.0; AccScaleOffset.zof = 0.0;
    
    cax = output.acc.signal.x*AccScaleOffset.xsc + AccScaleOffset.xof; % No calibration: apply current scales and offsets
    cay = output.acc.signal.y*AccScaleOffset.ysc + AccScaleOffset.yof;
    caz = output.acc.signal.z*AccScaleOffset.zsc + AccScaleOffset.zof;
    AccUD = 0;
    
    acc(:,1) = cax;
    acc(:,2) = cay;
    acc(:,3) = caz;
    thisACC = sqrt(cax.^2+cay.^2+caz.^2); % Magnitude
    

    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg ,'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
            eval(['thisData = data.',curTrack,'.signal;']);
            [CI_freq, ibi_freq, amp_freq, timestep, index_seg_end, num_pks, action_flag] = generate_ibi_freq_v2(thisData, thisACC);
            eval(['data.',curTrack,'.freq_HR.CI_freq = CI_freq;']);
            eval(['data.',curTrack,'.freq_HR.ibi_freq = ibi_freq;']);
            eval(['data.',curTrack,'.freq_HR.amp_freq = amp_freq;']);
            eval(['data.',curTrack,'.freq_HR.timestep = timestep;']);
            eval(['data.',curTrack,'.freq_HR.time = data.timestamps(index_seg_end);']);
            eval(['data.',curTrack,'.freq_HR.num_pks = num_pks;']);
            eval(['data.',curTrack,'.freq_HR.action_flag = action_flag;']);
    end

    %%

    save (metricsFilename, 'data');

end

end
