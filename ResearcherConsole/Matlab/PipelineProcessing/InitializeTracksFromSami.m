function InitializeTracksFromSami(c, sessionList, version)

if ~exist('version', 'var'),
    version = 'v4';
end

for i = 1:length(sessionList),
    [sc,time] = setSamiSessionData(c, sessionList{i});
    matFilename = setV0MatSessionData(c, sessionList{i});
    srcdata = getSimbandSamiSrc(sc,time, version);
    debugdata = getSimbandSami(sc,time, version);
    if strcmp(version, 'v5')||strcmp(version, 'v4')|| strcmp(version, 'v3'),
        data = srcdata;
        save(matFilename, 'data');
    end
    if strcmp(version, 'v4-debug')|| strcmp(version, 'v3-debug'),
        data = debugdata;
        save(matFilename, 'data');
    end
    
    if isfield(srcdata,'timestamps'),
        srctimestamps = srcdata.timestamps;
        signalcount = 0;
        existingsignals = {};
        if isfield(debugdata,'ppg'),
            tracks = {'a','b','c','d','e','f','g','h'};
            for i = 1:numel(tracks),
                if isfield(debugdata.ppg,tracks{i}),
                    signal = strcat('debugdata.ppg.',tracks{i});
                    if isfield(eval(signal), 'timestamps'),
                        signalcount = signalcount + 1;
                        existingsignals{signalcount} = signal;
                    end
                end
            end
        end

        if isfield(debugdata,'ecg'),
            if isfield(debugdata.ecg,'timestamps'),
                signalcount = signalcount + 1;
                existingsignals{signalcount} = ('debugdata.ecg');
            end
        end

        if isfield(debugdata,'acc'),
            tracks = {'x','y','z'};
            for i = 1:numel(tracks),
                if isfield(debugdata.acc,tracks{i}),
                    signal = strcat('debugdata.acc.',tracks{i});
                    if isfield(eval(signal), 'timestamps'),
                        signalcount = signalcount + 1;
                        existingsignals{signalcount} = signal;
                    end
                end
            end
        end
    %     debugsignaltimestamps = {'debugdata.ppg.a','debugdata.ppg.b','debugdata.ppg.c','debugdata.ppg.d', ...
    %         'debugdata.ppg.e','debugdata.ppg.f','debugdata.ppg.g','debugdata.ppg.h','debugdata.ecg', ...
    %         'debugdata.acc.x','debugdata.acc.y','debugdata.acc.z'};
    %     for i = 1:numel(debugsignaltimestamps),
    %         if isfield(eval(debugsignaltimestamps{i}), 'timestamps'),
    %             signalcount = signalcount + 1;
    %             existingsignals{signalcount} = debugsignaltimestamps{i};
    %             
    %         end
    %     end

        for i = 1:numel(existingsignals),
            debugtimestamps = strcat(existingsignals{i},'.timestamps');
            if length(srctimestamps) ~= length(eval(debugtimestamps)),
                display('Warning: All data may not be downloaded in the time aligned data structure. Download data using debug schema in order to download all the data');
                break;
            end
        end
    end
    
end
    
end
