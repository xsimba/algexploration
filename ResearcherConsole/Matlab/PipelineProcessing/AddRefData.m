function [refData] = AddRefData(c, sessionList)

for i = 1:length(sessionList),
    refFilename = setRefData(c, sessionList{i});

    dataFilename = setV0MatSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(dataFilename);

    % load data into workspace in v0 format
    %load(refFilename);
    
    disp(['Running AddRefData() on ', sessionList{i}]);
    
    [refData, startTime, duration] = RefData(refFilename);
    data.HR_ref = [];
    data.HR_ref.signal = refData;
    data.HR_ref.startTime = startTime;
    data.HR_ref.duration = duration;
    %%

    save (dataFilename, 'data');

end

end