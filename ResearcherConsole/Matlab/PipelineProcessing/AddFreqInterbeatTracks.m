function data = AddFreqInterbeatTracks(c, sessionList, filterFlag)
%
% add interbeattime tracks
%

if (~exist('filterFlag')),
    filterFlag = 1;
end

%
% may want to add some input validation 
%

%%
% loop over sessions
%
for i = 1:length(sessionList),
    metricsFilename = setMetricsSessionData(c, sessionList{i});
    
    % load data into workspace in v0 format
    load(metricsFilename);
    
    disp(['processing ', sessionList{i}]);

%    tracks = {'ecg', 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
    thisACC = data.acc.x.signal;
    if isfield(data.ppg ,'e')
        tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
    end
    if isfield(data.ppg ,'g')
        tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
    end
    
    for j = 1:length(tracks),
        curTrack = tracks{j};
            eval(['thisData = data.',curTrack,'.signal;']);
            FreqInterbeat = generate_ibi_freq(thisData, thisACC, filterFlag, data.fs);
            eval(['data.',curTrack,'.ibi_freq = FreqInterbeat;']);
    end

    %%

    save (metricsFilename, 'data');

end

end
