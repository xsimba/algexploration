function InitializeTracksFromCSVfromUnix(c, sessionList, version)

if ~exist('version', 'var')
    version = '3v0';
end

InitializeTracksFromCSV(c, sessionList, version, 1);

end

