% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : executeAllAlgorithmsWrapper.m
%  Content    : Wrapper for loading data, execution of all algorithms and subsequent plotting
%  Version    : 2.0
%  Package    : SIMBA
%  Created by : E.C. Wentink (eva.wentink@imec-nl.nl)
%  Date       : 28-08-2014
%  Modification and Version History: 
%  | Developer | Version |    Date    |     Changes      |
%  | F.Beutel  |   1.1   |  07-11-14  | Clean up, align with latest import function, add plotting functions and example data |
%  | F.Beutel  |   1.2   |  15-12-14  | read in data with 'getSimbandCsv', compatibility with old and incomplete datasets |
%  | F.Beutel  |   2.0   |  15-04-15  | Renamed, Independent of relative paths now, Moved example data to separate folder |
%  | F.Beutel  |   2.1   |  10-08-15  | Add few plots for validation of TDE integration |

%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all; close all; clc; warning off;

%% Load data

%Example data file
    filename = ['collection-Wed Jul 15 2015 09-42-45 GMT+0200 (Romance Daylight Time) _sc1 - Copy.csv'];
    
    %Access csv data
    data = getSimbandCsv(filename, '4v2'); %(filename, '3v0') for UCSF4

    %Parse the PAT stream from the band (if available)
    if isfield(data, 'pat_info') %until IMEC BE study 'isfield(data, 'band_pat_bp')', respectively 'parsePATstreamV2' was used
        data = parse_PAT_INFO_stream(data);
    end
    if isfield(data, 'pat_bp') 
        data = parse_PAT_BP_stream(data);
    end
    
% %Example data file .mat (e.g. UCSF4)
%     filename = ['rID-7_dID-1084_file-10_aligned.mat'];
%     load(filename);
%     data=aligned_d;
%     data.timestamps = (data.timestamps-data.timestamps(1))/1000; %divide by 1000, not 1024!!!

%Example .mat data file
%     filename = ['SP0001_C_20150312_M_SI_1.mat'];
%     load(filename); 


%% Add context and SSB variables if not available from band

%data.context.posture = {'sitting'}; %{};
%data.ssb.age.signal = [27]; %[];
%data.ssb.height.signal = [180]; %[];
%data.ssb.weight.signal = [85]; %[];

%dummy ssb & ref bp ONLY FOR TRACKER VALIDATION
% data.bp.sbpref = [110];
% data.bp.dbpref = [70];
% data.ssb.age.signal = 27;
% data.ssb.height.signal = 185;
% data.ssb.weight.signal = 85;

%% Execution of all algorithms
flip = 0; % flipping only needed for old BP training data!
run_TDE = 1;
[data]=executeAllAlgorithms(data, flip, run_TDE);


%% Debug Plots (uncomment to plot debug info)
debugplot_title = filename;
plotDebugInfoPATandBP(data, 'ppg.e', [debugplot_title,'_FE']);

%plotBandPAT(data);

plotMatlabPAT(data);


%% debug plot for ppg e CI
%     if isfield(data.ppg.e,'visual')
%         plotOneMatlabCI_2(data);
%     end

%% debug plot filtered PPG
% cd([AllAlgorithms_folder, '.\..\BeatDetection\Experiment'])
% Filt4to13Hz(data.ppg.e.signal)

%% BP Tracker Validation Info

% 1) Put Simband on and conduct Spotcheck
% 2) Download data for this spotcheck, executeAllAlgorithmsWrapper, input values from below/console on web UI (3380/settings/)

% SBP_est_reference_for_webUI = data.bp.sbp.signal(1)
% DBP_est_reference_for_webUI = data.bp.dbp.signal(1)
% %disp('Use cuff readings for "External" BP readings')
% HR_reference_for_webUI = data.pat_bp.signal(1)
% PAT_reference_for_webUI = data.pat_bp.signal(3)

% 3) Put Simband on and record an arbitrary number of spotchecks for tracking
% 4) Download data for these spotchecks AND the first spotcheck
% 5) Uncomment code section below for comparison between Tracker on Matlab and Band

%% BP Tracker Matlab Band comparison (uncomment to use)
% 
%     %Reference Spotcheck Definition
%     data.bp.tracker.band.reference.sbp_est.signal = data.bp.tracker.band.reference.sbp_est.signal(data.bp.tracker.band.reference.sbp_est.signal ~= 0);
%     data.bp.tracker.band.reference.dbp_est.signal = data.bp.tracker.band.reference.dbp_est.signal(data.bp.tracker.band.reference.dbp_est.signal ~= 0);
%     data.bp.tracker.band.reference.sbp_cal.signal = data.bp.tracker.band.reference.sbp_cal.signal(data.bp.tracker.band.reference.sbp_cal.signal ~= 0);
%     data.bp.tracker.band.reference.dbp_cal.signal = data.bp.tracker.band.reference.dbp_cal.signal(data.bp.tracker.band.reference.dbp_cal.signal ~= 0);
%     data.bp.tracker.band.reference.HR.signal = data.bp.tracker.band.reference.HR.signal(data.bp.tracker.band.reference.HR.signal ~= 0);
%     data.bp.tracker.band.reference.pat.signal = data.bp.tracker.band.reference.pat.signal(data.bp.tracker.band.reference.pat.signal ~= 0);
% 
%     %Tracker Output
%     data.bp.tracker.band.sbp_est.signal;
%     data.bp.tracker.band.dbp_est.signal;
%     data.bp.tracker.band.sbp_cal.signal;
%     data.bp.tracker.band.dbp_cal.signal;
% 
    

%     %Plotting
%     fig_tracker_validation = figure('Name', 'Tracker Validation'); hold on;
%     suptitle('Tracker Validation')
% 
%     %SBP
%     subplot(2,1,1), hold on
%     %band tracking trajectory
%     plot([NaN,data.bp.tracker.band.sbp_cal.signal(2:end)], 'g-o')
%     %offline tracking trajectory
%     plot(data.ppg.e.bp.tracker.file.calibration_based.via_pwv.sbp.signal, 'g--o')
%     if isfield(data.bp, 'sbpref') 
%         plot(data.bp.sbpref, 'r')
%     else
%         disp('No reference BP available')
%     end
%     xlabel('Spotcheck #')
%     ylabel('SBP [mmHg]')
%     legend('Simband, est', 'Matlab, est, PWV', 'Matlab, est, PAT', 'Simband, pseudo-cal', 'Matlab, pseudo-cal, PWV', 'Matlab, pseudo-cal, PAT')
% 
%     %DBP
%     subplot(2,1,2), hold on
%     plot([NaN,data.bp.tracker.band.dbp_cal.signal(2:end)], 'g-o')
%     plot(data.ppg.e.bp.tracker.file.calibration_based.via_pwv.dbp.signal, 'g--o')
%     if isfield(data.bp, 'dbpref') 
%         plot(data.bp.dbpref, 'r')
%     end
%     xlabel('Spotcheck #')
%     ylabel('DBP [mmHg]')
%     legend('Simband, est', 'Matlab, est, PWV', 'Matlab, est, PAT', 'Simband, pseudo-cal', 'Matlab, pseudo-cal, PWV', 'Matlab, pseudo-cal, PAT')

%% TDE integration validation

%load('data_old_TDE.mat')

% figure, hold on
% plot(data.ppg.a.beatsTDE.timestamps)
% plot(data.ppg.b.beatsTDE.timestamps)
% plot(data.ppg.c.beatsTDE.timestamps)
% plot(data.ppg.d.beatsTDE.timestamps)
% plot(data.ppg.e.beatsTDE.timestamps)
% plot(data.ppg.f.beatsTDE.timestamps)
% plot(data.ppg.g.beatsTDE.timestamps)
% plot(data.ppg.h.beatsTDE.timestamps)
% xlabel('samples')
% ylabel('timestamps')
% title('new')
% 
% figure, hold on
% plot(data_old_TDE.ppg.a.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.b.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.c.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.d.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.e.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.f.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.g.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.h.beatsTDE.timestamps)
% xlabel('samples')
% ylabel('timestamps')
% title('old')

% figure, hold on
% subplot(1,2,1), hold on
% plot(data_old_TDE.ppg.a.beatsTDE.timestamps - data.ppg.a.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.b.beatsTDE.timestamps - data.ppg.b.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.c.beatsTDE.timestamps - data.ppg.c.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.d.beatsTDE.timestamps - data.ppg.d.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.e.beatsTDE.timestamps - data.ppg.e.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.f.beatsTDE.timestamps - data.ppg.f.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.g.beatsTDE.timestamps - data.ppg.g.beatsTDE.timestamps)
% plot(data_old_TDE.ppg.h.beatsTDE.timestamps - data.ppg.h.beatsTDE.timestamps)
% xlabel('samples')
% ylabel('ibi')
% title('comparison ibi hilbert')
% 
% subplot(1,2,2), hold on
% plot(data_old_TDE.ppg.a.ibi_hilbert - data.ppg.a.ibi_hilbert)
% plot(data_old_TDE.ppg.b.ibi_hilbert - data.ppg.b.ibi_hilbert)
% plot(data_old_TDE.ppg.c.ibi_hilbert - data.ppg.c.ibi_hilbert)
% plot(data_old_TDE.ppg.d.ibi_hilbert - data.ppg.d.ibi_hilbert)
% plot(data_old_TDE.ppg.e.ibi_hilbert - data.ppg.e.ibi_hilbert)
% plot(data_old_TDE.ppg.f.ibi_hilbert - data.ppg.f.ibi_hilbert)
% plot(data_old_TDE.ppg.g.ibi_hilbert - data.ppg.g.ibi_hilbert)
% plot(data_old_TDE.ppg.h.ibi_hilbert - data.ppg.h.ibi_hilbert)
% xlabel('samples')
% ylabel('timestamps')
% title('comparison TDE')


%% Validation Feature Extraction Roberto 08/15

fprintf('\n')
disp('------------------------------Spotcheck Averages---------------------------------------')
disp('Results:         Band      Matlab        Delta')
disp('---------------------------------------------------------------------------------------')
disp(['HR            = ' num2str([data.pat_bp.HR_avg_for_BP, data.ppg.e.spotcheck.HR.signal, abs(data.pat_bp.HR_avg_for_BP-data.ppg.e.spotcheck.HR.signal)],5)]);
disp(['pat           = ' num2str([data.pat_bp.pat_avg_for_BP, data.ppg.e.spotcheck.pat.signal, abs(data.pat_bp.pat_avg_for_BP-data.ppg.e.spotcheck.pat.signal) ],5)]);
disp(['pat_foot      = ' num2str([data.validation.pat_foot.signal, data.ppg.e.spotcheck.pat_foot.signal, abs(data.validation.pat_foot.signal-data.ppg.e.spotcheck.pat_foot.signal)*1000 ],5)]);
disp(['decaytime     = ' num2str([data.validation.decaytime.signal, data.ppg.e.spotcheck.decaytime.signal, abs(data.validation.decaytime.signal-data.ppg.e.spotcheck.decaytime.signal)*1000 ],5)]);
disp(['risetime      = ' num2str([data.validation.risetime.signal, data.ppg.e.spotcheck.risetime.signal, abs(data.validation.risetime.signal-data.ppg.e.spotcheck.risetime.signal)*1000 ],5)]);
disp(['RCtime        = ' num2str([data.validation.RCtime.signal, data.ppg.e.spotcheck.RCtime.signal, abs(data.validation.RCtime.signal-data.ppg.e.spotcheck.RCtime.signal)*1000 ],5)]);
disp(['pp_sp_int     = ' num2str([data.validation.pp_sp_int.signal, data.ppg.e.spotcheck.pp_sp_int.signal, abs(data.validation.pp_sp_int.signal-data.ppg.e.spotcheck.pp_sp_int.signal)*1000 ],5)]);


disp(['ppg_pp_ft_amp = ' num2str([data.validation.ppg_pp_ft_amp.signal, data.ppg.e.spotcheck.ppg_pp_ft_amp.signal, abs(data.validation.ppg_pp_ft_amp.signal-data.ppg.e.spotcheck.ppg_pp_ft_amp.signal)],5)]);
disp(['ppg_upstgrad  = ' num2str([data.validation.ppg_upstgrad.signal, data.ppg.e.spotcheck.ppg_upstgrad.signal, abs(data.validation.ppg_upstgrad.signal-data.ppg.e.spotcheck.ppg_upstgrad.signal)],5)]);

disp('---------------------------------------------------------------------------------------')

bandBeatsTs = reshape(data.ppg.e.beats.info.timestamps,16,length(data.ppg.e.beats.info.timestamps)/16);
bandBeatsTs = bandBeatsTs(1,:);

bandEcgBeatsTs = data.ecg.beats.timestamps(intersect(find(data.ppg.e.beats.info.timestamps>data.ecg.beats.timestamps(1)),find(data.ecg.beats.timestamps<data.ppg.e.beats.info.timestamps(end)))); 

features = reshape(data.ppg.e.beats.info.signal,16,length(data.ppg.e.beats.info.signal)/16); 
features(features == 555)=nan; 

%% US timestamps
figure;
plot(bandBeatsTs);hold on
plot(data.ppg.e.bd.upstroke(4:end),'-.g') %%->offset due to number of beats
title('US timestamps')
legend('Band','Matlab')

%% Foot timestamps
figure;
plot(bandBeatsTs,features(1,:)+bandBeatsTs);hold on
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.foot,'-.g') %%offset due to number of beats
legend('Band','Matlab')
title('Foot timestamps')

%%Primary peak timestamps
figure;
plot(bandBeatsTs,features(2,:)+bandBeatsTs);hold on
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.secpeak,'-.g') %%offset due to number of beats
legend('Band','Matlab')
title('Secondary peak timestamps')

%%primary peak
figure;
plot(bandBeatsTs,features(3,:)+bandBeatsTs);hold on
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.pripeak,'-.g') %%offset due to number of beats
legend('Band','Matlab')
title('Primary peak timestamps')

%%dicrotic notch
figure;
plot(bandBeatsTs,features(4,:)+bandBeatsTs);hold on
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.dicrnot,'-.g') %%offset due to number of beats
legend('Band','Matlab')
title('Dicrotic notch timestamps')

%ftppamp
figure;
plot(bandBeatsTs,features(7,:));
hold on;
plot(data.ppg.e.features.timestamps,data.ppg.e.features.ppg_pp_ft_amp.signal,'-.g')
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.ppftamp,'-.r')
title('Foot to primary peak amp')
legend('Band','Matlab Filt','Matlab BD')

%US grad
figure;
plot(bandBeatsTs,features(8,:));
hold on;
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.upstgrad,'-.g')
title('US grad')
legend('Band','Matlab')

%decay time
figure;
plot(bandEcgBeatsTs,features(9,:));
hold on;
plot(data.ppg.e.features.timestamps,data.ppg.e.features.decaytime.signal,'-.g')
title('Decay time')
legend('Band','Matlab')

%risetime
figure;
plot(bandEcgBeatsTs,features(10,:));
hold on;
plot(data.ppg.e.features.timestamps,data.ppg.e.features.risetime.signal,'-.g')
title('Rise time')
legend('Band','Matlab')

%rctime
figure;
plot(bandEcgBeatsTs,features(11,:));
hold on;
plot(data.ppg.e.features.timestamps,data.ppg.e.features.RCtime.signal,'-.g')
title('RC time')
legend('Band','Matlab')

%US amp
figure;
plot(bandBeatsTs,features(12,:));
hold on;
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.debug.usamp,'-.g')
title('US amp')
legend('Band','Matlab')

%ftamp
figure;
plot(bandBeatsTs,features(13,:));
hold on;
plot(data.ppg.e.features.timestamps,data.ppg.e.features.foot_amp.signal,'-.g')
plot(data.ppg.e.bd.upstroke,data.ppg.e.filtsig.ftamp,'-.r')
title('Foot amp')
legend('Band','Matlab BD','Matlab filt')

%SP amp
figure;
plot(bandBeatsTs,features(14,:));
hold on;
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.debug.spamp,'-.g')
plot(data.ppg.e.bd.upstroke,data.ppg.e.filtsig.spamp,'-.r')
title('Secondary peak amp')
legend('Band','Matlab BD','Matlab filt')

%ppamp
figure;
plot(bandBeatsTs,features(15,:));
hold on;
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.debug.ppamp,'-.g')
plot(data.ppg.e.bd.upstroke,data.ppg.e.filtsig.ppamp,'-.r')
title('Primary peak amp')
legend('Band','Matlab BD','Matlab filt')

%dn amp
figure;
plot(bandBeatsTs,features(16,:));
hold on;
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.debug.dnamp,'-.g')
plot(data.ppg.e.bd.upstroke,data.ppg.e.filtsig.dnamp,'-.r')

title('Dicrotic notch amp')
legend('Band','Matlab')

%%

figure;
marker_size = 2;  
plot(data.timestamps,data.ecg.signal-nanmean(data.ecg.signal),'k');hold on
plot(data.ecg.bd.rpeak, data.ecg.bd.debug.usamp - nanmean(data.ecg.signal),'go','MarkerSize',marker_size,'LineWidth',2)
plot(data.timestamps,data.ppg.e.signal- nanmean(data.ppg.e.signal));
plot(data.ppg.e.bd.foot,data.ppg.e.bd.debug.ftamp- nanmean(data.ppg.e.signal),'ro','MarkerSize',marker_size,'LineWidth',2)
plot(data.ppg.e.bd.upstroke,data.ppg.e.bd.debug.usamp- nanmean(data.ppg.e.signal),'go','MarkerSize',marker_size,'LineWidth',2)
plot(data.ppg.e.bd.pripeak,data.ppg.e.bd.debug.ppamp- nanmean(data.ppg.e.signal),'mo','MarkerSize',marker_size,'LineWidth',2)
plot(data.ppg.e.bd.secpeak,data.ppg.e.bd.debug.spamp- nanmean(data.ppg.e.signal),'yo','MarkerSize',marker_size,'LineWidth',2)
plot(data.ppg.e.bd.dicrnot,data.ppg.e.bd.debug.dnamp- nanmean(data.ppg.e.signal),'ko','MarkerSize',marker_size,'LineWidth',2)
plot(data.spot_check.timestamps, data.spot_check.signal*10000 - nanmean(data.spot_check.signal*10000), '--md','MarkerSize',marker_size,'LineWidth',2);

figure;
plot(data.spot_check.timestamps,data.spot_check.signal);
hold on;
plot(data.ppg.e.beats.timestamps,data.ppg.e.beats.signal,'or')
plot(data.ppg.e.bd.upstroke,ones(length(data.ppg.e.bd.upstroke),1),'+b')
plot(data.ecg.beats.timestamps,data.ecg.beats.signal+0.1,'or')
plot(data.ecg.bd.rpeak,ones(length(data.ecg.bd.rpeak),1)+0.1,'+b')
ylim([0 4])
