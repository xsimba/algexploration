function heartRateComparison(data, label)

%Compares heart rate computed from matlab with that recorded from the band
%

if ~exist('label', 'var'),
    label = '';
end



%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

colring = 'kbgrmggkgbgrmggkg';
%colring = 'kgcrbmg';

metricsPPG = {'signal', 'beats', 'CI_times', 'CI_raw', 'ibi', 'DB', ...
    'biosemTime', 'biosemStatMed', 'biosemQual' };
metricsPPGDB = {'BeatAmp', 'FootAmp', 'PrPkAmp'};
metricsECG = metricsPPG(1:3);
tracks = {'ecg', tracksPPG{:}};

%
% Fusion-based instantaneous heart rate comparison with band heart rate and
% reference heart rate
%
figure;
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.1 0 0.33 1]);
ax = [];


for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(eval(['data.', trackName]), 'ibi_hilbert'),
        ax = [ax, subplot(4,2,i)];
        eval(['hrTime = data.',trackName,'.hilbert_fusion.timestamps;']); %fusion based ibi and hr, change these to lines 103 and 104 for hilbert based
        eval(['ibi = data.',trackName,'.hilbert_fusion.ibi;']);
        %eval(['hrTime = data.',trackName,'.hilbert_timestamps;']);
        %eval(['ibi = squeeze(data.',trackName,'.ibi_hilbert);']);
        plot(hrTime, 60 ./ ibi, [colring(trackNum+1),'x']);
        if isfield(data, 'band_hr')
            hold on;
            plot(data.band_hr.timestamps, data.band_hr.signal,'k');
        end
        if isfield(data, 'HR_ref')
            hold on;
            plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k-');
        end
        set(gca, 'YLim', [30 150]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Fusion based Instantaneous HR comparison with band HR');
set(t, 'Interpreter', 'none');
%
% Biosemantic heart rate comparison with band hr and reference heart rate
%
figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);
ciring = {[1,1,1]*0.8, ...
    [1,1,1]*0.6, ...
    [1,1,1]*0.4, ...
    [1,1,1]*0.2, ...
    [1,1,1]*0, ...
    };

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax = [ax, subplot(4,2,i)];
        eval(['hrTime = data.',trackName,'.biosemTime;']);
        % for frequency domain, use the short b/c 20 averaging is too laggy
        %
        %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['hrBiosem = data.',trackName,'.biosemStatMed.muHR;']);
        eval(['biosemQual = data.',trackName,'.biosemQual;']);
        eval(['t_hrci = data.',trackName,'.mat_CIraw.timestamps;']);
        eval(['hrci = data.',trackName,'.mat_CIraw.signal;']);        

        if (length(t_hrci) ~= length(hrTime)),
            hrci = interp1(t_hrci, hrci, hrTime, 'nearest');
        end
        for k = 1:5,
            ind = find(hrci == k);
            if (length(ind)>0),
                plot(hrTime(ind), hrBiosem(ind), '.', 'MarkerSize', 14, ...
                    'Color', ciring{k});
                hold on;
            end
        end
        
        if isfield(data, 'band_hr')
            hold on;
            plot(data.band_hr.timestamps, data.band_hr.signal,'k');
        end
        if isfield(data, 'HR_ref')
            hold on;
            plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'r-');
        end
        set(gca, 'YLim', [30 150]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Biosemantic HR');
set(t, 'Interpreter', 'none');

% figure;
% clf
% set(gcf, 'Units', 'Normalized');
% set(gcf, 'Position', [0.1 0 0.33 1]);
% 
% 
% for i = 1:8,
%     trackNum = i;
%     trackName = tracksPPG{i};
%     if isfield(eval(['data.', trackName, '.biosemStatMed']), 'HR_Error')
%         ax = [ax, subplot(4,2,i)];
%         eval(['hrTime = data.',trackName,'.biosemTime;']);
%         % for frequency domain, use the short b/c 20 averaging is too laggy
%         %
%         %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
%         eval(['HR_Error = data.',trackName,'.band_hr.HR_Error;']);
%         eval(['biosemQual = data.',trackName,'.biosemQual;']);
%         
%         %
%         % confidence calculation: binary AND fusion of high rate of change and high variance
%         %
%         lowConf = biosemQual.medConfidence > 8;
%         lowConf = lowConf(1:length(hrTime));  %??
%         highConf = ~lowConf;
%         
%         plot(hrTime(highConf), HR_Error(highConf), [colring(trackNum+1),'.']);
%         hold on;
%         plot(hrTime(lowConf), HR_Error(lowConf), '.', 'Color', [0.75 0.75 0.75]);
%         line([hrTime(1), hrTime(end)], [0 0], 'Color', 'k');
%         
%         set(gca, 'YLim', [-100 100]);
%         ylabel(trackName);
%         text(10, 80, ['RMS Error = ', num2str(eval(['data.',trackName,'.band_hr.HR_RMSError;'])), ' BPM']);
%         %    set(gca, 'XLim', [0 15000]);
%     end
% end
% % subplot(4,2,1);
% % t = title(label);
% % set(t, 'Interpreter', 'none');
% subplot(4,2,2);
% t = title('Band based Biosemantic HR Error');
% 
% %set(ax(1), 'XLim', [data.HR_ref.inferredTimestamps(1), data.HR_ref.inferredTimestamps(end)]);
% linkaxes(ax, 'xy');