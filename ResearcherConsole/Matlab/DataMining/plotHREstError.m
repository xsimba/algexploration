function plotHREstError(data)

%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

colring = 'kbgrmggkgbgrmggkg';
%colring = 'kgcrbmg';

figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.1 0 0.33 1]);
ax = [];

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(eval(['data.', trackName, '.biosemStatMed']), 'HR_Error')
        ax = [ax, subplot(4,2,i)];
        eval(['hrTime = data.',trackName,'.biosemTime;']);
        % for frequency domain, use the short b/c 20 averaging is too laggy
        %
        %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['HR_Error = data.',trackName,'.biosemStatMed.HR_Error;']);
        eval(['biosemQual = data.',trackName,'.biosemQual;']);
        
        %
        % confidence calculation: binary AND fusion of high rate of change and high variance
        %
        lowConf = biosemQual.medConfidence > 8;
        lowConf = lowConf(1:length(hrTime));  %??
        highConf = ~lowConf;
        
        plot(hrTime(highConf), HR_Error(highConf), [colring(trackNum+1),'.']);
        hold on;
        plot(hrTime(lowConf), HR_Error(lowConf), '.', 'Color', [0.75 0.75 0.75]);
        line([hrTime(1), hrTime(end)], [0 0], 'Color', 'k');
        
        set(gca, 'YLim', [-100 100]);
        ylabel(trackName);
        text(10, 80, ['RMS Error = ', num2str(eval(['data.',trackName,'.biosemStatMed.HR_RMSError;'])), ' BPM']);
        %    set(gca, 'XLim', [0 15000]);
    end
end
% subplot(4,2,1);
% t = title(label);
% set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Biosemantic HR Error');
set(t, 'Interpreter', 'none');

set(ax(1), 'XLim', [data.HR_ref.inferredTimestamps(1), data.HR_ref.inferredTimestamps(end)]);
linkaxes(ax, 'xy');