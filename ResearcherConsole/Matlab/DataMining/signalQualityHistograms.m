function signalQualityHistograms(data, label)

thisTrack = 2;

%
% create an interactive dashboard to display 4 channels of raw PPG,
%

if ~exist('label', 'var'),
    label = '';
end

%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

colring = 'kbgrmggrgbgrmggrg';
%colring = 'kgcrbmg';

metricsPPG = {'signal', 'beats', 'CI_times', 'CI_raw', 'DB' };
metricsPPGDB = {'BeatAmp', 'FootAmp', 'PrPkAmp'};
metricsECG = metricsPPG(1:3);
tracks = {'ecg', tracksPPG{:}};

try
    curTrack = 'ecg';
    for j = 1:length(metricsECG),
        metric = metricsECG{j};
        evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
        eval(evalString);
    end
    
    for i = 1:length(tracksPPG),
        curTrack = tracksPPG{i};
        for j = 1:length(metricsPPG),
            metric = metricsPPG{j};
            evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
            eval(evalString);
        end
    end
    assert(isfield(data.acc,'x'));
    assert(isfield(data.acc,'y'));
    assert(isfield(data.acc,'z'));
catch
    disp(['track: ',curTrack, '    metric:', metric]);
    error('fourChanComboDisplay: inputs missing some tracks of data');
end


%
% beat amplitude with time.
%
figure(1);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    ax{i} = subplot(4,2,i);
    eval(['beatTime = data.',trackName,'.beats(1,:);']);
    eval(['peakAmp = data.',trackName,'.DB.PrPkAmp;']);
    eval(['footAmp = data.',trackName,'.DB.FootAmp;']);
    plot(beatTime, peakAmp-footAmp, [colring(trackNum+1),'x']);
    ylabel(trackName);
    set(gca, 'YLim', [0 5000])
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,8);
xlabel('time[s]');
subplot(4,2,2);
t = title('Beat Amplitude');
set(t, 'Interpreter', 'none');


%
% CI beats histograms
%
figure(2)
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.33 0 0.33 1]);

for i = 1:8,
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['cibeats = data.',trackName,'.CI_beat;']);
        x = hist(cibeats, 0:4) + 1;
        pie(x);
        %    set(gca, 'XLim', [0 15000]);
    end
end

legend({'0', '1', '2', '3', '4'});
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t= title('CI beats');
set(t, 'Interpreter', 'none');


%
% CI raw histograms
%
figure(3)
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['ciraw = data.',trackName,'.CI_raw;']);
        hist(ciraw);
        set(gca, 'XLim', [0 4]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,8);
xlabel('CI raw');

%
% beat-based instantaneous heart rate
%
figure(4);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.15 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['beatTime = data.',trackName,'.beats(1,:);']);
        eval(['ibi = data.',trackName,'.ibi;']);
        plot(beatTime(2:end), 60 ./ ibi, [colring(trackNum+1),'x']);
        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('instantaneous HR');
set(t, 'Interpreter', 'none');

%
% histogram of beat amplitude
%
figure(5)
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.65 0 0.33 1]);
for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    ax{i} = subplot(4,2,i);
    eval(['beatTime = data.',trackName,'.beats(1,:);']);
    eval(['peakAmp = data.',trackName,'.DB.PrPkAmp;']);
    eval(['footAmp = data.',trackName,'.DB.FootAmp;']);
    hist(peakAmp-footAmp, 100);
    ylabel(trackName);
end

subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,8);
xlabel('Beat Amplitude');

