function plotSimbandPPGs(data)

figure;
colring = 'bgrmggkg';

timestamps = data.timestamps;

tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
for i =1:length(tracksPPG)
    ppg{i} = eval(['data.physiosignal.', tracksPPG{i}, '.signal']);
    ppgLabel{i} = tracksPPG{i}(5);
end

for i = 1:length(ppg)
    plotAxes(i) = subplot(length(tracksPPG)/2, 2, i);
    plot(timestamps, ppg{i}, colring(i))
    ylabel(['PPG', num2str(i-1), ' (',  ppgLabel{i}, ')'])
end
subplot(length(tracksPPG)/2, 2, length(ppg)-1); xlabel ('Time (s)')
subplot(length(tracksPPG)/2, 2, length(ppg)); xlabel ('Time (s)')
linkaxes(plotAxes, 'x')
set(gcf,'name','Simband PPG Signals','numbertitle','off')



beatsPPGChan = 5;
subplot(length(tracksPPG)/2, 2, beatsPPGChan);  
hold on;
beatTimestamps = 1E-3*(double(data.physiosignal.ppg.heartBeat.signal) - double(data.unixTimeStamps(1)));
beatIdx = [];
for i = 1:length(beatTimestamps)
    beatIdx = [beatIdx, find(timestamps >= beatTimestamps(i), 1)];
end
        
plot(beatTimestamps, ppg{beatsPPGChan}(beatIdx), 'b*')
    