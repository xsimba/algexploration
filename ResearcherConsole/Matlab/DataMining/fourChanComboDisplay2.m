function fourChanComboDisplay2(data, label, beatMetric, beatOffset)
%
% shows the 2nd four channels of a dataset
%
%

thisTrack = 9;

if ~exist('label', 'var'),
    label = '';
end

if ~exist('beatMetric', 'var'),
    beatMetric = 'ibi';
end

if ~exist('beatOffset', 'var'),
    beatOffset = data.timestamps(1);
end

fourChanComboDisplay(data, label, beatMetric, beatOffset, 4, ...
                              thisTrack);
