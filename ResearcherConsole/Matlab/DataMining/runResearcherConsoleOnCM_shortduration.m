
time_comp = 1.5;
% Set thresholds for publishing
thresholdA =.1;
thresholdB =.4;

collection_duration = 25;
%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.15;
bioLimits.rateLimitUp = 0.15;
bioLimits.windowSize = 10;
bioLimits.sigmaCutoff = 2.5;

c = loadSessionData('\\105.140.2.7\share\DataAnalysis\BioSemanticDevel-server2.xlsx');

% c = loadSessionData('.\BioSemanticDevel_Sessions.xlsx');


sessionList={};

% sessionList = {sessionList{:},'01202015_cm_test1','01202015_cm_test2','01202015_cm_test3','01202015_cm_test4','01202015_cm_test5','01182015_0345-0800_yelei','01192015_yelei', '01182015_1012_yelei'};
sessionList = {sessionList{:}, '02242015_range'};
for k = 1:length(sessionList)
%     clear clipData data
    InitializeTracksFromCSV(c, sessionList);
    dataFilename = setV0MatSessionData(c, sessionList{k});
    load(dataFilename);
    
    %Clip data into sections
    clipData = findSections(data);
    
    
    
    tracks = {'ppg.e'};
    for i = 1:length(clipData),
        
        %Running motion detection on accel
        options = simset('SrcWorkspace', 'current');
%         sim('motionDetector_20141216', [clipData{i}.timestamps(1), ...
%             clipData{i}.timestamps(1)+collection_duration], options);
        sim('motionDetector_20141216', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(end)], options);
        %                 sim('motionDetector_20141216', [clipData{i}.timestamps(1), ...
        %             clipData{i}.timestamps(1)+25], options);
        clipData{i}.motion_flag.timestamps = motionFlag.time;
        clipData{i}.motion_flag.signal = squeeze(motionFlag.signals.values);
%         clipData{i}.motion_flag.signal = zeros(size(squeeze(motionFlag.signals.values)));
        
        % uncommment for case of bad accelero.
        %             clipData{i}.motion_flag.timestamps = motionFlag.time;
        %         clipData{i}.motion_flag.signal = zeros(length(squeeze(motionFlag.signals.values)),1);
        
        options = simset('SrcWorkspace', 'current');
%         sim('motionCounter', [clipData{i}.timestamps(1), ...
%             clipData{i}.timestamps(1)+collection_duration], options);
sim('motionCounter', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(end)], options);
        %         sim('motionCounter', [clipData{i}.timestamps(1), ...
        %            clipData{i}.timestamps(1)+25], options);
        clipData{i}.motionCounter_flag = motionCounter_flag.Data(end);
        
        
        j = 1;
        curTrack = tracks{j};
        channel = 5;
        timestamps=[];ibi_out=[];
        
        %Running hilbert transform
        options = simset('SrcWorkspace', 'current');
%         sim('InstFreqHR_currentCcodeImpl_CM', [clipData{i}.timestamps(1), ...
%             clipData{i}.timestamps(1)+collection_duration], options);
sim('InstFreqHR_currentCcodeImpl_CM', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(end)], options);
        %         sim('InstFreqHR_currentCcodeImpl_CM', [clipData{i}.timestamps(1), ...
        %             clipData{i}.timestamps(1)+25], options);
        
        ibi_hilbert = squeeze(60./HRout.signals.values);
        eval(['clipData{1,',int2str(i),'}.',curTrack,'.ibi_hilbert = ibi_hilbert;']);
        eval(['clipData{1,',int2str(i),'}.',curTrack,'.hilbert_timestamps = HRout.time;']);
         timestamps = [timestamps;  HRout.time];
         ibi_out    = [ibi_out; ibi_hilbert/128];
        %
        % Running tde model
        %
%         options = simset('SrcWorkspace','current');
%         sim('tde_ibi', [clipData{i}.timestamps(1), ...
%             clipData{i}.timestamps(1)+collection_duration], options);
sim('tde_ibi_CM', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(end)], options);
        timestamps = [timestamps; simout.Time(find(simout.Data(:,2)==1))];
        ibi_out    = [ibi_out; double(simout.Data(find(simout.Data(:,2)==1),1))/128];
        %                 sim('tde_ibi_CM', [clipData{i}.timestamps(1), ...
        %             clipData{i}.timestamps(1)+25], options);
%         sim('tde_ibi_inverse', [clipData{i}.timestamps(1), clipData{i}.timestamps(1)+collection_duration], options);
%         timestamps = [timestamps; simout.Time(find(simout.Data(:,2)==1))];
%         ibi_out    = [ibi_out; double(simout.Data(find(simout.Data(:,2)==1),1))/128];
%         
%         sim('tde_ibi_40', [clipData{i}.timestamps(1), clipData{i}.timestamps(1)+collection_duration], options);
%         timestamps = [timestamps; simout.Time(find(simout.Data(:,2)==1))];
%         ibi_out    = [ibi_out; double(simout.Data(find(simout.Data(:,2)==1),1))/128];
%         
%         sim('tde_ibi_40_inverse', [clipData{i}.timestamps(1), clipData{i}.timestamps(1)+collection_duration], options);
%         timestamps = [timestamps; simout.Time(find(simout.Data(:,2)==1))];
%         ibi_out    = [ibi_out; double(simout.Data(find(simout.Data(:,2)==1),1))/128];
% %         
%         sim('tde_ibi_60', [clipData{i}.timestamps(1), clipData{i}.timestamps(1)+collection_duration], options);
%         timestamps = [timestamps; simout.Time(find(simout.Data(:,2)==1))];
%         ibi_out    = [ibi_out; double(simout.Data(find(simout.Data(:,2)==1),1))/128];
% % %         
%         sim('tde_ibi_60_inverse', [clipData{i}.timestamps(1), clipData{i}.timestamps(1)+collection_duration], options);
%         timestamps = [timestamps; simout.Time(find(simout.Data(:,2)==1))];
%         ibi_out    = [ibi_out; double(simout.Data(find(simout.Data(:,2)==1),1))/128];
% %         
%         timestamps = simout.Time(find(simout.Data(:,2)==1));
%         ibi_out    = double(simout.Data(find(simout.Data(:,2)==1),1))/128;
        
%         eval(['clipData{',int2str(i),'}.',curTrack,'.tde_ibi.timestamps = timestamps;']);
%         eval(['clipData{',int2str(i),'}.',curTrack,'.tde_ibi.ibi = ibi_out;']);
        
        %
        % hilbert and tde fusion
        %
%         eval(['hilbert_timestamps = clipData{',int2str(i),'}.',curTrack,'.hilbert_timestamps;']);
%         eval(['tde_ibi.timestamps = clipData{',int2str(i),'}.',curTrack,'.tde_ibi.timestamps;']);
        [fusion_timestamps, indx] = ...
            sort(timestamps);
%         eval(['ibi_hilbert = clipData{',int2str(i),'}.',curTrack,'.ibi_hilbert;']);
%         eval(['tde_ibi.ibi = clipData{',int2str(i),'}.',curTrack,'.tde_ibi.ibi;']);
%         ibi_fusion = [ibi_hilbert; tde_ibi.ibi];
        ibi_fusion = ibi_out;
        
        ibi_fusion = ibi_fusion(indx);
        
        eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.timestamps = fusion_timestamps;']);
        eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.ibi = ibi_fusion;']);
        
        %
        % Running BiosemHR on fusion
        %
        clipData{i} = computeBiosemBeatFilter(clipData{i}, curTrack, ...
            bioLimits, 'hilbert_fusion');
        
        %
        % Compiling the publication stats and publish CM result
        %
        eval (['rawBeats = clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.ibi;']);
        eval (['bbqBeats = clipData{',int2str(i),'}.',curTrack,'.biosemInterbeats(2,:);']);
        plottableRawBeats = rawBeats(rawBeats> 60/240 & rawBeats<60/30);
%         plottableRawBeats = rawBeats(rawBeats>0 & rawBeats<100);
        
        lenRat(i) = length(bbqBeats)/ length(rawBeats);
        lenRat2(i) = length(bbqBeats)/ length(plottableRawBeats);
        
        if (lenRat(i) > thresholdA && lenRat2(i) > thresholdB && ...
                clipData{i}.motionCounter_flag ==0),
            CM.HR = mean (60 ./ bbqBeats);
            CM.rawHR = median (60 ./ rawBeats(60./rawBeats>30 & 60./rawBeats<240));
            CM.HRact = CM.HR;
            CM.HRsigma = std(60 ./ bbqBeats);
            CM.HRCI = 5;  % make this a function of the std(biosemInterbeats)
            eval(['clipData{',int2str(i),'}.',curTrack,'.CM = CM;']);
        else
            CM.HR = NaN;
            CM.rawHR = NaN;
            CM.HRact = median (60 ./ bbqBeats);
            CM.HRsigma = std(60 ./ bbqBeats);
            CM.HRCI = 0;
            eval(['clipData{',int2str(i),'}.',curTrack,'.CM = CM;']);
        end
        
    end
    metricsFilename = setMetricsSessionData(c, sessionList{k});
    save (metricsFilename, 'clipData');
end
plotCMdistros;


