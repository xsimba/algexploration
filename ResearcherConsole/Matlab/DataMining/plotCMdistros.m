lenRat= [];
close all
figure(2);
clf
thresholdA =.1;
thresholdB =.4;

ax1 = subplot(311);
for i = 1:length(clipData),
    plot(clipData{i}.timestamps(end), 60./clipData{i}.ppg.e.hilbert_fusion.ibi, 'bx');
    hold on;
    plot(clipData{i}.timestamps(end) - 15, 60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:),'rx');
end
ylabel('Raw and Filtered HR');
title('Collection Mode Heart Rate');
set(gca, 'YLim', [0 200]);

ax2 = subplot(312);
for i = 1:length(clipData),
    rawBeats = clipData{i}.ppg.e.hilbert_fusion.ibi;
    plottableRawBeats = rawBeats(rawBeats> 60/240 & rawBeats<60/30);
    bbqBeats = clipData{i}.ppg.e.biosemInterbeats(2,:);
    lenRat(i) = length(bbqBeats)/ length(rawBeats);
    lenRat2(i) = length(bbqBeats)/ length(plottableRawBeats);

    plot(clipData{i}.timestamps(end), lenRat(i), 'bx');
    hold on;
    plot(clipData{i}.timestamps(end), lenRat2(i), 'ro');

end
ylabel('filtered vs. total Samples');

ax3 = subplot(313);
for i = 1:length(clipData),
    if (lenRat(i) > thresholdA && lenRat2(i) > thresholdB),
        plot(clipData{i}.timestamps(end), 60./clipData{i}.ppg.e.hilbert_fusion.ibi, 'bx');
        hold on;
        plot(clipData{i}.timestamps(end) - 15, 60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:),'rx');
    end
end
xlabel('time');
ylabel('Raw and Filtered HR, Accepted');
set(gca, 'YLim', [0 200]);
linkaxes([ax1, ax2, ax3],'x')


figure(3); clf;
ax4 = subplot(211);
for i = 1:length(clipData),
    if (lenRat(i) > thresholdA && lenRat2(i) > thresholdB && clipData{i}.motionCounter_flag ==0),
        plot(clipData{i}.timestamps(end) - 15, 60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:),'bx');
        hold on;
    elseif (lenRat(i) > thresholdA && lenRat2(i) > thresholdB && clipData{i}.motionCounter_flag ==1),
        plot(clipData{i}.timestamps(end) - 15, 60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:),'rx');
        hold on;
        
    end
end
ylabel('Raw and Filtered HR, Accepted');
set(gca, 'YLim', [0 200]);

ax5 = subplot(212);


thresh  = [0.03 0.05 0.07 0.09];
sym ='xvo.';
for i = 1:length(clipData),
    hold on;
%    if (lenRat(i) > threshold & clipData{i}.motionCounter_flag ==1),
%     if (clipData{i}.motionCounter_flag ==1),
%         plot(clipData{i}.timestamps(end) - 15, mean(60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:)),'ro');
%    for k = 1:4,
    if (lenRat(i) > thresholdA && lenRat2(i) > thresholdB && clipData{i}.motionCounter_flag ==0),        
%    if (lenRat(i) > thresh(k)),        
        plot(clipData{i}.timestamps(end) - 15, mean(60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:)),['b',sym(1)]);
        plot(clipData{i}.timestamps(end) - 15, median(60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:)),['g',sym(1)]);
    end
%     end
    

    
    
    
%    elseif (lenRat(i) > threshold & clipData{1,i}.motionCounter_flag ==0),
%        plot(clipData{i}.timestamps(end) - 15, median(60 ./ clipData{i}.ppg.e.biosemInterbeats(2,:)),'rx');
%    end
end
xlabel('time');
ylabel('HR');
set(gca, 'YLim', [0 200]);

linkaxes([ax4, ax5],'x')


%%  Analysis Plot %%
figure(4)
ax6 = subplot(211);
for i = 1:length(clipData),
    if ~isnan(clipData{i}.ppg.e.CM.HR),
        plot(clipData{i}.timestamps(end), 60 ./ clipData{i}.ppg.e.biosemInterbeats(2,2:end),'bx');
        hold on;
    elseif (length(clipData{i}.ppg.e.biosemInterbeats(2,2:end))>2),
        plot(clipData{i}.timestamps(end), 60 ./ clipData{i}.ppg.e.biosemInterbeats(2,2:end),'rx');
        hold on;        
    end
end
ylabel('HR [bpm]')
title('Accepted (blue) and Rejected (red) HR distributions');
set(gca, 'YLim', [30 170]);

ax7 = subplot(212);
for i = 1:length(clipData),
    hold on;
    plot(clipData{i}.timestamps(end), clipData{i}.ppg.e.CM.HR, 'bx', 'MarkerSize', 8, 'LineWidth', 2);
    plot(clipData{i}.timestamps(end), clipData{i}.ppg.e.CM.rawHR, 'gx', 'MarkerSize', 8, 'LineWidth', 2);
end
xlabel('time');
ylabel('HR [bpm]');
title('Collection Mode HR');
set(gca, 'YLim', [30 170]);

linkaxes([ax6, ax7],'x')

