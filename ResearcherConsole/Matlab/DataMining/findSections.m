% Function to analyze Simba data and break into valid sections
function [clipData] = findSections(data)


% tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
% if isfield(data.ppg ,'e')
%     tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
% end
% if isfield(data.ppg, 'g')
%     tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
% end
% if isfield(data, 'acc')
%     tracks = {tracks{:}, 'acc.x', 'acc.y', 'acc.z'};
% end

tracks = {'ppg.e','acc.x','acc.y','acc.z'};
deltaTime = diff(data.timestamps);
ins = find(deltaTime > 10);


for i = 1:length(tracks)
    eval(['sig = data.', tracks{i}, '.signal;']);
    sectionStruct = struct([]);
    thisStruct = struct([]);
    old_index = 1;
    for j = 1:length(ins)
        new_index = ins(1, j);
        thisStruct = struct('signal',sig(old_index:new_index),'timestamps',data.timestamps(old_index:new_index));
        sectionStruct = [sectionStruct; thisStruct];
        old_index = new_index + 1;
    end
    
    thisStruct = struct('signal',sig(old_index:end), 'timestamps', data.timestamps(old_index:end));
    sectionStruct = [sectionStruct; thisStruct];
    eval(['data.',tracks{i},'.sectionStruct = sectionStruct;']);
end

for i = 1:length(ins)+1
    sectionNumber = i;
    for j = 1:length(tracks)
        stream = eval(['data.',tracks{j}]);
        clipData{1,i}.timestamps = stream.sectionStruct(sectionNumber).timestamps;
        signal = stream.sectionStruct(sectionNumber).signal;
        eval(['clipData{1,',int2str(i),'}.',tracks{j},'.signal = signal;']);
    end
end

        



