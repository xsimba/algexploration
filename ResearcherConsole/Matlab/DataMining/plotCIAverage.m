function plotCIAverage(data, selectChannel, ref)

if ~exist('ref', 'var'),
    ref = [];
end;
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

colring = 'kbgrmggkgbgrmggkg';

figure(1);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.0 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['sig = data.',trackName,'.signal;']);
        plot(data.timestamps, sig, colring(1+trackNum));
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end

figure(2)
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.33 0 0.33 1]);
for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['CIraw_time = data.',trackName,'.CI_times;']);
        eval(['CIraw = data.',trackName,'.CI_raw;']);
        plot(CIraw_time, CIraw, [colring(trackNum+1),'o']);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
linkaxes([ax{:}], 'x');
figure(3);
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);
max_array = 0;
max_index_array = 0;
for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['signal = data.',trackName;]);
        signal_CI_average = CIAverage(signal);
        plot(signal_CI_average, [colring(trackNum+1), 'x']);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end


figure(4);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i} = subplot(4,2,i);
        eval(['hrTime = data.',trackName,'.biosemTime;']);
% for frequency domain, use the short b/c 20 averaging is too laggy
%
%        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['hrBiosem = data.',trackName,'.biosemStatMed.muHR;']);
        eval(['biosemQual = data.',trackName,'.biosemQual;']);

        %
        % confidence calculation: binary AND fusion of high rate of change and high variance
        %
        lowConf = biosemQual.medConfidence > 8;
        lowConf = lowConf(1:length(hrTime));  %??
        highConf = ~lowConf;
        
        plot(hrTime(highConf), hrBiosem(highConf), [colring(trackNum+1),'.']);
        hold on;
        plot(hrTime(lowConf), hrBiosem(lowConf), '.', 'Color', [0.75 0.75 0.75]);
%         if ~isempty(ref_data),
%             hold on;
%             plot(ref_data,'k');
%         end

        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end


figure(5);
clf;
plot(selectChannel.time(2:end), selectChannel.this_max_index(2:end), 'kx');
ylabel('Channel with highest CI');
xlabel('time (s)');

figure(6);
clf;
plot(selectChannel.time, selectChannel.hr, 'x');
hold on
plot(data.band_hr_bb_BioSem.timestamps, data.band_hr_bb_BioSem.signal, 'ro');

%hold on
%plot(ref.ibi_time_ref/32768, 60./(ref.ibi_ref/32768), 'kx')
legend('channelSelect HR', 'Band Biosem HR');

