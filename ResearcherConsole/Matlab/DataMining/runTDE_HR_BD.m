function data = runTDE_HR_BD(data)

bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.15;
bioLimits.rateLimitUp = 0.15;
bioLimits.windowSize = 10;
bioLimits.sigmaCutoff = 1.5;


options = simset('SrcWorkspace', 'current');
sim('motionDetector_forCcodeGen', [data.timestamps(1), data.timestamps(end)], options);
data.motion_flag.timestamps = motionFlag.time;
data.motion_flag.signal = squeeze(motionFlag.signals.values);

tracks = { 'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
if isfield(data.ppg ,'e')
    tracks = {tracks{:}, 'ppg.e', 'ppg.f'};
end
if isfield(data.ppg, 'g')
    tracks = {tracks{:}, 'ppg.g', 'ppg.h'};
end
for j = 1:8,
    curTrack = tracks{j};
    channel = j;
    
    options = simset('SrcWorkspace','current');   
    sim('InstFreqHR_currentCcodeImpl', [data.timestamps(1), data.timestamps(end)], options);   
    ibi_hilbert = squeeze(60./HRout.signals.values);
    eval(['data.',curTrack,'.ibi_hilbert = ibi_hilbert;']);
    eval(['data.',curTrack,'.hilbert_timestamps = HRout.time;']);
    
    sim('MonitoringMode_HR_BD', [data.timestamps(1), data.timestamps(end)], options);
    timestamps = simout.Time(find(simout.Data(:,2)==1));
    ibi_out    = double(simout.Data(find(simout.Data(:,2)==1),1));    
    eval(['data.' , curTrack, '.tde_ibi.timestamps = timestamps;']);
    eval(['data.' , curTrack, '.tde_ibi.ibi = ibi_out;']);
    eval(['data.' , curTrack, '.beatsTDE.timestamps = time_c.Data(simout.Data(:,1)==1,1);']);
%     eval(['data.' , curTrack, '.beatsTDE.ibi =  double(simout.Data(find(simout.Data(:,2)==1),1));']);
    
    
    eval(['ibi_hilbert = data.', tracks{j}, '.ibi_hilbert;']);
    eval(['time_hilbert = data.', tracks{j}, '.hilbert_timestamps;']);
    
    eval(['ibi_tde = data.', tracks{j}, '.tde_ibi.ibi;']);
    eval(['time_tde = data.', tracks{j}, '.tde_ibi.timestamps;']);
    
%     [fusion_timestamps, indx] =sort([time_hilbert; time_comp+time_tde]);   % '881 sample points' is manually tuned to align with ibi_hilbert!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    [fusion_timestamps, indx] =sort([time_hilbert; time_tde]);   % '881 sample points' is manually tuned to align with ibi_hilbert!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    ibi_fusion = [ibi_hilbert; ibi_tde ];
    ibi_fusion = ibi_fusion(indx);
    
    eval(['data.', tracks{j}, '.hilbert_fusion.timestamps = fusion_timestamps;']);
    eval(['data.', tracks{j}, '.hilbert_fusion.ibi = ibi_fusion;']);
    
        metric = 'hilbert_fusion';
        
        data = computeBiosemBeatFilter(data, curTrack, bioLimits, metric);
      
        %
        % compute final statistics (output is in biosemStatMed)
        %
        data = computeBiosemStaticStat(data, curTrack, 'biosemInterbeats');
        data = computeBiosemQuality(data, curTrack);
        
        

end


end