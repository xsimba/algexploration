function fourChanBeatFeatures(data, label, chSelect, beatOffset, trackOffset)
%
% create an interactive dashboard to display 4 channels of raw PPG with
% features shown.  Foot = o, upstroke = x, peak = +, notch = o
%
% data = input data structure
% label = plot label
% chSelect = selected PPG channels -- vector of indicies 1 to 8.  First
%            three selected
% beatOffset & timeOffset [default 0] -- offset times for raw ppg & beats streams, respectively
%

if isfield(data,'timestamp')
    data.timestamps = data.timestamp;
end

if ~exist('label', 'var'),
    label = '';
end

if ~exist('chSelect', 'var'),
    chSelect = [5, 8, 1];
end

if ~exist('beatOffset', 'var'),
    beatOffset = data.timestamps(1);
end

if ~exist('trackOffset', 'var'),
    trackOffset = 0;
end

%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
tracksPPG = tracksPPG(chSelect);

colring = 'kbgrmggkgbgrmggkg';
metricsPPG = {'signal', 'bd'};
metricsECG = metricsPPG(1:2);

try
    curTrack = 'ecg';
    for j = 1:length(metricsECG),
        metric = metricsECG{j};
        evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
        eval(evalString);
    end
    
    for i = 1:length(tracksPPG),
        curTrack = tracksPPG{i};
        for j = 1:length(metricsPPG),
            metric = metricsPPG{j};
            evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
            eval(evalString);
        end
    end
catch
    disp(['track: ',curTrack, '    metric:', metric]);
    error('fourChanComboDisplay: inputs missing some tracks of data');
end



figure(1);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0 0.33 0.9]);

%
% plot PPG channels and 
%
for i = 1:3,
    trackNum = i;
    trackName = tracksPPG{i};
    ax{i} = subplot(4,1,i);
    eval(['sig = data.',trackName,'.signal;']);
    plot(data.timestamps, sig, colring(1+chSelect(trackNum)));
    eval(['beat = beatOffset+data.',trackName,'.bd.upstroke(1,:);']);
    eval(['beatAmp = data.',trackName,'.bd.debug.usamp;']);
    hold on;
    plot(beat, beatAmp, 'kx', 'MarkerSize', 14);
    eval(['beat = beatOffset+data.',trackName,'.bd.foot(1,:);']);
    eval(['beatAmp = data.',trackName,'.bd.debug.ftamp;']);
    hold on;
    plot(beat, beatAmp, 'ko', 'MarkerSize', 14);
    eval(['beat = beatOffset+data.',trackName,'.bd.pripeak(1,:);']);
    eval(['beatAmp = data.',trackName,'.bd.debug.ppamp;']);
    hold on;
    plot(beat, beatAmp, 'k+', 'MarkerSize', 14);
    eval(['beat = beatOffset+data.',trackName,'.bd.dicrnot(1,:);']);
    eval(['beatAmp = data.',trackName,'.bd.debug.dnamp;']);
    hold on;
    plot(beat, beatAmp, 'kv', 'MarkerSize', 14);
    
    
    hold on;
    ylabel(trackName);
    if (i == 1),
        title (label);
    end
    
end


%
% plot ECG
%
ax{4} = subplot(4,1,4);
eval(['sig = data.ecg.signal;']);
plot(data.timestamps, sig, colring(1+trackNum));
eval(['beat = beatOffset+data.ecg.bd.rpeak(1,:);']);
eval(['beatAmp = data.ecg.bd.debug.usamp;']);
hold on;
plot(beat, beatAmp, 'kx', 'MarkerSize', 14);
xlabel ('time [s]');
ylabel ('ecg');



linkaxes([ax{:}], 'x');
end
