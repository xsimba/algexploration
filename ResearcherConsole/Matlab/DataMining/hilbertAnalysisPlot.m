function hilbertAnalysisPlot(data, track, label)

%runs biosemtracks without saving data to the metrics file and plots the
%output on a single track
if ~exist('label', 'var'),
    label = '';
end


%
% Biosemantic Otuput before filtering
%
figure;

if isfield(eval(['data.',track]), 'biosemInterbeats'),
    eval(['hrTime = data.',track,'.biosemInterbeats(1,:);']);
    eval(['ibi = squeeze(data.',track,'.biosemInterbeats(2,:));']);
    plot(hrTime, 60./ibi);
    if isfield(data, 'HR_ref')
        hold on;
        plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k');
    end
    set(gca, 'YLim', [30 150])
end

%
% Biosemantic heart rate
%
figure;
clf
ciring = {[1,1,1]*0.8, ...
    [1,1,1]*0.6, ...
    [1,1,1]*0.4, ...
    [1,1,1]*0.2, ...
    [1,1,1]*0, ...
    };

if isfield(data.ppg, track(end)),
    eval(['hrTime = data.',track,'.biosemTime;']);
    % for frequency domain, use the short b/c 20 averaging is too laggy
    %
    %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
    eval(['hrBiosem = data.',track,'.biosemStatMed.muHR;']);
    eval(['biosemQual = data.',track,'.biosemQual;']);
    eval(['t_hrci = data.',track,'.mat_CIraw.timestamps;']);
    eval(['hrci = data.',track,'.mat_CIraw.signal;']);        

    if (length(t_hrci) ~= length(hrTime)),
        hrci = interp1(t_hrci, hrci, hrTime, 'nearest');
    end
    for k = 1:5,
        ind = find(hrci == k);
        if (length(ind)>0),
            plot(hrTime(ind), hrBiosem(ind), '.', 'MarkerSize', 14, ...
                'Color', ciring{k});
            hold on;
        end
    end

    if isfield(data, 'HR_ref')
        hold on;
        plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k');
    end

    set(gca, 'YLim', [30 150]);
    %    set(gca, 'XLim', [0 15000]);
end

%
%plot rms errors
%

figure;
clf
if isfield(eval(['data.', track, '.biosemStatMed']), 'HR_Error')
    eval(['hrTime = data.',track,'.biosemTime;']);
    % for frequency domain, use the short b/c 20 averaging is too laggy
    %
    %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
    eval(['HR_Error = data.',track,'.biosemStatMed.HR_Error;']);
    eval(['biosemQual = data.',track,'.biosemQual;']);

    %
    % confidence calculation: binary AND fusion of high rate of change and high variance
    %
    lowConf = biosemQual.medConfidence > 8;
    lowConf = lowConf(1:length(hrTime));  %??
    highConf = ~lowConf;

    plot(hrTime(highConf), HR_Error(highConf),'b.');
    hold on;
    plot(hrTime(lowConf), HR_Error(lowConf), '.', 'Color', [0.75 0.75 0.75]);
    line([hrTime(1), hrTime(end)], [0 0], 'Color', 'k');

    set(gca, 'YLim', [-100 100]);
    text(10, 80, ['RMS Error = ', num2str(eval(['data.',track,'.biosemStatMed.HR_RMSError;'])), ' BPM']);
    %    set(gca, 'XLim', [0 15000]);
end

