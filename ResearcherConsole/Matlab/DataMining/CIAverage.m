function CIAverage = CIAverage(signal)

channel_CI = signal.CI_raw;
CI_times = signal.CI_times;
window = 10; %windows size is 10sec
move_by = 1; %move by 1sec
moving_average = 0;
for i = 1:move_by:(length(channel_CI) - window),
    moving_average = [moving_average mean(channel_CI(i:i+window))]; %average over 10 sec
end

CIAverage = moving_average(2:end);
    