function plotSimbandSignals(data)

figure;
colring = 'bgrmggkg';

timestamps = data.timestamps;

%  Plot ECG and beat detections
plotAxes(1) = subplot(3, 1, 1);
plot(timestamps, data.physiosignal.ecg.signal, 'm')

ecgBeatTimestamps = data.physiosignal.ecg.beats.timestamps(data.physiosignal.ecg.beats.timestamps >= timestamps(1) & data.physiosignal.ecg.beats.timestamps <= timestamps(end));
beatIdx = [];
for i = 1:length(ecgBeatTimestamps)
    beatIdx = [beatIdx, find(timestamps >= ecgBeatTimestamps(i), 1)];
end

hold on;
plot(ecgBeatTimestamps, data.physiosignal.ecg.signal(beatIdx), 'm*')
ylabel('ECG'), legend('ECG', 'Beat Detections')

% Plot PPG4 (e) and beat detections 
plotAxes(2) = subplot(3, 1, 2);
plot(timestamps, data.physiosignal.ppg.e.signal, 'g')

hold on;
beatTimestamps = 1E-3*(double(data.physiosignal.ppg.heartBeat.signal) - double(data.unixTimeStamps(1)));
beatIdx = [];
for i = 1:length(beatTimestamps)
    beatIdx = [beatIdx, find(timestamps >= beatTimestamps(i), 1)];
end
        
plot(beatTimestamps, data.physiosignal.ppg.e.signal(beatIdx), 'g*')
ylabel('PPG4 (e)'), legend('PPG4 (e)', 'Beat Detections')

% Plot Accel Magnitude and motion detections
plotAxes(3) = subplot(3, 1, 3);

plot(data.accel.magnitude.timestamps, data.accel.magnitude.signal, 'k')

hold on;
motionTimestamps = data.system.motion.timestamps(data.system.motion.signal == 1);
motionTimestamps = motionTimestamps(motionTimestamps >= timestamps(1) & motionTimestamps <= timestamps(end));
% motionIdx = [];
% for i = 1:length(motionTimestamps)
%     motionIdx = [motionIdx, find(data.accel.magnitude.timestamps >= motionTimestamps(i), 1)];
% end
plot(motionTimestamps, ones(size(motionTimestamps))*max(data.accel.magnitude.signal), 'k*')
ylabel('Accel'), legend('Accel', 'Motion Detections')


xlabel ('Time (s)')
linkaxes(plotAxes, 'x')
set(gcf,'name','Simband Signals','numbertitle','off')

%     Add GSR, Temperature, BioZ