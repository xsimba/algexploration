function channelSelect = channelSelect(data)

tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        eval(['signal = data.',trackName;]);
        signal_CI_average(:,i) = CIAverage(signal);
        max_CI_average(i) = max(signal_CI_average(:,i));
     end
end
%single CI max
[max_CI channel] = max(max_CI_average);
channelSelect.max_CI = max_CI;
channelSelect.channel = channel-1;

%best channel at every sec
channelSelect.this_max = 0;
channelSelect.this_max_index = 0;
channelSelect.time = 0;
for i = 1:length(signal_CI_average),
    [this_max this_max_index] = max(signal_CI_average(i,:));
    channelSelect.this_max = [channelSelect.this_max this_max];
    channelSelect.this_max_index = [channelSelect.this_max_index this_max_index-1];
    channelSelect.time = [channelSelect.time i];
        
end

channelSelect.hr = 0;
for j = 1: length(channelSelect.this_max_index),
    time_index = j + 6;
    eval(['hr = data.',tracksPPG{channelSelect.this_max_index(j) + 1},'.biosemStatMed.muHR;']);
    %length(hr), time_index
    channelSelect.hr = [ channelSelect.hr hr(time_index)];
    
end
channelSelect.hr = channelSelect.hr(2:end);