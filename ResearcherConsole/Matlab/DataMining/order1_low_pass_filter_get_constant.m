function alpha = order1_low_pass_filter_get_constant(Fc, Fs)
% /*!
% @brief	 <order1_low_pass_filter_get_constant>
% @details <Computes the alpha constant for a first-order filter based on sample
% frequency and a given cutoff frequency.>
%
% @param	 <float Fc> <cutoff frequency, in Hz>
% @param	 <float Fs> <sample frequency, in Hz>
% @return	 <parameter> <purpose/use of the return parameter>
%
% @note
% Fc = -Fs * ln(alpha) / (2*pi)
% 2*pi*Fc = -Fs * ln(alpha)
% ln(alpha) = -(2*pi*Fc/Fs)
% alpha = exp(-2*pi*Fc/Fs)>]
% */

alpha = NaN;

% 	// Divide-by-zero check
if (0 ~= Fs)
    
    alpha = exp(-2 * PI * Fc / Fs);  %// -2.0 is derived in the note above which is documented above.
end




