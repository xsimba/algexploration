function heartRateQualityPlotBand1(data, ref_data, label)

%
% create an interactive dashboard to display 4 channels of raw PPG,
%

if ~exist('label', 'var'),
    label = '';
end

if ~exist('ref_data', 'var'),
    ref_data = [];
end

timeOffset = data.timestamps(1);
%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

colring = 'kbgrmggkgbgrmggkg';
%colring = 'kgcrbmg';

metricsPPG = {'signal', 'beats', 'CI_times', 'CI_raw', 'ibi', 'DB', ...
    'biosemTime', 'biosemStatMed', 'biosemQual' };
metricsPPGDB = {'BeatAmp', 'FootAmp', 'PrPkAmp'};
metricsECG = metricsPPG(1:3);
tracks = {'ecg', tracksPPG{:}};

% try
%     curTrack = 'ecg';
%     for j = 1:length(metricsECG),
%         metric = metricsECG{j};
%         evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%         eval(evalString);
%     end
%
%     for i = 1:length(tracksPPG),
%         curTrack = tracksPPG{i};
%         for j = 1:length(metricsPPG),
%             metric = metricsPPG{j};
%             evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%             eval(evalString);
%         end
%     end
%     assert(isfield(data.acc,'x'));
%     assert(isfield(data.acc,'y'));
%     assert(isfield(data.acc,'z'));
% catch
%     disp(['track: ',curTrack, '    metric:', metric]);
%     error('fourChanComboDisplay: inputs missing some tracks of data');
% end


%
% beat-based instantaneous heart rate
%
figure(1);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.0 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)) && isfield(data.ppg.(trackName(end)), 'band_beats'),
        ax{i} = subplot(4,2,i);
        eval(['ibiTime = data.',trackName,'.band_beats.timestamps(2:end);']);
        eval(['ibi = diff(data.',trackName,'.band_beats.timestamps);']);
        plot(ibiTime, 60 ./ ibi, [colring(trackNum+1),'x']);
        if ~isempty(ref_data),
            hold on;
            plot((1:length(ref_data))+timeOffset, ref_data,'k');
        end
        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Beats instantaneous HR');
set(t, 'Interpreter', 'none');

% %
% % Freq-based instantaneous heart rate
% %
% figure(2);
% set(gcf, 'Units', 'Normalized');
% set(gcf, 'Position', [0.15 0 0.33 1]);
% 
% 
% for i = 1:8,
%     trackNum = i;
%     trackName = tracksPPG{i};
%     if isfield(eval(['data.', trackName]), 'ibi_freq'),
%         ax{i} = subplot(4,2,i);
%         eval(['ibiTime = 1*(1:length(data.',trackName,'.ibi_freq));']);
%         eval(['ibi = data.',trackName,'.ibi_freq;']);
%         plot(ibiTime, 60 ./ ibi, [colring(trackNum+1),'x']);
%         if ~isempty(ref_data),
%             hold on;
%             plot(ref_data,'k');
%         end
%         set(gca, 'YLim', [30 240]);
%         ylabel(trackName);
%         %    set(gca, 'XLim', [0 15000]);
%     end
% end
% subplot(4,2,1);
% t = title(label);
% set(t, 'Interpreter', 'none');
% subplot(4,2,2);
% t = title('Frequency-based instantaneous HR');
% set(t, 'Interpreter', 'none');

%
% Freq-based instantaneous heart rate
%
figure(2);
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.45 0 0.33 1]);
axoffset = 8;


for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(eval(['data.', trackName]), 'biosemInterbeats'),
        ax{i+axoffset} = subplot(4,2,i);
        eval(['hrTime = data.',trackName,'.biosemInterbeats(1,:);']);
        eval(['ibi = data.',trackName,'.biosemInterbeats(2,:);']);
        plot(hrTime, 60 ./ ibi, [colring(trackNum+1),'x']);
        if ~isempty(ref_data),
            hold on;
            plot((1:length(ref_data))+timeOffset, ref_data,'k');
        end
        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Biosemantic HR Selection');
set(t, 'Interpreter', 'none');


%
% Biosemantic heart rate
%
figure(3);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);
axoffset = length(ax);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i+axoffset} = subplot(4,2,i);
        eval(['hrTime = data.',trackName,'.biosemTime;']);
        % for frequency domain, use the short b/c 20 averaging is too laggy
        %
        %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['hrBiosem = data.',trackName,'.biosemStatMed.muHR;']);
        eval(['biosemQual = data.',trackName,'.biosemQual;']);
        
        %
        % confidence calculation: binary AND fusion of high rate of change and high variance
        %
        lowConf = biosemQual.medConfidence > 8;
        lowConf = lowConf(1:length(hrTime));  %??
        highConf = ~lowConf;
        
        plot(hrTime(highConf), hrBiosem(highConf), [colring(trackNum+1),'.']);
        hold on;
        plot(hrTime(lowConf), hrBiosem(lowConf), '.', 'Color', [0.75 0.75 0.75]);
        if ~isempty(ref_data),
            hold on;
            plot((1:length(ref_data))+timeOffset, ref_data,'k');
        end
        
        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Biosemantic HR');
set(t, 'Interpreter', 'none');

%
% Frequency features harmonics plot
%
figure(4);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.15 0 0.33 1]);
axoffset = length(ax);
for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax{i+axoffset} = subplot(4,2,i);
        
        % for frequency domain, use the short b/c 20 averaging is too laggy
        %
        %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['CI_freq = data.',trackName,'.freq_HR.CI_freq;']);
        eval(['ibi_freq = data.',trackName,'.freq_HR.ibi_freq;']);
        eval(['amp_freq = data.',trackName,'.freq_HR.amp_freq;']);
        eval(['time = data.',trackName,'.freq_HR.time;']);
        
        for k =1:size(CI_freq,2)
            %            [~,ind]=sort(CI_freq(:,k).*amp_freq(:,k),'descend');
            ind = find(CI_freq(:,k).*amp_freq(:,k)>0);
            for j = 1:3,
                if length(ind) >= j,
                    freq{j}(k) = ibi_freq(ind(j),k);
                else
                    freq{j}(k) = nan;
                end
            end
        end
        plot(time,freq{1}*60,'b.');
        hold on;
        plot(time,freq{2}*60,'r.');
        plot(time,freq{3}*60,'g.');
        if ~isempty(ref_data),
            hold on;
            plot((1:length(ref_data))+timeOffset, ref_data,'k');
        end
        
        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Freq HR');
set(t, 'Interpreter', 'none');

linkaxes([ax{:}], 'x');