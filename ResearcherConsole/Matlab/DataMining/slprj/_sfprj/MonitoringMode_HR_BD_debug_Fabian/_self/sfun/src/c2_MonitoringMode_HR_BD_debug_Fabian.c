/* Include files */

#include <stddef.h>
#include "blas.h"
#include "MonitoringMode_HR_BD_debug_Fabian_sfun.h"
#include "c2_MonitoringMode_HR_BD_debug_Fabian.h"
#include "mwmathutil.h"

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;

/* Function Declarations */
static void initialize_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void initialize_params_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void enable_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void disable_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void set_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_st);
static void finalize_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void sf_gateway_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void mdl_start_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void initSimStructsc2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber);
static void c2_info_helper(const mxArray **c2_info);
static const mxArray *c2_emlrt_marshallOut(const char * c2_u);
static const mxArray *c2_b_emlrt_marshallOut(const uint32_T c2_u);
static real_T c2_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_ppgBeatSample_PositiveOffset, const char_T *c2_identifier);
static real_T c2_b_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_c_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Amp_max, const char_T *c2_identifier);
static real_T c2_d_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_e_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Amp_nxt, const char_T *c2_identifier);
static real_T c2_f_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_g_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Amp_pre, const char_T *c2_identifier);
static real_T c2_h_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_i_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Idx_max, const char_T *c2_identifier);
static real_T c2_j_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_k_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Idx_ppg, const char_T *c2_identifier);
static real_T c2_l_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_m_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_tempAmp_middle, const char_T *c2_identifier);
static real_T c2_n_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_o_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_tempAmp_nxt, const char_T *c2_identifier);
static real_T c2_p_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static real_T c2_q_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_tempAmp_pre, const char_T *c2_identifier);
static real_T c2_r_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static uint8_T c2_s_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_is_active_c2_MonitoringMode_HR_BD_debug_Fabian, const char_T
   *c2_identifier);
static uint8_T c2_t_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void init_dsm_address_info
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);
static void init_simulink_io_address
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c2_Idx_max_not_empty = false;
  chartInstance->c2_Amp_max_not_empty = false;
  chartInstance->c2_Amp_pre_not_empty = false;
  chartInstance->c2_Amp_nxt_not_empty = false;
  chartInstance->c2_Idx_ppg_not_empty = false;
  chartInstance->c2_tempAmp_middle_not_empty = false;
  chartInstance->c2_tempAmp_pre_not_empty = false;
  chartInstance->c2_tempAmp_nxt_not_empty = false;
  chartInstance->c2_is_active_c2_MonitoringMode_HR_BD_debug_Fabian = 0U;
}

static void initialize_params_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static const mxArray *get_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  const mxArray *c2_st;
  const mxArray *c2_y = NULL;
  real_T c2_u;
  const mxArray *c2_b_y = NULL;
  real_T c2_b_u;
  const mxArray *c2_c_y = NULL;
  real_T c2_c_u;
  const mxArray *c2_d_y = NULL;
  real_T c2_d_u;
  const mxArray *c2_e_y = NULL;
  real_T c2_e_u;
  const mxArray *c2_f_y = NULL;
  real_T c2_f_u;
  const mxArray *c2_g_y = NULL;
  real_T c2_g_u;
  const mxArray *c2_h_y = NULL;
  real_T c2_h_u;
  const mxArray *c2_i_y = NULL;
  real_T c2_i_u;
  const mxArray *c2_j_y = NULL;
  uint8_T c2_j_u;
  const mxArray *c2_k_y = NULL;
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellmatrix(10, 1), false);
  c2_u = *chartInstance->c2_ppgBeatSample_PositiveOffset;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_b_u = chartInstance->c2_Amp_max;
  c2_c_y = NULL;
  if (!chartInstance->c2_Amp_max_not_empty) {
    sf_mex_assign(&c2_c_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_b_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 1, c2_c_y);
  c2_c_u = chartInstance->c2_Amp_nxt;
  c2_d_y = NULL;
  if (!chartInstance->c2_Amp_nxt_not_empty) {
    sf_mex_assign(&c2_d_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_c_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 2, c2_d_y);
  c2_d_u = chartInstance->c2_Amp_pre;
  c2_e_y = NULL;
  if (!chartInstance->c2_Amp_pre_not_empty) {
    sf_mex_assign(&c2_e_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_d_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 3, c2_e_y);
  c2_e_u = chartInstance->c2_Idx_max;
  c2_f_y = NULL;
  if (!chartInstance->c2_Idx_max_not_empty) {
    sf_mex_assign(&c2_f_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_e_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 4, c2_f_y);
  c2_f_u = chartInstance->c2_Idx_ppg;
  c2_g_y = NULL;
  if (!chartInstance->c2_Idx_ppg_not_empty) {
    sf_mex_assign(&c2_g_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_f_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 5, c2_g_y);
  c2_g_u = chartInstance->c2_tempAmp_middle;
  c2_h_y = NULL;
  if (!chartInstance->c2_tempAmp_middle_not_empty) {
    sf_mex_assign(&c2_h_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_h_y, sf_mex_create("y", &c2_g_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 6, c2_h_y);
  c2_h_u = chartInstance->c2_tempAmp_nxt;
  c2_i_y = NULL;
  if (!chartInstance->c2_tempAmp_nxt_not_empty) {
    sf_mex_assign(&c2_i_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_i_y, sf_mex_create("y", &c2_h_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 7, c2_i_y);
  c2_i_u = chartInstance->c2_tempAmp_pre;
  c2_j_y = NULL;
  if (!chartInstance->c2_tempAmp_pre_not_empty) {
    sf_mex_assign(&c2_j_y, sf_mex_create("y", NULL, 0, 0U, 1U, 0U, 2, 0, 0),
                  false);
  } else {
    sf_mex_assign(&c2_j_y, sf_mex_create("y", &c2_i_u, 0, 0U, 0U, 0U, 0), false);
  }

  sf_mex_setcell(c2_y, 8, c2_j_y);
  c2_j_u = chartInstance->c2_is_active_c2_MonitoringMode_HR_BD_debug_Fabian;
  c2_k_y = NULL;
  sf_mex_assign(&c2_k_y, sf_mex_create("y", &c2_j_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_setcell(c2_y, 9, c2_k_y);
  sf_mex_assign(&c2_st, c2_y, false);
  return c2_st;
}

static void set_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_st)
{
  const mxArray *c2_u;
  c2_u = sf_mex_dup(c2_st);
  *chartInstance->c2_ppgBeatSample_PositiveOffset = c2_emlrt_marshallIn
    (chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 0)),
     "ppgBeatSample_PositiveOffset");
  chartInstance->c2_Amp_max = c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c2_u, 1)), "Amp_max");
  chartInstance->c2_Amp_nxt = c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c2_u, 2)), "Amp_nxt");
  chartInstance->c2_Amp_pre = c2_g_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c2_u, 3)), "Amp_pre");
  chartInstance->c2_Idx_max = c2_i_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c2_u, 4)), "Idx_max");
  chartInstance->c2_Idx_ppg = c2_k_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell(c2_u, 5)), "Idx_ppg");
  chartInstance->c2_tempAmp_middle = c2_m_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c2_u, 6)), "tempAmp_middle");
  chartInstance->c2_tempAmp_nxt = c2_o_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c2_u, 7)), "tempAmp_nxt");
  chartInstance->c2_tempAmp_pre = c2_q_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell(c2_u, 8)), "tempAmp_pre");
  chartInstance->c2_is_active_c2_MonitoringMode_HR_BD_debug_Fabian =
    c2_s_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(c2_u, 9)),
    "is_active_c2_MonitoringMode_HR_BD_debug_Fabian");
  sf_mex_destroy(&c2_u);
  sf_mex_destroy(&c2_st);
}

static void finalize_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void sf_gateway_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  real_T c2_a;
  real_T c2_b;
  _sfTime_ = sf_get_time(chartInstance->S);
  if (!chartInstance->c2_Idx_max_not_empty) {
    chartInstance->c2_Idx_max = 0.0;
    chartInstance->c2_Idx_max_not_empty = true;
  }

  if (!chartInstance->c2_Amp_max_not_empty) {
    chartInstance->c2_Amp_max = 0.0;
    chartInstance->c2_Amp_max_not_empty = true;
  }

  if (!chartInstance->c2_Amp_pre_not_empty) {
    chartInstance->c2_Amp_pre = 0.0;
    chartInstance->c2_Amp_pre_not_empty = true;
  }

  if (!chartInstance->c2_Amp_nxt_not_empty) {
    chartInstance->c2_Amp_nxt = 0.0;
    chartInstance->c2_Amp_nxt_not_empty = true;
  }

  if (!chartInstance->c2_Idx_ppg_not_empty) {
    chartInstance->c2_Idx_ppg = 0.0;
    chartInstance->c2_Idx_ppg_not_empty = true;
  }

  if (!chartInstance->c2_tempAmp_middle_not_empty) {
    chartInstance->c2_tempAmp_middle = 0.0;
    chartInstance->c2_tempAmp_middle_not_empty = true;
  }

  if (!chartInstance->c2_tempAmp_pre_not_empty) {
    chartInstance->c2_tempAmp_pre = 0.0;
    chartInstance->c2_tempAmp_pre_not_empty = true;
  }

  if (!chartInstance->c2_tempAmp_nxt_not_empty) {
    chartInstance->c2_tempAmp_nxt = 0.0;
    chartInstance->c2_tempAmp_nxt_not_empty = true;
  }

  if ((!chartInstance->c2_Idx_max_not_empty) || (*chartInstance->c2_resetFlag))
  {
    chartInstance->c2_Idx_ppg++;
    *chartInstance->c2_ppgBeatSample_PositiveOffset = rtNaN;
    if (*chartInstance->c2_resetFlag) {
      c2_a = ((chartInstance->c2_Amp_pre + chartInstance->c2_Amp_nxt) - 2.0 *
              chartInstance->c2_Amp_max) * 0.5;
      c2_b = (chartInstance->c2_Amp_nxt - chartInstance->c2_Amp_pre) * 0.5;
      if ((muDoubleScalarAbs(c2_a) < muDoubleScalarAbs(c2_b)) || (c2_a == 0.0))
      {
        c2_a = 1.0;
        c2_b = 0.0;
      }

      *chartInstance->c2_ppgBeatSample_PositiveOffset =
        chartInstance->c2_Idx_ppg - (chartInstance->c2_Idx_max + -c2_b / (2.0 *
        c2_a));
      if (muDoubleScalarIsNaN(*chartInstance->c2_ppgBeatSample_PositiveOffset))
      {
        *chartInstance->c2_ppgBeatSample_PositiveOffset = 0.0;
      }
    }

    chartInstance->c2_Idx_ppg = 1.0;
    chartInstance->c2_Idx_max = 0.0;
    chartInstance->c2_Amp_max = rtNaN;
    chartInstance->c2_Amp_pre = rtNaN;
    chartInstance->c2_Amp_nxt = rtNaN;
    chartInstance->c2_tempAmp_pre = *chartInstance->c2_ppgIn;
    chartInstance->c2_Idx_ppg++;
  } else if (chartInstance->c2_Idx_ppg == 2.0) {
    chartInstance->c2_tempAmp_middle = *chartInstance->c2_ppgIn;
    chartInstance->c2_Idx_ppg++;
    *chartInstance->c2_ppgBeatSample_PositiveOffset = rtNaN;
  } else if (chartInstance->c2_Idx_ppg == 3.0) {
    chartInstance->c2_tempAmp_nxt = *chartInstance->c2_ppgIn;
    if ((chartInstance->c2_tempAmp_middle > chartInstance->c2_tempAmp_pre) &&
        (chartInstance->c2_tempAmp_middle > chartInstance->c2_tempAmp_nxt)) {
      chartInstance->c2_Idx_max = chartInstance->c2_Idx_ppg - 1.0;
      chartInstance->c2_Amp_max = chartInstance->c2_tempAmp_middle;
      chartInstance->c2_Amp_pre = chartInstance->c2_tempAmp_pre;
      chartInstance->c2_Amp_nxt = chartInstance->c2_tempAmp_nxt;
    }

    chartInstance->c2_Idx_ppg++;
    *chartInstance->c2_ppgBeatSample_PositiveOffset = rtNaN;
  } else {
    chartInstance->c2_tempAmp_pre = chartInstance->c2_tempAmp_middle;
    chartInstance->c2_tempAmp_middle = chartInstance->c2_tempAmp_nxt;
    chartInstance->c2_tempAmp_nxt = *chartInstance->c2_ppgIn;
    if ((chartInstance->c2_tempAmp_middle > chartInstance->c2_tempAmp_pre) &&
        (chartInstance->c2_tempAmp_middle > chartInstance->c2_tempAmp_nxt) &&
        ((chartInstance->c2_tempAmp_middle > chartInstance->c2_Amp_max) ||
         muDoubleScalarIsNaN(chartInstance->c2_Amp_max))) {
      chartInstance->c2_Idx_max = chartInstance->c2_Idx_ppg - 1.0;
      chartInstance->c2_Amp_max = chartInstance->c2_tempAmp_middle;
      chartInstance->c2_Amp_pre = chartInstance->c2_tempAmp_pre;
      chartInstance->c2_Amp_nxt = chartInstance->c2_tempAmp_nxt;
    }

    chartInstance->c2_Idx_ppg++;
    *chartInstance->c2_ppgBeatSample_PositiveOffset = rtNaN;
  }
}

static void mdl_start_c2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void initSimStructsc2_MonitoringMode_HR_BD_debug_Fabian
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber)
{
  (void)c2_machineNumber;
  (void)c2_chartNumber;
  (void)c2_instanceNumber;
}

const mxArray
  *sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_eml_resolved_functions_info(void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_createstruct("structure", 2, 10, 1),
                false);
  c2_info_helper(&c2_nameCaptureInfo);
  sf_mex_emlrtNameCapturePostProcessR2012a(&c2_nameCaptureInfo);
  return c2_nameCaptureInfo;
}

static void c2_info_helper(const mxArray **c2_info)
{
  const mxArray *c2_rhs0 = NULL;
  const mxArray *c2_lhs0 = NULL;
  const mxArray *c2_rhs1 = NULL;
  const mxArray *c2_lhs1 = NULL;
  const mxArray *c2_rhs2 = NULL;
  const mxArray *c2_lhs2 = NULL;
  const mxArray *c2_rhs3 = NULL;
  const mxArray *c2_lhs3 = NULL;
  const mxArray *c2_rhs4 = NULL;
  const mxArray *c2_lhs4 = NULL;
  const mxArray *c2_rhs5 = NULL;
  const mxArray *c2_lhs5 = NULL;
  const mxArray *c2_rhs6 = NULL;
  const mxArray *c2_lhs6 = NULL;
  const mxArray *c2_rhs7 = NULL;
  const mxArray *c2_lhs7 = NULL;
  const mxArray *c2_rhs8 = NULL;
  const mxArray *c2_lhs8 = NULL;
  const mxArray *c2_rhs9 = NULL;
  const mxArray *c2_lhs9 = NULL;
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(""), "context", "context", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("abs"), "name", "name", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "resolved",
                  "resolved", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1363713852U), "fileTimeLo",
                  "fileTimeLo", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 0);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 0);
  sf_mex_assign(&c2_rhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs0, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs0), "rhs", "rhs", 0);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs0), "lhs", "lhs", 0);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 1);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 1);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 1);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 1);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1395931856U), "fileTimeLo",
                  "fileTimeLo", 1);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 1);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 1);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 1);
  sf_mex_assign(&c2_rhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs1, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs1), "rhs", "rhs", 1);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs1), "lhs", "lhs", 1);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/abs.m"), "context",
                  "context", 2);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("eml_scalar_abs"), "name",
                  "name", 2);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 2);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elfun/eml_scalar_abs.m"),
                  "resolved", "resolved", 2);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1286818712U), "fileTimeLo",
                  "fileTimeLo", 2);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 2);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 2);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 2);
  sf_mex_assign(&c2_rhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs2, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs2), "rhs", "rhs", 2);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs2), "lhs", "lhs", 2);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(""), "context", "context", 3);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("rdivide"), "name", "name", 3);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 3);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "resolved",
                  "resolved", 3);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1363713880U), "fileTimeLo",
                  "fileTimeLo", 3);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 3);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 3);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 3);
  sf_mex_assign(&c2_rhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs3, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs3), "rhs", "rhs", 3);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs3), "lhs", "lhs", 3);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 4);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 4);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 4);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 4);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1395931856U), "fileTimeLo",
                  "fileTimeLo", 4);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 4);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 4);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 4);
  sf_mex_assign(&c2_rhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs4, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs4), "rhs", "rhs", 4);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs4), "lhs", "lhs", 4);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 5);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("eml_scalexp_compatible"),
                  "name", "name", 5);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 5);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_scalexp_compatible.m"),
                  "resolved", "resolved", 5);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1286818796U), "fileTimeLo",
                  "fileTimeLo", 5);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 5);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 5);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 5);
  sf_mex_assign(&c2_rhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs5, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs5), "rhs", "rhs", 5);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs5), "lhs", "lhs", 5);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/ops/rdivide.m"), "context",
                  "context", 6);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("eml_div"), "name", "name", 6);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 6);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "resolved",
                  "resolved", 6);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1386423952U), "fileTimeLo",
                  "fileTimeLo", 6);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 6);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 6);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 6);
  sf_mex_assign(&c2_rhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs6, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs6), "rhs", "rhs", 6);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs6), "lhs", "lhs", 6);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/eml/eml_div.m"), "context",
                  "context", 7);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("coder.internal.div"), "name",
                  "name", 7);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 7);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/coder/coder/+coder/+internal/div.p"), "resolved",
                  "resolved", 7);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1410807770U), "fileTimeLo",
                  "fileTimeLo", 7);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 7);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 7);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 7);
  sf_mex_assign(&c2_rhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs7, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs7), "rhs", "rhs", 7);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs7), "lhs", "lhs", 7);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(""), "context", "context", 8);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("isnan"), "name", "name", 8);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 8);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "resolved",
                  "resolved", 8);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1363713858U), "fileTimeLo",
                  "fileTimeLo", 8);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 8);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 8);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 8);
  sf_mex_assign(&c2_rhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs8, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs8), "rhs", "rhs", 8);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs8), "lhs", "lhs", 8);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/eml/lib/matlab/elmat/isnan.m"), "context",
                  "context", 9);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "coder.internal.isBuiltInNumeric"), "name", "name", 9);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut("double"), "dominantType",
                  "dominantType", 9);
  sf_mex_addfield(*c2_info, c2_emlrt_marshallOut(
    "[ILXE]$matlabroot$/toolbox/shared/coder/coder/+coder/+internal/isBuiltInNumeric.m"),
                  "resolved", "resolved", 9);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(1395931856U), "fileTimeLo",
                  "fileTimeLo", 9);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "fileTimeHi",
                  "fileTimeHi", 9);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeLo",
                  "mFileTimeLo", 9);
  sf_mex_addfield(*c2_info, c2_b_emlrt_marshallOut(0U), "mFileTimeHi",
                  "mFileTimeHi", 9);
  sf_mex_assign(&c2_rhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_assign(&c2_lhs9, sf_mex_createcellmatrix(0, 1), false);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_rhs9), "rhs", "rhs", 9);
  sf_mex_addfield(*c2_info, sf_mex_duplicatearraysafe(&c2_lhs9), "lhs", "lhs", 9);
  sf_mex_destroy(&c2_rhs0);
  sf_mex_destroy(&c2_lhs0);
  sf_mex_destroy(&c2_rhs1);
  sf_mex_destroy(&c2_lhs1);
  sf_mex_destroy(&c2_rhs2);
  sf_mex_destroy(&c2_lhs2);
  sf_mex_destroy(&c2_rhs3);
  sf_mex_destroy(&c2_lhs3);
  sf_mex_destroy(&c2_rhs4);
  sf_mex_destroy(&c2_lhs4);
  sf_mex_destroy(&c2_rhs5);
  sf_mex_destroy(&c2_lhs5);
  sf_mex_destroy(&c2_rhs6);
  sf_mex_destroy(&c2_lhs6);
  sf_mex_destroy(&c2_rhs7);
  sf_mex_destroy(&c2_lhs7);
  sf_mex_destroy(&c2_rhs8);
  sf_mex_destroy(&c2_lhs8);
  sf_mex_destroy(&c2_rhs9);
  sf_mex_destroy(&c2_lhs9);
}

static const mxArray *c2_emlrt_marshallOut(const char * c2_u)
{
  const mxArray *c2_y = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", c2_u, 15, 0U, 0U, 0U, 2, 1, strlen
    (c2_u)), false);
  return c2_y;
}

static const mxArray *c2_b_emlrt_marshallOut(const uint32_T c2_u)
{
  const mxArray *c2_y = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 7, 0U, 0U, 0U, 0), false);
  return c2_y;
}

static real_T c2_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_ppgBeatSample_PositiveOffset, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_ppgBeatSample_PositiveOffset), &c2_thisId);
  sf_mex_destroy(&c2_b_ppgBeatSample_PositiveOffset);
  return c2_y;
}

static real_T c2_b_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d0, 1, 0, 0U, 0, 0U, 0);
  c2_y = c2_d0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_c_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Amp_max, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_Amp_max),
    &c2_thisId);
  sf_mex_destroy(&c2_b_Amp_max);
  return c2_y;
}

static real_T c2_d_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d1;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_Amp_max_not_empty = false;
  } else {
    chartInstance->c2_Amp_max_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d1, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d1;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_e_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Amp_nxt, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_Amp_nxt),
    &c2_thisId);
  sf_mex_destroy(&c2_b_Amp_nxt);
  return c2_y;
}

static real_T c2_f_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d2;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_Amp_nxt_not_empty = false;
  } else {
    chartInstance->c2_Amp_nxt_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d2, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d2;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_g_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Amp_pre, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_Amp_pre),
    &c2_thisId);
  sf_mex_destroy(&c2_b_Amp_pre);
  return c2_y;
}

static real_T c2_h_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d3;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_Amp_pre_not_empty = false;
  } else {
    chartInstance->c2_Amp_pre_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d3, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d3;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_i_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Idx_max, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_j_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_Idx_max),
    &c2_thisId);
  sf_mex_destroy(&c2_b_Idx_max);
  return c2_y;
}

static real_T c2_j_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d4;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_Idx_max_not_empty = false;
  } else {
    chartInstance->c2_Idx_max_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d4, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d4;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_k_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_Idx_ppg, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_l_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_Idx_ppg),
    &c2_thisId);
  sf_mex_destroy(&c2_b_Idx_ppg);
  return c2_y;
}

static real_T c2_l_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d5;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_Idx_ppg_not_empty = false;
  } else {
    chartInstance->c2_Idx_ppg_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d5, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d5;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_m_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_tempAmp_middle, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_n_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_tempAmp_middle),
    &c2_thisId);
  sf_mex_destroy(&c2_b_tempAmp_middle);
  return c2_y;
}

static real_T c2_n_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d6;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_tempAmp_middle_not_empty = false;
  } else {
    chartInstance->c2_tempAmp_middle_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d6, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d6;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_o_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_tempAmp_nxt, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_p_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_tempAmp_nxt),
    &c2_thisId);
  sf_mex_destroy(&c2_b_tempAmp_nxt);
  return c2_y;
}

static real_T c2_p_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d7;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_tempAmp_nxt_not_empty = false;
  } else {
    chartInstance->c2_tempAmp_nxt_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d7, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d7;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static real_T c2_q_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_tempAmp_pre, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_r_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_tempAmp_pre),
    &c2_thisId);
  sf_mex_destroy(&c2_b_tempAmp_pre);
  return c2_y;
}

static real_T c2_r_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d8;
  if (mxIsEmpty(c2_u)) {
    chartInstance->c2_tempAmp_pre_not_empty = false;
  } else {
    chartInstance->c2_tempAmp_pre_not_empty = true;
    sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d8, 1, 0, 0U, 0, 0U, 0);
    c2_y = c2_d8;
  }

  sf_mex_destroy(&c2_u);
  return c2_y;
}

static uint8_T c2_s_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_b_is_active_c2_MonitoringMode_HR_BD_debug_Fabian, const char_T
   *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = c2_identifier;
  c2_thisId.fParent = NULL;
  c2_y = c2_t_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_active_c2_MonitoringMode_HR_BD_debug_Fabian), &c2_thisId);
  sf_mex_destroy(&c2_b_is_active_c2_MonitoringMode_HR_BD_debug_Fabian);
  return c2_y;
}

static uint8_T c2_t_emlrt_marshallIn
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance, const
   mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void init_dsm_address_info
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address
  (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance)
{
  chartInstance->c2_ppgIn = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c2_ppgBeatSample_PositiveOffset = (real_T *)
    ssGetOutputPortSignal_wrapper(chartInstance->S, 1);
  chartInstance->c2_resetFlag = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 1);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

void sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(493336301U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(327439061U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3661137185U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1168445038U);
}

mxArray* sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_post_codegen_info(void);
mxArray *sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_autoinheritance_info(void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("BhVRJ4WjjDzwOs0vSblZh");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,2,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,1,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,2,mxREAL);
      double *pr = mxGetPr(mxSize);
      pr[0] = (double)(1);
      pr[1] = (double)(1);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt" };

      mxArray *mxType = mxCreateStructMatrix(1,1,2,typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxCreateDoubleMatrix(0,0,mxREAL));
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c2_MonitoringMode_HR_BD_debug_Fabian_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_MonitoringMode_HR_BD_debug_Fabian_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "incompatibleSymbol", };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 3, infoFields);
  mxArray *fallbackReason = mxCreateString("feature_off");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxArray *fallbackType = mxCreateString("early");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c2_MonitoringMode_HR_BD_debug_Fabian_updateBuildInfo_args_info(void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray *sf_get_sim_state_info_c2_MonitoringMode_HR_BD_debug_Fabian
  (void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[5],T\"ppgBeatSample_PositiveOffset\",},{M[4],M[0],T\"Amp_max\",S'l','i','p'{{M1x2[424 431],M[0],}}},{M[4],M[0],T\"Amp_nxt\",S'l','i','p'{{M1x2[442 449],M[0],}}},{M[4],M[0],T\"Amp_pre\",S'l','i','p'{{M1x2[433 440],M[0],}}},{M[4],M[0],T\"Idx_max\",S'l','i','p'{{M1x2[415 422],M[0],}}},{M[4],M[0],T\"Idx_ppg\",S'l','i','p'{{M1x2[451 458],M[0],}}},{M[4],M[0],T\"tempAmp_middle\",S'l','i','p'{{M1x2[460 474],M[0],}}},{M[4],M[0],T\"tempAmp_nxt\",S'l','i','p'{{M1x2[489 500],M[0],}}},{M[4],M[0],T\"tempAmp_pre\",S'l','i','p'{{M1x2[476 487],M[0],}}},{M[8],M[0],T\"is_active_c2_MonitoringMode_HR_BD_debug_Fabian\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 10, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_MonitoringMode_HR_BD_debug_Fabian_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const char* sf_get_instance_specialization(void)
{
  return "3YmJaFrzHc1koVIwqsOalE";
}

static void sf_opaque_initialize_c2_MonitoringMode_HR_BD_debug_Fabian(void
  *chartInstanceVar)
{
  initialize_params_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
  initialize_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_enable_c2_MonitoringMode_HR_BD_debug_Fabian(void
  *chartInstanceVar)
{
  enable_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_disable_c2_MonitoringMode_HR_BD_debug_Fabian(void
  *chartInstanceVar)
{
  disable_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
}

static void sf_opaque_gateway_c2_MonitoringMode_HR_BD_debug_Fabian(void
  *chartInstanceVar)
{
  sf_gateway_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
}

static const mxArray*
  sf_opaque_get_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian(SimStruct* S)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  return get_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*)
     chartInfo->chartInstance);        /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
  (SimStruct* S, const mxArray *st)
{
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
  ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
  set_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*)
     chartInfo->chartInstance, st);
}

static void sf_opaque_terminate_c2_MonitoringMode_HR_BD_debug_Fabian(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*)
                    chartInstanceVar)->S;
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_MonitoringMode_HR_BD_debug_Fabian_optimization_info();
    }

    finalize_c2_MonitoringMode_HR_BD_debug_Fabian
      ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
    utFree(chartInstanceVar);
    if (crtInfo != NULL) {
      utFree(crtInfo);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_MonitoringMode_HR_BD_debug_Fabian
    ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*) chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_MonitoringMode_HR_BD_debug_Fabian(SimStruct *
  S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)(ssGetUserData(S));
    ChartInfoStruct * chartInfo = (ChartInfoStruct *)(crtInfo->instanceInfo);
    initialize_params_c2_MonitoringMode_HR_BD_debug_Fabian
      ((SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct*)
       (chartInfo->chartInstance));
  }
}

static void mdlSetWorkWidths_c2_MonitoringMode_HR_BD_debug_Fabian(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct =
      load_MonitoringMode_HR_BD_debug_Fabian_optimization_info();
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,sf_rtw_info_uint_prop(sf_get_instance_specialization(),
                infoStruct,2,"RTWCG"));
    ssSetEnableFcnIsTrivial(S,1);
    ssSetDisableFcnIsTrivial(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,2,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_update_buildInfo(sf_get_instance_specialization(),infoStruct,2);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,2,2);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,2,1);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=1; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 2; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,2);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(1549245237U));
  ssSetChecksum1(S,(3016953148U));
  ssSetChecksum2(S,(2198486882U));
  ssSetChecksum3(S,(1125298285U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c2_MonitoringMode_HR_BD_debug_Fabian(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Embedded MATLAB");
  }
}

static void mdlStart_c2_MonitoringMode_HR_BD_debug_Fabian(SimStruct *S)
{
  SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *chartInstance;
  ChartRunTimeInfo * crtInfo = (ChartRunTimeInfo *)utMalloc(sizeof
    (ChartRunTimeInfo));
  chartInstance = (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct *)
    utMalloc(sizeof(SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct));
  memset(chartInstance, 0, sizeof
         (SFc2_MonitoringMode_HR_BD_debug_FabianInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 1;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW = mdlRTW_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.mdlStart =
    mdlStart_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c2_MonitoringMode_HR_BD_debug_Fabian;
  chartInstance->chartInfo.extModeExec = NULL;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->S = S;
  crtInfo->checksum = SF_RUNTIME_INFO_CHECKSUM;
  crtInfo->instanceInfo = (&(chartInstance->chartInfo));
  crtInfo->isJITEnabled = false;
  crtInfo->compiledInfo = NULL;
  ssSetUserData(S,(void *)(crtInfo));  /* register the chart instance with simstruct */
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  sf_opaque_init_subchart_simstructs(chartInstance->chartInfo.chartInstance);
}

void c2_MonitoringMode_HR_BD_debug_Fabian_method_dispatcher(SimStruct *S, int_T
  method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_MonitoringMode_HR_BD_debug_Fabian(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_MonitoringMode_HR_BD_debug_Fabian(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_MonitoringMode_HR_BD_debug_Fabian(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_MonitoringMode_HR_BD_debug_Fabian_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
