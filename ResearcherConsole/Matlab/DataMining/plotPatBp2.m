function plotPatBp2 = plotPatBp2(data, patType, trackOffset)
if isfield(data,'timestamp')
    data.timestamps = data.timestamp;
end
if ~exist('trackOffset','var'),
    trackOffset = 4;
end

if ~exist('beatOffset', 'var'),
    beatOffset = data.timestamps(1);
end

if ~exist('patType', 'var'),
    patType = 'raw';
end

plotPatBp(data, patType, trackOffset);