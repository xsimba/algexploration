function biosemBandDebug1(data, polarRefData)

%
% biosem debug plot.  Top plot is data out of the M4,
% bottom plot is CI processing in the A7
%
if ~exist('label', 'var'),
    label = '';
end

%
% verify that all necessary channels are present
%
colring = 'kbgrmggrgbgrmggrg';
%colring = 'kbgrmggrg';
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
tracks = {'ecg', tracksPPG{:}};
ax = []


figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0.48 1 0.45]);

axnum = 1;
for i = 2:9,
    trackNum = i;
    trackName = tracks{i};
    flag = isfield(data.ppg, trackName(end));
    if (flag),
    eval(['flag = flag && isfield(data.',trackName,', ''band_hr_Biosem'');']);
    eval(['flag = flag && isfield(data.',trackName,', ''band_hr_BiosemSigma'');']);
    end
    if (flag == 1),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['hr = data.',trackName,'.band_hr_Biosem.signal;']);
        eval(['t_hr = data.',trackName,'.band_hr_Biosem.timestamps;']);
        eval(['sigma = data.',trackName,'.band_hr_BiosemSigma.signal;']);
        eval(['t_sigma = data.',trackName,'.band_hr_BiosemSigma.timestamps;']);
        plot(t_hr, hr, colring(trackNum));
        hold on;
        if (length(t_sigma) == length(t_hr)),
            plot(t_hr, hr+sigma, [colring(trackNum), '--']);
            plot(t_hr, hr-sigma, [colring(trackNum), '--']);
        else
            sigmaInterp = interp1(t_sigma, sigma, t_hr);
            plot(t_hr, hr+sigmaInterp, [colring(trackNum), '--']);
            plot(t_hr, hr-sigmaInterp, [colring(trackNum), '--']);
        end
        ylabel(trackName);
        if (i==1 || i==3);
            t = title(label);
            set (t, 'Interpreter', 'none');
        end
        set(gca, 'YLim', [0 250]);
    end
end


figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0 1 0.45]);

for i = 2:9,
    trackNum = i;
    trackName = tracks{i};
    flag = isfield(data.ppg, trackName(end));
    if (flag),
    eval(['flag = flag && isfield(data.',trackName,', ''band_CIraw'');']);
    eval(['flag = flag && isfield(data.',trackName,', ''band_hr_BiosemCI'');']);
    end
    if (flag == 1),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['t_ciraw = data.',trackName,'.band_CIraw.timestamps;']);
        eval(['ciraw = data.',trackName,'.band_CIraw.signal;']);
        eval(['t_hrci = data.',trackName,'.band_hr_BiosemCI.timestamps;']);
        eval(['hrci = data.',trackName,'.band_hr_BiosemCI.signal;']);
        plot(t_ciraw, ciraw, [colring(trackNum), 'o']);
        hold on;
        plot(t_hrci, hrci, ['kx']);
        ylabel(trackName);
        if (i==1 || i==3);
            t = title(label);
            set (t, 'Interpreter', 'none');
        end
        set(gca, 'YLim', [0 5]);
    end
end


ciring = {[1,1,1]*0.8, ...
    [1,1,1]*0.6, ...
    [1,1,1]*0.4, ...
    [1,1,1]*0.2, ...
    [1,1,1]*0, ...
    };
figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 .25 1 0.45]);
for i = 2:9,
    trackNum = i;
    trackName = tracks{i};
    flag = isfield(data.ppg, trackName(end));
    if (flag),
    eval(['flag = flag && isfield(data.',trackName,', ''band_hr_Biosem'');']);
    eval(['flag = flag && isfield(data.',trackName,', ''band_hr_BiosemCI'');']);
    end
    if (flag == 1),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['hr = data.',trackName,'.band_hr_Biosem.signal;']);
        eval(['t_hr = data.',trackName,'.band_hr_Biosem.timestamps;']);
        eval(['t_hrci = data.',trackName,'.band_hr_BiosemCI.timestamps;']);
        eval(['hrci = data.',trackName,'.band_hr_BiosemCI.signal;']);
        if (length(t_hrci) ~= length(t_hr)),
            hrci = interp1(t_hrci, hrci, t_hr, 'nearest');
        end
        for k = 1:5,
            ind = find(hrci == k);
            if (length(ind)>0),
                plot(t_hr(ind), hr(ind), '.', 'MarkerSize', 14, ...
                    'Color', ciring{k});
                hold on;
            end
        end
        plot(polarRefData)
        hold on;
        ylabel(trackName);
        if (i==1 || i==3);
            t = title(label);
            set (t, 'Interpreter', 'none');
        end
        set(gca, 'YLim', [40 250]);
    end
end

% if (exist(ax))
%     linkaxes([ax{:}], 'x');
% end

