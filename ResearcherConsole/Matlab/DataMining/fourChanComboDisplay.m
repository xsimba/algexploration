function fourChanComboDisplay(data, label, beatMetric, beatOffset, trackOffset, ...
                              showTrack)

accToG_Conv = 1/2^12;

%
% create an interactive dashboard to display 4 channels of raw PPG,
%
if isfield(data,'timestamp')
    data.timestamps = data.timestamp;
end

if ~exist('showTrack', 'var'),
    showTrack = 5;
end

if ~exist('label', 'var'),
    label = '';
end

if ~exist('beatMetric', 'var'),
    beatMetric = 'ibi';
end

if ~exist('beatOffset', 'var'),
    beatOffset = data.timestamps(1);
end

if ~exist('trackOffset', 'var'),
    trackOffset = 0;
end

thisTrack = showTrack;

%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
colring = 'kbgrmggkgbgrmggkg';
%colring = 'kbgrmggrg';
metricsPPG = {'signal', 'beats', beatMetric, 'CI_times', 'CI_raw'};
metricsECG = metricsPPG(1:3);
tracks = {'ecg', tracksPPG{:}};

try
    curTrack = 'ecg';
    for j = 1:length(metricsECG),
        metric = metricsECG{j};
        evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
        eval(evalString);
    end
    
    for i = 1:length(tracksPPG),
        curTrack = tracksPPG{i};
        for j = 1:length(metricsPPG),
            metric = metricsPPG{j};
            evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
            eval(evalString);
        end
    end
    assert(isfield(data.acc,'x'));
    assert(isfield(data.acc,'y'));
    assert(isfield(data.acc,'z'));
catch
    disp(['track: ',curTrack, '    metric:', metric]);
    error('fourChanComboDisplay: inputs missing some tracks of data');
end


%
% 
%

figure(1);
%figure('Color',[0.8 0.8 0.8]);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0 0.33 1]);

for i = 1:4,
    trackNum = i+trackOffset;
    trackName = tracksPPG{i+trackOffset};
    ax{i} = subplot(4,1,i);
    eval(['sig = data.',trackName,'.signal;']);
    eval(['beat = beatOffset+data.',trackName,'.beats(1,:);']);
    eval(['beatAmp = data.',trackName,'.beatAmp;']);
    plot(data.timestamps, sig, colring(1+trackNum));

    hold on;
    plot(beat, beatAmp, 'kx', 'MarkerSize', 14);
    hold on;
    try
    eval(['band_beats = data.',trackName,'.band_beats']);
    band_beatAmp = interp1(data.timestamps, sig, band_beats.timestamps);
    plot(band_beats.timestamps, band_beatAmp, [colring(1+trackNum),'o']);
    catch
    end
    ylabel(trackName);
end

figure(2);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.33 0 0.33 1]);

for i = 1:4,
    trackNum = i+trackOffset;
    trackName = tracksPPG{i+trackOffset};
    ax{i+4} = subplot(4,1,i);
    eval(['CIraw_time = data.',trackName,'.CI_times;']);
    eval(['CIraw = data.',trackName,'.CI_raw;']);
    eval(['beats = data.',trackName,'.beats(1,:);']);
    eval(['CIbeat = data.',trackName,'.CI_beat;']);
    plot(CIraw_time, CIraw, [colring(trackNum+1),'o']);
    hold on;
    minCIB = min([length(CIbeat),length(beats(1,:))]);
%    plot(beats(1,1:minCIB), CIbeat(1:minCIB), 'x', 'MarkerSize', 14 );

    set(gca, 'YLim', [0 4.5]);
    ylabel(['CI ',trackName]);
end

if trackOffset == 0,
    figure(3);
    clf
    set(gcf, 'Units', 'Normalized');
    set(gcf, 'Position', [0.66 0 0.33 1]);
    ax{9} = subplot(311);

    for j = 1:5,
        curTrack = tracks{j};
        eval(['thisIbi = data.',curTrack,'.', beatMetric, ';']);
        eval(['thisTime = data.',curTrack,'.beats(1,2:end)', ';']);
        if j == 1,
            sym = 'x';
        else
            sym = '.';
        end
        plot(thisTime, 60./thisIbi, [colring(j),sym], 'MarkerSize', 14);
        hold on;
        xlabel('time');
        ylabel('instantaneous HR [bpm]');
    end
    set (gca, 'YLim', [0 200]);
    title(label);

    ax{10} = subplot(312);

    curTrack = tracks{thisTrack};
    eval(['thisIbi = data.',curTrack,'.', beatMetric, ';']);
    eval(['thisTime = data.',curTrack,'.beats(1,2:end)', ';']);
    plot(thisTime, 60./thisIbi, [colring(thisTrack),'.'], 'MarkerSize', 14);

    hold on;
    xlabel('time');
    ylabel('instantaneous HR [bpm]');
    title('selected channel');
    set (gca, 'YLim', [0 200]);

    ax{11} = subplot(313);
    accelMag = [];
    for i = 1:length(data.acc.x.signal),
        accelMag(i) = norm([data.acc.x.signal(i), ...
            data.acc.y.signal(i), ...
            data.acc.z.signal(i)], 2) * accToG_Conv;
    end
    plot(data.timestamps, accelMag, 'x', 'MarkerSize', 14);
    xlabel('time');
    ylabel('acceleration magnitude [g]');

    linkaxes([ax{:}], 'x');
else
    figure(3);
    clf;
    set(gcf, 'Units', 'Normalized');
    set(gcf, 'Position', [0.66 0 0.33 1]);
    ax{9} = subplot(311);
    
    for i = 2:5,
        trackNum = i+trackOffset;
        curTrack = tracks{trackNum};
        eval(['thisIbi = data.',curTrack,'.', beatMetric, ';']);
        eval(['thisTime = data.',curTrack,'.beats(1,2:end)', ';']);
        if j == 1,
            sym = 'x';
        else
            sym = '.';
        end
        plot(thisTime, 60./thisIbi, [colring(trackNum),sym], 'MarkerSize', 14);
        hold on;
        xlabel('time');
        ylabel('instantaneous HR [bpm]');
    end
    set (gca, 'YLim', [0 200]);
    title(label);

    ax{10} = subplot(312);

    curTrack = tracks{thisTrack};
    eval(['thisIbi = data.',curTrack,'.', beatMetric, ';']);
    eval(['thisTime = data.',curTrack,'.beats(1,2:end)', ';']);
    plot(thisTime, 60./thisIbi, [colring(thisTrack),'.'], 'MarkerSize', 14);

    hold on;
    xlabel('time');
    ylabel('instantaneous HR [bpm]');
    title('selected channel');
    set (gca, 'YLim', [0 200]);

    ax{11} = subplot(313);
    accelMag = [];
    for i = 1:length(data.acc.x.signal),
        accelMag(i) = norm([data.acc.x.signal(i), ...
            data.acc.y.signal(i), ...
            data.acc.z.signal(i)], 2) * accToG_Conv;
    end
    plot(data.timestamps, accelMag, 'x', 'MarkerSize', 14);
    xlabel('time');
    ylabel('acceleration magnitude [g]');

    linkaxes([ax{:}], 'x');
end
