function okCode = plotBandBeats( data,signal,spotMode )

        if ~exist('spotMode','var')
            spotMode = 0 ;
        end
    
        okCode = 0 ; 
        
        if strcmpi(signal,'ecg')
           
            if isfield(data.(signal),'band_beats_CI') 
                
                s = data.ecg.signal;
                s_time = data.timestamps;
                bandBeats_time = data.ecg.band_beats_CI.timestamps;
                
                
                figure('units','normalized','outerposition',[0 0 1 1])
                if spotMode && isfield(data,'spot_check')
                    s = zscore(s);
                    bandBeats_Amp = interp1(s_time,s,bandBeats_time);
                    plot(data.spot_check.timestamps,data.spot_check.signal,'r','LineWidth',2);hold on
                    legend('SpotCheck Enabled')
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(bandBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                else
                    bandBeats_Amp = interp1(s_time,s,bandBeats_time);
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(bandBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                end
                
                title(upper(signal));

            end
        elseif strcmpi(signal(1:3),'ppg')
            channel = lower(signal(end));
            if isfield(data.ppg.(channel),'band_beats') 
                                
                s = data.ppg.(channel).signal;
                s_time = data.timestamps;
                bandBeats_time = data.ppg.(channel).band_beats.timestamps;
                
                
                figure('units','normalized','outerposition',[0 0 1 1])
                if spotMode && isfield(data,'spot_check')
                    s = zscore(s);
                    bandBeats_Amp = interp1(s_time,s,bandBeats_time);
                    plot(data.spot_check.timestamps,data.spot_check.signal,'r','LineWidth',2);hold on
                    legend('SpotCheck Enabled')
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(bandBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                else
                    bandBeats_Amp = interp1(s_time,s,bandBeats_time);
                    plot(s_time,s,'LineWidth',2);hold on
                    plot(bandBeats_time,bandBeats_Amp,'or','MarkerSize',10);
                end
                
                title(upper(signal))
            end
            
        end
        
        okCode = 1 ;
end

