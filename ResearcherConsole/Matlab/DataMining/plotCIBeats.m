function okCode = plotCIBeats( data,signal )
    
        okCode = 0 ;  
        
        col = [0.8242    0.8242    0.8242;
               0.4375    0.5000    0.5625; 
               1.0000    0.8398         0;
               1.0000    0.4961    0.3125;
               0.6016    0.8008    0.1953];
       
        if strcmpi(signal,'ecg')
            
            figure('units','normalized','outerposition',[0 0 1 1])
            
            if isfield(data.ecg ,'CI_beat') && ~isfield(data.ecg ,'CI_beat_c')

                s       = data.ecg.signal;
                beats_m = round(128*data.ecg.beats) ;
                ci_m    = data.ecg.CI_beat ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_m>=idx & beats_m<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_m(indici(jdx)),s(beats_m(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_m(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI Beats')
                title(signal)
                
            
            elseif ~isfield(data.ecg ,'CI_beat') && isfield(data.ecg ,'CI_beat_c')

                s       = data.ecg.signal;
                beats_c = round(128*data.ecg.beats_c) ;
                ci_c    = data.ecg.CI_beat_c ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_c>=idx & beats_c<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_c(indici(jdx)),s(beats_c(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_c(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI Beats')
                title(signal)
            
            elseif isfield(data.ecg ,'CI_beat') && isfield(data.ecg ,'CI_beat_c')

                s = data.ecg.signal;
                beats_m = round(128*data.ecg.beats) ;
                ci_m    = data.ecg.CI_beat ;
                beats_c = round(128*data.ecg.beats_c) ;
                ci_c    = data.ecg.CI_beat_c ;
                
                ax1 = subplot(211);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_m>=idx & beats_m<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_m(indici(jdx)),s(beats_m(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_m(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI Beats');
                
                ax2 = subplot(212);

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_c>=idx & beats_c<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_c(indici(jdx)),s(beats_c(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_c(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI Beats')
                title(signal)
                
                title(ax1,signal)
                title(ax2,signal)
                linkaxes([ax1,ax2],'xy');
                
            end
            
        elseif strcmpi(signal(1:3),'ppg')
            channel = lower(signal(end));
            figure('units','normalized','outerposition',[0 0 1 1])
            
            if isfield(data.ppg.(channel),'CI_beat') && ~isfield(data.ppg.(channel),'CI_beat_c')

                s       = data.ppg.(channel).signal;
                beats_m = round(128*data.ppg.(channel).beats(:)) ;
                ci_m    = data.ppg.(channel).CI_beat(:) ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_m>=idx & beats_m<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_m(indici(jdx)),s(beats_m(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_m(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI Beats')
                title(signal)
                
            
            elseif ~isfield(data.ppg.(channel) ,'CI_beat') && isfield(data.ppg.(channel) ,'CI_beat_c')

                s       = data.ppg.(channel).signal;
                beats_c = round(128*data.ppg.(channel).beats_C(:)) ;
                ci_c    = data.ppg.(channel).CI_beats_c(:) ;

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;

                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_c>=idx & beats_c<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_c(indici(jdx)),s(beats_c(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_c(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('C CI Beats')
                title(signal)
            
            elseif isfield(data.ppg.(channel) ,'CI_beat') && isfield(data.ppg.(channel) ,'CI_beat_c')

                s = data.ppg.(channel).signal;
                beats_m = round(128*data.ppg.(channel).beats(:)) ;
                ci_m    = data.ppg.(channel).CI_beat(:) ;
                beats_c = round(128*data.ppg.(channel).beats_c(:)) ;
                ci_c    = data.ppg.(channel).CI_beat_c(:) ;
                
                ax1 = subplot(211);
                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_m>=idx & beats_m<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_m(indici(jdx)),s(beats_m(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_m(indici(jdx)),:));
                            catch err
                                disp(err);%keyboard()
                            end
                        end
                        k = k + 1;
                    catch err
                    end
                end 
                ylabel('Matlab CI Beats');
                
                ax2 = subplot(212);

                plot(1,s(1),'color',col(1,:));hold on;
                plot(1,s(1),'color',col(2,:));hold on;
                plot(1,s(1),'color',col(3,:));hold on;
                plot(1,s(1),'color',col(4,:));hold on;
                plot(1,s(1),'color',col(5,:));hold on;
                
                [~,objh,~,~] =legend({'1','2','3','4','5'});
                set(objh,'linewidth',2.3);

                k = 1;
                for idx = 1:128:length(s)
                    try
                        plot(idx:idx+128,s(idx:idx+128),'LineWidth',1.7);hold on;
                        indici = find(beats_c>=idx & beats_c<(idx+128));
                        for jdx = 1:numel(indici)
                            try
                                plot(beats_c(indici(jdx)),s(beats_c(indici(jdx))),'o','MarkerSize',10,'MarkerFaceColor',col(ci_c(indici(jdx)),:));
                            catch err
                            end
                        end
                        k = k + 1;
                    catch err
                        disp(err);%keyboard()
                    end
                end 
                ylabel('C CI Beats')
                title(signal)
                
                title(ax1,signal)
                title(ax2,signal)
                linkaxes([ax1,ax2],'xy');
                
            end
        end
        
        okCode = 1 ;
end

