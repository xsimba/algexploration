%cd('C:\Users\m.wiggins\Documents\SimbandStuff\DataAnalysis\')

%setRCpath
RC_setup

time_comp = 1.5;


%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.15;
bioLimits.rateLimitUp = 0.15;
bioLimits.windowSize = 10;
bioLimits.sigmaCutoff = 1.5;


 c = loadSessionData('\\105.140.2.7\share\DataAnalysis\BioSemanticDevel-server2.xlsx');

% c = loadSessionData('.\BioSemanticDevel_Sessions.xlsx');


sessionList={};
%sessionList = {sessionList{:}, '10232014_sitstand'};
% sessionList = {sessionList{:}, '11042014_execWalk2'};
% sessionList = {sessionList{:}, '11112014_Yelei_2_hilb'}; % very good data quality
% sessionList = {sessionList{:}, '11182014_yelei2_hilb'}; % light motion and walking
% sessionList = {sessionList{:}, '11102014_Peter_debug'}; % mostly sitting, some motion in the middle, low resting HR
% sessionList = {sessionList{:}, '11252014_dnr'}; %
% sessionList = {sessionList{:}, '20141204_AlgoInitTest'}; % Initialization/Start of Algorithms
%sessionList = {sessionList{:}, '12082014_test1'};
%sessionList = {sessionList{:}, '12082014_test2'};
%sessionList = {sessionList{:}, '12082014_test3'};
%sessionList = {sessionList{:}, '12082014_test4'};
%sessionList = {sessionList{:}, '12082014_test5'};
%sessionList = {sessionList{:}, '12082014_test6'};
%sessionList = {sessionList{:}, '12082014_test7'};
%sessionList = {sessionList{:}, '01092015_test1'};
sessionList = {sessionList{:}, '01092015_test3'};

%RMStimeRange ={[12 632.6], [12 426.9], [12 589.3], [12 543.5], [12 541.5], [12 537], [12 742.2]};
%RMStimeRange = {[0 271]};
if (0),
    AddBiosemTracks(c, sessionList, 'ibi'); % Beat base HR
else
    InitializeTracksFromCSV(c, sessionList);
    %AddRefData(c, sessionList);
    
%      AddTracks(c, sessionList);
    AddInstFreqHRTracks(c, sessionList); % Hilbert based HR
    AddTdeInterbeatTracks_simu(c, sessionList) % TDE based HR
    
    %AddBiosemTracks(c, sessionList, 'ibi_hilbert');
    %AddHRrefInferredTimestamps(c, sessionList);
    %HRRMSErrors = AddHRPerformanceMetricsTracks(c, sessionList, RMStimeRange);
    
end

tracks = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'}; % ???????????????????????????

for i = 1:length(sessionList)
    
    fMetrics = setMetricsSessionData(c, sessionList{i});
    load(fMetrics);
    %data = motionMasking(data);
    
    for j = 1:length(tracks)
        eval(['ibi_hilbert = data.', tracks{j}, '.ibi_hilbert;']);
        eval(['time_hilbert = data.', tracks{j}, '.hilbert_timestamps;']);
        
        eval(['ibi_tde = data.', tracks{j}, '.tde_ibi.ibi;']);
        eval(['time_tde = data.', tracks{j}, '.tde_ibi.timestamps;']);
        
        [fusion_timestamps, indx] =sort([time_hilbert; time_comp+time_tde]);   % '881 sample points' is manually tuned to align with ibi_hilbert!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ibi_fusion = [ibi_hilbert; ibi_tde ];
        ibi_fusion = ibi_fusion(indx);
        
        eval(['data.', tracks{j}, '.hilbert_fusion.timestamps = fusion_timestamps;']);
        eval(['data.', tracks{j}, '.hilbert_fusion.ibi = ibi_fusion;']);
        
%         data = computeBiosemBeatFilter_fusion(data, tracks{j}, bioLimits, 'hilbert_fusion');
%         data = computeBiosemStaticStat(data, tracks{j}, 'biosemInterbeats');
%         data = computeBiosemQuality(data, tracks{j});
    end
    save (fMetrics, 'data');
    AddBiosemTracks(c,sessionList,'hilbert_fusion');
    %HRRMSErrors = AddHRPerformanceMetricsTracks(c, sessionList, RMStimeRange);
%     save (metricsFilename, 'data');
    
    
    %     data = computeBiosemBeatFilter_fusion(data, 'ibi_hilbert');
%     [data,HRRMSError] = HRPerformanceMetrics(data, RMStimeRange);
    fMetrics = setMetricsSessionData(c, sessionList{i});
    load(fMetrics);
    %fusionQualityPlot(data);
    %heartRateQualityPlot_tde(data,data.HR_ref.signal,sessionList{i});
    
    
    
    %     hilbertQualityPlot(data, sessionList{i})
    % %     naiveDataDisplay(data);
    %     plotHREstError(data);
    %     pause
    %
    %     close all
end







