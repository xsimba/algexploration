function plotSimbandAccels(data)

figure;
colring = 'brgk';

timestamps = data.timestamps;

tracksAccel = {'accel.x', 'accel.y', 'accel.z'};
trackAccelStr = {'X', 'Y', 'Z', 'Mag'};

accelMagIdx = length(tracksAccel)+1;
accel = cell(accelMagIdx,1); accel{accelMagIdx} = zeros(size(timestamps));

for i =1:length(tracksAccel)
    accel{i} = eval(['data.', tracksAccel{i}, '.signal']);
    accel{accelMagIdx} = accel{accelMagIdx} + accel{i}.^2;
end
accel{accelMagIdx} = sqrt(accel{accelMagIdx});

for i = 1:(length(accel))
    plotAxes(i) = subplot(length(accel), 1, i);
    plot(timestamps, accel{i}, colring(i))
    ylabel(['Accel ', trackAccelStr{i}])
end
xlabel ('Time (s)')
linkaxes(plotAxes, 'x')
set(gcf,'name','Simband Accel Signals','numbertitle','off')
