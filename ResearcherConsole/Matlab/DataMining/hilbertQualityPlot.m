function hilbertQualityPlot(data, label)

%
% create an interactive dashboard to display 4 channels of raw PPG,
%

if ~exist('label', 'var'),
    label = '';
end



%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};

colring = 'kbgrmggkgbgrmggkg';
%colring = 'kgcrbmg';

metricsPPG = {'signal', 'beats', 'CI_times', 'CI_raw', 'ibi', 'DB', ...
    'biosemTime', 'biosemStatMed', 'biosemQual' };
metricsPPGDB = {'BeatAmp', 'FootAmp', 'PrPkAmp'};
metricsECG = metricsPPG(1:3);
tracks = {'ecg', tracksPPG{:}};

% try
%     curTrack = 'ecg';
%     for j = 1:length(metricsECG),
%         metric = metricsECG{j};
%         evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%         eval(evalString);
%     end
%
%     for i = 1:length(tracksPPG),
%         curTrack = tracksPPG{i};
%         for j = 1:length(metricsPPG),
%             metric = metricsPPG{j};
%             evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%             eval(evalString);
%         end
%     end
%     assert(isfield(data.acc,'x'));
%     assert(isfield(data.acc,'y'));
%     assert(isfield(data.acc,'z'));
% catch
%     disp(['track: ',curTrack, '    metric:', metric]);
%     error('fourChanComboDisplay: inputs missing some tracks of data');
% end


%
% beat-based instantaneous heart rate
%
figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.1 0 0.33 1]);
ax = [];

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        if isfield(eval(['data.', trackName]), 'band_beats')
            ax = [ax, subplot(4,2,i)];
            eval(['ibiTime = data.',trackName,'.band_beats.timestamps(2:end);']);
            eval(['ibi = diff(data.',trackName,'.band_beats.timestamps);']);
            plot(ibiTime, 60 ./ ibi, [colring(trackNum+1),'x']);
            set(gca, 'YLim', [30 150]);
            ylabel(trackName);            
        end
        if isfield(data, 'HR_ref')
            hold on;
            plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k');
        end

        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Beats instantaneous HR');
set(t, 'Interpreter', 'none');


%
% Fusion-based instantaneous heart rate
%
figure;
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.15 0 0.33 1]);

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(eval(['data.', trackName]), 'ibi_hilbert'),
        ax = [ax, subplot(4,2,i)];
        
        eval(['hrTime = data.',trackName,'.hilbert_timestamps;']);
        eval(['ibi = squeeze(data.',trackName,'.ibi_hilbert);']);
        plot(hrTime, 60 ./ ibi, [colring(trackNum+1),'x']);
        if isfield(data, 'HR_ref')
            hold on;
            plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k');
        end
        set(gca, 'YLim', [30 150]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Hilbert based Instantaneous HR');
set(t, 'Interpreter', 'none');

%
%

%
% Biosemantic Otuput before filtering
%
figure;
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.15 0 0.33 1]);
axoffset = length(ax);


for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(eval(['data.', trackName]), 'biosemInterbeats'),
        ax = [ax, subplot(4,2,i)];
        eval(['hrTime = data.',trackName,'.biosemInterbeats(1,:);']);
        eval(['ibi = squeeze(data.',trackName,'.biosemInterbeats(2,:));']);
        plot(hrTime, 60 ./ ibi, [colring(trackNum+1),'x']);
        if isfield(data, 'HR_ref')
            hold on;
            plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k');
        end
        set(gca, 'YLim', [30 150]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Hilb InstHR Post BBQ');
set(t, 'Interpreter', 'none');




%
% Biosemantic heart rate
%
figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);
ciring = {[1,1,1]*0.8, ...
    [1,1,1]*0.6, ...
    [1,1,1]*0.4, ...
    [1,1,1]*0.2, ...
    [1,1,1]*0, ...
    };

for i = 1:8,
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        ax = [ax, subplot(4,2,i)];
        eval(['hrTime = data.',trackName,'.biosemTime;']);
        % for frequency domain, use the short b/c 20 averaging is too laggy
        %
        %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['hrBiosem = data.',trackName,'.biosemStatMed.muHR;']);
        eval(['biosemQual = data.',trackName,'.biosemQual;']);
        eval(['t_hrci = data.',trackName,'.mat_CIraw.timestamps;']);
        eval(['hrci = data.',trackName,'.mat_CIraw.signal;']);        

        if (length(t_hrci) ~= length(hrTime)),
            hrci = interp1(t_hrci, hrci, hrTime, 'nearest');
        end
        for k = 1:5,
            ind = find(hrci == k);
            if (length(ind)>0),
                plot(hrTime(ind), hrBiosem(ind), '.', 'MarkerSize', 14, ...
                    'Color', ciring{k});
                hold on;
            end
        end
        
        if isfield(data, 'HR_ref')
            hold on;
            plot(data.HR_ref.inferredTimestamps, data.HR_ref.signal,'k');
        end
      
        set(gca, 'YLim', [30 150]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
subplot(4,2,1);
t = title(label);
set(t, 'Interpreter', 'none');
subplot(4,2,2);
t = title('Biosemantic HR');
set(t, 'Interpreter', 'none');


set(ax(1), 'XLim', [data.HR_ref.inferredTimestamps(1), data.HR_ref.inferredTimestamps(end)]);
linkaxes(ax, 'xy');