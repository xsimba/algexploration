function plotPatBp = plotPatBp(data, patType, trackOffset)

if isfield(data,'timestamp')
    data.timestamps = data.timestamp;
end
if ~exist('trackOffset','var'),
    trackOffset = 0;
end

if ~exist('beatOffset', 'var'),
    beatOffset = data.timestamps(1);
end

if ~exist('patType', 'var'),
    patType = 'raw';
end

%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
colring = 'kbgrmggkgbgrmggkg';
%colring = 'kbgrmggrg';
metricsPPG = {'signal', 'beats', 'pat'};
metricsECG = metricsPPG(1:2);
tracks = {'ecg', tracksPPG{:}};

%
% 
%
try
    curTrack = 'ecg';
    for j = 1:length(metricsECG),
        metric = metricsECG{j};
        evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
        eval(evalString);
    end
    
    for i = 1:length(tracksPPG),
        curTrack = tracksPPG{i};
        for j = 1:length(metricsPPG),
            metric = metricsPPG{j};
            evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
            eval(evalString);
        end
    end
    assert(isfield(data.bp,'dbp1'));
    assert(isfield(data.bp,'sbp1'));
catch
    disp(['track: ',curTrack, '    metric:', metric]);
    error('plotPatBP: inputs missing some tracks of data');
end

%
%
%
clf
set(gcf, 'Units', 'Normalized');

%
% loop over channels (four at a time)
%
for i = 1:4,
    figure(i);
    clf
    set(gcf, 'Units', 'Normalized');
    set(gcf, 'Position', [0.25*(i-1) 0 0.25 1]);
    trackNum = i+trackOffset;
    trackName = tracksPPG{i+trackOffset};

    %
    % ecg plot
    %
    ax{1+(i-1)*4} = subplot(4,1,1);
    plot(data.timestamps, data.ecg.signal, 'k');
    hold on;
    eval(['ecgbeat = beatOffset+data.ecg.beats(1,:);']);
    beatAmp = interp1(data.timestamps,data.ecg.signal,ecgbeat);
    %band_beatAmp = interp1(data.timestamps, sig, band_beats.timestamps);
   % eval(['beatAmp = data.ecg.beatAmp;']);
    plot(ecgbeat, beatAmp,'kx', 'MarkerSize', 14);
    xlim([data.timestamps(1) data.timestamps(end)]);
    ylabel('ECG');
    
    %
    % ppg plot
    %
    ax{2+(i-1)*4} = subplot(4,1,2);
    eval(['ppgsig = data.',trackName,'.signal;']);
    plot(data.timestamps, ppgsig, colring(trackNum+1));
    hold on
    eval(['beat = beatOffset+data.',trackName,'.beats(1,:);']);
    eval(['beatAmp = data.',trackName,'.beatAmp;']);
    plot(beat, beatAmp, 'kx', 'MarkerSize', 14);
    xlim([data.timestamps(1) data.timestamps(end)]);
    ylabel('PPG');
    
    %
    % pat
    %
    ax{3+(i-1)*4} = subplot(4,1,3);
    if strcmpi(patType, 'raw'),
        eval(['ppgpat1 = data.',trackName,'.pat.pat1;']);
        plot(ecgbeat(2:end), ppgpat1(1,:), [colring(1+trackNum),'o']);
    else 
        eval(['ppgpat2 = data.',trackName,'.pat.pat2;']);
        plot(ecgbeat(1,2:end), ppgpat2(1,:), [colring(1+trackNum),'o']);
    end
    ylabel('PAT [s]');
    set(gca, 'YLim', [0.1 0.45]);
    
    %
    % blood pressure
    %
    ax{4+(i-1)*4} = subplot(4,1,4);
    plot(ecgbeat(2:end), data.bp.dbp1, [colring(1+trackNum), 'x']);
    hold on
    % piero: if reference is available plots a continuous line reference
    if isfield(data.bp,'dbpref')
        plot(data.timestamps, data.bp.dbpref,'Color',[0.4375    0.5000    0.6625],'LineWidth',3.8);
    end
    
    hold on
    plot(ecgbeat(2:end), data.bp.sbp1, [colring(1+trackNum), '+']);
    
    if isfield(data.bp,'sbpref')
        plot(data.timestamps, data.bp.sbpref,'Color',[0.4375    0.5000    0.6625],'LineWidth',3.8);
    end
    
    ylabel('BP [mmHg]');
    xlabel('time [s]');
    set(gca, 'YLim', [50 200]);
    

end

linkaxes([ax{:}], 'x');
%linkaxes([ax{[1 5 9 13]}], 'y');

    