%setRCpath
RC_setup

time_comp = 1.5;

%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.15;
bioLimits.rateLimitUp = 0.15;
bioLimits.windowSize = 10;
bioLimits.sigmaCutoff = 2.5;


c = loadSessionData('\\105.140.2.7\share\DataAnalysis\BioSemanticDevel-server2.xlsx');
%c = loadSessionData('.\BioSemanticDevel_Sessions.xlsx');

sessionList={};

%sessionList = {sessionList{:}, '01122015_cm_test1'};
sessionList = {sessionList{:}, '01182015_1012_yelei'};

InitializeTracksFromCSV(c, sessionList);
dataFilename = setV0MatSessionData(c, sessionList{1});
load(dataFilename);

%Clip data into sections
clipData = findSections(data);
tracks = {'ppg.e'};

for i = 1:length(clipData),
    
    %Running motion detection on accel
    options = simset('SrcWorkspace', 'current');
    sim('motionDetector_20141216', [clipData{i}.timestamps(1), clipData{i}.timestamps(end)], options);
    clipData{i}.motion_flag.timestamps = motionFlag.time;
    clipData{i}.motion_flag.signal = squeeze(motionFlag.signals.values);
    
    options = simset('SrcWorkspace', 'current');
    sim('motionCounter', [clipData{1,i}.timestamps(1), clipData{1,i}.timestamps(end)], options);
%     ratio_motion(i) = motion_cont.Data(end)/sample_cont.Data(end);
    clipData{i}.motionCounter_flag = motionCounter_flag.Data(end);

    
    j = 1;
    curTrack = tracks{j};
    channel = j;
    
    %Running hilbert transform
    options = simset('SrcWorkspace', 'current');
    sim('InstFreqHR_currentCcodeImpl_CM', [clipData{i}.timestamps(1), clipData{i}.timestamps(end)], options);
    
    ibi_hilbert = squeeze(60./HRout.signals.values);
    eval(['clipData{',int2str(i),'}.',curTrack,'.ibi_hilbert = ibi_hilbert;']);
    eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_timestamps = HRout.time;']);
    
    %Running tde model
    
    options = simset('SrcWorkspace','current');
    sim('tde_ibi_CM_debug', [clipData{i}.timestamps(1), clipData{i}.timestamps(end)], options);
    
    timestamps = simout.Time(find(simout.Data(:,2)==1));
    ibi_out    = double(simout.Data(find(simout.Data(:,2)==1),1))/128;
    eval(['clipData{1,',int2str(i),'}.',curTrack,'.tde_ibi.timestamps = timestamps;']);
    eval(['clipData{1,',int2str(i),'}.',curTrack,'.tde_ibi.ibi = ibi_out;']);
    
    %hilbert and tde fusion
    eval(['hilbert_timestamps = clipData{',int2str(i),'}.',curTrack,'.hilbert_timestamps;']);
    eval(['tde_ibi.timestamps = clipData{',int2str(i),'}.',curTrack,'.tde_ibi.timestamps;']);
    [fusion_timestamps, indx] =sort([hilbert_timestamps; time_comp+tde_ibi.timestamps]);   % '881 sample points' is manually tuned to align with ibi_hilbert!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    eval(['ibi_hilbert = clipData{',int2str(i),'}.',curTrack,'.ibi_hilbert;']);
    eval(['tde_ibi.ibi = clipData{',int2str(i),'}.',curTrack,'.tde_ibi.ibi;']);
    ibi_fusion = [ibi_hilbert; tde_ibi.ibi];
    
    ibi_fusion = ibi_fusion(indx);
    
    eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.timestamps = fusion_timestamps;']);
    eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.ibi = ibi_fusion;']);
    
    %Running BiosemHR on fusion
    
    clipData{i} = computeBiosemBeatFilter(clipData{i}, curTrack, bioLimits, 'hilbert_fusion');
    
    %
    % compute final statistics (output is in biosemStatMed)
    %
    clipData{i} = computeBiosemStaticStat(clipData{i}, curTrack, 'biosemInterbeats');
    clipData{i} = computeBiosemQuality(clipData{i}, curTrack);
    
    %Plotting
    %hilbertQualityPlot(clipData{i});
    %pause
    
end


