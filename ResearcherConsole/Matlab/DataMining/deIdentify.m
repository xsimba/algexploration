function dataout = deIdentify(data)


depth = -1;


%Return fieldnames of input structure, prefix these with "data.".
NAMES = cellfun(@(x) strcat('data.',x),fieldnames(data),'UniformOutput',false) ;

%Set state counters to initial values (k terminates recursive loop, g makes
%recursive loop behave in a different way.
k = 1 ; g = 0 ;

fndstruct = {} ;

%k is ~0 while all fields have not been searched to exhaustion or to
%specified depth.
while k ~= 0
    fndtemp = {} ;
    
    k = length(NAMES) ;
    
    %g set to 1 prevents fieldnames from being added to output NAMES.
    %Useful when determining whether fields at the lowest specified depth
    %are structures, without adding their child fieldnames (i.e. at
    %specified depth + 1) to NAMES output.
    if depth == 1
        g = 1 ;
    end
    
    for i = 1:length(NAMES)
        %If the current fieldname is a structure, find its child
        %fieldnames, add to NAMES if not at specified depth (g = 0). Add to
        %fndstruct (list of structures). 
        if isstruct(eval(NAMES{i})) == 1
            if g ~= 1
                fndtemp2 = fieldnames(eval(NAMES{i})) ;
                fndtemp2 = cellfun(@(x) strcat(sprintf('%s.'            ...
                    ,NAMES{i}),x),fndtemp2,'UniformOutput',false) ;
                fndtemp = cat(1,fndtemp,fndtemp2) ;
            elseif g == 1
                fndtemp = cat(1,fndtemp,NAMES{i}) ;
                k = k - 1 ;
            end
            fndstruct = cat(1,fndstruct,NAMES{i}) ;
        else
            fndtemp = cat(1,fndtemp,NAMES{i}) ;
            k = k - 1 ;
        end
    end
    
    NAMES = fndtemp ;
    
    %If we have reached depth, stop recording children fieldnames to NAMES
    %output, but determine whether fields at final depth are structures or
    %not (g). After this, terminate loop by setting k to 0.
    if depth ~= -1                                                      ...
            && any(cellfun(@(x) size(find(x == '.'),2),NAMES) == depth)
        g = 1 ;
    elseif depth ~= -1                                                  ...
            && any(cellfun(@(x) size(find(x == '.'),2),NAMES) > depth)
        k = 0 ;
    end
    
end


fields = NAMES;

removeList = {};
%fields = cellfun(@(x) x(6:end),NAMES,'UniformOutput',false);
%List of fields to be removed
for i = 1:numel(fields),
    if (length(fields{i})>13),
        if(strcmp(fields{i}(end-13:end), 'unixTimeStamps')),           
            removeList = {fields{i},removeList{:}};
        end   
    end
end

dataout = data;
if isfield(data,'unixTimeStamps'),
    dataout = rmfield(data,'unixTimeStamps');
end
for i = 1:numel(removeList),
    structname = (removeList{i}(1:(length(removeList{i}))-15));
    tempStruct = rmfield(eval(structname),'unixTimeStamps');
    fieldname = structname(6:end);
    if strcmp(structname,'data'),
        dataout = tempStruct;
    else
        eval(['dataout.',fieldname,'=tempStruct']);
    end
end
end
    