function biosemBandDebug2(data, label, firmwareversion)

accToG_Conv = 1/2^14;

%
% create an interactive dashboard to display 4 channels of raw PPG,
%
if isfield(data,'timestamp')
    data.timestamps = data.timestamp;
end

if ~exist('label', 'var'),
    label = '';
end
flip = 0;

if ~exist('firmwareversion', 'var'),
    firmwareversion = 'v0.23.0';
end

Ilnd = min(length(firmwareversion), 5);
ver = firmwareversion(1:Ilnd);
verNo = 0;
if Ilnd == 5,
    verNo = str2num(ver(4:5));
end
if strcmp(ver, 'v0.20') || strcmp(ver, 'v0.19'),
    flip = 1;
    accToG_Conv = accToG_Conv  * 4;
elseif (verNo > 20 && verNo < 23),
    flip = 1;
    accToG_Conv = accToG_Conv  * 4;
end

%
% verify that all necessary channels are present
%
colring = 'kbgrmggrgbgrmggrg';
%colring = 'kbgrmggrg';
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
tracks = {'ecg', tracksPPG{:}};



% try
%     curTrack = 'ecg';
%     for j = 1:length(metricsECG),
%         metric = metricsECG{j};
%         evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%         eval(evalString);
%     end
%
%     for i = 1:length(tracksPPG),
%         curTrack = tracksPPG{i};
%         for j = 1:length(metricsPPG),
%             metric = metricsPPG{j};
%             evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%             eval(evalString);
%         end
%     end
%     assert(isfield(data.acc,'x'));
%     assert(isfield(data.acc,'y'));
%     assert(isfield(data.acc,'z'));
% catch
%     disp(['track: ',curTrack, '    metric:', metric]);
%     error('fourChanComboDisplay: inputs missing some tracks of data');
% end
%

%
%
%

axnum = 0;
figure(1);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0.5 1 0.45]);
for i = 1:9,
    trackNum = i;
    trackName = tracks{i};
    if (i == 1 && isfield(data, trackName)) || (i>1 && isfield(data.ppg, trackName(end)) && isfield(data.ppg.(trackName(end)), 'band_beats')),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['beatTime = data.',trackName,'.band_beats.timestamps;']);
        ibi = diff(beatTime);
        ibiTime = beatTime(2:end);
        plot(ibiTime, 60./ibi, [colring(trackNum),'x']);
        set(gca, 'YLim', [0 240]);
        ylabel([trackName, 'inst HR']);
        if (i==1 || i==3);
            t = title(label);
            set (t, 'Interpreter', 'none');
        end
    end
end


figure(2);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0 1 0.45]);
for i = 1:9,
    trackNum = i;
    trackName = tracks{i};
    if (i == 1 && isfield(data, trackName)) || (i>1 && isfield(data.ppg, trackName(end)) && isfield(data.ppg.(trackName(end)), 'band_beats')),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['sig = data.',trackName,'.signal;']);
        eval(['beatTime = data.',trackName,'.band_beats.timestamps;']);
        beatAmp = interp1(data.timestamps, sig, beatTime);
        plot(data.timestamps, sig, colring(trackNum));
        hold on;
        plot(beatTime, beatAmp, 'kx');
        ylabel(trackName);
        if (i==1 || i==3);
            t = title(label);
            set (t, 'Interpreter', 'none');
        end
    end
end

figure(3);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0.5 1 0.25]);

axnum = 1;
for i = 2:9,
    trackNum = i;
    trackName = tracks{i};
    flag = isfield(data.ppg, trackName(end));
    if (flag),
        eval(['flag = flag && isfield(data.',trackName,', ''band_hr_Biosem'');']);
        eval(['flag = flag && isfield(data.',trackName,', ''band_hr_BiosemSigma'');']);
        eval(['flag = flag && isfield(data.ppg.(trackName(end)), ''band_beats'');']);        
    end
    if (flag == 1),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['hr = data.',trackName,'.band_hr_Biosem.signal;']);
        eval(['t_hr = data.',trackName,'.band_hr_Biosem.timestamps;']);
        eval(['sigma = data.',trackName,'.band_hr_BiosemSigma.signal;']);
        eval(['t_sigma = data.',trackName,'.band_hr_BiosemSigma.timestamps;']);
        plot(t_hr, hr, colring(trackNum));
        hold on;
        if (length(t_sigma) == length(t_hr)),
            plot(t_hr, hr+1.5*sigma, [colring(trackNum), '--']);
            plot(t_hr, hr-1.5*sigma, [colring(trackNum), '--']);
        else
            sigmaInterp = interp1(t_sigma, sigma, t_hr);
            plot(t_hr, hr+1.5*sigmaInterp, [colring(trackNum), '--']);
            plot(t_hr, hr-1.5*sigmaInterp, [colring(trackNum), '--']);
        end
        eval(['beatTime = data.',trackName,'.band_beats.timestamps;']);
        ibi = diff(beatTime);
        ibiTime = beatTime(2:end);
        plot(ibiTime, 60./ibi, [colring(trackNum),'x'], 'MarkerSize', 3);
        ylabel(trackName);
        if (i==1 || i==3);
            t = title([label, 'BiosemHR']);
            set (t, 'Interpreter', 'none');
        end
        set(gca, 'YLim', [50 150]);
    end
end


linkaxes([ax{:}], 'x');

