function naiveDataDisplay(data, label, firmwareversion)

% accToG_Conv = 1/2^14;
accToG_Conv = 1;

%
% create an interactive dashboard to display 4 channels of raw PPG,
%
if isfield(data,'timestamp')
    data.timestamps = data.timestamp;
end

if ~exist('label', 'var'),
    label = '';
end
flip = 0;

if ~exist('firmwareversion', 'var'),
    firmwareversion = 'v0.23.0';
end

Ilnd = min(length(firmwareversion), 5);
ver = firmwareversion(1:Ilnd);
verNo = 0;
if Ilnd == 5,
    verNo = str2num(ver(4:5));
end
if strcmp(ver, 'v0.20') || strcmp(ver, 'v0.19'),
    flip = 1;
    accToG_Conv = accToG_Conv  * 4;
elseif (verNo > 20 && verNo < 23),
    flip = 1;
    accToG_Conv = accToG_Conv  * 4;
end

%
% verify that all necessary channels are present
%
colring = 'kbgrmggrgbgrmggrg';
%colring = 'kbgrmggrg';
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
tracks = {'ecg', tracksPPG{:}};



% try
%     curTrack = 'ecg';
%     for j = 1:length(metricsECG),
%         metric = metricsECG{j};
%         evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%         eval(evalString);
%     end
%     
%     for i = 1:length(tracksPPG),
%         curTrack = tracksPPG{i};
%         for j = 1:length(metricsPPG),
%             metric = metricsPPG{j};
%             evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
%             eval(evalString);
%         end
%     end
%     assert(isfield(data.acc,'x'));
%     assert(isfield(data.acc,'y'));
%     assert(isfield(data.acc,'z'));
% catch
%     disp(['track: ',curTrack, '    metric:', metric]);
%     error('fourChanComboDisplay: inputs missing some tracks of data');
% end
% 

%
% 
%

figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0.5 1 0.4]);

axnum = 1;
for i = 1:9,
    trackNum = i;
    trackName = tracks{i};
    if (i == 1 && isfield(data, trackName)) || (i>1 && isfield(data.ppg, trackName(end))),
        axnum =axnum+1;
        ax{axnum} = subplot(3,3,i);
        eval(['sig = data.',trackName,'.signal;']);
        if (i > 1 && flip ==1),
            sig = sig * -1;
        end
        plot(data.timestamps, sig, colring(trackNum));
        ylabel(trackName);
        if (i==1 || i==3);
            t = title(label);
            set (t, 'Interpreter', 'none');
        end
    end
end


% figure;
% clf
% set(gcf, 'Units', 'Normalized');
% set(gcf, 'Position', [0 0 0.5 0.45]);
% if (isfield(data, 'bioz')),
%     if (isfield(data.bioz, 'i')),
%         axnum = axnum+1;
%         ax{axnum} = subplot(411);
%         sig = data.bioz.i.signal;
%         time = data.bioz.i.timestamps;
%         plot(time, sig, 'b.');
%         ylabel('bioz.i');
%     end
%     if (isfield(data.bioz, 'q')),
%         axnum = axnum+1;
%         ax{axnum} = subplot(412);
%         sig = data.bioz.q.signal;
%         time = data.bioz.q.timestamps;
%         plot(time, sig, 'r.');
%         ylabel('bioz.q');
%     end
% end
% if (isfield(data, 'gsr')),
%     if (isfield(data.gsr, 'phasic')),
%         axnum = axnum+1;
%         ax{axnum} = subplot(413);
%         sig = data.gsr.phasic.signal;
%         time = data.gsr.phasic.timestamps;
%         plot(time, sig, 'b.');
%         ylabel('GSR phasic');
%     end
%     if (isfield(data.gsr, 'tonic')),
%         axnum = axnum+1;
%         ax{axnum} = subplot(414);
%         sig = data.gsr.tonic.signal;
%         time = data.gsr.tonic.timestamps;
%         plot(time, sig, 'r.');
%         ylabel('GSR tonic');
%     end
% end    

figure;
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.5 0 0.5 0.4]);
if (isfield(data, 'acc')),
    time = data.timestamps;
    sigx = data.acc.x.signal * accToG_Conv;
    sigy = data.acc.y.signal * accToG_Conv;
    sigz = data.acc.z.signal * accToG_Conv;
    sig = sqrt(sigx.*sigx + sigy.*sigy + sigz.*sigz);
    axnum = axnum+1;
    ax{axnum} = subplot(4,1,1);
    plot(time, sigx, 'b');
    ylabel('x acc');
    axnum = axnum+1;
    ax{axnum} = subplot(4,1,2);
    plot(time, sigy, 'b');
    ylabel('y acc');
    axnum = axnum+1;
    ax{axnum} = subplot(4,1,3);
    plot(time, sigz, 'b');
    ylabel('z acc');
    axnum = axnum+1;
    ax{axnum} = subplot(4,1,4);
    plot(time, sig, 'b');
    ylabel('tot acc');
end
       
linkaxes([ax{:}], 'x');

