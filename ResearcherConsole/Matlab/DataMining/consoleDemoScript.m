%
% Research Console Test Script
%
% A. Khalak, 7/18/2014
 
%
% plug an auth token in here, can get this from the devices tab on 
% https://api.samihub.com/ 
%

%
% from Ram's band
%
deviceToken    = 'fbc35eac9f1d4850b03112801189b7dd';
deviceID = '771cf8655c3249ada22ea7c405e32eec';
userID   = '34823184b0fc4f4b8b584db655ca44a0';
startDate = '1358020800000'
endDate = '1406228400000'

% from Anabel's data collection

deviceToken = '1def59e56d1d4a06acf85e3cd926d2cb';
deviceID = 'c1717a85481b4f2aa0a1b231a190594';
userID = '712dc68dea5044d9b9461c16e369fa62';
startDate = '1406764800000'
endDate = '1406768400000'



% start date and end date (grab last 1 hr of data)
endDate = '1407441296000';
offset = 6*60; % 36 minutes of data
offset = 36*60; % 36 minutes of data
startDateNum = str2num(endDate) - offset*1000;
startDate = sprintf('%d',startDateNum);


%
% get the data -- this should be timed.
%tstart = tic;
% 
data = getSimbandData(userID, deviceID, deviceToken, startDate, endDate);

%tElapsed = toc(tstart);
%
% find sections using: findSections
%
ss_ecg = findSections(data.ECG(2,:),data.ECG(1,:));
ss_ppg_10 = findSections(data.PPG_10(2,:),data.PPG_10(1,:));
ss_ppg_11 = findSections(data.PPG_11(2,:),data.PPG_11(1,:));
ss_ppg_20 = findSections(data.PPG_20(2,:),data.PPG_20(1,:));
ss_ppg_21 = findSections(data.PPG_21(2,:),data.PPG_21(1,:));
 
% %
% % Prove that we can load a single section as well
% % 
% 
% %
% % Set a template from a particularly good set of data from a section
% % Find data that looks like this.
% %
% %   using setReferenceSignal
%data = load('MatlabTransfer-Ram-data.mat');
 reference_ecg_signal = ss_ecg(7).values(138950:139100);
 figure
 plot(ss_ecg(7).values(138950:139100));
 psdx_ref = setReferenceSignal(reference_ecg_signal);
% 
% reference_ppg_signal = ss_ppg1(2,1).section(13200:13335);
% psdx_ref = setReferenceSignal(reference_ppg_signal);
% %   using identifySignal
% 
% %%%%%%%%%%% Identify clean signal through FFT analysis %%%%%%%%%%%%%%%%%%%%
 index = 7;
 clean_signal = identifySignal(ss_ecg(index,1).values, psdx_ref);
%                                                   
% %%%%% Plot Sections and corresponding valid data points
 figure
 sect = ss_ecg(index,1);
% 
 plot(sect.time, sect.values);
 hold on
 plot(sect.time(1,clean_signal),sect.values(1,clean_signal),'ro');
 xlabel('Time in milli seconds since epoch');
 ylabel('ECG');
 legend('Section','Data')
 hold off
% 
% %%% Beat Detection (Integrating IMEC's algorithms
% section_number = 1;
% ecg = ss_ecg(section_number,1).section;
% ppg1_1 = ss_ppg1(section_number,1).section;
% BDunitTest
