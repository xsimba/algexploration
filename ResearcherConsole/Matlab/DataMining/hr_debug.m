hold on;
figure
para=2.5;
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d', 'ppg.e', 'ppg.f', 'ppg.g', 'ppg.h'};
ref_data=[];
for i = 5
    trackNum = i;
    trackName = tracksPPG{i};
    if isfield(data.ppg, trackName(end)),
        % for frequency domain, use the short b/c 20 averaging is too laggy
        %
        %        eval(['hrBiosem = data.',trackName,'.biosemStatShort.muHR;']);
        eval(['CI_freq = data.',trackName,'.freq_HR.CI_freq;']);
        eval(['ibi_freq = data.',trackName,'.freq_HR.ibi_freq;']);
        eval(['amp_freq = data.',trackName,'.freq_HR.amp_freq;']);
        eval(['time = data.',trackName,'.freq_HR.time;']);
        eval(['sigma = data.',trackName,'.biosemDebug.sigma;']);
        
        for k =1:size(CI_freq,2)
            %            [~,ind]=sort(CI_freq(:,k).*amp_freq(:,k),'descend');
            ind = find(CI_freq(:,k).*amp_freq(:,k)>0);
            for j = 1:7,
                if length(ind) >= j,
                    freq{j}(k) = ibi_freq(ind(j),k);
                else
                    freq{j}(k) = nan;
                end
            end
        end
        plot(time,freq{1}*60,'b.');
        hold on;
        plot(time,freq{2}*60,'r.');
        plot(time,freq{3}*60,'g.');
        plot(time,freq{4}*60,'k.');
        plot(time,freq{5}*60,'k.');
        plot(time,freq{6}*60,'k.');
        plot(time,freq{7}*60,'k.');
        
        if ~isempty(ref_data),
            hold on;
            plot((1:length(ref_data))+timeOffset, ref_data,'k');
        end
        set(gca, 'YLim', [30 240]);
        ylabel(trackName);
        %    set(gca, 'XLim', [0 15000]);
    end
end
plot(data.ppg.e.biosemInterbeats(1,:),60./data.ppg.e.biosemInterbeats(2,:),'g', 'linewidth',2.5)
plot(data.ppg.e.biosemInterbeats(1,:),60./data.ppg.e.biosemInterbeats(2,:) + 2.5*sigma, 'g--');
plot(data.ppg.e.biosemInterbeats(1,:),60./data.ppg.e.biosemInterbeats(2,:) - 2.5*sigma, 'g--');


plot(data.band_f_biosem_hr_prefilt.timestamps-3,data.band_f_biosem_hr_prefilt.signal,'b.')

sigmaRef=interp1(data.band_f_biosem_hr_sigma.timestamps, data.band_f_biosem_hr_sigma.signal, data.band_f_biosem_hr.timestamps);

plot(data.band_f_biosem_hr_prefilt.timestamps-3, data.band_f_biosem_hr_prefilt.signal + sigmaRef*2.5, 'm-');
plot(data.band_f_biosem_hr_prefilt.timestamps-3, data.band_f_biosem_hr_prefilt.signal - sigmaRef*2.5, 'm-');


