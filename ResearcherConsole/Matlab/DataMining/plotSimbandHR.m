function plotSimbandHR(data)

figure;

% Plot Operating Modes
plotAxes(1) = subplot(2, 1, 1);
plot(data.system.operationMode.timestamps, data.system.operationMode.signal, 'k*')
ylim([1 4])
set(gca,'YTick', [1:4], 'YTickLabel',{'To Collection', 'To Monitoring', 'To Low-power', 'To Fitness'})
ylabel('Mode of Operation Transition')

%  Plot Heart Rates
plotAxes(2) = subplot(2, 1, 2);
monHrIdx = find(data.heartRate.monitoring_hr.confidence.signal >= 3);
fitHrIdx = find(data.heartRate.lbhr.confidence.signal >= 3);
colHrIdx = find(data.heartRate.collection_hr.confidence.signal >= 3);
plot(data.heartRate.monitoring_hr.timestamps(monHrIdx), data.heartRate.monitoring_hr.signal(monHrIdx), 'b.', data.heartRate.lbhr.timestamps(fitHrIdx), data.heartRate.lbhr.signal(fitHrIdx), 'g.', data.heartRate.collection_hr.timestamps(colHrIdx), data.heartRate.collection_hr.signal(colHrIdx), 'm*')
ylabel('HR (BPM)'), xlabel('Time (s)')
legend('HR - Stationary Algorithm', 'HR - Fitness Mode Algorithm', 'HR - Collection Mode Algorithm')

linkaxes(plotAxes, 'x')
set(gcf,'name','Simband Heart Rate Estimators','numbertitle','off')

    