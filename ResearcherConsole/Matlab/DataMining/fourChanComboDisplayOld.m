function fourChanComboDisplayOld(data, label, beatMetric)
trackNum = 3;
accToG_Conv = 1/2^14;

%
% create an interactive dashboard to display 4 channels of raw PPG,
%

if ~exist('label', 'var'),
    label = '';
end

if ~exist('beatMetric', 'var'),
    beatMetric = 'interbeat';
end

%
% verify that all necessary channels are present
%
tracksPPG = {'ppg.a', 'ppg.b', 'ppg.c', 'ppg.d'};
colring = 'kgcrb';
metricsPPG = {'signal', 'beats', beatMetric, 'CI_times', 'CI'};
metricsECG = metricsPPG(1:3);
tracks = {'ecg', tracksPPG{:}};

try
    curTrack = 'ecg';
    for j = 1:length(metricsECG),
        metric = metricsECG{j};
        evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
        eval(evalString);
    end
    
    for i = 1:length(tracksPPG),
        curTrack = tracksPPG{i};
        for j = 1:length(metricsPPG),
            metric = metricsPPG{j};
            evalString=['assert(isfield(data.',curTrack,', ''', metric,'''));'];
            eval(evalString);
        end
    end
    assert(isfield(data.acc,'x'));
    assert(isfield(data.acc,'y'));
    assert(isfield(data.acc,'z'));
catch
    disp(['track: ',curTrack, '    metric:', metric]);
    error('fourChanComboDisplay: inputs missing some tracks of data');
end


%
% 
%

figure(1);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0 0 0.33 1]);
ax1 = subplot(411);
plot(data.timestamps, data.ppg.a.signal, colring(2));
hold on;
plot(data.ppg.a.beats(1,:), data.ppg.a.beats(2,:), 'kx', 'MarkerSize', 14);
ylabel('PPG 1');
title(label);

ax2 = subplot(412);
plot(data.timestamps, data.ppg.b.signal, colring(3));
hold on;
ylabel('PPG 2');
plot(data.ppg.b.beats(1,:), data.ppg.b.beats(2,:), 'kx', 'MarkerSize', 14);

ax3 = subplot(413);
plot(data.timestamps, data.ppg.c.signal, colring(4));
hold on;
plot(data.ppg.c.beats(1,:), data.ppg.c.beats(2,:), 'kx', 'MarkerSize', 14);
ylabel('PPG 3');

ax4 = subplot(414);
plot(data.timestamps, data.ppg.d.signal, colring(5));
hold on;
plot(data.ppg.d.beats(1,:), data.ppg.d.beats(2,:), 'kx', 'MarkerSize', 14);
xlabel('time [s]');
ylabel('PPG 4');

figure(2);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.33 0 0.33 1]);
ax5 = subplot(411);
plot(data.ppg.a.CI_times, data.ppg.a.CI, 'x', 'MarkerSize', 14)
set(gca, 'YLim', [0 4.5]);
ylabel('CI PPG 1');
title(label);
ax6 = subplot(412);
plot(data.ppg.b.CI_times, data.ppg.b.CI, 'x', 'MarkerSize', 14)
set(gca, 'YLim', [0 4.5]);
ylabel('CI PPG 2');
ax7 = subplot(413);
plot(data.ppg.c.CI_times, data.ppg.c.CI, 'x', 'MarkerSize', 14)
set(gca, 'YLim', [0 4.5]);
ylabel('CI PPG 3');
ax8 = subplot(414);
plot(data.ppg.d.CI_times, data.ppg.d.CI, 'x', 'MarkerSize', 14)
set(gca, 'YLim', [0 4.5]);
ylabel('CI PPG 4');

figure(3);
clf
set(gcf, 'Units', 'Normalized');
set(gcf, 'Position', [0.66 0 0.33 1]);
ax9 = subplot(311);

for j = 1:5,
    curTrack = tracks{j};
    eval(['thisIBtime = data.',curTrack,'.', beatMetric, ';']);
    if j == 1,
        sym = 'x';
    else
        sym = '.';
    end
    plot(thisIBtime(1,:), 60./thisIBtime(2,:), [colring(j),sym], 'MarkerSize', 14);
    hold on;
    xlabel('time');
    ylabel('instantaneous HR [bpm]');
end
set (gca, 'YLim', [0 200]);
title(label);

ax10 = subplot(312);
curTrack = tracks{trackNum};
eval(['thisIBtime = data.',curTrack,'.', beatMetric, ';']);
plot(thisIBtime(1,:), 60./thisIBtime(2,:), [colring(trackNum),'.'], 'MarkerSize', 14);
hold on;
xlabel('time');
ylabel('instantaneous HR [bpm]');
title('selected channel');
set (gca, 'YLim', [0 200]);

ax11 = subplot(313);
accelMag = [];
for i = 1:length(data.acc.x.signal),
    accelMag(i) = norm([data.acc.x.signal(i), ...
        data.acc.y.signal(i), ...
        data.acc.z.signal(i)], 2) * accToG_Conv;
end
plot(data.timestamps, accelMag, 'x', 'MarkerSize', 14);
xlabel('time');
ylabel('acceleration magnitude [g]');

linkaxes([ax1 ax2 ax3 ax4 ax5 ax6 ax7 ax8 ax9 ax10 ax11], 'x');

