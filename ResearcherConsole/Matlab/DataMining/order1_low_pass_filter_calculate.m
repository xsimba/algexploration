function y = order1_low_pass_filter_calculate(yn1, x, alpha)
% /*!
% @brief	 <order1_low_pass_filter_calculate>
% @details <Calculates a first order filter on incoming samples>
% 
% @param	 <float yn1>   <input, previous output>
% @param	 <float x>     <input, current input>
% @param	 <float alpha> <inupt, filter constant (alpha)>
% @return	 <float>       <filter output>
% */


	y = alpha * yn1 + (1 - alpha) * x;


