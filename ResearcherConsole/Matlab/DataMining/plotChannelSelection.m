function okCode = plotChannelSelection( data,ppg_selection )
    okCode = 0 ; 
    
    ppgChannels = 'abcdefgh';
    defaultChannel = 5;
    
    ppgSelTime = ppg_selection.timestamps;
    ppg_selection = ppg_selection.signal+1 ;

    selection = zeros(8,numel(data.timestamps));
        
    timeIndex = find(data.timestamps >= data.timestamps(1) & data.timestamps < ppgSelTime(1));
    selection(defaultChannel,timeIndex) = 1;
    
    for idx = 1:numel(ppgSelTime)-1
        timeIndex = find(data.timestamps >= ppgSelTime(idx) & data.timestamps < ppgSelTime(idx+1));
        selection(ppg_selection(idx),timeIndex) = 1;
    end
    
    timeIndex = find(data.timestamps >= ppgSelTime(end) & data.timestamps < data.timestamps(end));
    selection(ppg_selection(end),timeIndex) = 1;
    colSel = [0.4648    0.5312    0.5977; 1    0    0;];
    
    figure('units','normalized','outerposition',[0 0 1 1])
    
    for idx = 1:numel(ppgChannels)
       
        if isfield(data.ppg,ppgChannels(idx))
            ax{idx} = subplot(4,2,idx);hold on
            % this is slow
            % for jdx = 1:numel(data.timestamps)
            %     plot(data.timestamps(jdx),data.ppg.(ppgChannels(idx)).signal(jdx),'Color',colSel(selection(idx,jdx)+1,:),'LineWidth',2);
            % end            
            % instead of plotting all points let's plot only the red dots
            % on top of an all grey plot
            plot(data.timestamps,data.ppg.(ppgChannels(idx)).signal,'Color',colSel(1,:),'LineWidth',1.2);
            hold on
            for jdx = 1:numel(data.timestamps)
                if selection(idx,jdx)
                    plot(data.timestamps(jdx),data.ppg.(ppgChannels(idx)).signal(jdx),'Color',colSel(2,:),'LineWidth',1.3);
                end
            end
            ylabel(upper(['ppg.',ppgChannels(idx)]));
        end
    end
    
    linkaxes([ax{:}], 'x');
end
