
time_comp = 1.5;
% Set thresholds for publishing
thresholdA =.1;
thresholdB =.4;

collection_duration = 10;
%
% define biolimit parameters
%
bioLimits.minHR = 30;
bioLimits.maxHR = 260;
bioLimits.rateLimitDown = 0.15;
bioLimits.rateLimitUp = 0.15;
bioLimits.windowSize = 10;
bioLimits.sigmaCutoff = 2.5;

% c = loadSessionData('\\105.140.2.7\share\DataAnalysis\BioSemanticDevel-server2.xlsx');
c = loadSessionData('C:\work\git\DataAnalysis\BioSemanticDevel-server2.xlsx');

sessionList={};

% sessionList = {sessionList{:},'01202015_cm_test1','01202015_cm_test2','01202015_cm_test3','01202015_cm_test4','01202015_cm_test5','01182015_0345-0800_yelei','01192015_yelei', '01182015_1012_yelei'};
sessionList = {sessionList{:}, '03172015_test1'};
for k = 1:length(sessionList)
    clear clipData data
%     InitializeTracksFromCSV(c, sessionList);
    dataFilename = setV0MatSessionData(c, sessionList{k});
    load(dataFilename);
    
    %Clip data into sections
    clipData = findSections(data); 
    
    tracks = {'ppg.e'};
    for i = 1:length(clipData),
        
        %Running motion detection on accel
        options = simset('SrcWorkspace', 'current');
        sim('motionDetector_20141216', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(1)+collection_duration], options);
        %                 sim('motionDetector_20141216', [clipData{i}.timestamps(1), ...
        %             clipData{i}.timestamps(1)+25], options);
        clipData{i}.motion_flag.timestamps = motionFlag.time;
        clipData{i}.motion_flag.signal = squeeze(motionFlag.signals.values);
%         clipData{i}.motion_flag.signal = zeros(size(squeeze(motionFlag.signals.values)));
        
        % uncommment for case of bad accelero.
        %             clipData{i}.motion_flag.timestamps = motionFlag.time;
        %         clipData{i}.motion_flag.signal = zeros(length(squeeze(motionFlag.signals.values)),1);
        
        options = simset('SrcWorkspace', 'current');
        sim('motionCounter', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(1)+collection_duration], options);
        %         sim('motionCounter', [clipData{i}.timestamps(1), ...
        %            clipData{i}.timestamps(1)+25], options);
        clipData{i}.motionCounter_flag = motionCounter_flag.Data(end);
        
        
        j = 1;
        curTrack = tracks{j};
        channel = 5;
        timestamps=[];ibi_out=[];
        
        %Running hilbert transform
        options = simset('SrcWorkspace', 'current');
 
        sim('tde_ibi_CM_10_6comb', [clipData{i}.timestamps(1), ...
            clipData{i}.timestamps(1)+collection_duration], options);
%         timestamps = [simout.Time(find(simout.Data(:,2)==1)); simout.Time(find(simout.Data(:,4)==1)); simout.Time(find(simout.Data(:,6)==1)); simout.Time(find(simout.Data(:,8)==1)); simout.Time(find(simout.Data(:,10)==1)); simout.Time(find(simout.Data(:,12)==1))];
%         ibi_out    = [ double(simout.Data(find(simout.Data(:,2)==1),1))/128; double(simout.Data(find(simout.Data(:,4)==1),3))/128 ; double(simout.Data(find(simout.Data(:,6)==1),5))/128; double(simout.Data(find(simout.Data(:,8)==1),7))/128; double(simout.Data(find(simout.Data(:,10)==1),9))/128 ; double(simout.Data(find(simout.Data(:,12)==1),11))/128];   
%         
        timestamps = [ibi_flag_out.Time(find(ibi_flag_out.Data(:,1)==1)); ibi_flag_out.Time(find(ibi_flag_out.Data(:,2)==1)); ibi_flag_out.Time(find(ibi_flag_out.Data(:,3)==1)); ibi_flag_out.Time(find(ibi_flag_out.Data(:,4)==1)); ibi_flag_out.Time(find(ibi_flag_out.Data(:,5)==1)); ibi_flag_out.Time(find(ibi_flag_out.Data(:,6)==1))];
        ibi_out    = [ double(ibi_TDE_out.Data(find(ibi_flag_out.Data(:,1)==1),1))/128; double(ibi_TDE_out.Data(find(ibi_flag_out.Data(:,2)==1),2))/128 ; double(ibi_TDE_out.Data(find(ibi_flag_out.Data(:,3)==1),3))/128; double(ibi_TDE_out.Data(find(ibi_flag_out.Data(:,4)==1),4))/128; double(ibi_TDE_out.Data(find(ibi_flag_out.Data(:,5)==1),5))/128 ; double(ibi_TDE_out.Data(find(ibi_flag_out.Data(:,6)==1),6))/128]; 
 
        [fusion_timestamps, indx] = sort(timestamps);
 
        ibi_fusion = ibi_out;    
        ibi_fusion = ibi_fusion(indx);
        
        eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.timestamps = fusion_timestamps;']);
        eval(['clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.ibi = ibi_fusion;']);
        
        %
        % Running BiosemHR on fusion
        %
        clipData{i} = computeBiosemBeatFilter(clipData{i}, curTrack, ...
            bioLimits, 'hilbert_fusion');
        
        %
        % Compiling the publication stats and publish CM result
        %
        eval (['rawBeats = clipData{',int2str(i),'}.',curTrack,'.hilbert_fusion.ibi;']);
        eval (['bbqBeats = clipData{',int2str(i),'}.',curTrack,'.biosemInterbeats(2,:);']);
        plottableRawBeats = rawBeats(rawBeats> 60/240 & rawBeats<60/30);
%         plottableRawBeats = rawBeats(rawBeats>0 & rawBeats<100);
        
        lenRat(i) = length(bbqBeats)/ length(rawBeats);
        lenRat2(i) = length(bbqBeats)/ length(plottableRawBeats);
        
        if (lenRat(i) > thresholdA && lenRat2(i) > thresholdB && ...
                clipData{i}.motionCounter_flag ==0),
            CM.HR = median (60 ./ bbqBeats);
            CM.rawHR = median (60 ./ rawBeats(60./rawBeats>30 & 60./rawBeats<240));  
            CM.HRact = CM.HR;
            CM.HRsigma = std(60 ./ bbqBeats);
            CM.HRCI = 5;  % make this a function of the std(biosemInterbeats)
            eval(['clipData{',int2str(i),'}.',curTrack,'.CM = CM;']);
        else
            CM.HR = NaN;
            CM.rawHR = NaN;
            CM.HRact = median (60 ./ bbqBeats);
            CM.HRsigma = std(60 ./ bbqBeats);
            CM.HRCI = 0;
            eval(['clipData{',int2str(i),'}.',curTrack,'.CM = CM;']);
        end
        
    end
    metricsFilename = setMetricsSessionData(c, sessionList{k});
    save (metricsFilename, 'clipData');
end
plotCMdistros;


