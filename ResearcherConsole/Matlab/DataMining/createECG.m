function syn_ecg2 = createECG(ecg_template, hr)
r = ecg_template;
num = length(hr);
ibi  = round(60*128./hr);
ecg_candidates_ind =  floor(rand(num,1)*20)+1;
r = ecg_template;
norm_r = r-repmat(mean(r')',1,length(r(1,:))); %creating an average template

[maxv, r_location] = max(norm_r'); %r-location has the location of r-peaks

syn_ecg1 = [r(ecg_candidates_ind(1),:)];
ecg_location = [r_location(ecg_candidates_ind(1))];

for i = 2 :num
    syn_ecg1 = [syn_ecg1  r(ecg_candidates_ind(i),:)-r(ecg_candidates_ind(i),1)+syn_ecg1(end)];
    ecg_location(i) = length(r(1,:))*(i-1) +r_location(ecg_candidates_ind(i)) ;    
end

syn_ecg2 = [syn_ecg1(1:ecg_location(1))];
for i = 2:num
ecg_seg = syn_ecg1(ecg_location(i-1):ecg_location(i));
resampled_ecg = ecg_resample(ecg_seg,ibi(i-1),ecg_location(i)-ecg_location(i-1));    
resampled_ecg = resampled_ecg(1:ibi(i-1));
syn_ecg2 = [syn_ecg2 resampled_ecg];
end

end
