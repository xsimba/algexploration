function resampled_ecg = ecg_resample(ecg,sf_new,sf_old)
n_margin = floor (sf_old/2);
n_cut    = floor (sf_new/2);

temp_ecg = [flipud(ecg(1:n_margin)) ecg flipud(ecg(end-n_margin:end))];
temp_secg     = resample(temp_ecg,sf_new,sf_old);
resampled_ecg = temp_secg (n_cut+1:end-n_cut); 

end