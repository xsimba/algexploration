# -*- coding: utf-8 -*-
"""
Spider a directory tree of session files and infer an index.
Simple filter based rules to identify sessions.

"""

import sys, getopt

def isaMainName(name):
    if name[0] != '.' and name.partition('.')[2] == 'mat':
        return (True)
    return (False)

def filterOnMainName(name):
    if isaMainName(name):
        return(name.partition('.')[0])

def isaReferenceData(name):
    if name.partition('.')[2] == 'hdf5':
        return(True)
    return(False)

def filterOnReference(name):
    if isaReferenceData(name):
        return(name.partition('.')[0])

def getMainNameFromRef(name):
    return(name[0:11])

def main(argv):
    sessionFile = ''
    
    try:
        opts,spiderDir = getopt.getopt(argv, "h",["sessionFile="])
    except getopt.GetoptError:
        print 'spiderGenSessions.py --sessionFile <sessionFile> <parentDir>'
        sys.exit(2)
    if spiderDir == '':
        print 'spiderGenSessions.py --sessionFile <sessionFile> <parentDir>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'spiderGenSessions.py --sessionFile <sessionFile> <parentDir>'
            sys.exit(2)
        elif opt == 'sessionFile'
            sessionFile = arg

    print 'Spidering Directory: ' + spiderDir

    return spiderDir, sessionFile

#
# parse arguments and unpack
#    
if __name__ == "__main__":
    spiderDir, sessionFile = main(sys.argv[1:]) 

    sessionPath = dict()
    sessionRef = dict()    

    x = os.walk (spiderDir)
    for root, dirs, files in x:
        for z in files:
            if isaMainName(z):
                sessionName = filterOnMainName(z)
                sessionPath[sessionName] = root + '\\' + z + '\n'
                if (not sessionRef.has_key(sessionName)):
                    sessionRef(sessionName) = ''
            elif isaReferenceData(z):
                sessionRef(getMainNameFromRef(z)) = root + '\\' + z + '\n'

    fid = open(sessionFile, 'w')
    fid.write("SessionId, DeviceNickname, StartTime, EndTime, " +
        "WebGui-CSV, Imec-.mat_file, MetricsFile, ReferenceData")

        for key in sessionPath.keys():
            string = key +', UnknownDevice, 0, 0, ,' +
                     sessionPath[key] + ',' + sessionPath[key] + ',' + 
                     sessionRef[key]
            fid.write(string)
    fid.close()


