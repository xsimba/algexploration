# -*- coding: utf-8 -*-
"""
Interface to SAMI 'historicl' API for Simband data

"""
import requests
import json
from datetime import datetime
from time import mktime
import sensorSchema as ss
import sys,os
from time import sleep
import tarfile
import pdb


def blockTimeMessageSum(blockNum, tstart, tend, tLoadTotal, nextMsg=True):
    tLoad = (tend - tstart) / 1000.
    tLoadTotal = tLoadTotal + tLoad
    if (nextMsg):
        print 'block', blockNum, 'read complete in ', tLoad, 's.  Reading next block'
    else:
        print 'block', blockNum, 'read complete in ', tLoad, 's. All done.'
        print 'Total Data Access Time: ', tLoadTotal
    sys.stdout.flush()
    return tLoadTotal
    
def timerSample():
    thisTime = datetime.now()
    sec_since_epoch = mktime(thisTime.timetuple()) + thisTime.microsecond/1000000.0
    return sec_since_epoch * 1000


class samiHistoricalHandle():
    userID = ''
    deviceID = ''
    tokenID = ''
    count = 0
    
    def __init__(self,deviceID,tokenID, startDate, endDate):
       # self.userID = userID
        self.deviceID = deviceID
        self.tokenID = tokenID
        self.startDate = startDate
        self.endDate = endDate
        self.nextParameter = ''
    
    
    #
    # getGenericData()
    #
    def getGenericData(self, extractor):
        getHeaders = {'Authorization':'bearer '+self.tokenID}
        allData=[]
        count = 0
        getURL='https://api.samsungsami.io/v1.1/historical/normalized/messages?sdid='\
        + self.deviceID+ '&startDate=' + self.startDate + '&endDate=' + self.endDate

        print 'GET: ', getURL        
        
        tLoadTotal = 0         
        tstart = timerSample()
        response = requests.request('GET', getURL, headers = getHeaders, verify = False)
        print response
        tend = timerSample()
        dictResult = json.loads(response.content)
        hasData = dictResult.get('data')
        if hasData == None:
            print 'There is no data to extract with the specified parameters'
            sys.exit()

        if dictResult.has_key('next'):
            nextParameter = dictResult['next']
            data = dictResult['data']
            processData = map(extractor, data)
            allData.extend(processData)
            tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=True)
        else:
            data = dictResult['data']
            processData = map(extractor, data)
            allData.extend(processData)
            tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=False)
            return allData
            
        while True:
            try:
                getURL='https://api.samsungsami.io/v1.1/historical/normalized/messages?sdid='\
                + self.deviceID+'&startDate=' + self.startDate + '&endDate=' + self.endDate + '&offset=' + nextParameter
                #print getURL
                count = count + 1
                tstart = timerSample()
                response = requests.request('GET', getURL, headers = getHeaders, verify = False)
                print response
                #print response.content
                tend = timerSample()
                #pdb.set_trace()
                
                # print 'time to retrieve block in milliseconds', (tend-tstart)
                dictResult = json.loads(response.content)
                #print dictResult
                #pdb.set_trace()
                if dictResult.has_key('next'):
                    nextParameter = dictResult['next']
                    # print 'nextparameter',nextParameter
                    data = dictResult['data']
                    processData = map(extractor, data)
                    allData.extend(processData)
                    tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=True)
                else:
                    data = dictResult['data']
                    processData = map(extractor, data)
                    allData.extend(processData)
                    tLoadTotal = blockTimeMessageSum(count, tstart, tend, tLoadTotal, nextMsg=False)
                    return allData

            except ValueError, e:
                print e
                return allData

        return allData

    #
    # getScalarData is actually just getGenericData, but from an 
    # API level it makes more sense to call it this 
    #
    getScalarData = getGenericData

    #
    # getSimbaData  
    #            
    def getSimbaData(self):
        allData = self.getGenericData (ss.simbaExtractSingleMsg)
        
        return allData
    
    def exportSimbandMessages(self):

        getHeaders = {'Authorization':'bearer '+self.tokenID}
        getExportIdURL='https://api.samsungsami.io/v1.1/messages/export?sdid='\
            + self.deviceID + '&startDate=' + self.startDate + '&endDate=' + self.endDate
        exportId = requests.request('GET', getExportIdURL, headers = getHeaders, verify = False)
        dictResult = json.loads(exportId.content)
        print dictResult
        exportStatusURL='https://api.samsungsami.io/v1.1/messages/export/'+dictResult['exportId']+'/status'
        sleep(0.5)
        exportStatus = requests.request('GET',exportStatusURL, headers = getHeaders, verify=False)
        dictExportStatus = json.loads(exportStatus.content)
        print dictExportStatus
        while dictExportStatus['status'] != 'Success':
            exportStatus = requests.request('GET',exportStatusURL, headers = getHeaders, verify=False)
            sleep(5)
            dictExportStatus = json.loads(exportStatus.content)
            print dictExportStatus['status']
            if dictExportStatus['status'] == 'Success':
                exportDataURL='https://api.samsungsami.io/v1.1/messages/export/'+dictResult['exportId']+'/result'
                exportResult = requests.request('GET',exportDataURL, headers = getHeaders, verify=False, stream=True)
                print type((exportResult))
            #print exportResult.content
            
                file_name = "data.tgz"
                f = open(file_name,'wb')
                f.write(exportResult.content)
                f.close()
                Result = tarfile.open(file_name, mode='r')
                Result.extractall()
                Result.close()
        filepath = './' + str(dictResult['exportId']) + './' + str(self.startDate) + '.json'
        outputDict = json.loads(open(filepath,'r').read())
        os.remove(filepath)
        os.removedirs('./'+str(dictResult['exportId']))
        os.remove(file_name)    
        return outputDict

