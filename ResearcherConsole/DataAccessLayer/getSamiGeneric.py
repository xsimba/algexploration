# -*- coding: utf-8 -*-
"""
Retrieves all the messages from a specified device and parses all the data

Prints the time to taken retrieve each set of 1000 messages and also the
total length of data in seconds.

"""

import sys, getopt
import samiAccessHistorical as samiXH
import sensorSchema as ss 
import schemaDefinitions as sdef
from scipy.io import savemat
import numpy as np

def main(argv):
    #userID = ''
    deviceID = ''
    deviceToken = ''
    deviceType = ''
    startDate = ''
    endDate = ''
    
    try:
        opts,args = getopt.getopt(argv, "h",["did=","dtoken=","sdate=","edate=","device="])
    except getopt.GetoptError:
        print 'getSamiGeneric.py --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate> --device <deviceType>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'getSamiGeneric.py --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>  --device <deviceType>'
            sys.exit()
     #   elif opt == "--uid":
     #       userID = arg
        elif opt == "--did":
            deviceID = arg
        elif opt == "--device":
            deviceType = arg
        elif opt == "--dtoken":
            deviceToken = arg
        elif opt == "--sdate":
            startDate = arg
        elif opt == "--edate":
            endDate = arg
    credentials = {}
    pointer = {}
#    credentials['userID'] = userID
    credentials['deviceID'] = deviceID
    credentials['deviceToken'] = deviceToken
    credentials['deviceType'] = deviceType
    pointer['startDate'] = startDate
    pointer['endDate'] = endDate
    return credentials, pointer

#
# parse arguments and unpack
#    
if __name__ == "__main__":
    credentials, pointer = main(sys.argv[1:]) 

#userID = credentials['userID']
deviceID = credentials['deviceID']
deviceToken = credentials['deviceToken']
deviceType = credentials['deviceType']
startDate = pointer['startDate']
endDate = pointer['endDate']

#
# switch on device type
#
if   (deviceType == 'vitalConnect'):
    schema    = sdef.vitalConnectSchema()
elif (deviceType == 'fitbitFlex'):
    schema    = sdef.fitbitFlexSchema()
elif (deviceType == 'gearFit'):
    schema    = sdef.gearFitSchema()
elif (deviceType == 'withingsDevice'):
    schema    = sdef.withingsSchema()


#
# Create a connector to the device
#
f = samiXH.samiHistoricalHandle(deviceID,deviceToken,startDate,endDate)


#
# extract data
#
extractor = ss.genScalarExtractor(schema)
allData = f.getScalarData(extractor)
data = ss.parseGeneric(allData)

#
# convert floating point data
#
keyType = ss.getKeyTypes(schema)
for key in data.keys():
    if key not in keyType or keyType[key] != "Discrete":
        data[key] = map(np.float, data[key])

savemat ('MatlabTransfer.mat', data)


