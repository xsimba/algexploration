# -*- coding: utf-8 -*-
"""

Parsing library to transform SAMI messages to data objects in python

"""

import schemaDefinitions as sdef
import numpy as np
#import matplotlib.pyplot as plt
import base64
import struct
import json
import pdb
############################################################
#
# Simband style extraction.  This has a single fieldtype 
# for each JSON message, with some structured parsing.
#
# Currently, (16 Aug), the fields in data are 
#    metadata 
#    content
#
# With the metadata consisting of 
# channels, contentType, origin, sensorType, and sensorAnnotation
#
#

#
# extract data from JSON packet and interpolate timestamp based on
# sample rate if it is a vector.  This presumes that each JSON
# has a unique (ts, channel).
#
def simbaExtractSingleMsg(samiJson):
    #print samiJson
    samiTimeStamp = samiJson['cts']
    timeStamp = samiJson['ts'] 
    field = samiJson['data']['payload']['field']
    if samiJson['data']['payload'].has_key('field'):
        field = samiJson['data']['payload']['field']
    if samiJson['data']['payload'].has_key('type'):
        version = 'v3'
        streamType = samiJson['data']['payload']['type']
        if samiJson['data']['payload'].has_key('index'):
            streamIndex = samiJson['data']['payload']['index']
        else:
            streamIndex = 0
        keyString = streamType + "/" + str(streamIndex)
    else:
        keyString = field
        version = 'v4'
    key = sdef.simbaKeyWords(keyString, version)
    sigType = sdef.simbaDataTypes(keyString, version)
    schemaIncomplete = []
    if key == 'none' or sigType == 'none':
        #print 'Data not downloaded from stream', keyString
        # TODO: print some useful warning message based on *unique* 
        schemaIncomplete.extend(keyString)
    else:
        encodedValues = samiJson['data']['payload']['samples']
        if not (field):
            field = 'empty'
        if not (encodedValues):
            return []
        else:     
            decodedValues = base64.standard_b64decode(encodedValues)
            count = len(decodedValues) //4
            realValues = []
            for k in range(0, count):
                firstBytes = decodedValues[4*k:4*k+4]
                realValues.append(struct.unpack('f', firstBytes)[0])
        
        if samiJson['data'].has_key('sampleRate'):
            sampleRate = samiJson['data']['sampleRate']
        else:
            sampleRate = 128;
        timeStamps = []
        samiTimeStamps = []
    
        if len(realValues) > 1:
            numberValues = len(realValues)
            for k in range(0, numberValues):
                timeStamps.append(timeStamp + (k)*1000.0/sampleRate)
                samiTimeStamps.append(samiTimeStamp)
        elif len(realValues) ==1:
            timeStamps = [timeStamp]
            samiTimeStamps = [samiTimeStamp]
    
        rcDatum = {}    
        rcDatum['timeStamps']  = timeStamps
        rcDatum['values']     = realValues
        if keyString == 'heartBeat':
             rcDatum['values'] = timeStamps    
        rcDatum['sampleRate'] = sampleRate
        rcDatum['key']        = key
        rcDatum['samiTimeStamps'] = samiTimeStamps
        rcDatum['type']       = sigType
        
        return rcDatum

#
# normalize times places all of the signals on a single
# time grid and converts to sec.
#
def timeAlignParse(rawData, keyTypes):
    newDict = {}
    #
    # hard-coded sample rate for 128Hz (TO DO: fix this)
    # 
    sample_rate = 7.8125   
    
    #
    # time align by finding the minimum time and the 
    # maximum time and selecting a single grid to cover all
    # the points for SRCSignals.  The current implementation 
    # uses a window that includes all of the data.
    #
    # Types other than SRCSignal, each have their own timestamp
    # and value.
    #
    minTime = -1
    maxTime = -1
    for key in rawData:
        timeStamps = rawData[key][0]
        sigType = keyTypes[key]
        if (sigType != 'SrcSignal'):
            continue
        if (minTime < timeStamps[0]):
            minTime = timeStamps[0]
        if (maxTime > timeStamps[-1] or maxTime < 0):
            maxTime = timeStamps[-1]
    timeGrid = np.arange(minTime, maxTime, sample_rate)
    timeGrid = np.round(timeGrid)
    #timeGrid = timeGrid / 1000.0     # convert from ms to s

    newDict['timestamps'] = timeGrid    
    
    #
    # interpolate to the new time grid    
    #
    for key in rawData:
        sigType = keyTypes[key]
        if sigType != 'SrcSignal':
            newDict[key] = rawData[key][0:2]
            
        else:
            values = np.interp(timeGrid, rawData[key][0], rawData[key][1])
            newDict[key] = values
    
    return newDict


#
# parseSimba demuxes the cases of multiple channels into 
# arrays.
#        
def parseSimba(allData):      
    new_dict = {}
    keyTypes = {}
    for datum in allData:
        
        if isinstance(datum, dict) and datum.has_key('timeStamps'):
            timeStamps = datum['timeStamps']
            values = datum['values']
            key = datum['key']
            samiTimeStamps = datum['samiTimeStamps']
            sigType = datum['type']
                
            if new_dict.has_key(key):                        
                new_dict[key][0].extend(timeStamps)
                new_dict[key][1].extend(values)
                new_dict[key][2].extend(samiTimeStamps)
            else:
                new_dict[key] = (timeStamps, values, samiTimeStamps)
                keyTypes[key] = sigType
                            
    return new_dict, keyTypes

#################################################################
#
# Generic style extraction.  This has a multiple fieldtypes 
# for each JSON message, with the fields in data corresponding to 
# the stream names.
#

#
# extract data from JSON packet and translate into a dict.  
# This presumes that each JSON has a unique (ts), and all relevant
# channels for a given ts are in this JSON.  
#
def scalarExtractMsg(datum, schema):
    timeStamp     = datum['ts']
    samiTimestamp = datum['cts']
    parsedictData = datum['data']           

    rcDatum = {}    
    rcDatum['timeStamps']     = timeStamp
    rcDatum['samiTimeStamps'] = samiTimestamp

    #
    # Loop over all members of schema to avoid 
    # time alignment issues for missing fields
    #
    
    for keySami, keyVariable in schema.iteritems():
        
        if parsedictData.has_key(keySami):
            rcDatum[keyVariable[0]] = parsedictData[keySami]
        else:
            rcDatum[keyVariable[0]]  = np.NaN

    return rcDatum

#
# this is a generator function for scalar extract Msg
#
def genScalarExtractor(schema):
    extractor = lambda d : scalarExtractMsg(d, schema) 

    return extractor


#
# parseGeneric processes the case of multiple streams for a single
# unique timestamp.
#        
def parseGeneric(allData):      
    keyList = allData[0].keys()
    new_dict = {k : [] for k in keyList}
    
    for datum in allData:
        for key, value in datum.iteritems():
            new_dict[key].append(value)
                
    return new_dict

#
# getKeyTypes creates a new dict from a schema mapping the 
# output key to the type.
#
def getKeyTypes(schema):
    keyTypeDict = {}
    for keySami, keyVariable in schema.iteritems():
        keyTypeDict[keyVariable[0]] = keyVariable[1]
    return keyTypeDict
