# -*- coding: utf-8 -*-
"""
Retrieves all the messages from a Simband device and parse the data

Prints the time to taken retrieve each set of 1000 messages and also the
total length of data in seconds.

"""

import sys, getopt
import samiAccessHistorical as samiXH
import sensorSchema as ss 
from scipy.io import savemat

def main(argv):
    #userID = ''
    deviceID = ''
    deviceToken = ''
    startDate = ''
    endDate = ''
    
    try:
        opts,args = getopt.getopt(argv, "h",["did=","dtoken=","sdate=","edate="])
    except getopt.GetoptError:
        print 'getSamiSimband.py --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'getSamiSimband.py --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
            sys.exit()
        #elif opt == "--uid":
        #    userID = arg
        elif opt == "--did":
            deviceID = arg
        elif opt == "--dtoken":
            deviceToken = arg
        elif opt == "--sdate":
            startDate = arg
        elif opt == "--edate":
            endDate = arg
    credentials = {}
    pointer = {}
#    credentials['userID'] = userID
    credentials['deviceID'] = deviceID
    credentials['deviceToken'] = deviceToken
    pointer['startDate'] = startDate
    pointer['endDate'] = endDate
    return credentials, pointer

#
# parse arguments and unpack
#    
if __name__ == "__main__":
    credentials, pointer = main(sys.argv[1:]) 

#userID = credentials['userID']
deviceID = credentials['deviceID']
deviceToken = credentials['deviceToken']
startDate = pointer['startDate']
endDate = pointer['endDate']

#
# Create a connector to the device
#
f = samiXH.samiHistoricalHandle(deviceID,deviceToken,startDate,endDate)

#
# to do: use the information from SAMI to determine the
# correct parsing and send into an if-else block
#
allData = f.getSimbaData()

(rawData, sigTypes) = ss.parseSimba(allData)
savemat ('MatlabTransfer.mat', rawData)

data = ss.timeAlignParse(rawData, sigTypes)
savemat ('MatlabTransferSrc.mat', data)

