# -*- coding: utf-8 -*-
"""
Authenticate to SAMI and read data using export API based on trialId

"""

import sys, getopt
import samiAccessExport as samiXE
import sensorSchema as ss 
from scipy.io import savemat
#import pdb
from time import sleep

import BaseHTTPServer
import webbrowser
import SimpleHTTPServer

ShouldContinue = True

def main(argv):
    access_token = ''
    deviceId = ''
    trialId = ''
    startDate = ''
    endDate = ''
    
    try:
        opts,arguments = getopt.getopt(argv, "h",["startDate=","endDate=","sdid=","trialId="])
    except getopt.GetoptError:
        print 'getSamiSimbandExpTrialsAuthenticate.py --startDate <val> --endDate <val> --sdid <val> --trialId <val>'
        sys.exit(2)


    # process options
    for opt, val in opts:
        if opt == '-h':
            print 'getSamiSimbandExpTrialsAuthenticate.py --startDate <val> --endDate <val> --sdid <val> --trialId <val>'
            sys.exit()
        #elif opt == "--uid":
        #    userID = arg
        elif opt == "--startDate":
            startDate = val
        elif opt == "--endDate":
            endDate = val
        elif opt == "--sdid":
            deviceId = val
        elif opt == "--trialId":
            trialId = val

    # process JSON argument and validate
    param = dict()
    param['startDate'] = startDate
    param['endDate'] = endDate
    param['sdid'] = deviceId
#    if (not param.has_key(''))
# should do some validation in here as well...
        
    return access_token, param

#
# parse arguments and unpack
#    
if __name__ == "__main__":
    access_token, parameters = main(sys.argv[1:]) 


# get input parameters
startDate = parameters['startDate']
endDate = parameters['endDate']
sdid = parameters['sdid']

#
# define a keep alive function
#
def keep_running():

    global ShouldContinue    
    
    return ShouldContinue

#
# define a request handler function
#
class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        
        global ShouldContinue 
        #print self.path
        #self.log_request()
        if (self.path == "/"):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write("<script>location.href='/token/'+ location.hash.split('&').filter(function(s) { return s.startsWith('access_token'); })[0].substr(13);</script>")
            return

        if (self.path[:7] != "/token/"):
            self.send_response(204)
            return
            
        access_token = self.path[7:]

        # close window 
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<script>window.close()</script>")
        
        ShouldContinue = False

        #
        # Create a connector to the device
        #
        f = samiXE.samiExportHandle(access_token, startDate, endDate, sdid)

        #
        # get the data
        #
        allData = f.getSimbaPortalData()

        (rawData, sigTypes) = ss.parseSimba(allData)
        savemat ('MatlabTransfer.mat', rawData)

        data = ss.timeAlignParse(rawData, sigTypes)
        savemat ('MatlabTransferSrc.mat', data)




#
# start up a listener for authentication and wait for response
#
#clientID = "17505a3fcca44da9bc546f31d1f40ce2";
clientID = "8763b79f8c324a7c915e0b6b6fadf24f";
port = 51515
HandlerClass = MyRequestHandler
ServerClass  = BaseHTTPServer.HTTPServer
Protocol     = "HTTP/1.0"
url = "https://accounts.samsungsami.io/authorize?client_id=" + clientID + "&response_type=token&redirect_uri=http://localhost:" + str(port) + "&scope=read,write";
server_address = ('127.0.0.1', port)

HandlerClass.protocol_version = Protocol

httpd = ServerClass(server_address, HandlerClass)
webbrowser.open(url)

while keep_running():
    httpd.handle_request()


