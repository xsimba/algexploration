# -*- coding: utf-8 -*-
"""
test script to read live data 

"""

#Parsing live stream data
import sensorSchema as ss 
import json
from scipy.io import savemat

ins = open("logfile.txt","r")
array = []
for line in ins:
    array.append(line)

data = []
new_array=[]
toSearch = '# text-message: {'
replaceWith = '{'
for elem in array:
    if toSearch in elem:
        new_elem = elem.replace(toSearch, replaceWith)
        new_array.append(new_elem)
   
for elem in new_array:
        json_object =   json.loads(elem)
        data.append(json_object)

allData = []
for elem in data:
    if 'ts' in elem:
        temp = ss.simbaExtractSingleData(elem)
        allData.append(temp)

new_dict = ss.parseSimba(allData)

savemat('FromLiveStream.mat', new_dict)