# -*- coding: utf-8 -*-
"""
Access data using 'historical' API

"""

from samiAccessHistorical import getData
import sys, getopt
from parseHistoricalData import parseHistoricalData


def main(argv):
    userID = ''
    deviceID = ''
    deviceToken = ''
    startDate = ''
    endDate = ''
    
    try:
        opts,args = getopt.getopt(argv, "h",["uid=","did=","dtoken=","sdate=","edate="])
    except getopt.GetoptError:
        print 'getHistorical.py --uid <userID> --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'userCredentials.py --uid <userID> --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
            sys.exit()
        elif opt == "--uid":
            userID = arg
        elif opt == "--did":
            deviceID = arg
        elif opt == "--dtoken":
            deviceToken = arg
        elif opt == "--sdate":
            startDate = arg
        elif opt == "--edate":
            endDate = arg
    credentials = {}
    credentials['userID'] = userID
    credentials['deviceID'] = deviceID
    credentials['deviceToken'] = deviceToken
    credentials['startDate'] = startDate
    credentials['endDate'] = endDate
    return credentials
    
if __name__ == "__main__":
    credentials = main(sys.argv[1:])

userID = credentials['userID']
deviceID = credentials['deviceID']
deviceToken = credentials['deviceToken']
startDate = credentials['startDate']
endDate = credentials['endDate']

'''
userID = 'f82abfdfbd594853b8ca21e2eb96932d'
simbandDeviceID = '771cf8655c3249ada22ea7c405e32eec'
simbandTokenID = 'fbc35eac9f1d4850b03112801189b7dd'
startDate = '1325376000000'
currentTime = int(time.time()) * 1000
endDate = str(currentTime)
'''

print 'userID', userID
print 'deviceID', deviceID
print 'deviceToken', deviceToken
print 'startDate', startDate
print 'endDate', endDate

x = getData(userID,deviceID,deviceToken,startDate,endDate)
allData = x.simbaData()
parseHistoricalData(allData)

