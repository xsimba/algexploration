# -*- coding: utf-8 -*-
"""
samiExportHandle class manages the 

takes deviceId, startDate, endDate access_token as inputs.

example constructor
data = getData(access_token, startDate, endDate, deviceId)

"""
import requests
import io
import json
import sys
import tarfile
from cStringIO import StringIO
from datetime import datetime
from time import mktime
import sensorSchema as ss
from time import sleep


#
# 
#
class samiExportHandle():
    access_token = ''
    startDate = ''
    endDate = ''
    deviceId = ''
    trialId = ''
    count = 0
    
    def __init__(self,access_token, startDate, endDate, deviceId, trialId=""):
        self.access_token = access_token
        self.startDate = startDate
        self.endDate = endDate
        self.deviceId = deviceId
        self.trialId = trialId

    # TODO: add validation
    def timerSample(self):
        thisTime = datetime.now()
        sec_since_epoch = mktime(thisTime.timetuple()) + thisTime.microsecond / 1.0e6
        return sec_since_epoch * 1000
    
    
    #
    # parseJsonFromFilePointer(json_file_pointer, extractor): 
    # reads in a Json file, and using the extractor, outputs a dict list. Note input 
    # is a file pointer -- the result of an open() command.   
    #
    def parseJsonFromFilePointer(self, json_file_pointer, extractor):

        dictResult = json.load(json_file_pointer)

        # parse and convert
        hasData = dictResult.get('data')
        if hasData == None:
            print 'There is no data to extract with the specified parameters'
            return []

        data = dictResult['data']
        processData = map(extractor, data)

        return processData

    #
    # Returns the json data from file pointer without parsing it.
    def getJsonFromFilePointer(self, json_file_pointer):

        dictResult = json.load(json_file_pointer)

        # parse and convert
        hasData = dictResult.get('data')
        if hasData == None:
            print 'There is no data to extract with the specified parameters'
            return []

        data = dictResult['data']
        return data


    #
    # processTarredJsonFile unpacks a .tgz and slurps in all of the JSONs into 
    # a the parser "extractor" as specified and outputs a dict list.  
    #
    def processTgzJsonFromTarfilePointer(self, tar_file_pointer, extractor):
        
        Result = tar_file_pointer        
        names = Result.getnames()

        allData = []

        # loop through elements of the .tgz file, assuming that it is a single sdid
        # TODO: need to put an os command to find the filenames and loop
        numParts= len(names)
        index = 1
        if numParts > 0:
            names.sort()
        for name in names:
            f = Result.extractfile(name)
            processData = self.parseJsonFromFilePointer(f, extractor)
            allData.extend(processData)
            
        return allData

        #
    # processTarredJsonFile unpacks a .tgz and slurps in all of the JSONs into
    # a the parser "extractor" as specified and outputs a dict list.
    #
    def createJsonFromTarfilePointer(self, tar_file_pointer, filename):

        Result = tar_file_pointer
        names = Result.getnames()

        # loop through elements of the .tgz file, assuming that it is a single sdid
        # TODO: need to put an os command to find the filenames and loop
        numParts= len(names)
        index = 1
        if numParts > 0:
            names.sort()

            for name in names:
                if index == 1:
                    f = Result.extractfile(name)
                    with io.open(filename, "w", buffering=1, encoding='utf-8') as outfile:
                        outfile.write(unicode(f.read()))
                else:
                    with io.open(filename, "a", buffering=1, encoding='utf-8') as outfile:
                        outfile.write(unicode(f.read()))

                index += 1


    #
    # processExistingTgz(self, tar_file_name, extractor):
    #
    #
    def processExistingTgz(self, tar_file_name, extractor):
    
        Result = tarfile.open(tar_file_name, mode='r')
        allData = self.processTgzJsonFromTarfilePointer(Result)

        return allData

    def writeTarToDisk(self, data):
        tar2 = tarfile.open("json.tar.gz",mode= "w:gz")
        info = tarfile.TarInfo(name="foo")
        info.size = len(data.content)
        tar2.addfile(tarinfo=info, fileobj=StringIO(data.content))
        tar2.close()


 #
    # getGenericData.
    #                (1) uses the export API to request a set of data,
    #                (2) polls until it is ready,
    #                (3) downloads it as a tgz, and
    #                (4) calls the parser on each element
    #
    def getGenericData(self, url, extractor):
        getHeaders = {'Authorization':'bearer '+self.access_token}

        # refashion this export request to support sdid's & trials.
        print url
        sys.stdout.flush()

        getExportIdURL= url

        exportId = requests.request('GET', getExportIdURL, headers = getHeaders, verify = False)

        exportInfo = json.loads(exportId.content)
        #print exportInfo

        exportStatusURL='https://api.samsungsami.io/v1.1/messages/export/'+exportInfo['exportId']+'/status'
        sleep(0.5)
        exportStatus = requests.request('GET',exportStatusURL, headers = getHeaders, verify=False)
        dictExportStatus = json.loads(exportStatus.content)
        print "Request status: " + dictExportStatus['status']
        sys.stdout.flush()

        while dictExportStatus['status'] != 'Success':
            exportStatus = requests.request('GET',exportStatusURL, headers = getHeaders, verify=False)
            sleep(5)
            dictExportStatus = json.loads(exportStatus.content)
            print "Request status: " + dictExportStatus['status']
            sys.stdout.flush()

            if dictExportStatus['status'] == 'Success':
                exportDataURL='https://api.samsungsami.io/v1.1/messages/export/'+exportInfo['exportId']+'/result'
        print "Connecting to Server"
        sys.stdout.flush()
        exportResult = requests.request('GET',exportDataURL, headers = getHeaders, verify=False, stream=True)
        print "Reading " + str(float(exportResult.headers['content-length'])/1.0e6) + "Mb of data..."
        sys.stdout.flush()
        tar = tarfile.open(mode= "r:gz", fileobj = StringIO(exportResult.content))

        #self.writeTarToDisk(exportResult.content)

        return [tar, extractor]


    def convertGenericData(self, tarPtr, extractor):

        print "Decoding..."
        sys.stdout.flush()
        data = self.processTgzJsonFromTarfilePointer(tarPtr, extractor)
        print "Download Complete"
        sys.stdout.flush()

        return data


    def saveSimbaPortalJson(self, filename):
        url = 'https://api.samsungsami.io/v1.1/messages/export?sdid='\
        + self.deviceId + '&startDate=' + self.startDate + '&endDate=' + self.endDate

        [tarPtr, extractor] = self.getGenericData(url, ss.simbaExtractSingleMsg)
        self.createJsonFromTarfilePointer(tarPtr, filename)

        print "Download Complete"

        return


    def getSimbaPortalData(self):
        url = 'https://api.samsungsami.io/v1.1/messages/export?sdid='\
        + self.deviceId + '&startDate=' + self.startDate + '&endDate=' + self.endDate

        [tarPtr, extractor] = self.getGenericData(url, ss.simbaExtractSingleMsg)
        data = self.convertGenericData(tarPtr, extractor)

        return data

    def saveSimbaTrialJson(self, filename):
        url = 'https://api.samsungsami.io/v1.1/messages/export?sdid='\
        + self.deviceId + '&trialId=' + self.trialId + '&startDate=' + \
	    self.startDate + '&endDate=' + self.endDate

        [tarPtr, extractor] = self.getGenericData(url, ss.simbaExtractSingleMsg)
        self.createJsonFromTarfilePointer(tarPtr, filename)

        print "Download Complete"

        return



    def getSimbaTrialData(self):
        url = 'https://api.samsungsami.io/v1.1/messages/export?sdid='\
        + self.deviceId + '&trialId=' + self.trialId + '&startDate=' + \
	    self.startDate + '&endDate=' + self.endDate

        [tarPtr, extractor] = self.getGenericData(url, ss.simbaExtractSingleMsg)
        data = self.convertGenericData(tarPtr, extractor)


        return data
