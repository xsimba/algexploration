# -*- coding: utf-8 -*-
"""
Retrieves all the messages from vital connect device and parses all the data

Prints the time to taken retrieve each set of 1000 messages and also the
total length of data in seconds.

"""

import sys, getopt
import samiAccessHistorical as samiXH
import sensorSchema as ss 
import schemaDefinitions as sdef
from scipy.io import savemat
import numpy as np

def main(argv):
    userID = ''
    deviceID = ''
    deviceToken = ''
    startDate = ''
    endDate = ''
    
    try:
        opts,args = getopt.getopt(argv, "h",["uid=","did=","dtoken=","sdate=","edate="])
    except getopt.GetoptError:
        print 'getSamiVC.py --uid <userID> --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'getSamiData.py --uid <userID> --did <did> --dtoken <dtoken> --sdate <startdate> --edate <enddate>'
            sys.exit()
        elif opt == "--uid":
            userID = arg
        elif opt == "--did":
            deviceID = arg
        elif opt == "--dtoken":
            deviceToken = arg
        elif opt == "--sdate":
            startDate = arg
        elif opt == "--edate":
            endDate = arg
    credentials = {}
    pointer = {}
    credentials['userID'] = userID
    credentials['deviceID'] = deviceID
    credentials['deviceToken'] = deviceToken
    pointer['startDate'] = startDate
    pointer['endDate'] = endDate
    return credentials, pointer

#
# parse arguments and unpack
#    
if __name__ == "__main__":
    credentials, pointer = main(sys.argv[1:]) 

userID = credentials['userID']
deviceID = credentials['deviceID']
deviceToken = credentials['deviceToken']
startDate = pointer['startDate']
endDate = pointer['endDate']

#
# Create a connector to the device
#
f = samiXH.samiHistoricalHandle(userID,deviceID,deviceToken,startDate,endDate)

#
# to do: use the information from SAMI to determine the
# correct parsing and send into an if-else block
#
vcExtractMsg = ss.genScalarExtractor(sdef.vitalConnectSchema())
allData = f.getScalarData(vcExtractMsg)

data = ss.parseGeneric(allData)

for key in data.keys():
    data[key] = map(np.float, data[key])

savemat ('MatlabTransfer.mat', data)


