# -*- coding: utf-8 -*-
"""
test script for SamiAuthentication

"""


import BaseHTTPServer
import SimpleHTTPServer
import webbrowser

import pdb

ShouldContinue = True

def keep_running():

    global ShouldContinue    
    
    return ShouldContinue


class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        
        global ShouldContinue 
        print self.path
        #self.log_request()
        if (self.path == "/"):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write("<script>location.href='/token/'+ location.hash.split('&').filter(function(s) { return s.startsWith('access_token'); })[0].substr(13);</script>")
            return

        if (self.path[:7] != "/token/"):
            self.send_response(204)
            return
            
        token = self.path[7:]
        print token

        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<script>window.close()</script>")
        
        ShouldContinue = False


#
# start up a listener for authentication and wait for response
#
clientID = "17505a3fcca44da9bc546f31d1f40ce2";
port = 51515

HandlerClass = MyRequestHandler
ServerClass  = BaseHTTPServer.HTTPServer
Protocol     = "HTTP/1.0"
url = "https://accounts.samsungsami.io/authorize?client_id=" + clientID + "&response_type=token&redirect_uri=http://localhost:" + str(port) + "&scope=read,write";
server_address = ('127.0.0.1', port)
HandlerClass.protocol_version = Protocol
httpd = ServerClass(server_address, HandlerClass)
webbrowser.open(url)

while keep_running():
    httpd.handle_request()

