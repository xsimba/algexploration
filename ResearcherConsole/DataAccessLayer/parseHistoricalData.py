# -*- coding: utf-8 -*-
"""
parse messages from historical API

"""

import os
import pickle
from scipy.io import savemat
#reading allData from a file
#There are 21 keywords: Retrieves amplitude and timestamps for each 


def parseHistoricalData(allData):
    
    numberData = len(allData)
    count = 0
    ecgRaw = []
    ecgRawTime = []
    ecgVisualTime = []
    ecgVisual = []
    ecgTime = []
    ecg = []
    ppg3VisualTime = []
    ppg3Visual = []
    ppg1VisualTime = []
    ppg1Visual = []
    ppg0VisualTime = []
    ppg0Visual = []
    ppg2VisualTime = []
    ppg2Visual = []
    ppg2_1Time = []
    ppg2_1 = []
    ppg2_2Time = []
    ppg2_2 = []
    ppg1_1Time = []
    ppg1_1 = []
    ppg1_2Time = []
    ppg1_2 = []
    accelxTime = []
    accelx = []
    accelyTime = []
    accely = []
    accelzTime = []
    accelz = []
    heartBeatTime = []
    heartBeat = []
    ecgHeartBeatTime = []
    ecgHeartBeat = []
    patTime = []
    pat = []
    hrTime = []
    hr = []
    ppgFilt1_1 = []
    ppgFilt1_1Time = []
    ppgFilt1_2 = []
    ppgFilt1_2Time = []
    ppgFilt2_1 = []
    ppgFilt2_1Time = []
    ppgFilt2_2 = []
    ppgFilt2_2Time = []
    ppgPre1_1 = []
    ppgPre1_1Time = []
    ppgPre1_2 = []
    ppgPre1_2Time = []
    ppgPre2_1 = []
    ppgPre2_1Time = []
    ppgPre2_2 = []
    ppgPre2_2Time = []
    hrv = []
    hrvTime = []
    bp1Time = []
    bp1 = []
    bp2Time = []
    bp2 = []
    ecgFilteredTime = []
    ecgFiltered = []
    timeStamps=[]

    # Parsing Data
    while count < numberData :
        datum = allData[count]
        x = datum[0]            #Timestamp of each message (ts)
        timeStamps.append(x)
        y = datum[1]            #Amplitude vector 
        sampleRate = datum[2]   #sample rate at which data was collected
        z = datum[3]            #keyword identifier
        channels = datum[4]     #no.of channels form which data was collected
        
        #Based on the identifier data is parsed into different data types        
        if z == 1:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ecgRaw.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate #OSAMI records timestamp for the first element
                    ecgRawTime.append(nextTimeStamp)                     #Timestamps for the other elements in the vector are inferred
            else:
                ecgRaw.append(y)
                ecgRawTime.append(x)
        elif z == 2:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ecgVisual.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ecgVisualTime.append(nextTimeStamp)
            else:
                ecgVisual.append(y)
                ecgVisualTime.append(x)
        elif z == 3:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ecg.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ecgTime.append(nextTimeStamp)
            else:
                ecg.append(y)
                ecgTime.append(x)
        elif z == 4:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ppg3Visual.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ppg3VisualTime.append(nextTimeStamp)
            else:
                ppg3Visual.append(y)
                ppg3VisualTime.append(x)
        elif z == 5:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ppg1Visual.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ppg1VisualTime.append(nextTimeStamp)
            else:
                ppg1Visual.append(y)
                ppg1VisualTime.append(x)
        elif z == 6:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ppg0Visual.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ppg0VisualTime.append(nextTimeStamp)
            else:
                ppg0Visual.append(y)
                ppg0VisualTime.append(x)
        elif z == 7:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ppg2Visual.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ppg2VisualTime.append(nextTimeStamp)
            else:
                ppg2Visual.append(y)
                ppg2VisualTime.append(x)
        elif z == 8:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2): #ppg2 and ppg1 receive data from channels
                    ppg2_1.append(y[k])
                    ppg2_2.append(y[k+1])
                    ppg2_1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    ppg2_2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)                
        elif z == 9:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2):
                    ppg1_1.append(y[k])
                    ppg1_2.append(y[k+1])
                    ppg1_1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    ppg1_2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 10:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                if (channels == 3):
                    for k in range(0, numberAmplitudes, 3): #accelerometer receives data from either 2 or 3 channels
                        accelx.append(y[k])
                        accelxTime.append(firstTimeStamp + k*1000.0/sampleRate)
                        accely.append(y[k+1])
                        accelyTime.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
                        accelz.append(y[k+2])
                        accelzTime.append(firstTimeStamp + (k+2)*1000.0/sampleRate)
                elif (channels == 2): #if accelerometer receives data from 2 channels
                    for k in range(0, numberAmplitudes, 2):
                        accelx.append(y[k])
                        accelxTime.append(firstTimeStamp + k*1000.0/sampleRate)
                        accely.append(y[k+1])
                        accelyTime.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 11:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    heartBeat.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    heartBeatTime.append(nextTimeStamp)
            else:
                heartBeat.append(y)
                heartBeatTime.append(x)
        elif z == 12:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ecgHeartBeat.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ecgHeartBeatTime.append(nextTimeStamp)
            else:
                ecgHeartBeat.append(y)
                ecgHeartBeatTime.append(x)
        elif z == 13:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    pat.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    patTime.append(nextTimeStamp)
            else:
                pat.append(y)
                patTime.append(x)
        elif z == 14:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    hr.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    hrTime.append(nextTimeStamp)
            else:
                hr.append(y)
                hrTime.append(x)
        elif z == 15:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2):
                    ppgFilt1_1.append(y[k])
                    ppgFilt1_1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    ppgFilt1_2.append(y[k+1])
                    ppgFilt1_2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 16:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2):
                    ppgFilt2_1.append(y[k])
                    ppgFilt2_1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    ppgFilt2_2.append(y[k+1])
                    ppgFilt2_2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 17:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2):
                    ppgPre1_1.append(y[k])
                    ppgPre1_1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    ppgPre1_2.append(y[k+1])
                    ppgPre1_2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 18:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2):
                    ppgPre2_1.append(y[k])
                    ppgPre2_1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    ppgPre2_2.append(y[k+1])
                    ppgPre2_2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 19:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    hrv.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    hrvTime.append(nextTimeStamp)
            else:
                hrv.append(y)
                hrvTime.append(x)
        elif z == 20:
            firstTimeStamp = x
            if (len(y) > 1):
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes, 2):
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    bp1.append(y[k])
                    bp1Time.append(firstTimeStamp + k*1000.0/sampleRate)
                    bp2.append(y[k+1])
                    bp2Time.append(firstTimeStamp + (k+1)*1000.0/sampleRate)
        elif z == 21:
            firstTimeStamp = x
            if len(y) > 1:
                numberAmplitudes = len(y)
                for k in range(0, numberAmplitudes):
                    ecgFiltered.append(y[k])
                    nextTimeStamp = firstTimeStamp + k*1000.0/sampleRate
                    ecgFilteredTime.append(nextTimeStamp)
            else:
                ecgFiltered.append(y)
                ecgFilteredTime.append(x)
        count += 1
 


    # writing allData to a file
    with open('allData.pickle', 'wb') as f:
        if os.path.isfile('allData.pickle') == 'False' :
            pickle.dump(allData,f)

    #reading allData from a file
    with open('allData.pickle', 'rb') as f:
        if os.path.isfile('allData.pickle') == 'True':
            allData = pickle.load(f)
    
    savemat('ecg.mat',{'ECG':ecg})    #ecg, ppg1_1, ppg1_2, ppg2_1, ppg2_2, accel(x,y,z) and bp are saved
    savemat('ppg1_1.mat',{'PPG1_1':ppg1_1})
    savemat('ppg1_2.mat', {'PPG1_2':ppg1_2})
    savemat('ppg2_1.mat',{'PPG2_1':ppg2_1})
    savemat('ppg2_2.mat', {'PPG2_2':ppg2_2})
    savemat('xaccel.mat',{'x_Accelerometer':accelx})
    savemat('yaccel.mat',{'y_Accelerometer':accely})
    savemat('zaccel.mat',{'z_Accelerometer':accelz})
    savemat('ecgTime.mat', {'ecgTime':ecgTime})
    savemat('ppg1_1Time.mat',{'ppg1_1Time':ppg1_1Time})
    savemat('ppg1_2Time.mat',{'ppg1_2Time':ppg1_2Time})
    savemat('ppg2_1Time.mat',{'ppg2_1Time':ppg2_1Time})
    savemat('ppg2_2Time.mat',{'ppg2_2Time':ppg2_2Time})
    savemat('accelTimex.mat',{'accelTimex':accelxTime})
    savemat('accelTimey.mat', {'accelTimey':accelyTime})
    savemat('accelTimez.mat', {'accelTimez':accelzTime})
    savemat('bp1.mat',{'BP1':bp1})
    savemat('bp1Time.mat',{'BP1Time':bp1Time})
    savemat('bp2.mat',{'BP2':bp2})
    savemat('bp2Time.mat',{'BP2Time':bp2Time})
#savemat('ecgFiltered.mat',{'EcgFiltered':ecgFiltered})
#savemat('ecgHeartBeat.mat',{'EcgHeartBeat':ecgHeartBeat})
#savemat('ecgRaw.mat',{'EcgRaw':ecgRaw})
#savemat('ecgVisual.mat',{'EcgVisual':ecgVisual})
#savemat('heartBeat.mat',{'heartBeat':heartBeat})
#savemat('hr.mat',{'HR':hr})
#savemat('hrv.mat',{'HRV':hrv})
#savemat('pat.mat',{'PAT':pat})
#savemat('ppg0Visual.mat',{'PPG_0_VISUAL':ppg0Visual})
#savemat('ppg1Visual.mat',{'PPG_1_VISUAL':ppg1Visual})
#savemat('ppg3Visual.mat',{'PPG_3_VISUAL':ppg3Visual})
#savemat('ppgFilt1.mat',{'PPG_Filt1':ppgFilt1})
#savemat('ppgFilt2.mat',{'PPG_Filt2':ppgFilt2})
#savemat('ppgPre1.mat',{'PPG_Pre1':ppgPre1})
#savemat('ppgPre2.mat',{'PPG_Pre2':ppgPre2})
#savemat('allData.mat',{'allData':allData})