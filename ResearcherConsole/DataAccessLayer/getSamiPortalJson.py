# -*- coding: utf-8 -*-
"""
Authenticate to SAMI and read data using export API based on trialId

"""

import sys, getopt
import samiAccessExport as samiXE
import BaseHTTPServer
import webbrowser
import SimpleHTTPServer
import json, io
import time

ShouldContinue = True

def validateClipboard(data, writeToFile):

    clipboard = json.loads(data)

    if bool(clipboard):
        if(clipboard.has_key("startDate")):
            startDate = str(clipboard.get("startDate"))
        else:
            print "Missing startDate parameter"
            sys.exit()

        if(clipboard.has_key("endDate")):
            endDate = str(clipboard.get("endDate"))
        else:
            print "Missing endDate parameter"
            sys.exit()

        if(clipboard.has_key("sdids")):
            deviceId = str(clipboard.get("sdids"))
        else:
            print "Missing sdids parameter"
            sys.exit()

        if bool(writeToFile):
            with io.open('SamiJsonExportTrial.params.json', 'w', encoding='utf-8') as outfile:
                outfile.write(unicode(json.dumps(clipboard, indent=4,  ensure_ascii=False)))

        return startDate, endDate, deviceId

    else:
        print "Invalid pameters"
        sys.exit()

def main(argv):
    access_token = ''
    deviceId = ''
    startDate = ''
    endDate = ''
    fileName="DataExport"



    try:
        opts, arguments = getopt.getopt(argv, "h", ["params=", "pfile=", "outfile="])
    except getopt.GetoptError:
        print 'Usage: getSamiPortalJson --params <val> | --pfile <path to parameter file> [--outfile <filename>]'
        sys.exit(2)

    # process options
    for opt, val in opts:
        if opt == '-h':
            print 'Usage: getSamiPortalJson.py --params <val> | --pfile <path to parameter file> [--outfile <filename>]'
            sys.exit()

        elif opt == '--params':
            startDate, endDate, deviceId = validateClipboard(val.strip(), True)

        elif opt == '--pfile':
             with io.open(val, 'r', encoding='utf-8') as pfile:
                data = pfile.read().strip()
                startDate, endDate, deviceId = validateClipboard(data, False)



    # process JSON argument and validate
    param = dict()
    param['startDate'] = startDate
    param['endDate'] = endDate
    param['sdid'] = deviceId
#    if (not param.has_key(''))
# should do some validation in here as well...

    return access_token, param, fileName

#
# parse arguments and unpack
#
if __name__ == "__main__":
    access_token, parameters, fileName = main(sys.argv[1:])


# get input parameters
startDate = parameters['startDate']
endDate = parameters['endDate']
sdid = parameters['sdid']

#
# define a keep alive function
#
def keep_running():

    global ShouldContinue

    return ShouldContinue

#
# define a request handler function
#
class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):

        global ShouldContinue
        #print self.path
        #self.log_request()
        if (self.path == "/"):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write("<script>location.href='/token/'+ location.hash.split('&').filter(function(s) { return s.startsWith('access_token'); })[0].substr(13);</script>")
            return

        if (self.path[:7] != "/token/"):
            self.send_response(204)
            return

        access_token = self.path[7:]

        # close window
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<script>window.close()</script>")

        ShouldContinue = False

        #
        # Create a connector to the device
        #
        f = samiXE.samiExportHandle(access_token, startDate, endDate, sdid)

        #
        # get the data
        #
        outFile = fileName + str(int(time.time())) + ".json"
        f.saveSimbaPortalJson(outFile)

#
# start up a listener for authentication and wait for response
#
#clientID = "17505a3fcca44da9bc546f31d1f40ce2";
clientID = "8763b79f8c324a7c915e0b6b6fadf24f";
port = 51515
HandlerClass = MyRequestHandler
ServerClass  = BaseHTTPServer.HTTPServer
Protocol     = "HTTP/1.0"
url = "https://accounts.samsungsami.io/authorize?client_id=" + clientID + "&response_type=token&redirect_uri=http://localhost:" + str(port) + "&scope=read,write";
server_address = ('127.0.0.1', port)

HandlerClass.protocol_version = Protocol

httpd = ServerClass(server_address, HandlerClass)
webbrowser.open(url)

while keep_running():
    httpd.handle_request()
