function [FoundBeats, BeatTimestamps, State] = FindBeats(InputSamples, BlockSize, Timestamp, State, Params, SigType)
%--------------------------------------------------------------------------------
% Project   : SIMBA
% Filename  : FindBeats.m
% Content   : Matlab function for beat detection with C code as comments
% Version   : GIT 1
% Author    : Alex Young (alex.young@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%--------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% Timestamp       Timestamp of first sample in block (32768 Hz ticks)
% FoundBeats      Boolean flag to indicate if any beats were found in current block
% BeatTimestamps  List of timestamps of beats
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters
% SigType         Signal type, determines wavelet and processing options. 0:ECG, 1:PPG, 2:BioZ

% FROM BeatDetect2.cpp (5 MAY 2014)

% BeatDetect_Base_t::BeatDetect_Base_t(
%             size_t  BlockSize_,
%             // FIR filter
%             uint32_t numTaps_,
%             float *pState_,
%             float *pCoeffs_,
%             float GroupDelay_,
%             // Offset control
%             const OffsetControlParams_t&  OffsetParams_,
%             // Peak detection
%             const PeakDetect_t::Params_t&  PeakDetectParams,
%             // Memory allocation
%             MemoryMgr_t& MemMgr ) :
%     pMemMgr( &MemMgr ),
%     BlockSize( BlockSize_ ),
%     GroupDelay( GroupDelay_ ),
%     HasInitializedStateBuf( false ),
%     Offset( PeakDetectParams.offset ),
%     Count(0),
%     OffsetParams( OffsetParams_ ),
%     PeakDetector( PeakDetectParams ),
%     PeakIndexList( PeakIndexBuffer, MaxNumPeaks, true ),                    // Allow list to overflow
%     InterpolatedIndexList( InterpolatedIndexBuffer, MaxNumPeaks, false ),   // This list shouldn't overflow!
%     pPrevFilteredSamples( NULL )
% {
%     arm_fir_init_f32( &CWT_FIR, numTaps_, pCoeffs_, pState_, BlockSize_ );
% }

if (isempty(State)) % Populate state: CWT state and from PeakDetect.cpp
    State.cwt = ones(length(Params.CWT_FIR)-1,1)*InputSamples(1); % Initial state of CWT, constant level of first sample
    State.cmp = 0;
    State.v = 0;
    State.av = 0;
    State.dv = 0;
    State.max = 0;
    State.maxindx = 0;
    State.fallingedge = 0; % false
    State.risingedge = 0; % false
    State.holdoffcounter = 0;
    State.PrevInputSamples = ones(1,BlockSize)*InputSamples(1);
    State.PrevBlockSize = BlockSize;
    State.Count = 0;
    State.offset = 0;
end

% bool BeatDetect_Base_t::FindBeats(
%         const smartptr& InputData,                  // Input: New block of sample data
%         SimpleList_t<uint32_t>& BeatTimestamps )    // Output: Detected beats' timestamps (32 KHz resolution)
% {
%     BeatTimestamps.Clear();        // For each call of BeatDetector, we start with an empty list
%     // Setup normal pointers/refs from the smart pointer
%     const simba::MetaData * const pInputMetaData = InputData.getptr();
%     /*const*/ float       * const pInputSamples  = (float*) (pInputMetaData + 1);
% 
%     assert_dbg( pInputMetaData->samplecount() == BlockSize );       // Block size must always match!
% 
% 
%     // Handle initialization of the FIR's state buffer: Copy first sample value over the entire state buffer
%     if (!HasInitializedStateBuf)
%     {
%         HasInitializedStateBuf = true;
%         const size_t NumSamples = CWT_FIR.numTaps + BlockSize - 1;
%         const float  FirstSampleValue = *pInputSamples;
%         for (size_t i=0; i<NumSamples; i++)             // Iterate all state buffer entries
%             CWT_FIR.pState[i] = FirstSampleValue;
%     }
BeatTimestamps = [];
%     // Allocate memory for CWT output
%     //
%     assert_dbg( pMemMgr != NULL );
%     void *pMem = pMemMgr->Alloc( sizeof(simba::MetaData) + BlockSize * sizeof(float) );
%     assert( pMem != NULL );
%     //
%     // Set proper meta data:  Use placement syntax to construct a new meta data (via default constructor)
%     //  Note: This meta data will not actually be used!  TODO: Do we want to refactor smartptr, so that
%     //  we can use it without the meta data type?
%     simba::MetaData * const pFilteredMetaData = new (pMem) simba::MetaData();
%     //
%     float * const pFilteredSamples = (float*) (pFilteredMetaData + 1);
%     //
%     smartptr FilteredData = pMem;       // Setup a smart pointer to the allocated memory
% 
% 
%     // Apply filters: CWT and Abs / Zero Clamp
%     PerformFiltering( pInputSamples, pFilteredSamples );
[CWTsamples,State.cwt] = filter(fliplr(Params.CWT_FIR),1,InputSamples,State.cwt); % Perform filter (reverse order for Matlab to match C impl.)
if     (SigType == 0), aCWTsamples = abs(CWTsamples);             % 0: ECG use abs function
elseif (SigType == 1), aCWTsamples = CWTsamples.*(CWTsamples>0);  % 1: PPG use clamp function
else                   aCWTsamples = abs(CWTsamples);             % 2: BioZ use abs function
end

% figure;plot(aCWTsamples);

%     // Automatically update the offset parameter for the peak detector to the optimal value
%     //
%     // Calculate standard deviation of the filtered samples
%     float StdDev;
%     arm_std_f32( pFilteredSamples, &StdDev, BlockSize );
%     StdDev *= OffsetParams.StdScale;
%     //
%     float OffUpdate;
%     if (Count < OffsetParams.OffUpdateFblk)
%     {
%         Count++;
%         OffUpdate = OffsetParams.OffUpdateFast;
%     }
%     else
%     {
%         OffUpdate = OffsetParams.OffUpdateSlow;
%     }
%     Offset = Offset * (1.0f-OffUpdate) + StdDev * OffUpdate;
ss = std(aCWTsamples)*Params.StdScale;
if (State.Count < Params.OffUpdateFblk), State.offset = State.offset*(1-Params.OffUpdateFast) + ss*Params.OffUpdateFast; % Fast update for first OffUpdateFblk sample blocks
else                                     State.offset = State.offset*(1-Params.OffUpdateSlow) + ss*Params.OffUpdateSlow; % Slow update for rest
end

%     // Apply the peak detection algorithm
%     PeakDetector.setOffset( Offset );                           // Activate the new offset value at the peak detector
%     bool FoundPeaks = PeakDetector.FindPeaks( pFilteredSamples, BlockSize, PeakIndexList );
[FoundPeaks, PeakIndexList, State] = FindPeaks(aCWTsamples, BlockSize, State, Params);

%     if (FoundPeaks)
%     {
%         // Apply the peak quad interpolation algorithm
%         QuadInterpolatePeaks(
%                     pFilteredSamples,               // Input: Current block of filtered samples
%                     pPrevFilteredSamples,           // Input: Previous block of filtered samples
%                     BlockSize,                      // Input: Size of previous block of filtered samples (all blocks are the same, of course)
%                     &PeakIndexList,                 // Input: List of detected peaks (integer index values)
%                     &InterpolatedIndexList );       // Output: Corrected list of peaks (float index values)
% 
%         // Now calculate the peaks' timestamps from the (fractional) indici
%         size_t NumElems = PeakIndexList.GetNumElems();
%         for (size_t i=0; i<NumElems; i++)
%         {
%             // Correct the interpolated index with the CWT filter's group delay
%             float CorrectedIndex = InterpolatedIndexList[i] - GroupDelay;
% 
%             // Convert (fractional) index to 32 KHz resolution timestamp offset
%             float TimestampOffset = CorrectedIndex * ClockRate / SampleRate;
% 
%             // Base timestamp + offset
%             //  Note: Conversion to signed int is used, because the float value can be negative!
%             uint32_t PeakTimestamp = pInputMetaData->timestamp0() + (int32_t) TimestampOffset;
% 
%             BeatTimestamps.Add( PeakTimestamp );
%         }
%     }
if (FoundPeaks)
    InterpolatedIndexList = QuadInterpolatePeaks(aCWTsamples, State.PrevInputSamples, State.PrevBlockSize, PeakIndexList);
    for n = 1:length(PeakIndexList)
        PeakTimestamp = Timestamp + (InterpolatedIndexList(n) - Params.GroupDelay) * Params.ts2samRatio;
        %added Piero
        PeakTimestamp = PeakTimestamp./32768; 
        BeatTimestamps = [BeatTimestamps PeakTimestamp]; %#ok<AGROW>
    end
end

%     // Store the current block. This is used next time as the 'previous block'.
%     PrevFilteredData = FilteredData;                   // Store a copy of the smart pointer
%     pPrevFilteredSamples = pFilteredSamples;           // Optimization: Also store the direct pointer to the samples
% 
%     return FoundPeaks;
% }
State.PrevInputSamples = aCWTsamples;
State.PrevBlockSize = BlockSize;
State.Count = State.Count + 1;
FoundBeats = FoundPeaks;

