% convert_MIT
clear all; 

d = dir('./*.mat');

for idx = 1:numel(d)
    disp(idx)
    load(d(idx).name)
    
    data.fs = 128 ; 
    % ecg
    if size(ECG,1)>size(ECG,2)
        data.ecg.signal = ECG' ; 
    else
        data.ecg.signal = ECG ;
    end
    % ecg annotation
    if size(qrs,1)>size(qrs,2)
        data.ecg.annotations = qrs' ; 
    else
        data.ecg.annotations = qrs ;
    end
    
    % ppg green
    if size(PPG2G,1)>size(PPG2G,2)
        data.ppg.a.signal = PPG2G' ; 
    else
        data.ppg.a.signal = PPG2G ;
    end
    % ppg green annotation
    if size(PPG2G,1)>size(PPG2G,2)
        data.ppg.a.annotations = upstroke_green' ; 
    else
        data.ppg.a.annotations = upstroke_green ;
    end

    % ppg red
    if size(PPG2R,1)>size(PPG2R,2)
        data.ppg.b.signal = PPG2R' ; 
    else
        data.ppg.b.signal = PPG2R ;
    end
    
    data.timestamps = 1/128:1/128:size(data.ecg.signal,2)/128;
    
    %save(['MIT-BIH-',d(idx).name(1:end-4)],'data')
    save(d(idx).name(1:end-4),'data')

end