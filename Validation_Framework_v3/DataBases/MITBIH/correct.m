% convert_MIT
clear all; 

d = dir('./*.mat');

for idx = 1:numel(d)
    disp(idx)
    load(d(idx).name)
    temp = data ; 
    clear data;
    data.timestamps = temp.timestamps' ; 
    data.ecg.signal = temp.ecg.signal' ; 
    data.ecg.annotations = temp.ecg.annotations';
    
    %save(['MIT-BIH-',d(idx).name(1:end-4)],'data')
    save(d(idx).name(1:end-4),'data')

end