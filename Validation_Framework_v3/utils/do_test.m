% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : validation_framework.m
%  Content    : Test Framework for Simba Algorithgms
%  Version    : 0.3
%  Package    : SIMBA.Test
%  Features   : Basic Framework for multiple Algorithms Testing with
%             : 1. 1 Dataset
%             : 2. Deals with Noise Noise
%             : 3. Generates Pictures and Report % to be finished the
%             integration. The SNR does not look properly
%  Created by : P. Casale (pierluigi.casale@imec-nl.nl)
%  Date       : 14-07-2014
%  Modification and Version History: 
%  | Developer | Version | Date | 
%  
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

function test_results = do_test( algorithms_to_test,datasets_to_test,signals_to_test,input_parameters )

    test_results = struct();
    
    % addpath 
    dataset_directory = 'DataBases';
    algorithms_directory = 'Algorithms';
        
    for algIdx = 1:numel(algorithms_to_test)
        addpath(fullfile(pwd,algorithms_directory,algorithms_to_test{algIdx}));
        this_datasets_to_test = datasets_to_test{algIdx};
        for dsIdx = 1:numel(this_datasets_to_test)
           disp(['Testing ',algorithms_to_test{algIdx}, ' on ',this_datasets_to_test{dsIdx},'... ']);
           % looks for test files in the dataset directory 
           test_files = dir(fullfile(pwd,dataset_directory,this_datasets_to_test{dsIdx},'*.mat'));    
           
           % defines 
           this_dataset_detections = cell(1,numel(test_files));
           this_dataset_annotations = cell(1,numel(test_files));
           
           % looks for the signals to test
           this_signals_to_test = signals_to_test{algIdx}; 
           for sigIdx =1:numel(this_signals_to_test)
                for testIdx = 1:numel(test_files)
                    % load test file = variable name is "data"
                    load(fullfile(pwd,dataset_directory,this_datasets_to_test{dsIdx},test_files(testIdx).name));
                    this_signals_to_test = signals_to_test{algIdx};
                    % extend for more signals and for annotations feedback
                    % additional logic is due to inconsistences between
                    % acquisitions. Remove this once the data format is
                    % defined
                    switch this_signals_to_test{sigIdx}
                        case 'ECG'
                            try
                                this_signal = data.ecg.signal;
                                this_annotations = data.ecg.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end
                        case 'PPGA'
                            try
                                this_signal = data.ppg.a.signal;
                                this_annotations = data.ppg.a.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end
                        case 'PPGB'
                            try
                                this_signal = data.ppg.b.signal;
                                this_annotations = data.ppg.b.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end
                        case 'PPGC'
                            try
                                this_signal = data.ppg.c.signal;
                                this_annotations = data.ppg.c.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end     
                        case 'PPGD'
                            try
                                this_signal = data.ppg.d.signal;
                                this_annotations = data.ppg.d.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end        
                        case 'PPGE'
                            try
                                this_signal = data.ppg.e.signal;
                                this_annotations = data.ppg.e.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end     
                        case 'PPGF'
                            try
                                this_signal = data.ppg.f.signal;
                                this_annotations = data.ppg.f.annotations;
                            catch err
                                if strcmpi(err.identifier,'MATLAB:nonExistentField')
                                    this_annotations = NaN;
                                end
                            end
                    end
                    % runs the function here
                    detections_timestamps = feval(algorithms_to_test{algIdx},this_signal,input_parameters{algIdx});
                    % stores the results
                    this_dataset_detections{testIdx} = detections_timestamps ;
                    this_dataset_annotations{testIdx} = this_annotations ;
                end
                test_results.(algorithms_to_test{algIdx}).(this_signals_to_test{sigIdx}).(this_datasets_to_test{dsIdx}).detections = this_dataset_detections;
                test_results.(algorithms_to_test{algIdx}).(this_signals_to_test{sigIdx}).(this_datasets_to_test{dsIdx}).annotations = this_dataset_annotations;
           end
        end
        rmpath(fullfile(pwd,algorithms_directory,algorithms_to_test{algIdx}));
    end
    % compute metrics
    disp('Done Testing .... Computing Metrics')
    test_results = compute_metrics( test_results );
end

