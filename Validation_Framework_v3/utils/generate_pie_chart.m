function plotname = generate_pie_chart( data,params )
    
    nelem = histc(data,params.breakpoints);
    figure('units','normalized','outerposition',[0 0 1 1])
    try
        pie(nelem,params.element_names);
    catch err
        keyboard()
    end
    title(params.title);
    plotname = [params.plotname,params.name_suffix];
    saveas(gcf,plotname,'fig');
    saveas(gcf,plotname,'png');
    close all
    
end

