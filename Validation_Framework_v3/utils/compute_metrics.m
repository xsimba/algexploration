function test_results = compute_metrics(test_results)
    algorithms_to_test = fieldnames(test_results);
    for algIdx = 1:numel(algorithms_to_test)
        signals_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}));
        for sigIdx = 1:numel(signals_to_test)
            datasets_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}));
            for dsIdx = 1:numel(datasets_to_test)
                
                this_test_results = test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx});
                
%                 % annotation based metrics without time tolerance
%                 annotation_based_metrics_no_timetol = compute_annotation_based_metrics_no_timetol_binary(this_test_results.detections,this_test_results.annotations);
%                 test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_based_metrics = annotation_based_metrics_no_timetol;
%                 
                % annotation based metrics with time tolerance
                annotation_based_metrics_timetol = compute_annotation_based_metrics_with_timetol(this_test_results.detections,this_test_results.annotations);
                test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_based_metrics_timetol = annotation_based_metrics_timetol;
%                 
%                 % RMSE metric
%                 rmse_metrics = compute_annotation_based_metrics_RMSE(this_test_results.detections,this_test_results.annotations);
%                 test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).rmse_metrics = rmse_metrics;
%                 
                % annotation free metrics
                annotation_free_metrics = compute_annotation_free_metrics(this_test_results.detections);
                test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_free_metrics = annotation_free_metrics;
                
            end
        end
    end  
end