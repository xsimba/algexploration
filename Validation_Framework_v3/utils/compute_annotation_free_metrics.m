function annotation_free_metrics = compute_annotation_free_metrics( all_detections )

    interbeat_time = [];
    interbeat_groups = [];

    for tmpIdx = 1:numel(all_detections)
        temp = diff(all_detections{tmpIdx});
        interbeat_time = [interbeat_time temp];
        interbeat_groups = [interbeat_groups tmpIdx*ones(1,numel(temp))];
    end
  
    annotation_free_metrics.interbeat_time = interbeat_time;
    annotation_free_metrics.interbeat_groups = interbeat_groups;

end

