function filename = write_report( test_results )

    filename = ['Test_Report_',datestr(fix(clock),30)];
    mkdir(fullfile(pwd,filename))
    cd(fullfile(pwd,filename));
    addpath(fullfile('..','utils'));
    
    fin = fopen([filename,'.html'],'w');
    fprintf(fin,'<!DOCTYPE html>');
    fprintf(fin,'<html>');
    fprintf(fin,'<body>');
  
    [plotname_tpr,plotname_fpr,plotname_ppv] = generate_comparison_plots( test_results );
  
    fprintf(fin,'<h1> Comparison Results True Positive Rate</h1>');
    for idx = 1:size(plotname_tpr,1)
        for jdx = 1:size(plotname_tpr,2)
            if ~strcmpi(plotname_tpr{idx,jdx},'None')
                fprintf(fin,['<img src="',plotname_tpr{idx,jdx},'.png" alt="',plotname_tpr{idx,jdx},'" width="742" height="442">']);
            end
        end
    end
    
    fprintf(fin,'<h1> Comparison Results False Positive Rate</h1>');
    for idx = 1:size(plotname_fpr,1)
        for jdx = 1:size(plotname_fpr,2)
            if ~strcmpi(plotname_fpr{idx,jdx},'None')
                fprintf(fin,['<img src="',plotname_fpr{idx,jdx},'.png" alt="',plotname_fpr{idx,jdx},'" width="742" height="442">']);
            end
        end
    end
    
    fprintf(fin,'<h1> Comparison Results Positive Predictive Value</h1>');
    for idx = 1:size(plotname_ppv,1)
        for jdx = 1:size(plotname_ppv,2)
            if ~strcmpi(plotname_ppv{idx,jdx},'None')
                fprintf(fin,['<img src="',plotname_ppv{idx,jdx},'.png" alt="',plotname_ppv{idx,jdx},'" width="742" height="442">']);
            end
        end
    end

    algorithms_to_test = fieldnames(test_results);
    for algIdx = 1:numel(algorithms_to_test)
        signals_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}));
        for sigIdx = 1:numel(signals_to_test)
            datasets_to_test = fieldnames(test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}));
            for dsIdx = 1:numel(datasets_to_test)
                
                fprintf(fin,['<h1> Test Results ',algorithms_to_test{algIdx},' - ',signals_to_test{sigIdx},' - ',datasets_to_test{dsIdx},'</h1>']);
                %fprintf(fin,['<p> Annotations-based performance measures </p>']);
                ab_metrics = test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_based_metrics_timetol;
                if ab_metrics.validity
                    params = {};
                    params{1}.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                    params{1}.plotname = 'True_Positive_Rate_'; params{1}.ylabel = 'True Positive Rate'; params{1}.xlabel = 'Time Tolerance';
                    params{2}.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                    params{2}.plotname = 'False_Negative_Rate_'; params{2}.ylabel = 'False Negative Rate'; params{2}.xlabel = 'Time Tolerance';
                    params{3}.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                    params{3}.plotname = 'Positive_Predictive_Value_'; params{3}.ylabel = 'Positive Predictive Value'; params{3}.xlabel = 'Time Tolerance';
                    params{4}.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                    params{4}.plotname = 'F-Measure_'; params{4}.ylabel = 'F-Measure'; params{4}.xlabel = 'Time Tolerance';
                    figure_names = generate_metrics_plots( ab_metrics,params );
                    fprintf(fin,['<img src="',figure_names{1},'.png" alt="',figure_names{1},'" width="742" height="442">']);
                    fprintf(fin,['<img src="',figure_names{2},'.png" alt="',figure_names{2},'" width="742" height="442">']);
                    fprintf(fin,['<img src="',figure_names{3},'.png" alt="',figure_names{3},'" width="742" height="442">']);
                    fprintf(fin,['<img src="',figure_names{4},'.png" alt="',figure_names{4},'" width="742" height="442">']);
                end
              
                %fprintf(fin,['<p> Annotation-free performance measures </p>']);

                af_metrics = test_results.(algorithms_to_test{algIdx}).(signals_to_test{sigIdx}).(datasets_to_test{dsIdx}).annotation_free_metrics;
                
                % interbeat time histogram
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.xlabel = 'Interbeat time [secs]';
                params.ylabel = 'Interbeat time';
                params.plotname = 'Iterbeattime_Histogram_';
                plotname = generate_histogram(af_metrics.interbeat_time,params);
                disp(plotname)
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);

                % pie chart
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.breakpoints = [0,.3,1,5];
                params.element_names = {'< .3 secs','< 1 secs','< 5 secs','> 5 secs'};
                params.title = 'Percentage of Beats within the boundary';
                params.plotname = 'Iterbeattime_Pie_';
                plotname = generate_pie_chart(af_metrics.interbeat_time, params);
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);

                % box plot
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.xlabel = 'Test Files';
                params.ylabel = 'Interbeat time [secs]';
                params.plotname = 'Iterbeattime_Boxplot_';
                plotname = generate_boxplots(af_metrics.interbeat_time,af_metrics.interbeat_groups,params);
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);

                % linear plots
                params = struct();
                params.name_suffix = [signals_to_test{sigIdx},'-',algorithms_to_test{algIdx},'-',datasets_to_test{dsIdx}];
                params.xlabel = 'Interbeat time [secs]';
                params.ylabel = 'Interbeat time [secs]';
                params.plotname = 'Iterbeattime_Plot_';
                plotname = generate_linear_plot(af_metrics.interbeat_time,params);
                fprintf(fin,['<img src="',plotname,'.png" alt="',plotname,'" width="742" height="442">']);
                
            end
        end
    end  
    
    fprintf(fin,'</body>');
    fprintf(fin,'</html>');
    fclose(fin);

    rmpath(fullfile('..','utils'));
    cd('..')

end

