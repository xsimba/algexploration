function plotname = generate_histogram( data,params )
    figure('units','normalized','outerposition',[0 0 1 1])
    hist(data,0:10);grid on;
    h = findobj(gca,'Type','patch');
    set(h,'FaceColor',[.8 .8 1],'EdgeColor','k')
    xlim([-1 11])
    ylabel(params.ylabel);
    xlabel(params.xlabel)
    plotname = [params.plotname,params.name_suffix];
    saveas(gcf,plotname,'fig');
    saveas(gcf,plotname,'png');
    
    close all

end

