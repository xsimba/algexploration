function annotation_based_metrics = compute_annotation_based_metrics_with_timetol( all_detections,all_annotations )
    
    num_tests = numel(all_annotations);
    time_tolerance = 0.05:0.05:.75;
    x_points = numel(time_tolerance);
    
    true_positive_rate = zeros(num_tests,x_points);
    positive_predictive_value = zeros(num_tests,x_points);
    false_negative_rate = zeros(num_tests,x_points);
    f_measure = zeros(num_tests,x_points);

    for fileIdx = 1:num_tests 
        
        annotations = all_annotations{fileIdx};
        detections = repmat(all_detections{fileIdx},numel(annotations),1);
        time_boundaries = abs(bsxfun(@minus,detections,annotations'));
        % save some memory
        clear detections ; 
        
        for tolIdx = 1:x_points
            
            agreements = sum(time_boundaries<=time_tolerance(tolIdx),2);
            num_true_positive = sum(agreements==1);
            num_false_positive = sum(agreements>0)-sum(agreements==1);
            num_false_negative = sum(agreements==0);
            
            true_positive_rate(fileIdx,tolIdx) = num_true_positive/(num_true_positive+num_false_negative);
            positive_predictive_value(fileIdx,tolIdx) = num_true_positive/(num_true_positive+num_false_positive);
            false_negative_rate(fileIdx,tolIdx) = num_false_negative/(num_true_positive+num_false_negative);
                          
            f_measure(fileIdx,tolIdx) = 2*(false_negative_rate(fileIdx,tolIdx)*positive_predictive_value(fileIdx,tolIdx))...
                /(false_negative_rate(fileIdx,tolIdx)*positive_predictive_value(fileIdx,tolIdx));  
            
        end
        
    end
    
    annotation_based_metrics.time_tolerance = time_tolerance ; 
    annotation_based_metrics.true_positive_rate = mean(true_positive_rate,1);
    annotation_based_metrics.false_negative_rate = mean(false_negative_rate,1);
    annotation_based_metrics.positive_predictive_value = mean(positive_predictive_value,1);
    annotation_based_metrics.f_measure = mean(f_measure,1);

    % check validaty 
    if all(isnan(annotation_based_metrics.f_measure))
        annotation_based_metrics.validity = 0 ;
    else
        annotation_based_metrics.validity = 1 ;
    end
end

