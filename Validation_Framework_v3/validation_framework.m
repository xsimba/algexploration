% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : validation_framework.m
%  Content    : Test Framework for Simba Algorithgms
%  Version    : 3.1
%  Package    : SIMBA.Test (unofficial)
%  Features   : Basic Framework for multiple Algorithms Testing with
%             : 1. Multiple Datasets
%             : 2. Multiple Signals
%             : 3. Quantitative and Qualitative Metrics
%             : 4. Variable Function Plots
% Not Covered : 1. Error Checking
%  Created by : P. Casale (pierluigi.casale@imec-nl.nl)
%  Date       : 14-07-2014
%  Modification and Version History: 
%  | Developer | Version |   Date   |      Note          |          
%  |  pcasale  |  0.3.3  | 17072014 | Added Qualitative  |
%  |  pcasale  |  0.3.5  | 22072014 | Added Quantitative |
%  Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%       *** imec STRICTLY CONFIDENTIAL ***
% --------------------------------------------------------------------------------

clear all ; close all; 
addpath('./utils')
% define algorithms to test

algorithms_to_test = {'BDBasic',...   % Algorithm 1 to be used for both ECG and PPG
    'BD',...                          % Algorithm 2 to be used for ECG
    'BD'};                            % Algorithm 3 to be used for PPG

% define signals to test

signals_to_test = {{'ECG','PPGA','PPGB'},...        % Algorithm 1 working with ECG
    {'ECG'},...                                     % Algorithm 2 working with ECG
    {'PPGA','PPGB'}};                               % Algorithm 3 working with PPGA and PPGB

% define datasets used for testing

datasets_to_test = {{'DatasetOne','Resting','MITBIH'},...  % Algorithm 1 tested on DatasetOne, Resting and MIT 
    {'DatasetOne','Resting','MITBIH'},...                  % Algorithm 2 tested on DatasetOne, Resting and MIT - only ECG
    {'DatasetOne','Resting'}};                             % Algorithm 3 tested on DatasetOne and Resting - only PPG

% define the configurations of the algorithms

Cfg_BDBasic.Fs = 128 ;                % Configuration BDBasic for both ECG and PPG
Cfg_BD_ECG = 'ECG';                   % Configuration BD for ECG
Cfg_BD_PPG = 'PPG';                   % Configuration BD for PPGA/PPGB

% embeds in a cell

configuration_parameters = {Cfg_BDBasic,...   % Configuration Algorithm 1
    Cfg_BD_ECG , ...                          % Configuration Algorithm 2
    Cfg_BD_PPG};                              % Configuration Algorithm 3


% do test
test_results = do_test( algorithms_to_test,datasets_to_test,signals_to_test,configuration_parameters );

% write report
disp('Writing Report...')
report_location = write_report( test_results );

% save results of the test in the report directory
disp('Saving Results...')
save(fullfile(pwd,report_location,'test_results'),'test_results');

rmpath('./utils')

web(fullfile(pwd,report_location,[report_location,'.html']))