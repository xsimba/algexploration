function [action_flag, acm_acc] = action_detector(acc_sig, fft_acc, fft_ppg)

% Apollo3.5 Bosch BMA280 http://www.bosch-sensortec.com/en/homepage/products_3/3_axis_sensors/acceleration_sensors/bma280/bma280
% acc_sig calibration
% Accelerometer data -> mag 3 channels

% thre_resting  = ;
% thre_activity = ;
% 
% switch ()
%     case ()
%  
%     case ()
%         
%     otherwise
% 
% end

% v1.0 simple absolute threshold value applied on accelerometer data
threshold =.1;

temp_flag = zeros(size(acc_sig));
temp_flag(acc_sig-mean(acc_sig)>threshold)=1;

acm_acc   = sum(temp_flag)/length(temp_flag);

if acm_acc >1/1000;
    action_flag = 1;
else
    action_flag = 0;
end