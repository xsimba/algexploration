
% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : freq_decision.m
%  Content    : freature processing for HR estimation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 21-10-2014
%  Modification and Version History: 
%  | Developer | Version |    Date   | 
%  | Yelei Li  |   2.0   | 21-10-2014|
% --------------------------------------------------------------------------------

function [CI_freq] = feature_processing2(PPG_freq,ACC_freq,activity_flag, blast_radius)
%
% Inputs:
% PPG freq peaks (location), Acc freq peaks (location) and activity_flag
%
% If activity flag > 0, then subtract Acc peaks, otherwise report sorted PPG peaks. 
%
% Outputs  v1:
% freq_ibi
% Output v2:
% CI_freq: rejected peaks are set to 0, otherwise set to 1

CI_freq = ones(length(PPG_freq),1);

if activity_flag == 0   
    ACC_freq = [];                                                        
end

[~,diff_freq] = intersect(PPG_freq,ACC_freq);                             % remove PPG local peaks which overlap with motion artifacts
if ~isempty (diff_freq)
    CI_freq(diff_freq) = 0;
end
if blast_radius>0,
    for k = 0:blast_radius,
        [~,diff_freq] = intersect(PPG_freq,ACC_freq+k);                             % remove PPG local peaks which overlap with motion artifacts
        if ~isempty (diff_freq)
            CI_freq(diff_freq) = 0;
        end
        [~,diff_freq] = intersect(PPG_freq,ACC_freq-k);                             % remove PPG local peaks which overlap with motion artifacts
        if ~isempty (diff_freq)
            CI_freq(diff_freq) = 0;
        end
    end
end

end
