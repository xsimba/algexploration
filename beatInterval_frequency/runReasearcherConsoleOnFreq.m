clear all;
cd('C:\work\git\DataAnalysis\')
RC_setup

c = loadSessionData('\\105.140.2.7\share\DataAnalysis\BioSemanticDevel-server2.xlsx');

sessionList={};
% sessionList = {sessionList{:}, '10232014_sitstand'};
% sessionList = {sessionList{:}, '11042014_execWalk2'};
% sessionList = {sessionList{:}, '11112014_Yelei_2_hilb'}; % very good data quality
% sessionList = {sessionList{:}, '11182014_yelei2_hilb'}; % light motion and walking
% sessionList = {sessionList{:}, '11102014_Peter_debug'}; % mostly sitting, some motion in the middle, low resting HR
% sessionList = {sessionList{:}, '11252014_dnr'}; %
% sessionList = {sessionList{:}, '20141204_AlgoInitTest'}; % Initialization/Start of Algorithms
sessionList = {sessionList{:}, '12082014_test1_freq'};
sessionList = {sessionList{:}, '12082014_test2_freq'};
sessionList = {sessionList{:}, '12082014_test3_freq'};
sessionList = {sessionList{:}, '12082014_test4_freq'};
sessionList = {sessionList{:}, '12082014_test5_freq'};
sessionList = {sessionList{:}, '12082014_test6_freq'};
sessionList = {sessionList{:}, '12082014_test7_freq'};

% sessionList={};
% sessionList = {sessionList{:}, '12082014_test1_freq'};

RMStimeRange ={[0 632.6], [0 426.9], [0 589.3], [0 543.5], [0 541.5], [0 537], [0 742.2]};

if (0),
    AddBiosemTracks(c, sessionList, 'ibi'); % Beat base HR
else
%     InitializeTracksFromCSV(c, sessionList);
%     AddRefData(c, sessionList);
%  
%     AddTracks(c, sessionList);
%     AddInstFreqHRTracks(c, sessionList); % Hilbert based HR
    AddFreqInterbeatTracks_v3(c, sessionList)
    AddBiosemTracks(c, sessionList, 'ibi_freq2');
%     AddHRrefInferredTimestamps(c, sessionList);
    HRRMSErrors = AddHRPerformanceMetricsTracks(c, sessionList, RMStimeRange);
end

for i = 1:length(sessionList)
%     AddFreqInterbeatTracks_v3(c, sessionList{i})
%     AddBiosemTracks_fusion(c,sessionList{i},'ibi_freq2')
    fMetrics = setMetricsSessionData(c, sessionList{i});
    load(fMetrics);
    heartRateQualityPlotBand1(data,data.HR_ref.signal,sessionList{i});
%     hilbertQualityPlot(data, sessionList{i})
    naiveDataDisplay(data);
    plotHREstError(data);
    pause
    
    close all
end







