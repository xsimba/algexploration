function [State] = find_beatInterval_freq(InputSamples_PPG,InputSamples_ACC, BlockSize, Timestamp, State, Params, FIR_option)
% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : auto_HR.m
%  Content    : Heart rate estimator based on autocorrelation
%  Package    : SIMBA.Validation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 17-09-2014
%  Modification and Version History: 
%  | Developer | Version |    Date   | 
%  | Yelei Li  |   1.0   | 17-09-2014|
% --------------------------------------------------------------------------------

% InputSamples    Block of input samples
% BlockSize       Number of input samples
% Timestamp       Timestamp of first sample in block (32768 Hz ticks)
% FoundBeats      Boolean flag to indicate if any beats were found in current block
% BeatTimestamps  List of timestamps of beats
% State           Structure of state variables: input and output, initialised on first call
% Params          Structure of parameters
% SigType         Signal type, determines wavelet and processing options. 0:ECG, 1:PPG, 2:BioZ
% isempty(State)
if (isempty(State)) % Populate state: CWT state and from PeakDetect.cpp
%    if FIR_option~=0 
%         State.cwt = ones(length(Params.CWT_FIR)-1,1)*InputSamples(1); % Initial state of CWT, constant level of first sample     
%     %     % Initialise filter
% %         [~,State.cwt] = filter(fliplr(Params.CWT_FIR),1,ones(length(Params.CWT_FIR),1)*InputSamples(1)); % Perform filter (reverse order for Matlab to match C impl.)
%    end
   
%     State.HistoryCwtOutput = zeros(1,BlockSize*20); % History of CWT output including current: 5 blocks
    State.HistoryCwtOutput_PPG = repmat(InputSamples_PPG,1,Params.N_block);
    State.HistoryCwtOutput_ACC = repmat(InputSamples_ACC,1,Params.N_block);
end
% plot(State.HistoryCwtOutput_PPG);hold on;plot(InputSamples_PPG,'r')

% For each call to FindBeats, we start with an empty list
% BeatTimestamps = [];


% CWT history update
%         Previous history         input
% +-----+-----+-----+-----+-----+ +-----+
% |     :                       | |     |
% +-----+-----+-----+-----+-----+ +-----+
%                    \               |
%                     v              v
%         +-----+-----+-----+-----+-----+
%         |                       :     | New history
%         +-----+-----+-----+-----+-----+
%                                 ^
%                             timestamp

State.HistoryCwtOutput_PPG(1:end-BlockSize) = State.HistoryCwtOutput_PPG(1+BlockSize:end);
State.HistoryCwtOutput_PPG(end-BlockSize+1:end) = InputSamples_PPG;

State.HistoryCwtOutput_ACC(1:end-BlockSize) = State.HistoryCwtOutput_ACC(1+BlockSize:end);
State.HistoryCwtOutput_ACC(end-BlockSize+1:end) = InputSamples_ACC;

if FIR_option~=0
    % Apply filters: CWT and Abs / Zero Clamp
    [CWTsamples_PPG] = filter(Params.bf,1,State.HistoryCwtOutput_PPG-mean(State.HistoryCwtOutput_PPG)); % Perform filter (reverse order for Matlab to match C impl.)
% CWTsamples_PPG = State.HistoryCwtOutput_PPG;
%     aCWTsamples = CWTsamples.*(CWTsamples>0);  % 1: PPG use clamp function
CWTsamples_PPG=CWTsamples_PPG(51:end);
     CWTsamples_ACC = State.HistoryCwtOutput_ACC;
else 
     CWTsamples_PPG = State.HistoryCwtOutput_PPG;
     CWTsamples_ACC = State.HistoryCwtOutput_ACC;
%     aCWTsamples = State.HistoryCwtOutput;
end

State.location_zc = [];
% figure;[Rxy]=acf(State.HistoryCwtOutput',100);figure(10);plot(State.HistoryCwtOutput);pause; close all;
[ppgFFT,f1] =DAT2FFT2(CWTsamples_PPG',128);
[accFFT,f] =DAT2FFT2(CWTsamples_ACC',128);

[freq_range] = find(f1>1 & f1<4.1);
temp_signal  = ppgFFT(freq_range);
temp_acc     = accFFT(freq_range);
thre_ppg     = mean(temp_signal)+std(temp_signal);
thre_acc     = mean(temp_acc)+1.5*std(temp_acc);
 
% mean(accFFT)
% mean(accFFT)/std(accFFT) 

% find local peaks*********************************************************
local_ppg = find(temp_signal(1:end-2)<=temp_signal(2:end-1) & temp_signal(2:end-1)>=temp_signal(3:end))+1;
local_acc = find(temp_acc(1:end-2)<=temp_acc(2:end-1) & temp_acc(2:end-1)>=temp_acc(3:end))+1;

% find pts above threshold*************************************************
peak_ppg = find(temp_signal>=thre_ppg);
peak_acc = find(temp_acc>=thre_acc);

% identify motion artifact corruption
candidate_ppg = intersect(local_ppg,peak_ppg);
% std(accFFT)
if std(accFFT)>=3
    candidate_acc = intersect(local_acc,peak_acc);
else
    candidate_acc = [];
end

diff_peaks = setdiff(candidate_ppg,candidate_acc);
if isempty (diff_peaks)
    diff_peaks = candidate_ppg;
end

[peak_location, index_peak] = sort (ppgFFT(freq_range(diff_peaks)));

if ~isempty(peak_location)
    State.max_value = peak_location(end);
    State.inx_ibi  = f1(freq_range(diff_peaks(index_peak(end))));
else
    State.inx_ibi = 0;
end

% figure(11);
% clf
%  plot(CWTsamples_PPG);hold on;plot(State.HistoryCwtOutput_PPG-mean(State.HistoryCwtOutput_PPG),'r')
%  plot(f1,ppgFFT);hold on;plot(f,accFFT,'k');
%          plot(1:f1(end),thre_ppg*ones(length(1:f1(end)),1))
%         plot(1:f1(end),thre_acc*ones(length(1:f1(end)),1),'r')
%         set(gca, 'XLim', [1 4]);
%  hold on;
%  plot(f1(freq_range(diff_peaks)),ppgFFT(freq_range(diff_peaks)),'ro');set(gca, 'XLim', [.8 4.1]);
%  plot(State.inx_ibi,State.max_value,'ro');set(gca, 'XLim', [.8 4.1])
%  plot(f1(freq_range(candidate_acc)),accFFT(freq_range(candidate_acc)),'go');
% figure(12);clf;plot(CWTsamples_PPG);
%hold on;plot(State.HistoryCwtOutput_PPG-mean(State.HistoryCwtOutput_PPG),'r')
%  pause;

