function [CI_freq, ibi_freq, amp_freq, timestep, index_seg_end, ...
    num_pks,action_flag, Params] = generate_ibi_freq_v2(thisData, thisACC, Fs)
%
% generate_ibi_freq_v2.  Generate the frequency domain features for
% BiosemHR_v2.
%

%
% TODO (ak): 
% (1) Make it possible to adjust parameters from outside this
% routine.
% (2) Make a distinction between input parameters and outputs.. (e.g.
% sample length is not a process parameter).
%

%     addpath ../BeatDetection
%     addpath ../BeatDetection/Wavelets/Coeffs
%     CWT_PPG = importdata('./BeatDetection/Wavelets/Coeffs/PPGwavelet10_128HzDiffLPF.csv');  % PPG CWT coefficients 12 Hz passband, 2nd order numerical differentiation
    CWT_PPG = importdata('PPGwavelet10_128HzDiffLPF.csv');  % PPG CWT coefficients 12 Hz passband, 2nd order numerical differentiation

    % General
    if (~exist('Fs')),
        Params.Fs                            = 128 ;                                  % Sampling Frequency
    else
        Params.Fs                            = Fs ;
    end

%     filter history correction
    InputStream_PPG = filter(fliplr(CWT_PPG),1,thisData);        % Band-pass filtering 
            

    if size(InputStream_PPG,1)>size(InputStream_PPG,2)
        InputStream_PPG=InputStream_PPG';
    end
    
    if size(thisACC,1)>size(thisACC,2)
        InputStream_ACC=thisACC';
    else
        InputStream_ACC=thisACC;
    end
    

    Params.Len  = length(InputStream_PPG);
    % Heart rate estimation by frequency analysis
    Params.Troi  = 7.5;  % Region of interest duration (s)
    Params.Tmar  = 0.5;  % Margin duration (s)
    Params.Tupd  = 0.5;  % Update resolution (s)
    Params.ThFA  = 0.10; % 10% threshold to accept a frequency peak (normalised to 1 for highest peak)
    Params.MxPk  = 20;   % Maximum number of correlation peaks.
    Params.HRmin = 27.0; % Minimum bpm
    Params.HRmax = 240.0; % Maximum bpm
    Params.Harm  = 2;     % Maximum harmonic
    Params.Wamp  = 0;    % Use HR weighted amplitude as a sorting criteria = 1, or not = 0
    
    Params.FrMin = Params.HRmin/60.0;
    Params.FrMax = Params.HRmax*Params.Harm/60.0;

    Params.SegROI  = round(Params.Fs*Params.Troi/2)*2; % Segment region of interest (even)
    Params.SegMar  = round(Params.Fs*Params.Tmar/2)*2; % Segment margin (even)
    Params.SegLen  = Params.SegROI+2*Params.SegMar;    % Segment length: |-Mar-|-----ROI-----|-Mar-|
    Params.SegSkp  = round(Params.Fs*Params.Tupd);     % Segment skip
    Params.CsegLen = Params.SegROI+Params.SegMar;     % Circular segment length: |-----ROI-----|\Mar\|
    
    Params.xfseg = 0.5 - 0.5*cos(pi*(0:Params.SegMar-1)/Params.SegMar); % Cross fade function for margins (0 to 0.999)

    Params.Nseg = floor((Params.Len-Params.SegLen)/Params.SegSkp) + 1; % UsableLen = SegLen + (n-1)*SegOvl
    
    ffa         = zeros(Params.Nseg,Params.CsegLen/2); % Frequency analysis display array
    sdp         = zeros(1,Params.Nseg);         % Beat signal std
    index_seg_end = zeros(1,Params.Nseg);         % Beat signal std
    ibi_freq_raw = NaN(Params.MxPk,Params.Nseg);        % Raw ridge data for HR - instantaneous raw HR value (bpm)
    ibi_freq    = NaN(Params.MxPk,Params.Nseg);        % Ridge data for HR - instantaneous HR value (bpm)
    amp_freq    = NaN(Params.MxPk,Params.Nseg);        % Ridge data for HR - instantaneous HR amplitude (normalised to 0 lag)
    
     for n = 0:Params.Nseg-1
        
        Seg_PPG  = InputStream_PPG((1:Params.SegLen)+n*Params.SegSkp); % Extract cardiac signal segment
        
        % todo: add output for representative time for the calculation (not
        % the below)
        index_seg_end(n+1) = Params.SegLen + n*Params.SegSkp;
            
        Cseg_PPG = [Seg_PPG(Params.SegMar+1:Params.SegMar+Params.SegROI) ...
            Seg_PPG(1:Params.SegMar).*Params.xfseg+Seg_PPG(Params.SegMar+Params.SegROI+1:end).*(1-Params.xfseg)]; % Linear to circular conversion
        
        Seg_ACC  = InputStream_ACC((1:Params.SegLen)+n*Params.SegSkp); % Extract cardiac signal segment    
      
        Cseg_ACC = [Seg_ACC(Params.SegMar+1:Params.SegMar+Params.SegROI) Seg_ACC(1:Params.SegMar).*Params.xfseg+Seg_ACC(Params.SegMar+Params.SegROI+1:end).*(1-Params.xfseg)]; % Linear to circular conversion
               
        action_flag(n+1)     = action_detector(Cseg_ACC);

        Params.Wamp  = 2; % 0: no weighting, 1: HR weighted, 2: 1/HR weighted amplitude as a sorting criteria
        [iPPG, rPPG, aPPGx, num_pks(n+1)] = FreqAnalyHR(Cseg_PPG, Params, 0, .5, action_flag(n+1));     % do together with comlex fft ppg acc
        
        Params.Wamp  = 0; % 0: no weighting, 1: HR weighted, 2: 1/HR weighted amplitude as a sorting criteria
        [iACC, rACC, aACC] = FreqAnalyHR(Cseg_ACC, Params, 0, 1.5, 0);
        
        % Compensate amplitudes due to differentiation in PPG filter
        aPPG = aPPGx./iPPG;
        
        CI_freq(:,n+1)    = feature_processing(rPPG/60,rACC/60,action_flag(n+1));
        ibi_freq_raw(:,n+1)   = rPPG/60;
        ibi_freq(:,n+1)   = iPPG/60;
        amp_freq(:,n+1)   = aPPG;
        
        
%         1. FreqAnalyHR
%         2. activity_flag
%         3. decision_processing
    
     end
    
     timestep          = Params.Tupd;

end

