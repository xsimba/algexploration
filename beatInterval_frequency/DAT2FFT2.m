%--------------------------------------------------------------------------------
% Project    : SIMBA
% Filename : <Leadonoffdet.m>
% Content   : Convert data into fft, calculate the peak freq and the height
% Version   : GIT 1
% Author    : Eva C. Wentink (eva.wentink@imec-nl.nl)
% Copyright : Stichting imec Nederland (http://www.imec-nl.nl) 
%      *** imec STRICTLY CONFIDENTIAL ***
%%--------------------------------------------------------------------------------
% 
function [datfft,f] =DAT2FFT2(dat2fft,Fs) 

% size(dat2fft)
for p = 1:1:size(dat2fft,2)
    dat2fft(:,p)=dat2fft(:,p)-mean(dat2fft(:,p));
    T = 1/Fs;          % Sample time
    L = length(dat2fft);   % Length of signal
    NFFT = 2^nextpow2(L);
    dat_fft(:,p) = fft(dat2fft(:,p),NFFT)/L; %fft of the acc channels
    f(p,:) = Fs/2*linspace(0,1,NFFT/2+1); %corresponding frequencies
    
    datfft(p,:) = 2*abs(dat_fft(1:NFFT/2+1,p)); 

end