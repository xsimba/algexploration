close all;
clear all;
clc;

%dbstop if error

%% Select data set
% Local test data (a-h=PPG, mxyz=ACC)
% load('10252014_biking.mat');     % AccCal: NavyaBand, Channels: abxym
load('10242014_treadmill2.mat'); % AccCal: NavyaBand, Channels: eyz or fxm
% load('10232014_sitstand.mat');   % AccCal: NavyaBand, Channels: befhy
% load('10242014_treadmill3.mat'); % AccCal: NavyaBand, Channels: efxzm
% load('10242014_treadmill4.mat'); % AccCal: NavyaBand, Channels: efyz

Params.Fs = 128;

%% accelerometer data processing *******************************************
% Rough calibration               MinX,  MaxX,  MinY,  MaxY,  MinZ, MaxZ, PlotFlag (to get values)
[AccOut] = AccGainOffset(data.acc, -3100, +4400, -3350, +4050, -2400, +5150, 0); % NavyaBand Calibration
% [AccOut] = AccGainOffset(data.acc, -4096, +4096, -4096, +4096, -4096, +4096, 0); % Default Calibration for g = 4096
% [AccOut] = AccGainOffset(data.acc,    -1,    +1,    -1,    +1,    -1,    +1, 0); % Default Calibration for g = 1
%
InputStream_ACC_raw    = AccOut.y.signal; % Select ACC channel x, y, z or m
ActionDetector_ACC_raw = AccOut.m.signal; % Use magnitude for action detection

%% Examine input channels
h = fdesign.highpass('fst,fp,ast,ap', 0.250, 0.500, 50.00, 0.100, Params.Fs); % Filter for spectrograms only
HdSOS = design(h, 'ellip','MatchExactly', 'both');
Fmax = 12;

if (0)
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,AccOut.x.signal),1024,Fmax,Params.Fs,'acc X', 2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,AccOut.y.signal),1024,Fmax,Params.Fs,'acc Y', 2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,AccOut.z.signal),1024,Fmax,Params.Fs,'acc Z', 2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,AccOut.m.signal),1024,Fmax,Params.Fs,'acc mag', 2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.a.signal),1024,Fmax,Params.Fs,'ppg a',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.b.signal),1024,Fmax,Params.Fs,'ppg b',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.c.signal),1024,Fmax,Params.Fs,'ppg c',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.d.signal),1024,Fmax,Params.Fs,'ppg d',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.e.signal),1024,Fmax,Params.Fs,'ppg e',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.f.signal),1024,Fmax,Params.Fs,'ppg f',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.g.signal),1024,Fmax,Params.Fs,'ppg g',2);
    SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,data.ppg.h.signal),1024,Fmax,Params.Fs,'ppg h',2);
end

%**************************************************************************

% Filter design
% Wavelet filter                     Fpass, Fstop, Apass, Astop, Fs
h = fdesign.lowpass('fp,fst,ap,ast',  8.00, 10.75, 0.100, 61.00, Params.Fs); % Tune to get integer group delay
Hd = design(h, 'equiripple');
kd = [+1 0 -1]; % Make wavelet combining LPF and diff...
Hd.Numerator = conv(Hd.Numerator,kd); % Convolve LPF with differentiation kernel
Params.hpf = Hd;
gd = grpdelay(Params.hpf); Params.hpf_gd = gd(1);
disp(['Filter delay = ',num2str(Params.hpf_gd/Params.Fs),' s (',num2str(Params.hpf_gd),' samples)']);

% coef = Hd.Numerator; % For those amongst you that are curious, here are the filter coefficients...
% save('HRfreqCoef.mat','coef');

if (0) % See filter response
    q = abs(fft([Params.hpf.Numerator zeros(1,32768-length(Params.hpf.Numerator))]));q=q(1:16385);q=q/max(q);
    figure;plot((0:16384)*Params.Fs/32768,max(20*log10(q),-100));grid on;xlabel('Frequency (Hz)');ylabel('Normalised magnitude (dB)');title('Filter response');
    figure;plot((0:16384)*Params.Fs/32768,q);grid on;xlabel('Frequency (Hz)');ylabel('Normalised magnitude');title('Filter response');
end

% Align ACC data to PPG (delay by filter group delay)
InputStream_ACC    = [InputStream_ACC_raw(1)*ones(1,Params.hpf_gd)    InputStream_ACC_raw];
ActionDetector_ACC = [ActionDetector_ACC_raw(1)*ones(1,Params.hpf_gd) ActionDetector_ACC_raw];

%      1   2   3   4   5   6   7   8
list={'a','b','c','d','e','f','g','h'};

j = 5; % Select single PPG channel, 1 to 8 = a to g

for i = j %1:8
    eval(['InputStream_PPG_raw=data.ppg.', list{i} ,'.signal;']);
    
    % Handle initial conditions
    InputStream_PPG = filter(Params.hpf,[InputStream_PPG_raw(1)*ones(1,2*Params.hpf_gd) InputStream_PPG_raw]); % Perform FIR filter (reverse coefficient order for C impl.)
    InputStream_PPG = InputStream_PPG(2*Params.hpf_gd+1:end);
    
    [CI_freq, ibi_freq, amp_freq, timestep,index_seg_end, num_pks,action_flag] = generate_ibi_freq_v3(InputStream_PPG, InputStream_ACC, ActionDetector_ACC);
    
    if (i==j) % Zero to disable, 1 to 8 = a to g
        figure;hold on;
        plot(InputStream_PPG_raw-mean(InputStream_PPG_raw),'b');plot(InputStream_PPG,'r');
        plot((InputStream_ACC-mean(InputStream_ACC))*1,'g');title('Filter in/out (zero mean)');legend('Raw sig','Filt sig','Motion');
        SpecHarm(filtfilt(HdSOS.sosMatrix,HdSOS.ScaleValues,InputStream_PPG),1024,Fmax,Params.Fs,'Filtered signal',2);
    end
    
    eval(['data.ppg.', list{i}, '.freq_HR.ibi_freq=ibi_freq;']);
    eval(['data.ppg.', list{i}, '.freq_HR.amp_freq=amp_freq;']);
    eval(['data.ppg.', list{i}, '.freq_HR.CI_freq=CI_freq;']);
    eval(['data.ppg.', list{i}, '.freq_HR.timestep=timestep;']);
    eval(['data.ppg.', list{i}, '.freq_HR.time=data.timestamps(index_seg_end);']);
    eval(['data.ppg.', list{i}, '.freq_HR.num_pks=num_pks;']);
    eval(['data.ppg.', list{i}, '.freq_HR.action_flag=action_flag;']);
end

i = j; % Select single PPG channel for plots, 1 to 8 = a to g

eval(['w = data.ppg.', list{i}, '.freq_HR;']);

% Frequency component plots
figure; hold on; grid on;
plot(0,NaN,'.','Color',[1.0 0.7 0.3],'MarkerSize',20);plot(0,NaN,'b.');plot(0,NaN,'r.');plot(0,NaN,'g.');plot(0,NaN,'k','linewidth',2); % Only for legend!
for i=1:size(w.CI_freq,2) % Plot culled component frequencies
    cc = (w.CI_freq(:,i)==0)&(w.ibi_freq(:,i)>0);
    cf = w.ibi_freq(cc,i);
    if ~isempty(cf)
        for n=1:length(cf), plot((i-1)*timestep,cf(n)*60,'.','Color',[1.0 0.7 0.3],'MarkerSize',20); end
    end
end
hold on;
for i=1:size(w.CI_freq,2) % Plot 3 strongest components (in increasing order)
    [~,ind]=sort(w.CI_freq(:,i).*w.amp_freq(:,i),'descend');
    plot((i-1)*timestep,w.ibi_freq(ind(3),i)*60,'g.');
    plot((i-1)*timestep,w.ibi_freq(ind(2),i)*60,'r.');
    plot((i-1)*timestep,w.ibi_freq(ind(1),i)*60,'b.');
end
plot(timestep*(1:length(action_flag)),action_flag*25,'k','linewidth',2); % Indicate when action flag is active
title('Frequency masked components');xlabel('Time (s)');ylabel('Heart rate (bpm)');legend('Cull','1st','2nd','3rd','action');

figure; hold on; grid on;
plot(0,NaN,'b.');plot(0,NaN,'r.');plot(0,NaN,'g.'); % Only for legend!
for i=1:size(w.CI_freq,2)
    plot((i-1)*timestep,w.ibi_freq(ind(3),i)*60,'g.');
    plot((i-1)*timestep,w.ibi_freq(ind(2),i)*60,'r.');
    plot((i-1)*timestep,w.ibi_freq(ind(1),i)*60,'b.');
end
title('Raw components');xlabel('Time (s)');ylabel('Heart rate (bpm)');legend('1st','2nd','3rd');


%% Amplitude plots
% If you want to invert the colormap then use this: colormap(fliplr(colormap')');
ScaleGlob = 1; % 0:Scale each segment, 1:Scale globally

Nseg = size(w.amp_freq,2);
load cmap_wyrbk.mat; % For sparse data

HRamp = NaN*w.amp_freq'; % NaN array of same size as amp_freq
if (ScaleGlob)
    mx = max(max(w.amp_freq));
    for n = 1:Nseg
        amps = w.amp_freq(:,n)'/mx; % Use global scaling
        HRamp(n,(amps>0)) = amps(amps>0); % Only include values>0
    end
else
    for n = 1:Nseg
        amps = w.amp_freq(:,n)'/max(w.amp_freq(:,n)); % Scale each segment
        HRamp(n,(amps>0)) = amps(amps>0); % Only include values>0
    end
end
figure;hold on;
for q=max(num_pks):-1:1 % Plot lower amplitude data first so that it does not obscure the higher amplitude data
    scatter((0:Nseg-1)*timestep,60*w.ibi_freq(q,:)',HRamp(:,q)*50,HRamp(:,q),'filled');colormap(cmap); colorbar;
end
cl = caxis; caxis([0 cl(2)]); % Set lower colour map limit to zero
set(gca,'color',[0.76 0.87 0.76]); % Background colour light green
grid on;xlabel('Time (s)');ylabel('Instantaneous HR (bpm)');title('Raw component amplitude');


HRamp = NaN*w.amp_freq'; % NaN array of same size as amp_freq
if (ScaleGlob)
    mx = max(max(w.CI_freq.*w.amp_freq));
    for n = 1:Nseg
        amps = w.CI_freq(:,n)'.*w.amp_freq(:,n)'/mx; % Cull matched frequencies and use global scaling
        HRamp(n,(amps>0)) = amps(amps>0); % Only include values>0
    end
else
    for n = 1:Nseg
        amps = w.CI_freq(:,n)'.*w.amp_freq(:,n)'; % Cull matched frequencies
        amps = amps/max(amps); % Scale each segment
        HRamp(n,(amps>0)) = amps(amps>0); % Only include values>0
    end
end
figure;hold on;
for q=max(num_pks):-1:1 % Plot lower amplitude data first so that it does not obscure the higher amplitude data
    scatter((0:Nseg-1)*timestep,60*w.ibi_freq(q,:)',HRamp(:,q)*50,HRamp(:,q),'filled');colormap(cmap); colorbar;
end
cl = caxis; caxis([0 cl(2)]); % Set lower colour map limit to zero
set(gca,'color',[0.76 0.87 0.76]); % Background colour light green
grid on;xlabel('Time (s)');ylabel('Instantaneous HR (bpm)');title('Masked component amplitude');
