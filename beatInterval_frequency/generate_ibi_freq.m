% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : generate_ibi_freq.m
%  Content    : Heart rate estimator based on autocorrelation
%  Package    : SIMBA.Validation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 16-09-2014
%  Modification and Version History: 
%  | Developer | Version |    Date   | 
%  | Yelei Li  |   1.0   | 16-09-2014|
% --------------------------------------------------------------------------------

function ibi_freq = generate_ibi_freq( InputStream_PPG,InputStream_ACC, FIR_option, Fs )

    Params.N_block = 10;
    
    dims = size(InputStream_PPG);
    if dims(1)>dims(2)
        InputStream_PPG = InputStream_PPG';
        InputStream_ACC = InputStream_ACC';
    end
    
    % General
    if (~exist('Fs')),
        Fs                            = 128 ;                                  % Sampling Frequency
    end
    FreqTS                        = 32768;                                 % Timestamp clock frequency
    BlockSize                     = round(Fs*1);                         % 500 ms blocks
    Params.ts2samRatio            = FreqTS/Fs;                             % Ratio of timestamp to sample frequency

if FIR_option == 0
    
else
    n_order = 50;
    cutoff_f = [0 .7/64 .8/64 1];
    m_switch = [0  0  1  1];
    Params.bf = firpm(n_order,cutoff_f,m_switch);
end        

    Nblocks = floor(length(InputStream_PPG)/BlockSize);
    State = []; 
    
    for Blk = 0:Nblocks-1

        
        Timestamp = Blk*BlockSize*FreqTS/Fs; % Time stamp @ FreqTS
        InputSamples_PPG = InputStream_PPG(Blk*BlockSize+1:Blk*BlockSize+BlockSize); 
        InputSamples_ACC = InputStream_ACC(Blk*BlockSize+1:Blk*BlockSize+BlockSize); 
             
        [State] = find_beatInterval_freq(InputSamples_PPG,InputSamples_ACC, BlockSize, Timestamp, State, Params, FIR_option);
        ibi_freq(Blk+1)=1/State.inx_ibi;

%         ibi_acf(Blk+1) = State.location_zc / Fs;

    end
    

end

