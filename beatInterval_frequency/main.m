close all;
clear all;
clc;

Params.Fs= 128; %sampling rate

addpath ../BeatDetection
addpath ../BeatDetection/Wavelets/Coeffs

% load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\Apollo3.5Data\09302014\09302014_treadmill_metrics.mat');
% load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\Apollo3.5Data\10022014\10022014_treadmill_metrics.mat')
% load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\Apollo3.5Data\10132014\10132014_elliptical_metrics.mat');
% load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\Apollo3.5Data\10132014\10132014_treadmill2_metrics.mat')      %bad dataset
% load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\ssicPre\15-data_metrics.mat')
% load('\\105.140.2.7\share\SimbandDataArea\SSIC data collection\Apollo3.5Data\10102014\10102014_treadmill_metrics.mat')    % bad
% load('\\105.140.2.7\share\SimbandDataArea\Imec_data_collection\Sep18_treadmill\data_src_metrics.mat')   %bad

% InputStream_ACC=data.acc.z.signal;

% accelerometer data processing *******************************************
output.acc.signal.x = data.acc.x.signal;
output.acc.signal.y = data.acc.y.signal;
output.acc.signal.z = data.acc.z.signal;

% Default scale and offset settings for Bosch BMA280 (also dependent on configuration settings)
AccScaleOffset.xsc = 1/4096.0; AccScaleOffset.xof = 0.0;
AccScaleOffset.ysc = 1/4096.0; AccScaleOffset.yof = 0.0;
AccScaleOffset.zsc = 1/4096.0; AccScaleOffset.zof = 0.0;

cax = output.acc.signal.x*AccScaleOffset.xsc + AccScaleOffset.xof; % No calibration: apply current scales and offsets
cay = output.acc.signal.y*AccScaleOffset.ysc + AccScaleOffset.yof;
caz = output.acc.signal.z*AccScaleOffset.zsc + AccScaleOffset.zof;
AccUD = 0;

acc(:,1) = cax;
acc(:,2) = cay;
acc(:,3) = caz;
InputStream_ACC = sqrt(cax.^2+cay.^2+caz.^2); % Magnitude

figure;hold on;plot(cax,'r');plot(cay,'b');plot(caz,'g');plot(InputStream_ACC,'k');
title('Accelerometer calibrated input');xlabel('Time (samples)');ylabel('Acceleration (g)');legend('X','Y','Z','M');

 h = fdesign.highpass('fst,fp,ast,ap', 0.250, 0.500, 50.00, 0.100, Params.Fs);
Hd = design(h, 'ellip','MatchExactly', 'both'); 
Fmax = 12;
SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,InputStream_ACC),1024,Fmax,Params.Fs,'acc mag', 0);
SpecHarm(filtfilt(Hd.sosMatrix,Hd.ScaleValues,data.ppg.a.signal),1024,Fmax,Params.Fs,'ppg a',0);
    
%**************************************************************************

list={'a','b','c','d','e','f','g','h'};

CWT_PPG = importdata('./BeatDetection/Wavelets/Coeffs/PPGwavelet10_128HzDiffLPF.csv');  % PPG CWT coefficients 12 Hz passband, 2nd order numerical differentiation

% InputStream_ACCf = filter(fliplr(CWT_PPG),1,InputStream_ACC);        % Band-pass filtering 
InputStream_ACCf = InputStream_ACC; % No filtering 

for i =1:8
    
    eval(['InputStream_PPG=data.ppg.', list{i} ,'.signal']);
    
%     [HRestVecgFAraw HRestVecgFA HRestAecgFA] = FreqAnalyHR(filter(fliplr(CWT_ECG),1,ecgR), fs, 0, 0.5);
    InputStream_PPG = filter(fliplr(CWT_PPG),1,InputStream_PPG); % Band-pass filtering 
    Params.Len  = length(InputStream_PPG);
    % Heart rate estimation by frequency analysis
    Params.Troi  = 7.5;  % Region of interest duration (s)
    Params.Tmar  = 0.5;  % Margin duration (s)
    Params.Tupd  = 0.5;  % Update resolution (s)
    Params.ThFA  = 0.10; % 10% threshold to accept a frequency peak (normalised to 1 for highest peak)
    Params.MxPk  = 20;   % Maximum number of correlation peaks.
    Params.HRmin =  27.0; % Minimum bpm
    Params.HRmax = 240.0; % Maximum bpm
    Params.Harm  = 2;     % Maximum harmonic
    Params.Wamp  = 0;    % Use HR weighted amplitude as a sorting criteria = 1, or not = 0
    
    Params.FrMin = Params.HRmin/60.0;
    Params.FrMax = Params.HRmax*Params.Harm/60.0;

    Params.SegROI  = round(Params.Fs*Params.Troi/2)*2; % Segment region of interest (even)
    Params.SegMar  = round(Params.Fs*Params.Tmar/2)*2; % Segment margin (even)
    Params.SegLen  = Params.SegROI+2*Params.SegMar;    % Segment length: |-Mar-|-----ROI-----|-Mar-|
    Params.SegSkp  = round(Params.Fs*Params.Tupd);     % Segment skip
    Params.CsegLen = Params.SegROI+Params.SegMar;     % Circular segment length: |-----ROI-----|\Mar\|
    
    Params.xfseg = 0.5 - 0.5*cos(pi*(0:Params.SegMar-1)/Params.SegMar); % Cross fade function for margins (0 to 0.999)

    Params.Nseg = floor((Params.Len-Params.SegLen)/Params.SegSkp) + 1; % UsableLen = SegLen + (n-1)*SegOvl
    
    ffa         = zeros(Params.Nseg,Params.CsegLen/2); % Frequency analysis display array
    sdp         = zeros(1,Params.Nseg);         % Beat signal std
    ibi_freq_raw = NaN(Params.MxPk,Params.Nseg);        % Raw ridge data for HR - instantaneous raw HR value (bpm)
    ibi_freq    = NaN(Params.MxPk,Params.Nseg);        % Ridge data for HR - instantaneous HR value (bpm)
    amp_freq    = NaN(Params.MxPk,Params.Nseg);        % Ridge data for HR - instantaneous HR amplitude (normalised to 0 lag)
%     iHR         = zeros(1,MxPk);         % Instantaneous HR, interpolated (for 1 segment)
%     rHR         = zeros(1,MxPk);         % Instantaneous HR, raw (for 1 segment)
%     aHR         = zeros(1,MxPk);         % Instantaneous HR amplitude (for 1 segment)
    
    for n = 0:Params.Nseg-1
        
        Seg_PPG  = InputStream_PPG((1:Params.SegLen)+n*Params.SegSkp)'; % Extract cardiac signal segment
       
        
        if size(Seg_PPG,1)>size(Seg_PPG,2)
            Seg_PPG=Seg_PPG';
        end
            
        Cseg_PPG = [Seg_PPG(Params.SegMar+1:Params.SegMar+Params.SegROI) Seg_PPG(1:Params.SegMar).*Params.xfseg+Seg_PPG(Params.SegMar+Params.SegROI+1:end).*(1-Params.xfseg)]; % Linear to circular conversion
        
        Seg_ACC  = InputStream_ACCf((1:Params.SegLen)+n*Params.SegSkp)'; % Extract cardiac signal segment    
                    
        if size(Seg_ACC,1)>size(Seg_ACC,2)
            Seg_ACC=Seg_ACC';
        end
        
        Cseg_ACC = [Seg_ACC(Params.SegMar+1:Params.SegMar+Params.SegROI) Seg_ACC(1:Params.SegMar).*Params.xfseg+Seg_ACC(Params.SegMar+Params.SegROI+1:end).*(1-Params.xfseg)]; % Linear to circular conversion
        
        
        action_flag(i,n+1)     = action_detector(Cseg_ACC);
%          figure(10)
%     clf
        [iPPG, rPPG, aPPG] = FreqAnalyHR(Cseg_PPG, Params, 0, 0.5, 1);     % do together with comlex fft ppg acc
        % [iACC, rACC, aACC] = FreqAnalyHR(Cseg_ACC, Params, 0, 1.5, 0);
        [iACC, rACC, aACC] = FreqAnalyHR(Cseg_ACC, Params, 0, 1.5, 0);
%         pause;
        
        CI_freq(:,n+1)    = feature_processing(rPPG,rACC,1);
        ibi_freq_raw(:,n+1)   = rPPG;
        ibi_freq(:,n+1)   = iPPG;
        amp_freq(:,n+1)   = aPPG;
        % Npk
        
        
%         1. FreqAnalyHR
%         2. activity_flag
%         3. decision_processing
    
    end
    
    
%     [ibi_freq,amp_freq,CI_freq,ibi_freq2] = generate_ibi_freq_multi( InputStream_PPG,InputStream_ACC, 1, 128 );
%     
    eval(['data.ppg.', list{i}, '.ibi_freq=ibi_freq;']);
    eval(['data.ppg.', list{i}, '.amp_freq=amp_freq;']);
    eval(['data.ppg.', list{i}, '.CI_freq=CI_freq;']);
    eval(['data.ppg.', list{i}, '.ibi_freq_raw=ibi_freq_raw;']);
    
end

figure
for i =1:size(data.ppg.a.CI_freq,2)
[~,ind]=max(data.ppg.a.CI_freq(:,i).*data.ppg.a.amp_freq(:,i));
hold on;
plot((i-1)*.5,data.ppg.a.ibi_freq(ind,i)*60,'.')
plot((i-1)*.5,data.ppg.a.ibi_freq(ind+1,i)*60,'r.')
plot((i-1)*.5,data.ppg.a.ibi_freq(ind+2,i)*60,'g.')
plot((i-1)*.5,data.ppg.a.ibi_freq(ind+3,i)*60,'m.')
plot((i-1)*.5,data.ppg.a.ibi_freq(ind+4,i)*60,'k.')
plot((i-1)*.5,data.ppg.a.ibi_freq(ind+5,i)*60,'y.')
end
% figure
figure;plot((1:length(InputStream_ACC))/128,InputStream_ACC./max(InputStream_ACC))
hold on;
plot((1:length(action_flag(1,:)))/2,action_flag(1,:),'r')

figure;hold on;plot(InputStream_ACCf,'b');plot(InputStream_ACC,'k');
title('Accelerometer calibrated input');xlabel('Time (samples)');ylabel('Acceleration (g)');legend('FiltM','M');
