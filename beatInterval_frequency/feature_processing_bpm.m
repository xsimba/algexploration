% --------------------------------------------------------------------------------
%  Project    : SIMBA
%  Filename   : freq_decision.m
%  Content    : freature processing for HR estimation
%  Created by : Yelei Li (yelei.li@ssi.samsung.com)
%  Date       : 21-10-2014
%  Modification and Version History:
%  | Developer | Version |    Date   |
%  | Yelei Li  |   2.0   | 21-10-2014|
% --------------------------------------------------------------------------------

function [CI_freq] = feature_processing_bpm(SigFreq_bpm, MotFreq_bpm, SigNumPk, MotNumPk, activity_flag, blast_radius_bpm)
% SigFreq_bpm        Signal frequency peak value in bpm
% MotFreq_bpm        Motion frequency peak value in bpm
% SigNumPk           Number of elements in signal
% MotNumPk           Number of elements in motion
% activity_flag      Flag to indicate activity, 0:no activity, 1:activity
% blast_radius_bpm   Range of detection of corresponding frequencies in units of bpm
% CI_freq            Output flag indicating peak rejection, 0:rejected, 1:keep

CI_freq = ones(length(SigFreq_bpm),1);

if ((MotNumPk>0)&&(SigNumPk>0)&&(activity_flag>0)) % Only when there are peaks present and activity is detected
    % Sort input frequencies to reduce number of comparisons
    [~,si] = sort(SigFreq_bpm(1:SigNumPk)); % Signal: Sort frequencies (lowest with lowest indexes)
    SigFreq_bpm(1:SigNumPk) = SigFreq_bpm(si);
    [~,mi] = sort(MotFreq_bpm(1:MotNumPk)); % Motion: Sort frequencies (lowest with lowest indexes)
    MotFreq_bpm(1:MotNumPk) = MotFreq_bpm(mi);
    
    idx_mot_start = 1;
    idx_mot_end   = MotNumPk;
    for nsig = 1:SigNumPk
        for nmot = idx_mot_start:idx_mot_end
            fdiff = SigFreq_bpm(nsig) - MotFreq_bpm(nmot);
            if (fdiff >= blast_radius_bpm), idx_mot_start = nmot; end % Adjust start point to narrow search
            if (abs(fdiff) <= blast_radius_bpm), CI_freq(si(nsig)) = 0; break; end % When frequency difference is within blast radius, flag and exit loop
        end
    end
end


% if activity_flag ~= 0
%     [~,diff_freq] = intersect(PPG_freq,ACC_freq);
%     if ~isempty (diff_freq)
%         CI_freq(diff_freq) = 0; % Remove PPG local peaks which overlap with motion artifacts
%     end
%     if blast_radius>0,
%         for k = 1:blast_radius,
%             [~,diff_freq] = intersect(PPG_freq,ACC_freq+k);
%             if ~isempty (diff_freq)
%                 CI_freq(diff_freq) = 0; % Remove PPG local peaks which overlap with motion artifacts
%             end
%             [~,diff_freq] = intersect(PPG_freq,ACC_freq-k);
%             if ~isempty (diff_freq)
%                 CI_freq(diff_freq) = 0; % Remove PPG local peaks which overlap with motion artifacts
%             end
%         end
%     end
% end

end
